#---------------------------------------------------------------------------------------------------
# Plot the zonal mean of the specified variables
#---------------------------------------------------------------------------------------------------
import os, ngl, xarray as xr, numpy as np, glob
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
np.seterr(divide='ignore', invalid='ignore')
np.errstate(divide='ignore', invalid="ignore")
#---------------------------------------------------------------------------------------------------
case,name,clr,dsh,mrk = [],[],[],[],[]
def add_case(case_in,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
pvar,lev_list = [],[]
def add_var(var_name,lev=-1): pvar.append(var_name); lev_list.append(lev)
#---------------------------------------------------------------------------------------------------

add_case('CERES-EBAF', n='CERES scrip area',  c='red',   d=0)
add_case('CERES-EBAF', n='CERES oblate area', c='green', d=1)
# add_case('CERES-EBAF', n='CERES cos wght',    c='blue',  d=2)
# add_case('CERES-EBAF', n='CERES no wgt',      c='purple',d=3)


# add_var('RESTOA')
# add_var('FSNTOA')
add_var('FLUT')  


htype,years,months,first_file,num_files = 'h0',[],[],0,3
# htype,years,months,first_file,num_files = 'h1',[],[],1,10
# months = np.arange(1,1+1,1)

fig_file = 'CERES_global_mean_test'

bin_min      = -90
bin_max      =  90
bin_dlat     = 1

plot_diff    = True
print_stats  = True
num_plot_col = 1
add_legend   = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_pvar,num_case = len(pvar),len(case)

wks = ngl.open_wks('png',fig_file)
plot = [None]*num_pvar
res = hs.res_xy()
res.vpHeightF = 0.3
res.xyLineThicknessF = 8
res.tiXAxisString = 'sin( Latitude )'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
msg_list = []
for v in range(num_pvar):
   print('  var: '+hc.tcolor.MAGENTA+pvar[v]+hc.tcolor.ENDC)
   data_list,std_list,cnt_list = [],[],[]
   bin_list = []
   glb_mean_list = []
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      if case[c]!='CERES-EBAF':
         case_obj = he.Case( name=case[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      file_list = sorted(glob.glob('/gpfs/alpine/scratch/hannah6/cli115/Obs/CERES-EBAF/*'))
      file_list = file_list[:num_files]
      ds = xr.open_mfdataset(file_list,combine='by_coords',concat_dim='time').isel(nv=0)

      lat1D,lon1D = ds['lat'],ds['lon']
      lat2D =              np.repeat( lat1D.values[...,None],len(lon1D),axis=1)
      lon2D = np.transpose(np.repeat( lon1D.values[...,None],len(lat1D),axis=1))

      # print(); print(lat2D)
      # print(); print(lon2D)
      # exit()

      # wgt = np.cos( np.deg2rad( lat1D.values ) )
      # print(np.sum(wgt))
      # print(np.sum(wgt_val))
      # wgt = wgt / np.sum(wgt)
      # for i in range(len(lat1D)): print(f'  {lat1D[i].values}   {wgt[i]}')
      # exit()

      if c==0:
         data = ds[pvar[v]].load().stack(ncol=('lat','lon'))
         lat = xr.DataArray(lat2D,dims=('lat','lon')).stack(ncol=('lat','lon'))
         lon = xr.DataArray(lon2D,dims=('lat','lon')).stack(ncol=('lat','lon'))
         # load area from scrip data
         scrip_ds = xr.open_dataset(os.getenv('HOME')+'/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc')
         area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})

         # for i in range(1000):
         #    tmp_scrip_lat = scrip_ds['grid_center_lat'][i].values
         #    tmp_scrip_lon = scrip_ds['grid_center_lon'][i].values
         #    print(f'  {lat[i].values}  {tmp_scrip_lat}      {lon[i].values}  {tmp_scrip_lon}')
         # exit()
      
      if c==1:
         data = ds[pvar[v]].load().stack(ncol=('lat','lon'))
         lat = xr.DataArray(lat2D,dims=('lat','lon')).stack(ncol=('lat','lon'))
         # calculate area
         xlon, ylat = np.meshgrid(lat1D,lon1D)
         xlon,ylat = np.transpose(xlon),np.transpose(ylat)
         # print(); print(xlon)
         # print(); print(ylat)
         # exit()
         R = hc.earth_radius(ylat)
         dlat = np.deg2rad(np.gradient(ylat, axis=1))
         dlon = np.deg2rad(np.gradient(xlon, axis=0))
         dy,dx = dlat * R , dlon * R * np.cos(np.deg2rad(ylat))
         # area = np.absolute(dy*dx) / np.square(R) # calculate area and convert to steridians
         area = np.absolute(dy*dx)  # calculate area
         area = xr.DataArray(area,dims=('lat','lon')).stack(ncol=('lat','lon'))
      
      if c==2:
         data = ds[pvar[v]]
         lat = ds['lat']

      if c==3:
         data = ds[pvar[v]]
         lat = ds['lat']

      # avoid dask array
      data.load()

      # convert ncol to DataArray - multiindex causes problems
      if 'ncol' in data.dims:
         data['ncol'] = np.arange(data.shape[1])
         area['ncol'] = np.arange(data.shape[1])
         lat['ncol']  = np.arange(data.shape[1])
      #-------------------------------------------------------------------------
      # Calculate global mean
      #-------------------------------------------------------------------------
      # if 'area' in locals() :
      #    gbl_mean = ( (data.mean(dim='time')*area).sum() / area.sum() ).values 
      #    # mask = xr.DataArray( np.ones(data.shape[1],dtype=bool), dims=data.dims[1] )
      #    # mask = mask & (lat>=bin_min) & (lat<=bin_max)
      #    # tmp_area = area.where( mask, drop=True)
      #    # tmp_data = data.where( mask, drop=True)
      #    # gbl_mean = ( (tmp_data.mean(dim='time')*tmp_area).sum() / tmp_area.sum() ).values 

      # area = area / np.sum(area) # normalize

      if c==0: gbl_mean = ( (data.mean(dim='time')*area).sum() / area.sum() ).values
      if c==1: gbl_mean = ( (data.mean(dim='time')*area).sum() / area.sum() ).values

      if c==2:
         # lat1D,lon1D = ds['lat'],ds['lon']
         # lat2D = np.repeat( lat1D.values[...,None],len(lon1D),axis=1)
         weights = np.cos( np.deg2rad( lat2D ) )
         # weights = weights / np.sum(weights)
         tmp_data = data.mean(dim='time').values
         gbl_mean = np.sum( np.multiply( tmp_data, weights ) ) / np.sum(weights)
      if c==3:
         gbl_mean = data.mean().values
      
      glb_mean_list.append(gbl_mean)

      #-------------------------------------------------------------------------
      # print diagnostic stuff
      #-------------------------------------------------------------------------
      if print_stats:
         # hc.print_time_length(data.time,indent=(' '*6))

         # msg = hc.print_stat(data,name=pvar[v],stat='naxsh',indent=(' '*6),compact=True,fmt='%.8f')            

         # tmp = data.mean(dim='time')
         # msg = hc.print_stat(tmp.values,name='data',stat='naxsh',indent=(' '*6),compact=True,fmt='%.8f')
         
         # msg = hc.print_stat(area.values,name='area',stat='naxsh',indent=(' '*6),compact=True,fmt='%.8f')
         
         # tmp = data.mean(dim='time').values * area.values
         # msg = hc.print_stat(tmp,name='data*area',stat='naxsh',indent=(' '*6),compact=True,fmt='%.12f')

         # tmp = data.mean(dim='time') * area
         # msg = hc.print_stat(tmp,name='data*area',stat='naxsh',indent=(' '*6),compact=True,fmt='%.12f')
         

         # if c==0:area_baseline = area
         # if c==1:
         #    area_diff = area/np.sum(area) - area_baseline/np.sum(area_baseline)
         #    msg = hc.print_stat(area_diff,name='area_diff',stat='naxsh',indent=(' '*6),compact=True)

         # msg_list.append('  case: '+case[c]+'\n'+msg)
         if 'gbl_mean' in locals() :
            print(f'      Area Weighted Global Mean : '+hc.tcolor.CYAN+f'{gbl_mean:6.4}'+hc.tcolor.ENDC)

         # if c==0: area1 = area.values
         # if c==1:
         #    area2 = area.values
         #    area1,area2 = area1/np.sum(area1) , area2/np.sum(area2)
         #    for i in range(20):
         #       print(f'   {area1[i]}   {area2[i]}')

         #    msg = hc.print_stat(area1,name='area1',stat='naxsh',indent=(' '*6),compact=True,fmt='%.8f')
         #    msg = hc.print_stat(area2,name='area2',stat='naxsh',indent=(' '*6),compact=True,fmt='%.8f')
         #    exit()

      #-------------------------------------------------------------------------
      # Calculate zonal mean
      #-------------------------------------------------------------------------
      if c==0 or c==1:

         bin_ds = hc.bin_YbyX( data.mean(dim='time'), lat, 
                              bin_min=bin_min, bin_max=bin_max, 
                              bin_spc=bin_dlat, wgt=area )
         data_list.append( bin_ds['bin_val'].values )
         lat_bins = bin_ds['bins'].values

         # print(bin_ds['bin_val'].values)

         # if c==0:baseline = bin_ds['bin_val'].values
         # if c==1:
         #    diff_tmp = bin_ds['bin_val'].values - baseline
         #    msg = hc.print_stat(diff_tmp,name='diff_tmp',stat='naxsh',indent=(' '*6),compact=True)
         #    exit()

      if c==2:
         weights = np.cos( np.deg2rad( lat2D ) )
         # weights = weights / np.sum(weights)
         # # wgt_data = np.multiply( data.mean(dim='time').values, weights )
         # wgt_data = data.mean(dim='time').values * weights
         # print(wgt_data.shape)
         # exit()
         # zonal_mean_data = np.sum( data.mean(dim='time').values * weights, axis=1 )
         zonal_mean_data = np.sum( data.mean(dim='time').values * weights, axis=1 ) / np.sum(weights,axis=1)
         # zonal_mean_data = data.mean(dim=['time','lon'])
         lat_bins = lat.values
         data_list.append( zonal_mean_data )
      if c==3:
         zonal_mean_data = data.mean(dim=['time','lon'])
         lat_bins = lat.values
         data_list.append( zonal_mean_data.values )

      sin_lat_bins = np.sin(lat_bins*np.pi/180.)
      bin_list.append(sin_lat_bins)

      if 'area' in locals(): del area
      if 'gbl_mean' in locals(): del gbl_mean

   #----------------------------------------------------------------------------
   # Take difference from first case
   #----------------------------------------------------------------------------
   if plot_diff :
      data_tmp = data_list
      data_baseline = data_list[0]
      for c in range(num_case): data_list[c] = data_list[c] - data_baseline

      res.trYMinF = np.min(data_list)
      res.trYMaxF = np.max(data_list)
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   res.trXMinF,res.trXMaxF = -1.,1.

   lat_tick = np.array([-90,-60,-30,0,30,60,90])
   res.tmXBMode = "Explicit"
   res.tmXBValues = np.sin( lat_tick*3.14159/180. )
   res.tmXBLabels = lat_tick

   for c in range(num_case):
      res.xyLineColor   = clr[c]
      res.xyDashPattern = dsh[c]
      tplot = ngl.xy(wks, bin_list[c], np.ma.masked_invalid( data_list[c] ), res)
      if c==0:
         plot[v] = tplot
      else:
         ngl.overlay( plot[v], tplot )

   hs.set_subtitles(wks, plot[v], '', '', pvar[v], font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
pres = hs.setres_panel()
pres.nglFrame = False
pres.nglPanelRight = 0.5
ngl.panel(wks,plot,layout,pres)
#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1 and add_legend:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.03*num_case
   lgres.lgLabelFontHeightF = 0.008
   lgres.lgLabelFont        = "courier"
   lgres.lgMonoDashIndex    = False
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 16
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   labels = name
   max_label_len = max([len(n) for n in name])+2
   for n,nn in enumerate(name):
      labels[n] = f'  {nn:{max_label_len}}'
      if num_pvar==1: labels[n] += f'  {glb_mean_list[n]:6.1f}'

   ndcx,ndcy = 0.5,0.6

   pid = ngl.legend_ndc(wks, len(labels), labels, ndcx, ndcy, lgres)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
ngl.frame(wks)
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# wgt_lat_list,wgt_val_list = [],[]
# wgt_lat_list.append( -89.50), wgt_val_list.append(  0.000077)
# wgt_lat_list.append( -88.50), wgt_val_list.append(  0.000231)
# wgt_lat_list.append( -87.50), wgt_val_list.append(  0.000384)
# wgt_lat_list.append( -86.50), wgt_val_list.append(  0.000538)
# wgt_lat_list.append( -85.50), wgt_val_list.append(  0.000691)
# wgt_lat_list.append( -84.50), wgt_val_list.append(  0.000844)
# wgt_lat_list.append( -83.50), wgt_val_list.append(  0.000997)
# wgt_lat_list.append( -82.50), wgt_val_list.append(  0.001149)
# wgt_lat_list.append( -81.50), wgt_val_list.append(  0.001301)
# wgt_lat_list.append( -80.50), wgt_val_list.append(  0.001453)
# wgt_lat_list.append( -79.50), wgt_val_list.append(  0.001604)
# wgt_lat_list.append( -78.50), wgt_val_list.append(  0.001755)
# wgt_lat_list.append( -77.50), wgt_val_list.append(  0.001905)
# wgt_lat_list.append( -76.50), wgt_val_list.append(  0.002054)
# wgt_lat_list.append( -75.50), wgt_val_list.append(  0.002203)
# wgt_lat_list.append( -74.50), wgt_val_list.append(  0.002351)
# wgt_lat_list.append( -73.50), wgt_val_list.append(  0.002498)
# wgt_lat_list.append( -72.50), wgt_val_list.append(  0.002645)
# wgt_lat_list.append( -71.50), wgt_val_list.append(  0.002790)
# wgt_lat_list.append( -70.50), wgt_val_list.append(  0.002935)
# wgt_lat_list.append( -69.50), wgt_val_list.append(  0.003078)
# wgt_lat_list.append( -68.50), wgt_val_list.append(  0.003221)
# wgt_lat_list.append( -67.50), wgt_val_list.append(  0.003363)
# wgt_lat_list.append( -66.50), wgt_val_list.append(  0.003504)
# wgt_lat_list.append( -65.50), wgt_val_list.append(  0.003643)
# wgt_lat_list.append( -64.50), wgt_val_list.append(  0.003781)
# wgt_lat_list.append( -63.50), wgt_val_list.append(  0.003918)
# wgt_lat_list.append( -62.50), wgt_val_list.append(  0.004054)
# wgt_lat_list.append( -61.50), wgt_val_list.append(  0.004189)
# wgt_lat_list.append( -60.50), wgt_val_list.append(  0.004321)
# wgt_lat_list.append( -59.50), wgt_val_list.append(  0.004453)
# wgt_lat_list.append( -58.50), wgt_val_list.append(  0.004584)
# wgt_lat_list.append( -57.50), wgt_val_list.append(  0.004713)
# wgt_lat_list.append( -56.50), wgt_val_list.append(  0.004840)
# wgt_lat_list.append( -55.50), wgt_val_list.append(  0.004966)
# wgt_lat_list.append( -54.50), wgt_val_list.append(  0.005090)
# wgt_lat_list.append( -53.50), wgt_val_list.append(  0.005213)
# wgt_lat_list.append( -52.50), wgt_val_list.append(  0.005333)
# wgt_lat_list.append( -51.50), wgt_val_list.append(  0.005453)
# wgt_lat_list.append( -50.50), wgt_val_list.append(  0.005570)
# wgt_lat_list.append( -49.50), wgt_val_list.append(  0.005686)
# wgt_lat_list.append( -48.50), wgt_val_list.append(  0.005800)
# wgt_lat_list.append( -47.50), wgt_val_list.append(  0.005912)
# wgt_lat_list.append( -46.50), wgt_val_list.append(  0.006023)
# wgt_lat_list.append( -45.50), wgt_val_list.append(  0.006131)
# wgt_lat_list.append( -44.50), wgt_val_list.append(  0.006237)
# wgt_lat_list.append( -43.50), wgt_val_list.append(  0.006342)
# wgt_lat_list.append( -42.50), wgt_val_list.append(  0.006445)
# wgt_lat_list.append( -41.50), wgt_val_list.append(  0.006545)
# wgt_lat_list.append( -40.50), wgt_val_list.append(  0.006643)
# wgt_lat_list.append( -39.50), wgt_val_list.append(  0.006740)
# wgt_lat_list.append( -38.50), wgt_val_list.append(  0.006834)
# wgt_lat_list.append( -37.50), wgt_val_list.append(  0.006927)
# wgt_lat_list.append( -36.50), wgt_val_list.append(  0.007017)
# wgt_lat_list.append( -35.50), wgt_val_list.append(  0.007105)
# wgt_lat_list.append( -34.50), wgt_val_list.append(  0.007191)
# wgt_lat_list.append( -33.50), wgt_val_list.append(  0.007274)
# wgt_lat_list.append( -32.50), wgt_val_list.append(  0.007355)
# wgt_lat_list.append( -31.50), wgt_val_list.append(  0.007434)
# wgt_lat_list.append( -30.50), wgt_val_list.append(  0.007511)
# wgt_lat_list.append( -29.50), wgt_val_list.append(  0.007586)
# wgt_lat_list.append( -28.50), wgt_val_list.append(  0.007658)
# wgt_lat_list.append( -27.50), wgt_val_list.append(  0.007728)
# wgt_lat_list.append( -26.50), wgt_val_list.append(  0.007796)
# wgt_lat_list.append( -25.50), wgt_val_list.append(  0.007861)
# wgt_lat_list.append( -24.50), wgt_val_list.append(  0.007923)
# wgt_lat_list.append( -23.50), wgt_val_list.append(  0.007984)
# wgt_lat_list.append( -22.50), wgt_val_list.append(  0.008042)
# wgt_lat_list.append( -21.50), wgt_val_list.append(  0.008097)
# wgt_lat_list.append( -20.50), wgt_val_list.append(  0.008151)
# wgt_lat_list.append( -19.50), wgt_val_list.append(  0.008201)
# wgt_lat_list.append( -18.50), wgt_val_list.append(  0.008250)
# wgt_lat_list.append( -17.50), wgt_val_list.append(  0.008295)
# wgt_lat_list.append( -16.50), wgt_val_list.append(  0.008339)
# wgt_lat_list.append( -15.50), wgt_val_list.append(  0.008380)
# wgt_lat_list.append( -14.50), wgt_val_list.append(  0.008418)
# wgt_lat_list.append( -13.50), wgt_val_list.append(  0.008454)
# wgt_lat_list.append( -12.50), wgt_val_list.append(  0.008487)
# wgt_lat_list.append( -11.50), wgt_val_list.append(  0.008518)
# wgt_lat_list.append( -10.50), wgt_val_list.append(  0.008546)
# wgt_lat_list.append(  -9.50), wgt_val_list.append(  0.008572)
# wgt_lat_list.append(  -8.50), wgt_val_list.append(  0.008595)
# wgt_lat_list.append(  -7.50), wgt_val_list.append(  0.008615)
# wgt_lat_list.append(  -6.50), wgt_val_list.append(  0.008633)
# wgt_lat_list.append(  -5.50), wgt_val_list.append(  0.008649)
# wgt_lat_list.append(  -4.50), wgt_val_list.append(  0.008661)
# wgt_lat_list.append(  -3.50), wgt_val_list.append(  0.008672)
# wgt_lat_list.append(  -2.50), wgt_val_list.append(  0.008679)
# wgt_lat_list.append(  -1.50), wgt_val_list.append(  0.008685)
# wgt_lat_list.append(  -0.50), wgt_val_list.append(  0.008687)
# wgt_lat_list.append(   0.50), wgt_val_list.append(  0.008687)
# wgt_lat_list.append(   1.50), wgt_val_list.append(  0.008685)
# wgt_lat_list.append(   2.50), wgt_val_list.append(  0.008679)
# wgt_lat_list.append(   3.50), wgt_val_list.append(  0.008672)
# wgt_lat_list.append(   4.50), wgt_val_list.append(  0.008661)
# wgt_lat_list.append(   5.50), wgt_val_list.append(  0.008649)
# wgt_lat_list.append(   6.50), wgt_val_list.append(  0.008633)
# wgt_lat_list.append(   7.50), wgt_val_list.append(  0.008615)
# wgt_lat_list.append(   8.50), wgt_val_list.append(  0.008595)
# wgt_lat_list.append(   9.50), wgt_val_list.append(  0.008572)
# wgt_lat_list.append(  10.50), wgt_val_list.append(  0.008546)
# wgt_lat_list.append(  11.50), wgt_val_list.append(  0.008518)
# wgt_lat_list.append(  12.50), wgt_val_list.append(  0.008487)
# wgt_lat_list.append(  13.50), wgt_val_list.append(  0.008454)
# wgt_lat_list.append(  14.50), wgt_val_list.append(  0.008418)
# wgt_lat_list.append(  15.50), wgt_val_list.append(  0.008380)
# wgt_lat_list.append(  16.50), wgt_val_list.append(  0.008339)
# wgt_lat_list.append(  17.50), wgt_val_list.append(  0.008295)
# wgt_lat_list.append(  18.50), wgt_val_list.append(  0.008250)
# wgt_lat_list.append(  19.50), wgt_val_list.append(  0.008201)
# wgt_lat_list.append(  20.50), wgt_val_list.append(  0.008151)
# wgt_lat_list.append(  21.50), wgt_val_list.append(  0.008098)
# wgt_lat_list.append(  22.50), wgt_val_list.append(  0.008042)
# wgt_lat_list.append(  23.50), wgt_val_list.append(  0.007984)
# wgt_lat_list.append(  24.50), wgt_val_list.append(  0.007923)
# wgt_lat_list.append(  25.50), wgt_val_list.append(  0.007861)
# wgt_lat_list.append(  26.50), wgt_val_list.append(  0.007796)
# wgt_lat_list.append(  27.50), wgt_val_list.append(  0.007728)
# wgt_lat_list.append(  28.50), wgt_val_list.append(  0.007658)
# wgt_lat_list.append(  29.50), wgt_val_list.append(  0.007586)
# wgt_lat_list.append(  30.50), wgt_val_list.append(  0.007511)
# wgt_lat_list.append(  31.50), wgt_val_list.append(  0.007434)
# wgt_lat_list.append(  32.50), wgt_val_list.append(  0.007355)
# wgt_lat_list.append(  33.50), wgt_val_list.append(  0.007274)
# wgt_lat_list.append(  34.50), wgt_val_list.append(  0.007190)
# wgt_lat_list.append(  35.50), wgt_val_list.append(  0.007105)
# wgt_lat_list.append(  36.50), wgt_val_list.append(  0.007017)
# wgt_lat_list.append(  37.50), wgt_val_list.append(  0.006927)
# wgt_lat_list.append(  38.50), wgt_val_list.append(  0.006834)
# wgt_lat_list.append(  39.50), wgt_val_list.append(  0.006740)
# wgt_lat_list.append(  40.50), wgt_val_list.append(  0.006643)
# wgt_lat_list.append(  41.50), wgt_val_list.append(  0.006545)
# wgt_lat_list.append(  42.50), wgt_val_list.append(  0.006444)
# wgt_lat_list.append(  43.50), wgt_val_list.append(  0.006342)
# wgt_lat_list.append(  44.50), wgt_val_list.append(  0.006237)
# wgt_lat_list.append(  45.50), wgt_val_list.append(  0.006131)
# wgt_lat_list.append(  46.50), wgt_val_list.append(  0.006023)
# wgt_lat_list.append(  47.50), wgt_val_list.append(  0.005912)
# wgt_lat_list.append(  48.50), wgt_val_list.append(  0.005800)
# wgt_lat_list.append(  49.50), wgt_val_list.append(  0.005686)
# wgt_lat_list.append(  50.50), wgt_val_list.append(  0.005570)
# wgt_lat_list.append(  51.50), wgt_val_list.append(  0.005453)
# wgt_lat_list.append(  52.50), wgt_val_list.append(  0.005334)
# wgt_lat_list.append(  53.50), wgt_val_list.append(  0.005212)
# wgt_lat_list.append(  54.50), wgt_val_list.append(  0.005090)
# wgt_lat_list.append(  55.50), wgt_val_list.append(  0.004966)
# wgt_lat_list.append(  56.50), wgt_val_list.append(  0.004840)
# wgt_lat_list.append(  57.50), wgt_val_list.append(  0.004713)
# wgt_lat_list.append(  58.50), wgt_val_list.append(  0.004584)
# wgt_lat_list.append(  59.50), wgt_val_list.append(  0.004453)
# wgt_lat_list.append(  60.50), wgt_val_list.append(  0.004322)
# wgt_lat_list.append(  61.50), wgt_val_list.append(  0.004189)
# wgt_lat_list.append(  62.50), wgt_val_list.append(  0.004054)
# wgt_lat_list.append(  63.50), wgt_val_list.append(  0.003918)
# wgt_lat_list.append(  64.50), wgt_val_list.append(  0.003781)
# wgt_lat_list.append(  65.50), wgt_val_list.append(  0.003643)
# wgt_lat_list.append(  66.50), wgt_val_list.append(  0.003504)
# wgt_lat_list.append(  67.50), wgt_val_list.append(  0.003363)
# wgt_lat_list.append(  68.50), wgt_val_list.append(  0.003221)
# wgt_lat_list.append(  69.50), wgt_val_list.append(  0.003079)
# wgt_lat_list.append(  70.50), wgt_val_list.append(  0.002935)
# wgt_lat_list.append(  71.50), wgt_val_list.append(  0.002790)
# wgt_lat_list.append(  72.50), wgt_val_list.append(  0.002645)
# wgt_lat_list.append(  73.50), wgt_val_list.append(  0.002498)
# wgt_lat_list.append(  74.50), wgt_val_list.append(  0.002351)
# wgt_lat_list.append(  75.50), wgt_val_list.append(  0.002203)
# wgt_lat_list.append(  76.50), wgt_val_list.append(  0.002054)
# wgt_lat_list.append(  77.50), wgt_val_list.append(  0.001905)
# wgt_lat_list.append(  78.50), wgt_val_list.append(  0.001755)
# wgt_lat_list.append(  79.50), wgt_val_list.append(  0.001604)
# wgt_lat_list.append(  80.50), wgt_val_list.append(  0.001453)
# wgt_lat_list.append(  81.50), wgt_val_list.append(  0.001301)
# wgt_lat_list.append(  82.50), wgt_val_list.append(  0.001149)
# wgt_lat_list.append(  83.50), wgt_val_list.append(  0.000997)
# wgt_lat_list.append(  84.50), wgt_val_list.append(  0.000844)
# wgt_lat_list.append(  85.50), wgt_val_list.append(  0.000691)
# wgt_lat_list.append(  86.50), wgt_val_list.append(  0.000538)
# wgt_lat_list.append(  87.50), wgt_val_list.append(  0.000384)
# wgt_lat_list.append(  88.50), wgt_val_list.append(  0.000231)
# wgt_lat_list.append(  89.50), wgt_val_list.append(  0.000077)

# wgt_lat = np.array(wgt_lat_list)
# wgt_val = np.array(wgt_val_list)
