#!/bin/bash
#SBATCH --account=m3312
###SBATCH --account=m4310
#SBATCH --constraint=cpu
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --qos=regular
#SBATCH --time=08:00:00
###SBATCH --job-name=analysis_batch_wk_cpl
###SBATCH --job-name=analysis_batch_wk_qbo
#SBATCH --job-name=analysis_batch_wk_prc
#SBATCH --output=/global/homes/w/whannah/Research/E3SM/logs_slurm/analysis.batch.slurm-%A.out
#SBATCH --mail-user=hannah6@llnl.gov
#SBATCH --mail-type=END,FAIL

# command to submit:  sbatch analysis.batch.nersc.sh

source activate pyn_env 

date

# time python -u code_MJO/plot.MJO.lag_regression.horz.v1.py
# time python -u code_hov/plot.hov.v1.py
# time python -u code_wk/plot.wk.v1.py
# time python -u code_QBO/2023_SciDAC_calculate_QOI.py
time python -u code_clim/plot.clim.precip.bin.v2.py
# time python -u code_ENSO/plot.ENSO.timeseries.v1.py

date