#!/bin/bash
#SBATCH -A CLI115
###SBATCH -J analysis_batch
###SBATCH -J analysis_batch_glb_ts
#SBATCH -J analysis_batch_wk
#SBATCH -N 1
#SBATCH -p gpu
#SBATCH -t 8:00:00
###SBATCH -o $HOME/Research/E3SM/slurm_logs/analysis.batch.slurm-%A.out
#SBATCH -o slurm.log.analysis.batch.%A.out
#SBATCH --mail-user=hannah6@llnl.gov
#SBATCH --mail-type=END,FAIL

# command to submit:  sbatch analysis.batch.olcf.py

source activate pyn_env 

date

# time python -u code_MJO/plot.MJO.lag_regression.horz.v1.py
# time python -u code_scream/plot.scream.animation.map.v1.py
# time python -u code_scream/plot.scream.debug.histogram.v1.py
# time python -u code_clim/plot.clim.precip.bin.v2.py
# time python -u code_scream/plot.scream.timeseries.profile.v2.py
time python -u code_wk/plot.wk.v1.py
# time python -u code_KPP/plot.KPP.zonal-mean.py
# time python -u code_clim/plot.clim.timeseries.global_mean.v1.py

date