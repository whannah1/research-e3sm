#!/bin/bash
#SBATCH -A CLI115
###SBATCH -J analysis_batch
###SBATCH -J analysis_batch_glb_ts
#SBATCH -J analysis_batch_scream_animation
#SBATCH -N 1
#SBATCH -p gpu
#SBATCH -t 8:00:00
###SBATCH -o $HOME/Research/E3SM/slurm_logs/analysis.batch.slurm-%A.out
#SBATCH -o slurm.log.analysis.batch.%A.out
#SBATCH --mail-user=hannah6@llnl.gov
#SBATCH --mail-type=END,FAIL

# command to submit:  sbatch batch.scream.animation.olcf.sh

source activate pyn_env 

date

time python -u code_scream/plot.scream.animation.map.v1.py

date