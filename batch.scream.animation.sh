#!/bin/bash
#SBATCH --account=m3312
#SBATCH --constraint=knl
#SBATCH -q regular
#SBATCH --job-name=scream-animation
#SBATCH --nodes=1
#SBATCH --time=6:00:00
#SBATCH --output=logs_slurm/slurm-%x-%j.out
​
# To run this script use the following command:
# sbatch batch.scream.animation.sh

source activate pyn_env 

# time python -u /global/homes/w/whannah/Research/E3SM/code_scream/plot.scream.animation.map.v1.py
time python -u ./code_scream/plot.scream.animation.map.v1.py