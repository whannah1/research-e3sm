import os, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string
data_dir,data_sub = None,None

### CHX experiments
name,case = [],[]
case.append(f'NOAA'); name.append('NOAA')
case.append(f'ERA5'); name.append('ERA5')
# case.append(f'INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_125_115.DT_2e0.BVT.2008-10-01'); name.append('E3SM')

#-------------------------------------------------------------------------------

var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)
add_var('TS')


fig_type = 'png'
fig_file = 'chk.INCITE2020.sst'

# lat1,lat2 = -60,60

use_remap,remap_grid = False,'180x360' # 90x180 / 180x360

htype,years,months,first_file,num_files = 'h1',[],[],0,1

plot_diff,add_diff,diff_base = True,True,0

print_stats = True
var_x_case = False
num_plot_col = 1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.02 - 0.0012*num_var - 0.0014*(num_case+int(add_diff))
subtitle_font_height = max(subtitle_font_height,0.005)

subtitle_font_height = 0.025

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.018
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

def get_comp(case):
   comp = 'eam'
   if 'CESM' in case: comp = 'cam'
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,lat_list,lon_list = [],[],[]
   # if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_sub_tmp = data_sub
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir, data_sub=data_sub_tmp )

      # case_obj.time_freq = 'daily'
      # case_obj.time_freq = 'monthly'
      # case_obj = he.Case( name=case[c], data_dir='/global/cscratch1/sd/hewang/acme_scratch/cori-knl/' )

      case_obj.set_coord_names(var[v])

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   

      if use_remap :
         lat = case_obj.load_data('lat',component=get_comp(case[c]),htype=htype,
                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
         lon = case_obj.load_data('lon',component=get_comp(case[c]),htype=htype,
                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      # else:
      #    aname = case_obj.area_name
      #    area = case_obj.load_data(aname,component=get_comp(case[c]),htype=htype)

      if case[c]=='NOAA':
         ds = xr.open_dataset('/global/homes/w/whannah/HICCUP/data_scratch/HICCUP.sst_noaa.2008-10-01.nc')
         data = ds['SST_cpl'][0,:,:]
         lat = ds['lat']
         lon = ds['lon']
      elif case[c]=='ERA5':
         # ds = xr.open_dataset('/global/cscratch1/sd/whannah/HICCUP/data/ERA5_validation.TS.2008-10-01.nc')
         ds = xr.open_dataset('/global/cscratch1/sd/whannah/HICCUP/data/ERA5_validation.TS.2008-10-01.remap_180x360.nc')
         data = ds['skt'][0,:,:] - 273
         # lat,lon = ds['latitude'],ds['longitude']
         lat,lon = ds['lat'],ds['lon']
      else:
         data = case_obj.load_data(var[v], component=get_comp(case[c]),htype=htype,ps_htype=htype,
                                         years=years,months=months,lev=lev,
                                         first_file=first_file,num_files=num_files,
                                         use_remap=use_remap,remap_str=f'remap_{remap_grid}')

      # if case[c]=='ERAi': erai_lat,erai_lon = data['lat'],data['lon']

      # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         data = data.mean(dim='time')

      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      # if case[c]=='TRMM' and 'lon1' not in locals(): 
      #    data_list.append( ngl.add_cyclic(data.values) )
      # else:
      #    data_list.append( data.values )

      data_list.append( data.values )

      if use_remap or case_obj.obs:
         lat_list.append(lat)
         lon_list.append(lon)
      else:
         lat_list.append(None)
         lon_list.append(None)

      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()
   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      # print()
      # print( np.array(data_list).shape)
      # print( np.array(data_list[0]).shape)
      # print( np.array(data_list[1]).shape)
      # print()
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir, data_sub=data_sub )
      case_obj.set_coord_names(var[v])

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      tres.cnFillPalette = "BlueWhiteOrangeRed"

      #-------------------------------------------------------------------------
      # Set explicit contour levels
      
      # if var[v]=='TS': tres.cnLevels = np.arange(0,40+2,2)
      
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 21
         aboutZero = False
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      var_str = var[v]
      
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_remap or case_obj.obs :
         # if case[c]=='ERAi': lat,lon = erai_lat,erai_lon
         lat = lat_list[c]
         lon = lon_list[c]
         hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
      else:
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype)
         
      if plot_diff and c==diff_base : base_name = name[c]

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         #----------------------------------------------------------------------
         # set plot subtitles
         #----------------------------------------------------------------------
         ctr_str = ''
         if 'name' not in vars(): case_name = case_obj.short_name
         if 'name'     in vars(): case_name = name[c]

         hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         
         if var[v] in ['TS'] : tres.cnLevels = np.arange(-10,10+1,1)

         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         
         ### override the level settings and just use auto
         # tres.cnLevelSelectionMode = "AutomaticLevels"

         if use_remap or case_obj.obs :
            # if case[c]=='ERAi': lat,lon = erai_lat,erai_lon
            lat = lat_list[c]
            lon = lon_list[c]
            hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
            # hs.set_cell_fill(tres,case_obj=case_obj,use_remap=True,lat=lat,lon=lon)
         else:
            hs.set_cell_fill(tres,case_obj=case_obj)

         tres.lbLabelBarOn = True

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+1
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)

         #-----------------------------------
         #-----------------------------------
         ctr_str = ''
         # case_name = name[c]+' - '+base_name
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name
         
         # hs.set_subtitles(wks, plot[ipd], case_name, '', var_str+' (Diff)', font_height=subtitle_font_height)
         hs.set_subtitles(wks, plot[ipd], case_name, 'Difference', var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if plot_diff : num_case = num_case+len(diff_case)   # use this to plot both before and after diff

# title_str = None
# title_str = 'ANN'
# if months==[1,2,12]: title_str = 'DJF'
# if months==[6,7,8]: title_str = 'JJA'
# if title_str is not None:
#    textres =  ngl.Resources()
#    textres.txFontHeightF =  0.025
#    ngl.text_ndc(wks,title_str,0.5,.7,textres)

num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]

if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
