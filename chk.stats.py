import os, ngl, subprocess as sp
import numpy as np, xarray as xr
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy, string
host = hc.get_host()
data_dir,data_sub = None,None
# data_dir,data_sub = '/project/projectdirs/m3312/jlee1046/E3SM/',''
#-------------------------------------------------------------------------------
# if host=='olcf':

#-------------------------------------------------------------------------------
if host=='cori':

   name,case = [],[]
   case.append('E3SM.RGMA.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.RADNX_4.00');     name.append('MMF1')

#-------------------------------------------------------------------------------
var = []
# var.append('PRECT')
# var.append('TGCLDLWP')
# var.append('TGCLDIWP')
# var.append('P-E')
# var.append('LHFLX')
# var.append('SHFLX')
# var.append('TMQ')
# var.append('FLNT')
# var.append('FSNT')
# var.append('WSPD')
# var.append('WSPD_BOT')
# var.append('U')
# var.append('NET_TOA_RAD')
# var.append('TIMINGF')

var.append('SPBUOY')
var.append('SPBUOYSD')
var.append('SPDQ')
var.append('SPDQC')
var.append('SPDQI')
var.append('SPDT')
var.append('SPKVH')
var.append('SPLCLOUD')
var.append('SPMC')
var.append('SPMCDN')
var.append('SPMCUDN')
var.append('SPMCUP')
var.append('SPMCUUP')
var.append('SPMSEF')
var.append('SPNDROPCOL')
var.append('SPNDROPMIX')
var.append('SPNDROPSRC')
var.append('SPPFLX')
var.append('SPQC')
var.append('SPQG')
var.append('SPQI')
var.append('SPQPEVP')
var.append('SPQPFALL')
var.append('SPQPFLX')
var.append('SPQPSRC')
var.append('SPQPTR')
var.append('SPQR')
var.append('SPQS')
var.append('SPQTFLX')
var.append('SPQTFLXS')
var.append('SPQTLS')
var.append('SPQTTR')
var.append('SPQVFLUX')
var.append('SPTK')
var.append('SPTKE')
var.append('SPTKES')
var.append('SPTLS')
var.append('SPTVFLUX')
var.append('SPWTKE')

# lev = -71
# lev = -58  # ~ 860 hPa
# lev = -45  # ~ 492 hPa

htype,years,months,first_file,num_files = 'h0',[],[],0,2


#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

zero_cnt = 0
zero_var_list = []

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   # hc.printline()
   # print('  var: '+var[v])

   if 'lev_list' in locals(): lev = lev_list[v]

   for c in range(num_case):
      # print('    case: '+case[c])
      case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub )
      tvar = var[v]
      case_obj.set_coord_names(tvar)
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------      
      # area = case_obj.load_data(case_obj.area_name,htype=htype,years=years,months=months,
      #                                              first_file=first_file,num_files=num_files).astype(np.double)
      data = case_obj.load_data(tvar,              htype=htype,years=years,months=months,
                                                   first_file=first_file,num_files=num_files,lev=lev)

      # if var[v]=='TIMINGF': data = data.where(data!=0,drop=True)

      # # Special handling of CRM grid variables
      # if 'crm_nx' in data.dims : 
      #    data = data.mean(dim=('crm_nx','crm_ny')).isel(crm_nz=15)
      #    print(data)

      #-------------------------------------------------------------------------
      # print the stat info
      #-------------------------------------------------------------------------

      if data.min()==0 and data.mean()==0 and data.max()==0 : 
         # print(var[v])
         zero_var_list.append(var[v])
         zero_cnt += 1

      hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True,fmt='e')

      #-------------------------------------------------------------------------
      # calculate other stuff
      #-------------------------------------------------------------------------

      # # average over time dimension
      # if 'time' in data.dims : 
      #    hc.print_time_length(data.time,indent=' '*6)
      #    data = data.mean(dim='time')
         
      # # Calculate area weighted global mean
      # if 'area' in vars() :
      #    gbl_mean = ( (data*area).sum() / area.sum() ).values 
      #    print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

      # # Calculate RMSE
      # if c==0:baseline = data
      # if c>0:
      #    rmse = (np.sqrt( np.square( data - baseline ).mean(dim=['ncol']) )).values
      #    print(f'      Root Mean Square Error    : {rmse:6.4}')

print(f'\nzero count: {zero_cnt}\n')
for v in zero_var_list: print(f'  {v}')
print()
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
