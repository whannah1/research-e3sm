import vcs, cdms2, sys
import os
home = os.getenv("HOME")

fig_file = home+"/Research/E3SM/figs_3D/3D.vcs.v1.cubed_sphere.png"

grid_file = home+"/Downloads/vcs3D_cubed_sphere_volume_grid.nc"

#-------------------------------------------------------------------------------
x = vcs.init()
f = cdms2.open( home+"/Downloads/vcs3D_cubed_sphere_volume.nc" )   
v = f["RELHUM"] 

dv3d = vcs.get3d_scalar()
dv3d.ToggleVolumePlot      = vcs.on
dv3d.ToggleSphericalProj   = vcs.on
# dv3d.ScaleTransferFunction = [ 78.7, 132.0 ]
dv3d.ScaleTransferFunction = [ 82, 132.0 ]
dv3d.ScaleColormap         = [ 75.4, 100.0 ]
dv3d.ScaleOpacity          = [0.27, 1.0]
dv3d.PointSize             = [5, 2]
# dv3d.VerticalScaling       = [ 1.7 ]
dv3d.VerticalScaling       = [ 1.5 ]

dv3d.Camera = {'Position': (-108.11442471080369,  \
                            -476.65927617219285,  \
                              84.45227482307195), \
               'ViewUp': (0.,0.,1.), \
               'FocalPoint': (0,0,0)}

x.plot( v, dv3d, grid_file=str(grid_file) )
x.png(fig_file)

#-------------------------------------------------------------------------------
print(fig_file)