import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import pg_checkerboard_utilities as pg

# export PYNGL_RANGS=~/.conda/envs/pyn_env/lib/ncarg/database/rangs

case = 'E3SM.HR-OUTPUT.ne30pg2_ne30pg2.F-MMF1.CRMNX_256.CRMDX_500.RADNX_8.BVT.00'
scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'
htype,years,months,first_file,num_files = 'h1',[],[],0,1

fig_type,fig_file = 'png',os.getenv('HOME')+'/Research/E3SM/figs_3D/3D.MMF.v1'

# reg_name,clat,clon,tt = 'FLORIDA', 27, 360-80, 12
reg_name,clat,clon,tt = 'WESTPAC', 1, 148, 21
# reg_name,clat,clon,tt = 'EASTPAC', 4, 360-79, 12
# reg_name,clat,clon,tt = 'GULF', 40, 360-75, 12

sfc_var,crm_var,crm_var2 = 'PRECT','CRM_W','CRM_QCI'
# sfc_var,crm_var = 'PRECT','CRM_QCI'
# sfc_var,crm_var = 'TS','CRM_QC'
# sfc_var,crm_var = 'LHFLX','CRM_QC'
# sfc_var,crm_var = 'TGCLDLWP','CRM_QV'

global_chk = False

num_crm = 12

verbose = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
class tcolor:
  ENDC,RED,GREEN,YELLOW,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[33m','\033[35m','\033[36m'
def run_cmd(cmd):
  if verbose: print('\n' + tcolor.GREEN + cmd + tcolor.ENDC + '\n')
  (msg, err) = sp.Popen(cmd, stdout=sp.PIPE, shell=True, universal_newlines=True).communicate()
  return msg
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
scrip_ds = xr.open_dataset(scrip_file_name)
center_lat = scrip_ds['grid_center_lat'][:].values
center_lon = scrip_ds['grid_center_lon'][:].values
corner_lat = scrip_ds['grid_corner_lat'][:].values
corner_lon = scrip_ds['grid_corner_lon'][:].values

wkres = ngl.Resources()
# npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

idir = os.getenv('HOME')+f'/E3SM/scratch/{case}/run'

ds = xr.open_dataset(f'{idir}/{case}.eam.h1.0001-01-01-00000.nc')

sfc_data = ds[sfc_var][tt,:].values

if sfc_var=='PRECT': sfc_data = sfc_data*86400.*1e3

#-------------------------------------------------------------------------------
# Find CRM indices
#-------------------------------------------------------------------------------
if not global_chk:

  ### locate a number of nearest columns using great circle distances
  cos_dist = np.sin(clat*hc.deg_to_rad)*np.sin(center_lat*hc.deg_to_rad) + \
             np.cos(clat*hc.deg_to_rad)*np.cos(center_lat*hc.deg_to_rad)*np.cos((center_lon-clon)*hc.deg_to_rad)
  cos_dist = np.where( cos_dist> 1.0,  1.0, cos_dist )
  cos_dist = np.where( cos_dist<-1.0, -1.0, cos_dist )
  dist = np.arccos( cos_dist )
  # icol = np.where( dist==np.min(dist) )[0][0]
  ncol_idx = ds['ncol'].sortby( xr.DataArray( dist, coords=[ds['ncol']] ) )[:num_crm]

#---------------------------------------------------------------------------------------------------
# plot map
#---------------------------------------------------------------------------------------------------
# res = hs.res_default()
# res.mpCenterLatF = clat
# res.mpCenterLonF = clon
# res.mpProjection                = "Satellite"
# res.mpPerimOn                   = False
# res.mpGeophysicalLineColor      = 'white'
# res.mpOutlineBoundarySets       = "NoBoundaries"
# res.mpFillOn                    = False

# map_plot = ngl.map(wks,res)

#-------------------------------------------------------------------------------
# plot surface map
#-------------------------------------------------------------------------------

map_res = hs.res_contour_fill_map()
map_res.tmXBOn           = False
map_res.tmYLOn           = False
map_res.tmYROn           = False
map_res.lbLabelBarOn     = False
map_res.mpGridAndLimbOn   = True
map_res.mpGridLatSpacingF = 3.
map_res.mpGridLonSpacingF = 3.
map_res.mpGridLineColor   = 'gray'
map_res.cnFillMode       = "CellFill"
map_res.sfXArray         = scrip_ds.variables['grid_center_lon'].values
map_res.sfYArray         = scrip_ds.variables['grid_center_lat'].values
map_res.sfXCellBounds    = scrip_ds.variables['grid_corner_lon'].values
map_res.sfYCellBounds    = scrip_ds.variables['grid_corner_lat'].values

if not global_chk:
  # map_res.mpDataBaseVersion = "MediumRes"
  map_res.mpDataBaseVersion = "HighRes"
  map_res.mpCenterLatF       = clat - 3
  map_res.mpCenterLonF       = clon + 5
  map_res.mpCenterRotF       = 60.
  map_res.mpProjection       = "Satellite"
  map_res.mpPerimOn          = False
  map_res.mpLimitMode,zoom_angle      = "angles", 30
  map_res.mpLeftAngleF, map_res.mpRightAngleF = zoom_angle,zoom_angle
  map_res.mpTopAngleF, map_res.mpBottomAngleF = zoom_angle,zoom_angle
  # map_res.mpSatelliteAngle1F = 45
  # map_res.mpSatelliteAngle2F = 90
  # map_res.mpSatelliteDistF   = 1.1  # distance in multiples of the earth's radius of the satellite
  map_res.mpSatelliteAngle1F = 60
  map_res.mpSatelliteAngle2F = 90
  map_res.mpSatelliteDistF   = 1.05

data_min = np.min( sfc_data )
data_max = np.max( sfc_data )
aboutZero = False
nlev = 200
if sfc_var in ['LHFLX','SHFLX']: aboutZero = True
clev_tup = ngl.nice_cntr_levels(data_min,data_max,cint=None,max_steps=nlev,returnLevels=True,aboutZero=False)
cmin,cmax,cint,clev = clev_tup
map_res.cnFillPalette = 'BlueWhiteOrangeRed'
# map_res.cnFillPalette = 'MPL_viridis'
# map_res.cnLevelSelectionMode = "ExplicitLevels"
if sfc_var in ['PRECT']:
  map_res.cnFillPalette = 'WhiteBlueGreenYellowRed'
map_res.cnLevels = np.linspace(cmin,cmax,num=nlev)

sfc_plot = ngl.contour_map(wks,sfc_data,map_res)


### draw and finalize
ngl.draw(sfc_plot)

### plot markers for debugging
# gsres = ngl.Resources()
# gsres.gsMarkerIndex = 16
# gsres.gsMarkerColor = 'red'
# gsres.gsMarkerSizeF = 16.0
# for i in range(num_crm):
#   gsres.gsMarkerColor = 'black'
#   if i==0: gsres.gsMarkerColor = 'red'
#   if i==1: gsres.gsMarkerColor = 'green'
#   ndcx, ndcy = ngl.datatondc(sfc_plot, center_lon[ncol_idx[i]], center_lat[ncol_idx[i]] )
#   ngl.polymarker_ndc(wks, ndcx, ndcy, gsres)

ngl.frame(wks)

### identify surface map image size
msg = run_cmd(f'magick identify -format "%W %H" {fig_file}.png ')
sfc_fig_wid, sfc_fig_hgt = int(msg.split()[0]), int(msg.split()[1])


# print(f'  sfc_fig_wid: {sfc_fig_wid}   sfc_fig_hgt: {sfc_fig_hgt}')
# print(f'  ndcx: {ndcx[0]}   ndcy: {ndcy[0]}')
# exit()
  

#-------------------------------------------------------------------------------
# Overlay CRM data
#-------------------------------------------------------------------------------
if not global_chk:
  print()

  ares                = ngl.Resources()
  ares.amZone         = 1

  crm_res = hs.res_contour_fill()
  crm_res.vpHeightF           = 0.10
  crm_res.vpWidthF            = 0.12
  crm_res.nglMaximize         = False
  crm_res.tmXBOn              = False
  crm_res.tmYLOn              = False
  crm_res.tmYROn              = False
  crm_res.lbLabelBarOn        = False
  crm_res.cnConstFEnableFill  = True
  crm_res.cnLevelSelectionMode = "ExplicitLevels"
  # crm_res.cnFillPalette       = 'MPL_viridis'
  # if crm_var=='CRM_W':crm_res.cnFillPalette       = 'BlueWhiteOrangeRed'

  crm_res2 = copy.deepcopy(crm_res)
  crm_res2.cnFillOn         = False
  crm_res2.cnLinesOn        = True
  crm_res2.cnLineLabelsOn   = False
  crm_res2.cnInfoLabelOn    = False
  crm_res2.cnLineThicknessF = 4.

  if crm_var=='CRM_QCI':
    crm_data_global = ds['CRM_QC'].isel(time=tt,crm_ny=0)
    crm_data_global +=ds['CRM_QI'].isel(time=tt,crm_ny=0)
  else:
    crm_data_global = ds[crm_var].isel(time=tt,crm_ny=0)

  crm_data_global.isel(crm_nz=slice(0,40))


  ### set color levels for shading
  crm_data_tmp = crm_data_global.isel(ncol=ncol_idx)
  crm_data_min = np.min( crm_data_tmp.values )
  crm_data_max = np.max( crm_data_tmp.values )
  nlev,aboutZero = 21,False
  if crm_var in ['CRM_W']: aboutZero = True
  if crm_var in ['CRM_W']: 
    std = np.std( crm_data_tmp.values ) * 3 ; data_min,data_max = -1*std,std
  clev_tup = ngl.nice_cntr_levels(data_min,data_max,cint=None,max_steps=nlev,returnLevels=True,aboutZero=False)
  cmin,cmax,cint,clev = clev_tup
  crm_res.cnLevels = np.linspace(cmin,cmax,num=nlev)
  crm_res.cnFillPalette = 'MPL_viridis'
  if crm_var in ['CRM_W']: crm_res.cnFillPalette = 'BlueWhiteOrangeRed'
  

  ### calculate distance to scale CRM image and opacity
  clat_perspective = map_res.mpCenterLatF # clat
  clon_perspective = map_res.mpCenterLonF # clon
  dist = np.zeros(num_crm)
  for i in range(num_crm):
    dist[i] = hc.calc_great_circle_distance( clat_perspective, center_lat[ncol_idx[i]] , 
                                             clon_perspective, center_lon[ncol_idx[i]] )#*180/3.14159
  scale_factor =  np.min(dist) / dist
  # for i in range(num_crm): print(f'  dist: {dist[i]:10.5f}    scale_factor: {scale_factor[i]:10.5f} ')

  anno = [None]*num_crm
  for i in range(num_crm):

    crm_fig_file = fig_file+f'.tmp.crm_{i:03d}'
    crm_wks = ngl.open_wks('png',crm_fig_file)

    ### get NDC coordinates of this column
    ndcx, ndcy = ngl.datatondc(sfc_plot, center_lon[ncol_idx[i]], center_lat[ncol_idx[i]] )
    if str(ndcx)!='[--]':

      #------------------------------------------------------
      # get NDC coordinates of north and south edges
      #------------------------------------------------------
      corner_lats = corner_lat[ncol_idx[i]]
      corner_lons = corner_lon[ncol_idx[i]]
      edge_lats,edge_lons = np.empty(4),np.empty(4)
      edge_bear = np.empty(4)
      for e in range(4):
        c1,c2 = e,(e+1)%4
        tlon1,tlon2 = corner_lons[c1],corner_lons[c2]
        if tlon1>360: tlon1 = 360-tlon1
        if tlon2>360: tlon2 = 360-tlon2
        edge_lats[e] = np.average([corner_lats[c1],corner_lats[c2]])
        edge_lons[e] = np.average([tlon1,tlon2])
        edge_bear[e] = pg.calc_great_circle_bearing(center_lat[ncol_idx[i]],edge_lats[e],
                                                    center_lon[ncol_idx[i]],edge_lons[e])
      
      edge_bear_360 = np.where(edge_bear<0,edge_bear+360,edge_bear)

      # find northernmost and southernmost edges
      north_bear_dist = np.absolute(edge_bear)
      south_bear_dist = np.absolute(edge_bear_360 - 180)
      for e in range(4):
        if north_bear_dist[e]==np.min(north_bear_dist): north_edge_ind = e
        if south_bear_dist[e]==np.min(south_bear_dist): south_edge_ind = e

      ### get NDC coordinates of this column
      north_edge_ndcx, north_edge_ndcy = ngl.datatondc(sfc_plot, edge_lons[north_edge_ind], edge_lats[north_edge_ind] )
      south_edge_ndcx, south_edge_ndcy = ngl.datatondc(sfc_plot, edge_lons[south_edge_ind], edge_lats[south_edge_ind] )

      ### find slope between northernmost and southernmost edges
      cell_slope = np.abs( north_edge_ndcy[0] - south_edge_ndcy[0] ) / np.abs( north_edge_ndcx[0] - south_edge_ndcx[0] )

      #------------------------------------------------------
      #------------------------------------------------------

      if verbose: 
        # print(f'  lat,lon: {center_lat[ncol_idx[i]]:6.2f} , {center_lon[ncol_idx[i]]:6.2f}   ndcx: {ndcx[0]:6.4f}  ndcy: {ndcy[0]:6.4f}')
        print(f'  lat,lon: {center_lat[ncol_idx[i]]:6.2f} , {center_lon[ncol_idx[i]]:6.2f}')
        print(f'  center ndcx, ndcy: {           ndcx[0]*sfc_fig_wid:8.2f}  {           ndcy[0]*sfc_fig_hgt:8.2f}')
        print(f'  N edge ndcx, ndcy: {north_edge_ndcx[0]*sfc_fig_wid:8.2f}  {north_edge_ndcy[0]*sfc_fig_hgt:8.2f}')
        print(f'  S edge ndcx, ndcy: {south_edge_ndcx[0]*sfc_fig_wid:8.2f}  {south_edge_ndcy[0]*sfc_fig_hgt:8.2f}')
        print(f'  cell_slope: {cell_slope}')
        print()

      # exit()

      #------------------------------------------------------
      # old method
      #------------------------------------------------------

      ### load CRM data for this column and create a plot
      # crm_data = crm_data_global.isel(ncol=ncol_idx[i]).copy()
      # crm_plot = ngl.contour(wks,crm_data,crm_res)

      ### attach plot via annotation
      # ares.amOrthogonalPosF,ares.amParallelPosF  = ndcy, ndcx
      # anno[i] = ngl.add_annotation(sfc_plot, crm_plot ,ares)

      #------------------------------------------------------
      #------------------------------------------------------

      ### load CRM data for this column and create a plot - save to file
      scale_scale_fac = 3
      crm_res.cnFillOpacityF = ( scale_factor[i] + scale_scale_fac - 1 ) / scale_scale_fac
      crm_data = crm_data_global.isel(ncol=ncol_idx[i]).copy()
      crm_plot = ngl.contour(crm_wks,crm_data,crm_res)


      ### add contoursc
      if 'crm_var2' in locals():
        if crm_var2=='CRM_QCI':
          crm_data_global_2 = ds['CRM_QC'].isel(time=tt,crm_ny=0)
          crm_data_global_2 +=ds['CRM_QI'].isel(time=tt,crm_ny=0)
        else:
          crm_data_global_2 = ds[crm_var2].isel(time=tt,crm_ny=0)
        crm_data_global_2.isel(crm_nz=slice(0,40))
        crm_data2 = crm_data_global_2.isel(ncol=ncol_idx[i]).copy()
        if crm_var2=='CRM_QCI': crm_res2.cnLevels = np.arange(5,50,5)*1e-4
        crm_res2.cnLineColor = 'green'
        ngl.overlay( crm_plot, ngl.contour(crm_wks,crm_data2,crm_res2) )

      ngl.draw(crm_plot); ngl.frame(crm_wks); hc.trim_png(crm_fig_file,verbose=False)
      ngl.destroy(crm_wks)

      ### identify CRM image size
      msg = run_cmd(f'magick identify -format "%W %H" {crm_fig_file}.png ')
      crm_fig_wid, crm_fig_hgt = int(msg.split()[0]), int(msg.split()[1])

      #------------------------------------------------------
      #------------------------------------------------------

      # ### original and new coordinate positions of the edges for distortion
      # xul=0           ; dxul=xul
      # yul=0           ; dyul=yul + crm_fig_hgt*0.50
      # xur=crm_fig_wid ; dxur=xur - crm_fig_wid*0.40
      # yur=0           ; dyur=yur
      # xlr=crm_fig_wid ; dxlr=xlr - crm_fig_wid*0.40
      # ylr=crm_fig_hgt ; dylr=ylr - crm_fig_hgt*0.50
      # xll=0           ; dxll=xll
      # yll=crm_fig_hgt ; dyll=yll #+ crm_fig_hgt*0.20

      ### original and new coordinate positions of the edges for distortion      
      xul=0           ; dxul=xul
      yul=0           ; dyul=yul + crm_fig_wid*cell_slope
      xur=crm_fig_wid ; dxur=xur #- crm_fig_wid*0.40
      yur=0           ; dyur=yur
      xlr=crm_fig_wid ; dxlr=xlr #- crm_fig_wid*0.40
      ylr=crm_fig_hgt ; dylr=ylr - crm_fig_wid*cell_slope
      xll=0           ; dxll=xll
      yll=crm_fig_hgt ; dyll=yll #+ crm_fig_hgt*0.20

      # shrink "far" side of CRM for perspective
      dyur=dyur + crm_fig_hgt*0.01
      dylr=dylr - crm_fig_hgt*0.01


      ### use imagemagick to distort the image
      cmd = f'convert {crm_fig_file}.png -matte  -virtual-pixel transparent -distort Perspective '
      cmd += f' "{xul},{yul} {dxul},{dyul} {xur},{yur} {dxur},{dyur}'
      cmd += f'  {xlr},{ylr} {dxlr},{dylr} {xll},{yll} {dxll},{dyll}"'
      cmd += f' {crm_fig_file}.png'
      run_cmd(cmd)
      # sp.check_call(cmd,shell=True)

      ### identify CRM image size
      msg = run_cmd(f'magick identify -format "%W %H" {crm_fig_file}.png ')
      new_crm_fig_wid, new_crm_fig_hgt = int(msg.split()[0]), int(msg.split()[1])

      # ### use imagemagick to scale the image
      # scale_scale_fac = 1
      # scl_crm_fig_wid = new_crm_fig_wid * ( scale_factor[i] + scale_scale_fac - 1 ) / scale_scale_fac
      # scl_crm_fig_hgt = new_crm_fig_hgt * ( scale_factor[i] + scale_scale_fac - 1 ) / scale_scale_fac
      # run_cmd(f'convert {crm_fig_file}.png -resize {scl_crm_fig_wid}x{scl_crm_fig_hgt} {crm_fig_file}.png')

      # ### identify CRM image size (again)
      # msg = run_cmd(f'magick identify -format "%W %H" {crm_fig_file}.png ')
      # new_crm_fig_wid, new_crm_fig_hgt = int(msg.split()[0]), int(msg.split()[1])

      ### scale the CRM image by distance when combining
      scale_scale_fac = 1
      new_crm_fig_wid = new_crm_fig_wid * ( scale_factor[i] + scale_scale_fac - 1 ) / scale_scale_fac
      new_crm_fig_hgt = new_crm_fig_hgt * ( scale_factor[i] + scale_scale_fac - 1 ) / scale_scale_fac

      ### use imagemagick to attach plot to map in correct location
      ### Note: imagemagick origin is upper-left whereas NDC origin is lower-left
      offsetx =               int(ndcx*sfc_fig_wid) - int(new_crm_fig_wid/2) + int(new_crm_fig_wid*0.1)
      offsety = sfc_fig_hgt - int(ndcy*sfc_fig_hgt) - int(new_crm_fig_hgt/2) - int(new_crm_fig_hgt*0.1)
      cmd = f'composite'
      cmd+= f' -geometry {new_crm_fig_wid}x{new_crm_fig_hgt}+{offsetx}+{offsety}'
      cmd+= f' {crm_fig_file}.png {fig_file}.png {fig_file}.png'
      run_cmd(cmd)
      # sp.check_call(cmd,shell=True)

  # ngl.draw(sfc_plot)

  ### plot markers for debugging
  # gsres = ngl.Resources()
  # gsres.gsMarkerIndex = 16
  # gsres.gsMarkerColor = 'red'
  # gsres.gsMarkerSizeF = 4.0
  # # for i in range(num_crm):
  # for i in range(1):
  #   ndcx, ndcy = ngl.datatondc(sfc_plot, center_lon[ncol_idx[i]], center_lat[ncol_idx[i]] )
  #   ngl.polymarker_ndc(wks, ndcx, ndcy, gsres)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


# ngl.draw(sfc_plot)
# ngl.draw(crm_plot)
# ngl.frame(wks)

ngl.end()

hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------