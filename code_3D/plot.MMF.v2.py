import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import pg_checkerboard_utilities as pg

# export PYNGL_RANGS=~/.conda/envs/pyn_env/lib/ncarg/database/rangs

fig_type,fig_file = 'png',os.getenv('HOME')+'/Research/E3SM/figs_3D/3D.MMF.v2'


scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'

# case = 'E3SM.HR-OUTPUT.ne30pg2_ne30pg2.F-MMF1.CRMNX_256.CRMDX_500.RADNX_8.BVT.00'
# reg_name,clat,clon,date,tt = 'MADAGASCAR', -28, 50, '0001-01-08',2
# reg_name,clat,clon,date,tt = 'PANAMA', 3, 360-78, '0001-01-14', 2
# reg_name,clat,clon,date,tt = 'WESTPAC', 0, 147, '0001-01-20', 2
# reg_name,clat,clon,date,tt = 'EASTPAC', 4, 360-79, '0001-01-08', 12
# reg_name,clat,clon,date,tt = 'GULF', 40, 360-75, '0001-01-08', 12
# reg_name,clat,clon,date,tt = 'FLORIDA', 25, 360-76, '0001-01-09', 2  
# reg_name,clat,clon,date,tt = 'FLORIDA', 27, 360-80.5, '0001-01-18', 0

case = 'E3SM.HR-OUTPUT.ne30pg2_ne30pg2.F-MMF1.CRMNX_256.CRMDX_500.RADNX_8.BVT.01'
# reg_name,clat,clon,date,tt = 'FLORIDA', 27, 360-80.5, '0001-07-12', 2
reg_name,clat,clon,date,tt = 'GULF', 30, 360-89, '0001-07-05', 3

# sfc_var,crm_var,crm_var2 = 'PRECT','CRM_W','CRM_QCI'
sfc_var,crm_var = 'PRECT','CRM_QCI'
# sfc_var,crm_var = 'TS','CRM_QC'
# sfc_var,crm_var = 'LHFLX','CRM_QC'
# sfc_var,crm_var = 'TGCLDLWP','CRM_QV'

global_chk = False

num_crm = 20  ### Note that we are using a precip threshold

verbose = False

hi_res_land = True

# slat = clat - 3
# slon = clon + 5

# slat = clat - 5
# slon = clon + 8

slat = clat - 7
slon = clon + 6

npix = 4096
# npix = 2048
# npix = 1024

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
class tcolor:
  ENDC,RED,GREEN,YELLOW,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[33m','\033[35m','\033[36m'
def run_cmd(cmd,verbose=verbose):
  if verbose: print('\n' + tcolor.GREEN + cmd + tcolor.ENDC + '\n')
  (msg, err) = sp.Popen(cmd, stdout=sp.PIPE, stderr=sp.PIPE, shell=True, universal_newlines=True).communicate()
  if verbose:
    print(f'  msg: {msg}')
    print(f'  err: {err}')
  if err!="":
    if not verbose: print('\n' + tcolor.GREEN + cmd + tcolor.ENDC + '\n')
    print(f'err: {err}')
    exit()
  return msg
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
scrip_ds = xr.open_dataset(scrip_file_name)
center_lat = scrip_ds['grid_center_lat'][:].values
center_lon = scrip_ds['grid_center_lon'][:].values
corner_lat = scrip_ds['grid_corner_lat'][:].values
corner_lon = scrip_ds['grid_corner_lon'][:].values

wkres = ngl.Resources()
wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

idir = os.getenv('HOME')+f'/E3SM/scratch/{case}/run'

ds = xr.open_dataset(f'{idir}/{case}.eam.h1.{date}-00000.nc')

sfc_data = ds[sfc_var][tt,:].values

if sfc_var=='PRECT': sfc_data = sfc_data*86400.*1e3

#-------------------------------------------------------------------------------
# Find CRM indices
#-------------------------------------------------------------------------------
if not global_chk:

  ### locate a number of nearest columns using great circle distance from center point
  cos_dist = np.sin(clat*hc.deg_to_rad)*np.sin(center_lat*hc.deg_to_rad) + \
             np.cos(clat*hc.deg_to_rad)*np.cos(center_lat*hc.deg_to_rad)*np.cos((center_lon-clon)*hc.deg_to_rad)
  cos_dist = np.where( cos_dist> 1.0,  1.0, cos_dist )
  cos_dist = np.where( cos_dist<-1.0, -1.0, cos_dist )
  cdist = np.arccos( cos_dist )
  # icol = np.where( dist==np.min(dist) )[0][0]

  ncol_idx = ds['ncol'].sortby( xr.DataArray( cdist, coords=[ds['ncol']] ) )[:num_crm]

  ### calculate distance from satellite view location
  cos_dist = np.sin(slat*hc.deg_to_rad)*np.sin(center_lat*hc.deg_to_rad) + \
             np.cos(slat*hc.deg_to_rad)*np.cos(center_lat*hc.deg_to_rad)*np.cos((center_lon-slon)*hc.deg_to_rad)
  cos_dist = np.where( cos_dist> 1.0,  1.0, cos_dist )
  cos_dist = np.where( cos_dist<-1.0, -1.0, cos_dist )
  sdist = np.arccos( cos_dist )
  
  ### resort column indices by distance from satellite to avoid overlap issues
  cdist = cdist[ncol_idx.values]
  sdist = sdist[ncol_idx.values]
  sat_dist_sort_idx = np.argsort(sdist)[::-1]
  dist     = cdist[sat_dist_sort_idx]
  sdist    = sdist[sat_dist_sort_idx]
  ncol_idx = ncol_idx[sat_dist_sort_idx]

  ### sort distance by satellite distance  
  # print('dist:')
  # for n in range(len(ncol_idx.values)): print(f'  {cdist[n]:5.3f}  {sdist[n]:5.3f}  {dist[n]:5.3f}')
  # exit()


#-------------------------------------------------------------------------------
# plot surface map
#-------------------------------------------------------------------------------

map_res = hs.res_contour_fill_map()
# map_res.vpHeightF        = 0.1
# map_res.vpWidthF         = 0.6
map_res.tmXBOn           = False
map_res.tmYLOn           = False
map_res.tmYROn           = False
map_res.lbLabelBarOn     = False
map_res.cnFillMode       = "CellFill"
map_res.sfXArray         = scrip_ds.variables['grid_center_lon'].values
map_res.sfYArray         = scrip_ds.variables['grid_center_lat'].values
map_res.sfXCellBounds    = scrip_ds.variables['grid_corner_lon'].values
map_res.sfYCellBounds    = scrip_ds.variables['grid_corner_lat'].values



if not global_chk:
  map_res.mpGridAndLimbOn   = True
  map_res.mpGridLatSpacingF = 3.
  map_res.mpGridLonSpacingF = 3.
  map_res.mpGridLineColor   = 'gray'
  if hi_res_land:
    map_res.mpDataBaseVersion = "HighRes"
  else:
    map_res.mpDataBaseVersion = "MediumRes"
  map_res.mpCenterLatF       = slat
  map_res.mpCenterLonF       = slon
  map_res.mpCenterRotF       = 60.
  map_res.mpProjection       = "Satellite"
  map_res.mpPerimOn          = False
  map_res.mpLimitMode,zoom_angle      = "angles", 30
  map_res.mpLeftAngleF, map_res.mpRightAngleF = zoom_angle,zoom_angle
  map_res.mpTopAngleF, map_res.mpBottomAngleF = zoom_angle,zoom_angle
  # map_res.mpSatelliteAngle1F = 60
  # map_res.mpSatelliteAngle2F = 90
  # map_res.mpSatelliteDistF   = 1.05

  map_res.mpCenterRotF       = 30.
  zoom_angle = 30; map_res.mpLeftAngleF, map_res.mpRightAngleF = zoom_angle,zoom_angle
  zoom_angle = 20; map_res.mpTopAngleF, map_res.mpBottomAngleF = zoom_angle,zoom_angle
  map_res.mpSatelliteAngle1F = 56
  map_res.mpSatelliteAngle2F = 90
  map_res.mpSatelliteDistF   = 1.08

data_min = np.min( sfc_data )
data_max = np.max( sfc_data )
aboutZero = False
nlev = 200
if sfc_var in ['LHFLX','SHFLX']: aboutZero = True
clev_tup = ngl.nice_cntr_levels(data_min,data_max,cint=None,max_steps=nlev,returnLevels=True,aboutZero=False)
cmin,cmax,cint,clev = clev_tup
map_res.cnFillPalette = 'BlueWhiteOrangeRed'
if sfc_var in ['PRECT']:
  map_res.cnFillPalette = 'WhiteBlueGreenYellowRed'
  rgba = ngl.read_colormap_file(map_res.cnFillPalette)
  num_trans_clr = 10
  for c in range(num_trans_clr): rgba[c][3] = 0.5+(c/num_trans_clr)*0.5
  map_res.cnFillPalette = rgba
  # map_res.cnFillPalette = cmap
map_res.cnLevelSelectionMode = "ExplicitLevels"
map_res.cnLevels = np.linspace(cmin,cmax,num=nlev)

map_res.mpFillOn     = True           # Turn on map fill.
# ic = ngl.new_color(wks,0.75,0.75,0.75) # gray
# ic = ngl.new_color(wks,179/256, 160/256, 66/256) # brown-ish
gc=220;ic = ngl.new_color(wks,gc/256, gc/256, gc/256) # brown-ish
map_res.mpFillColors = [0,-1,ic,-1]   # Fill land and leave oceans and inland water transparent

sfc_plot = ngl.contour_map(wks,sfc_data,map_res)


### draw and finalize
ngl.draw(sfc_plot)

### plot markers for debugging
# gsres = ngl.Resources()
# gsres.gsMarkerIndex = 16
# gsres.gsMarkerColor = 'red'
# gsres.gsMarkerSizeF = 16.0
# for i in range(num_crm):
#   gsres.gsMarkerColor = 'black'
#   if i==0: gsres.gsMarkerColor = 'red'
#   if i==1: gsres.gsMarkerColor = 'green'
#   ndcx, ndcy = ngl.datatondc(sfc_plot, center_lon[ncol_idx[i]], center_lat[ncol_idx[i]] )
#   ngl.polymarker_ndc(wks, ndcx, ndcy, gsres)

ngl.frame(wks)

# print(f'  {fig_file}.png')
# exit()

### identify surface map image size
msg = run_cmd(f'magick identify -format "%W %H" {fig_file}.png ')
sfc_fig_wid, sfc_fig_hgt = int(msg.split()[0]), int(msg.split()[1])


# print(f'  sfc_fig_wid: {sfc_fig_wid}   sfc_fig_hgt: {sfc_fig_hgt}')
# print(f'  ndcx: {ndcx[0]}   ndcy: {ndcy[0]}')
# exit()
  

#-------------------------------------------------------------------------------
# Overlay CRM data
#-------------------------------------------------------------------------------
if not global_chk:
  print()

  ares                = ngl.Resources()
  ares.amZone         = 1

  crm_res = hs.res_contour_fill()
  # crm_res.vpHeightF           = 0.5
  # crm_res.vpWidthF            = 0.6
  # crm_res.vpHeightF           = 0.10
  # crm_res.vpWidthF            = 0.12
  crm_res.nglMaximize         = False
  crm_res.tmXBOn              = False
  crm_res.tmYLOn              = False
  crm_res.tmYROn              = False
  crm_res.lbLabelBarOn        = False
  crm_res.cnConstFEnableFill  = True
  crm_res.cnLevelSelectionMode = "ExplicitLevels"
  # crm_res.cnFillPalette       = 'MPL_viridis'
  # if crm_var=='CRM_W':crm_res.cnFillPalette       = 'BlueWhiteOrangeRed'

  crm_res2 = copy.deepcopy(crm_res)
  crm_res2.cnFillOn         = False
  crm_res2.cnLinesOn        = True
  crm_res2.cnLineLabelsOn   = False
  crm_res2.cnInfoLabelOn    = False
  crm_res2.cnLineThicknessF = 4.

  if crm_var=='CRM_QCI':
    crm_data_global = ds['CRM_QC'].isel(time=tt,crm_ny=0)
    crm_data_global +=ds['CRM_QI'].isel(time=tt,crm_ny=0)
  else:
    crm_data_global = ds[crm_var].isel(time=tt,crm_ny=0)

  crm_data_global.isel(crm_nz=slice(0,40))

  ### set color levels for shading
  crm_data_tmp = crm_data_global.isel(ncol=ncol_idx)

  crm_data_min = np.min( crm_data_tmp.values )
  crm_data_max = np.max( crm_data_tmp.values )
  nlev,aboutZero = 21,False
  if crm_var in ['CRM_W']: aboutZero = True
  if crm_var in ['CRM_W']: std = np.std( crm_data_tmp.values ) * 3 ; data_min,data_max = -1*std,std
  clev_tup = ngl.nice_cntr_levels(crm_data_min,crm_data_max,cint=None,max_steps=nlev,returnLevels=True,aboutZero=False)
  cmin,cmax,cint,clev = clev_tup
  crm_res.cnLevels = np.linspace(cmin,cmax,num=nlev)
  crm_res.cnFillPalette = 'MPL_viridis'
  if crm_var in ['CRM_W']: crm_res.cnFillPalette = 'BlueWhiteOrangeRed'

  ### define distance scaling for better perspective
  # clat_perspective = slat # map_res.mpCenterLatF # clat
  # clon_perspective = slon # map_res.mpCenterLonF # clon
  # dist = np.zeros(num_crm)
  # for i in range(num_crm):
  #   dist[i] = hc.calc_great_circle_distance( clat_perspective, center_lat[ncol_idx[i]] , 
  #                                            clon_perspective, center_lon[ncol_idx[i]] )#*180/3.14159
  # scale_factor =  np.min(sdist) / sdist
  min_scale = 0.7
  scale_factor =  1. - sdist / np.max(sdist) * min_scale
  # for i in range(num_crm): print(f'  sdist: {sdist[i]:10.5f}    scale_factor: {scale_factor[i]:10.5f} ')

  dist_s_to_c = hc.calc_great_circle_distance( slat, clat , slon, clon )
  
  anno = [None]*num_crm
  for i in range(num_crm):

    if (sfc_data[ncol_idx[i]]) < 3.0: 
      if verbose: print('  skipping due to small precip')
      continue

    crm_fig_file = fig_file+f'.tmp.crm_{i:03d}'
    crm_wks = ngl.open_wks('png',crm_fig_file,wkres)

    

    ### get NDC coordinates of this column
    ndcx, ndcy = ngl.datatondc(sfc_plot, center_lon[ncol_idx[i]], center_lat[ncol_idx[i]] )
    # if str(ndcx)!='[--]' and str(ndcy)!='[--]':
    if str(ndcx)=='[--]' and str(ndcy)=='[--]': 
      ngl.destroy(crm_wks)
    else:
      #------------------------------------------------------
      # get NDC coordinates of north and south edges
      #------------------------------------------------------
      corner_lats = corner_lat[ncol_idx[i]]
      corner_lons = corner_lon[ncol_idx[i]]
      edge_lats,edge_lons = np.empty(4),np.empty(4)
      edge_bear = np.empty(4)
      for e in range(4):
        c1,c2 = e,(e+1)%4
        tlon1,tlon2 = corner_lons[c1],corner_lons[c2]
        if tlon1>360: tlon1 = 360-tlon1
        if tlon2>360: tlon2 = 360-tlon2
        edge_lats[e] = np.average([corner_lats[c1],corner_lats[c2]])
        edge_lons[e] = np.average([tlon1,tlon2])
        edge_bear[e] = pg.calc_great_circle_bearing(center_lat[ncol_idx[i]],edge_lats[e],
                                                    center_lon[ncol_idx[i]],edge_lons[e])
      
      edge_bear_360 = np.where(edge_bear<0,edge_bear+360,edge_bear)

      # find northernmost and southernmost edges
      north_bear_dist = np.absolute(edge_bear)
      south_bear_dist = np.absolute(edge_bear_360 - 180)
      for e in range(4):
        if north_bear_dist[e]==np.min(north_bear_dist): north_edge_ind = e
        if south_bear_dist[e]==np.min(south_bear_dist): south_edge_ind = e

      ### get NDC coordinates of this column
      north_edge_ndcx, north_edge_ndcy = ngl.datatondc(sfc_plot, edge_lons[north_edge_ind], edge_lats[north_edge_ind] )
      south_edge_ndcx, south_edge_ndcy = ngl.datatondc(sfc_plot, edge_lons[south_edge_ind], edge_lats[south_edge_ind] )

      if str(north_edge_ndcx)=='[--]' \
      or str(south_edge_ndcx)=='[--]' \
      or str(north_edge_ndcy)=='[--]' \
      or str(south_edge_ndcy)=='[--]' :
        if verbose: print('  skipping due to edge issue')
        ngl.destroy(crm_wks)
        continue

      ### find slope between northernmost and southernmost edges
      cell_slope = np.abs( north_edge_ndcy[0] - south_edge_ndcy[0] ) / np.abs( north_edge_ndcx[0] - south_edge_ndcx[0] )

      #------------------------------------------------------
      # Print stuff
      #------------------------------------------------------
      if verbose: 
        # print(f'  lat,lon: {center_lat[ncol_idx[i]]:6.2f} , {center_lon[ncol_idx[i]]:6.2f}   ndcx: {ndcx[0]:6.4f}  ndcy: {ndcy[0]:6.4f}')
        print(f'  lat,lon: {center_lat[ncol_idx[i]]:6.2f} , {center_lon[ncol_idx[i]]:6.2f}')
        print(f'  center ndcx, ndcy: {           ndcx[0]*sfc_fig_wid:8.2f}  {           ndcy[0]*sfc_fig_hgt:8.2f}')
        print(f'  N edge ndcx, ndcy: {north_edge_ndcx[0]*sfc_fig_wid:8.2f}  {north_edge_ndcy[0]*sfc_fig_hgt:8.2f}')
        print(f'  S edge ndcx, ndcy: {south_edge_ndcx[0]*sfc_fig_wid:8.2f}  {south_edge_ndcy[0]*sfc_fig_hgt:8.2f}')
        print(f'  cell_slope: {cell_slope}')
        print()

      #------------------------------------------------------
      #------------------------------------------------------

      ### load CRM data for this column and create a plot - save to file
      scale_scale_fac = 3 # make this smaller for more dramatic effect
      crm_res.cnFillOpacityF = ( scale_factor[i] + scale_scale_fac - 1 ) / scale_scale_fac
      crm_data = crm_data_global.isel(ncol=ncol_idx[i]).copy()
      crm_plot = ngl.contour(crm_wks,crm_data,crm_res)


      ### add contours
      # if 'crm_var2' in locals():
      #   if crm_var2=='CRM_QCI':
      #     crm_data_global_2 = ds['CRM_QC'].isel(time=tt,crm_ny=0)
      #     crm_data_global_2 +=ds['CRM_QI'].isel(time=tt,crm_ny=0)
      #   else:
      #     crm_data_global_2 = ds[crm_var2].isel(time=tt,crm_ny=0)
      #   crm_data_global_2.isel(crm_nz=slice(0,40))
      #   crm_data2 = crm_data_global_2.isel(ncol=ncol_idx[i]).copy()
      #   if crm_var2=='CRM_QCI': crm_res2.cnLevels = np.arange(5,50,5)*1e-4
      #   crm_res2.cnLineColor = 'green'
      #   ngl.overlay( crm_plot, ngl.contour(crm_wks,crm_data2,crm_res2) )

      ngl.draw(crm_plot); ngl.frame(crm_wks)
      hc.trim_png(crm_fig_file,verbose=False)
      ngl.destroy(crm_wks)

      ### identify CRM image size
      msg = run_cmd(f'magick identify -format "%W %H" {crm_fig_file}.png ')
      crm_fig_wid, crm_fig_hgt = int(msg.split()[0]), int(msg.split()[1])

      #------------------------------------------------------
      # resize the CRM image to match the base image
      #------------------------------------------------------
      # run_cmd(f'convert {crm_fig_file}.png -resize {sfc_fig_wid}x{sfc_fig_hgt}\\! {crm_fig_file}.png')
      run_cmd(f'convert {crm_fig_file}.png -resize {sfc_fig_wid}x{sfc_fig_hgt} {crm_fig_file}.png')

      ### identify resized CRM image size (should match surface image)
      msg = run_cmd(f'magick identify -format "%W %H" {crm_fig_file}.png ')
      crm_fig_wid, crm_fig_hgt = int(msg.split()[0]), int(msg.split()[1])

      #------------------------------------------------------
      # Distort the CRM image to place on map - do not crop!
      #------------------------------------------------------

      ### place lower corners according to pg2 north and south  grid cell edges
      xlr=crm_fig_wid ; dxlr=              north_edge_ndcx[0]*sfc_fig_wid
      ylr=crm_fig_hgt ; dylr=sfc_fig_hgt - north_edge_ndcy[0]*sfc_fig_hgt
      xll=0           ; dxll=              south_edge_ndcx[0]*sfc_fig_wid
      yll=crm_fig_hgt ; dyll=sfc_fig_hgt - south_edge_ndcy[0]*sfc_fig_hgt
      
      ### pull CRM edges away from pg2 cell edges
      new_crm_wid = np.sqrt( np.power(dxlr-dxll,2) + np.power(dylr-dyll,2) )
      dxlr=dxlr - new_crm_wid*0.08
      dylr=dylr + new_crm_wid*0.08*cell_slope
      dxll=dxll + new_crm_wid*0.08
      dyll=dyll - new_crm_wid*0.08*cell_slope

      ### place upper corners based on lower corners - scaled by new width in pixels
      new_crm_wid = np.sqrt( np.power(dxlr-dxll,2) + np.power(dylr-dyll,2) )
      new_crm_hgt = new_crm_wid*0.50
      scale_scale_fac = 5 # make this smaller for more dramatic effect
      new_crm_hgt = new_crm_wid*0.6*( scale_factor[i] + scale_scale_fac - 1 ) / scale_scale_fac
      xul=0           ; dxul=dxll
      yul=0           ; dyul=dyll-new_crm_hgt
      xur=crm_fig_wid ; dxur=dxlr
      yur=0           ; dyur=dylr-new_crm_hgt*0.85

      # if i==0: first_crm_wid = new_crm_wid

      ### find slope between northernmost and southernmost edges
      cell_slope = np.abs( north_edge_ndcy[0] - south_edge_ndcy[0] ) / np.abs( north_edge_ndcx[0] - south_edge_ndcx[0] )

      ### place upper corners based on lower corners scaled by distance from satellite
      # crm_dist_north_edge = hc.calc_great_circle_distance( slat, edge_lats[north_edge_ind] , 
      #                                                      slon, edge_lons[north_edge_ind] )
      # crm_dist_south_edge = hc.calc_great_circle_distance( slat, edge_lats[south_edge_ind] , 
      #                                                      slon, edge_lons[south_edge_ind] )
      # # max_scale,min_scale = 1.0,0.2
      # # scale_factor =  max_scale - sdist / np.max(sdist) * min_scale
      # # scale_ul = max_scale * ( 1. - crm_dist_south_edge / np.max(sdist) * min_scale )
      # # scale_ur = max_scale * ( 1. - crm_dist_north_edge / np.max(sdist) * min_scale )
      # scale_ul = 1. - 0.2 * crm_dist_south_edge / ( dist_s_to_c )
      # scale_ur = 1. - 0.2 * crm_dist_north_edge / ( dist_s_to_c )
      # xul=0           ; dxul=dxll
      # xur=crm_fig_wid ; dxur=dxlr
      # yul=0           ; dyul=dyll - 200 * scale_ul
      # yur=0           ; dyur=dylr - 200 * scale_ur
      # diff_ul  = dyul-dyll
      # diff_ur  = dyur-dylr
      # print(f'  scale_ul: {scale_ul:8.4f}    scale_ur: {scale_ur:8.4f}   diff_ul: {diff_ul:8.4f}   diff_ur: {diff_ur:8.4f}')


      ### skip current column if it's going to spill over the edges
      for dx in [ dxul, dxur, dxlr, dxll ] :
        if dx<sfc_fig_wid*0.05 or dx>sfc_fig_wid*0.95 : 
          if verbose: print(f' skipping i = {i}')
          continue
      for dy in [ dyul, dyur, dylr, dyll ] :
        if dy<sfc_fig_hgt*0.05 or dy>sfc_fig_hgt*0.95 : 
          if verbose: print(f' skipping i = {i}')
          continue
        

      ### modify the image
      cmd  = f'convert {crm_fig_file}.png   -virtual-pixel transparent '
      cmd += ' -distort Perspective '
      cmd += f' "{xul},{yul} {dxul},{dyul} {xur},{yur} {dxur},{dyur}'
      cmd += f'  {xlr},{ylr} {dxlr},{dylr} {xll},{yll} {dxll},{dyll}"'
      cmd += f' {crm_fig_file}.png'
      run_cmd(cmd)

      ### the distortion above creates a gray bar, so replace that color with transparency
      # for gray_val in range(181,190):
      # for gray_val_ctr in [189,109,249]:
      #   for gray_val in range(gray_val_ctr-5,gray_val_ctr+5):
      #     cmd = f'convert {crm_fig_file}.png -fuzz 10% '
      #     cmd+=f' -fill transparent'
      #     cmd+=f' -opaque \'rgb({gray_val},{gray_val},{gray_val})\' '
      #     cmd+=f' {crm_fig_file}.png'
      #     run_cmd(cmd,verbose=True)

      gray_val=189
      cmd = f'convert {crm_fig_file}.png -fuzz 0% -alpha off '
      cmd+=f' -fill transparent -opaque \'rgb({gray_val},{gray_val},{gray_val})\' '
      cmd+=f' {crm_fig_file}.png'
      run_cmd(cmd)

      cmd = f'convert {crm_fig_file}.png -fuzz 0% -alpha off '
      cmd+=f' -fill transparent -opaque black '
      cmd+=f' {crm_fig_file}.png'
      run_cmd(cmd)

      # cmd = f'convert {crm_fig_file}.png -fuzz 0% '
      # cmd+=f' -fill transparent -opaque \'rgba(189,189,189,0.0941176)\' '
      # cmd+=f' {crm_fig_file}.png'
      # run_cmd(cmd)

      # 1558,0
      # convert figs_3D/3D.MMF.v2.tmp.crm_000.png -crop '1x1+1558+0' txt:-

      # for gray_val in range(180,190):
      #   cmd = f'convert {crm_fig_file}.png -fuzz 0% '
      #   cmd+=f' -fill \'rgba(0,0,0,0)\' -opaque \'rgb({gray_val},{gray_val},{gray_val})\' '
      #   cmd+=f' {crm_fig_file}.png'
      #   run_cmd(cmd)
      #------------------------------------------------------
      # composite the modified CRM image
      #------------------------------------------------------
      cmd = f'composite -alpha On -compose src-atop'
      cmd+= f' -geometry {crm_fig_wid}x{crm_fig_hgt}+0+0'
      cmd+= f' {crm_fig_file}.png {fig_file}.png {fig_file}.png'
      run_cmd(cmd)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

ngl.end()
hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------