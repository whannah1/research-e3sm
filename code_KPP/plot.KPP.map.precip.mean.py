import os, ngl, glob, copy, string, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean

mvar,ovar,lev_list = [],[],[]
def add_var(mvar_name,ovar_name=None,lev=0): 
   mvar.append(mvar_name); ovar.append(ovar_name); lev_list.append(lev)
#-------------------------------------------------------------------------------
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne120pg2_scrip.nc'

case,name,path = [],[],[]

name.append('ERA5'); case.append('ERA5')
path.append('/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5/daily_ne120pg2/ERA5.daily.sfc.1950-1.remap_ne120pg2.nc')

name.append('IMERG'); case.append('IMERG')
path.append('/gpfs/alpine/scratch/hannah6/cli115/Obs/IMERG/daily_ne120pg2/3B-DAY.MS.MRG.3IMERG.2001010[1-5]-S000000-E235959.V06.remap_ne120pg2.nc4')

name.append('E3SM-MMF'); case.append('WCYCL1950-MMF1.frontier.CRAYCLANGGPU.ne120pg2.8192_1024.2023_01')
path.append(f'/gpfs/alpine/cli115/proj-shared/yuanx/ACME_SIMULATIONS/{case[-1]}/run/{case[-1]}.eam.h1.*.nc')

#-------------------------------------------------------------------------------

add_var('PRECT')

fig_type = 'png'
fig_file = os.getenv('HOME')+f'/Research/E3SM/figs_KPP/F2.kpp.map.{mvar[0]}.mean'

plot_diff = False
var_x_case = False
num_plot_col = 1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(mvar),len(case)

subtitle_font_height = 0.015

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

num_case_alt = num_case*2-1 if plot_diff else num_case
plot = [None]*(num_var*num_case_alt)
   
res = hs.res_contour_fill_map()
res.tmYLLabelFontHeightF         = 0.012
res.tmXBLabelFontHeightF         = 0.012
res.lbLabelFontHeightF           = 0.012
# res.tmXBOn                       = False
# res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'
scrip_ds = xr.open_dataset(scrip_file_path)
res.cnFillMode    = "CellFill"
res.sfXArray      = scrip_ds['grid_center_lon'].values
res.sfYArray      = scrip_ds['grid_center_lat'].values
res.sfXCellBounds = scrip_ds['grid_corner_lon'].values
res.sfYCellBounds = scrip_ds['grid_corner_lat'].values

# res.mpLimitMode = 'LatLon'
# res.mpMinLatF = -50
# res.mpMaxLatF =  50

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+mvar[v]+hc.tcolor.ENDC)
   data_list = []
   glb_avg_list = []
   lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      tvar = mvar[v] 
      if case[c]=='ERA5' : tvar = 'tp'
      if case[c]=='IMERG': tvar = 'precipAvg'

      #-------------------------------------------------------------------------
      # read the data
      
      file_list = sorted(glob.glob(path[c]))

      # print(); print(path[c])
      # print(); print(file_list)

      # ds = xr.open_mfdataset( file_list, combine='by_coords', concat_dim='time', use_cftime=True )
      ds = xr.open_mfdataset( file_list, combine='nested', concat_dim='time', use_cftime=True )

      data = ds[tvar].mean(dim='time')

      # print(); print(data)

      # handle of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      #-------------------------------------------------------------------------
      # adjust units
      if mvar[v]=='PRECT' and name[c]=='ERA5':   data = data*1e3*24.
      if mvar[v]=='PRECT' and name[c]=='IMERG':  data = data*24.
      if mvar[v]=='PRECT' and 'E3SM' in name[c]: data = data*86400.*1e3
      #-------------------------------------------------------------------------

      # # print stats before time averaging
      # hc.print_stat(data,name=tvar,stat='naxsh',indent='    ',compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         # hc.print_time_length(data.time,indent=' '*6)
         data = data.mean(dim='time')

      # print stats after time averaging
      hc.print_stat(data,name=tvar,stat='naxsh',indent='    ',compact=True)

      # for d in data.values: 
      #    if d>200: print(d)
      # exit()
      
      #-------------------------------------------------------------------------
      # # Calculate area weighted global mean
      # area = ds['area']
      # gbl_mean = ( (data*area).sum() / area.sum() ).values 
      # print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
      # glb_avg_list.append(gbl_mean)

      #-------------------------------------------------------------------------
      # append to data lists
      data_list.append( np.ma.masked_invalid(data.values) )

      if plot_diff and c==0 : data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[0]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[0]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      # if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'BlueWhiteOrangeRed'
      # if var[v] in ['U','V','UBOT','VBOT','U850','V850','U200','V200',
      #               'MMF_CVT_TEND_T','MMF_CVT_TEND_Q']: 
      #    tres.cnFillPalette = "BlueWhiteOrangeRed"

      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      # tres.cnLevels = np.arange(0,60+1,1)
      tres.cnLevels = np.arange(1,61+5,5)
      
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      # else:
      #    nlev = 41
      #    aboutZero = False
      #    if mvar[v] in ['SPTLS','SPQTLS','U','V','VOR','DIV',
      #                  'U850','V850','U200','V200',
      #                  'MMF_CVT_TEND_T','MMF_CVT_TEND_Q',] : 
      #       aboutZero = True
      #    clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
      #                                    returnLevels=False, aboutZero=aboutZero )
      #    if clev_tup==None: 
      #       tres.cnLevelSelectionMode = 'AutomaticLevels'   
      #    else:
      #       cmin,cmax,cint = clev_tup
      #       tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
      #       tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      var_str = mvar[v]
      if var_str=='PRECT': var_str = 'Precipitation [mm/day]'

      lev_str = None
      if lev>0: lev_str = f'{lev}mb'
      if lev<0: lev_str = f'k={(lev*-1)}'
      if lev_str is not None:
         var_str = f'{lev_str} {mvar[v]}'

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
      #----------------------------------------------------------------------
      # set plot subtitles
      #----------------------------------------------------------------------
      ctr_str = ''
      # if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'

      lstr = name[c]

      hs.set_subtitles(wks, plot[ip], lstr, '', var_str, center_sub_string=ctr_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c>0 :
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         tres.lbLabelBarOn = True

         ipd = ip
         if     var_x_case: ipd = ip+(num_case-1)
         if not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)
         
         #-----------------------------------
         ctr_str = ''
         ctr_str = 'Diff'
         # if glb_avg_list != []: 
         #    glb_diff = glb_avg_list[c] - glb_avg_list[0]
         #    ctr_str += f' ({glb_diff:6.4})'
         lstr = name[c]
         
         hs.set_subtitles(wks, plot[ipd], lstr, '', var_str, center_sub_string=ctr_str,font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]

if not plot_diff and (num_case==1 or num_var==1):
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
