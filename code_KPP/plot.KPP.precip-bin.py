# v1 - original
# v2 - calculate dist for each point first as a way to handle area weighting issues
import os, ngl, xarray as xr, numpy as np, glob
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
home = os.getenv("HOME")
print()
#-------------------------------------------------------------------------------
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne120pg2_scrip.nc'

case,name,path,clr = [],[],[],[]

name.append('IMERG'); case.append('IMERG'); clr.append('green')
path.append('/gpfs/alpine/scratch/hannah6/cli115/Obs/IMERG/daily_ne120pg2/3B-DAY.MS.MRG.3IMERG.2001010[1-5]-S000000-E235959.V06.remap_ne120pg2.nc4')

name.append('ERA5'); clr.append('blue'); case.append('ERA5')
path.append('/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5/daily_ne120pg2/ERA5.daily.sfc.1950-1.remap_ne120pg2.nc')

name.append('E3SM-MMF'); clr.append('red'); case.append('WCYCL1950-MMF1.frontier.CRAYCLANGGPU.ne120pg2.8192_1024.2023_01')
path.append(f'/gpfs/alpine/cli115/proj-shared/yuanx/ACME_SIMULATIONS/{case[-1]}/run/{case[-1]}.eam.h1.*.nc')

#-------------------------------------------------------------------------------

fig_type = 'png'
fig_file = os.getenv('HOME')+f'/Research/E3SM/figs_KPP/F3.kpp.precip-bin'

lat1,lat2 = -50,50
# lat1,lat2 = -30,30

var = 'PRECT'

recalculate = False

temp_dir = home+"/Research/E3SM/data_temp"


bin_min_ctr = 0.04
bin_min_wid = 0.02
bin_spc_pct = 25.
nbin = 40

#---------------------------------------------------------------------------------------------------
# use this code for checking bin sizes and range
#---------------------------------------------------------------------------------------------------
# bin_min, bin_spc, bin_spc_log, nbin_log = 0.04, 0.02, 25., 40
# # bin_min, bin_spc, bin_spc_log, nbin_log = 0.105, 0.01, 10., 100  # Gabe's 2016 paper
# bin_log_wid = np.zeros(nbin_log)
# bin_log_ctr = np.zeros(nbin_log)
# bin_log_ctr[0] = bin_min
# bin_log_wid[0] = bin_spc
# for b in range(1,nbin_log):
#    bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
#    bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
# nbin = nbin_log
# bin_coord = xr.DataArray( bin_log_ctr )
# ratio = bin_log_wid / bin_log_ctr
# for b in range(nbin_log): print('  center: {:12.6f}      width: {:12.6f}     ratio: {:6.4f}'.format(bin_log_ctr[b],bin_log_wid[b],ratio[b]))
# exit()
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.xyXStyle = "Log"

if 'clr' not in vars(): 
   clr = ['black']*num_case
   # if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   

if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh
#---------------------------------------------------------------------------------------------------
# Load area and create spatial mask
#---------------------------------------------------------------------------------------------------
scrip_ds = xr.open_dataset(scrip_file_path)
lat_name,lon_name = 'grid_center_lat','grid_center_lon'
tmp_data = np.ones(len(scrip_ds[lat_name]),dtype=bool)
mask = xr.DataArray( tmp_data, coords=scrip_ds[lat_name].coords )
if 'lat1' in locals(): mask = mask & (scrip_ds[lat_name]>=lat1) & (scrip_ds[lat_name]<=lat2)
if 'lon1' in locals(): mask = mask & (scrip_ds[lon_name]>=lon1) & (scrip_ds[lon_name]<=lon2)
mask = mask.rename({'grid_size':'ncol'})
area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})
area = area.where( mask, drop=True)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
frq_list = []
amt_list = []
bin_list = []
case_name = []

gpcp_cnt = 0



for c in range(num_case):
   print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

   tvar = var
   if case[c]=='ERA5' : tvar = 'tp'
   if case[c]=='IMERG': tvar = 'precipAvg'

   #-------------------------------------------------------------------------
   # read the data
   
   file_list = sorted(glob.glob(path[c]))

   # print(); print(path[c])
   # print(); print(file_list)

   # ds = xr.open_mfdataset( file_list, combine='by_coords', concat_dim='time', use_cftime=True )
   ds = xr.open_mfdataset( file_list, combine='nested', concat_dim='time', use_cftime=True )

   data = ds[tvar].resample(time='D').mean(dim='time')

   # print(); print(data); exit()

   # handle of lev dimension
   if 'lev' in data.dims : data = data.isel(lev=0)

   #-------------------------------------------------------------------------
   # adjust units
   if var=='PRECT' and name[c]=='ERA5':   data = data*1e3*24.
   if var=='PRECT' and name[c]=='IMERG':  data = data*24.
   if var=='PRECT' and 'E3SM' in name[c]: data = data*86400.*1e3
   #-------------------------------------------------------------------------
   # apply spatial mask
   data = data.where( mask, drop=True)
   
   #-------------------------------------------------------------------------

   # print stats after converting to daily average and unit adjustment
   hc.print_stat(data,name=tvar,stat='naxsh',indent='    ',compact=True)

   gbl_mean = ( (data*area).sum() / area.sum() ).values 

   #-------------------------------------------------------------------------
   # Recreate bin_YbyX here for log bin case
   #-------------------------------------------------------------------------
   bin_min = bin_min_ctr
   bin_spc = bin_min_wid
   bin_spc_log = bin_spc_pct
   #----------------------------------------------------
   # set up bins
   bin_log_wid = np.zeros(nbin)
   bin_log_ctr = np.zeros(nbin)
   bin_log_ctr[0] = bin_min
   bin_log_wid[0] = bin_spc
   for b in range(1,nbin):
      bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
      bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
   bin_coord = xr.DataArray( bin_log_ctr )

   #----------------------------------------------------
   # create output data arrays
   ntime = len(data['time']) if 'time' in data.dims else 1   
   
   shape,dims,coord = (nbin,),'bins',[('bins', bin_coord)]

   bin_cnt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
   bin_amt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
   bin_frq = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )

   condition = xr.DataArray( np.full(data.shape,False,dtype=bool), coords=data.coords )

   wgt, *__ = xr.broadcast(area,data)
   wgt = wgt.transpose()

   data_area_wgt = (data*wgt) / wgt.sum()

   ones_area_wgt = xr.DataArray( np.ones(data.shape), coords=data.coords )
   ones_area_wgt = ( ones_area_wgt*wgt ) / wgt.sum()

   #----------------------------------------------------
   # Loop through bins
   for b in range(nbin):
      bin_bot = bin_log_ctr[b] - bin_log_wid[b]/2.
      bin_top = bin_log_ctr[b] + bin_log_wid[b]/2.
      condition.values = ( data.values >=bin_bot )  &  ( data.values < bin_top )
      bin_cnt[b] = condition.sum()
      if bin_cnt[b]>0 :
         bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
         bin_amt[b] = data_area_wgt.where(condition,drop=True).sum() / (bin_spc_pct/1e2)

   #----------------------------------------------------
   # use a dataset to hold all the output
   dims = ('bins',)
   bin_ds = xr.Dataset()
   bin_ds['bin_amt'] = (dims, bin_amt )
   bin_ds['bin_frq'] = (dims, bin_frq )
   bin_ds['bin_cnt'] = (dims, bin_cnt )
   bin_ds['bin_pct'] = (dims, bin_cnt/bin_cnt.sum()*1e2 )
   # bin_ds['bin_cnt'] = (dims, bin_cnt.sum(dim='ncol') )
   # bin_ds['bin_pct'] = (dims, bin_cnt.sum(dim='ncol')/bin_cnt.sum()*1e2 )
   bin_ds.coords['bins'] = ('bins',bin_coord)

   bin_ds['gbl_mean'] = gbl_mean

   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   frequency = np.ma.masked_invalid( bin_frq.values )
   amount    = np.ma.masked_invalid( bin_amt.values )
   
   if 'gbl_mean' in locals():
      print('\n'+f'    Area weighted global mean:  {gbl_mean} ')
      print(     f'    sum of rain amount dist:    {np.sum(amount*(bin_spc_pct/1e2))} \n')

   frq_list.append( frequency    )
   amt_list.append( amount )
   bin_list.append( bin_coord )
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
unit_str = ''
res.tiXAxisString = 'Rain Rate [mm/day]'

var_str = "Precipitation"

#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

res.xyExplicitLegendLabels = name
res.pmLegendDisplayMode    = "Always"
res.pmLegendOrthogonalPosF = -1.13
res.pmLegendParallelPosF   =  0.8+0.04
res.pmLegendWidthF         =  0.16
res.pmLegendHeightF        =  0.12   
res.lgBoxMinorExtentF      =  0.16   

res.trXMinF = np.min(bin_list)
res.trXMaxF = np.max(bin_list)

### plot frequency
res.tiYAxisString = 'Frequency [%]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(frq_list) ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)


### plot rain amount
res.pmLegendDisplayMode = "Never"
res.tiYAxisString = 'Rain Amount [mm/day]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(amt_list) ,res) )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
