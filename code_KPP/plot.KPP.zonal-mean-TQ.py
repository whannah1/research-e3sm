import os, ngl, glob, copy, string, subprocess as sp, numpy as np, xarray as xr, warnings, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
#-------------------------------------------------------------------------------
case_list = []
name_list = []
path_list = []
obs_flag  = []
def add_case(c,n,p,obs=False):
   case_list.append(c)
   name_list.append(n)
   path_list.append(p.replace('.C.A.S.E.',c))
   obs_flag.append(obs)
#-------------------------------------------------------------------------------
mvar,ovar = [],[]
def add_var(mvar_name,ovar_name): 
   mvar.append(mvar_name); ovar.append(ovar_name)
#-------------------------------------------------------------------------------
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne120pg2_scrip.nc'



add_case('ERA5_1950',n='ERA5 1950',obs=True,p='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5/daily_ne120pg2/ERA5.daily.atm.1950-1.remap_ne120pg2.nc')
# add_case('ERA5_1960',n='ERA5 1960',obs=True,p='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5/daily_ne120pg2/ERA5.daily.atm.1960-1.remap_ne120pg2.nc')
# add_case('ERA5_1990',n='ERA5 1990',obs=True,p='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5/daily_ne120pg2/ERA5.daily.atm.1990-1.remap_ne120pg2.nc')

add_case('WCYCL1950-MMF1.frontier.CRAYCLANGGPU.ne120pg2.8192_1024.2023_01',n='E3SM-MMF',p='/gpfs/alpine/cli115/proj-shared/yuanx/ACME_SIMULATIONS/.C.A.S.E./run/.C.A.S.E..eam.h1.*.nc')

#-------------------------------------------------------------------------------

add_var('T','t')
add_var('Q','q')

fig_type = 'png'
fig_file = os.getenv('HOME')+f'/Research/E3SM/figs_KPP/F6.kpp.zonal-mean.TQ'

plot_diff = False
var_x_case = False
num_plot_col = len(mvar)

recalculate = False

lat1, lat2, dlat = -88., 88., 2

# lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925])
lev = np.array([50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950])

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(mvar),len(case_list)

subtitle_font_height = 0.015

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

num_case_alt = num_case*2-1 if plot_diff else num_case
plot = [None]*(num_var*num_case_alt)
   
# res = hs.res_contour_fill_map()
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
# res.lbLabelFontHeightF           = 0.01
# res.tmXBOn                       = False
# res.tmYLOn                       = False
# # res.mpGeophysicalLineColor       = 'white'
# scrip_ds = xr.open_dataset(scrip_file_path)
# res.cnFillMode    = "CellFill"
# res.sfXArray      = scrip_ds['grid_center_lon'].values
# res.sfYArray      = scrip_ds['grid_center_lat'].values
# res.sfXCellBounds = scrip_ds['grid_corner_lon'].values
# res.sfYCellBounds = scrip_ds['grid_corner_lat'].values


res = hs.res_contour_fill()
res.vpHeightF = 0.3
res.trYReverse = True
# res.tiXAxisString = 'sin( Latitude )'
res.tiXAxisString = 'Latitude'
res.tiYAxisString = 'Pressure [hPa]'

res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.lbLabelFontHeightF     = 0.015

res.lbRightMarginF = -1
res.lbLeftMarginF  = -1

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+mvar[v]+hc.tcolor.ENDC)
   data_list = []
   lev_list = []
   glb_avg_list = []
   for c in range(num_case):

      tvar = ovar[v] if obs_flag[c] else mvar[v]

      temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
      tmp_file = f'{temp_dir}/kpp.zonal-mean.{case_list[c]}.{mvar[v]}.nc'

      print(' '*4+f'case: {hc.tcolor.GREEN}{case_list[c]}{hc.tcolor.ENDC}  => {tmp_file}')

      if recalculate:
         #----------------------------------------------------------------------
         # read the data
         file_list = sorted(glob.glob(path_list[c]))

         with dask.config.set(**{'array.slicing.split_large_chunks': True}):
            # ds = xr.open_mfdataset( file_list, combine='by_coords', concat_dim='time', use_cftime=True )
            ds = xr.open_mfdataset( file_list, combine='nested', concat_dim='time', use_cftime=True )
            if obs_flag[c]:
               data = ds[tvar].rename({'level':'lev'})
               data = data.sel(lev=lev) # select levels specified above (no interpolation)
            else:
               # remap model data to pressure levels
               data = he.interpolate_to_pressure(ds,data_mlev=ds[tvar],lev=lev,ds_ps=ds,ps_var='PS'
                                                ,interp_type=2,extrap_flag=True)

         data.load()

         # print stats before time averaging
         hc.print_stat(data,name=tvar,stat='naxsh',indent='    ',compact=True)

         # average over time dimension
         if 'time' in data.dims : data = data.mean(dim='time', skipna=True)

         lat,area = ds['lat'],ds['area']

         if 'time' in  lat.dims :  lat =  lat.isel(time=0)
         if 'time' in area.dims : area = area.isel(time=0)
         #----------------------------------------------------------------------
         # Calculate time and zonal mean
         with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            bin_ds = hc.bin_YbyX( data, lat, bin_min=lat1, bin_max=lat2, bin_spc=dlat, wgt=area, keep_lev=True )

         print('writing to file: '+tmp_file)
         bin_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( tmp_file )

      lat_bins = bin_ds['bins'].values
      sin_lat_bins = np.sin(lat_bins*np.pi/180.)
      data_binned = np.ma.masked_invalid( bin_ds['bin_val'].transpose().values )

      if mvar[v]=='Q' : data_binned = data_binned*1e3

      #-------------------------------------------------------------------------
      # # Calculate area weighted global mean
      # area = ds['area']
      # gbl_mean = ( (data*area).sum() / area.sum() ).values 
      # print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
      # glb_avg_list.append(gbl_mean)

      #-------------------------------------------------------------------------
      # append to data lists
      lev_list.append( bin_ds['lev'].values )
      data_list.append( data_binned )

      if plot_diff and c==0 : data_baseline = data_binned.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[0]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[0]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      # tres.cnFillPalette = 'BlueWhiteOrangeRed'
      
      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      tres.cnLevelSelectionMode = 'ExplicitLevels'
      if mvar[v]=='U'     : tres.cnLevels = np.arange(-40,40+4,4)
      if mvar[v]=='OMEGA' : tres.cnLevels = np.linspace(-0.15,0.15,num=11)
      if mvar[v]=='T'     : tres.cnLevels = np.arange(200,300+10,10)
      # if mvar[v]=='Q'     : tres.cnLevels = np.arange(2,14+2,2)
      if mvar[v]=='Q'     : tres.cnLevels = np.logspace(np.log10(0.1),np.log10(14),num=11)

      tres.lbTitleFontHeightF = 0.02
      tres.lbTitlePosition = 'Bottom'
      if mvar[v]=='U'     : tres.lbTitleString = '[m/s]'
      if mvar[v]=='OMEGA' : tres.lbTitleString = '[m/s]'
      if mvar[v]=='Q'     : tres.lbTitleString = '[g/kg]'
      if mvar[v]=='T'     : tres.lbTitleString = '[K]'
      

      #-------------------------------------------------------------------------
      # Create plot
      tres.sfXArray = lat_bins
      tres.sfYArray = lev_list[c]

      lev_tick = np.array([900,800,700,600,500,400,300,200,100])
      tres.tmYLMode = "Explicit"
      tres.tmYLValues = lev_tick
      tres.tmYLLabels = lev_tick

      # # this doesn't work... axis keeps getting automatically rescaled?
      # tres.sfXArray = sin_lat_bins
      # lat_tick = np.array([-90,-60,-30,0,30,60,90])
      # tres.tmXBMode = "Explicit"
      # tres.tmXBValues = np.sin( lat_tick*3.14159/180. )
      # tres.tmXBLabels = lat_tick

      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      # turn on color bar for bottom panels
      if not var_x_case: tres.lbLabelBarOn = False if c<(num_case-1) else True

      plot[ip] = ngl.contour(wks, data_list[c], tres)

      #----------------------------------------------------------------------
      # set plot subtitles
      hs.set_subtitles(wks, plot[ip], name_list[c], '', mvar[v], center_sub_string='', font_height=subtitle_font_height)
      
      #-------------------------------------------------------------------------
      # create difference plot
      if plot_diff and c>0 :
         
         data_list[c] = data_list[c] - data_baseline

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         tres.lbLabelBarOn = True

         ipd = ip
         if     var_x_case: ipd = ip+(num_case-1)
         if not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)
         
         #-----------------------------------

         hs.set_subtitles(wks, plot[ipd], name[c], '', var_str, center_sub_string='Diff',font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]

if not plot_diff and (num_case==1 or num_var==1):
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
