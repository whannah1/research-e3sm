import os, ngl, glob, copy, string, subprocess as sp, numpy as np, xarray as xr, warnings
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean

mvar,ovar = [],[]
def add_var(mvar_name,ovar_name): 
   mvar.append(mvar_name); ovar.append(ovar_name)
#-------------------------------------------------------------------------------
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne120pg2_scrip.nc'

case,name,path = [],[],[]

name.append('ERA5'); case.append('ERA5')
path.append('/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5/daily_ne120pg2/ERA5.daily.atm.1950-1.remap_ne120pg2.nc')
# path.append('/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5/daily_ne120pg2/ERA5.daily.atm.1950-1.vertical_velocity.remap_ne120pg2.nc')

name.append('E3SM-MMF'); case.append('WCYCL1950-MMF1.frontier.CRAYCLANGGPU.ne120pg2.8192_1024.2023_01')
path.append(f'/gpfs/alpine/cli115/proj-shared/yuanx/ACME_SIMULATIONS/{case[-1]}/run/{case[-1]}.eam.h1.*.nc')

#-------------------------------------------------------------------------------

add_var('U','u')
add_var('OMEGA','w') # need to get vert velocity for ERA5!!!!
# add_var('T','t')
# add_var('Q','q')

fig_type = 'png'
fig_file = os.getenv('HOME')+f'/Research/E3SM/figs_KPP/kpp.zonal-mean.UTQ'
# fig_file = os.getenv('HOME')+f'/Research/E3SM/figs_KPP/kpp.zonal-mean.UWT'

plot_diff = False
var_x_case = False
num_plot_col = 2

recalculate = False

lat1, lat2, dlat = -88., 88., 2
# lat1, lat2, dlat = -89., 89., 1

# lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,
#                550,600,650,700,750,800,825,850,875,900,925,950,975,1000])

lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,
               550,600,650,700,750,800,825,850,875,900,925])

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
obs_list = ['ERA5']
num_var,num_case = len(mvar),len(case)

subtitle_font_height = 0.015

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

num_case_alt = num_case*2-1 if plot_diff else num_case
plot = [None]*(num_var*num_case_alt)
   
# res = hs.res_contour_fill_map()
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
# res.lbLabelFontHeightF           = 0.01
# res.tmXBOn                       = False
# res.tmYLOn                       = False
# # res.mpGeophysicalLineColor       = 'white'
# scrip_ds = xr.open_dataset(scrip_file_path)
# res.cnFillMode    = "CellFill"
# res.sfXArray      = scrip_ds['grid_center_lon'].values
# res.sfYArray      = scrip_ds['grid_center_lat'].values
# res.sfXCellBounds = scrip_ds['grid_corner_lon'].values
# res.sfYCellBounds = scrip_ds['grid_corner_lat'].values


res = hs.res_contour_fill()
res.vpHeightF = 0.3
res.trYReverse = True
res.tiXAxisString = 'sin( Latitude )'
res.tiYAxisString = 'Pressure [hPa]'

res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.lbLabelFontHeightF     = 0.015

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+mvar[v]+hc.tcolor.ENDC)
   data_list = []
   glb_avg_list = []
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      tvar = ovar[v] if case[c] in obs_list else mvar[v]

      temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
      tmp_file = f'{temp_dir}/kpp.zonal-mean.{case[c]}.{mvar[v]}'

      print('\n    tmp_file: '+tmp_file+'\n')

      if recalculate:

         #----------------------------------------------------------------------
         # read the data
         
         file_list = sorted(glob.glob(path[c]))

         # ds = xr.open_mfdataset( file_list, combine='by_coords', concat_dim='time', use_cftime=True )
         ds = xr.open_mfdataset( file_list, combine='nested', concat_dim='time', use_cftime=True )

         # print(ds)
         # print(ds[tvar])
         # exit()

         if case[c] in obs_list:
            data = ds[tvar].rename({'level':'lev'})
         else:
            # remap model data to pressure levels
            data = he.interpolate_to_pressure(ds,data_mlev=ds[tvar],lev=lev,ds_ps=ds,ps_var='PS'
                                             ,interp_type=2,extrap_flag=True)

         # print stats before time averaging
         hc.print_stat(data,name=tvar,stat='naxsh',indent='    ',compact=True)

         # average over time dimension
         if 'time' in data.dims : 
            data = data.mean(dim='time', skipna=True)

         lat,area = ds['lat'],ds['area']

         if 'time' in  lat.dims :  lat =  lat.isel(time=0)
         if 'time' in area.dims : area = area.isel(time=0)
         #----------------------------------------------------------------------
         # Calculate time and zonal mean

         with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)

            bin_ds = hc.bin_YbyX( data, lat,    \
                                  bin_min=lat1, \
                                  bin_max=lat2, \
                                  bin_spc=dlat, \
                                  wgt=area,     \
                                  keep_lev=True )

         print('writing to file: '+tmp_file)
         bin_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( tmp_file )


      lat_bins = bin_ds['bins'].values
      sin_lat_bins = np.sin(lat_bins*np.pi/180.)

      data_binned = np.ma.masked_invalid( bin_ds['bin_val'].transpose().values )

      if mvar[v]=='Q' : data_binned = data_binned*1e3

      #-------------------------------------------------------------------------
      # # Calculate area weighted global mean
      # area = ds['area']
      # gbl_mean = ( (data*area).sum() / area.sum() ).values 
      # print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
      # glb_avg_list.append(gbl_mean)

      #-------------------------------------------------------------------------
      # append to data lists
      data_list.append( data_binned )

      if plot_diff and c==0 : data_baseline = data_binned.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[0]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[0]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      # tres.cnFillPalette = 'BlueWhiteOrangeRed'
      
      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      tres.cnLevelSelectionMode = 'ExplicitLevels'
      if mvar[v]=='U'     : tres.cnLevels = np.arange(-40,40+4,4)
      if mvar[v]=='OMEGA' : tres.cnLevels = np.linspace(-0.15,0.15,num=11)
      if mvar[v]=='T'     : tres.cnLevels = np.arange(200,300+10,10)
      if mvar[v]=='Q'     : tres.cnLevels = np.arange(2,14+2,2)

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      tres.sfXArray = sin_lat_bins

      lat_tick = np.array([-90,-60,-30,0,30,60,90])
      tres.tmXBMode = "Explicit"
      tres.tmXBValues = np.sin( lat_tick*3.14159/180. )
      tres.tmXBLabels = lat_tick

      tres.sfYCStartV = min( bin_ds['lev'].values )
      tres.sfYCEndV   = max( bin_ds['lev'].values )

      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      plot[ip] = ngl.contour(wks, data_list[c], tres)

      #----------------------------------------------------------------------
      # set plot subtitles
      #----------------------------------------------------------------------
      ctr_str = ''
      # if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'

      lstr = name[c]
      var_str = mvar[v]

      hs.set_subtitles(wks, plot[ip], lstr, '', var_str, center_sub_string=ctr_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c>0 :
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         tres.lbLabelBarOn = True

         ipd = ip
         if     var_x_case: ipd = ip+(num_case-1)
         if not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)
         
         #-----------------------------------
         ctr_str = ''
         ctr_str = 'Diff'
         # if glb_avg_list != []: 
         #    glb_diff = glb_avg_list[c] - glb_avg_list[0]
         #    ctr_str += f' ({glb_diff:6.4})'
         lstr = name[c]
         
         hs.set_subtitles(wks, plot[ipd], lstr, '', var_str, center_sub_string=ctr_str,font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]

if not plot_diff and (num_case==1 or num_var==1):
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
