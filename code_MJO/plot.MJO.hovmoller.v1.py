import os, ngl, xarray as xr, numpy as np, numba
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------


# case = ['E3SM.ne30pg2_r05_oECv3.F-MMF1.01']
# case = ['E3SM.ne30pg2_r05_oECv3.F-MMF1.CRM-AC.RAD-AC.01']
case = ['E3SM.ne30pg2_r05_oECv3.F-MMF1.01','E3SM.ne30pg2_r05_oECv3.F-MMF1.CRM-AC.RAD-AC.01']

# var = ['FLNT']
var = ['U850']
# var = ['U','FLNT']
# var = ['FLNT','U']
# lev = 850

fig_type = "png"
fig_file = os.getenv("HOME")+"/Research/E3SM/figs_MJO/MJO.hovmoller.v1"


lat1,lat2 = -15,15
# lon1,lon2 = 40,220

# htype,years,first_file,num_files = 'h1',[],73*5-6*4,6*6
htype,years,first_file,num_files = 'h1',[],0,0
# htype,years,first_file,num_files = 'h1',[],int(365*4/5),int(90/5)
# htype,years,first_file,num_files = 'h1',[],int(365/5),5
# htype,years,num_files = 'h2',[],-int(365*2/5)    # use h2 for 3D variables

recalculate_hov = True

dlon = 2
# lon_bins = np.arange(40,220,dlon)
lon_bins = np.arange(0,360,dlon)

# dlon = 1
# lon_bins = np.arange(1,360,dlon)

apply_filter = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var  = len(var)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_case
res = hs.res_contour_fill()
# res.vpHeightF = 0.6
# res.vpWidthF  = 0.3
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

# res.trXMinF,res.trXMaxF = 0, 180

if 'lev' not in locals(): lev = np.array([0])

#---------------------------------------------------------------------------------------------------
# Create the band-pass filter
#---------------------------------------------------------------------------------------------------
nwgt = 91
fc_lp = 1./( 20.)
fc_hp = 1./(100.)
wgt = hc.filter_wgts_lp_lanczos(nwgt,fc_lp=fc_lp,fc_hp=fc_hp)

@numba.njit()
def filter_numba(data,data_filt,wgt,tvals0,win_width,ntime):   
   for i in range(ntime):
      if i >= win_width and i < (ntime-win_width-1) :
         tvals = tvals0 + i
         data_filt[i] = np.sum( data[tvals] * wgt )
   return data_filt

@numba.njit()
def hov_numba(data, lon, lon_bins, hov, nbin, ntime, ncol):
   for b in range( nbin ):
      bin_bot = lon_bins[b] - dlon/2. 
      bin_top = lon_bins[b] - dlon/2. + dlon
      cnt = 0
      for n in range( ncol ):
         if ( lon[n] >=bin_bot ) & ( lon[n]  <bin_top ):
            cnt = cnt+1
            for t in range( ntime ):
               hov[t,b] = hov[t,b] + data[t,n]
      hov[:,b] = hov[:,b]/cnt

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hov_list = []
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c], time_freq='daily' )
      
      # Subset the data
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      # v = 0
      # data = case_obj.load_data(var[v],htype=htype,lev=lev,years=years,first_file=first_file,num_files=num_files)
      # exit()

      lon  = case_obj.load_data('lon', htype='h0',num_files=1)
      area = case_obj.load_data('area',htype='h0',num_files=1).astype(np.double)

      # if recalculate_hov:
      #    time = case_obj.load_data('time',htype=htype,years=years,first_file=first_file,num_files=num_files).resample(time='D').mean(dim='time')

      # if htype=='h1' : case_obj.hist_path = case_obj.data_dir+case_obj.name+'/hourly_2d_hist/'
      # if htype=='h2' : case_obj.hist_path = case_obj.data_dir+case_obj.name+'/3hourly_3d_hist/'
   
      tmp_file = case_obj.data_dir+case_obj.name+f'/MJO.hovmoller.horz.v1.tmp_{var[v]}_lat1={lat1}_lat2={lat2}.nc'
      if recalculate_hov :
         print("recalculating...")
         #----------------------------------------------------------------------
         # Load the data and convert to daily
         #----------------------------------------------------------------------
         print('\nLoading data ('+var[v]+')...\n')

         data = case_obj.load_data(var[v],htype=htype,lev=lev,years=years,first_file=first_file,num_files=num_files)
         # time = case_obj.load_data('time',htype=htype,        years=years,first_file=first_file,num_files=num_files)

         # if 'lev' in data.dims : data = data.isel(lev=0)

         # Convert to daily
         # time = time.resample(time='D').mean(dim='time')
         data = data.resample(time='D').mean(dim='time')

         time = data.time

         # hc.printline()
         # print(data)
         # print(time)
         # hc.printline()
         # exit()
         
         #----------------------------------------------------------------------
         # bin the data
         #----------------------------------------------------------------------
         # Setup output array
         hov = xr.DataArray( np.zeros( (len(time),len(lon_bins)) ), \
                             coords=[('time',time),('lon',lon_bins)], \
                             dims=['time','lon'] )

         # hc.printline()
         # print(hov)
         # hc.print_stat(hov,name=var[v])
         # hc.printline()


         hov_numba( data.values, lon.values, lon_bins, hov.values, \
                    len(lon_bins), len(data.time), len(data.ncol))

         # for b in range( len(lon_bins) ):
         #    bin_bot = lon_bins[b] - dlon/2. #+ dlon#*(b  )
         #    bin_top = lon_bins[b] - dlon/2. + dlon#*(b+1)
         #    condition =  ( lon >=bin_bot ) & ( lon  <bin_top )
         #    if np.sum(condition.values)>0 :
         #       hov[:,b] = data.where(condition,drop=True).mean(dim='ncol').values
         #       # for t in range( len(time) ):
         #       #    hov[t,b] = X.isel(time=t).where(condition,drop=True).mean(dim='ncol')
                  
         hc.printline()
         # print(hov)
         hc.print_stat(hov,name=var[v])
         hc.printline()

         #----------------------------------------------------------------------
         # Filter in time
         #----------------------------------------------------------------------
         # ntime = len(hov.time)
         # win_width = np.floor(nwgt/2)
         # tvals0 = np.arange( 0-win_width, 0+win_width+1, dtype=np.int)
         # hov_filt = hov.copy(data=np.full(hov.shape,np.nan))
         # for b in range( len(lon_bins) ):
         #    filter_numba(hov[:,b].values,hov_filt[:,b].values,wgt,tvals0,win_width,ntime)
         # hov = hov_filt

         # hc.printline()
         # print(hov)
         # hc.print_stat(hov,name=var[v])
         # hc.printline()

         #----------------------------------------------------------------------
         # Write to file 
         #----------------------------------------------------------------------
         print('\nWriting hovmoller data to file: '+tmp_file+'\n')
         hov.name = var[v]
         hov.to_netcdf(path=tmp_file,mode='w')
         # hov_ds = hov.to_dataset
         # hov_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         hov_ds = xr.open_dataset( tmp_file )         
         time = hov_ds['time']
         hov = hov_ds[var[v]]

         #----------------------------------------------------------------------
         # Filter in time
         #----------------------------------------------------------------------
         if apply_filter:
            # hc.printline()
            # hc.print_stat(hov,name=var[v])

            ntime = len(hov.time)
            win_width = np.floor(nwgt/2)
            tvals0 = np.arange( 0-win_width, 0+win_width+1, dtype=np.int)
            # hov_mean = hov.mean(dim='time')
            hov = hov - hov.mean(dim='time')
            hov_filt = hov.copy(data=np.full(hov.shape,np.nan))
            for b in range( len(lon_bins) ):
               filter_numba(hov[:,b].values,hov_filt[:,b].values,wgt,tvals0,win_width,ntime)
               # hov_filt[:,b] = ( hov_filt[:,b] + hov_mean[b] ) - hov_mean.mean()
            hov = hov_filt

            # hc.printline()
            # print(hov)
            # hc.print_stat(hov,name=var[v])
            # hc.printline()

            # exit()
         #----------------------------------------------------------------------
         #----------------------------------------------------------------------

      # print(hov)
      # exit()

      hov_list.append(hov)


   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   unit_str = ''
   # if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
   # res.tiXAxisString = unit_str
   res.tiYAxisString = 'Time [days]'
   res.tiXAxisString = 'Longitude'

   # res.tmXBMode      = 'Explicit'
   # res.tmXBValues    = np.arange( len(lon_bins) )
   # res.tmXBLabels    = lon_bins
   res.sfXArray = lon_bins

   var_str = var
   if var=="PRECT" : var_str = "Precipitation"


   # res.trXMinF = bin_ds['bins'].min().values
   # res.trXMaxF = bin_ds['bins'].max().values
   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------
   # res.tiYAxisString = ''

   ntime = len(hov.time)

   # res.trYMaxF = 350

   if v==0 : 

      num_clev = 41
      aboutZero = False
      if var[v] in ['U','V']: aboutZero = True
      if apply_filter: aboutZero = True
      min_val = np.min([np.min(h) for h in hov_list])
      max_val = np.max([np.max(h) for h in hov_list])
      clev_tup = ngl.nice_cntr_levels(min_val, max_val,       \
                                      cint=None, max_steps=num_clev, \
                                      returnLevels=False, aboutZero=aboutZero )
      if clev_tup==None: 
         print('SWITCHING TO AUTOMATIC CONTOUR LEVELS!')
         res.cnLevelSelectionMode = "AutomaticLevels"   
      else:
         cmin,cmax,cint = clev_tup
         res.cnLevels = np.linspace(cmin,cmax,num=num_clev)
         res.cnLevelSelectionMode = "ExplicitLevels"


      for c in range(num_case):
         plot[c] = ngl.contour(wks, hov_list[c].values ,res) 
         # plot.append( ngl.contour(wks, hov[nwgt:ntime-nwgt-1,:].values ,res) )
         # hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

      print('\nContour Levels:')
      print(ngl.get_float_array(plot[len(plot)-1].contour,"cnLevels"))
      print()

   else:
      res.cnFillOn   = False
      res.cnLinesOn  = True

      res.cnLineDashPattern = 1

      for c in range(num_case):
         tplot = ngl.contour(wks, hov[nwgt:ntime-nwgt-1,:].values ,res)
         ngl.overlay(plot[c],tplot)
      
         levels = ngl.get_float_array(tplot.contour,"cnLevels")

         # print(levels)
         # print(levels[ np.where(levels>=0) ])

         res.cnLevelSelectionMode = "ExplicitLevels"
         res.cnLevels = levels[ np.where(levels>=0) ]
         res.cnLineDashPattern = 0
         ngl.overlay(plot[c],ngl.contour(wks, hov[nwgt:ntime-nwgt-1,:].values ,res))

      


#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------