import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, numba
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
# import wavenumber_frequency_functions as wf
# import cmocean
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,d=None,s=None,g=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = tmp_name.replace('.',' ')
      tmp_name = tmp_name.replace('_','=')
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name); case_dir.append(d); case_sub.append(s); case_grid.append(g)
svar_name,svar_htype,svar_lev_list = [],[],[]
cvar_name,cvar_htype,cvar_lev_list = [],[],[]
def add_var(svar_name_in,svar_htype_in,cvar_name_in=None,cvar_htype_in=None,svar_lev=-1,cvar_lev=None): 
   svar_name.append(svar_name_in); svar_htype.append(svar_htype_in); svar_lev_list.append(svar_lev)
   cvar_name.append(cvar_name_in); cvar_htype.append(cvar_htype_in); cvar_lev_list.append(cvar_lev)
#-------------------------------------------------------------------------------
### rotating RCE
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.03', n='MMF RCEROT',         d='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.03', n='MMF RCEROT FIX-QRT', d='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')

### Variance transport validation
add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.00',     n='MMF')
add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_0.00',n='MMF BVT')
add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_1.00',n='MMF FVT1')
#-------------------------------------------------------------------------------

# ind_var,ind_htype = 'PRECT','h1'
ind_var,ind_htype = 'FLNT','h1'

# add_var('PRECT','h1',None,'h2',cvar_lev=850)
# add_var('PRECT','h1','U','h2',cvar_lev=850)
# add_var('FLNT','h1','U','h2',cvar_lev=850)
# add_var('PRECT','h1','LHFLX','h1')
# add_var('PRECT','h1','DMSE_RAD','h2')

# add_var('FLNT','h1')
add_var('U','h2',cvar_lev=850)

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_MJO/MJO.lag_regression.v1'


lat1,lat2 = -5,5
mxlag = 25      # maximum time lag (days)

first_file,num_files = 0,365*10
# first_file,num_files = 0,90; mxlag=5 # for debugging

regression_months = [10,11,12,1,2,3]

recalculate_ind = True    # 2x10 years took ~70 min
recalculate_reg = True
create_plot     = True

filter_data = True

# Set regression index region center longitude and width (ilon +/- iwid)
# ilon,iwid = 90,10
ilon,iwid = 90,5
# ilon,iwid = 150,10

dlon = 2
lon_bins = np.arange(dlon,360,dlon)

var_x_case = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case), len(svar_name)

if 'regression_months' not in locals(): regression_months = None

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case)
res = hs.res_contour_fill()
res.vpHeightF = 0.4
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.trXMaxF = 180

res.tmXBMode = "Explicit"
res.tmXBValues = np.array([0,60,120,180,240,300])
res.tmXBLabels = ['0','60E','120E','180','120W','60W']

# Create the band-pass filter weights
nwgt = 121
fhi,flo = 20.,100.,
fc_lp,fc_hp = 1./fhi,1./flo
wgt = hc.filter_wgts_lp_lanczos(nwgt,fc_lp=fc_lp,fc_hp=fc_hp)
win_width = np.floor(nwgt/2)
tvals0 = np.arange( 0-win_width, 0+win_width+1, dtype=int)

# tmp_file_dir  = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/tmp_data'
# tmp_file_dir  = os.getenv('HOME')+'/Research/E3SM/data_tmp'
tmp_file_dir  = '/gpfs/alpine/scratch/hannah6/cli115/data_tmp'
tmp_file_tail = f'lat1_{lat1}.lat2_{lat2}.ilon_{ilon}.iwid_{iwid}'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   if 'INCITE2019'      in case: comp = 'cam'
   if 'RGMA'            in case: comp = 'cam'
   if 'CESM'            in case: comp = 'cam'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
# temporal filtering routines
#---------------------------------------------------------------------------------------------------
# @numba.njit()
# def filter_numba_1D(data,data_filt,wgt,tvals0,win_width,ntime):   
#    for i in range(ntime):
#       if i >= win_width and i < (ntime-win_width-1) :
#          tvals = tvals0 + i
#          data_filt[i] = np.sum( data[tvals] * wgt )
#    return data_filt

# @numba.njit()
# def filter_numba_2D(data,wgt,tvals0,win_width):   
#    (ntime,ncol) = data.shape
#    data_filt = np.full((ntime,ncol),np.nan)
#    for t in range(ntime):
#       if t >= win_width and t < (ntime-win_width-1) :
#          for i in range(ncol):
#             tvals = tvals0 + t
#             data_filt[t,i] = np.sum( data[tvals,i] * wgt )
#    return data_filt

@numba.njit()
def filter_numba(data,wgt,tvals0,win_width):   
   if data.ndim==1:
      ntime,ncol = data.shape[0], 1
      data_filt = np.full((ntime),np.nan)
   else:
      (ntime,ncol) = data.shape
      data_filt = np.full((ntime,ncol),np.nan)
   for t in range(ntime):
      if t >= win_width and t < (ntime-win_width-1) :
         for i in range(ncol):
            tvals = tvals0 + t
            if data.ndim==1: data_filt[t]   = np.sum( data[tvals]   * wgt )
            if data.ndim==2: data_filt[t,i] = np.sum( data[tvals,i] * wgt )
   # return np.squeeze(data_filt) # drop dimension with length=1
   return data_filt # drop dimension with length=1
#---------------------------------------------------------------------------------------------------
# regression routines
#---------------------------------------------------------------------------------------------------
@numba.njit()
def meridional_avg(data, lon, lon_bins, dlon):
   # input data should have dimensions (time,ncol)
   (ntime,ncol) = data.shape
   nbin = len(lon_bins)
   data_lon_avg = np.zeros( ( ntime, nbin ) )
   for b in range(nbin):
      bin_bot = lon_bins[b] - dlon/2.
      bin_top = lon_bins[b] + dlon/2.
      condition =  ( lon >=bin_bot ) & ( lon  <bin_top )
      if np.sum(condition)>0 :
         cnt = 0
         for i in range(ncol):
            if condition[i]:
               for t in range(ntime):
                  data_lon_avg[t,b] = (data_lon_avg[t,b]*cnt + data[t,i])/(cnt+1)
               cnt += 1
   return data_lon_avg

@numba.njit()
def lag_regression_numba(data_in, index, mxlag, lag_coord, num_invalid):
   # input data should have dimensions (time,lon)
   (ntime,nbin) = data_in.shape
   valid_time_vals = np.arange( num_invalid+mxlag, ntime-num_invalid-mxlag-1, 1)
   reg_out = np.zeros( ( len(lag_coord), nbin ) )
   for b in range(nbin):
      for l in range( len(lag_coord) ):            
         # data_tmp = data_in[valid_time_vals+lag_coord[l],b]
         # reg_out[l,b] = np.cov( index[valid_time_vals], data_tmp )[1,0] / np.var( index[valid_time_vals] )
         data_tmp = data_in[valid_time_vals+lag_coord[l],b]
         ind_tmp = index[valid_time_vals]
         # data_avg, ind_avg = np.mean(data_tmp),np.mean(ind_tmp)
         data_avg  = np.mean(data_tmp)
         ind_avg = np.mean(ind_tmp)
         reg_out[l,b] = np.sum( (data_tmp-data_avg) * (ind_tmp-ind_avg) ) \
                      / np.sum( np.square(data_tmp-data_avg) )
   return reg_out

def load_and_regress(case_obj, var_name, var_htype, 
                     var_lev_list, first_file, num_files, 
                     ind, lon, mxlag, filt_wgt):
   print(f'    Loading data ('+hc.tcolor.CYAN+f'{var_name}'+hc.tcolor.ENDC+')...')
   data = case_obj.load_data(var_name,htype=var_htype,lev=var_lev_list, first_file=first_file,num_files=num_files)
   num_invalid = 0 # NaNs at each end of the time coordinate
   if 'lev' in data.dims : data = data.isel(lev=0)                               # Get rid of lev dimension
   data = data.resample(time='D').mean(dim='time')                               # convert to daily mean
   time = data.time                                                              # save time coordinate
   data_mavg = meridional_avg(data.values, lon.values, lon_bins, dlon)           # average meridionially - returns a numpy array
   (ntime,ncol) = data_mavg.shape
   if filter_data:                                                               # Filter in time - brute force method w/ numba
      print(f'    Filtering data ({int(fhi)}-{int(flo)} day)...')
      # data_filt = np.full(data_mavg.shape,np.nan)
      # filter_numba_2D(data_mavg,data_filt,filt_wgt,tvals0,win_width,ntime,ncol)
      data_filt = filter_numba(data_mavg,filt_wgt,tvals0,win_width)
      data_mavg = copy.deepcopy(data_filt)
      num_invalid = int( (len(filt_wgt)-1)/2 )
   ### temporal subset of data and index
   index = ind.values
   if regression_months is not None:
      time_mask = np.ones([len(time)],dtype=bool)
      # time_mask = time_mask & [ m in regression_months for m in time['month'].values ]
      time_mask = time_mask & [ m in regression_months for m in data['time.month'].values ]
      data_mavg = data_mavg[time_mask,:]
      if len(index)>len(time): index = index[:len(time)]
      index = index[time_mask]
   ### do the regression
   print('    Regressing data on index...')
   lag_coord = np.arange( -mxlag, mxlag+1, 1)
   reg = lag_regression_numba(data_mavg, index, mxlag, lag_coord, num_invalid)
   reg_da = xr.DataArray( reg, coords=[('lag',lag_coord),('lon',lon_bins)], dims=['lag','lon'] )
   reg_ds = xr.Dataset()
   reg_ds[f'{var_name}_reg'] = reg_da
   return reg_ds

# def vertically_integrate(case_obj,data):
#    ps = case_obj.load_data('PS',htype=cvar_htype[v],first_file=first_file,num_files=num_files)
#    p0 = case_obj.load_data('P0',htype=cvar_htype[v],num_files=1)
#    a = case_obj.load_data('hyai',htype=cvar_htype[v],num_files=1)
#    b = case_obj.load_data('hybi',htype=cvar_htype[v],num_files=1)
#    p = ( a*p0 + b*ps ).transpose('time','ilev','ncol')
#    num_ilev = len(a)
#    dp = xr.DataArray( p[:,1:num_ilev,:].values - p[:,0:num_ilev-1,:].values, coords=cdata.coords )
#    cdata = (cdata*dp/9.81).sum(dim='lev') / (dp/9.81).sum(dim='lev')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
lag_list, lon_list, sdata_list, cdata_list = [],[],[],[]
for c in range(num_case):
   print('  case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

   data_dir_tmp,data_sub_tmp = None, None
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]

   populate_files = False
   if recalculate_ind or recalculate_reg: populate_files = True

   case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                       data_dir=data_dir_tmp, data_sub=data_sub_tmp, 
                       populate_files=populate_files )
   # case_obj.set_coord_names(svar_name[v])
   
   # Subset the data
   if 'lat1' in locals() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
   # if 'lev'  in vars() : case_obj.lev  = lev

   # Set up coordinates for regression data
   # lon = case_obj.load_data('lon',htype='h0')
   
   # lag = xr.DataArray( np.arange(-mxlag,mxlag,1), dims='lag' )
   # lag['lag'] = ('lag',lag)

   # case_obj.hist_path = case_obj.data_dir+case_obj.name+'/hourly_2d_hist/'

   #----------------------------------------------------------------------------
   # Calculate MJO index 
   #----------------------------------------------------------------------------
   case_obj.lon1,case_obj.lon2 = ilon - iwid , ilon + iwid
   ind_file = f'{tmp_file_dir}/MJO.lag_regression.horz.v1.{case[c]}.index_{ind_var}.{tmp_file_tail}.nc'

   if recalculate_ind :

      print(f'    Loading index data ('+hc.tcolor.CYAN+f'{ind_var}'+hc.tcolor.ENDC+')...')

      ### Load index data
      ind = case_obj.load_data(ind_var,htype=ind_htype,first_file=first_file,num_files=num_files)
      
      # ### spatial average and convert to daily mean
      # ind = ind.mean(dim='ncol').resample(time='D').mean(dim='time')

      ### convert to daily mean
      ind = ind.resample(time='D').mean(dim='time')
      time = ind.time

      ### Filter in time - brute force method w/ numba
      ntime = len(ind.time)
      # ind_filt = ind.copy(data=np.full(ind.shape,np.nan))
      # filter_numba_1D(ind.values,ind_filt.values,wgt,tvals0,win_width,ntime)
      ind_filt = filter_numba(ind.values,wgt,tvals0,win_width)

      ### spatial average
      ind_filt = np.mean(ind_filt,axis=1)

      ### flip sign of OLR index
      if ind_var in ['FLNT','FLUT']: ind_filt = ind_filt * -1

      ### normalize index
      ind_filt = ind_filt - np.nanmean(ind_filt)
      ind_filt = ind_filt / np.nanstd(ind_filt)
   
      ind = xr.DataArray( ind_filt, coords=[('time',time)], dims=['time'] )
      ind.name = ind_var

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
      print(f'    Writing MJO index to file: {ind_file}')
      ind.to_netcdf(path=ind_file,mode='w')
   else:
      if recalculate_reg :
         print(f'    Reading MJO index file: {ind_file}')
         ind = xr.open_mfdataset( ind_file )[ind_var]
      else:
         print(f'    MJO index file: {ind_file}')

   #----------------------------------------------------------------------------
   # Loop through variables to calculate lagged regression field
   #----------------------------------------------------------------------------
   for v in range(num_var):
      case_obj.lon1,case_obj.lon2 = 0,360
      shd_tmp_file = f'{tmp_file_dir}/MJO.lag_regression.horz.v1.{case[c]}.{svar_name[v]}.{tmp_file_tail}.nc'
      cnt_tmp_file = f'{tmp_file_dir}/MJO.lag_regression.horz.v1.{case[c]}.{cvar_name[v]}.{tmp_file_tail}.nc'

      if filter_data:
         shd_tmp_file = shd_tmp_file.replace('.nc',f'.filt-{fhi}-{flo}.nc')
         cnt_tmp_file = cnt_tmp_file.replace('.nc',f'.filt-{fhi}-{flo}.nc')

      if recalculate_reg :
         #----------------------------------------------------------------------
         # regress data on the index
         #----------------------------------------------------------------------
         lon = case_obj.load_data('lon',htype='h1').astype(np.double)
         # area = case_obj.load_data('area').astype(np.double)

         if svar_name[v] is not None: 
            shd_reg_ds = load_and_regress(case_obj, svar_name[v], svar_htype[v],
                                          svar_lev_list[v], first_file, num_files,
                                          ind, lon, mxlag, wgt)
            print(f'    Writing regressed data to file: {shd_tmp_file}')
            shd_reg_ds.to_netcdf(path=shd_tmp_file,mode='w')

         if cvar_name[v] is not None: 
            cnt_reg_ds = load_and_regress(case_obj, cvar_name[v], cvar_htype[v],
                                          cvar_lev_list[v], first_file, num_files,
                                          ind, lon, mxlag, wgt)
            print(f'    Writing regressed data to file: {cnt_tmp_file}')
            cnt_reg_ds.to_netcdf(path=cnt_tmp_file,mode='w')
         
      else:
         if create_plot:
            if svar_name[v] is not None: 
               print(f'    Reading regressed data file: {shd_tmp_file}')
               shd_reg_ds = xr.open_mfdataset( shd_tmp_file )
            if cvar_name[v] is not None: 
               print(f'    Reading regressed data file: {cnt_tmp_file}')
               cnt_reg_ds = xr.open_mfdataset( cnt_tmp_file )
            

      if create_plot:
         lag_list.append(shd_reg_ds['lag'].values)
         lon_list.append(shd_reg_ds['lon'].values)
         if svar_name[v] is not None: sdata_list.append(shd_reg_ds[f'{svar_name[v]}_reg'].values)
         if cvar_name[v] is not None: cdata_list.append(cnt_reg_ds[f'{cvar_name[v]}_reg'].values)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# ind_ds = xr.open_mfdataset( ind_file )
# ind = ind_ds[ind_var].values
# time = np.arange(0,len(ind))

# ncl_tmp_path = os.getenv('HOME')+'/Research/E3SM/data_temp'
# ncl_ind_file = f'{ncl_tmp_path}/MJO.lag_regression.horizontal.{case[0]}.ivar_FLNT_index_lat1=-15_lat2=15_ilon=90_iwid=10.nc'
# ncl_reg_file = f'{ncl_tmp_path}/MJO.lag_regression.horizontal.{case[0]}.ivar_FLNT_FLNT_filt_lat1=-15_lat2=15_ilon=90_iwid=10.nc'
# ncl_ind_ds = xr.open_dataset( ncl_ind_file )
# ncl_reg_ds = xr.open_dataset( ncl_reg_file )
# ncl_ind = ncl_ind_ds['index'].values
# ncl_reg = ncl_reg_ds['RC'].values

# for t in range(60,60+50):
#    ratio = ind[t] / ncl_ind[t]
#    print(f'  {ind[t]:10.6}    {ncl_ind[t]:10.6}    {ratio:10.6}')

# qpres = hs.res_default()
# qpres.vpHeightF = 0.4
# qpres.xyLineThicknessF  = 6.
# qpwks = ngl.open_wks('x11','ind_test')
# t1,t2 = 365*3,365*4

# qpres.xyLineColor = 'blue'
# qplot = ngl.xy(qpwks,ncl_reg_ds['lon'].values+180,ncl_reg[0,:],qpres)
# qpres.xyLineColor = 'red'
# tmp_reg = sdata_list[0][0,:]
# # tmp_reg = tmp_reg /np.std(tmp_reg)
# ngl.overlay(qplot, ngl.xy(qpwks,lon_list[c],tmp_reg,qpres))
# ngl.draw(qplot)
# ngl.frame(qpwks)

# print(); print(sdata_list[0][:,0])
# print(); print(sdata_list[0][0,:])
# print(); print(sdata_list)
# print(); print(cdata_list)
# print()
# exit()

if not create_plot: exit('\nExiting because create_plot is False.')

if svar_name[v] is not None: 
   
   sdata_min = np.min([np.min(np.ma.masked_invalid(d)) for d in sdata_list])
   sdata_max = np.max([np.max(np.ma.masked_invalid(d)) for d in sdata_list])
   # sdata_min = np.nanmin([np.nanmin(d) for d in sdata_list])
   # sdata_max = np.nanmax([np.nanmax(d) for d in sdata_list])

if cvar_name[v] is not None: 
   cdata_min = np.min([np.min(np.ma.masked_invalid(d)) for d in cdata_list])
   cdata_max = np.max([np.max(np.ma.masked_invalid(d)) for d in cdata_list])
   # cdata_min = np.nanmin([np.nanmin(d) for d in cdata_list])
   # cdata_max = np.nanmax([np.nanmax(d) for d in cdata_list])

for c in range(num_case):
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   def set_clev(res_in, data_min, data_max, var_name, nlev=11):
      aboutZero = False
      if var_name in ['U','V'] : aboutZero = True
      (cmin,cmax,cint) = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev,returnLevels=False, aboutZero=aboutZero )         
      clev = np.linspace(cmin,cmax,num=nlev)
      res_in.cnLevels = clev
      res_in.cnLevelSelectionMode = 'ExplicitLevels'
      return clev

   def neg_dash_contours(res_in, clev):
      dsh = np.zeros((len(clev)),'i')
      for k in range(len(clev)):
         if (clev[k] < 0.): dsh[k] = 6
      # rlist                       = ngl.Resources()
      # rlist.cnLineDashPatterns    = dsh
      # rlist.cnMonoLineDashPattern = False
      # ngl.set_values(res_in,rlist)
      res_in.cnLineDashPatterns    = dsh
      res_in.cnMonoLineDashPattern = False
      res_in.cnFillOn  = False
      res_in.cnLinesOn = True
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   # res.trXMinF = bin_ds['bins'].min().values
   # res.trXMaxF = bin_ds['bins'].max().values

   # res.tiYAxisString = ''

   # print(); print(lag_list[c].shape)
   if svar_name[v] is not None: hc.print_stat(sdata_list[c],name=f'\n{svar_name[v]} regression',compact=True)
   if cvar_name[v] is not None: hc.print_stat(cdata_list[c],name=f'\n{cvar_name[v]} regression',compact=True)
   # exit()

   res.sfYArray = lag_list[c]
   res.sfXArray = lon_list[c]

   sres = copy.deepcopy(res)
   cres = copy.deepcopy(res)
   if svar_name[v] is not None: 
      slev = set_clev(sres, sdata_min, sdata_max, svar_name[v])
   if cvar_name[v] is not None: 
      clev = set_clev(cres, cdata_min, cdata_max, cvar_name[v])
      neg_dash_contours(cres, clev)

   ip = v*num_case+c if var_x_case else c*num_var+v

   plot[ip] = ngl.contour(wks, sdata_list[c] ,sres)

   if cvar_name[v] is not None: 
      ngl.overlay(plot[ip], ngl.contour(wks, cdata_list[c] ,cres))

   var_str = svar_name[v]
   # if var_str=="PRECT" : var_str = "Precipitation"
   hs.set_subtitles(wks, plot[ip], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [num_var,num_case] if var_x_case else [num_case,num_var]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
