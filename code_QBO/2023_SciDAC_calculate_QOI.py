import os, errno, subprocess as sp, numpy as np, xarray as xr, copy, string, dask, glob
import QBO_diagnostic_methods as QBO_methods
# Here's a handy conda environment:
# conda create --name pyn_env --channel conda-forge pyngl xarray numba dask scipy cftime scikit-learn netcdf4 cmocean
#---------------------------------------------------------------------------------------------------
# case_list, gweff_list, cfrac_list, hdpth_list = [],[],[],[]
# def add_case(case_in,gweff=None,cfrac=None,hdpth=None):
#   global case_list, gweff_list, cfrac_list, hdpth_list
#   case_list.append(case_in)
#   gweff_list.append(gweff)
#   cfrac_list.append(cfrac)
#   hdpth_list.append(hdpth)

# case_list,case_dir,case_sub = [],[],[]
# def add_case(case_in,p=None,s=None):
#   global case_list,case_dir,case_sub
#   case_list.append(case_in)
#   case_dir.append(p)
#   case_sub.append(s)

case_name,case_list,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
run_length_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case_list.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   run_length_list.append(r)
#---------------------------------------------------------------------------------------------------

mach = 'chrysalis' # chrysalis / perlmutter

# add_case('ERA-Interim')
# add_case('ERA5')

### jack's QBO tuning ensemble @ LCRC
# add_case('20230215.amip.NGD_v3atm.QBOv01.chrysalis', gweff=0.40, cfrac=10  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv02.chrysalis', gweff=0.35, cfrac=10  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv03.chrysalis', gweff=0.35, cfrac=20  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv04.chrysalis', gweff=0.20, cfrac=10  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv05.chrysalis', gweff=0.35, cfrac=10  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv06.chrysalis', gweff=0.20, cfrac=15  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv07.chrysalis', gweff=0.20, cfrac=15  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv08.chrysalis', gweff=0.10, cfrac=20  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv09.chrysalis', gweff=0.10, cfrac=20  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv10.chrysalis', gweff=0.09, cfrac=20  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv11.chrysalis', gweff=0.07, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv12.chrysalis', gweff=0.05, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv13.chrysalis', gweff=0.40, cfrac= 5  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv14.chrysalis', gweff=0.90, cfrac=20  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv15.chrysalis', gweff=0.20, cfrac=10  , hdpth=1.50)
# add_case('20230215.amip.NGD_v3atm.QBOv16.chrysalis', gweff=0.10, cfrac=10  , hdpth=1.50)
# add_case('20230215.amip.NGD_v3atm.QBOv17.chrysalis', gweff=0.20, cfrac=20  , hdpth=1.50)
# add_case('20230215.amip.NGD_v3atm.QBOv18.chrysalis', gweff=0.20, cfrac=20  , hdpth=1.25)
# add_case('20230215.amip.NGD_v3atm.QBOv21.chrysalis', gweff=0.10, cfrac=12.5, hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv22.chrysalis', gweff=0.20, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv23.chrysalis', gweff=0.10, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv24.chrysalis', gweff=0.20, cfrac=25  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv25.chrysalis', gweff=0.10, cfrac=25  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv26.chrysalis', gweff=0.10, cfrac=25  , hdpth=0.40)
# add_case('20230215.amip.NGD_v3atm.QBOv27.chrysalis', gweff=0.10, cfrac=25  , hdpth=0.30)


### L80 ensemble @ NERSC
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_1.00',  n='',p=tmp_path,s=tmp_sub) # default
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.01.CF_0.01.HD_0.70',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.05.CF_0.25.HD_0.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.09.CF_0.20.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_0.10.HD_1.50',n='',p=tmp_path,s=tmp_sub)#
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_0.25.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_0.10.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_0.15.HD_0.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_0.20.HD_1.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_0.20.HD_1.50',n='',p=tmp_path,s=tmp_sub)#
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_0.25.HD_0.50',n='',p=tmp_path,s=tmp_sub)#
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_0.10.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_0.10.HD_0.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_0.20.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.40.CF_0.10.HD_1.00',n='',p=tmp_path,s=tmp_sub)#
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.59.CF_0.41.HD_0.55',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_0.07.HD_1.35',n='',p=tmp_path,s=tmp_sub)#
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.70.CF_0.25.HD_0.90',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.80.CF_0.01.HD_1.15',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.90.CF_0.20.HD_0.25',n='',p=tmp_path,s=tmp_sub)
### add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_0.30.HD_0.63',n='',p=tmp_path,s=tmp_sub) # outlier - keeps crashing

### L80 ensemble @ NERSC
# gweff_list.append(0.35); cfrac_list.append(10); hdpth_list.append(1.00) #  0 - use defaults
# gweff_list.append(0.10); cfrac_list.append(10); hdpth_list.append(1.50) #  2 L72-run # 16
# gweff_list.append(0.35); cfrac_list.append(20); hdpth_list.append(1.00) #  3 L72-run #  3
# gweff_list.append(0.20); cfrac_list.append(20); hdpth_list.append(1.50) #  4 L72-run # 17
# gweff_list.append(0.10); cfrac_list.append(25); hdpth_list.append(0.25) #  5 L72-run # 25
# gweff_list.append(0.20); cfrac_list.append(25); hdpth_list.append(0.50) #  6 L72-run # 22
# gweff_list.append(0.35); cfrac_list.append(10); hdpth_list.append(0.50) #  7 L72-run #  5
# gweff_list.append(0.35); cfrac_list.append(10); hdpth_list.append(0.25) #  8 L72-run #  2
# gweff_list.append(0.20); cfrac_list.append(10); hdpth_list.append(1.00) #  9 L72-run #  4
# gweff_list.append(0.20); cfrac_list.append(15); hdpth_list.append(0.50) # 10 L72-run #  7
# gweff_list.append(0.09); cfrac_list.append(20); hdpth_list.append(0.25) # 11 L72-run # 10  
# gweff_list.append(0.20); cfrac_list.append(20); hdpth_list.append(1.25) # 12 L72-run # 18  
# gweff_list.append(0.05); cfrac_list.append(25); hdpth_list.append(0.50) # 13 L72-run # 12  
# gweff_list.append(0.80); cfrac_list.append( 1); hdpth_list.append(1.15) # 14 L72-run # 31  
# gweff_list.append(0.90); cfrac_list.append(20); hdpth_list.append(0.25) # 15 L72-run # 14  
# gweff_list.append(0.59); cfrac_list.append(41); hdpth_list.append(0.55) # 16 L72-run # 40  
# gweff_list.append(0.01); cfrac_list.append( 1); hdpth_list.append(0.70) # 17 L72-run # 28  
# gweff_list.append(0.40); cfrac_list.append(10); hdpth_list.append(1.00) # 18 L72-run #  1  
# gweff_list.append(0.70); cfrac_list.append(25); hdpth_list.append(0.90) # 19 L72-run # 30  
# gweff_list.append(0.60); cfrac_list.append( 7); hdpth_list.append(1.35) # 20 L72-run # 38  

# ### gweff_list.append(0.10); cfrac_list.append(30); hdpth_list.append(0.63) #  1 L72-run # 44 - outlier - can't finish?
# for n in range(len(hdpth_list)):
#   e = gweff_list[n]
#   c = cfrac_list[n]
#   h = hdpth_list[n]
#   tcase = '.'.join(['E3SM','2023-SCIDAC','ne30pg2_EC30to60E2r2','AMIP',f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}'])
#   case_list.append(tcase)


### v3alpha04 AMIP bi-grid
# add_case('20230922.v3alpha04.F20TR.chrysalis'                 ,c='red' ,d=1,r='30yr',p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev'   ,s='post/atm/180x360_aave/ts/monthly')
# add_case('20231012.v3alpha04_bigrid_L80_QBO1.F20TR.chrysalis' ,c='blue',d=1,r='30yr',p='/lcrc/group/acme/ac.benedict/E3SMv3_dev',s='post/atm/180x360_aave/ts/monthly')

### v3alpha04 PI-CPL tri-grid
# add_case('20230924.v3alpha04_trigrid.piControl.chrysalis'       ,c='red' ,d=0,r='30yr',p='/lcrc/group/e3sm2/ac.xzheng/E3SMv3_dev' ,s='post/atm/180x360_aave/ts/monthly')
# add_case('20231023.v3alpha04-L80QBO.trigrid.piControl.chrysalis',c='blue',d=0,r='30yr',p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev'   ,s='post/atm/180x360_aave/ts/monthly')


### gweff_list.append(0.10); cfrac_list.append(30); hdpth_list.append(0.63) #  1 L72-run # 44 - outlier - can't finish?

### additional 6 cases following initial ensemble
# gweff_list.append(0.18); cfrac_list.append(14); hdpth_list.append(0.52)
# gweff_list.append(0.35); cfrac_list.append(11); hdpth_list.append(0.66)
# gweff_list.append(0.06); cfrac_list.append(13); hdpth_list.append(0.59)
# gweff_list.append(0.22); cfrac_list.append( 8); hdpth_list.append(0.48)
# gweff_list.append(0.52); cfrac_list.append(16); hdpth_list.append(0.89)
# gweff_list.append(0.13); cfrac_list.append(10); hdpth_list.append(0.82)

### new runs to extend ensemble (2024)
# gweff_list.append(0.79); cfrac_list.append(12); hdpth_list.append(0.72)
# gweff_list.append(0.78); cfrac_list.append(13); hdpth_list.append(0.67)
# gweff_list.append(0.77); cfrac_list.append( 9); hdpth_list.append(0.94)
# gweff_list.append(0.55); cfrac_list.append(10); hdpth_list.append(0.38)
# gweff_list.append(0.63); cfrac_list.append( 8); hdpth_list.append(0.62)
# gweff_list.append(0.14); cfrac_list.append(10); hdpth_list.append(1.37)
# gweff_list.append(0.13); cfrac_list.append(22); hdpth_list.append(0.56)

### new runs to extend ensemble - 2024-05-02
# gweff_list.append(0.31); cfrac_list.append(11); hdpth_list.append(1.39)
# gweff_list.append(0.70); cfrac_list.append(21); hdpth_list.append(0.31)
# gweff_list.append(0.81); cfrac_list.append(20); hdpth_list.append(0.39)
# gweff_list.append(0.11); cfrac_list.append(18); hdpth_list.append(0.87)
# gweff_list.append(0.11); cfrac_list.append(23); hdpth_list.append(0.86)

# gweff_list.append(0.13); cfrac_list.append(16); hdpth_list.append(1.22)
# gweff_list.append(0.33); cfrac_list.append(17); hdpth_list.append(0.72)
# gweff_list.append(0.40); cfrac_list.append(15); hdpth_list.append(1.17)
# gweff_list.append(0.43); cfrac_list.append(23); hdpth_list.append(0.60)
# gweff_list.append(0.47); cfrac_list.append(17); hdpth_list.append(0.46)

# gweff_list.append(0.60); cfrac_list.append(15); hdpth_list.append(1.35)
# gweff_list.append(0.63); cfrac_list.append(22); hdpth_list.append(0.78)
# gweff_list.append(0.67); cfrac_list.append(22); hdpth_list.append(1.18)
# gweff_list.append(0.69); cfrac_list.append(17); hdpth_list.append(0.64)
# gweff_list.append(0.77); cfrac_list.append(18); hdpth_list.append(0.93)

# gweff_list.append(0.86); cfrac_list.append(17); hdpth_list.append(0.29)
# gweff_list.append(0.45); cfrac_list.append(20); hdpth_list.append(1.40)
# gweff_list.append(0.60); cfrac_list.append(12); hdpth_list.append(1.03)

# for n in range(len(hdpth_list)):
#   e = gweff_list[n]
#   c = cfrac_list[n]
#   h = hdpth_list[n]
#   tcase = '.'.join(['E3SM','2023-SCIDAC','ne30pg2_EC30to60E2r2','AMIP',f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}'])
#   case_list.append(tcase)

# add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L72')

# add_case('ERA-Interim')
# add_case('ERA5')


# remap_lev = np.array([ 1., 2., 3., 5., 7., 10., 20., 30., 40., 50., 70., 100., 125., 150.])
remap_lev = np.array([ 1., 2., 3., 5., 7., 10., 20., 30., 50., 70., 100., 125., 150.])

lat1,lat2 = -5,5
# lat1,lat2 = -6,6

pow_spec_lev = 20.


# run_length = '30yr' # 10yr / 20yr / 30yr

if mach=='chrysalis' : 
  scratch_out_path = '/lcrc/group/e3sm/ac.whannah/scratch/chrys/SciDAC_QOI'
  obs_root = '/lcrc/group/e3sm/diagnostics/observations/Atm'
if mach=='perlmutter': 
  scratch_out_path = '/pscratch/sd/w/whannah/e3sm_scratch/SciDAC_QOI'
  obs_root = '/global/cfs/cdirs/e3sm/diagnostics/observations/Atm'

#---------------------------------------------------------------------------------------------------
# calculate some things that will be needed below
period = np.concatenate( (np.arange(2.0, 33.0), np.arange(34.0, 100.0, 2.0)), axis=0 )
period = xr.DataArray(period, coords={'period':period}).assign_attrs({'units':'months'})
#---------------------------------------------------------------------------------------------------
print()
for c,case in enumerate(case_list):
  
  run_length = run_length_list[c]

  # scratch_root     = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
  # # scratch_in_path  = f'{scratch_root}/{case}/post/atm/180x360/ts/monthly/10yr/'
  # scratch_in_path  = f'{scratch_root}/{case}/post/atm/180x360/ts/monthly/{run_length}'
  # scratch_out_path = '/pscratch/sd/w/whannah/e3sm_scratch/SciDAC_QOI'

  # scratch_in_path  = f'{case_dir[c]}/{case}/{case_sub[c]}/{run_length}'

  scratch_root     = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'

  if case=='E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L72':
    scratch_root = '/global/cfs/cdirs/m4310/whannah/E3SM'

  # scratch_in_path  = f'{scratch_root}/{case}/post/atm/180x360/ts/monthly/10yr/'
  scratch_in_path  = f'{scratch_root}/{case}/post/atm/180x360/ts/monthly/{run_length}'
  scratch_out_path = '/pscratch/sd/w/whannah/e3sm_scratch/SciDAC_QOI'

  

  ### Jack's data @ LCRC
  # scratch_in_path = '/lcrc/group/e3sm/ac.cchen/20230215.amip.NGD_v3atm.QBOv01.chrysalis/archive/atm/hist/QBO'
  # scratch_out_path = '/lcrc/group/e3sm/ac.whannah/E3SMv3_dev/SciDAC_QOI'
  # input_file_path = f'{scratch_in_path}/{case}.U.mon.rgr.nc' 
  #-----------------------------------------------------------------------------
  # print(scratch_in_path)
  # exit()
  #-----------------------------------------------------------------------------
  if not os.path.exists(scratch_out_path): os.mkdir(scratch_out_path)
  #-----------------------------------------------------------------------------
  # Load the data
  if not case in ['ERA-Interim','ERA5']:
    # scratch_out_path = f'{scratch_root}/{case}/post/qbo/'
    # input_file_path  = f'{scratch_in_path}/U_198401_199312.nc' 

    # input_file_name  = f'{scratch_in_path}/U_198401_199312.nc' 
    if run_length=='10yr': input_file_name  = f'{scratch_in_path}/U_198401_199312.nc' 
    # if run_length=='10yr': input_file_name  = f'{scratch_in_path}/U_198501_199412.nc' 
    if run_length=='20yr': input_file_name  = f'{scratch_in_path}/U_198401_200312.nc' 
  else:
    # obs_root = '/lcrc/group/e3sm/diagnostics/observations/Atm'
    obs_root = '/global/cfs/cdirs/e3sm/diagnostics/observations/Atm'
    if case=='ERA-Interim': input_file_name = f'{obs_root}/time-series/ERA-Interim/ua_197901_201612.nc'
    if case=='ERA5':        input_file_name = f'{obs_root}/time-series/ERA5/ua_197901_201912.nc'

  #-----------------------------------------------------------------------------
  def chk_file_exists(f): 
    if not os.path.isfile(f): 
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), f)
  if isinstance(input_file_path, list):
    for input_file_name in input_file_path:
      chk_file_exists(input_file_name)
  else:  
    chk_file_exists(input_file_path)
  #-----------------------------------------------------------------------------
  ds = xr.open_mfdataset( input_file_path )
  ds.load()
  if case in ['ERA-Interim','ERA5']:
    if run_length=='10yr': ds = ds.isel(time=slice(0,10*12))
    if run_length=='20yr': ds = ds.isel(time=slice(0,20*12))
    if run_length=='30yr': ds = ds.isel(time=slice(0,30*12))
  #-----------------------------------------------------------------------------
  lon      = ds['lon']
  lat      = ds['lat']
  lon_bnds = ds['lon_bnds']
  lat_bnds = ds['lat_bnds']
  if 'time' in lon.dims:
    lon      = lon.isel(time=0)
    lat      = lat.isel(time=0)
  if 'time' in lon_bnds.dims:
    lon_bnds = lon_bnds.isel(time=0)
    lat_bnds = lat_bnds.isel(time=0)
  if 'lat' in lon_bnds.dims: lon_bnds = lon_bnds.isel(lat=0)
  if 'lon' in lat_bnds.dims: lat_bnds = lat_bnds.isel(lon=0)
  area = QBO_methods.calculate_area(lon.values,lat.values,lon_bnds.values,lat_bnds.values)
  area = xr.DataArray( area, coords=[ds['lat'],ds['lon']] )
  #-----------------------------------------------------------------------------
  if case in ['ERA-Interim','ERA5']:
    data = ds['ua']
    data = data.rename({'plev':'lev'})
    data['lev'] = data['lev']/1e2
    # data = data.sel(lev=remap_lev)
    # print()
    # print(data)
    # print()
    # exit()
  else:
    data = QBO_methods.interpolate_to_pressure(ds,'U',remap_lev,'PS',interp_type=2,extrap_flag=False)
  lev = data['lev']
  #-----------------------------------------------------------------------------
  # calculate area weighted average on spatial subset
  data = data.sel(lat=slice(lat1,lat2))
  area = area.sel(lat=slice(lat1,lat2))
  data_avg = (data*area).sum(dim=('lon','lat')) / area.sum(dim=('lon','lat'))
  #-----------------------------------------------------------------------------
  # Calculate quantities of interest
  psd_spec, pow_spec = QBO_methods.get_psd_from_deseason(data_avg.sel(lev=pow_spec_lev).values, period.values)
  
  # tmp_data = ( data_avg.sel(lev=10) + data_avg.sel(lev=20) ) / 2.
  # print(); print(tmp_data); print()
  # psd_spec, pow_spec = QBO_methods.get_psd_from_deseason(tmp_data.values, period.values)

  data_avg = data_avg.sel(lev=remap_lev)
  lev = data_avg['lev']
  psd_prof, amp_prof = QBO_methods.get_20to40month_fft_amplitude(data_avg.values, lev.values)
  
  # convert to xarray DataArray
  pow_spec = xr.DataArray( pow_spec, coords=[period] )
  amp_prof = xr.DataArray( amp_prof, coords=[lev.assign_attrs({'units':'mb'})] )

  # print(); print(pow_spec)
  # print(); print(amp_prof)
  # exit()
  #-----------------------------------------------------------------------------
  # write QOI data to file
  output_file_name = f'{scratch_out_path}/{case}.QBO_QOI.{run_length}.nc'

  print(f'  writing QOI data to: {output_file_name}')

  ds_out = xr.Dataset()
  ds_out['pow_spec']     = pow_spec
  ds_out['amp_prof']     = amp_prof
  ds_out['pow_spec_lev'] = xr.DataArray(pow_spec_lev).assign_attrs({'units':'mb'}) 
  ds_out['lat1']         = lat1
  ds_out['lat2']         = lat2

  if 'gweff_list' in globals(): 
    if gweff_list[c] is not None: 
      ds_out['effgw']  = gweff_list[c]

  if 'gweff_list' in globals(): 
    if cfrac_list[c] is not None: 
      ds_out['CF']     = cfrac_list[c]

  if 'gweff_list' in globals(): 
    if hdpth_list[c] is not None: 
      ds_out['hdepth'] = hdpth_list[c]
  
  ds_out.to_netcdf(path=output_file_name,mode='w')

#---------------------------------------------------------------------------------------------------
print()
print('Done!')
print()
