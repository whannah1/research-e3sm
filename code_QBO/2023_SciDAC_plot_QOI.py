import os, subprocess as sp, numpy as np, xarray as xr, copy, string, dask, numba
import QBO_diagnostic_methods as QBO_methods
# Here's a handy conda environment:
# conda create --name pyn_env --channel conda-forge pyngl xarray numba dask scipy cftime scikit-learn netcdf4 cmocean
#---------------------------------------------------------------------------------------------------
# case_list, effgw_list, CF_list, hdepth_list = [],[],[],[]
# def add_case(case_in,effgw=None,CF=None,hdepth=None):
#    global case_list, effgw_list, CF_list, hdepth_list
#    case_list.append(case_in)
#    effgw_list.append(effgw)
#    CF_list.append(CF)
#    hdepth_list.append(hdepth)

# case_list, gweff_list, cfrac_list, hdpth_list = [],[],[],[]
# def add_case(case_in,gweff=None,cfrac=None,hdpth=None):
#    global case_list, gweff_list, cfrac_list, hdpth_list
#    case_list.append(case_in)
#    gweff_list.append(gweff)
#    cfrac_list.append(cfrac)
#    hdpth_list.append(hdpth)
case_name,case_list,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
run_length_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case_list.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   run_length_list.append(r)
#---------------------------------------------------------------------------------------------------

fig_type = 'png'
fig_file = 'SciDAC_QOI'


# add_case('20230215.amip.NGD_v3atm.QBOv01.chrysalis',gweff=0.40, cfrac=10  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv02.chrysalis',gweff=0.35, cfrac=10  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv03.chrysalis',gweff=0.35, cfrac=20  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv04.chrysalis',gweff=0.20, cfrac=10  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv05.chrysalis',gweff=0.35, cfrac=10  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv06.chrysalis',gweff=0.20, cfrac=15  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv07.chrysalis',gweff=0.20, cfrac=15  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv08.chrysalis',gweff=0.10, cfrac=20  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv09.chrysalis',gweff=0.10, cfrac=20  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv10.chrysalis',gweff=0.09, cfrac=20  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv11.chrysalis',gweff=0.07, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv12.chrysalis',gweff=0.05, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv13.chrysalis',gweff=0.40, cfrac= 5  , hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv14.chrysalis',gweff=0.90, cfrac=20  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv15.chrysalis',gweff=0.20, cfrac=10  , hdpth=1.50)
# add_case('20230215.amip.NGD_v3atm.QBOv16.chrysalis',gweff=0.10, cfrac=10  , hdpth=1.50)
# add_case('20230215.amip.NGD_v3atm.QBOv17.chrysalis',gweff=0.20, cfrac=20  , hdpth=1.50)
# add_case('20230215.amip.NGD_v3atm.QBOv18.chrysalis',gweff=0.20, cfrac=20  , hdpth=1.25)
# add_case('20230215.amip.NGD_v3atm.QBOv21.chrysalis',gweff=0.10, cfrac=12.5, hdpth=1.00)
# add_case('20230215.amip.NGD_v3atm.QBOv22.chrysalis',gweff=0.20, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv23.chrysalis',gweff=0.10, cfrac=25  , hdpth=0.50)
# add_case('20230215.amip.NGD_v3atm.QBOv24.chrysalis',gweff=0.20, cfrac=25  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv25.chrysalis',gweff=0.10, cfrac=25  , hdpth=0.25)
# add_case('20230215.amip.NGD_v3atm.QBOv26.chrysalis',gweff=0.10, cfrac=25  , hdpth=0.40)
# add_case('20230215.amip.NGD_v3atm.QBOv27.chrysalis',gweff=0.10, cfrac=25  , hdpth=0.30)

# ### L80 ensemble @ NERSC
# gweff_list.append(0.35); cfrac_list.append(10); hdpth_list.append(1.00) #  0 - use defaults
# gweff_list.append(0.10); cfrac_list.append(10); hdpth_list.append(1.50) #  2 L72-run # 16
# gweff_list.append(0.35); cfrac_list.append(20); hdpth_list.append(1.00) #  3 L72-run #  3
# gweff_list.append(0.20); cfrac_list.append(20); hdpth_list.append(1.50) #  4 L72-run # 17
# gweff_list.append(0.10); cfrac_list.append(25); hdpth_list.append(0.25) #  5 L72-run # 25
# gweff_list.append(0.20); cfrac_list.append(25); hdpth_list.append(0.50) #  6 L72-run # 22
# gweff_list.append(0.35); cfrac_list.append(10); hdpth_list.append(0.50) #  7 L72-run #  5
# gweff_list.append(0.35); cfrac_list.append(10); hdpth_list.append(0.25) #  8 L72-run #  2
# gweff_list.append(0.20); cfrac_list.append(10); hdpth_list.append(1.00) #  9 L72-run #  4
# gweff_list.append(0.20); cfrac_list.append(15); hdpth_list.append(0.50) # 10 L72-run #  7
# gweff_list.append(0.09); cfrac_list.append(20); hdpth_list.append(0.25) # 11 L72-run # 10  
# gweff_list.append(0.20); cfrac_list.append(20); hdpth_list.append(1.25) # 12 L72-run # 18  
# gweff_list.append(0.05); cfrac_list.append(25); hdpth_list.append(0.50) # 13 L72-run # 12  
# gweff_list.append(0.80); cfrac_list.append( 1); hdpth_list.append(1.15) # 14 L72-run # 31  
# gweff_list.append(0.90); cfrac_list.append(20); hdpth_list.append(0.25) # 15 L72-run # 14  
# gweff_list.append(0.59); cfrac_list.append(41); hdpth_list.append(0.55) # 16 L72-run # 40  
# gweff_list.append(0.01); cfrac_list.append( 1); hdpth_list.append(0.70) # 17 L72-run # 28  
# gweff_list.append(0.40); cfrac_list.append(10); hdpth_list.append(1.00) # 18 L72-run #  1  
# gweff_list.append(0.70); cfrac_list.append(25); hdpth_list.append(0.90) # 19 L72-run # 30  
# gweff_list.append(0.60); cfrac_list.append( 7); hdpth_list.append(1.35) # 20 L72-run # 38  
# ### gweff_list.append(0.10); cfrac_list.append(30); hdpth_list.append(0.63) #  1 L72-run # 44 - outlier - can't finish?
# for n in range(len(hdpth_list)):
#   e = gweff_list[n]
#   c = cfrac_list[n]
#   h = hdpth_list[n]
#   tcase = '.'.join(['E3SM','2023-SCIDAC','ne30pg2_EC30to60E2r2','AMIP',f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}'])
#   case_list.append(tcase)


### L80 ensemble
tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_10.HD_1.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_20.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_20.HD_1.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_25.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_15.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.09.CF_20.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_20.HD_1.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.05.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.80.CF_01.HD_1.15',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.90.CF_20.HD_0.25',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.59.CF_41.HD_0.55',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.01.CF_01.HD_0.70',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.40.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.70.CF_25.HD_0.90',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_07.HD_1.35',n='',p=tmp_path,s=tmp_sub) # top-tier

# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_15.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.05.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.90.CF_20.HD_0.25',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.40.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_07.HD_1.35',n='',p=tmp_path,s=tmp_sub) # top-tier

# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.18.CF_14.HD_0.52',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_11.HD_0.66',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.06.CF_13.HD_0.59',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.22.CF_08.HD_0.48',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.52.CF_16.HD_0.89',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.13.CF_10.HD_0.82',n='',p=tmp_path,s=tmp_sub)


# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.50',n='',p=tmp_path,s=tmp_sub,c='red') # top-tier
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.55.CF_10.HD_0.38',n='',p=tmp_path,s=tmp_sub,c='blue')

### new runs to extend ensemble - 2024-05-02
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.31.CF_11.HD_1.39',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.70.CF_21.HD_0.31',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.81.CF_20.HD_0.39',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.11.CF_18.HD_0.87',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.11.CF_23.HD_0.86',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.13.CF_16.HD_1.22',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.33.CF_17.HD_0.72',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.40.CF_15.HD_1.17',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.43.CF_23.HD_0.60',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.47.CF_17.HD_0.46',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_15.HD_1.35',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.63.CF_22.HD_0.78',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.67.CF_22.HD_1.18',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.69.CF_17.HD_0.64',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.77.CF_18.HD_0.93',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.86.CF_17.HD_0.29',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.45.CF_20.HD_1.40',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_12.HD_1.03',n='',p=tmp_path,s=tmp_sub)

### v3alpha04 AMIP bi-grid
add_case('20230922.v3alpha04.F20TR.chrysalis'                ,c='red' ,d=1,r='30yr',p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev'   ,s='post/atm/180x360_aave/ts/monthly')
add_case('20231012.v3alpha04_bigrid_L80_QBO1.F20TR.chrysalis',c='blue',d=1,r='30yr',p='/lcrc/group/acme/ac.benedict/E3SMv3_dev',s='post/atm/180x360_aave/ts/monthly')

### v3alpha04 PI-CPL tri-grid
add_case('20230924.v3alpha04_trigrid.piControl.chrysalis'       ,c='red' ,d=0,r='30yr',p='/lcrc/group/e3sm2/ac.xzheng/E3SMv3_dev' ,s='post/atm/180x360_aave/ts/monthly')
add_case('20231023.v3alpha04-L80QBO.trigrid.piControl.chrysalis',c='blue',d=0,r='30yr',p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev'   ,s='post/atm/180x360_aave/ts/monthly')



add_case('ERA-Interim',r='30yr',c='black')
add_case('ERA5'       ,r='30yr',c='gray')


# run_length = '10yr' # 10yr / 20yr / 30yr


# scratch_path = '/lcrc/group/e3sm/ac.whannah/E3SMv3_dev/SciDAC_QOI'
scratch_path = '/lcrc/group/e3sm/ac.whannah/scratch/chrys/SciDAC_QOI'
# scratch_path = '/pscratch/sd/w/whannah/e3sm_scratch/SciDAC_QOI'

#---------------------------------------------------------------------------------------------------
# Load the data
lev_list = []
amp_list = []
per_list = []
pow_list = []

lev_list2 = []
amp_list2 = []
per_list2 = []
pow_list2 = []

for c,case in enumerate(case_list):
  print(case)
  #-----------------------------------------------------------------------------
  # Load the data

  run_length = run_length_list[c]
  
  # file_suffix = '.QBO_QOI.nc'
  # if '20230215.amip.NGD_v3atm' in case: file_suffix = '.QOI.nc'
  file_suffix = f'.QBO_QOI.{run_length}.nc'

  QOI_file_name = f'{scratch_path}/{case}{file_suffix}'

  ds = xr.open_dataset( QOI_file_name )
  pow_spec     = ds['pow_spec']
  amp_prof     = ds['amp_prof']
  period       = ds['period']
  lev          = ds['lev']
  pow_spec_lev = ds['pow_spec_lev']
  lat1         = ds['lat1']
  lat2         = ds['lat2']
  # if 'effgw' in ds.vars:  effgw  = ds['effgw']  else: effgw  = None
  # if 'CF' in ds.vars:     CF     = ds['CF']     else: CF     = None
  # if 'hdepth' in ds.vars: hdepth = ds['hdepth'] else: hdepth = None
  
  lev_list.append( lev.values )
  amp_list.append( amp_prof.values )
  per_list.append( period.values )
  pow_list.append( pow_spec.values )


  # # add 20 year analysis for comparison
  # run_length = '20yr'
  # file_suffix = f'.QBO_QOI.{run_length}.nc'
  # QOI_file_name = f'{scratch_path}/{case}{file_suffix}'
  # ds = xr.open_dataset( QOI_file_name )
  # pow_spec     = ds['pow_spec']
  # amp_prof     = ds['amp_prof']
  # period       = ds['period']
  # lev          = ds['lev']
  # pow_spec_lev = ds['pow_spec_lev']
  # lat1         = ds['lat1']
  # lat2         = ds['lat2']
  # lev_list2.append( lev.values )
  # amp_list2.append( amp_prof.values )
  # per_list2.append( period.values )
  # pow_list2.append( pow_spec.values )
  
#---------------------------------------------------------------------------------------------------


#---------------------------------------------------------------------------------------------------
# creat plot with PyNGL
import ngl
wkres = ngl.Resources()
wkres.wkColorMap = "cmocean_phase"
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*2
# define common resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.xyLineThicknessF             = 6
# resources for amplitude profile
amp_res = copy.deepcopy(res)
amp_res.vpHeightF = 0.5
amp_res.trYReverse = True
amp_res.xyYStyle ="Log"
amp_res.tiYAxisString = 'Pressure (hPa)'
amp_res.tiXAxisString = 'Amplitude (m/s)'
amp_res.trXMinF = 0
amp_res.trXMaxF = np.max([np.nanmax(d) for d in amp_list]) + 2
amp_res.trYMinF = 1e0
amp_res.trYMaxF = 1e2
# resources for power spectra
pow_res = copy.deepcopy(res)
pow_res.vpHeightF = 0.5
pow_res.tiXAxisString = 'Period (months)'
pow_res.tiYAxisString = 'Amplitude'
pow_res.trXMinF = 0
pow_res.trXMaxF = 50
pow_res.trYMinF = 0
pow_res.trYMaxF = np.max([np.nanmax(d) for d in pow_list]) + 2
# define line colors

color_map = ngl.get_MDfloat_array(wks,"wkColorMap")
num_color = len(color_map[:,0])
# clr = np.linspace(30,num_color,len(case_list), dtype=int)
clr = np.linspace(30,num_color,len(case_list), dtype=int)

# overlay lines for all cases
for c,case in enumerate(case_list):
    amp_res.xyLineColor      = clr[c]
    pow_res.xyLineColor      = clr[c]
    amp_res.xyDashPattern    = dsh[c]
    pow_res.xyDashPattern    = dsh[c]

    # use different settings for obs
    # if case=='ERA-Interim':
    #   amp_res.xyLineColor='black'; amp_res.xyLineThicknessF=6; amp_res.xyDashPattern=0
    #   pow_res.xyLineColor='black'; pow_res.xyLineThicknessF=6; pow_res.xyDashPattern=0
    # if case=='ERA5':
    #   amp_res.xyLineColor='gray'; amp_res.xyLineThicknessF=6; amp_res.xyDashPattern=0
    #   pow_res.xyLineColor='gray'; pow_res.xyLineThicknessF=6; pow_res.xyDashPattern=0
    tplot_amp = ngl.xy(wks, amp_list[c], lev_list[c], amp_res)
    tplot_pow = ngl.xy(wks, per_list[c], pow_list[c], pow_res)
    
    if c==0 :
      plot[0] = tplot_amp
      plot[1] = tplot_pow
    else:
      ngl.overlay(plot[0],tplot_amp)
      ngl.overlay(plot[1],tplot_pow)

    # # overlay 20-yr data
    # amp_res.xyDashPattern = 1
    # pow_res.xyDashPattern = 1
    # if case=='ERA-Interim' or case=='ERA5': continue
    # tplot_amp = ngl.xy(wks, amp_list2[c], lev_list2[c], amp_res)
    # tplot_pow = ngl.xy(wks, per_list2[c], pow_list2[c], pow_res)  
    # ngl.overlay(plot[0],tplot_amp)
    # ngl.overlay(plot[1],tplot_pow)

# Finalize plot
pnl_res = ngl.Resources()
pnl_res.nglPanelXWhiteSpacePercent = 5
pnl_res.nglPanelYWhiteSpacePercent = 5
pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
pnl_res.nglPanelFigureStringsJust        = "TopLeft"
pnl_res.nglPanelFigureStringsFontHeightF = 0.01
ngl.panel(wks,plot[0:len(plot)],[1,2],pnl_res)
ngl.end()
# trim white space
fig_file = fig_file+".png"
if os.path.isfile(fig_file) :
  os.system(f'convert -trim +repage {fig_file} {fig_file}')
  print(f'\n{fig_file}\n')
else:
  raise FileNotFoundError(f'\n{fig_file} does not exist?!\n')

    
#---------------------------------------------------------------------------------------------------
