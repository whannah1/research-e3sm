import os, ngl, copy, glob, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------

src_root = '/global/cfs/cdirs/m4310/wandiyu/ERA5'
dst_root = '/global/cfs/cdirs/m4310/whannah/ERA5'

yr1 = 1980
yr2 = 2022

#---------------------------------------------------------------------------------------------------
for y in range(yr1,yr2+1):
   for m in range(1,12+1):
      print(); print(f'    yr/mn: {y} / {m}')
      #----------------------------------------------------------------------
      src_file = f'{src_root}/ERA5_6hrly_{y:04}{m:02}_tem.nc'
      dst_file = f'{dst_root}/ERA5_monthly_{y:04}{m:02}_tem.nc'
      #----------------------------------------------------------------------
      # open dataset
      ds = xr.open_mfdataset(src_file).load()
      #----------------------------------------------------------------------
      # residual calculation
      ds['dudt'] = ds['u'].differentiate(coord='time',datetime_unit='s')
      ds['residual'] =  ds['dudt'] - ds['utendvtem'] - ds['utendwtem']-ds['utendepfd']
      #----------------------------------------------------------------------
      # monthly average of entire dataset
      ds_out = ds.resample(time='ME').mean('time')    # Convert to monthly mean
      #----------------------------------------------------------------------
      # print(); print(ds)
      # print(); print(ds_out)
      # print(); exit()
      #----------------------------------------------------------------------
      print(f'    {dst_file}')
      ds_out.to_netcdf(path=dst_file,mode='w')
#---------------------------------------------------------------------------------------------------
