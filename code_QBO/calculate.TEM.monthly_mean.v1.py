import os, ngl, copy, glob, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
tmp_date_list = []
# tmp_date_list.append('1983-01-01') # phase 1 - pi*1/4
# tmp_date_list.append('1993-04-01')
# tmp_date_list.append('2002-07-01')
# tmp_date_list.append('2022-10-01')
# tmp_date_list.append('1986-10-01') # phase 1 - pi*3/4
# tmp_date_list.append('1995-07-01')
# tmp_date_list.append('2000-04-01')
# tmp_date_list.append('2009-01-01')
# tmp_date_list.append('1982-01-01') # phase 1 - pi*5/4
# tmp_date_list.append('1987-04-01')
# tmp_date_list.append('2014-07-01')
# tmp_date_list.append('2021-10-01')
# tmp_date_list.append('1984-10-01') # phase 1 - pi*7/4
# tmp_date_list.append('1994-07-01')
# tmp_date_list.append('2006-04-01')
# tmp_date_list.append('2013-01-01')
#---------------------------------------------------------------------------------------------------
compset = 'F20TR'
grid    = 'ne30pg2_r05_IcoswISC30E3r5'
ens_id  = '2024-SCIDAC-PCOMP-TEST' # initial test to work out logistical issues
def get_case_name(e,c,h,d): return'.'.join(['E3SM',ens_id,grid,f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}',f'{d}'])
#---------------------------------------------------------------------------------------------------
case_list = []
gweff_list, cfrac_list, hdpth_list = [],[],[]
start_date_list = []
def add_case(e,c,h,d):
   gweff_list.append(e)
   cfrac_list.append(c)
   hdpth_list.append(h)
   start_date_list.append(d)
   case_list.append( get_case_name(e,c,h,d) )
#---------------------------------------------------------------------------------------------------
tdate = '1983-01-01'
add_case(e=0.35,c=10,h=0.50,d=tdate) #  <<< v3 default
add_case(e=0.12,c=16,h=0.48,d=tdate) # prev surrogate optimum
tdate = '1993-04-01'
add_case(e=0.35,c=10,h=0.50,d=tdate) #  <<< v3 default
add_case(e=0.12,c=16,h=0.48,d=tdate) # prev surrogate optimum
#---------------------------------------------------------------------------------------------------
# # build list of cases with all dates
# for date in tmp_date_list:
#    add_case(e=0.35,c=10,h=0.50,d=date) #  <<< v3 default
#    add_case(e=0.12,c=16,h=0.48,d=date) # prev surrogate optimum
#    # add_case(e=0.09,c=20,h=0.25,d=date) # no QBO at all
#    # add_case(e=0.70,c=21,h=0.31,d=date) # QBO is too fast

#---------------------------------------------------------------------------------------------------

scratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# scratch = '/global/cfs/cdirs/m3312/whannah/2022-QBO-TEST'

htype = 'h1'

debug = False

#---------------------------------------------------------------------------------------------------
num_case = len(case_list)
for c in range(num_case):
   print(hc.tcolor.CYAN+'  case: '+case_list[c]+hc.tcolor.ENDC)

   substr = 'tem'
   # substr = 'tem_y' # alt version using Yaga's code

   search_str = f'{htype}.{substr}.'
   remap_str  = 'remap_90x180'

   # file_path = f'{scratch}/{case[c]}/data_{remap_str}_prs/*.eam.{search_str}*'
   file_path = f'{scratch}/{case_list[c]}/data_{remap_str}_tem/*.eam.{search_str}*'
   file_list = sorted(glob.glob(file_path))
   # file_list.remove(file_list[-1]) # last file is empty

   # print(); print(file_list)

   if file_list==[]:
      print(); print(file_path)
      exit('ERROR - no files found!')

   # find bounds for loop over years
   yr_pos = file_list[0].find(search_str) + len(search_str)
   yr1 = int(file_list[ 0][yr_pos:yr_pos+4])
   yr2 = int(file_list[-1][yr_pos:yr_pos+4])

   if debug:
      print(); print(f'Year range: {yr1} - {yr2}'); print()

   for y in range(yr1,yr2+1):
      for m in range(1,12+1):

         # file_path = f'{scratch}/{case[c]}/data_{remap_str}_prs/*.eam.{search_str}{y:04}-{m:02}*'
         file_path = f'{scratch}/{case_list[c]}/data_{remap_str}_tem/*.eam.{search_str}{y:04}-{m:02}*'
         file_list = sorted(glob.glob(file_path))

         if len(file_list)<28: continue

         print(); print(f'    yr/mn: {y} / {m}')

         # print(file_list)
         # continue
         # exit()

         if debug:
            for ff in file_list: print(ff)
         #----------------------------------------------------------------------
         # define output file
         f_out = file_list[0][:file_list[0].find(search_str)]+f'h0.{substr}.{y:04}-{m:02}.{remap_str}.nc'
         #----------------------------------------------------------------------
         # open dataset
         ds = xr.open_mfdataset(file_list).load()
         #----------------------------------------------------------------------
         # residual calculation
         ds['dudt'] = ds['u'].differentiate(coord='time',datetime_unit='s')
         ds['residual'] =  ds['dudt'] - ds['utendvtem'] - ds['utendwtem']-ds['utendepfd']
         #----------------------------------------------------------------------
         # monthly average of entire dataset
         ds_out = ds.resample(time='ME').mean('time')    # Convert to monthly mean
         #----------------------------------------------------------------------
         # print(); print(ds)
         # print(); print(ds_out)
         # print()

         print(f'    {f_out}')
         ds_out.to_netcdf(path=f_out,mode='w')
         
         if debug: break
      if debug: break

if debug: print(); print('Go and make sure the file(s) looks ok!'); exit()
