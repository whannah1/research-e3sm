import os, ngl, copy, xarray as xr, numpy as np, cmocean, warnings, numba
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------
# var, var_str, file_type_list = [], [], []
# def add_var(var_name,file_type=None,n=''):
#   var.append(var_name)
#   file_type_list.append(file_type)
#   var_str.append(n)
#---------------------------------------------------------------------------------------------------
# var_x,var_z,var_x_str,var_z_str = [],[],[],[]
# def add_vars(vx,vz,vx_name=None,vz_name=None): 
#    # if lev==-1: lev = np.array([0])
#    vx_str_tmp = vx if vx_name is None else vx_name
#    vz_str_tmp = vz if vz_name is None else vz_name
#    var_x.append(vx)
#    var_z.append(vz)
#    var_x_str.append(vx_str_tmp)
#    var_z_str.append(vz_str_tmp)
#---------------------------------------------------------------------------------------------------
# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,875,900])
lev = np.array([50,100,150,200,250,300,350,400,450,500,550,600])
# lev = np.array([200,300,400,500])
# lev = np.array([100,200,300])
#---------------------------------------------------------------------------------------------------

### 2024 SciDAC heating test
# add_case('E3SM.2024-SCIDAC-heating-test-00.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

add_case('E3SM.2024-SCIDAC-heating-test-01.F2010',n='E3SM',d=0,c='red',   p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
# add_case('E3SM.2024-SCIDAC-heating-test-02.F2010',n='E3SM',d=1,c='blue',  p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
add_case('E3SM.2024-SCIDAC-heating-test-03.F2010',n='E3SM',d=0,c='green', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
add_case('E3SM.2024-SCIDAC-heating-test-04.F2010',n='E3SM',d=1,c='blue',  p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')


#---------------------------------------------------------------------------------------------------
# lat1,lat2 = -10,10
# lat1,lat2 = -15,15
lat1,lat2 = -30,30
#---------------------------------------------------------------------------------------------------

# num_plot_col = len(var_z)
# num_plot_col = 1

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.gwd-bin.v1'

htype,first_file,num_files = 'h1',0,10
# htype,first_file,num_files = 'h2',0,2

print_stats        = False

var_x_case = False

recalculate = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = 1#len(var_z)
num_case = len(case)

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in locals(): lev = np.array([0])

wkres = ngl.Resources() ; npix = 2048*2 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
# plot = [None]*num_var


res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 12
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

x_tick_vals = [1e-2,1e-1,1e0,1e1,1e2,1e3]

res.tmXBMode   = "Explicit"
res.tmXBValues = np.array(x_tick_vals)
res.tmXBLabels = x_tick_vals
res.xyXStyle = 'Log'

res.tiYAxisString = f'Percent occurrence of total data (ntime*ncol)'
# res.tiXAxisString = f'Max dT/dt [K/day] over {np.max(lev)}-{np.min(lev)} mb'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
# for v in range(num_var):
v = 0
hc.printline()
# print(' '*2+f'var: {hc.tcolor.GREEN}{var_z[v]}{hc.tcolor.ENDC}')
# cnt_list = []
# lev_list = []
# data_list = []
# binx_list = []
# biny_list = []

hdth_bin_list = []
maxq_bin_list = []

hdth_cnt_list = []
maxq_cnt_list = []
for c in range(num_case):
   #-------------------------------------------------------------------------
   temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
   tmp_file = f'{temp_dir}/QBO.heating-profile-bin.v4.{case[c]}'
   # tmp_file += f'.{var_x[v]}.{var_z[v]}'
   tmp_file += f'.htype_{htype}.f0_{first_file}.nf_{num_files}'
   if 'lat1' in vars(): tmp_file += f'.lat1_{lat1}.lat2_{lat2}'
   if 'lon1' in vars(): tmp_file += f'.lon1_{lat1}.lon2_{lat2}'
   tmp_file = tmp_file + f'.nc'
   print(' '*4+f'case: {hc.tcolor.CYAN}{case[c]:50}{hc.tcolor.ENDC}  {tmp_file}')
   #----------------------------------------------------------------------------
   if recalculate :
      #-------------------------------------------------------------------------
      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      case_obj = he.Case( name=case[c], atm_comp='eam', 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2
      #-------------------------------------------------------------------------
      hdth = case_obj.load_data('HDEPTH', htype=htype,first_file=first_file,num_files=num_files)
      maxq = case_obj.load_data('MAXQ0',  htype=htype,first_file=first_file,num_files=num_files)

      #-------------------------------------------------------------------------
      hc.print_stat(hdth,name='hdth ',indent=' '*4,compact=True)
      hc.print_stat(maxq,name='maxq', indent=' '*4,compact=True)
      # exit()
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      bin_min, bin_spc, nbin_log, bin_spc_log = 5e-3, 1e-3, 88, 10
      # Log mode - logarithmically spaced bins that increase in width by bin_spc_log [%]
      bin_log_wid = np.zeros(nbin_log)
      bin_log_ctr = np.zeros(nbin_log)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin_log):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      hdth_nbin_x = nbin_log
      hdth_bin_x_coord = xr.DataArray( bin_log_ctr )
      hdth_bin_log_ctr = bin_log_ctr
      hdth_bin_log_wid = bin_log_wid

      # bin_min, bin_spc, nbin_log, bin_spc_log = 5e-2, 1e-2, 44, 20
      bin_min, bin_spc, nbin_log, bin_spc_log = 5e-2, 1e-2, 88, 10
      # Log mode - logarithmically spaced bins that increase in width by bin_spc_log [%]
      bin_log_wid = np.zeros(nbin_log)
      bin_log_ctr = np.zeros(nbin_log)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin_log):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      maxq_nbin_x = nbin_log
      maxq_bin_x_coord = xr.DataArray( bin_log_ctr )
      maxq_bin_log_ctr = bin_log_ctr
      maxq_bin_log_wid = bin_log_wid
      #-------------------------------------------------------------------------
      shape, dims, coords = (hdth_nbin_x), ('bin_x'), [hdth_bin_x_coord]
      hdth_cnt = xr.DataArray( np.zeros(shape,dtype=hdth.dtype), dims=dims, coords=coords )
      shape, dims, coords = (maxq_nbin_x), ('bin_x'), [maxq_bin_x_coord]
      maxq_cnt = xr.DataArray( np.zeros(shape,dtype=maxq.dtype), dims=dims, coords=coords )
      #-------------------------------------------------------------------------
      for bx in range(hdth_nbin_x):
         hdth_bin_bot_x = hdth_bin_log_ctr[bx] - hdth_bin_log_wid[bx]/2.
         maxq_bin_bot_x = maxq_bin_log_ctr[bx] - maxq_bin_log_wid[bx]/2.
         hdth_bin_top_x = hdth_bin_log_ctr[bx] + hdth_bin_log_wid[bx]/2.
         maxq_bin_top_x = maxq_bin_log_ctr[bx] + maxq_bin_log_wid[bx]/2.
         hdth_condition = (hdth.values>=hdth_bin_bot_x) & (hdth.values<hdth_bin_top_x)
         maxq_condition = (maxq.values>=maxq_bin_bot_x) & (maxq.values<maxq_bin_top_x)

         # with warnings.catch_warnings():
         #    warnings.simplefilter("ignore", category=RuntimeWarning)
         #    if np.sum(hdth_condition)>0: hdth_cnt[bx] = np.sum( hdth_condition )
         #    if np.sum(maxq_condition)>0: maxq_cnt[bx] = np.sum( maxq_condition )

         if np.sum(hdth_condition)>0: hdth_cnt[bx] = np.sum( hdth_condition )
         if np.sum(maxq_condition)>0: maxq_cnt[bx] = np.sum( maxq_condition )

      total_cnt = len(hdth['time']) * len(hdth['ncol'])

      hdth_cnt = (hdth_cnt/total_cnt)*1e2
      maxq_cnt = (maxq_cnt/total_cnt)*1e2

      hdth_bin_list.append(hdth_bin_x_coord.values)
      maxq_bin_list.append(maxq_bin_x_coord.values)

      hdth_cnt_list.append(hdth_cnt.values)
      maxq_cnt_list.append(maxq_cnt.values)
      
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
   #    print('writing to file: '+tmp_file)
   #    bin_ds.to_netcdf(path=tmp_file,mode='w')
   # else:
   #    bin_ds = xr.open_dataset( tmp_file )

#-----------------------------------------------------------------------------
tres = copy.deepcopy(res)

# cnt_min  = np.nanmin([np.nanmin(x) for x in cnt_list])
# cnt_max  = np.nanmax([np.nanmax(x) for x in cnt_list])
# data_min = np.nanmin([np.nanmin(x) for x in data_list])
# data_max = np.nanmax([np.nanmax(x) for x in data_list])

#-----------------------------------------------------------------------------
plot = [None]*2

for c in range(num_case):

   tres.xyDashPattern=dsh[c]
   tres.xyLineColor=clr[c]

   tres.tiXAxisString = '[km]'
   hdth_plot = ngl.xy(wks, hdth_bin_list[c], hdth_cnt_list[c], tres)

   tres.tiXAxisString = '[K]'
   maxq_plot = ngl.xy(wks, maxq_bin_list[c], maxq_cnt_list[c], tres)

   if c==0:
      plot[0] = hdth_plot
      plot[1] = maxq_plot
   else:
      ngl.overlay(plot[0],hdth_plot)
      ngl.overlay(plot[1],maxq_plot)

hs.set_subtitles(wks, plot[0], '', '', 'HDEPTH', font_height=0.01)
hs.set_subtitles(wks, plot[1], '', '', 'MAXQ0',  font_height=0.01)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# layout = [num_var,num_case] if var_x_case else [num_case,num_var]
layout = [1,len(plot)]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
