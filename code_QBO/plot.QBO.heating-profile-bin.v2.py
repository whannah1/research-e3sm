import os, ngl, copy, xarray as xr, numpy as np, cmocean, warnings
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------
# var, var_str, file_type_list = [], [], []
# def add_var(var_name,file_type=None,n=''):
#   var.append(var_name)
#   file_type_list.append(file_type)
#   var_str.append(n)
#---------------------------------------------------------------------------------------------------
var_x,var_y,var_x_str,var_y_str = [],[],[],[]
def add_vars(vx,vy,vx_name=None,vy_name=None): 
   # if lev==-1: lev = np.array([0])
   vx_str_tmp = vx if vx_name is None else vx_name
   vy_str_tmp = vy if vy_name is None else vy_name
   var_x.append(vx)
   var_y.append(vy)
   var_x_str.append(vx_str_tmp)
   var_y_str.append(vy_str_tmp)
#---------------------------------------------------------------------------------------------------
# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([50,100,150,200,300,400,500,600,700,750,800,850,875,900])
# lev = np.array([200,300,400,500])
lev = np.array([100,150,200,250,300])
#---------------------------------------------------------------------------------------------------

### 2024 SciDAC heating test
add_case('E3SM.2024-SCIDAC-heating-test-00.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

#---------------------------------------------------------------------------------------------------

add_vars(vx='DTCOND',vy='TTEND_CLUBB'     )

# add_vars(vx='ZMDT',vy='P3DT'            )
# add_vars(vx='ZMDT',vy='TTEND_CLUBB'     )
# add_vars(vx='ZMDT',vy='CLOUDFRAC_CLUBB' )


# bin_min_x, bin_max_x, bin_spc_x = 20, 100, 2
bin_min_x, bin_max_x, bin_spc_x = None, None, None
bin_min_y, bin_max_y, bin_spc_y = None, None, None

bin_min, bin_spc, nbin_log, bin_spc_log = 1e-2, 1e-2, 44, 20
x_tick_vals = [1e-2,1e-1,1e0,1e1,1e2,1e3]
#---------------------------------------------------------------------------------------------------
# lat1,lat2 = -10,10
# lat1,lat2 = -15,15
lat1,lat2 = -30,30
#---------------------------------------------------------------------------------------------------

num_plot_col = len(var_y)
# num_plot_col = 2


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.heating-profile-bin.v2'


# htype,first_file,num_files = 'h2',10,1
htype,first_file,num_files = 'h2',0,30


overlay_vars = True

use_snapshot,ss_t  = False,0

omit_bot,bot_k     = False,-2
omit_top,top_k     = False,30

print_stats        = False

var_x_case = False

recalculate = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = len(var_y)
num_case = len(case)

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in locals(): lev = np.array([0])

wkres = ngl.Resources() ; npix = 2048*2 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*num_case)

cres_common = hs.res_contour()

sres_common = hs.res_contour_fill()
sres_common.tmYLLabelFontHeightF         = 0.008
sres_common.tmXBLabelFontHeightF         = 0.008
sres_common.lbLabelFontHeightF           = 0.01
sres_common.lbLabelAngleF                = -45
# sres_common.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
sres_common.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
# sres_common.cnFillPalette = "MPL_viridis"

sres_common.cnFillMode    = 'RasterFill'
# sres_common.tiXAxisString = 'Log10(Obj Size) [km]'
# sres_common.tiYAxisString = 'Count Weighted Histogram'

# sres_common.tiYAxisString = 'Pressure [hPa]'
# sres_common.trYReverse = True

sres_common.tmYLMode = "Explicit"
sres_common.tmYLValues = np.array(x_tick_vals)
sres_common.tmYLLabels = x_tick_vals

sres_common.tmXBMode = "Explicit"
sres_common.tmXBValues = np.array(x_tick_vals)
sres_common.tmXBLabels = x_tick_vals

#---------------------------------------------------------------------------------------------------
def bin_ZbyXY( data_y,data_x,
               bin_min_x,bin_max_x,bin_spc_x,
               bin_min_y,bin_max_y,bin_spc_y,):
   #-----------------------------------------------------------------------------
   # Log mode - logarithmically spaced bins that increase in width by bin_spc_log [%]
   bin_log_wid = np.zeros(nbin_log)
   bin_log_ctr = np.zeros(nbin_log)
   bin_log_ctr[0] = bin_min
   bin_log_wid[0] = bin_spc
   for b in range(1,nbin_log):
      bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
      bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
   nbin_x = nbin_log
   bin_x_coord = xr.DataArray( bin_log_ctr )
   #-----------------------------------------------------------------------------
   nbin_y = nbin_x
   bin_y_coord = bin_x_coord
   #-----------------------------------------------------------------------------  
   # nbin_x = np.round( ( bin_max_x - bin_min_x + bin_spc_x )/bin_spc_x ).astype(int)
   # bins_x = np.linspace(bin_min_x,bin_max_x,nbin_x)
   # bin_x_coord = xr.DataArray( bins_x )
   #-----------------------------------------------------------------------------
   # nbin_y = np.round( ( bin_max_y - bin_min_y + bin_spc_y )/bin_spc_y ).astype(int)
   # bins_y = np.linspace(bin_min_y,bin_max_y,nbin_y)
   #-----------------------------------------------------------------------------
   # bins_y = data_y
   # nbin_y = len(bins_y)
   # bin_y_coord = xr.DataArray( bins_y )
   #-----------------------------------------------------------------------------
   shape = (nbin_y,nbin_x)
   dims  = ('bin_y','bin_x')
   # coords = {'bin_x':bin_x_coord,'bin_y':bin_y_coord}
   coords = [bin_y_coord,bin_x_coord]
   # bin_val = xr.DataArray( np.full( shape,np.nan,dtype=data_z.dtype), dims=dims, coords=coords )
   bin_cnt = xr.DataArray( np.zeros(shape,       dtype=data_x.dtype), dims=dims, coords=coords )
   #-----------------------------------------------------------------------------
   # Loop through bins
   for bx in range(nbin_x):
      for by in range(nbin_y):
         bin_bot_x = bin_log_ctr[bx] - bin_log_wid[bx]/2.
         bin_top_x = bin_log_ctr[bx] + bin_log_wid[bx]/2.
         bin_bot_y = bin_log_ctr[by] - bin_log_wid[by]/2.
         bin_top_y = bin_log_ctr[by] + bin_log_wid[by]/2.
         # bin_bot_y = bins_y[b]
         # bin_top_y = bins_y[b+1]
         # bin_bot_x = bin_min_x - bin_spc_x/2. + bin_spc_x*(bx  )
         # bin_top_x = bin_min_x - bin_spc_x/2. + bin_spc_x*(bx+1)
         # bin_bot_y = bin_min_y - bin_spc_y/2. + bin_spc_y*(by  )
         # bin_top_y = bin_min_y - bin_spc_y/2. + bin_spc_y*(by+1)
         condition_x = ( data_x.values >=bin_bot_x ) & ( data_x.values  <bin_top_x )
         condition_y = ( data_y.values >=bin_bot_y ) & ( data_y.values  <bin_top_y )
         condition_xy = condition_x & condition_y

         # condition_xy = ( data_x.isel(lev=by)>=bin_bot_x ) & ( data_x.isel(lev=by)<bin_top_x )

         if np.sum(condition_xy)>0:
            with warnings.catch_warnings():
               warnings.simplefilter("ignore", category=RuntimeWarning)
               # bin_val[by,bx] = data_z.where(condition_xy).mean(skipna=True)
               # bin_val[by,bx] = data_z.isel(lev=by).where(condition_xy).mean(skipna=True)
               bin_cnt[by,bx] = np.sum( condition_xy )
   #-----------------------------------------------------------------------------
   bin_ds = xr.Dataset()
   # bin_ds['bin_val'] = bin_val
   bin_ds['bin_cnt'] = bin_cnt
   bin_ds['bin_pct'] = bin_cnt/bin_cnt.sum()*1e2
   #-----------------------------------------------------------------------------
   return bin_ds
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(' '*2+f'var: {hc.tcolor.GREEN}{var_y[v]}{hc.tcolor.ENDC}')
   cnt_list = []
   data_list = []
   binx_list = []
   biny_list = []
   for c in range(num_case):
      #-------------------------------------------------------------------------
      temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
      tmp_file = f'{temp_dir}/QBO.heating-profile-bin.v2.{case[c]}.{var_x[v]}.{var_y[v]}'
      tmp_file += f'htype_{htype}.f0_{first_file}.nf_{num_files}'
      if 'lat1' in vars(): tmp_file += f'.lat1_{lat1}.lat2_{lat2}'
      if 'lon1' in vars(): tmp_file += f'.lon1_{lat1}.lon2_{lat2}'
      tmp_file = tmp_file + f'.nc'
      print(' '*4+f'case: {hc.tcolor.CYAN}{case[c]:50}{hc.tcolor.ENDC}  {tmp_file}')
      #-------------------------------------------------------------------------
      if recalculate :
         #----------------------------------------------------------------------
         data_dir_tmp,data_sub_tmp = None, None
         if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         if case_sub[c] is not None: data_sub_tmp = case_sub[c]
         case_obj = he.Case( name=case[c], atm_comp='eam', 
                             data_dir=data_dir_tmp, data_sub=data_sub_tmp )
         if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
         if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2
         #----------------------------------------------------------------------
         data_x = case_obj.load_data(var_x[v],htype=htype,first_file=first_file,num_files=num_files,lev=lev)
         data_y = case_obj.load_data(var_y[v],htype=htype,first_file=first_file,num_files=num_files,lev=lev)
         #----------------------------------------------------------------------
         # unit conversion
         def unit_conversion(var,data):
            if var=='DTCOND'     : data = data*86400
            if var=='ZMDT'       : data = data*86400
            if var=='TTEND_CLUBB': data = data*86400
            if var=='P3DT'       : data = data*86400
            return data
         data_x = unit_conversion(var_x[v],data_x)
         data_y = unit_conversion(var_y[v],data_y)
         #----------------------------------------------------------------------
         # if var_x[v]=='DTCOND' and var_y[v] in ['ZMDT','P3DT','TTEND_CLUBB']:
         #    data_z = xr.where( data_x>0.01, data_z/data_x, np.nan)
         #----------------------------------------------------------------------
         hc.print_stat(data_x,name=f'data_x',indent=' '*4,compact=True)
         hc.print_stat(data_y,name=f'data_y',indent=' '*4,compact=True)
         #----------------------------------------------------------------------
         # bin the data
         bin_ds = bin_ZbyXY( data_y, data_x, \
                             bin_min_x, bin_max_x, bin_spc_x, \
                             bin_min_y, bin_max_y, bin_spc_y, )
         #----------------------------------------------------------------------
         print('writing to file: '+tmp_file)
         bin_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( tmp_file )

      # print(); print(bin_ds)
      # print(); print(bin_ds['bin_cnt'])
      # print(); print(bin_ds['bin_cnt'][0,:].values)
      # print(); print(bin_ds['bin_y'][0].values)
      # print()
      # exit()

      # data_list.append(bin_ds['bin_val'].values)
      with warnings.catch_warnings():
         warnings.simplefilter("ignore", category=RuntimeWarning)
         log_cnt = np.log10(bin_ds['bin_pct'].values)
      log_cnt = np.where(np.isinf(log_cnt), np.nan, log_cnt)
      cnt_list.append(log_cnt)

      data_list.append(bin_ds['bin_pct'].values)

      binx_list.append(bin_ds['bin_x'].values)
      biny_list.append(bin_ds['bin_y'].values)

   #-----------------------------------------------------------------------------
   cres = copy.deepcopy(cres_common)
   sres = copy.deepcopy(sres_common)

   sres.tiXAxisString = var_x_str[v]
   sres.tiYAxisString = var_y_str[v]

   cres.sfXArray, cres.sfYArray = binx_list[0], biny_list[0]
   sres.sfXArray, sres.sfYArray = binx_list[0], biny_list[0]

   cnt_min  = np.nanmin([np.nanmin(x) for x in cnt_list])
   cnt_max  = np.nanmax([np.nanmax(x) for x in cnt_list])
   data_min = np.nanmin([np.nanmin(x) for x in data_list])
   data_max = np.nanmax([np.nanmax(x) for x in data_list])

   sres.cnLevels = np.logspace( -2, 0, num=20)#.round(decimals=2)

   # sres.cnLevels = np.logspace( -2, 1, num=30)#.round(decimals=2)
   
   # if var_y[v]=='CLOUDFRAC_CLUBB': sres.cnLevels = np.arange(0.01,0.99+0.02,0.02)
   # if var_y[v]=='P3DT'           : sres.cnLevels = np.logspace( -2, 1, num=30)#.round(decimals=2)

   # if var_y[v]=='VapWaterPath': sres.cnLevels = np.arange(36,50+2,2)
   # if var_y[v]=='LiqWaterPath': sres.cnLevels = np.arange(0.1,1+0.1,0.1)*1
   # if var_y[v]=='IceWaterPath': sres.cnLevels = np.arange(0.1,1+0.1,0.1)*1
   # # if var_y[v]=='precip_total_surf_mass_flux': sres.cnLevels = np.arange(10,60+10,10)
   
   

   nlev = 11
   (cmin,cmax,cint) = ngl.nice_cntr_levels(cnt_min, cnt_max, cint=None, max_steps=nlev, returnLevels=False, aboutZero=False )
   cres.cnLevels = np.linspace(cmin,cmax,num=nlev)
   cres.cnLevelSelectionMode = 'ExplicitLevels'
   # res.cnLevels = np.arange(1.4,3.4+0.4,0.4)

   sres.cnLevelSelectionMode = 'ExplicitLevels'
   if not hasattr(sres,'cnLevels') : 
      nlev = 11
      (cmin,cmax,cint) = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, returnLevels=False, aboutZero=False )
      sres.cnLevels = np.linspace(cmin,cmax,num=nlev)
    
   #-----------------------------------------------------------------------------
   for c in range(num_case):
      ip = v*num_case+c if var_x_case else c*num_var+v

      # cplot = ngl.contour(wks, np.ma.masked_invalid(cnt_list[c]), cres)
      splot = ngl.contour(wks, np.ma.masked_invalid(data_list[c]), sres)

      plot[ip] = splot
      # ngl.overlay( plot[ip], cplot )
      # hs.set_subtitles(wks, plot[ip], case_name[c], '', var_y_str[v], font_height=0.008)
      hs.set_subtitles(wks, plot[ip], case_name[c], '', '', font_height=0.008)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
layout = [num_var,num_case] if var_x_case else [num_case,num_var]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
