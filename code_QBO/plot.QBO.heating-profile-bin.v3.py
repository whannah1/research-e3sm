import os, ngl, copy, xarray as xr, numpy as np, cmocean, warnings
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------
# var, var_str, file_type_list = [], [], []
# def add_var(var_name,file_type=None,n=''):
#   var.append(var_name)
#   file_type_list.append(file_type)
#   var_str.append(n)
#---------------------------------------------------------------------------------------------------
var_x,var_z,var_x_str,var_z_str = [],[],[],[]
def add_vars(vx,vz,vx_name=None,vz_name=None): 
   # if lev==-1: lev = np.array([0])
   vx_str_tmp = vx if vx_name is None else vx_name
   vz_str_tmp = vz if vz_name is None else vz_name
   var_x.append(vx)
   var_z.append(vz)
   var_x_str.append(vx_str_tmp)
   var_z_str.append(vz_str_tmp)
#---------------------------------------------------------------------------------------------------
# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([50,100,150,200,300,400,500,600,700,750,800,850,875,900])
lev = np.array([50,100,150,200,250,300,350,400,450,500,550,600])
# lev = np.array([200,300,400,500])
# lev = np.array([100,200,300])
#---------------------------------------------------------------------------------------------------

### 2024 SciDAC heating test
add_case('E3SM.2024-SCIDAC-heating-test-00.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

#---------------------------------------------------------------------------------------------------

# add_vars(vx='DTCOND',vz='ZMDT'       )
# add_vars(vx='DTCOND',vz='P3DT'       )
# add_vars(vx='DTCOND',vz='TTEND_CLUBB')


# bin_min_x, bin_max_x, bin_spc_x = 20, 100, 2
# bin_min_x, bin_max_x, bin_spc_x = None, None, None
# bin_min_y, bin_max_y, bin_spc_y = None, None, None

# bin_min, bin_spc, nbin_log, bin_spc_log = 1e-2, 1e-2, 44, 20
# x_tick_vals = [1e-2,1e-1,1e0,1e1,1e2,1e3]
#---------------------------------------------------------------------------------------------------
# lat1,lat2 = -10,10
# lat1,lat2 = -15,15
lat1,lat2 = -30,30
#---------------------------------------------------------------------------------------------------

# num_plot_col = len(var_z)
# num_plot_col = 1


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.heating-profile-bin.v3'


htype,first_file,num_files = 'h2',0,30


overlay_vars = True

use_snapshot,ss_t  = False,0

omit_bot,bot_k     = False,-2
omit_top,top_k     = False,30

print_stats        = False

var_x_case = False

recalculate = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = 1#len(var_z)
num_case = len(case)

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in locals(): lev = np.array([0])

wkres = ngl.Resources() ; npix = 2048*2 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
# plot = [None]*2#(num_var*num_case)


res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 12
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

res.tiYAxisString = 'Pressure [hPa]'
res.trYReverse = True
# res.xyYStyle = 'Log

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
# for v in range(num_var):
v = 0
hc.printline()
# print(' '*2+f'var: {hc.tcolor.GREEN}{var_z[v]}{hc.tcolor.ENDC}')
cnt_list = []
lev_list = []
data_list = []
binx_list = []
biny_list = []
for c in range(num_case):
   #-------------------------------------------------------------------------
   temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
   tmp_file = f'{temp_dir}/QBO.heating-profile-bin.v3.{case[c]}'
   # tmp_file += f'.{var_x[v]}.{var_z[v]}'
   tmp_file += f'.htype_{htype}.f0_{first_file}.nf_{num_files}'
   if 'lat1' in vars(): tmp_file += f'.lat1_{lat1}.lat2_{lat2}'
   if 'lon1' in vars(): tmp_file += f'.lon1_{lat1}.lon2_{lat2}'
   tmp_file = tmp_file + f'.nc'
   print(' '*4+f'case: {hc.tcolor.CYAN}{case[c]:50}{hc.tcolor.ENDC}  {tmp_file}')
   #-------------------------------------------------------------------------
   if recalculate :
      #----------------------------------------------------------------------
      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      case_obj = he.Case( name=case[c], atm_comp='eam', 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2
      #----------------------------------------------------------------------
      zmdt  = case_obj.load_data('ZMDT',       htype=htype,first_file=first_file,num_files=num_files,lev=lev)
      clbdt = case_obj.load_data('TTEND_CLUBB',htype=htype,first_file=first_file,num_files=num_files,lev=lev)
      #----------------------------------------------------------------------
      zmdt  = zmdt *86400
      clbdt = clbdt*86400
      #----------------------------------------------------------------------
      hc.print_stat(zmdt ,name=f'zmdt ',indent=' '*4,compact=True)
      hc.print_stat(clbdt,name=f'clbdt',indent=' '*4,compact=True)
      #----------------------------------------------------------------------
      #----------------------------------------------------------------------
      #----------------------------------------------------------------------
      shape,dims,coords = (len(zmdt['lev'])), ('lev'), [xr.DataArray( zmdt['lev'] )]
      bin_val = xr.DataArray( np.full( shape,np.nan,dtype=zmdt.dtype), dims=dims, coords=coords )
      bin_cnt = xr.DataArray( np.zeros(shape,       dtype=zmdt.dtype), dims=dims, coords=coords )
      #----------------------------------------------------------------------
      for by in range(len(zmdt['lev'])):
         # condition1 = 
         # condition = clbdt.isel(lev=by)>0 & zmdt.isel(lev=by)==0
         # condition = clbdt.isel(lev=by)>0 & zmdt.isel(lev=by)>0.01
         condition = ( clbdt.isel(lev=by) > zmdt.isel(lev=by) ) & ( zmdt.isel(lev=by)>0 )
         # condition = condition1 & condition2
         if np.sum(condition)>0:
            with warnings.catch_warnings():
               warnings.simplefilter("ignore", category=RuntimeWarning)
               bin_val[by] = clbdt.isel(lev=by).where(condition).mean(skipna=True)
               bin_cnt[by] = np.sum( condition )
      #----------------------------------------------------------------------
      bin_ds = xr.Dataset()
      bin_ds['bin_val'] = bin_val
      bin_ds['bin_cnt'] = bin_cnt
      bin_ds['bin_pct'] = bin_cnt/bin_cnt.sum()*1e2
      #----------------------------------------------------------------------
      #----------------------------------------------------------------------
      #----------------------------------------------------------------------
      print('writing to file: '+tmp_file)
      bin_ds.to_netcdf(path=tmp_file,mode='w')
   else:
      bin_ds = xr.open_dataset( tmp_file )

   data_list.append(bin_ds['bin_val'].values)
   
   cnt_list.append(bin_ds['bin_pct'].values)

   # with warnings.catch_warnings():
   #    warnings.simplefilter("ignore", category=RuntimeWarning)
   #    log_cnt = np.log10(bin_ds['bin_pct'].values)
   # log_cnt = np.where(np.isinf(log_cnt), np.nan, log_cnt)
   # cnt_list.append(log_cnt)
   # binx_list.append(bin_ds['bin_x'].values)
   # biny_list.append(bin_ds['bin_y'].values)
   lev_list.append(bin_ds['lev'].values)

#-----------------------------------------------------------------------------
tres = copy.deepcopy(res)

cnt_min  = np.nanmin([np.nanmin(x) for x in cnt_list])
cnt_max  = np.nanmax([np.nanmax(x) for x in cnt_list])
data_min = np.nanmin([np.nanmin(x) for x in data_list])
data_max = np.nanmax([np.nanmax(x) for x in data_list])

#-----------------------------------------------------------------------------
# for c in range(num_case):
plot = [None]*3

# print(); print(zmdt)
# print(); print(zmdt.mean(dim=('time','ncol')))
# print()
# exit()

# condition_str = 'ZMDT=0'
# condition_str = 'ZMDT>0.01'
condition_str = 'CLUBB>ZMDT & ZMDT>0'

# tres.tiXAxisString = f'ZMDT [K/day]'
# plot[0] = ngl.xy(wks, zmdt.mean(dim=('time','ncol')).values, lev_list[0], tres)


tres.tiXAxisString = f'CLUBB Heating (when {condition_str}) [K/day]'
plot[1] = ngl.xy(wks, np.ma.masked_invalid(data_list[c]), lev_list[0], tres)

tres.tiXAxisString = f'Occurrence of ZMDT{condition_str} [%]'
plot[2] = ngl.xy(wks, np.ma.masked_invalid( cnt_list[c]), lev_list[0], tres)

plot = plot[1:]

# hs.set_subtitles(wks, plot[0], '', '', '', font_height=0.008)
# hs.set_subtitles(wks, plot[1], '', '', '', font_height=0.008)

# if c==0 :
#    plot[ip] = tplot
# else:
#    ngl.overlay(plot[ip],tplot)

# ### add vertical line
# lres = hs.res_xy()
# lres.xyLineThicknessF = 1
# lres.xyDashPattern = 0
# lres.xyLineColor = 'black'
# ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

# ngl.overlay( plot[ip], cplot )
# hs.set_subtitles(wks, plot[ip], case_name[c], '', var_z_str[v], font_height=0.008)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# layout = [num_var,num_case] if var_x_case else [num_case,num_var]
layout = [1,len(plot)]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
