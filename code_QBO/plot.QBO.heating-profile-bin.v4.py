import os, ngl, copy, xarray as xr, numpy as np, cmocean, warnings, numba
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------
# var, var_str, file_type_list = [], [], []
# def add_var(var_name,file_type=None,n=''):
#   var.append(var_name)
#   file_type_list.append(file_type)
#   var_str.append(n)
#---------------------------------------------------------------------------------------------------
var_x,var_z,var_x_str,var_z_str = [],[],[],[]
def add_vars(vx,vz,vx_name=None,vz_name=None): 
   # if lev==-1: lev = np.array([0])
   vx_str_tmp = vx if vx_name is None else vx_name
   vz_str_tmp = vz if vz_name is None else vz_name
   var_x.append(vx)
   var_z.append(vz)
   var_x_str.append(vx_str_tmp)
   var_z_str.append(vz_str_tmp)
#---------------------------------------------------------------------------------------------------
# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,875,900])
lev = np.array([50,100,150,200,250,300,350,400,450,500,550,600])
# lev = np.array([200,300,400,500])
# lev = np.array([100,200,300])
#---------------------------------------------------------------------------------------------------

### 2024 SciDAC heating test
# add_case('E3SM.2024-SCIDAC-heating-test-00.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
add_case('E3SM.2024-SCIDAC-heating-test-01.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
add_case('E3SM.2024-SCIDAC-heating-test-02.F2010',n='E3SM',d=0,c='blue', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

#---------------------------------------------------------------------------------------------------
lat1,lat2 = -10,10
# lat1,lat2 = -15,15
# lat1,lat2 = -30,30
#---------------------------------------------------------------------------------------------------

# num_plot_col = len(var_z)
# num_plot_col = 1


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.heating-profile-bin.v4'


# htype,first_file,num_files = 'h2',0,30
htype,first_file,num_files = 'h2',5,1


overlay_vars = True

use_snapshot,ss_t  = False,0

omit_bot,bot_k     = False,-2
omit_top,top_k     = False,30

print_stats        = False

var_x_case = False

recalculate = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = 1#len(var_z)
num_case = len(case)

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in locals(): lev = np.array([0])

wkres = ngl.Resources() ; npix = 2048*2 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
# plot = [None]*2#(num_var*num_case)


res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 12
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

# bin_min, bin_spc, nbin_log, bin_spc_log = 5e-2, 1e-2, 44, 20
bin_min, bin_spc, nbin_log, bin_spc_log = 5e-2, 1e-2, 88, 10

x_tick_vals = [1e-2,1e-1,1e0,1e1,1e2,1e3]

res.tmXBMode   = "Explicit"
res.tmXBValues = np.array(x_tick_vals)
res.tmXBLabels = x_tick_vals
res.xyXStyle = 'Log'

res.tiYAxisString = f'Percent occurrence of total data (ntime*ncol)'
res.tiXAxisString = f'Max dT/dt [K/day] over {np.max(lev)}-{np.min(lev)} mb'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
# for v in range(num_var):
v = 0
hc.printline()
# print(' '*2+f'var: {hc.tcolor.GREEN}{var_z[v]}{hc.tcolor.ENDC}')
cnt_list = []
lev_list = []
data_list = []
binx_list = []
biny_list = []
for c in range(num_case):
   #-------------------------------------------------------------------------
   temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
   tmp_file = f'{temp_dir}/QBO.heating-profile-bin.v4.{case[c]}'
   # tmp_file += f'.{var_x[v]}.{var_z[v]}'
   tmp_file += f'.htype_{htype}.f0_{first_file}.nf_{num_files}'
   if 'lat1' in vars(): tmp_file += f'.lat1_{lat1}.lat2_{lat2}'
   if 'lon1' in vars(): tmp_file += f'.lon1_{lat1}.lon2_{lat2}'
   tmp_file = tmp_file + f'.nc'
   print(' '*4+f'case: {hc.tcolor.CYAN}{case[c]:50}{hc.tcolor.ENDC}  {tmp_file}')
   #----------------------------------------------------------------------------
   if recalculate :
      #-------------------------------------------------------------------------
      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      case_obj = he.Case( name=case[c], atm_comp='eam', 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2
      #-------------------------------------------------------------------------
      zmfdt = case_obj.load_data('ZMDT',       htype=htype,first_file=first_file,num_files=num_files)
      # zmfdt = case_obj.load_data('ZMDT',       htype=htype,first_file=first_file,num_files=num_files,lev=lev)
      # p3mdt = case_obj.load_data('P3DT',       htype=htype,first_file=first_file,num_files=num_files,lev=lev)
      # clbdt = case_obj.load_data('TTEND_CLUBB',htype=htype,first_file=first_file,num_files=num_files,lev=lev)

      zmfdt = zmfdt.isel(ncol = slice(0,1000))

      # print(zmfdt)
      # exit()

      #-------------------------------------------------------------------------
      zmfdt = zmfdt*86400
      # clbdt = clbdt*86400
      # p3mdt = p3mdt*86400
      #-------------------------------------------------------------------------
      # # screen out negative values
      # zmfdt2 = xr.where(zmfdt >= 0, zmfdt, 0)
      # clbdt2 = xr.where(clbdt >= 0, clbdt, 0)
      # p3mdt2 = xr.where(p3mdt >= 0, p3mdt, 0)
      #-------------------------------------------------------------------------
      # totdt = zmfdt + clbdt + p3mdt
      # totdt2 = zmfdt2 + clbdt2 + p3mdt2
      #-------------------------------------------------------------------------
      zmfdt_max = zmfdt.isel(lev=zmfdt.argmax(dim="lev"))
      # clbdt_max = clbdt.isel(lev=clbdt.argmax(dim="lev"))
      # p3mdt_max = p3mdt.isel(lev=p3mdt.argmax(dim="lev"))
      # totdt_max = totdt.isel(lev=totdt.argmax(dim="lev"))

      # zmfdt2_max = zmfdt2.isel(lev=zmfdt2.argmax(dim="lev"))
      # clbdt2_max = clbdt2.isel(lev=clbdt2.argmax(dim="lev"))
      # p3mdt2_max = p3mdt2.isel(lev=p3mdt2.argmax(dim="lev"))
      # totdt2_max = totdt2.isel(lev=totdt2.argmax(dim="lev"))
      # #-------------------------------------------------------------------------
      # @numba.njit()
      # def find_max_bottom_up(data, data_max, k_list):
      #    ( ntime, nlev, ncol ) = data.shape
      #    # data_max = np.zeros([ntime,ncol])
      #    for t in range(ntime):
      #       for i in range(ncol):
      #          for k in k_list:
      #             if data[t,i,k]<0: break
      #             if data[t,i,k]>data_max[t,i]:
      #                data_max[t,i] = data[t,i,k]
      #    return data_max

      # ( ntime, nlev, ncol ) = zmfdt.values.shape

      # # zmfdt2_max = find_max_bottom_up( data=zmfdt.values, k_list=list(range(nlev)[::-1]) )

      # data_max = np.zeros([ntime,ncol])
      # zmfdt2_max = find_max_bottom_up( zmfdt.values, data_max, k_list=list(range(nlev)[::-1]) )

      # zmfdt2_max = xr.DataArray(zmfdt2_max,coords=zmfdt_max.coords)
      #-------------------------------------------------------------------------
      def find_max_bottom_up(data, data_max, k_list):
         ( ntime, nlev, ncol ) = data.shape
         data_max = xr.zeros_like(data.isel(lev=0,drop=True)
         # for t in range(ntime):
         #    for i in range(ncol):
         for k in range(nlev)[::-1]:
            if data[t,i,k]<0: break
            if data[t,i,k]>data_max[t,i]:
               data_max[t,i] = data[t,i,k]
         return data_max

      ( ntime, nlev, ncol ) = zmfdt.values.shape

      # zmfdt2_max = find_max_bottom_up( data=zmfdt.values, k_list=list(range(nlev)[::-1]) )

      
      zmfdt2_max = find_max_bottom_up( zmfdt.values, data_max, k_list=list(range(nlev)[::-1]) )

      zmfdt2_max = xr.DataArray(zmfdt2_max,coords=zmfdt_max.coords)
      #-------------------------------------------------------------------------
      # hc.print_stat(zmfdt,name=f'zmfdt ',indent=' '*4,compact=True)
      # hc.print_stat(clbdt,name=f'clbdt',indent=' '*4,compact=True)
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      # Log mode - logarithmically spaced bins that increase in width by bin_spc_log [%]
      bin_log_wid = np.zeros(nbin_log)
      bin_log_ctr = np.zeros(nbin_log)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin_log):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      nbin_x = nbin_log
      bin_x_coord = xr.DataArray( bin_log_ctr )
      #-------------------------------------------------------------------------
      shape, dims, coords = (nbin_x), ('bin_x'), [bin_x_coord]
      zmfdt_cnt = xr.DataArray( np.zeros(shape,dtype=zmfdt.dtype), dims=dims, coords=coords )
      # clbdt_cnt = xr.DataArray( np.zeros(shape,dtype=clbdt.dtype), dims=dims, coords=coords )
      # p3mdt_cnt = xr.DataArray( np.zeros(shape,dtype=p3mdt.dtype), dims=dims, coords=coords )
      # totdt_cnt = xr.DataArray( np.zeros(shape,dtype=totdt.dtype), dims=dims, coords=coords )

      zmfdt2_cnt = xr.DataArray( np.zeros(shape,dtype=zmfdt.dtype), dims=dims, coords=coords )
      # clbdt2_cnt = xr.DataArray( np.zeros(shape,dtype=clbdt.dtype), dims=dims, coords=coords )
      # p3mdt2_cnt = xr.DataArray( np.zeros(shape,dtype=p3mdt.dtype), dims=dims, coords=coords )
      # totdt2_cnt = xr.DataArray( np.zeros(shape,dtype=totdt.dtype), dims=dims, coords=coords )
      #-------------------------------------------------------------------------
      for bx in range(nbin_x):
         bin_bot_x = bin_log_ctr[bx] - bin_log_wid[bx]/2.
         bin_top_x = bin_log_ctr[bx] + bin_log_wid[bx]/2.
         zmfdt_condition = (zmfdt_max.values>=bin_bot_x) & (zmfdt_max.values<bin_top_x)
         # clbdt_condition = (clbdt_max.values>=bin_bot_x) & (clbdt_max.values<bin_top_x)
         # p3mdt_condition = (p3mdt_max.values>=bin_bot_x) & (p3mdt_max.values<bin_top_x)
         # totdt_condition = (totdt_max.values>=bin_bot_x) & (totdt_max.values<bin_top_x)

         zmfdt2_condition = (zmfdt2_max.values>=bin_bot_x) & (zmfdt2_max.values<bin_top_x)
         # clbdt2_condition = (clbdt2_max.values>=bin_bot_x) & (clbdt2_max.values<bin_top_x)
         # p3mdt2_condition = (p3mdt2_max.values>=bin_bot_x) & (p3mdt2_max.values<bin_top_x)
         # totdt2_condition = (totdt2_max.values>=bin_bot_x) & (totdt2_max.values<bin_top_x)

         with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            if np.sum(zmfdt_condition)>0: zmfdt_cnt[bx] = np.sum( zmfdt_condition )
            # if np.sum(clbdt_condition)>0: clbdt_cnt[bx] = np.sum( clbdt_condition )
            # if np.sum(p3mdt_condition)>0: p3mdt_cnt[bx] = np.sum( p3mdt_condition )
            # if np.sum(totdt_condition)>0: totdt_cnt[bx] = np.sum( totdt_condition )

            if np.sum(zmfdt2_condition)>0: zmfdt2_cnt[bx] = np.sum( zmfdt2_condition )
            # if np.sum(clbdt2_condition)>0: clbdt2_cnt[bx] = np.sum( clbdt2_condition )
            # if np.sum(p3mdt2_condition)>0: p3mdt2_cnt[bx] = np.sum( p3mdt2_condition )
            # if np.sum(totdt2_condition)>0: totdt2_cnt[bx] = np.sum( totdt2_condition )

      total_cnt = len(zmfdt['time']) + len(zmfdt['ncol'])
      zmfdt_cnt = zmfdt_cnt/total_cnt*1e2
      # clbdt_cnt = clbdt_cnt/total_cnt*1e2
      # p3mdt_cnt = p3mdt_cnt/total_cnt*1e2
      # totdt_cnt = totdt_cnt/total_cnt*1e2

      zmfdt2_cnt = zmfdt2_cnt/total_cnt*1e2
      # clbdt2_cnt = clbdt2_cnt/total_cnt*1e2
      # p3mdt2_cnt = p3mdt2_cnt/total_cnt*1e2
      # totdt2_cnt = totdt2_cnt/total_cnt*1e2
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
   #    print('writing to file: '+tmp_file)
   #    bin_ds.to_netcdf(path=tmp_file,mode='w')
   # else:
   #    bin_ds = xr.open_dataset( tmp_file )

#-----------------------------------------------------------------------------
tres = copy.deepcopy(res)

# cnt_min  = np.nanmin([np.nanmin(x) for x in cnt_list])
# cnt_max  = np.nanmax([np.nanmax(x) for x in cnt_list])
# data_min = np.nanmin([np.nanmin(x) for x in data_list])
# data_max = np.nanmax([np.nanmax(x) for x in data_list])

#-----------------------------------------------------------------------------
# for c in range(num_case):
plot = [None]*1

tres.xyDashPattern = 0

tres.xyLineColor='red';   zmfdt_plot = ngl.xy(wks, bin_x_coord.values, zmfdt_cnt.values, tres)
# tres.xyLineColor='blue';  clbdt_plot = ngl.xy(wks, bin_x_coord.values, clbdt_cnt.values, tres)
# tres.xyLineColor='green'; p3mdt_plot = ngl.xy(wks, bin_x_coord.values, p3mdt_cnt.values, tres)
# tres.xyLineColor='black'; totdt_plot = ngl.xy(wks, bin_x_coord.values, totdt_cnt.values, tres)

tres.xyDashPattern = 1

tres.xyLineColor='red';   zmfdt2_plot = ngl.xy(wks, bin_x_coord.values, zmfdt2_cnt.values, tres)
# tres.xyLineColor='blue';  clbdt2_plot = ngl.xy(wks, bin_x_coord.values, clbdt2_cnt.values, tres)
# tres.xyLineColor='green'; p3mdt2_plot = ngl.xy(wks, bin_x_coord.values, p3mdt2_cnt.values, tres)
# tres.xyLineColor='black'; totdt2_plot = ngl.xy(wks, bin_x_coord.values, totdt2_cnt.values, tres)

plot[0] =           zmfdt_plot
ngl.overlay(plot[0],zmfdt2_plot)

# plot[0] =           totdt_plot
# ngl.overlay(plot[0],zmfdt_plot)
# ngl.overlay(plot[0],p3mdt_plot)
# ngl.overlay(plot[0],clbdt_plot)

# ngl.overlay(plot[0],totdt2_plot)
# ngl.overlay(plot[0],zmfdt2_plot)
# ngl.overlay(plot[0],p3mdt2_plot)
# ngl.overlay(plot[0],clbdt2_plot)

# hs.set_subtitles(wks, plot[ip], case_name[c], '', var_z_str[v], font_height=0.008)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# layout = [num_var,num_case] if var_x_case else [num_case,num_var]
layout = [1,len(plot)]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
