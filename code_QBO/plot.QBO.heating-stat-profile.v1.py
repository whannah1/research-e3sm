import os, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------
var,vclr,vdsh = [],[],[]
def add_var(var_name,clr='black',dsh=0): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh)
#---------------------------------------------------------------------------------------------------
# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([100,150,200,300,400,500,600,700,750,800,850,875,900])
lev = np.array([200,300,400,500])
#---------------------------------------------------------------------------------------------------

### 2024 SciDAC heating test
add_case('E3SM.2024-SCIDAC-heating-test-00.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

#---------------------------------------------------------------------------------------------------

# add_var('DTCOND'          ,clr='black')
add_var('ZMDT'            ,clr='red')
add_var('P3DT'            ,clr='green')
add_var('TTEND_CLUBB'     ,clr='blue')
# add_var('CLOUDFRAC_CLUBB' ,clr='')
# add_var('WP2_CLUBB'       ,clr='')
# add_var('WP3_CLUBB'       ,clr='')

#---------------------------------------------------------------------------------------------------
# lat1,lat2 = -10,10
lat1,lat2 = -30,30
#---------------------------------------------------------------------------------------------------

num_plot_col = len(var)
# num_plot_col = 2


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.heating-stat-profile.v1'


htype,years,months,first_file,num_files = 'h1',[],[],0,10


overlay_vars = True

use_snapshot,ss_t  = False,0

omit_bot,bot_k     = False,-2
omit_top,top_k     = False,30

print_stats        = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources() ; npix = 2048*2 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
# plot = [None]*(num_var*num_case)
if overlay_vars: 
   plot = [None]*(num_case)
else:
   plot = [None]*(num_var)
res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

res.tiYAxisString = 'Pressure [hPa]'
res.trYReverse = True
# res.xyYStyle = 'Log'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(hc.tcolor.GREEN+'  var: '+var[v]+hc.tcolor.ENDC)
   data_list,lev_list = [],[]
   for c in range(num_case):
      # print(hc.tcolor.CYAN+'    case: '+case[c]+hc.tcolor.ENDC)
      #-------------------------------------------------------------------------
      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      case_obj = he.Case( name=case[c], atm_comp='eam', 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      #-------------------------------------------------------------------------
      tvar = var[v]
      area_name = 'area'
      #-------------------------------------------------------------------------
      # read the data   
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      area = case_obj.load_data(area_name,htype=htype,first_file=first_file,num_files=num_files).astype(np.double)
      data = case_obj.load_data(tvar,     htype=htype,first_file=first_file,num_files=num_files,lev=lev)

      #-------------------------------------------------------------------------
      # unit conversion
      if var[v]=='ZMDT'       : data = data*86400
      if var[v]=='TTEND_CLUBB': data = data*86400
      if var[v]=='P3DT'       : data = data*86400
      #-------------------------------------------------------------------------
      # hc.print_time_length(data.time,indent=' '*6)
      #-------------------------------------------------------------------------
      # print( np.sum( np.isnan(data) ) )
      # exit()

      for k in range(len(data['lev'])):
         data_tmp = data.isel(lev=k).values
         qvals = xr.DataArray( np.nanquantile(data_tmp,[0.9,0.99,0.999,0.9999,0.99999]) )
         qmsg = ''
         for q in qvals: qmsg+= f"  {q:6.2f}"
         print(f'  lev: {data["lev"][k].values}   {qmsg} ')
         # print(f'k: {k}  lev: {data["lev"][k].values}   {np.nanmedian(data_tmp)} {np.nanmean(data_tmp)}  {np.nanmax(data_tmp)}')
      
      print()
      break
exit()

'''
Quantile              90%   99.0%   99.9%  99.99% 99.999%

ZMDT     lev: 200    0.00    2.00    8.24   16.56   28.27
         lev: 300    0.00    8.68   22.71   46.24   93.71
         lev: 400    0.01   18.81   42.53   81.87  116.41
         lev: 500    3.90   26.42   54.68   91.23  121.69

P3DT     lev: 200    0.31    3.09    8.29   11.12   12.74
         lev: 300    0.79   22.61   34.74   38.34   40.68
         lev: 400    0.00   28.47   34.36   36.40   38.05
         lev: 500    0.00   13.65   23.49   29.94   35.99

CLUBB_DT lev: 200    0.02    1.02    7.53   18.99   29.15
         lev: 300    0.02    1.10   27.75   55.07   75.39
         lev: 400    0.03    7.54   35.91   60.05   76.55
         lev: 500    0.03   12.48   31.97   53.49   72.59
'''

   #    #-------------------------------------------------------------------------
   #    if case[c]=='ERA5':
   #       data = ( (data*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )
   #    else:
   #       if 'lat' in data.dims and 'lon' in data.dims:
   #          data = ( (data*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )
   #       else:
   #          data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
   #    #-------------------------------------------------------------------------
   #    if omit_bot: data = data[:bot_k]
   #    if omit_top: data = data[top_k:]
   #    #-------------------------------------------------------------------------
   #    if print_stats:
   #       hc.print_stat(data,name=f'{var[v]} after averaging',indent=' '*4,compact=True)
   #    #-------------------------------------------------------------------------
   #    data_list.append( data.values )
   #    lev_list.append( data['lev'].values )
   #    #-------------------------------------------------------------------------
   # data_list_list.append(data_list)
   # lev_list_list.append(lev_list)

#-------------------------------------------------------------------------------
# Create plot 1 - overlay all vars for each case
#-------------------------------------------------------------------------------
if overlay_vars:
   for c in range(num_case):
      ip = c
      tres = copy.deepcopy(res)
      data_min = np.min( data_list_list[0][c] )
      data_max = np.max( data_list_list[0][c] )
      for v in range(num_var):
         data_min = np.min([ data_min, np.nanmin(data_list_list[v][c]) ])
         data_max = np.max([ data_max, np.nanmax(data_list_list[v][c]) ])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      
      for v in range(num_var):
         tres.xyLineColor   = vclr[v]
         tres.xyMarkerColor = vclr[v]
         tres.xyDashPattern = vdsh[v]
         tplot = ngl.xy(wks, data_list_list[v][c], lev_list_list[v][c], tres)

         if v==0 :
            plot[ip] = tplot
         else:
            ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''

      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      hs.set_subtitles(wks, plot[ip], case_name[c], ctr_str, '', font_height=0.01)
     
#-------------------------------------------------------------------------------
# Create plot 2 - overlay all cases for each var
#-------------------------------------------------------------------------------
if not overlay_vars:
   for v in range(num_var):
      data_list = data_list_list[v]
      lev_list = lev_list_list[v]
      
      tres = copy.deepcopy(res)

      if var[v] in ['O3','Mass_so4']: tres.xyXStyle = 'Log'

      # ip = v*num_case+c
      ip = c*num_var+v
      
      
      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      ip = v

      for c in range(num_case):
         tres.xyLineColor   = clr[c]
         tres.xyMarkerColor = clr[c]
         tres.xyDashPattern = dsh[c]

         tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''
      var_str = var[v]
      if var[v]=='CLOUD': var_str = 'Cloud Fraction'


      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str, font_height=0.01)

#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.08
   lgres.lgLabelFontHeightF = 0.012
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 8
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   lx,ly = 0.5,0.45
   if num_var==2: lx,ly = 0.3,0.45
   if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [1,len(plot)]
# layout = [int(np.ceil(num_var/4.)),4]
# layout = [num_var,num_case]
# layout = [num_case,num_var]
# layout = [1,num_var]

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]


# if num_var==1  : layout = [int(np.ceil(num_case/2.)),2]
# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]

# if num_case==1 and num_var==4 : layout = [2,2]
# if num_case==1 and num_var==6 : layout = [3,2]

#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
