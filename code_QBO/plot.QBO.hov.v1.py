import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import pandas as pd
import cmocean
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
#-------------------------------------------------------------------------------
# Create tarball of hov data:
# tar -czvf QBO.hov.2023_L80_ensemble.tar.gz data_temp/QBO.hov.* 
# tar -czvf QBO.hov.2023_AMIP.tar.gz data_temp/QBO.hov.v1.E3SM.2023-SCIDAC-v2-AMIP* 
#-------------------------------------------------------------------------------

### v1 vs v2 PI coupled
# add_case('E3SM.PI-CPL.v1.ne30.01',n='E3SMv1 PI coupled')
# add_case('E3SM.PI-CPL.v2.ne30.01',n='E3SMv2 PI coupled')

### Runs for QBO paper - OLD
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72.01',       n='E3SM control')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72-nsu40.01', n='E3SM L72 smoothed')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L80-rsu40.01', n='E3SM L80 refined')

### Runs for QBO paper - NEW
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run' # ''archive/atm/hist'
# add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L72', n='L72', p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L80', n='L80', p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L128',n='L128',p=tmp_path,s=tmp_sub)

### new v3 runs with L72 & L80
# add_case('ERAi',n='ERAi',p='/lcrc/group/e3sm/diagnostics/observations/Atm/time-series/ERA-Interim/ua_197901_201612.nc',s='')

# add_case('20230629.v3alpha02.amip.chrysalis.L72',n='E3SM L72',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')
# add_case('20230629.v3alpha02.amip.chrysalis.L80',n='E3SM L80',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')

# add_case('20230629.v3alpha02.amip.chrysalis.L80',             n='AMIP L80', p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')
# add_case('20231002.v3alpha04_bigrid_L80_QBO1.F2010.chrysalis',n='F2010 L80',p='/lcrc/group/acme/ac.benedict/E3SMv3_dev',s='run')

# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP',      n='E3SM L80',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='run')
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.fix01',n='E3SM L80 w/ bug fix',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='run')

### v3alpha04 L80
# add_case('20230922.v3alpha04.F20TR.chrysalis',                n='v3alpha04 L72',p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev/',   s='archive/atm/hist')
# add_case('20231012.v3alpha04_bigrid_L80_QBO1.F20TR.chrysalis',n='v3alpha04 L80',p='/lcrc/group/acme/ac.benedict/E3SMv3_dev/',s='archive/atm/hist')


### L80 ensemble
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # top-tier
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_10.HD_1.50',n='',p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_20.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_20.HD_1.50',n='',p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_25.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_15.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.09.CF_20.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_20.HD_1.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.05.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub) # top-tier
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.80.CF_01.HD_1.15',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.90.CF_20.HD_0.25',n='',p=tmp_path,s=tmp_sub) # top-tier
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.59.CF_41.HD_0.55',n='',p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.01.CF_01.HD_0.70',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.40.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # top-tier
# # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.70.CF_25.HD_0.90',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_07.HD_1.35',n='',p=tmp_path,s=tmp_sub) # top-tier

# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub)

#### add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_30.HD_0.63',n='',p=tmp_path,s=tmp_sub) # outlier - did not finish

# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.18.CF_14.HD_0.52',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_11.HD_0.66',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.06.CF_13.HD_0.59',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.22.CF_08.HD_0.48',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.52.CF_16.HD_0.89',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.13.CF_10.HD_0.82',n='',p=tmp_path,s=tmp_sub)


### new runs to extend ensemble (2024)
# gweff_list, cfrac_list, hdpth_list = [],[],[]
# gweff_list.append(0.79); cfrac_list.append(12); hdpth_list.append(0.72)
# gweff_list.append(0.78); cfrac_list.append(13); hdpth_list.append(0.67)
# gweff_list.append(0.77); cfrac_list.append( 9); hdpth_list.append(0.94)
# gweff_list.append(0.55); cfrac_list.append(10); hdpth_list.append(0.38)
# gweff_list.append(0.63); cfrac_list.append( 8); hdpth_list.append(0.62)
# gweff_list.append(0.14); cfrac_list.append(10); hdpth_list.append(1.37)
# gweff_list.append(0.13); cfrac_list.append(22); hdpth_list.append(0.56)
# for n in range(len(hdpth_list)):
#   e = gweff_list[n]
#   c = cfrac_list[n]
#   h = hdpth_list[n]
#   tcase = '.'.join(['E3SM','2023-SCIDAC','ne30pg2_EC30to60E2r2','AMIP',f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}'])
#   add_case(tcase,n='',p=tmp_path,s=tmp_sub)

# for c in range(len(case)):
#    if 'E3SM.2023-SCIDAC.' in case[c]:
#       ef_idx = case[c].find('EF_')
#       cf_idx = case[c].find('CF_')
#       hd_idx = case[c].find('HD_')
#       ef = float(case[c][ef_idx+3:ef_idx+3+4])
#       cf = float(case[c][cf_idx+3:cf_idx+3+2])
#       hd = float(case[c][hd_idx+3:hd_idx+3+4])
#       case_name[c] = f'{case_name[c]}  EF={ef} CF={cf} HD={hd}'

### new runs to extend ensemble - 2024-05-02
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.31.CF_11.HD_1.39',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.70.CF_21.HD_0.31',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.81.CF_20.HD_0.39',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.11.CF_18.HD_0.87',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.11.CF_23.HD_0.86',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.13.CF_16.HD_1.22',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.33.CF_17.HD_0.72',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.40.CF_15.HD_1.17',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.43.CF_23.HD_0.60',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.47.CF_17.HD_0.46',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_15.HD_1.35',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.63.CF_22.HD_0.78',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.67.CF_22.HD_1.18',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.69.CF_17.HD_0.64',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.77.CF_18.HD_0.93',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.86.CF_17.HD_0.29',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.45.CF_20.HD_1.40',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_12.HD_1.03',n='',p=tmp_path,s=tmp_sub)

### new ensemble extension - 2024-07-09
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.12.CF_16.HD_0.48',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.04.CF_20.HD_0.75',n='',p=tmp_path,s=tmp_sub)


### SciDAC MMF tests w/ new vertical grids
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L60',n='MMF L60',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L64',n='MMF L64',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L72',n='MMF L72',p=tmp_path,s=tmp_sub)


# add_case('20230922.v3alpha04.F20TR.chrysalis'                   ,n='L72 AMIP'  ,c='red' ,d=1, p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev'   ,s='archive/atm/hist')
# add_case('20231012.v3alpha04_bigrid_L80_QBO1.F20TR.chrysalis'   ,n='L80 AMIP'  ,c='blue',d=1, p='/lcrc/group/acme/ac.benedict/E3SMv3_dev',s='archive/atm/hist')
# add_case('20230924.v3alpha04_trigrid.piControl.chrysalis'       ,n='L72 PI-CPL',c='red' ,d=0, p='/lcrc/group/e3sm2/ac.xzheng/E3SMv3_dev' ,s='archive/atm/hist')
# add_case('20231023.v3alpha04-L80QBO.trigrid.piControl.chrysalis',n='L80 PI-CPL',c='blue',d=0, p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev'   ,s='archive/atm/hist')
# add_case('20231023.v3alpha04-L80QBO.trigrid.piControl.chrysalis',n='L80 PI-CPL',c='blue',d=0, p='/lcrc/group/e3sm2/ac.wlin/E3SMv3_dev'   ,s='run')

### Jim's L96 test for scidac
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_path,tmp_sub = '/global/cfs/cdirs/m4310/whannah/E3SM','archive/atm/hist'
# # add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L72' ,n='v2 L72',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L80' ,n='v2 L80',p=tmp_path,s=tmp_sub)
# tmp_path,tmp_sub = '/global/cfs/cdirs/m4310/benedict/forWalter',''
# add_case('20240422.v3.F20TR.ne30pg2_r05_IcoswISC30E3r5.L96',  n='v3 L96',p=tmp_path,s=tmp_sub)

### 2024 nontraditional coriolis
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
# add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-off.NCT-off'               ,n='EAM ',       p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-off'                ,n='EAM NHS',    p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-on'                 ,n='EAM NHS+NCT',p=tmp_path,s=tmp_sub)
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-off.NCT-off' ,n='MMF ',       p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-off'  ,n='MMF NHS',    p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-on'   ,n='MMF NHS+NCT',p=tmp_path,s=tmp_sub)
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.NXY_64_1.NHS-off.NCT-off' ,n='MMF ',       p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.NXY_64_1.NHS-on.NCT-off'  ,n='MMF NHS',    p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.NXY_64_1.NHS-on.NCT-on'   ,n='MMF NHS+NCT',p=tmp_path,s=tmp_sub)

### 2024 v3.HR dev/tuning runs
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne120pg2_scrip.nc'
# tmp_path,tmp_sub = '/pscratch/sd/t/terai/E3SMv3_dev/','archive/atm/hist'
# add_case('20240718.v3.F2010-TMSOROC05-Z0015_plus4K.ne120pg2_r025_icos30.dust.pm-cpu',                n='ne120pg2 v0', p=tmp_path,s=tmp_sub)
# add_case('20240821.v3.F2010-TMSOROC05-Z0015_plus4K.ne120pg2_r025_icos30.grav_wav_tunings.pm-cpu',    n='ne120pg2 v1', p=tmp_path,s=tmp_sub)
# tmp_path,tmp_sub = '/pscratch/sd/t/terai/E3SMv3_dev/','run'
# add_case('20240823.v3.F2010-TMSOROC05-Z0015_plus4K.ne120pg2_r025_icos30.oro_conv_gw_tunings.pm-cpu',         n='ne120pg2 v2', p=tmp_path,s=tmp_sub)
# add_case('20240827.v3.F2010-TMSOROC05-Z0015.ne120pg2_r025_icos30.Nomassfluxadj.n0256t120x1.kdreg2.hy',       n='ne120pg2 20240827 effgw_beres=0.25', p='/global/cfs/cdirs/e3sm/ndk/ms13-oct7/',s='run')
# add_case('20241009.v3.F2010-TMSOROC05-Z0015.ne120pg2_r025_icos30.gw_dust_mods.n0256t120x1.coilr4.kdreg2.hy', n='ne120pg2 20241009 effgw_beres=0.10',  p='/global/cfs/cdirs/e3sm/ndk/tmpdata/',s='run')
 

### 2024 SciDAC - pseudo-composite (PCOMP) / process oriented ensemble 
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
# add_case('E3SM.2024-SCIDAC-PCOMP-TEST-02.ne30pg2_r05_IcoswISC30E3r5.EF_0.35.CF_10.HD_0.50.1984-01-01', n='e,c,h=0.35,10,0.50 - v3 default',    p=tmp_path,s=tmp_sub)
# # add_case('E3SM.2024-SCIDAC-PCOMP-TEST-02.ne30pg2_r05_IcoswISC30E3r5.EF_0.12.CF_16.HD_0.48.1984-01-01', n='e,c,h=0.12,16,0.48 - surrogate opt', p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-SCIDAC-PCOMP-TEST-02.ne30pg2_r05_IcoswISC30E3r5.EF_0.09.CF_20.HD_0.25.1984-01-01', n='e,c,h=0.09,20,0.25 - no QBO',        p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-SCIDAC-PCOMP-TEST-02.ne30pg2_r05_IcoswISC30E3r5.EF_0.70.CF_21.HD_0.31.1984-01-01', n='e,c,h=0.70,21,0.31 - QBO too fast',  p=tmp_path,s=tmp_sub)

### 2024 SciDAC - AQP tests for multi-fidelity
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
# add_case('E3SM.2024-SCIDAC-AQP-test-00.FAQP', n='AQP-test', p=tmp_path,s=tmp_sub)

### 2024 SciDAC Ensemble
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
# add_case('E3SM_2024-SCIDAC-00_F20TR_ne30pg2_EF_0.35_CF_10_HD_0.50_HM_02.5_SS_10.0_PS_700', n='E3SMv3 default w/ fix', p=tmp_path,s=tmp_sub)
# add_case('E3SM_2024-SCIDAC-00_F20TR_ne30pg2_EF_0.18_CF_11_HD_0.48_HM_02.5_SS_10.0_PS_700', n='E3SMv3', p=tmp_path,s=tmp_sub)

# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.50', n='E3SMv3 default (prev ens)',p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='archive/atm/hist')

# add_case('v3.LR.amip_0101.QBObenchmark.20241008',                          n='EAMv3 benchmark', p='/global/cfs/cdirs/m4310/data/sims',s='archive/atm/hist')
# add_case('SciDAC.E3SMv3.20241220.SciDACWMH_QBOBnMk.Dtopo.ICWMH.UCWMH.AQP3',n='AQPv3',           p='/global/cfs/cdirs/m4310/data/sims/aqua/xsunlanl',s='archive/atm/hist')



# ERA5 levels
# lev = np.array([   1.,    2.,    3.,    5.,    7.,   10.,   20.,   30.,   50.,   70.,
#                  100.,  125.,  150.,  175.,  200.,  225.,  250.,  300.,  350.,  400.,
#                  450.,  500.,  550.,  600.,  650.,  700.,  750.,  775.,  800.,  825.,
#                  850.,  875.,  900.,  925.,  950.,  975., 1000.])
lev = np.array([   0.1, 0.2, 0.5, 1.,    2.,    3.,    5.,    7.,   10.,   20.,   30.,   50.,   70.,  100.,  125.,  150.,])


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

var = ['U']
# var = ['O3']
# var = ['U','Q','O3']
# var = ['V']
# var = ['T','Q']

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.hov.v1'

lat1,lat2 = -5,5
# lat1,lat2 = -10,10

# yr1,yr2 = 1995, 1999
htype,first_file,num_files = 'h0',0,12*5
# htype,first_file,num_files = 'h0',12*(12+5),12*5
# htype,first_file,num_files = 'h0',0,5*12
# htype,first_file,num_files = 'h0',0,0


use_height_coord = False

print_stats = True
print_time  = False

var_x_case = False

use_common_label_bar = True

num_plot_col = len(var)#1

add_obs = True
obs_case = 'ERA5' # ERAi / ERA5

recalculate = False

# write_to_file = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
num_plot = (num_case+1)*num_var if add_obs else num_case*num_var
plot = [None]*(num_plot)
res = hs.res_contour_fill()
# res.vpHeightF = 0.1
res.vpHeightF = 0.2
res.tmYLLabelFontHeightF         = 0.01
res.tmXBLabelFontHeightF         = 0.01
res.tiXAxisFontHeightF           = 0.01
res.tiYAxisFontHeightF           = 0.01
if use_common_label_bar:
   res.lbLabelBarOn = False

# # disable these by default - turn back on for bottom panel(s)
# res.tmXBOn = False
# res.tiXAxisOn = False

res.tiXAxisString = 'Time'
# if htype=='h0': res.tiXAxisString = 'Time [months]'
if htype=='h0': res.tiXAxisString = 'Time [years]'

if use_height_coord:
   res.tiYAxisString = 'Height [km]'
   res.nglYAxisType = "LinearAxis"
   res.trYMinF = 20
else:
   res.tiYAxisString = 'Pressure [mb]'
   res.trYReverse = True
   # res.trYLog = True # doesn't work due to irregular spacing :(
   res.trYMinF = 5
   res.trYMaxF = 100

   tm_vals = [1,10,50,100,200]
   res.tmYLMode = 'Explicit'
   res.tmYLValues = tm_vals
   res.tmYLLabels = tm_vals
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   if 'PI-CPL.v1' in case: comp = 'cam'
   return comp

def calculate_obs_area(lon,lat,lon_bnds,lat_bnds):
   re = 6.37122e06  # radius of earth
   nlat,nlon = len(lat),len(lon)
   area = np.empty((nlat,nlon),np.float64)
   for j in range(nlat):
      for i in range(nlon):
         dlon = np.absolute( lon_bnds[j,1] - lon_bnds[j,0] )
         dlat = np.absolute( lat_bnds[j,1] - lat_bnds[j,0] )
         dx = re*dlon*np.pi/180.
         dy = re*dlat*np.pi/180.
         area[j,i] = dx*dy
   return area
#---------------------------------------------------------------------------------------------------
def get_tmp_file_name(case_in,var_in):
   tmp_root = os.getenv('HOME')+f'/Research/E3SM/data_temp'
   tmp_file = f'{tmp_root}/QBO.hov.v1.{case_in}.{var_in}.lat_{lat1}_{lat2}.nc'
   return tmp_file
#---------------------------------------------------------------------------------------------------
def write_file(case_in,var_in,data_avg,Z_avg=None):
   tmp_file = get_tmp_file_name(case_in,var_in)
   print('    writing to file: '+tmp_file)
   ds_out = xr.Dataset( coords=data_avg.coords )
   ds_out[var[v]] = data_avg
   ds_out.to_netcdf(path=tmp_file,mode='w')
#---------------------------------------------------------------------------------------------------
def load_file(case_in,var_in):
   tmp_file = get_tmp_file_name(case_in,var_in)
   print(tmp_file)
   ds = xr.open_dataset( tmp_file )
   data_avg = ds[var_in]

   if 'yr1' in globals(): data_avg = data_avg.where( data_avg['time.year']>=yr1, drop=True)
   if 'yr2' in globals(): data_avg = data_avg.where( data_avg['time.year']<=yr2, drop=True)

   return data_avg
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   tvar = var[v]
   data_list,time_list,lev_list = [],[],[]
   
   #----------------------------------------------------------------------------
   # read the model data
   #----------------------------------------------------------------------------
   for c in range(num_case):
      print(f'    case: {hc.tclr.CYAN}{case[c]}{hc.tclr.END}')

      if recalculate:
      # if True:

         data_dir_tmp,data_sub_tmp = None, None
         # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
         if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         if case_sub[c] is not None: data_sub_tmp = case_sub[c]
         case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                             data_dir=data_dir_tmp, data_sub=data_sub_tmp, time_freq=None )
         if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
         if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

         # avoid creating large chunks
         with dask.config.set(**{'array.slicing.split_large_chunks': True}):  

            data = case_obj.load_data(tvar,    
                                      component=get_comp(case[c]),
                                      htype=htype,
                                      first_file=first_file,
                                      num_files=num_files,
                                      lev=lev)
            area = case_obj.load_data('area',
                                      component=get_comp(case[c]),
                                      htype=htype,
                                      first_file=first_file,
                                      num_files=num_files).astype(np.double)

            if use_height_coord: Z = case_obj.load_data('Z3',htype=htype,num_files=num_files)

            hc.print_time_length(data.time,indent=' '*4)

            avg_dims= ('ncol')
            if 'lat' in data.dims: avg_dims= ('lat','lon')
            # data_avg = ( (data*area).sum(dim=avg_dims) / area.sum(dim=avg_dims) )
            data_avg = data.mean(dim=avg_dims)

            if use_height_coord: Z_avg = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') ).mean(dim='time') / 1e3

            ### adjust time to represent the middle of the month instead of the end
            time = data_avg.time.values
            time_orig = copy.deepcopy(time)
            for i,t in enumerate(time):
               if i==0:
                  dt = pd.Timedelta('15 days')
               else:
                  dt = ( time_orig[i] - time_orig[i-1] ) / 2
               time[i] = time_orig[i] - dt
            data_avg['time'] = time

            # print()
            # print(f'{hc.tclr.RED}WARNING{hc.tclr.END} - subsetting by year: {yr1}:{yr2}')
            # if 'yr1' in globals(): data_avg = data_avg.where( data['time.year']>=yr1, drop=True)
            # if 'yr2' in globals(): data_avg = data_avg.where( data['time.year']<=yr2, drop=True)
            
            time = data_avg['time.year'].values + data_avg['time.month'].values/12.

            if print_time: print(); print(time); print()

            # data_list.append( data_avg.transpose().values )
            # # time_list.append( data_avg['time'].values )
            # time_list.append( time )
            # if use_height_coord:
            #    lev_list.append( Z_avg.values )
            # else:
            #    lev_list.append( data_avg['lev'].values )

            write_file(case[c],var[v],data_avg)
      else:
         data_avg = load_file(case[c],var[v])

      data_list.append( data_avg.transpose().values )
      lev_list.append( data_avg['lev'].values )
      time = data_avg['time.year'].values + data_avg['time.month'].values/12.
      time_list.append( time )
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   if var[v] in ['U','V','T','Q'] and add_obs and not use_height_coord:
      if os.path.exists( '/lcrc/group/e3sm' ):
         obs_scratch_path = '/lcrc/group/e3sm/diagnostics/observations/Atm/time-series'
      if os.path.exists( '/global/cfs/cdirs/e3sm' ):
         obs_scratch_path = '/global/cfs/cdirs/e3sm/diagnostics/observations/Atm/time-series'
      if obs_case=='ERAi': obs_data_file = f'{obs_scratch_path}/ERA-Interim/ua_197901_201612.nc'
      if obs_case=='ERA5': 
         if var[v]=='U': obs_var,obs_data_file =  'ua',f'{obs_scratch_path}/ERA5/ua_197901_201912.nc'
         if var[v]=='V': obs_var,obs_data_file =  'va',f'{obs_scratch_path}/ERA5/va_197901_201912.nc'
         if var[v]=='T': obs_var,obs_data_file =  'ta',f'{obs_scratch_path}/ERA5/ta_197901_201912.nc'
         if var[v]=='Q': obs_var,obs_data_file = 'hus',f'{obs_scratch_path}/ERA5/hus_197901_201912.nc'
      print(f'')

      print(f'    case: {obs_case:10}  obs_data_file: {obs_data_file}')

      if recalculate:
      # if True:
         
         ds = xr.open_dataset(obs_data_file)
         area = calculate_obs_area(ds['lon'].values,ds['lat'].values,ds['lon_bnds'].values,ds['lat_bnds'].values)
         area = xr.DataArray( area, coords=[ds['lat'],ds['lon']] )
         data = ds[obs_var]
         data = data.sel(lat=slice(lat1,lat2))
         area = area.sel(lat=slice(lat1,lat2))
         data_avg = (data*area).sum(dim=('lon','lat')) / area.sum(dim=('lon','lat'))

         # print(data_avg)
         # hc.print_stat(data_avg,name=f'ERA5 {var[v]}',stat='naxsh',indent='    ',compact=True)
         # exit()

         ### truncate obs to match model data
         time = data_avg['time.year'].values + data_avg['time.month'].values/12.
         sim_t1 = time_list[0][ 0]
         sim_t2 = time_list[0][-1]
         t_beg,t_end = None,None
         if sim_t1>1850:
            for i,t in enumerate(time):
               # print()
               # print(f't: {t}')
               # print(f'sim_t1: {sim_t1}')
               # print(f'sim_t2: {sim_t2}')
               # print() # exit()
               if t_beg is None and t==sim_t1: t_beg = i
               if t_end is None and t==sim_t2: t_end = i
         else:
            # looks like we have an F-compset - so just truncate length to match
            t_beg,t_end = 0,int(sim_t2*12-sim_t1*12+1)

         if t_beg is not None and t_end is not None:
            # print(f'\n  sim_t1/sim_t2: {sim_t1} / {sim_t2}  t_beg/t_end: {t_beg} / {t_end} \n')
            data_avg = data_avg.isel(time=slice(t_beg,t_end))
            time = data_avg['time.year'].values + data_avg['time.month'].values/12.
         else:
            exit(f'Something went wrong? t_beg: {t_beg}  t_end: {t_end}')


         if print_time: print(); print(time); print()

         k_list = []
         for k,p in enumerate(data_avg['plev'].values/1e2):
            if p in lev: k_list.append(k)
         data_avg = data_avg.isel(plev=k_list)

         # print(data_avg)
         # hc.print_stat(data_avg,name=f'ERA5 {var[v]}',stat='naxsh',indent='    ',compact=True)
         # exit()


         write_file(case_name[0],var[v],data_avg)
      else:
         data_avg = load_file(obs_case,var[v])


      if var[v]=='Q': data_avg = data_avg*1e3

      lev_list.insert(0, data_avg['plev'].values/1e2 )

      # make time length match the model data
      data_avg = data_avg.isel(time=slice(0,len(time_list[0]),))

      time = data_avg['time.year'].values + data_avg['time.month'].values/12.
      time_list.insert(0, time )
      
      data_list.insert(0, data_avg.transpose().values )
      if v==0: case_name.insert(0, obs_case )
   #----------------------------------------------------------------------------
   # print stats after time averaging
   print()
   num_case_tmp = (num_case+1) if add_obs else num_case
   for c in range(num_case_tmp):
      hc.print_stat(data_list[c],name=f'{case_name[c]:40} {var[v]}',stat='naxsh',indent='    ',compact=True)
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)

   # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
   # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
   tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

   if var[v]=='Q': tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )

   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   tres.cnLevelSelectionMode = 'ExplicitLevels'

   if var[v]=='U': tres.cnLevels = np.linspace(-50,50,num=21)
   if var[v]=='T': tres.cnLevels = np.arange(190,260+5,5)
   if var[v]=='Q': tres.cnLevels = np.arange(2,40+2,2)*1e-4

   # nlev = 21
   # aboutZero = False
   # if var[v] in ['U','V'] : aboutZero = True
   # clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, returnLevels=False, aboutZero=aboutZero )
   # if clev_tup==None: 
   #    tres.cnLevelSelectionMode = 'AutomaticLevels'   
   # else:
   #    cmin,cmax,cint = clev_tup
   #    tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
   #    tres.cnLevelSelectionMode = 'ExplicitLevels'

   num_case_tmp = (num_case+1) if add_obs else num_case
   for c in range(num_case_tmp):
      
      # time = time_list[c]
      # time = ( time - time[0] ).astype('float') / 86400e9
      # time = ( time - time[0] ).astype('float') / (60*60*24*365)

      tres.sfYArray = lev_list[c]#.astype(int)
      tres.sfXArray = time_list[c]
      # tres.sfXArray = np.linspace( 1./12., float(num_files)/12., num=len(time) )

      # if use_common_label_bar and c==(num_case_tmp-1): tres.lbLabelBarOn = True

      ip = c*num_var + v

      plot[ip] = ngl.contour(wks, data_list[c], tres)

      #-------------------------------------------------------------------------
      # Set strings at top of plot
      #-------------------------------------------------------------------------
      var_str = var[v]
      if var[v]=='PRECT' : var_str = 'Precipitation [mm/day]'
      if var[v]=='U'     : var_str = 'Zonal Wind [m/s]'

      ctr_str = ''
      # if var[v] in ['PRECT','PRECC','PRECL'] : ctr_str = 'Mean: '+'%.2f'%avg_X+' [mm/day]'
      # hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], ctr_str, var_str, font_height=0.01)
      # hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], '', '', font_height=0.008)
      hs.set_subtitles(wks, plot[ip], case_name[c], '', var[v], font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
# layout = [num_var,num_case] if var_x_case else [num_case,num_var]
# layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
layout = [num_case+1,num_var] if add_obs else [num_case,num_var]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5

if use_common_label_bar: 
   pnl_res.nglPanelLabelBar   = True
   # pnl_res.lbTopMarginF       =  0.2
   # pnl_res.lbBottomMarginF    = -0.2
   pnl_res.lbLeftMarginF      = 0.5+0.5
   pnl_res.lbRightMarginF     = 0.5
   # pnl_res.lbTitleString      = "m/s"
   # pnl_res.lbTitlePosition    = "bottom"
   # pnl_res.lbLabelFontHeightF = 0.001
   # pnl_res.lbTitleFontHeightF = 0.01

### add panel labels
pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
pnl_res.nglPanelFigureStringsJust        = "TopLeft"
pnl_res.nglPanelFigureStringsFontHeightF = 0.01

# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
