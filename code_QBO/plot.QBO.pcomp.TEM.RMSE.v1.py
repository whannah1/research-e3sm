import os, ngl, copy, xarray as xr, numpy as np, glob
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import cmocean
#---------------------------------------------------------------------------------------------------
gscratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
tmp_date_list = []
# v2
# tmp_date_list.append('1983-01-01') # phase 1 - pi*1/4
# tmp_date_list.append('1993-04-01')
# tmp_date_list.append('2004-05-01')
# tmp_date_list.append('2022-10-01')
# tmp_date_list.append('1985-11-01') # phase 2 - pi*3/4
# tmp_date_list.append('1991-04-01')
# tmp_date_list.append('2000-01-01')
# tmp_date_list.append('2011-07-01')
tmp_date_list.append('1984-01-01') # phase 3 - pi*5/4
tmp_date_list.append('2001-04-01')
tmp_date_list.append('2005-07-01')
# tmp_date_list.append('2021-11-01')
# tmp_date_list.append('1987-07-01') # phase 4 - pi*7/4
# tmp_date_list.append('1994-10-01')
# tmp_date_list.append('2008-01-01')
# tmp_date_list.append('2015-04-01')
#---------------------------------------------------------------------------------------------------
compset = 'F20TR'
grid    = 'ne30pg2_r05_IcoswISC30E3r5'
ens_id = '2024-SCIDAC-PCOMP-TEST-01' # second test with alternate land/river configuration
# ens_id = '2024-SCIDAC-PCOMP-TEST-02' # second test with alternate land/river configuration
def get_case_name(e,c,h,d): return'.'.join(['E3SM',ens_id,grid,f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}',f'{d}'])
#---------------------------------------------------------------------------------------------------
case = []
gweff_list, cfrac_list, hdpth_list = [],[],[]
case_dir,case_sub = [],[]
case,name = [],[]
clr_list,dsh_list,mrk_list = [],[],[]
def add_case(e,c,h,d=None,n=None,p=None,s=None,dsh=0,clr='black',mrk=0):
   gweff_list.append(e)
   cfrac_list.append(c)
   hdpth_list.append(h)
   case_dir.append(p); case_sub.append(s)
   dsh_list.append(dsh)
   clr_list.append(clr)
   mrk_list.append(mrk)
#---------------------------------------------------------------------------------------------------
add_case(e=0.35,c=10,h=0.50,dsh=0,clr='green')     #  <<< v3 default
# add_case(e=0.12,c=16,h=0.48,dsh=0,clr='blue')      # prev surrogate optimum
add_case(e=0.09,c=20,h=0.25,dsh=0,clr='red')       # no QBO at all
add_case(e=0.70,c=21,h=0.31,dsh=0,clr='orange')    # QBO is too fast
#---------------------------------------------------------------------------------------------------
var,vname,lev_list,vclr,vdsh = [],[],[],[],[]
p_min_list = []
p_max_list = []
def add_var(var_in,name=None,lev=-1,c='black',d=0,p_min=None,p_max=None): 
   var.append(var_in); lev_list.append(lev)
   vclr.append(c); vdsh.append(d)
   vname.append(name)
   p_min_list.append(p_min)
   p_max_list.append(p_max)
#---------------------------------------------------------------------------------------------------
# add_var('u',        name='Zonal Wind')
# add_var('utendepfd',name='EP Flux Div',p_min=10e2,p_max=100e2) # u-wind tendency due to TEM EP flux divergence
# add_var('wf',       name='Wave Forcing',p_min=10e2,p_max=100e2) # u-wind tendency due to TEM EP flux divergence
# add_var('wf',       name='Wave Forcing',p_min=20e2,p_max= 70e2) # u-wind tendency due to TEM EP flux divergence
# add_var('wf',       name='Wave Forcing',p_min=30e2,p_max=100e2) # u-wind tendency due to TEM EP flux divergence


add_var('dudt',     name='dUdt',        p_min=20e2,p_max= 70e2) # 
add_var('utendvtem',name='utendvtem',   p_min=20e2,p_max= 70e2) # 
add_var('utendwtem',name='utendwtem',   p_min=20e2,p_max= 70e2) # 
add_var('wf',       name='Wave Forcing',p_min=20e2,p_max= 70e2) # u-wind tendency due to TEM EP flux divergence
#---------------------------------------------------------------------------------------------------

fig_file,fig_type = 'figs_QBO/QBO.pcomp.TEM.RMSE.v1','png'

lat1,lat2 = -5,5

remap_str,htype = 'remap_90x180','h0.tem'
# remap_str,htype = 'remap_90x180','h1.tem'

obs_root = '/global/cfs/cdirs/m4310/whannah/ERA5'

print_stats        = False
print_profile      = False

# p_min = 10e2
# p_max = 100e2

second_month = False

num_plot_col = len(var)

#-------------------------------------------------------------------------------
def lat_mean(da):
  cos_lat_rad = np.cos(da.lat*np.pi/180)
  return (da*cos_lat_rad).mean(dim='lat') / (cos_lat_rad).mean(dim='lat')
#-------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case_dir)
num_date = len(tmp_date_list)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)

res = hs.res_xy()
res.vpHeightF = 0.4
res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.02
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
res.tiXAxisFontHeightF  = 0.01
# res.tmYLMinorOn = True

res.tiXAxisString = f'Start Date'
res.tiYAxisString = f'RMSE'

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

# res.trYReverse = True
# res.xyYStyle = 'Log'

tend_vars = ['utendepfd','wf','dudt','utendvtem','utendwtem']
#---------------------------------------------------------------------------------------------------
def lat_mean(da,lat_name='lat'):
  cos_lat_rad = np.cos(da[lat_name]*np.pi/180)
  return (da*cos_lat_rad).mean(dim=lat_name) / (cos_lat_rad).mean(dim=lat_name)
#---------------------------------------------------------------------------------------------------
def get_data(ds,var,lat_name='lat',lev_name='plev'):
   global htype
   if var[v]=='wf': 
      if htype=='h1.tem':
         ds['dudt'] = ds['u'].differentiate(coord='time',datetime_unit='s')
         ds['residual'] =  ds['dudt'] - ds['utendvtem'] - ds['utendwtem']-ds['utendepfd']
         data = ds['residual'] + ds['utendepfd']
      else:
         data = ds['residual'] + ds['utendepfd']
   else:
      data = ds[var[v]]
   data = data.sel({lat_name:slice(lat1,lat2)})
   data = data.sel({lev_name:slice(200e2,0)})
   data = data.mean(dim='time')
   data = lat_mean( data, lat_name )
   if var[v] in tend_vars: data = data*86400.
   for i in range(len(data)):
      if data[lev_name][i]>p_max: data[i] = 0#np.nan
      if data[lev_name][i]<p_min: data[i] = 0#np.nan
   return data
#---------------------------------------------------------------------------------------------------
# h1_t1,h1_t2 = 0*4, 10*4
# h1_t1,h1_t2 = 5*4, 15*4
h1_t1,h1_t2 = 10*4, 20*4
h1_t1,h1_t2 = 15*4, 25*4
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print(' '*2+f'var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
   rmse = np.zeros([num_case,num_date])

   p_min = p_min_list[v]
   p_max = p_max_list[v]

   for t,date in enumerate(tmp_date_list):
      print(' '*4+f'date: {hc.tclr.MAGENTA}{date}{hc.tclr.END}')
      yr = date[0:4]
      mn_1 = date[5:7]
      mn_2 = f'{(int(mn_1)+1):02d}'
      #-------------------------------------------------------------------------
      print(' '*6+f'case: {hc.tclr.CYAN}ERA5{hc.tclr.END}')
      file_list = []
      if htype=='h0.tem':
         if second_month:
            file_list.append( f'{obs_root}/ERA5_monthly_{yr}{mn_2}_tem.nc' )
         else:
            file_list.append( f'{obs_root}/ERA5_monthly_{yr}{mn_1}_tem.nc' )
      if htype=='h1.tem':
         src_root = '/global/cfs/cdirs/m4310/wandiyu/ERA5'
         file_list.append( f'{src_root}/ERA5_6hrly_{yr:04}{mn_1:02}_tem.nc' )
      ds = xr.open_mfdataset(file_list)
      if htype=='h1.tem': ds = ds.isel(time=slice(h1_t1,h1_t2))
      ds = ds.rename({'latitude':'lat','level':'plev'})
      ds = ds.isel(plev=slice(None, None, -1))
      ds['plev'] = ds['plev']*1e2
      baseline = get_data(ds,var)

      # hc.print_stat(baseline,compact=True)
      # exit()
      #-------------------------------------------------------------------------
      for c in range(num_case):
         case = get_case_name( gweff_list[c], cfrac_list[c], hdpth_list[c], date )
         # print(' '*6+f'case: {hc.tclr.CYAN}{case}{hc.tclr.END}')
         file_list = []
         if htype=='h0.tem':
            if second_month:
               file_list.append( f'{gscratch}/{case}/data_{remap_str}_tem/{case}.eam.{htype}.{yr}-{mn_2}.{remap_str}.nc' )
            else:
               file_list.append( f'{gscratch}/{case}/data_{remap_str}_tem/{case}.eam.{htype}.{yr}-{mn_1}.{remap_str}.nc' )
         if htype=='h1.tem':
            file_path = f'{gscratch}/{case}/data_{remap_str}_tem/{case}.eam.{htype}.{yr}-{mn_1}-*.{remap_str}.nc'
            file_list = sorted( glob.glob( file_path ) )
         ds = xr.open_mfdataset(file_list,combine='nested',concat_dim='time')
         if htype=='h1.tem': ds = ds.isel(time=slice(h1_t1,h1_t2))
         data = get_data(ds,var)
         #----------------------------------------------------------------------
         error = np.ma.masked_invalid(data) - np.ma.masked_invalid(baseline)
         rmse[c,t] = np.sqrt( np.mean( np.square( error )))
         print(' '*4+f'case: {hc.tclr.CYAN}{case:30}{hc.tclr.END}   RMSE: {rmse[c,t]}')

   # exit()
   #----------------------------------------------------------------------------
   # Create plot
   ip = v
   
   tres = copy.deepcopy(res)
   
   # if var[v] in tend_vars: tres.tiXAxisString = f'[m/s/day]'
   # if var[v] =='u':        tres.tiXAxisString = f'[m/s]'
      
   tres.xyLineColors   = clr_list
   tres.xyMarkerColors = clr_list
   # tres.xyDashPatterns = dsh_list[c]
   tres.xyDashPattern = 0
   tres.xyLineThicknessF = 8
   tres.trXMinF          = 0-0.2
   tres.trXMaxF          = num_date-1+0.2
   tres.tmXBMode   = 'Explicit'
   tres.tmXBValues = np.arange(num_date)
   tres.tmXBLabels = tmp_date_list

   # if len(rmse)==1: rmse = [rmse[0],rmse[0]]

   plot[ip] = ngl.xy(wks, np.arange(num_date), rmse, tres)      
   

   if htype=='h1.tem': 
      d1 = int(h1_t1/4)
      d2 = int(h1_t2/4)
      hs.set_subtitles(wks, plot[ip], vname[v], f'days {d1}-{d2}', f'P: {(p_max/1e2)}-{(p_min/1e2)} mb', font_height=0.01)
   else:
      hs.set_subtitles(wks, plot[ip], vname[v], '', f'P: {(p_max/1e2)}-{(p_min/1e2)} mb', font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pres = hs.setres_panel()
ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
