import os, ngl, copy, xarray as xr, numpy as np, glob
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import cmocean
#---------------------------------------------------------------------------------------------------
# case_dir,case_sub = [],[]
# case,name = [],[]
# clr,dsh,mrk = [],[],[]
# def add_case(case_in,n=None,p=None,s=None,d=0,c='black',m=0):
#    global name,case,case_dir,case_sub,clr,dsh,mrk
#    case.append(case_in); name.append(n)
#    case_dir.append(p); case_sub.append(s)
#    dsh.append(d) ; clr.append(c) ; mrk.append(m)

# ##------------------------------------------------------------------------------
# cscratch = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl'
# gscratch = '/global/cfs/cdirs/m3312/whannah/2022-QBO-TEST'
# pscratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# ##------------------------------------------------------------------------------
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72.01',       n='E3SM control',     c='red'  ,p=gscratch,s='run')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72-nsu40.01', n='E3SM L72 smoothed',c='green',p=gscratch,s='run')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L80-rsu40.01', n='E3SM L80 refined', c='blue' ,p=gscratch,s='run')

#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72.GW-MOD-0', n='E3SM L72',      d=1,c='black', p=pscratch,s='run')
#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72-nsu40',    n='E3SM L72-nsu40',d=1,c='red',   p=pscratch,s='run')
#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72-rscl',     n='E3SM L72-rscl', d=1,c='purple',p=pscratch,s='run')
#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72-rlim',     n='E3SM L72-rlim', d=1,c='pink',  p=pscratch,s='run')

#---------------------------------------------------------------------------------------------------
gscratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
tmp_date_list = []
# tmp_date_list.append('1983-01-01') # phase 1 - pi*1/4
# tmp_date_list.append('1993-04-01')
# tmp_date_list.append('2002-07-01')
# tmp_date_list.append('2022-10-01')
# tmp_date_list.append('1986-10-01') # phase 1 - pi*3/4
# tmp_date_list.append('1995-07-01')
# tmp_date_list.append('2000-04-01')
# tmp_date_list.append('2009-01-01')
# tmp_date_list.append('1982-01-01') # phase 1 - pi*5/4
# tmp_date_list.append('1987-04-01')
# tmp_date_list.append('2014-07-01')
# tmp_date_list.append('2021-10-01')
# tmp_date_list.append('1984-10-01') # phase 1 - pi*7/4
# tmp_date_list.append('1994-07-01')
# tmp_date_list.append('2006-04-01')
# tmp_date_list.append('2013-01-01')

# v2
# tmp_date_list.append('1983-01-01') # phase 1 - pi*1/4
# tmp_date_list.append('1993-04-01')
# tmp_date_list.append('2004-05-01')
# tmp_date_list.append('2022-10-01')
# tmp_date_list.append('1985-11-01') # phase 2 - pi*3/4
# tmp_date_list.append('1991-04-01')
# tmp_date_list.append('2000-01-01')
# tmp_date_list.append('2011-07-01')
tmp_date_list.append('1984-01-01') # phase 3 - pi*5/4
tmp_date_list.append('2001-04-01')
tmp_date_list.append('2005-07-01')
# tmp_date_list.append('2021-11-01')
# tmp_date_list.append('1987-07-01') # phase 4 - pi*7/4
# tmp_date_list.append('1994-10-01')
# tmp_date_list.append('2008-01-01')
# tmp_date_list.append('2015-04-01')

# second test ensemble
# tmp_date_list.append('2004-05-01')
# tmp_date_list.append('1985-11-01')
# tmp_date_list.append('1984-01-01')
# tmp_date_list.append('1987-07-01')

#---------------------------------------------------------------------------------------------------
compset = 'F20TR'
grid    = 'ne30pg2_r05_IcoswISC30E3r5'
# ens_id  = '2024-SCIDAC-PCOMP-TEST' # initial test to work out logistical issues
ens_id = '2024-SCIDAC-PCOMP-TEST-01' # second test with alternate land/river configuration
def get_case_name(e,c,h,d): return'.'.join(['E3SM',ens_id,grid,f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}',f'{d}'])
#---------------------------------------------------------------------------------------------------
case = []
gweff_list, cfrac_list, hdpth_list = [],[],[]
# start_date_list = []
case_dir,case_sub = [],[]
case,name = [],[]
clr_list,dsh_list,mrk_list = [],[],[]
def add_case(e,c,h,d=None,n=None,p=None,s=None,dsh=0,clr='black',mrk=0):
   gweff_list.append(e)
   cfrac_list.append(c)
   hdpth_list.append(h)
   # case.append( get_case_name(e,c,h,d) )
   # name.append(n)
   case_dir.append(p); case_sub.append(s)
   dsh_list.append(dsh)
   clr_list.append(clr)
   mrk_list.append(mrk)
   # start_date_list.append(d)
#---------------------------------------------------------------------------------------------------
# # temporary case list for dev
# tdate = '1983-01-01'
# add_case(e=0.35,c=10,h=0.50,d=tdate,dsh=0,clr='red') #  <<< v3 default
# add_case(e=0.12,c=16,h=0.48,d=tdate,dsh=0,clr='blue') # prev surrogate optimum
# tdate = '1993-04-01'
# add_case(e=0.35,c=10,h=0.50,d=tdate,dsh=1,clr='red') #  <<< v3 default
# add_case(e=0.12,c=16,h=0.48,d=tdate,dsh=1,clr='blue') # prev surrogate optimum
#---------------------------------------------------------------------------------------------------
# # build list of cases with all dates
# for t,date in enumerate(tmp_date_list):
#    add_case(e=0.35,c=10,h=0.50,d=date,dsh=0,clr='red') #  <<< v3 default
#    add_case(e=0.12,c=16,h=0.48,d=date,dsh=0,clr='blue') # prev surrogate optimum
#    # add_case(e=0.09,c=20,h=0.25,d=date) # no QBO at all
#    # add_case(e=0.70,c=21,h=0.31,d=date) # QBO is too fast
#---------------------------------------------------------------------------------------------------
add_case(None,None,None,    dsh=0,clr='black')  # ERA5
add_case(e=0.35,c=10,h=0.50,dsh=0,clr='green')    #  <<< v3 default
# add_case(e=0.12,c=16,h=0.48,dsh=0,clr='blue')   # prev surrogate optimum
add_case(e=0.09,c=20,h=0.25,dsh=0,clr='red')  # no QBO at all
add_case(e=0.70,c=21,h=0.31,dsh=0,clr='orange')    # QBO is too fast
#---------------------------------------------------------------------------------------------------
var,vname,lev_list,vclr,vdsh = [],[],[],[],[]
def add_var(var_in,name=None,lev=-1,c='black',d=0): 
   var.append(var_in); lev_list.append(lev)
   vclr.append(c); vdsh.append(d)
   vname.append(name)
#---------------------------------------------------------------------------------------------------
add_var('u',        name='Zonal Wind')
# add_var('utendepfd',name='EP Flux Div') # u-wind tendency due to TEM EP flux divergence
# add_var('residual', name='Residual') 
# add_var('wf',       name='Wave Forcing') # Total wave forcing estimate
# add_var('dudt',     name='dudt') # Total Eulerian u-wind tendency

compare_second_month = False
#---------------------------------------------------------------------------------------------------

# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])

if compare_second_month:
   num_plot_col = len(var)+2
else:
   num_plot_col = len(var)


fig_file,fig_type = 'figs_QBO/QBO.pcomp.TEM.profile.v1','png'

# lat1,lat2 = -5,5
lat1,lat2 = -15,15

# htype,first_file,num_files = 'h2',0,10#365*1
remap_str,htype = 'remap_90x180','h0.tem'

obs_root = '/global/cfs/cdirs/m4310/whannah/ERA5'

print_stats        = False
print_profile      = False

p_min = 10
p_max = 100

#-------------------------------------------------------------------------------
def lat_mean(da):
  cos_lat_rad = np.cos(da.lat*np.pi/180)
  return (da*cos_lat_rad).mean(dim='lat') / (cos_lat_rad).mean(dim='lat')
#-------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case_dir)
num_date = len(tmp_date_list)

wks = ngl.open_wks(fig_type,fig_file)
# plot = [None]*(num_var*num_date)
if compare_second_month:
   plot = [None]*((num_var+2)*num_date)
else:
   plot = [None]*(num_var*num_date)

res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.02
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
res.tiXAxisFontHeightF  = 0.015
# res.tmYLMinorOn = True

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

res.trYReverse = True
# res.xyYStyle = 'Log'

tend_vars = ['utendepfd','wf','dudt']
#---------------------------------------------------------------------------------------------------
def lat_mean(da,lat_name='lat'):
  cos_lat_rad = np.cos(da[lat_name]*np.pi/180)
  return (da*cos_lat_rad).mean(dim=lat_name) / (cos_lat_rad).mean(dim=lat_name)
#---------------------------------------------------------------------------------------------------
def get_data(ds,var,lat_name='lat',lev_name='plev'):
   if var[v]=='wf':
      data = ds['residual'] + ds['utendepfd']
   else:
      data = ds[var[v]]
   data = data.sel({lat_name:slice(lat1,lat2)})
   data = data.sel({lev_name:slice(200e2,0)})
   # data = data.mean(dim='time')
   data = lat_mean( data, lat_name )
   if var[v] in tend_vars:data = data*86400.

   # for i in range(len(data)):
   #    if data[lev_name][i]>p_max: data[i] = 0#np.nan
   #    if data[lev_name][i]<p_min: data[i] = 0#np.nan

   return data
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
data_list_list_2 = []
for v in range(num_var):
   hc.printline()
   print(' '*2+f'var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
   for t,date in enumerate(tmp_date_list):
      print(' '*4+f'date: {hc.tclr.MAGENTA}{date}{hc.tclr.END}')
      yr,mn = date[0:4],date[5:7]
      mn_2 = f'{(int(mn)+1):02d}'
      data_list,lev_list = [],[]
      data_list_2 = []
      for c in range(num_case):
         if gweff_list[c] is None:
            print(' '*6+f'case: {hc.tclr.CYAN}ERA5{hc.tclr.END}')
            ds = xr.open_mfdataset(f'{obs_root}/ERA5_monthly_{yr}{mn}_tem.nc')
            ds = ds.rename({'latitude':'lat','level':'plev'})
            ds = ds.isel(plev=slice(None, None, -1))
            ds = ds.isel(time=0)
            ds['plev'] = ds['plev']*1e2

            ds_2 = xr.open_mfdataset(f'{obs_root}/ERA5_monthly_{yr}{mn_2}_tem.nc')
            ds_2 = ds_2.rename({'latitude':'lat','level':'plev'})
            ds_2 = ds_2.isel(plev=slice(None, None, -1),time=0)
            ds_2['plev'] = ds_2['plev']*1e2
         else:
            case = get_case_name( gweff_list[c], cfrac_list[c], hdpth_list[c], date )
            print(' '*6+f'case: {hc.tclr.CYAN}{case}{hc.tclr.END}')
            file_path = f'{gscratch}/{case}/data_{remap_str}_tem/{case}.eam.{htype}.{yr}-{mn}.{remap_str}.nc'
            ds = xr.open_dataset(file_path)
            ds = ds.isel(time=0)

            file_path = f'{gscratch}/{case}/data_{remap_str}_tem/{case}.eam.{htype}.{yr}-{mn_2}.{remap_str}.nc'
            ds_2 = xr.open_dataset(file_path)
            ds_2 = ds_2.isel(time=0)
         #----------------------------------------------------------------------
         data = get_data(ds,var)
         data_2 = get_data(ds_2,var)
         #----------------------------------------------------------------------
         # append final data to list
         data_list.append( data.values )
         lev_list.append( data['plev'].values/1e2 )

         data_list_2.append( data_2.values )
      #-------------------------------------------------------------------------
      data_list_list.append(data_list)
      lev_list_list.append(lev_list)
      data_list_list_2.append(data_list_2)

#---------------------------------------------------------------------------------------------------
# Create plot - overlay all cases for each var
for v in range(num_var):
   for t,date in enumerate(tmp_date_list):

      # ip = v
      if compare_second_month:
         ip   = t*(num_var+2)+v
         ip_2 = t*(num_var+2)+v+1
      else:
         ip = t*num_var+v
      

      data_list = data_list_list[v*num_date+t]
      lev_list = lev_list_list[v*num_date+t]

      data_list_2 = data_list_list_2[v*num_date+t]
      
      #-------------------------------------------------------------------------
      for c in range(len(data_list)):
         for i in range(len(data_list[0])):
            if lev_list[c][i]>p_max: data_list[c][i] = 0#np.nan
            if lev_list[c][i]<p_min: data_list[c][i] = 0#np.nan

            if lev_list[c][i]>p_max: data_list_2[c][i] = 0#np.nan
            if lev_list[c][i]<p_min: data_list_2[c][i] = 0#np.nan

      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.trYMinF = p_min
      tres.trYMaxF = p_max

      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])

      tres.trXMinF = data_min
      tres.trXMaxF = data_max

      if var[v] in tend_vars:
         tres.trXMinF = -0.6
         tres.trXMaxF =  0.6

      if var[v] in tend_vars: tres.tiXAxisString = f'[m/s/day]'
      if var[v] =='u':        tres.tiXAxisString = f'[m/s]'
      #-------------------------------------------------------------------------
      for c in range(num_case):   
         tres.xyLineColor   = clr_list[c]
         tres.xyMarkerColor = clr_list[c]
         # tres.xyDashPattern = dsh_list[c]
         tres.xyDashPattern = 0
         tres.xyLineThicknessF = 12 if clr_list[c]=='black' else 8

         # tplot = ngl.xy(wks, np.ma.masked_invalid(data_list[c]), lev_list[c], tres)

         tplot = ngl.xy(wks, np.ma.masked_invalid(data_list[c]), lev_list[c], tres)         
         if compare_second_month:
            tres.xyDashPattern = 1
            tplot_2 = ngl.xy(wks, np.ma.masked_invalid(data_list_2[c]), lev_list[c], tres)
         
         # if c==0 and t==0:
         #    plot[ip] = tplot
         # else:
         #    ngl.overlay(plot[ip],tplot)

         if c==0 :
            plot[ip] = tplot
            if compare_second_month: plot[ip_2] = tplot_2
         else:
            ngl.overlay(plot[ip],tplot)
            if compare_second_month: ngl.overlay(plot[ip_2],tplot_2)

         # tres.xyDashPattern = 1
         # ngl.overlay(plot[ip], ngl.xy(wks, np.ma.masked_invalid(data_list_2[c]), lev_list[c], tres) )
      #-------------------------------------------------------------------------
      # add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))
      #-------------------------------------------------------------------------
      # reg_str = ''
      # if 'lat1' in locals(): 
      #    lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
      #    lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
      #    reg_str += f' {lat1_str}:{lat2_str} '
      # if 'lon1' in locals(): 
      #    lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
      #    lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
      #    reg_str += f' {lon1_str}:{lon2_str} '
      #-------------------------------------------------------------------------
      # hs.set_subtitles(wks, plot[ip], date[:(4+1+2)], '', vname[v], font_height=0.01)

      yr = date[0:4]
      mn_1 = date[5:7]
      mn_2 = f'{(int(mn_1)+1):02d}'
      hs.set_subtitles(wks, plot[ip], f'{yr}-{mn_1}', '', vname[v], font_height=0.01)
      if compare_second_month: 
         hs.set_subtitles(wks, plot[ip_2], f'{yr}-{mn_2}', '', vname[v], font_height=0.01)
#---------------------------------------------------------------------------------------------------
# add RMSE panel
if compare_second_month:
   print('-'*80)
   print('Adding RMSE panel')
   print()
   for t,date in enumerate(tmp_date_list):
      yr = date[0:4]
      mn_1 = date[5:7]
      mn_2 = f'{(int(mn_1)+1):02d}'
      date1 = f'{yr}-{mn_1}'
      date2 = f'{yr}-{mn_2}'
      print(' '*2+f'date 1/2: {hc.tclr.MAGENTA}{date1}{hc.tclr.END} / {hc.tclr.MAGENTA}{date2}{hc.tclr.END}')
      rmse_list = []
      v=0
      data_list   = data_list_list  [v*num_date+t]
      data_list_2 = data_list_list_2[v*num_date+t]
      for c in range(num_case):
         data_1 = data_list[c]
         data_2 = data_list_2[c]
         if c==0: 
            baseline_1 = data_1
            baseline_2 = data_2

            # hc.print_stat(baseline_1,name='baseline_1',compact=True)
            # hc.print_stat(baseline_2,name='baseline_2',compact=True)
            # exit()

         else:
            rmse_1 =  np.sqrt( np.mean( np.square( np.ma.masked_invalid(data_1) - np.ma.masked_invalid(baseline_1) )))
            rmse_2 =  np.sqrt( np.mean( np.square( np.ma.masked_invalid(data_2) - np.ma.masked_invalid(baseline_2) )))
            rmse_list.append(np.array([rmse_1,rmse_2]))
            print(' '*4+f'case: {hc.tclr.CYAN}{case:30}{hc.tclr.END}   RMSE 1/2: {rmse_1} / {rmse_2}')
      print()
      print('-'*80)

      # exit()

      tres = copy.deepcopy(res)
      tres.trYReverse = False
      tres.xyMarkLineMode   = "MarkLines"
      tres.xyLineColors     = clr_list[1:]
      tres.xyMarkerColors   = clr_list[1:]
      tres.xyLineThicknessF = 12
      tres.trXMinF          = -0.1
      tres.trXMaxF          =  1.1
      tres.tmXBMode   = 'Explicit'
      tres.tmXBValues = [0,1]
      tres.tmXBLabels = [date1,date2]

      ip = t*(num_var+2)+v+2

      plot[ip] = ngl.xy(wks, np.arange(num_case), np.array(rmse_list), tres)

      hs.set_subtitles(wks, plot[ip], 'RMSE', '', vname[0], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
