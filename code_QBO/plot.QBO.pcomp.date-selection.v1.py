import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import random
#-------------------------------------------------------------------------------

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.phase.timeseries.v1','png'

#-------------------------------------------------------------------------------
# Read file 

file = open(os.getenv('HOME')+'/Research/E3SM/QBO_EOF_coeff.txt','r')
lines = file.read().split('\n')
file.close()

phc_list = []
phc_list.append( np.pi*1./4. )
phc_list.append( np.pi*3./4. )
phc_list.append( np.pi*5./4. - 2*np.pi )
phc_list.append( np.pi*7./4. - 2*np.pi )

mn = []
yr = []
e1 = []
e2 = []
phc = []
for l in lines[8:]:
  # print(l.split())
  # exit()
  if len(l)>0:
    mn.append(  int(l.split()[0]))
    yr.append(  int(l.split()[1]))
    e1.append(float(l.split()[2]))
    e2.append(float(l.split()[3]))
    # ph.append(l.split()[-1])
    if l.split()[-1]=='W' : phc.append( phc_list[0] )
    if l.split()[-1]=='TE': phc.append( phc_list[1] )
    if l.split()[-1]=='E' : phc.append( phc_list[2] )
    if l.split()[-1]=='TW': phc.append( phc_list[3] )

time_coord = np.array(yr) + (np.array(mn)-1)/12

#-------------------------------------------------------------------------------
# Calculate QBO phase

eof1 = np.array(e1)
eof2 = np.array(e2)

phs = np.arctan2( eof2, eof1 ) #+ np.pi

#-------------------------------------------------------------------------------
# print list to manually identify dates for psuedo composite

#for t,p in enumerate(phc):

#exit()
#-------------------------------------------------------------------------------
# Print list of time indices and yr/mn for each phase

# for p in range(4):
#   print(f'  Phase: {p+1}')
#   for t,ph in enumerate(phc):
#     if ph==phc_list[p]: 
#       if mn[t] in [1,4,7,10]:
#         print(f'    {t:4d}  {yr[t]:4d}  {mn[t]:2d}')

# exit()
#-------------------------------------------------------------------------------
# Print compact table of time indices and yr/mn for each phase

# msg = []
# mlen = 0
# for p in range(4):
#   pcnt = 0
#   #print(f'  Phase: {p}')
#   tmsg = f'    Phase {p+1}' 
#   tmsg = tmsg + ' '*(4+13-len(tmsg))
#   if len(msg)==0:
#     msg.append(tmsg); pcnt+=1
#   else:
#     msg[0] = msg[0]+tmsg
#   for t,ph in enumerate(phc):
#     if ph==phc_list[p]: 
#       #phs_1_list.append(t):
#       if mn[t] in [1,4,7,10]:
#         # print(f'    {t:4d}  {yr[t]:4d}  {mn[t]:2d}')
#         tmsg = f'    {t:04d}  {yr[t]:4d}-{mn[t]:02d}'
#         pcnt+=1
#         if len(msg)<(pcnt+1):
#           msg.append(tmsg)
#         else:
#           msg[pcnt] = msg[pcnt] + tmsg
# for m in msg: print(m)

#-------------------------------------------------------------------------------
# Print compact table - prioritize quadrant centering

msg = []; mlen = 0
for p in range(4):
  pcnt = 0
  tmsg = f'    Phase {p+1}' 
  tmsg = tmsg + ' '*(4+13-len(tmsg))
  if len(msg)==0: 
    msg.append(tmsg); pcnt+=1
  else: 
    msg[0] = msg[0]+tmsg
  for t in range(len(phs)):
    dphs = np.pi/8
    if phs[t]>=(phc_list[p]-dphs) and phs[t]<(phc_list[p]+dphs):
      # if mn[t] in [1,4,7,10]:
      if mn[t] in [1,2,4,5,7,8,10,11]:
      # if True:
        # print(f'    {t:4d}  {yr[t]:4d}  {mn[t]:2d}')
        tmsg = f'    {t:04d}  {yr[t]:4d}-{mn[t]:02d}'
        pcnt+=1
        if len(msg)<(pcnt+1):
          msg.append(tmsg)
        else:
          msg[pcnt] = msg[pcnt] + tmsg
for m in msg: print(m)

exit()

#-------------------------------------------------------------------------------
# build list of time indices for each phase
phs_1_list = [] # W
phs_2_list = [] # TE
phs_3_list = [] # E
phs_4_list = [] # TW
for t,p in enumerate(phc):
  if p==phc_list[0]: phs_1_list.append(t)
  if p==phc_list[1]: phs_2_list.append(t)
  if p==phc_list[2]: phs_3_list.append(t)
  if p==phc_list[3]: phs_4_list.append(t)


#-------------------------------------------------------------------------------
# def calc_min_diff():
#   min_diff_list = []
#   for p in range(4):
#     min_diff = int(1e5)
#     for i in range(4):
#       for ii in range(4):
#         if i!=ii:
#           diff = np.absolute( smp[p,i] - smp[p,ii] )
#           if diff < min_diff: min_diff = diff
#     # print(f'p: {p}  min_diff: {min_diff}')
#     min_diff_list.append(min_diff)
#   return min_diff_list
#-------------------------------------------------------------------------------
# # Find 4x "random" samples of each phase
#smp = np.array([[None]*4]*4)
#random.seed(12345678)
# smp[0,:] = random.sample(phs_1_list,4)
# smp[1,:] = random.sample(phs_2_list,4)
# smp[2,:] = random.sample(phs_3_list,4)
# smp[3,:] = random.sample(phs_4_list,4)
#-------------------------------------------------------------------------------
# # Find 4x "random" samples of each phase - alt sampling to ensure things are spaced out
# smp = np.array([[None]*4]*4)
# for t,p in enumerate(phc):
#   p==phs_1:
#   p==phs_2:
#   p==phs_3:
#   p==phs_4:
# for i in range(4):
#   t1 = 10*i
#   t2 = 10*(i+1)
#   print(phs_1_list[t1:t2])
#   print( random.sample( phs_1_list[t1:t2] ,1)[0] )
#   # smp[0,i] = random.sample( phs_1_list[t1:t2] ,1)[0]
#   # smp[1,i] = random.sample( phs_2_list[t1:t2] ,1)[0]
#   # smp[2,i] = random.sample( phs_3_list[t1:t2] ,1)[0]
#   # smp[3,i] = random.sample( phs_4_list[t1:t2] ,1)[0]
#-------------------------------------------------------------------------------
# first batch of dates

smp = np.array( [[None]*4]*4 )

smp[0,:] = np.array( [ 48, 171, 282, 525 ] )
smp[1,:] = np.array( [ 93, 198, 255, 360 ] )
smp[2,:] = np.array( [ 36,  99, 426, 513 ] )
smp[3,:] = np.array( [ 69, 186, 327, 408 ] )

#-------------------------------------------------------------------------------
# Alt dates - prioritize centering on phase quadrants

smp = np.array( [[None]*4]*4 )

smp[0,:] = np.array( [ 48, 171, 282, 525 ] )
smp[1,:] = np.array( [ 93, 198, 255, 360 ] )
smp[2,:] = np.array( [ 36,  99, 426, 513 ] )
smp[3,:] = np.array( [ 69, 186, 327, 408 ] )

#-------------------------------------------------------------------------------
# print sample dates
print('Samples:')
for p in range(4):
  print(); print(f'  Phase {p+1}:')
  for i in range(4):
    t = int(smp[p,i])
    print(f'    {t:4d}  {yr[t]}-{mn[t]:02d}')

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

