import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import random
#-------------------------------------------------------------------------------

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.pcomp.profile.v1','png'


#-------------------------------------------------------------------------------
num_phs = 4
num_smp = 4

# # v1
# phase1 = [ 36, 159, 270, 513]
# phase2 = [ 81, 186, 243, 348]
# phase3 = [ 24,  87, 414, 501]
# phase4 = [ 57, 174, 315, 396]


adj = 12 # time adjustment to account for 1979 vs 1980 start

# v2
phase1 = [ 48-adj, 171-adj, 304-adj, 525-adj ]
phase2 = [ 82-adj, 147-adj, 252-adj, 390-adj ]
phase3 = [ 60-adj, 267-adj, 318-adj, 514-adj ]
phase4 = [102-adj, 189-adj, 348-adj, 435-adj ]

phases = [phase1, phase2, phase3, phase4]
#-------------------------------------------------------------------------------
def lat_mean(da):
  cos_lat_rad = np.cos(da.lat*np.pi/180)
  return (da*cos_lat_rad).mean(dim='lat') / (cos_lat_rad).mean(dim='lat')
#-------------------------------------------------------------------------------
# Read data
# ds = xr.open_dataset('/pscratch/sd/w/wandiyu/monthly_data/ERA5_6hrly_tem_monthly_mean_90x180_1980-2022.nc')
ds = xr.open_dataset('/global/cfs/cdirs/m4310/whannah/ERA5_6hrly_tem_monthly_mean_90x180_1980-2022.nc')

# print(ds['lev'].values); exit()

ds = ds.sel(lev=[100.,70.,50.,30.,20.,10.,7.,5.])

ds['dudt'] = ds['u'].differentiate(coord='time',datetime_unit='s')
ds['residual'] =  ds['dudt'] - ds['utendvtem'] - ds['utendwtem']-ds['utendepfd']
ds['wf'] = ds['residual'] + ds['utendepfd']
#-------------------------------------------------------------------------------
# ds = ds.isel(time=phases[i])
ds = ds.sel(lat=slice(-5,5))
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Create plot
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_phs

res = hs.res_xy()
# res.vpHeightF               = 0.1
# res.xyLineThicknessF        = 8
res.tmYLLabelFontHeightF    = 0.015
res.tmXBLabelFontHeightF    = 0.015
res.tiXAxisFontHeightF      = 0.015
res.tiYAxisFontHeightF      = 0.015
res.xyLineThicknessF        = 8
res.trYReverse              = True
res.trYMinF                 =  10
res.trYMaxF                 = 100
res.trXMinF                 = -50
res.trXMaxF                 =  50

ures = copy.deepcopy(res)
fres = copy.deepcopy(res)

lev = ds['lev'].values

# print(); print(ds[ 'u'])
# print(); print(ds[ 'u'].lat)
# print()
# print(); print(ds['wf'])
# print(); print(ds['wf'].lat)
# print()
# exit()

ds[ 'u'] = lat_mean( ds[ 'u'] )
ds['wf'] = lat_mean( ds['wf'] )

u_mag = np.max(np.absolute(ds[ 'u'].values))
f_mag = np.max(np.absolute(ds['wf'].values))
scale = ( u_mag / f_mag ) * 3

for p in range(num_phs):
  #-----------------------------------------------------
  tmp_u = ds[ 'u'].isel(time=phases[p])
  tmp_f = ds['wf'].isel(time=phases[p])
  # tmp_u = lat_mean( ds[ 'u'].isel(time=phases[p]) )
  # tmp_f = lat_mean( ds['wf'].isel(time=phases[p]) )
  # tmp_u = lat_mean( ds[ 'u'].isel(time=phases[p]) ).mean(dim='time').values
  # tmp_f = lat_mean( ds['wf'].isel(time=phases[p]) ).mean(dim='time').values
  #-----------------------------------------------------
  # u_mag = np.max(np.absolute(tmp_u.values))
  # f_mag = np.max(np.absolute(tmp_f.values))
  # scale = u_mag / f_mag
  #-----------------------------------------------------
  tmp_u = tmp_u.mean(dim='time').values
  tmp_f = tmp_f.mean(dim='time').values * scale
  #-----------------------------------------------------
  ures.xyLineColor = 'green'
  fres.xyLineColor = 'magenta'
  ures.xyLineThicknessF = 8
  fres.xyLineThicknessF = 8
  plot[p] =             ngl.xy(wks, tmp_u, ds['lev'].values, ures)
  ngl.overlay( plot[p], ngl.xy(wks, tmp_f, ds['lev'].values, fres) )
  hs.set_subtitles(wks, plot[p], '', f'Phase {(p+1)}', '', font_height=0.015)
  for i in range(num_smp):
    # tmp_u = lat_mean(  ds[ 'u'].isel(time=phases[p][i]) ).values
    # tmp_f = lat_mean(  ds['wf'].isel(time=phases[p][i]) ).values * scale
    tmp_u = ds[ 'u'].isel(time=phases[p][i]).values
    tmp_f = ds['wf'].isel(time=phases[p][i]).values * scale
    ures.xyLineColor = 'green'
    fres.xyLineColor = 'magenta'
    ures.xyLineThicknessF = 1
    fres.xyLineThicknessF = 1
    ngl.overlay( plot[p], ngl.xy(wks, tmp_u, lev, ures) )
    ngl.overlay( plot[p], ngl.xy(wks, tmp_f, lev, fres) )

#-------------------------------------------------------------------------------
# Add line at zero
xx = np.array([-1e12,1e12])
lres = hs.res_xy()
lres.xyLineColor = 'black'
lres.xyLineThicknessF = 1
lres.xyDashPattern = 0
for p in range(num_phs):
  ngl.overlay( plot[p], ngl.xy(wks, xx*0, xx, lres))
#-------------------------------------------------------------------------------
# layout = [len(plot),1]

num_plot_col = num_phs
layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

# ngl.draw(plot[1]); ngl.frame(wks)

hc.trim_png(fig_file)
