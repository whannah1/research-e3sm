import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import random
#-------------------------------------------------------------------------------

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.phase-composite.v1','png'


#-------------------------------------------------------------------------------
# Read EOF index file 

file = open(os.getenv('HOME')+'/Research/E3SM/QBO_EOF_coeff.txt','r')
lines = file.read().split('\n')
file.close()

phc_list = []
phc_list.append( np.pi*1./4. )
phc_list.append( np.pi*3./4. )
phc_list.append( np.pi*5./4. - 2*np.pi )
phc_list.append( np.pi*7./4. - 2*np.pi )

mn = []
yr = []
e1 = []
e2 = []
phc = []
for l in lines[8:]:
  if len(l)>0:
    mn.append(  int(l.split()[0]))
    yr.append(  int(l.split()[1]))
    e1.append(float(l.split()[2]))
    e2.append(float(l.split()[3]))
    # ph.append(l.split()[-1])
    # if l.split()[-1]=='W' : phc.append( phc_list[0] )
    # if l.split()[-1]=='TE': phc.append( phc_list[1] )
    # if l.split()[-1]=='E' : phc.append( phc_list[2] )
    # if l.split()[-1]=='TW': phc.append( phc_list[3] )
time_coord = np.array(yr) + (np.array(mn)-1)/12
for t in range(len(time_coord)):
  if time_coord[t]==1980: t1 = t
  if time_coord[t]==2023: t2 = t
time_coord = time_coord[t1:t2]
e1 = e1[t1:t2]
e2 = e2[t1:t2]
# Calculate QBO phase
eof1 = np.array(e1)
eof2 = np.array(e2)
phs = np.arctan2( eof2, eof1 ) + np.pi
#-------------------------------------------------------------------------------
# print(); print(f'min phs: {np.min(phs)}')
# print(); print(f'max phs: {np.max(phs)}')
# exit()
#-------------------------------------------------------------------------------
def lat_mean(da):
  cos_lat_rad = np.cos(da.lat*np.pi/180)
  return (da*cos_lat_rad).mean(dim='lat') / (cos_lat_rad).mean(dim='lat')
#-------------------------------------------------------------------------------
# Read data
ds = xr.open_dataset('/global/cfs/cdirs/m4310/whannah/ERA5_6hrly_tem_monthly_mean_90x180_1980-2022.nc')

# print(ds['lev'].values)
# exit()

ds = ds.sel(lev = [ 100.,   70.,   50.,   30., 20.,   10. ] )

ds['dudt'] = ds['u'].differentiate(coord='time',datetime_unit='s')
ds['residual'] =  ds['dudt'] - ds['utendvtem'] - ds['utendwtem']-ds['utendepfd']
ds['wf'] = ds['residual'] + ds['utendepfd']
#-------------------------------------------------------------------------------
# ds = ds.isel(time=phases[i])
tmp_u = lat_mean( ds[ 'u'].sel(lat=slice(-5,5)) )
tmp_f = lat_mean( ds['wf'].sel(lat=slice(-5,5)) )

lev = ds['lev'].values
num_lev = len(lev)

num_phs = 24#12
phs_coord = np.empty(num_phs)
phs_comp_u = np.empty([num_phs,num_lev])
phs_comp_f = np.empty([num_phs,num_lev])
for p in range(num_phs):
  phs_coord[p] = p * 2*np.pi / num_phs
  phs_min = phs_coord[p] - (2*np.pi/num_phs)/2.
  phs_max = phs_coord[p] + (2*np.pi/num_phs)/2.
  # print(f'{phs_min:8.2f}  {phs_coord[p]:8.2f}  {phs_max:8.2f}')
  phs_list = []
  for t in range(len(time_coord)):
    if phs_max>(2*np.pi):
      if phs[t]>=phs_min and phs[t]<(phs_max-2*np.pi): 
        phs_list.append(t)
    else:
      if phs[t]>=phs_min and phs[t]<phs_max: 
        phs_list.append(t)
  phs_comp_u[p,:] = tmp_u.isel(time=phs_list).mean(dim='time').values
  phs_comp_f[p,:] = tmp_f.isel(time=phs_list).mean(dim='time').values

# print(); print(len(phs))
# print(); print(da)
# print(); print(phs_comp)
# exit()


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Create plot
wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]
res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF = 0.015
res.tmXBLabelFontHeightF = 0.015
res.tiXAxisFontHeightF   = 0.015
res.tiYAxisFontHeightF   = 0.015
res.lbLabelBarOn         = False
res.trYReverse           = True
res.tiXAxisString        = 'Phase'

# res.tiYAxisString = 'Pressure [hPa]'
# res.trYReverse = True
# res.trYMinF = 10
# res.trYMaxF = 100

# tm_vals = [1,10,50,100,200]
# res.tmYLMode = 'Explicit'
# res.tmYLValues = tm_vals
# res.tmYLLabels = tm_vals

tm_vals = [0,np.pi*2/4,np.pi,np.pi*6/4]
res.tmXBMode = 'Explicit'
res.tmXBValues = tm_vals
res.tmXBLabels = tm_vals

res.sfYArray = lev
res.sfXArray = phs_coord

res.cnFillOn  = True
res.cnLinesOn = False

plot[0] = ngl.contour(wks, phs_comp_f.transpose(), res)

res.cnLineLabelsOn = False
res.cnInfoLabelOn  = False
res.cnFillOn  = False
res.cnLinesOn = True

ngl.overlay( plot[0], ngl.contour(wks, phs_comp_u.transpose(), res) )

hs.set_subtitles(wks, plot[0], '', 'QBO EOF Phase Composite', '', font_height=0.015)

#-------------------------------------------------------------------------------
# # Add line at zero
# xx = np.array([-1e12,1e12])
# lres = hs.res_xy()
# lres.xyLineColor = 'black'
# lres.xyLineThicknessF = 1
# lres.xyDashPattern = 0
# for p in range(num_phs):
#   ngl.overlay( plot[p], ngl.xy(wks, xx*0, xx, lres))
#-------------------------------------------------------------------------------
# layout = [len(plot),1]

# num_plot_col = num_phs
# layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

layout = [1,1]

pnl_res = hs.setres_panel()
# pnl_res.nglPanelYWhiteSpacePercent = 5
ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

# ngl.draw(plot[1]); ngl.frame(wks)

hc.trim_png(fig_file)
