import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import random
#-------------------------------------------------------------------------------

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.phase.timeseries.v1','png'

#-------------------------------------------------------------------------------
# Read file 

file = open(os.getenv('HOME')+'/Research/E3SM/QBO_EOF_coeff.txt','r')
lines = file.read().split('\n')
file.close()

phc_list = []
phc_list.append( np.pi*1./4. )
phc_list.append( np.pi*3./4. )
phc_list.append( np.pi*5./4. - 2*np.pi )
phc_list.append( np.pi*7./4. - 2*np.pi )

mn = []
yr = []
e1 = []
e2 = []
phc = []
for l in lines[8:]:
  # print(l.split())
  # exit()
  if len(l)>0:
    mn.append(  int(l.split()[0]))
    yr.append(  int(l.split()[1]))
    e1.append(float(l.split()[2]))
    e2.append(float(l.split()[3]))
    # ph.append(l.split()[-1])
    if l.split()[-1]=='W' : phc.append( phc_list[0] )
    if l.split()[-1]=='TE': phc.append( phc_list[1] )
    if l.split()[-1]=='E' : phc.append( phc_list[2] )
    if l.split()[-1]=='TW': phc.append( phc_list[3] )

time_coord = np.array(yr) + (np.array(mn)-1)/12

#-------------------------------------------------------------------------------
# Calculate QBO phase

eof1 = np.array(e1)
eof2 = np.array(e2)

phs = np.arctan2( eof2, eof1 )

#-------------------------------------------------------------------------------
# print list to manually identify dates for psuedo composite

#for t,p in enumerate(phc):

#exit()
#-------------------------------------------------------------------------------
# Print list of time indices and yr/mn for each phase

# for p in range(4):
#   print(f'  Phase: {p+1}')
#   for t,ph in enumerate(phc):
#     if ph==phc_list[p]: 
#       if mn[t] in [1,4,7,10]:
#         print(f'    {t:4d}  {yr[t]:4d}  {mn[t]:2d}')

# exit()
#-------------------------------------------------------------------------------
# Print compact table of time indices and yr/mn for each phase

msg = []
mlen = 0
for p in range(4):
  pcnt = 0
  #print(f'  Phase: {p}')
  tmsg = f'    Phase {p+1}' 
  tmsg = tmsg + ' '*(4+13-len(tmsg))
  if len(msg)==0:
    msg.append(tmsg); pcnt+=1
  else:
    msg[0] = msg[0]+tmsg
  for t,ph in enumerate(phc):
    if ph==phc_list[p]: 
      #phs_1_list.append(t):
      if mn[t] in [1,4,7,10]:
        # print(f'    {t:4d}  {yr[t]:4d}  {mn[t]:2d}')
        tmsg = f'    {t:04d}  {yr[t]:4d}-{mn[t]:02d}'
        pcnt+=1
        if len(msg)<(pcnt+1):
          msg.append(tmsg)
        else:
          msg[pcnt] = msg[pcnt] + tmsg
for m in msg: print(m)

# exit()

#-------------------------------------------------------------------------------
# build list of time indices for each phase
phs_1_list = [] # W
phs_2_list = [] # TE
phs_3_list = [] # E
phs_4_list = [] # TW
for t,p in enumerate(phc):
  if p==phc_list[0]: phs_1_list.append(t)
  if p==phc_list[1]: phs_2_list.append(t)
  if p==phc_list[2]: phs_3_list.append(t)
  if p==phc_list[3]: phs_4_list.append(t)


#-------------------------------------------------------------------------------
# def calc_min_diff():
#   min_diff_list = []
#   for p in range(4):
#     min_diff = int(1e5)
#     for i in range(4):
#       for ii in range(4):
#         if i!=ii:
#           diff = np.absolute( smp[p,i] - smp[p,ii] )
#           if diff < min_diff: min_diff = diff
#     # print(f'p: {p}  min_diff: {min_diff}')
#     min_diff_list.append(min_diff)
#   return min_diff_list
#-------------------------------------------------------------------------------
# # Find 4x "random" samples of each phase
#smp = np.array([[None]*4]*4)
#random.seed(12345678)
# smp[0,:] = random.sample(phs_1_list,4)
# smp[1,:] = random.sample(phs_2_list,4)
# smp[2,:] = random.sample(phs_3_list,4)
# smp[3,:] = random.sample(phs_4_list,4)
#-------------------------------------------------------------------------------
# # Find 4x "random" samples of each phase - alt sampling to ensure things are spaced out
# smp = np.array([[None]*4]*4)
# for t,p in enumerate(phc):
#   p==phs_1:
#   p==phs_2:
#   p==phs_3:
#   p==phs_4:
# for i in range(4):
#   t1 = 10*i
#   t2 = 10*(i+1)
#   print(phs_1_list[t1:t2])
#   print( random.sample( phs_1_list[t1:t2] ,1)[0] )
#   # smp[0,i] = random.sample( phs_1_list[t1:t2] ,1)[0]
#   # smp[1,i] = random.sample( phs_2_list[t1:t2] ,1)[0]
#   # smp[2,i] = random.sample( phs_3_list[t1:t2] ,1)[0]
#   # smp[3,i] = random.sample( phs_4_list[t1:t2] ,1)[0]
#-------------------------------------------------------------------------------

smp = np.array( [[None]*4]*4 )

# # v1
# smp[0,:] = np.array( [ 48, 171, 282, 525 ] )
# smp[1,:] = np.array( [ 93, 198, 255, 360 ] )
# smp[2,:] = np.array( [ 36,  99, 426, 513 ] )
# smp[3,:] = np.array( [ 69, 186, 327, 408 ] )

# v2
smp[0,:] = np.array( [ 48, 171, 304, 525 ] )
smp[1,:] = np.array( [ 82, 147, 252, 390 ] )
smp[2,:] = np.array( [ 60, 267, 318, 514 ] )
smp[3,:] = np.array( [102, 189, 348, 435 ] )

# smp[0,:] = np.array( [102, 0, 0, 0 ] )
# smp[1,:] = np.array( [0, 213, 0, 0 ] )
# smp[2,:] = np.array( [0, 0, 408, 0 ] )
# smp[3,:] = np.array( [0, 0, 0, 436 ] )


#-------------------------------------------------------------------------------
# print sample dates

# print('Samples:')
# for p in range(4):
#   print(); print(f'  Phase {p+1}:')
#   for i in range(4):
#     t = int(smp[p,i])
#     print(f'    {t:4d}  {yr[t]}-{mn[t]:02d}')

print()
for p in range(4):
  print(); print(f'Phase {(p+1)}  (pi*{((p+1)*2-1)}/4)')
  for i in range(4):
    t = int(smp[p,i])
    print(f'{yr[t]}-{mn[t]:02d}')

#-------------------------------------------------------------------------------
# Create plot
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*2

res = hs.res_xy()
res.vpHeightF               = 0.1
res.xyLineThicknessF        = 8
res.tmYLLabelFontHeightF    = 0.008
res.tmXBLabelFontHeightF    = 0.008
res.tiXAxisFontHeightF      = 0.008
res.tiYAxisFontHeightF      = 0.008
res.tmXBMinorOn             = True
res.tmXBMinorPerMajor       = 10
res.tmXBMinorOutwardLengthF = 0.005
res.tmXBMajorOutwardLengthF = 0.005
res.tiXAxisString           = 'Year'

res.trXMinF = time_coord[0]
res.trXMaxF = time_coord[-1]

t2 = -1 # 12*12

res.xyLineColor = 'green'
plot[0] = ngl.xy(wks, time_coord[:t2], eof1[:t2], res)
res.xyLineColor = 'magenta'
ngl.overlay( plot[0], ngl.xy(wks, time_coord[:t2], eof2[:t2], res) )

res.trYMinF = -1*np.pi - np.pi/10
res.trYMaxF =    np.pi + np.pi/10

res.xyLineColor = 'gray'
plot[1] = ngl.xy(wks, time_coord[:t2], phc[:t2], res)
res.xyLineColor = 'black'
ngl.overlay( plot[1], ngl.xy(wks, time_coord[:t2], phs[:t2], res) )

hs.set_subtitles(wks, plot[0], '', 'QBO EOF 1/2', '', font_height=0.015)
hs.set_subtitles(wks, plot[1], '', 'QBO Phase',   '', font_height=0.015)

#-------------------------------------------------------------------------------
# Add horizontal line at zero
xx = np.array([-1e12,1e12])
lres = hs.res_xy()
lres.xyLineColor = 'black'
lres.xyLineThicknessF = 1
lres.xyDashPattern = 0
for p in range(len(plot)):
  ngl.overlay( plot[p], ngl.xy(wks, xx, xx*0, lres))

#-------------------------------------------------------------------------------
# add sample indicators
mres = hs.res_xy()
mres.xyMarkLineMode = 'Markers'
mres.xyMarkerSizeF = 0.008
mres.xyMarker = 16
mres.xyLineThicknessF = 8

# smp_list = smp.tolist()
# # mres.xyMarkerColor = 'green'
# ngl.overlay( plot[0], ngl.xy(wks, time_coord[smp_list], eof1[smp_list], mres) )
# # mres.xyMarkerColor = 'magenta'
# ngl.overlay( plot[0], ngl.xy(wks, time_coord[smp_list], eof2[smp_list], mres) )
# mres.xyMarkerColor = 'cyan'
# ngl.overlay( plot[1], ngl.xy(wks, time_coord[smp_list], phs[smp_list], mres) )

sres = hs.res_xy()
sres.xyLineThicknessF = 1
# pclr = ['black','black','black','black']
# pclr = ['cyan','cyan','cyan','cyan']
# pclr = ['red','green','blue','purple']
pclr = ['blue','purple','red','green']
for p in range(4):
  sres.xyLineColor   = pclr[p]
  mres.xyMarkerColor = pclr[p]
  # Markers
  smp_list = smp[p,:].tolist()
  ngl.overlay( plot[1], ngl.xy(wks, time_coord[smp_list], phs[smp_list], mres) )
  for i in range(4):
    # Horizontal line
    sres.xyDashPattern = 0
    xx = np.array([-1e12,1e12])
    yy = np.array([1,1])*phs[smp[p,i]]
    ngl.overlay( plot[1], ngl.xy(wks, xx, yy, sres) )

    # Vertical lines
    sres.xyDashPattern = 2
    xx = np.array([1,1])*time_coord[smp[p,i]]
    yy = np.array([-1e12,1e12])
    ngl.overlay( plot[1], ngl.xy(wks, xx, yy, sres) )



#-------------------------------------------------------------------------------
# layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

# pnl_res = hs.setres_panel()
# pnl_res.nglPanelYWhiteSpacePercent = 5
# ngl.panel(wks,plot,[len(plot),1],pnl_res)
# ngl.end()

ngl.draw(plot[1])
ngl.frame(wks)

hc.trim_png(fig_file)
#-------------------------------------------------------------------------------

