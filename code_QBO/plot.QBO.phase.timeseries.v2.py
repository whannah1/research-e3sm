import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import random
#-------------------------------------------------------------------------------

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.phase.timeseries.v2','png'

#-------------------------------------------------------------------------------
# Read file 

file = open(os.getenv('HOME')+'/Research/E3SM/QBO_EOF_coeff.txt','r')
lines = file.read().split('\n')
file.close()

phc_list = []
phc_list.append( np.pi*1./4. )
phc_list.append( np.pi*3./4. )
phc_list.append( np.pi*5./4. - 2*np.pi )
phc_list.append( np.pi*7./4. - 2*np.pi )

mn = []
yr = []
e1 = []
e2 = []
phc = []
for l in lines[8:]:
  # print(l.split())
  # exit()
  if len(l)>0:
    mn.append(  int(l.split()[0]))
    yr.append(  int(l.split()[1]))
    e1.append(float(l.split()[2]))
    e2.append(float(l.split()[3]))
    # ph.append(l.split()[-1])
    if l.split()[-1]=='W' : phc.append( phc_list[0] )
    if l.split()[-1]=='TE': phc.append( phc_list[1] )
    if l.split()[-1]=='E' : phc.append( phc_list[2] )
    if l.split()[-1]=='TW': phc.append( phc_list[3] )

time_coord = np.array(yr) + (np.array(mn)-1)/12

#-------------------------------------------------------------------------------
# Calculate QBO phase

eof1 = np.array(e1)
eof2 = np.array(e2)

phs = np.arctan2( eof2, eof1 )

eof1 = eof1 / np.std(eof1)
eof2 = eof2 / np.std(eof2)

#-------------------------------------------------------------------------------

smp = np.array( [[None]*4]*4 )

# # v1
# smp[0,:] = np.array( [ 48, 171, 282, 525 ] )
# smp[1,:] = np.array( [ 93, 198, 255, 360 ] )
# smp[2,:] = np.array( [ 36,  99, 426, 513 ] )
# smp[3,:] = np.array( [ 69, 186, 327, 408 ] )

# v2
smp[0,:] = np.array( [ 48, 171, 304, 525 ] )
smp[1,:] = np.array( [ 82, 147, 252, 390 ] )
smp[2,:] = np.array( [ 60, 267, 318, 514 ] )
smp[3,:] = np.array( [102, 189, 348, 435 ] )

# smp[0,:] = np.array( [102, 0, 0, 0 ] )
# smp[1,:] = np.array( [0, 213, 0, 0 ] )
# smp[2,:] = np.array( [0, 0, 408, 0 ] )
# smp[3,:] = np.array( [0, 0, 0, 436 ] )

#-------------------------------------------------------------------------------
# print sample dates

# print('Samples:')
# for p in range(4):
#   print(); print(f'  Phase {p+1}:')
#   for i in range(4):
#     t = int(smp[p,i])
#     print(f'    {t:4d}  {yr[t]}-{mn[t]:02d}')

print()
for p in range(4):
  print()
  print(f'Phase {(p+1)}  (pi*{((p+1)*2-1)}/4)')
  print()
  for i in range(4):
    t = int(smp[p,i])
    print(f'{yr[t]}-{mn[t]:02d}')

#-------------------------------------------------------------------------------
# Create plot
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*1

res = hs.res_xy()
res.xyLineThicknessF        = 4
res.tmYLLabelFontHeightF    = 0.008
res.tmXBLabelFontHeightF    = 0.008
res.tiXAxisFontHeightF      = 0.008
res.tiYAxisFontHeightF      = 0.008
res.tiXAxisString           = 'EOF1'
res.tiYAxisString           = 'EOF2'

# res.trXMinF = eof1[0]
# res.trXMaxF = time_coord[-1]

t2 = -1 # 12*12

# res.xyLineColor = 'green'
plot[0] = ngl.xy(wks, eof1[:t2], eof2[:t2], res)


# res.trYMinF = -1*np.pi - np.pi/10
# res.trYMaxF =    np.pi + np.pi/10

# res.xyLineColor = 'gray'
# plot[1] = ngl.xy(wks, time_coord[:t2], phc[:t2], res)
# res.xyLineColor = 'black'
# ngl.overlay( plot[1], ngl.xy(wks, time_coord[:t2], phs[:t2], res) )

# hs.set_subtitles(wks, plot[0], '', 'QBO EOF 1/2', '', font_height=0.015)
hs.set_subtitles(wks, plot[0], '', 'QBO Phase',   '', font_height=0.015)

#-------------------------------------------------------------------------------
# Add lines
xx = np.array([-1e3,1e3])
lres = hs.res_xy()
lres.xyLineColor = 'gray'
lres.xyLineThicknessF = 2
lres.xyDashPattern = 0
# horz and vert lines
ngl.overlay( plot[0], ngl.xy(wks, xx, xx*0, lres))
ngl.overlay( plot[0], ngl.xy(wks, xx*0, xx, lres))
# diagonal lines
ngl.overlay( plot[0], ngl.xy(wks, xx, xx,   lres))
ngl.overlay( plot[0], ngl.xy(wks, xx, xx*-1,lres))

#-------------------------------------------------------------------------------
# add sample indicators
mres = hs.res_xy()
mres.xyMarkLineMode = 'Markers'
mres.xyMarkerSizeF = 0.012
mres.xyMarker = 16
mres.xyLineThicknessF = 8

pclr = ['blue','purple','red','green']
for p in range(4):
  mres.xyMarkerColor = pclr[p]
  smp_list = smp[p,:].tolist()
  ngl.overlay( plot[0], ngl.xy(wks, eof1[smp_list], eof2[smp_list], mres) )

#-------------------------------------------------------------------------------
# layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

# pnl_res = hs.setres_panel()
# pnl_res.nglPanelYWhiteSpacePercent = 5
# ngl.panel(wks,plot,[len(plot),1],pnl_res)
# ngl.end()

ngl.draw(plot[0])
ngl.frame(wks)

hc.trim_png(fig_file)
#-------------------------------------------------------------------------------

