import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
import scipy, numba
#-------------------------------------------------------------------------------
# based on E3SM diagnostics package:
# https://github.com/E3SM-Project/e3sm_diags/blob/main/e3sm_diags/driver/qbo_driver.py
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

### Runs for QBO paper
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72.01',       n='E3SM control')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72-nsu40.01', n='E3SM L72 smoothed')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L80-rsu40.01', n='E3SM L80 refined')

### v3 candidate w/ smoothed L72
# add_case('20230127.amip.v3atm.FourthSmoke.chrysalis',                   n='v3 '     ,         c='black',p='/lcrc/group/e3sm/ac.golaz/E3SMv3_dev'  ,s='archive/atm/hist')
# add_case('20230214.amip.v3atm.FourthSmoke.chrysalis.L72-nsu40',         n='v3 L72sm',         c='red',  p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')
# add_case('20230228.amip.v3atm.FourthSmoke.chrysalis.errgw-02',          n='v3 effgw-02',      c='green',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='run')
# add_case('20230228.amip.v3atm.FourthSmoke.chrysalis.L72-nsu40.errgw-02',n='v3 L72sm+effgw-02',c='blue', p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='run')

# /lcrc/group/e3sm/ac.golaz/E3SMv3_dev/20230127.amip.v3atm.FourthSmoke.chrysalis
# /lcrc/group/e3sm/ac.whannah/E3SMv3_dev/20230214.amip.v3atm.FourthSmoke.chrysalis.L72-nsu40/
# /lcrc/group/e3sm/ac.whannah/E3SMv3_dev/20230228.amip.v3atm.FourthSmoke.chrysalis.errgw-02
# /lcrc/group/e3sm/ac.whannah/E3SMv3_dev/20230228.amip.v3atm.FourthSmoke.chrysalis.L72-nsu40.errgw-02

### L80 ensemble
tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # defaults
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_10.HD_1.50',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_20.HD_1.00',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_20.HD_1.50',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_25.HD_0.25',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.50',n='',p=tmp_path,s=tmp_sub)
add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_15.HD_0.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.09.CF_20.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.20.CF_20.HD_1.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.05.CF_25.HD_0.50',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.80.CF_01.HD_1.15',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.90.CF_20.HD_0.25',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.59.CF_41.HD_0.55',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.01.CF_01.HD_0.70',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.40.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.70.CF_25.HD_0.90',n='',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.60.CF_07.HD_1.35',n='',p=tmp_path,s=tmp_sub)
### add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_30.HD_0.63',n='',p=tmp_path,s=tmp_sub) # keeps crashing

for c in range(len(case)):
   if 'E3SM.2023-SCIDAC.' in case[c]:
      ef_idx = case[c].find('EF_')
      cf_idx = case[c].find('CF_')
      hd_idx = case[c].find('HD_')
      ef = float(case[c][ef_idx+3:ef_idx+3+4])
      cf =   int(case[c][cf_idx+3:cf_idx+3+2])
      hd = float(case[c][hd_idx+3:hd_idx+3+4])
      case_name[c] = f'{case_name[c]}  EF={ef} CF={cf} HD={hd}'

clr = None # clear color list to trigger automatic color list generation


# lev = np.array([   1.,    2.,    3.,    5.,    7.,   10.,   20.,   30.,   50.,   70.,  100.,  125.,  150.,])
lev = [20,50]


bin_min,bin_max,bin_spc = -40,40,4

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

var = ['U']

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_QBO/QBO.phase-histogram.v1'

lat1,lat2 = -5,5
# lat1,lat2 = -10,10

htype,first_file,num_files = 'h0',0,12*10

print_stats = True

var_x_case = False

use_common_label_bar = True

num_plot_col = 1

recalculate_timeseries = False

add_obs = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case,num_lev = len(var),len(case),len(lev)

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*num_lev

if clr is None: clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int).tolist()

res = hs.res_xy()
res.vpHeightF = 0.3
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
# res.tmXBAutoPrecision = False
# res.tmXBPrecision = 2

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
@numba.jit(nopython=True)
def calculate_obs_area(lon,lat,lon_bnds,lat_bnds):
   re = 6.37122e06  # radius of earth
   nlat,nlon = len(lat),len(lon)
   area = np.empty((nlat,nlon),np.float64)
   for j in range(nlat):
      for i in range(nlon):
         dlon = np.absolute( lon_bnds[j,1] - lon_bnds[j,0] )
         dlat = np.absolute( lat_bnds[j,1] - lat_bnds[j,0] )
         dx = re*dlon*np.pi/180.
         dy = re*dlat*np.pi/180.
         area[j,i] = dx*dy
   return area
#---------------------------------------------------------------------------------------------------
def calculate_histogram(data,bin_min,bin_max,bin_spc):
   nbin = np.round( ( bin_max - bin_min + bin_spc )/bin_spc ).astype(np.int)
   bins = np.linspace(bin_min,bin_max,nbin)
   bin_coord = xr.DataArray( bins )
   shape,dims,coord = (nbin,),'bin',[('bin', bin_coord.values)]
   bin_cnt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
   val_chk = np.isfinite(data.values)
   # Loop through bins
   for b in range(nbin):
      bin_bot = bin_min - bin_spc/2. + bin_spc*(b  )
      bin_top = bin_min - bin_spc/2. + bin_spc*(b+1)
      condition = xr.DataArray( np.full(data.shape,False,dtype=bool), coords=data.coords )
      condition.values = ( np.where(val_chk,data.values,bin_bot-1e3) >=bin_bot ) \
                        &( np.where(val_chk,data.values,bin_bot-1e3)  <bin_top )
      if np.sum(condition.values)>0 :
         bin_cnt[b] = np.sum( condition )
   bin_ds = xr.Dataset()
   bin_ds['cnt'] = bin_cnt
   bin_ds.coords['bins'] = bin_coord
   return bin_ds
#---------------------------------------------------------------------------------------------------
v=0
# for v in range(num_var):
for k in range(num_lev):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   tvar = var[v]
   area_name = 'area'
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   # data_list,lev_list = [],[]
   # period_list, power_list = [],[]
   bin_list = []
   cnt_list = []
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   if tvar=='U' and add_obs:
      ovar = 'ua'
      # obs_data_file = '/lcrc/group/e3sm/diagnostics/observations/Atm/time-series/ERA-Interim/ua_197901_201612.nc'
      obs_data_file = '/global/cfs/cdirs/e3sm/diagnostics/observations/Atm/time-series/ERA-Interim/ua_197901_201612.nc'
      ds = xr.open_dataset(obs_data_file)
      area = calculate_obs_area(ds['lon'].values,ds['lat'].values,ds['lon_bnds'].values,ds['lat_bnds'].values)
      area = xr.DataArray( area, coords=[ds['lat'],ds['lon']] )
      data = ds[ovar]
      data = data.sel(plev=lev[k]*1e2)#.isel(plev=0)
      data = data.sel(lat=slice(lat1,lat2))
      area = area.sel(lat=slice(lat1,lat2))
      data_avg = (data*area).sum(dim=('lon','lat')) / area.sum(dim=('lon','lat'))
      #----------------------------------------------------------------------
      hc.print_stat(data_avg,name=var[v]+' (ERAi)',stat='naxsh',indent='    ',compact=True)
      #----------------------------------------------------------------------
      # create histogram
      bin_ds = calculate_histogram(data_avg,bin_min,bin_max,bin_spc)
      bin_list.append(bin_ds['bins'].values)
      cnt_list.append(bin_ds['cnt'].values)
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for c in range(num_case):
      tmp_file = os.getenv('HOME')+f'/Research/E3SM/data_temp/QBO.phase-histogram.v1.{case[c]}.{var[v]}.lev_{lev[k]}.lat1_{lat1}.lat2_{lat2}.nc'

      print()
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      print('    time series file: '+tmp_file)

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp, time_freq=None )
      if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
      if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

      # avoid creating large chunks
      with dask.config.set(**{'array.slicing.split_large_chunks': True}):  
         if recalculate_timeseries:
            data = case_obj.load_data(tvar,    
                                      component=get_comp(case[c]),
                                      htype=htype,
                                      first_file=first_file,
                                      num_files=num_files,
                                      lev=np.array([lev[k]]))
            area = case_obj.load_data(area_name,
                                      component=get_comp(case[c]),
                                      htype=htype,
                                      first_file=first_file,
                                      num_files=num_files).astype(np.double)

            # hc.print_time_length(data.time,indent=' '*4)

            data_avg = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
            
            ### print stats after time averaging
            # if print_stats: hc.print_stat(data_avg,name=var[v],stat='naxsh',indent='    ',compact=True)

            ds_out = xr.Dataset( coords=data_avg.coords )
            ds_out[var[v]] = data_avg
            ds_out.to_netcdf(path=tmp_file,mode='w')
         #----------------------------------------------------------------------
         else:
            tmp_ds = xr.open_dataset( tmp_file, use_cftime=True  )
            data_avg = tmp_ds[var[v]]

      if 'lev' in data_avg.dims: data_avg = data_avg.isel(lev=0)
      #----------------------------------------------------------------------
      # create histogram
      bin_ds = calculate_histogram(data_avg,bin_min,bin_max,bin_spc)
      bin_list.append(bin_ds['bins'].values)
      cnt_list.append(bin_ds['cnt'].values)
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   # for c in range(num_case):

   pct_list = []
   for c in range( (num_case+1) if add_obs else num_case ):
      pct_list.append( cnt_list[c] / np.sum(cnt_list[c]) )

   tres.trXMinF = np.min([np.nanmin(d) for d in bin_list])
   tres.trXMaxF = np.max([np.nanmax(d) for d in bin_list])
   tres.trYMinF = 0
   tres.trYMaxF = np.max([np.nanmax(d) for d in pct_list])

   # tres.tiXAxisString = 'Period (months)'
   # tres.tiYAxisString = 'Amplitude'

   for c in range( (num_case+1) if add_obs else num_case ):


      if add_obs:
         if c==0:
            tres.xyLineColor,tres.xyDashPattern = 'gray',0
            tres.xyLineThicknessF = 16
         else:
            tres.xyLineColor   = clr[c-1]
            tres.xyDashPattern = dsh[c-1]
            tres.xyLineThicknessF = 8
      else:
         tres.xyLineColor   = clr[c]
         tres.xyDashPattern = dsh[c]

      tplot = ngl.xy(wks, bin_list[c], pct_list[c], tres)

      ip = k#v
      if c==0 :
         plot[ip] = tplot
      else:
         ngl.overlay(plot[ip],tplot)

      #-------------------------------------------------------------------------
      # Set strings at top of plot
      #-------------------------------------------------------------------------
      # hs.set_subtitles(wks, plot[len(plot)-1], '', f'{int(lev[k])}mb', '', font_height=0.015)
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   # create output file for further testing of QOI calculations
   all_file = os.getenv('HOME')+f'/Research/E3SM/scidac_2023/QBO.phase-histogram.v1.lev_{lev[k]}.lat1_{lat1}.lat2_{lat2}.nc'
   all_case_list = copy.deepcopy(case)
   all_case_list.insert(0,'ERA-interim')
   ds_all = xr.Dataset()
   ds_all['cnt'] = xr.DataArray( cnt_list, dims=('case','bin') )
   ds_all.coords['bin'] = bin_list[0]
   ds_all['case_name'] = all_case_list
   ds_all.to_netcdf(path=all_file,mode='w')

   print()
   print(' data file for all cases: '+hc.tcolor.CYAN+all_file+hc.tcolor.ENDC)
   print()

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
# layout = [num_var,num_case] if var_x_case else [num_case,num_var]

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01

# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

ngl.panel(wks,plot[0:len(plot)],layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
