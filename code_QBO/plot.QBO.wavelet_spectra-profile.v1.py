import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, dask, glob
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
import scipy, pywt
from statsmodels.tsa.arima.model import ARIMA
import QBO_diagnostic_methods as QBO_methods
#-------------------------------------------------------------------------------
# based on E3SM diagnostics package:
# https://github.com/E3SM-Project/e3sm_diags/blob/main/e3sm_diags/driver/qbo_driver.py
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
#-------------------------------------------------------------------------------


add_case('ERA5', n='ERA5')

tmp_path,tmp_sub = '/global/cfs/cdirs/e3smdata/simulations/','archive/atm/hist'
add_case('v2.LR.amip_0101', n='E3SMv2 AMIP', d=1, c='red',   p=tmp_path,s=tmp_sub) 

tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/e3smv3_amip','archive/atm/hist'
add_case('v3.LR.amip_0101', n='E3SMv3 AMIP', d=0, c='red',   p=tmp_path,s=tmp_sub)

#-------------------------------------------------------------------------------

var = ['U']
# pow_spec_lev = 20.
pow_spec_lev = np.array([   1.,    2.,    3.,    5.,    7.,   10.,   20.,   30.,   50.,   70.,  100. ])
num_lev = len(pow_spec_lev)

fig_file,fig_type = 'figs_QBO/QBO.wavelet_spectra-profile.v1','png'
tmp_file_head     = 'data_temp/QBO.wavelet_spectra-profile.v1'

lat1,lat2 = -5,5

yr1,yr2=2000,2019
htype = 'h0'

longest_period = 5*12
period = np.arange(1, longest_period + 1)
num_period = len(period)


print_stats = True
var_x_case = False
use_common_label_bar = True

num_plot_col = len(case)

recalculate = False

# year_start = 1950+first_file/12
# year_end   = 1950+first_file/12+num_files/12+1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

plot = [None]*num_var*num_case

res = hs.res_contour_fill()
res.vpHeightF              = 0.4
res.tmYLLabelFontHeightF   = 0.015
res.tmXBLabelFontHeightF   = 0.015
res.tiXAxisFontHeightF     = 0.015
res.tiYAxisFontHeightF     = 0.015

res.tiYAxisString = 'Pressure [mb]'
res.tiXAxisString = 'Period [months]'

if use_common_label_bar:
   res.lbLabelBarOn = False

Y_tm_vals = [5,10,50,100,200]
res.trYReverse    = True
res.tmYLMode      = 'Explicit'
res.tmYLValues    = Y_tm_vals
res.tmYLLabels    = Y_tm_vals

X_tm_log = np.array([12,24,28,32,36,48,60])
# res.xyXStyle      = 'Log'
res.tmXBMode      = 'Explicit'
res.tmXBValues    = X_tm_log
res.tmXBLabels    = X_tm_log

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def deseason(xraw):
    # Calculates the deseasonalized data
    months_per_year = 12
    # Create array to hold climatological values and deseasonalized data
    # Create months_per_year x 1 array of zeros
    xclim = np.zeros((months_per_year, 1))
    # Create array with same shape as xraw
    x_deseasoned = np.zeros(xraw.shape)
    # Iterate through all 12 months.
    for month in np.arange(months_per_year):
        # `xraw[month::12]` will return the data for this month every year (12 months)
        # (i.e., from month until the end of xraw, get every 12th month)
        # Get the mean of this month, using data from every year, ignoring NaNs
        xclim[month] = np.nanmean(xraw[month::months_per_year])
    num_years = int(np.floor(len(x_deseasoned) / months_per_year))
    # Iterate through all years in x_deseasoned (same number as in xraw)
    for year in np.arange(num_years):
        year_index = year * months_per_year
        # Iterate through all months of the year
        for month in np.arange(months_per_year):
            month_index = year_index + month
            # Subtract the month's mean over num_years from xraw's data for this month in this year
            # i.e., get the difference between this month's value and it's "usual" value
            x_deseasoned[month_index] = xraw[month_index] - xclim[month]
    return x_deseasoned
#---------------------------------------------------------------------------------------------------
def get_psd_from_wavelet_scipy(data):
   deg = 6
   period = np.arange(1, longest_period + 1)
   freq = 1 / period
   widths = deg / (2 * np.pi * freq)
   cwtmatr = scipy.signal.cwt(data, scipy.signal.morlet2, widths=widths, w=deg)
   psd = np.mean( np.square( np.abs(cwtmatr) ), axis=1)
   return (period, psd)
#---------------------------------------------------------------------------------------------------
def get_psd_from_wavelet_pywt(data):
   deg = 6
   period = np.arange(1, longest_period + 1)
   widths = deg / ( 2 * np.pi / period )
   [cfs, freq] = pywt.cwt(data, scales=widths, wavelet='cmor1.5-1.0')
   psd = np.mean( np.square( np.abs(cfs) ), axis=1)
   period = 1 / freq
   return (period, psd)
#---------------------------------------------------------------------------------------------------
def get_alpha_sigma2(data_in):
   ## Fit AR1 model to estimate the autocorrelation (alpha) and variance (sigma2)
   mod = ARIMA( data_in, order=(1,0,0) )
   res = mod.fit()
   return (res.params[1],res.params[2])
#---------------------------------------------------------------------------------------------------
def get_tmp_file(case,var,yr1,yr2):
   return f'{tmp_file_head}.{case}.{var}.{yr1}-{yr2}.nc'
#---------------------------------------------------------------------------------------------------
def mask_data(ds,data,lat_name='lat',lon_name='lon'):
   global lat1,lat2
   
   if 'ncol' in data.dims:
      tmp_data = np.ones([len(ds['ncol'])],dtype=bool)
      tmp_dims,tmp_coords = ('ncol'),{'ncol':data['ncol']}
   else:
      tmp_data = np.ones([len(ds[lat_name]),len(ds[lon_name])],dtype=bool)
      tmp_dims,tmp_coords = (lat_name,lon_name),{lat_name:ds[lat_name],lon_name:ds[lon_name]}
   mask = xr.DataArray( tmp_data, coords=tmp_coords, dims=tmp_dims )
   mask = mask & (ds[lat_name]>= lat1) & (ds[lat_name]<= lat2)
   data = data.where( mask.compute(), drop=True)
   return data
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print(f'  var: {hc.tclr.MAGENTA}{var[v]}{hc.tclr.END}')
   tvar = var[v]
   area_name = 'area'
   #----------------------------------------------------------------------------
   # read the data
   data_list,lev_list = [],[]
   wav_power_list, wav_period_list = [],[]
   fft_power_list, fft_period_list = [],[]
   variance_list, alpha_list, sigma2_list = [],[],[]
   for c in range(num_case):
      tmp_file = get_tmp_file(case[c],var[v],yr1,yr2)
      print(f'    case: {hc.tclr.GREEN}{case[c]}{hc.tclr.END}  =>  {tmp_file}')
      if recalculate:

         if case[c]=='ERA5':

            lat_name,lon_name = 'lat','lon'
            xy_dims = (lon_name,lat_name)
            obs_root = '/global/cfs/cdirs/e3sm/diagnostics/observations/Atm'
            input_file_name = f'{obs_root}/time-series/ERA5/ua_197901_201912.nc'
            ds = xr.open_dataset( input_file_name )

            ds = ds.isel(time=slice( (12*(yr1-1979)),(12*(yr2+1-1979)), ))

            area = QBO_methods.calculate_area(ds['lon'].values,ds['lat'].values,ds['lon_bnds'].values,ds['lat_bnds'].values)
            area = xr.DataArray( area, coords=[ds['lat'],ds['lon']] )  
            data = ds['ua']
            data = data.rename({'plev':'lev'})
            data['lev'] = data['lev']/1e2
            data = data.sel(lev=pow_spec_lev)

            data = mask_data(ds,data)
            area = mask_data(ds,area)
            data_avg = ( (data*area).sum(dim=xy_dims) / area.sum(dim=xy_dims) )

         else:
            data_dir_tmp,data_sub_tmp = None, None
            if case_dir[c] is not None: data_dir_tmp = case_dir[c]
            if case_sub[c] is not None: data_sub_tmp = case_sub[c]
            case_obj = he.Case( name=case[c], atm_comp='eam', 
                                data_dir=data_dir_tmp, data_sub=data_sub_tmp, time_freq=None )
            if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
            if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2
            #-------------------------------------------------------------------
            file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*.eam.{htype}.*'
            file_list = sorted(glob.glob(file_path))
            #-------------------------------------------------------------------
            # subset files that fall within [yr1:yr2]
            file_list_all = file_list ; file_list = []
            for f in range(len(file_list_all)):
               yr = int(file_list_all[f][-10:-10+4])
               if yr>=yr1 and yr<=yr2: file_list.append(file_list_all[f])
            #-------------------------------------------------------------------
            ds = xr.open_mfdataset( file_list )
            ds = ds.where( ds['time.year']>=yr1, drop=True)
            ds = ds.where( ds['time.year']<=yr2, drop=True)
            #-------------------------------------------------------------------
            area = ds[area_name]
            data = he.interpolate_to_pressure(ds,data_mlev=ds[tvar],
                                              lev=pow_spec_lev,
                                              ds_ps=ds,ps_var='PS',
                                              interp_type=2,extrap_flag=True)#.isel(lev=0)
            data = mask_data(ds,data)
            area = mask_data(ds,area)
            data_avg = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
         #----------------------------------------------------------------------
         # convert to anomalies
         data_avg = data_avg - data_avg.mean(dim='time')
         # detrend in time
         fit = xr.polyval(data_avg['time'], data_avg.polyfit(dim='time', deg=1).polyfit_coefficients)
         data_avg = data_avg - fit
         #-------------------------------------------------------------------------
         wavelet_spec = np.full([num_lev,num_period],np.nan)
         for k in range(num_lev):
            ( period, wavelet_spec[k,:] ) = get_psd_from_wavelet_pywt(data_avg.isel(lev=k).values)
         #----------------------------------------------------------------------
         ds_out = xr.Dataset( coords=data_avg.coords )
         ds_out['period']       = xr.DataArray(period,      coords={'period':period})
         ds_out['wavelet_spec'] = xr.DataArray(wavelet_spec,coords={'lev':pow_spec_lev,'period':period,})
         ds_out.to_netcdf(path=tmp_file,mode='w')
      else:
         tmp_ds = xr.open_dataset( tmp_file, use_cftime=True  )
         period       = tmp_ds['period'].values
         wavelet_spec = tmp_ds['wavelet_spec'].values
      #-------------------------------------------------------------------------
      wav_power_list.append( np.sqrt(wavelet_spec) )
      wav_period_list.append( period )

   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
   # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )

   data_min = np.min([np.nanmin(d) for d in wav_power_list])
   data_max = np.max([np.nanmax(d) for d in wav_power_list])

   tres.cnLevelSelectionMode = 'ExplicitLevels'
   tres.cnLevels = np.arange(6,54+6,6)

   for c in range(num_case):

      tres.sfYArray = pow_spec_lev
      tres.sfXArray = period

      ip = c*num_var + v

      # X_tm_log = np.array([12,24,28,32,36,48,60])
      X_tm_val = np.array([12,24,28,36,48,60])
      X_tm_txt = []
      for x in X_tm_val:
         if x==28:
            X_tm_txt.append(f'~Z110~~F22~{x}~F21~~Z100~')
         else:
            # X_tm_txt.append(f'~F21~{x}~F21~')
            X_tm_txt.append(f'{x}')
      tres.tmXBMode      = 'Explicit'
      tres.tmXBValues    = X_tm_val
      tres.tmXBLabels    = X_tm_txt
      # tres.tmXBLabelFontColor = 'black'
      # tres.tmXBValues    = np.array([12,24,36,48,60])
      # tres.tmXBLabels    = ['12','24','~F22~36~F21~','48','60']

      plot[ip] = ngl.contour(wks, wav_power_list[c], tres)

      # X_tm_log = np.array([28,32,36])
      # X_tm_log = np.array([28,28])
      # tres.tmXBMode      = 'Explicit'
      # tres.tmXBValues    = X_tm_log
      # tres.tmXBLabels    = X_tm_log
      # tres.tmXBLabelFontColor = 'blue'

      

      # ngl.overlay( plot[ip], ngl.contour(wks, wav_power_list[c], tres) )

      hs.set_subtitles(wks, plot[ip], case_name[c], '', '', font_height=0.01)

      #-------------------------------------------------------------------------
      # add lines for dominant periods
      lres = hs.res_xy()
      lres.xyLineColor = 'gray'
      lres.xyLineThicknessF = 2
      lres.xyDashPattern = 2
      
      yy = np.array([-1e3,1e3])
      
      # for f in [32,28,24]:
      for f in [30,26]:
         xx = np.array([1,1]) * f
         ngl.overlay(plot[ip], ngl.xy(wks, xx, yy, lres) )

      # for p in [80,20]:
      for p in [20]:
         ngl.overlay(plot[ip], ngl.xy(wks, np.array([-1,1])*1e3, np.array([1,1])*p, lres) )

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5

if use_common_label_bar: 
   pnl_res.nglPanelLabelBar   = True
   # pnl_res.lbTopMarginF       =  0.2
   # pnl_res.lbBottomMarginF    = -0.2
   pnl_res.lbLeftMarginF      = 0.5+0.5
   pnl_res.lbRightMarginF     = 0.5
   pnl_res.nglPanelLabelBarLabelFontHeightF = 0.008
   pnl_res.lbTitleString      = f'{yr1}-{yr2} Wavelet Power Spectrum'
   pnl_res.lbTitlePosition    = 'bottom'
   pnl_res.lbTitleFontHeightF = 0.01

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
