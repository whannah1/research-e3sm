import os, ngl, copy, xarray as xr, numpy as np, glob
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import cmocean
#---------------------------------------------------------------------------------------------------
# case_dir,case_sub = [],[]
# case,name = [],[]
# clr,dsh,mrk = [],[],[]
# def add_case(case_in,n=None,p=None,s=None,d=0,c='black',m=0):
#    global name,case,case_dir,case_sub,clr,dsh,mrk
#    case.append(case_in); name.append(n)
#    case_dir.append(p); case_sub.append(s)
#    dsh.append(d) ; clr.append(c) ; mrk.append(m)

# ##------------------------------------------------------------------------------
# cscratch = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl'
# gscratch = '/global/cfs/cdirs/m3312/whannah/2022-QBO-TEST'
# pscratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# ##------------------------------------------------------------------------------
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72.01',       n='E3SM control',     c='red'  ,p=gscratch,s='run')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72-nsu40.01', n='E3SM L72 smoothed',c='green',p=gscratch,s='run')
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L80-rsu40.01', n='E3SM L80 refined', c='blue' ,p=gscratch,s='run')

#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72.GW-MOD-0', n='E3SM L72',      d=1,c='black', p=pscratch,s='run')
#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72-nsu40',    n='E3SM L72-nsu40',d=1,c='red',   p=pscratch,s='run')
#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72-rscl',     n='E3SM L72-rscl', d=1,c='purple',p=pscratch,s='run')
#add_case('E3SM.QBO-TEST-02.F2010.ne30pg2.L72-rlim',     n='E3SM L72-rlim', d=1,c='pink',  p=pscratch,s='run')

#---------------------------------------------------------------------------------------------------
gscratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
tmp_date_list = []
tmp_date_list.append('1983-01-01') # phase 1 - pi*1/4
# tmp_date_list.append('1993-04-01')
# tmp_date_list.append('2002-07-01')
# tmp_date_list.append('2022-10-01')
# tmp_date_list.append('1986-10-01') # phase 1 - pi*3/4
# tmp_date_list.append('1995-07-01')
# tmp_date_list.append('2000-04-01')
# tmp_date_list.append('2009-01-01')
# tmp_date_list.append('1982-01-01') # phase 1 - pi*5/4
# tmp_date_list.append('1987-04-01')
# tmp_date_list.append('2014-07-01')
# tmp_date_list.append('2021-10-01')
# tmp_date_list.append('1984-10-01') # phase 1 - pi*7/4
# tmp_date_list.append('1994-07-01')
# tmp_date_list.append('2006-04-01')
# tmp_date_list.append('2013-01-01')
#---------------------------------------------------------------------------------------------------
compset = 'F20TR'
grid    = 'ne30pg2_r05_IcoswISC30E3r5'
ens_id  = '2024-SCIDAC-PCOMP-TEST' # initial test to work out logistical issues
def get_case_name(e,c,h,d): return'.'.join(['E3SM',ens_id,grid,f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}',f'{d}'])
#---------------------------------------------------------------------------------------------------
case = []
gweff_list, cfrac_list, hdpth_list = [],[],[]
start_date_list = []
case_dir,case_sub = [],[]
case,name = [],[]
clr_list,dsh_list,mrk_list = [],[],[]
def add_case(e,c,h,d,n=None,p=None,s=None,dsh=0,clr='black',mrk=0):
   gweff_list.append(e)
   cfrac_list.append(c)
   hdpth_list.append(h)
   case.append( get_case_name(e,c,h,d) )
   name.append(n)
   case_dir.append(p); case_sub.append(s)
   dsh_list.append(dsh)
   clr_list.append(clr)
   mrk_list.append(mrk)
   start_date_list.append(d)
#---------------------------------------------------------------------------------------------------
# # temporary case list for dev
# tdate = '1983-01-01'
# add_case(e=0.35,c=10,h=0.50,d=tdate,dsh=0,clr='red') #  <<< v3 default
# add_case(e=0.12,c=16,h=0.48,d=tdate,dsh=0,clr='blue') # prev surrogate optimum
# tdate = '1993-04-01'
# add_case(e=0.35,c=10,h=0.50,d=tdate,dsh=1,clr='red') #  <<< v3 default
# add_case(e=0.12,c=16,h=0.48,d=tdate,dsh=1,clr='blue') # prev surrogate optimum
#---------------------------------------------------------------------------------------------------
# build list of cases with all dates
for t,date in enumerate(tmp_date_list):
   add_case(e=0.35,c=10,h=0.50,d=date,dsh=0,clr='red') #  <<< v3 default
   add_case(e=0.12,c=16,h=0.48,d=date,dsh=0,clr='blue') # prev surrogate optimum
   # add_case(e=0.09,c=20,h=0.25,d=date) # no QBO at all
   # add_case(e=0.70,c=21,h=0.31,d=date) # QBO is too fast
#---------------------------------------------------------------------------------------------------
var,vname,lev_list,vclr,vdsh = [],[],[],[],[]
def add_var(var_in,name=None,lev=-1,c='black',d=0): 
   var.append(var_in); lev_list.append(lev)
   vclr.append(c); vdsh.append(d)
   vname.append(name)
#---------------------------------------------------------------------------------------------------
add_var('u',        name='U')
add_var('utendepfd',name='EP Flux Divergence') # u-wind tendency due to TEM EP flux divergence
add_var('wf',       name='Total Wave Forcing') # u-wind tendency due to TEM EP flux divergence
# add_var('BUTGWSPEC')
#---------------------------------------------------------------------------------------------------

# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])

num_plot_col = len(var)


fig_file,fig_type = 'figs_QBO/TEM.profile.v1','png'
tmp_file_head = 'data_temp/TEM.profile.v1'

lat1,lat2 = -5,5

# htype,first_file,num_files = 'h2',0,10#365*1
remap_str,search_str = 'remap_90x180','h0.tem.'; first_file,num_files = 0,1#12*20
# remap_str,search_str = 'remap_90x180','h2.tem.'; first_file,num_files = 0,365*20

plot_diff = False

print_stats        = False
print_profile      = False

p_min = 10
p_max = 100

#-------------------------------------------------------------------------------
def lat_mean(da):
  cos_lat_rad = np.cos(da.lat*np.pi/180)
  return (da*cos_lat_rad).mean(dim='lat') / (cos_lat_rad).mean(dim='lat')
#-------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)
num_date = len(tmp_date_list)

# if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_case*num_var)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)
res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
res.tiXAxisFontHeightF  = 0.015
# res.tmYLMinorOn = True

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

res.trYReverse = True
# res.xyYStyle = 'Log'

tend_vars = ['utendepfd','wf']
#---------------------------------------------------------------------------------------------------
def lat_mean(da,lat_name='lat'):
  cos_lat_rad = np.cos(da[lat_name]*np.pi/180)
  return (da*cos_lat_rad).mean(dim=lat_name) / (cos_lat_rad).mean(dim=lat_name)
#---------------------------------------------------------------------------------------------------
def get_data(ds,var,lat_name='lat',lev_name='plev'):
   if var[v]=='wf':
      data = ds['residual'] + ds['utendepfd']
   else:
      data = ds[var[v]]
   data = data.sel({lat_name:slice(lat1,lat2)})
   data = data.sel({lev_name:slice(200e2,0)})
   data = data.mean(dim='time')
   data = lat_mean( data, lat_name )
   if var[v] in tend_vars:
      data = data*86400.
   return data
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(f'  var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
   data_list,lev_list = [],[]
   for c in range(num_case):
      print(f'    case: {hc.tclr.CYAN}{case[c]}{hc.tclr.END}')
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
   
      file_path = f'{gscratch}/{case[c]}/data_{remap_str}_tem/*.eam.{search_str}*'
      file_list = sorted(glob.glob(file_path))

      if 'first_file' in locals(): file_list = file_list[first_file:]
      if 'num_files'  in locals(): file_list = file_list[:num_files]

      # for ff in file_list: print(ff)

      # ds = xr.open_mfdataset(file_list)
      #----------------------------------------------------------------------
      # manually combine files due to issues with open_mfdataset on Perlmutter
      ds = xr.open_dataset(file_list[0])
      for f in file_list[1:]:
        ds_tmp = xr.open_dataset(f)
        ds = xr.concat([ds, ds_tmp], dim='time')
      #----------------------------------------------------------------------
      data = get_data(ds,var)
      #-------------------------------------------------------------------------
      # append final data to list
      data_list.append( data.values )
      lev_list.append( data['plev'].values/1e2 )

   data_list_list.append(data_list)
   lev_list_list.append(lev_list)

#---------------------------------------------------------------------------------------------------
# Create plot - overlay all cases for each var
#-------------------------------------------------------------------------------
for v in range(num_var):
   data_list = data_list_list[v]
   lev_list = lev_list_list[v]
   
   tres = copy.deepcopy(res)

   ip = v
   # ip = v*num_case+c
   # ip = c*num_var+v
   
   baseline = data_list[0]
   if plot_diff:
      for c in range(num_case): 
         data_list[c] = data_list[c] - baseline

   for c in range(num_case): 
      for i in range(len(data_list[c])):
         if lev_list[c][i]>p_max: data_list[c][i] = np.nan
         if lev_list[c][i]<p_min: data_list[c][i] = np.nan
   tres.trYMinF = p_min
   tres.trYMaxF = p_max
   
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = data_min
   tres.trXMaxF = data_max

   if var[v] in tend_vars:
      tres.trXMinF = -0.6
      tres.trXMaxF =  0.6

   for c in range(num_case):   
      tres.xyLineColor   = clr_list[c]
      tres.xyMarkerColor = clr_list[c]
      tres.xyDashPattern = dsh_list[c]
      if c==0:
         if var[v] in tend_vars: tres.tiXAxisString = f'[m/s/day]'
         if var[v] =='u':        tres.tiXAxisString = f'[m/s]'

      tplot = ngl.xy(wks, np.ma.masked_invalid(data_list[c]), lev_list[c], tres)

      
      if (c==1 and plot_diff) or (c==0 and not plot_diff) :
         plot[ip] = tplot
      elif (plot_diff and c>0) or not plot_diff:
         ngl.overlay(plot[ip],tplot)

   ### add vertical line
   lres = hs.res_xy()
   lres.xyLineThicknessF = 1
   lres.xyDashPattern = 0
   lres.xyLineColor = 'black'
   ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

   reg_str = ''
   # var_str = var[v]
   if 'lat1' in locals(): 
      lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
      lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
      reg_str += f' {lat1_str}:{lat2_str} '
   if 'lon1' in locals(): 
      lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
      lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
      reg_str += f' {lon1_str}:{lon2_str} '

   if plot_diff: var_str += ' (diff)'

   hs.set_subtitles(wks, plot[ip], vname[v], '', reg_str, font_height=0.01)

#---------------------------------------------------------------------------------------------------
# # old method for overlaying ERA5 data

# adj = 12 # time adjustment to account for 1979 vs 1980 start
# # v2
# # phase1 = [ 48-adj, 171-adj, 304-adj, 525-adj ]
# # phase2 = [ 82-adj, 147-adj, 252-adj, 390-adj ]
# # phase3 = [ 60-adj, 267-adj, 318-adj, 514-adj ]
# # phase4 = [102-adj, 189-adj, 348-adj, 435-adj ]
# # phases = [phase1, phase2, phase3, phase4]

# ERA5_time_ind = [ 48-adj, 171-adj ]

# ds = xr.open_dataset('/global/cfs/cdirs/m4310/whannah/ERA5_6hrly_tem_monthly_mean_90x180_1980-2022.nc')

# ds['dudt'] = ds['u'].differentiate(coord='time',datetime_unit='s')
# ds['residual'] =  ds['dudt'] - ds['utendvtem'] - ds['utendwtem']-ds['utendepfd']
# ds['wf'] = ds['residual'] + ds['utendepfd']

# ds = ds.sel(lat=slice(lat1,lat2),lev=slice(200e2,0))

# era5_lev  = ds['lev'].values
# era5_u = lat_mean( ds['u' ].isel(time=ERA5_time_ind) )
# # era5_f = lat_mean( ds['wf'].isel(time=ERA5_time_ind) )
# era5_f = lat_mean( ds['utendepfd'].isel(time=ERA5_time_ind) )


# tres.xyLineColor   = 'black'
# tres.xyMarkerColor = 'black'

# tres.xyDashPattern = 0
# ngl.overlay( plot[0], ngl.xy(wks, era5_u.isel(time=0).values, era5_lev, tres) )
# tres.xyDashPattern = 1
# ngl.overlay( plot[0], ngl.xy(wks, era5_u.isel(time=1).values, era5_lev, tres) )

# tres.xyDashPattern = 0
# ngl.overlay( plot[1], ngl.xy(wks, era5_f.isel(time=0).values, era5_lev, tres) )
# tres.xyDashPattern = 1
# ngl.overlay( plot[1], ngl.xy(wks, era5_f.isel(time=1).values, era5_lev, tres) )

#---------------------------------------------------------------------------------------------------
obs_root = '/global/cfs/cdirs/m4310/whannah/ERA5'
hc.printline()
print(f'  case: {hc.tclr.CYAN}ERA5{hc.tclr.END}')
for t,date in enumerate(tmp_date_list):
   print(f'    date: {hc.tclr.MAGENTA}{date}{hc.tclr.END}')
   yr = date[0:4]
   mn = date[5:7]
   obs_file = f'{obs_root}/ERA5_monthly_{yr}{mn}_tem.nc'
   obs_ds = xr.open_mfdataset(obs_file)
   obs_ds = obs_ds.rename({'latitude':'lat'})
   obs_ds = obs_ds.rename({'level':'plev'})
   obs_ds = obs_ds.isel(plev=slice(None, None, -1))
   obs_ds['plev'] = obs_ds['plev']*1e2
   obs_ds.load()
   for v in range(num_var):
      print(f'      var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
      #-------------------------------------------------------------------------
      data = get_data(obs_ds,var)#,lat_name='latitude',lev_name='level')
      #-------------------------------------------------------------------------
      tres.xyLineColor   = 'black'
      tres.xyMarkerColor = 'black'
      tres.xyDashPattern = t
      ngl.overlay( plot[v], ngl.xy(wks, data.values, data['plev'].values/1e2, tres) )
      #-------------------------------------------------------------------------
hc.printline()

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
