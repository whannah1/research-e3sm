import os, ngl, xarray as xr, numpy as np, numba
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import copy, cftime, warnings
import cmocean
#-------------------------------------------------------------------------------
case_dir,case_sub = [],[]
case,name = [],[]
clr,dsh,mrk = [],[],[]
def add_case(case_in,n=None,p=None,s=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   case.append(case_in); name.append(n)
   case_dir.append(p); case_sub.append(s)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)
#-------------------------------------------------------------------------------

# add_case('E3SM.2024-SCIDAC-zonal-mean-test.GNUCPU.ne30pg2_oECv3.F2010',n='E3SM+ZM-output',p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
# add_case('E3SM.2024-SCIDAC-zonal-mean-test-01.GNUCPU.ne30pg2_oECv3.F2010',n='E3SM+ZM-output',p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
add_case('E3SM.2024-SCIDAC-zonal-mean-test-03.GNUCPU.ne30pg2_oECv3.F2010',n='E3SM+ZM-output',p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

#-------------------------------------------------------------------------------

# lev = np.array([1,2,3,5,7,10,20,35,50,75,100,125,150,200,250,300,350,400,450,500,
#                550,600,650,700,750,800,825,850,875,900,925,950,975])
# lev = np.array([100,200,300,400,500,600,700,800,900,950])

# add_var('THzm')
# add_var('UWzm')
# add_var('Uzm')
# add_var('VTHzm')
# add_var('Vzm')
# add_var('WTHzm')
# add_var('Wzm')
# num_plot_col = 2

# add_var('U')
# add_var('V')
# add_var('OMEGA')
# num_plot_col = len(var)
# add_var('Uzm')
# add_var('Vzm')
# add_var('Wzm')

# add_var('UV')
add_var('UVzm_old')
add_var('UVzm')

# add_var('VTH')
# add_var('VTHzm_old')
# add_var('VTHzm')

# add_var('U')
# add_var('Uzm_old')
# add_var('Uzm')

plot_diff = False

var_x_case = True

# htype,first_file,num_files = 'h0',0,1
htype,first_file,num_files = 'h1',3,1


fig_file = 'figs_QBO/ZM_output_test'

lat1, lat2, dlat = -89., 89., 2

lat_bins = np.arange(lat1,lat2+dlat/2,dlat)


# tmp_file_head = 'data/zonal-mean'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
num_var = len(var)
num_case = len(case)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks('png',fig_file,wkres)
# if plot_diff:
#    plot = [None]*(num_var*(num_case*2))
# else:
#    plot = [None]*((num_var+0)*num_case)
plot = [None]*((num_var+0)*num_case)
res = hs.res_contour_fill()
res.vpHeightF = 0.3
res.trYReverse = True
res.tiXAxisString = 'Latitude'
# res.tiXAxisString = 'sin( Latitude )'
res.tiYAxisString = 'Pressure [hPa]'

res.trYMaxF = 100.

res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.lbLabelFontHeightF     = 0.015


lres = hs.res_xy()
lres.xyDashPattern    = 0
lres.xyLineThicknessF = 6
lres.xyLineColor      = 'black'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
@numba.njit()
def bin_numba(data, lat, lat_bins, nbin, ncol, nlev, bin_out, cnt):
   # cnt = np.zeros([nbin,nlev])
   for k in range( nlev ):
      for b in range( nbin ):
         bin_bot = lat_bins[b] - dlat/2. 
         bin_top = lat_bins[b] - dlat/2. + dlat
         for n in range( ncol ):
            if lat[n]>=bin_bot and lat[n]<bin_top :
               cnt[b,k] = cnt[b,k]+1
               bin_out[b,k] = ( bin_out[b,k]*cnt[b,k] + data[k,n] ) / ( cnt[b,k] + 1 )
@numba.njit()
def pert(X,Xb,Xp,lat,lat_bins,nbin,ncol,nlev):
   for k in range( nlev ):
      for b in range( nbin ):
         bin_bot = lat_bins[b] - dlat/2. 
         bin_top = lat_bins[b] - dlat/2. + dlat
         for n in range( ncol ):
            if lat[n]>=bin_bot and lat[n]<bin_top :
               Xp[k,n] = X[k,n] - Xb[b,k]
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+hc.tcolor.CYAN+var[v]+hc.tcolor.ENDC)
   data_list = []
   bins_list = []
   levs_list = []
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
      case_obj.set_coord_names(var[v])

      #-------------------------------------------------------------------------
      # read the data
      lat  = case_obj.load_data('lat',  htype=htype)
      area = case_obj.load_data('area', htype=htype).astype(np.double)

      # if 'zm' in var[v]:
      #    data = case_obj.load_data(var[v],htype=htype, first_file=first_file, num_files=num_files )
      # else:
      #    data = case_obj.load_data(var[v],htype=htype, first_file=first_file, num_files=num_files, lev=lev )

      tvar = var[v]
      if var[v]=='UV': tvar = 'U'
      data = case_obj.load_data(tvar,htype=htype, first_file=first_file, num_files=num_files )

      data = data.isel(time=0,drop=True)

      if var[v]=='UV': 
         tvar = 'V'
         U = data
         V = case_obj.load_data(tvar,htype=htype, first_file=first_file, num_files=num_files )
         # U = U.isel(time=0,drop=True)
         V = V.isel(time=0,drop=True)
         nbin = len(lat_bins)
         ncol = len(data.ncol)
         nlev = len(data.lev)
         # cnt_out = np.zeros([nbin,nlev])
         Ub = np.zeros([nbin,nlev])
         Vb = np.zeros([nbin,nlev])
         bin_numba(U.values, lat.values, lat_bins, nbin, ncol, nlev, Ub, np.zeros([nbin,nlev]))
         bin_numba(V.values, lat.values, lat_bins, nbin, ncol, nlev, Vb, np.zeros([nbin,nlev]))
         # Up = U - Ub
         # Vp = V - Vb
         Up = np.zeros([nlev,ncol])
         Vp = np.zeros([nlev,ncol])
         pert(U.values,Ub,Up,lat.values,lat_bins,nbin,ncol,nlev)
         pert(V.values,Vb,Vp,lat.values,lat_bins,nbin,ncol,nlev)
         data = xr.DataArray(Up*Vp,coords=U.coords)

      if 'time' in data.dims:
         data = data.isel(time=0,drop=True)

      # print()
      # print(data.shape)
      # print()

      # hc.print_stat(data,compact=True)
      #----------------------------------------------------------------------
      if 'zm' in var[v]:
         data = data.isel(zalon=0)#.mean(dim='time')#.transpose()
         tmp_data = data.values
         bins_list.append( data['zalat'].values )
         levs_list.append( data['lev'].values )
      else:
         nbin = len(lat_bins)
         ncol = len(data.ncol)
         nlev = len(data.lev)
         bin_out = np.zeros([nbin,nlev])
         cnt_out = np.zeros([nbin,nlev])
         bin_numba(data.values, lat.values, lat_bins, nbin, ncol, nlev, bin_out, cnt_out)
         tmp_data = bin_out.transpose()
         bins_list.append( lat_bins )
         levs_list.append( data['lev'].values )
         
      if plot_diff :
         if v==0: baseline = tmp_data
         if v>=1: tmp_data = tmp_data - baseline

      hc.print_stat(tmp_data,compact=True)

      data_list.append( tmp_data )

   #------------------------------------------------------------------------------------------------
   # Plot zonally averaged data
   for c in range(num_case):
      # data_dir_tmp,data_sub_tmp = None, None
      # if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      # if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      # case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
      #-------------------------------------------------------------------------
      ip = v*num_case+c if var_x_case else c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors, contour levels, and plot strings
      tres = copy.deepcopy(res)
      tres.cnLevelSelectionMode = "ExplicitLevels"

      if var[v] in ['UVzm', 'UV' ,'UVzm_old']: tres.cnLevels = np.linspace(-50,50,21)
      # if var[v] in ['UVzm', 'UV' ,'UVzm_old']: tres.cnLevels = np.logspace(  0.0, 2, num=30)
      if var[v] in ['U',    'Uzm','Uzm_old']: tres.cnLevels = np.linspace(-40,40,41)
      if var[v] in ['V',    'Vzm']: tres.cnLevels = np.linspace(-6,6,11)
      if var[v] in ['OMEGA']      : tres.cnLevels = np.linspace(-0.04,0.04,11)
      if var[v] in ['Wzm']        : tres.cnLevels = np.linspace(-0.02,0.02,11)

      if plot_diff and v>=1: 
         tres.cnLevelSelectionMode = "AutomaticLevels"
         # if var[v] in ['UVzm', 'UV' ,'UVzm_old']: tres.cnLevels = np.linspace(-1,1,21)
         # if var[v] in ['UVzm', 'UV' ,'UVzm_old']: tres.cnLevels = np.logspace(  0.0, 2, num=30)
         if var[v] in ['U',    'Uzm','Uzm_old' ]: tres.cnLevels = np.linspace(-2,2,21)

      #-------------------------------------------------------------------------
      # Create contour plot
      #-------------------------------------------------------------------------
      # tres.sfYArray = levs_list[c]
      # tres.sfXArray = bins_list[c]
      # lat_tick = np.array([-90,-60,-30,0,30,60,90])
      # res.tmXBMode = "Explicit"
      # res.tmXBValues = lat_tick
      # res.tmXBLabels = lat_tick

      # plot[ip] = ngl.contour(wks, data_list[c], tres)

      # cstr = ''
      # if plot_diff and v>=1: cstr = '(diff)'
      # hs.set_subtitles(wks, plot[ip], name[c], cstr, var[v], font_height=0.01)

      #-------------------------------------------------------------------------
      # Create xy line plot of single level
      #-------------------------------------------------------------------------
      k = 20 # 30mb
      # k = 25 # 50mb

      clr = ['red','blue']
      dsh = [0,1]

      tres = copy.deepcopy(lres)
      tres.xyLineColor   = clr[v]
      tres.xyDashPattern = dsh[v]
      # tres.xyLineThicknessF = 6
      tplot = ngl.xy(wks, bins_list[c], np.ma.masked_invalid( data_list[c][k,:] ), tres)
      if c==0 and v==0:
         plot[0] = tplot
      else:
         ngl.overlay(plot[0],tplot)

rstr = f'{var[v]} {levs_list[c][k]:.2f} mb'
print(f'rstr: {rstr}')
hs.set_subtitles(wks, plot[0], '', '', rstr, font_height=0.01)
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

if 'num_plot_col' in locals():
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [num_var,num_case] if var_x_case else [num_case,num_var]

# ngl.panel(wks,plot,layout,hs.setres_panel())
# ngl.panel(wks,plot,layout,ngl.Resources())

ngl.panel(wks,[plot[0]],[1,1],ngl.Resources())

ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
