import os, ngl, copy, glob, xarray as xr, numpy as np
import hapy_common as hc
class tclr:END,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
tmp_date_list = []
tmp_date_list.append('1983-01-01') # phase 1 - pi*1/4
tmp_date_list.append('1993-04-01')
# tmp_date_list.append('2002-07-01')
# tmp_date_list.append('2022-10-01')
# tmp_date_list.append('1986-10-01') # phase 1 - pi*3/4
# tmp_date_list.append('1995-07-01')
# tmp_date_list.append('2000-04-01')
# tmp_date_list.append('2009-01-01')
# tmp_date_list.append('1982-01-01') # phase 1 - pi*5/4
# tmp_date_list.append('1987-04-01')
# tmp_date_list.append('2014-07-01')
# tmp_date_list.append('2021-10-01')
# tmp_date_list.append('1984-10-01') # phase 1 - pi*7/4
# tmp_date_list.append('1994-07-01')
# tmp_date_list.append('2006-04-01')
# tmp_date_list.append('2013-01-01')
#---------------------------------------------------------------------------------------------------
compset = 'F20TR'
grid    = 'ne30pg2_r05_IcoswISC30E3r5'
ens_id  = '2024-SCIDAC-PCOMP-TEST' # initial test to work out logistical issues
def get_case_name(e,c,h,d): return'.'.join(['E3SM',ens_id,grid,f'EF_{e:0.2f}',f'CF_{c:02.0f}',f'HD_{h:0.2f}',f'{d}'])
#---------------------------------------------------------------------------------------------------
case_list = []
gweff_list, cfrac_list, hdpth_list = [],[],[]
start_date_list = []
def add_case(e,c,h,d):
   gweff_list.append(e)
   cfrac_list.append(c)
   hdpth_list.append(h)
   start_date_list.append(d)
   case_list.append( get_case_name(e,c,h,d) )
#---------------------------------------------------------------------------------------------------
tdate = '1983-01-01'
add_case(e=0.35,c=10,h=0.50,d=tdate) #  <<< v3 default
add_case(e=0.12,c=16,h=0.48,d=tdate) # prev surrogate optimum
tdate = '1993-04-01'
add_case(e=0.35,c=10,h=0.50,d=tdate) #  <<< v3 default
add_case(e=0.12,c=16,h=0.48,d=tdate) # prev surrogate optimum
#---------------------------------------------------------------------------------------------------
# # build list of cases with all dates
# for date in tmp_date_list:
#    add_case(e=0.35,c=10,h=0.50,d=date) #  <<< v3 default
#    add_case(e=0.12,c=16,h=0.48,d=date) # prev surrogate optimum
#    # add_case(e=0.09,c=20,h=0.25,d=date) # no QBO at all
#    # add_case(e=0.70,c=21,h=0.31,d=date) # QBO is too fast

#---------------------------------------------------------------------------------------------------

scratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# scratch = '/global/cfs/cdirs/m3312/whannah/2022-QBO-TEST'

htype = 'h1'

#---------------------------------------------------------------------------------------------------
# constants
H     = 7e3          # m         assumed mean scale heightof the atmosphere
P0    = 101325       # Pa        surface pressure
Rd    = 287.058      # J/kg/K    gas constant for dry air
cp    = 1004.64      # J/kg/K    specific heat for dry air
g     = 9.80665      # m/s       global average of gravity at MSLP
a     = 6.37123e6    # m         Earth's radius
omega = 7.29212e-5   # 1/s       Earth's rotation rate
pi    = 3.14159

#---------------------------------------------------------------------------------------------------
num_case = len(case_list)
for c in range(num_case):
   print(f'    case: {tclr.CYAN}{case_list[c]}{tclr.END}')

   root = f'{scratch}/{case_list[c]}/data_remap_90x180_tem'

   file_path = f'{root}/*eam.{htype}.*'

   file_list = sorted(glob.glob(file_path))

   #----------------------------------------------------------------------------
   # loop through files to calculate TEM terms 
   for f in file_list: 

      f_out = f.replace(f'.{htype}.',f'.{htype}.tem.')

      cmd = f'mv {f} {f_out}'
      print(cmd)
      os.system(cmd)

      #-------------------------------------------------------------------------

print(); print('done.'); print()
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------