import os
import sys
import numpy as np
import subprocess
import glob

# salloc --account=CLI115 --nodes=1

os.system('module load openblas netlib-lapack')
os.system('module unload openmpi')

case = ['earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m']

tempest_path = os.getenv("HOME")+'/Tempest/tempestextremes/bin/'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)
for c in range(num_case):
   idir = '/gpfs/alpine/scratch/hannah6/cli115/analysis_files/'+case[c]+'/'
   infiles = glob.glob(idir+'*tc_tracking_input*')
   infiles = ' '.join(infiles)
   
   os.environ["infiles"] = infiles
   os.environ["outfile"] = idir+'TE_output.txt'
   os.environ["connectivityf"] = idir+'connectivity_file'

   # os.system('echo $infiles ')
   # exit()

   cmd = tempest_path+"DetectNodes"
   cmd = cmd + " --in_data_list \"$infiles\" "
   cmd = cmd + " --timestride 2"
   cmd = cmd + " --in_connect $connectivityf"
   cmd = cmd + " --out $outfile"
   cmd = cmd + " --closedcontourcmd \"PSL,200.0,5.5,0;_DIFF(Z300,Z500),-6.0,6.5,1.0\" "
   cmd = cmd + " --mergedist 6.0 --searchbymin PSL --outputcmd \"PSL,min,0;_VECMAG(UBOT,VBOT),max,2;PHIS,max,0\" "

   print('\n'+cmd+'\n')
   os.system(cmd)
   
   # cmd = tempest_path+"StitchNodes"
   # cmd = cmd + " --format \"ncol,lon,lat,slp,wind,phis\" "
   # cmd = cmd + " --range 8.0"
   # cmd = cmd + " --minlength 10"
   # cmd = cmd + " --maxgap 3"
   # cmd = cmd + " --in $outfile"
   # cmd = cmd + " --out trajectories.txt"
   # cmd = cmd + " --threshold \"wind,>=,10.0,10;lat,<=,50.0,10; lat,>=,-50.0,10;phis,<=,150.0,10\" "

   # print('\n'+cmd+'\n')
   # os.system(cmd)

   # cmd = tempest_path+"NodeFileEditor"
   # cmd = cmd + " --in_connect $connectivityf"
   # cmd = cmd + " --in_data_list \"$infiles\" "
   # cmd = cmd + " --in_file trajectories.txt"
   # cmd = cmd + " --out_file radprof.txt"
   # cmd = cmd + " --in_fmt \"lon,lat,slp,wind,phis\" "
   # cmd = cmd + " --calculate \"rprof=radial_wind_profile(UBOT,VBOT,159,0.125) ;rsize=lastwhere(rprof,>,8)\" "
   # cmd = cmd + " --out\_fmt \"lon,lat,rsize,rprof\" "

   # print('\n'+cmd+'\n')
   # os.system(cmd)
   
   # cmd = tempest_path+"NodeFileFilter"
   # cmd = cmd + " --in_nodefile radprof.txt"
   # cmd = cmd + " --in_fmt \"lon,lat,rsize,rprof\" "
   # cmd = cmd + " --in_connect $connectivityf"
   # cmd = cmd + " --in_data_list \"$infiles\" "
   # cmd = cmd + " --out_data_list \"node_output_files.txt\" "
   # cmd = cmd + " --bydist \"PRECT,rsize\" "

   # print('\n'+cmd+'\n')
   # os.system(cmd)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------