import os
import ngl
import copy
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs

### case name
case = 'INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'

### case name for plot title
case_name = 'INCITE2019'

input_file_path = f'/ccs/home/hannah6/E3SM/scratch/{case}/run/{case}.cam.h1.0004-01-01-00000.nc'
# input_file_path = f'/ccs/home/hannah6/E3SM/scratch/{case}/run/{case}.cam.h2.0004-01-01-00000.nc'

scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne120pg2_scrip.nc'

### list of variables to plot
# var = ['UBOT','TMQ','TGCLDLWP','PRECT']
# var = ['SPTKE','SPTKES']
var = ['TGCLDIWP']
# var = ['CLDLIQ','CLDICE']
# lev = 60

# Available fields:
# FLDS, FLDSC, FLNS, FLNT, FSDS, FSDSC, FSNS, FSNT
# LHFLX, LWCF, PRECT, PS, PSL, QREFHT, SHFLX, SWCF, 
# TGCLDIWP, TGCLDLWP, TMQ, TREFHT, TS, U10, UBOT, VBOT, Z300, Z500

# create_png,create_gif = True,True
create_png,create_gif = True,False
# create_png,create_gif = False,True

### single time index to load (no averaging)
time1,time2 = 0,1#0
# time1 = 0
# time2 = time1+10

### output figure type and name
fig_type = 'x11'
# fig_type = 'png'

# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_animation/incite2019.animation.map.v1'+f'.{var[0]}'

# lat1,lat2 = -60,60
# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_animation/incite2019.animation.map.v1'+f'.{var[0]}.{lat1}_{lat2}'

# lat1,lat2,lon1,lon2 = 0,40,360-160,360-80     # East Pac
# lat1,lat2,lon1,lon2 = -45,0,90,180            # Australia
# lat1,lat2,lon1,lon2 = -45,0,180,180+90            # South Pac
lat1,lat2,lon1,lon2 = 0,45,140,140+90            # North Pac
gif_file = os.getenv('HOME')+'/Research/E3SM/figs_animation/incite2019.animation.map.v1'+f'.{var[0]}.lat_{lat1}_{lat2}.lon_{lon1}_{lon2}'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.lbOrientation                = 'Horizontal'
res.lbLabelFontHeightF           = 0.008
res.mpGridAndLimbOn              = False
res.mpCenterLonF                 = 180
res.mpGeophysicalLineColor       = 'white'
res.mpLimitMode                  = 'LatLon' 
if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

#---------------------------------------------------------------------------------------------------
# define function to add subtitles to the top of plot
#---------------------------------------------------------------------------------------------------
def set_subtitles(wks, plot, left_string='', center_string='', right_string='', font_height=0.01):
   ttres          = ngl.Resources()
   ttres.nglDraw  = False

   ### Use plot extent to call ngl.text(), otherwise you will see this error: 
   ### GKS ERROR NUMBER   51 ISSUED FROM SUBROUTINE GSVP  : --RECTANGLE DEFINITION IS INVALID
   strx = ngl.get_float(plot,'trXMinF')
   stry = ngl.get_float(plot,'trYMinF')
   ttres.txFontHeightF = font_height

   ### Set annotation resources to describe how close text is to be attached to plot
   amres = ngl.Resources()
   if not hasattr(ttres,'amOrthogonalPosF'):
      amres.amOrthogonalPosF = -0.52   # Top of plot plus a little extra to stay off the border
   else:
      amres.amOrthogonalPosF = ttres.amOrthogonalPosF

   ### Add left string
   amres.amJust,amres.amParallelPosF = 'BottomLeft', -0.5   # Left-justified
   tx_id_l   = ngl.text(wks, plot, left_string, strx, stry, ttres)
   anno_id_l = ngl.add_annotation(plot, tx_id_l, amres)
   # Add center string
   amres.amJust,amres.amParallelPosF = 'BottomCenter', 0.0   # Centered
   tx_id_c   = ngl.text(wks, plot, center_string, strx, stry, ttres)
   anno_id_c = ngl.add_annotation(plot, tx_id_c, amres)
   # Add right string
   amres.amJust,amres.amParallelPosF = 'BottomRight', 0.5   # Right-justified
   tx_id_r   = ngl.text(wks, plot, right_string, strx, stry, ttres)
   anno_id_r = ngl.add_annotation(plot, tx_id_r, amres)

   return
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

ds = xr.open_dataset( input_file_path )
ncol = ds['ncol']
mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
if 'lat1' in vars(): mask = mask & (ds['lat']>=lat1)
if 'lat2' in vars(): mask = mask & (ds['lat']<=lat2)
if 'lon1' in vars(): mask = mask & (ds['lon']>=lon1)
if 'lon2' in vars(): mask = mask & (ds['lon']<=lon2)

case_obj = he.Case( name=case )

tfiles = ''
for t in range(time1,time2):
   fig_file = gif_file+'.'+str(t).zfill(3)
   tfiles = tfiles+'  '+f'{fig_file}.{fig_type}'
   print(f'  t: {t:3d}     {fig_file}.{fig_type}')

   if create_png :
      ### create the plot workstation
      wks = ngl.open_wks(fig_type,fig_file)
      plot = []

      for v in range(num_var):
         # print('  var: '+var[v])
         #-------------------------------------------------------------------------
         # read the data
         #-------------------------------------------------------------------------
         data = ds[var[v]].isel(time=t).where(mask,drop=True)
         # data = ds[var[v]].isel(time=t,lev=lev).where(mask,drop=True)
         # data = ds[var[v]].isel(time=t).where(mask,drop=True).sum(dim='lev')

         if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3

         ### print temporal frequency and length
         # dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
         # print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

         ### Calculate time mean
         if 'time' in data.dims : data = data.mean(dim='time')

         ### Calculate area-weighted global mean
         # area = ds['area'].where( mask,drop=True)
         # gbl_mean = ( (data*area).sum() / area.sum() ).values 
         # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

         #-------------------------------------------------------------------------
         # Set colors and contour levels
         #-------------------------------------------------------------------------
         tres = copy.deepcopy(res)
         ### change the color map depending on the variable
         tres.cnFillPalette = 'ncl_default'
         if var[v] in ['OMEGA']                    : tres.cnFillPalette = 'BlueWhiteOrangeRed'
         if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
         if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = 'CBR_wet'
         if var[v] in ['TMQ']                      : tres.cnFillPalette = 'MPL_YlGnBu'
         if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = 'MPL_viridis'
         if var[v] in ['CLDLIQ','CLDICE']          : tres.cnFillPalette = 'MPL_viridis'

         ### specify specific contour intervals
         # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,80+1,1)
         # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.logspace(-1,2,num=100)
         if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.logspace(-6,2,num=100)
         if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
         if var[v]=='TGCLDLWP'            : tres.cnLevels = np.logspace( -2 , 0.25, num=100)
         if var[v]=='TGCLDIWP'            : tres.cnLevels = np.logspace( -10, 2, num=255)
         if var[v]=='TMQ'                 : tres.cnLevels = np.arange(2,62+1,1)
         if var[v] in ['UBOT','VBOT']     : tres.cnLevels = np.linspace(-20,20,81)
         if var[v]=='SPTKE'               : tres.cnLevels = np.arange(0.2,10+0.2,0.2)
         if var[v]=='SPTKES'              : tres.cnLevels = np.arange(1,21,1)*1e-2

         if var[v]=='CLDLIQ'              : tres.cnLevels = np.logspace(-8,-1,num=100)
         if var[v]=='CLDICE'              : tres.cnLevels = np.logspace(-8,-1,num=100)

         ### use this for symmetric contour intervals
         if var[v] in ['U','V'] :
            cmin,cmax,cint,clev = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
                                                       cint=None, max_steps=21,              \
                                                       returnLevels=True, aboutZero=True )
            tres.cnLevels = np.linspace(cmin,cmax,num=91)

         if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
         
         #-------------------------------------------------------------------------
         # Set up cell fill attributes using scrip grid file
         #-------------------------------------------------------------------------
         # hs.set_cell_fill(case_obj,tres)
         scripfile = xr.open_dataset(scrip_file_name)
         tres.cnFillMode    = 'CellFill'
         tres.sfXArray      = scripfile['grid_center_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
         tres.sfYArray      = scripfile['grid_center_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
         tres.sfXCellBounds = scripfile['grid_corner_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values 
         tres.sfYCellBounds = scripfile['grid_corner_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
         
         #-------------------------------------------------------------------------
         # Create plot
         #-------------------------------------------------------------------------
         plot.append( ngl.contour_map(wks,data.values,tres) )
         set_subtitles(wks, plot[len(plot)-1], case_name, '', var[v], font_height=0.01)
      #------------------------------------------------------------------------------------------------
      # Finalize plot
      #------------------------------------------------------------------------------------------------
      layout = [len(plot),1]
      if num_var==4 : layout = [2,2]
      ngl.panel(wks,plot[0:len(plot)],layout,pres)
      del wks

      ### trim white space from image using imagemagik
      fig_file = fig_file+'.'+fig_type
      os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
      # print('\n'+fig_file+'\n')

if create_png : ngl.end()
#---------------------------------------------------------------------------------------------------
# Create animated gif
#---------------------------------------------------------------------------------------------------
if create_gif :
   # cmd = "convert -repage 0x0 -delay 10 -loop 0  figs_scream/scream.animation.map.v1.*png   figs_scream/scream.animation.map.v1.gif"
   cmd = 'convert -repage 0x0 -delay 10 -loop 0  '+tfiles+' '+gif_file+'.gif'
   print('\n'+cmd+'\n')
   os.system(cmd)
   print('\n  '+gif_file+'.gif'+'\n')
