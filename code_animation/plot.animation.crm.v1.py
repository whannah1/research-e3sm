import os, ngl, copy, glob
import xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
class tcolor: ENDC,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None):
   global name,case,case_dir,case_sub
   case.append(case_in); name.append(n)
   case_dir.append(p); case_sub.append(s)
var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)
#-------------------------------------------------------------------------------

npix = 1024
# npix = 2048


scratch_cpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
scratch_gpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
scrach_summit = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/'


add_case('E3SM.2023-PAM-SCM-00.ne4_ne4.FSCM-ARM97-MMF1',                  n='MMF-SAM',p=scratch_gpu,s='run')
add_case('E3SM.2023-PAM-SCM-00.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',p=scratch_gpu,s='run')

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

htype,first_file,num_files = 'h2',5,2



add_var('CRM_QV')
add_var('CRM_QC')
add_var('CRM_QI')
add_var('CRM_QP')
# add_var('CRM_U')


### single time index to load (no averaging)
# time1,time2,dtime = 0,24*4-1,1
time1,time2,dtime = 0,72*num_files-1,1
# time1,time2,dtime = 0,1,1


create_png = True
create_gif = True

### output figure type and name
fig_type = 'png'



gif_file = os.getenv('HOME')+'/Research/E3SM/figs_animation/animation.crm.v1'#+f'.{case[0]}.{var[0]}'

if 'lat1' in locals(): gif_file = gif_file+f'.{lat1}_{lat2}'

var_x_case = False
num_plot_col = len(case)

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if 'create_png' not in locals(): create_png = False
if 'create_gif' not in locals(): create_gif = False

num_var = len(var)
num_case = len(case)

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.cnConstFEnableFill           = True
res.lbOrientation                = 'Horizontal'
res.lbLabelFontHeightF           = 0.008

res.lbLabelBarOn                 = True
res.pmTickMarkDisplayMode        = "Never"

pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 10
pres.nglPanelXWhiteSpacePercent = 10


if 'create_png' not in locals(): create_png = False
if 'create_gif' not in locals(): create_gif = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# ds = xr.open_mfdataset( input_file_path, combine='by_coords' )
# ncol = ds['ncol']
# mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
# if 'lat1' in vars(): mask = mask & (ds['lat'].isel(time=0)>=lat1)
# if 'lat2' in vars(): mask = mask & (ds['lat'].isel(time=0)<=lat2)
# if 'lon1' in vars(): mask = mask & (ds['lon'].isel(time=0)>=lon1)
# if 'lon2' in vars(): mask = mask & (ds['lon'].isel(time=0)<=lon2)

# def drop_rad_band(ds): 
#       if 'lwband' in ds.variables: ds = ds.drop(['lwband'])
#       if 'swband' in ds.variables: ds = ds.drop(['swband'])
#       return ds

def get_file_list(c):
   input_file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{case[c]}.eam.{htype}.*.nc'

   files = sorted(glob.glob(input_file_path))
   files = files[ first_file : first_file+num_files ]

   return files
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# if create_png and num_case==1 :
#    ds = xr.open_mfdataset( get_file_list(0), combine='by_coords' )
#    ntime = len(ds['time'])
#    print(f'  ntime = {ntime}')
#    print(f'  time1 = {time1}')
#    print(f'  time2 = {time2}')
#    print(f'  dtime = {dtime}')
#    if ntime<time2: exit('ERROR: time2 is too large!')
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
tfiles = ''
for t in range(time1,time2,dtime):
   fig_file = gif_file+'.'+str(t).zfill(3)
   tfiles = tfiles+'  '+f'{fig_file}.{fig_type}'
   tmp_path = os.getenv('HOME')+'/Research/E3SM/'
   short_fig_file = fig_file.replace(tmp_path,'')
   msg = f'  t: {t:3d}     {short_fig_file}.{fig_type}'
   msg = tcolor.GREEN + msg + tcolor.ENDC
   print(msg)


   if create_png :
      ### create the plot workstation
      wkres = ngl.Resources()
      wkres.wkWidth,wkres.wkHeight  = npix,npix
      wks = ngl.open_wks(fig_type,fig_file,wkres)
      plot = [None]*(num_var*num_case)

      for c in range(num_case):
         #------------------------------------------------
         #------------------------------------------------
         # if num_case>1:
         # print('    case: '+case[c])
         ds = xr.open_mfdataset( get_file_list(c), combine='by_coords' )
         #------------------------------------------------
         #------------------------------------------------
         for v in range(num_var):
            # print('  var: '+var[v])
            ip = v*num_case+c if var_x_case else c*num_var+v
            #-------------------------------------------------------------------------
            # read the data
            #-------------------------------------------------------------------------
            if var[v]=='CRM_QP':
                if 'MMF1' in case[c]: data = ds['CRM_QPC'] #+ ds['CRM_QPI']
                if 'MMF2' in case[c]: data = ds['CRM_QR']
            elif var[v]=='CRM_QI' and 'MMF1' in case[c]:
                data = ds['CRM_QI'] + ds['CRM_QPI']
            else:
                data = ds[var[v]]

            data = data.isel(time=t).isel(crm_ny=0,ncol=0)

            if var[v]=='CRM_QV': data = data*1e3
            if var[v]=='CRM_QC': data = data*1e3
            if var[v]=='CRM_QI': data = data*1e3
            if var[v]=='CRM_QP': data = data*1e3

            #-------------------------------------------------------------------------
            # Set colors and contour levels
            #-------------------------------------------------------------------------
            tres = copy.deepcopy(res)
            # tres.cnFillPalette = 'ncl_default'
            tres.cnFillPalette = 'MPL_viridis'

            tres.cnLevelSelectionMode = 'ExplicitLevels'

            # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,40+1,1)
            # if var[v]=='CRM_QV' : tres.cnLevels = np.arange(1,16+1,1)
            if var[v]=='CRM_QV' : tres.cnLevels = np.linspace(1,15,20)
            # if var[v]=='CRM_QC' : tres.cnLevels = np.linspace(0.01,3.0,10)
            # if var[v]=='CRM_QI' : tres.cnLevels = np.linspace(0.001,0.2,30)
            if var[v]=='CRM_QC' : tres.cnLevels = np.logspace( -3 , 0.4, num=40)
            if var[v]=='CRM_QI' : tres.cnLevels = np.logspace( -3 , 0.4, num=40)
            if var[v]=='CRM_QP' : tres.cnLevels = np.logspace( -3 , 0.4, num=40)
            if var[v]=='CRM_U'  : tres.cnLevels = np.linspace(-20,20,21)

            #-------------------------------------------------------------------------
            # Create plot
            #-------------------------------------------------------------------------

            plot[ip] = ngl.contour(wks,data.values,tres)

            hs.set_subtitles(wks, plot[ip], name[c], '', var[v],font_height=0.01)
      #------------------------------------------------------------------------------------------------
      # Finalize plot
      #------------------------------------------------------------------------------------------------

      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      else:
         # layout = [num_var,num_case]
         # layout = [num_case,num_var]
         layout = [num_var,num_case] if var_x_case else [num_case,num_var]
         if num_case==4 and num_var==1: layout = [2,2]

      ngl.panel(wks,plot[0:len(plot)],layout,pres)
      # del wks

      ### trim white space from image using imagemagik
      fig_file = fig_file+'.'+fig_type
      os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )

      # print('\n'+fig_file+'\n')

      ngl.destroy(wks)

if create_png : ngl.end()
#---------------------------------------------------------------------------------------------------
# Create animated gif
#---------------------------------------------------------------------------------------------------
if create_gif :
   # cmd = "convert -repage 0x0 -delay 10 -loop 0  figs_scream/scream.animation.map.v1.*png   figs_scream/scream.animation.map.v1.gif"
   # cmd = f'convert -repage 0x0 -delay 10 -loop 0  {tfiles} {gif_file}.gif'
   cmd = f'convert +repage -delay 10 -loop 0  {tfiles} {gif_file}.gif'
   print('\n'+cmd+'\n')
   os.system(cmd)

   tmp_path = os.getenv('HOME')+'/Research/E3SM/'
   short_gif_file = gif_file.replace(tmp_path,'')
   print(f'\n  {short_gif_file}.gif'+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

