import os, ngl, copy, glob, cmocean
import xarray as xr, numpy as np
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,c=None,d=None):
   global name,case,case_dir,case_sub
   case.append(case_in); name.append(n)
   case_dir.append(p); case_sub.append(s)
#-------------------------------------------------------------------------------

# npix = 1024
npix = 2048

class tcolor: ENDC,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'


### fully coupled test
# name,case = [],[]
# case.append(f'E3SM.CPL-TEST.GNUGPU.ne30pg2_EC30to60E2r2.WCYCL1850_MMFXX.RADNX_4.BVT.00');     name.append('WCYCL1850_MMFXX')

### RCE tests for Da
# case.append(f'E3SM.ne30pg2.F-MMFXX-RCEMIP.00');     name.append('RCE no VT no rotation')
# case.append(f'E3SM.ne30pg2.F-MMFXX-RCEROT.00');     name.append('RCE no VT w/ rotation')
# case.append(f'E3SM.ne30pg2.F-MMFXX-RCEMIP.BVT.00'); name.append('RCE w/ VT no rotation')
# case.append(f'E3SM.ne30pg2.F-MMFXX-RCEROT.BVT.00'); name.append('RCE w/ VT w/ rotation')

# scratch_path = os.getenv('HOME')+'/E3SM/scratch'
# case.append(f'E3SM.VTVAL-HC.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.00');     name.append('E3SM-MMF')
# case.append(f'E3SM.VTVAL-HC.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_0.00');name.append('E3SM-MMF+BVT')

# case.append(f'{case_head}.CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_4.FCVT_16.00');     name.append('MMF+FVT 16')

# scratch_cpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# scratch_gpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
# add_case('E3SM.2023-PAM-DEV-02.GNUCPU.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='PAM CPU'     ,p=scratch_cpu,s='run')
# add_case('E3SM.2023-PAM-DEV-03.GNUCPU.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='PAM CPU w/SL',p=scratch_cpu,s='run')
# add_case('E3SM.2023-PAM-DEV-02.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='PAM GPU'     ,p=scratch_gpu,s='run')
# add_case('E3SM.2023-PAM-DEV-03.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='PAM GPU w/SL',p=scratch_gpu,s='run')
# add_case('E3SM.2023-PAM-DEV-03.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.L64',n='SAM GPU'     ,p=scratch_gpu,s='run')

# ### test run for 2024 E3SM tutorial
# tmp_path = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# add_case('E3SM.2024-E3SM-tutorial-hindcast.2023-09-08',n='E3SM',p=tmp_path,s='run')

### 2024 Rotating RCE for Da Yang
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2024-RCEROT.FRCEROT-MMF1_320.NX_128x1.DX_1000', n='MMF RCEROT 320K', p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-RCEROT.FRCEROT-MMF1_300.NX_128x1.DX_1000', n='MMF RCEROT 300K', p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-RCEROT-01.FRCEROT-MMF1_320.NX_128x1.DX_1000', n='MMF RCEROT 320K', p=tmp_path,s=tmp_sub)

### 2024 relaxed slab ocean tests
tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
add_case('E3SM.2024-RSO-test-00.F2010',    n='F2010',           c='red',     p=tmp_path,s=tmp_sub)
add_case('E3SM.2024-RSO-test-00.F2010-RSO',n='F2010+RSO tau=8', c='blue',    p=tmp_path,s=tmp_sub) # RSO_relax_tau = 8 days
add_case('E3SM.2024-RSO-test-01.F2010-RSO',n='F2010+RSO MLD=50',c='purple',  p=tmp_path,s=tmp_sub) # Fixed MLD = 50
add_case('E3SM.2024-RSO-test-02.F2010-RSO',n='F2010+RSO tau=2', c='magenta', p=tmp_path,s=tmp_sub) # RSO_relax_tau = 2 days

#-------------------------------------------------------------------------------
### scrip file for native grid plot
# scrip_file_name = '/global/homes/w/whannah/E3SM/scratch/scream/ne1024np4_scrip_c20190603.nc'
# scrip_file_name= os.getenv("HOME")+'/E3SM/data_grid/ne120pg1_scrip.nc'
scrip_file_name= os.getenv("HOME")+'/E3SM/data_grid/ne30pg2_scrip.nc'
# scrip_file_name= os.getenv("HOME")+'/E3SM/data_grid/ne4pg2_scrip.nc'
# scrip_file_name= os.getenv("HOME")+'/E3SM/data_grid/ne45pg2_scrip.nc'
#-------------------------------------------------------------------------------

htype = 'h1'


### list of variables to plot
# var = ['PRECT','TGCLDLWP','TMQ','UBOT']
# var = ['PRECT','TGCLDLWP']
# var = ['TMQ','PRECT','TGCLDLWP']
# var = ['TGCLDLWP','TGCLDIWP']
# var = ['Q850','T850']
# var = ['WSPD850','WSPDBOT']
# var = ['TS']
var = ['SST']

### single time index to load (no averaging)
# time1,time2,dtime = 1,40,1
# time1,time2,dtime = 0,23,1
# time1,time2,dtime = 5*24,10*24,3
time1,time2,dtime = 0,48*2,1
# day1,nday = 0,3;time1,time2,dtime = day1*24,(day1+nday)*24,1
# time1 = 25*8; time2 = time1+2

plot_diff = True

single_frame = False

create_png = True
create_gif = True

### output figure type and name
fig_type = 'png'

rotating_sphere = False


# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_animation/animation.map.v1'+f'.{case}.{var[0]}.{lat1}_{lat2}'
# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_animation/animation.map.v1'+f'.{case[0]}.{var[0]}.{lat1}_{lat2}'
gif_file = os.getenv('HOME')+'/Research/E3SM/figs_animation/animation.map.v1'+f'.{case[0]}.{var[0]}'

if 'lat1' in locals(): gif_file = gif_file+f'.{lat1}_{lat2}'

# lat1,lat2 = -60,60
# lat1,lat2 = -30,30
# lat1,lat2,lon1,lon2 = -0,25,130,180+30
# lat1,lat2,lon1,lon2 = -30,40,120,360-30
# lat1,lat2,lon1,lon2 = -15,15,50,120           # Indian Ocean 
# lat1,lat2,lon1,lon2 = 0,40,360-160,360-80     # East Pac
# lat1,lat2,lon1,lon2 = 0,40,360-90,360-10      # Atlantic/carribean
# lat1,lat2,lon1,lon2 = -30,0,360-90,360-60     # S. America
# lat1,lat2,lon1,lon2 = -45,0,90,180            # Australia
# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.animation.map.v1'+f'.{var[0]}.lat_{lat1}_{lat2}.lon_{lon1}_{lon2}'

var_x_case = True
num_plot_col = 2

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.cnConstFEnableFill           = True
res.lbOrientation                = 'Horizontal'
res.lbLabelFontHeightF           = 0.008

res.lbLabelBarOn                 = True
# res.mpPerimOn                    = False
# res.pmTickMarkDisplayMode        = "Never"

res.mpGridAndLimbOn              = False
res.mpCenterLonF                 = 180
res.mpLimitMode                  = 'LatLon' 
if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

# res.mpProjection = 'Mollweide'
res.mpProjection = 'Robinson'

if rotating_sphere: res.mpProjection = "Orthographic"

pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 10
pres.nglPanelXWhiteSpacePercent = 10


if 'create_png' not in locals(): create_png = False
if 'create_gif' not in locals(): create_gif = False
#---------------------------------------------------------------------------------------------------
# define function to add subtitles to the top of plot
#---------------------------------------------------------------------------------------------------
def set_subtitles(wks, plot, left_string='', center_string='', right_string='', font_height=0.015):
   ttres          = ngl.Resources()
   ttres.nglDraw  = False

   ### Use plot extent to call ngl.text(), otherwise you will see this error: 
   ### GKS ERROR NUMBER   51 ISSUED FROM SUBROUTINE GSVP  : --RECTANGLE DEFINITION IS INVALID
   strx = ngl.get_float(plot,'trXMinF')
   stry = ngl.get_float(plot,'trYMinF')
   ttres.txFontHeightF = font_height

   ### Set annotation resources to describe how close text is to be attached to plot
   amres = ngl.Resources()
   if not hasattr(ttres,'amOrthogonalPosF'):
      amres.amOrthogonalPosF = -0.52   # Top of plot plus a little extra to stay off the border
   else:
      amres.amOrthogonalPosF = ttres.amOrthogonalPosF

   ### Add left string
   amres.amJust,amres.amParallelPosF = 'BottomLeft', -0.5   # Left-justified
   tx_id_l   = ngl.text(wks, plot, left_string, strx, stry, ttres)
   anno_id_l = ngl.add_annotation(plot, tx_id_l, amres)
   # Add center string
   amres.amJust,amres.amParallelPosF = 'BottomCenter', 0.0   # Centered
   tx_id_c   = ngl.text(wks, plot, center_string, strx, stry, ttres)
   anno_id_c = ngl.add_annotation(plot, tx_id_c, amres)
   # Add right string
   amres.amJust,amres.amParallelPosF = 'BottomRight', 0.5   # Right-justified
   tx_id_r   = ngl.text(wks, plot, right_string, strx, stry, ttres)
   anno_id_r = ngl.add_annotation(plot, tx_id_r, amres)

   return
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# ds = xr.open_mfdataset( input_file_path, combine='by_coords' )
# ncol = ds['ncol']
# mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
# if 'lat1' in vars(): mask = mask & (ds['lat'].isel(time=0)>=lat1)
# if 'lat2' in vars(): mask = mask & (ds['lat'].isel(time=0)<=lat2)
# if 'lon1' in vars(): mask = mask & (ds['lon'].isel(time=0)>=lon1)
# if 'lon2' in vars(): mask = mask & (ds['lon'].isel(time=0)<=lon2)

def drop_rad_band(ds): 
      if 'lwband' in ds.variables: ds = ds.drop(['lwband'])
      if 'swband' in ds.variables: ds = ds.drop(['swband'])
      return ds

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
if create_png and num_case==1 :
   c = 0
   input_file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{case[c]}.eam.{htype}.*.nc'

   ds = xr.open_mfdataset( input_file_path, combine='by_coords', preprocess=drop_rad_band )

   ntime = len(ds['time'])
   
   print(f'  ntime = {ntime}')
   print(f'  time1 = {time1}')
   print(f'  time2 = {time2}')
   print(f'  dtime = {dtime}')
   if ntime<time2: exit('ERROR: time2 is too large!')
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
tfiles = ''
for t in range(time1,time2,dtime):
   fig_file = gif_file+'.'+str(t).zfill(3)
   tfiles = tfiles+'  '+f'{fig_file}.{fig_type}'
   tmp_path = os.getenv('HOME')+'/Research/E3SM/'
   short_fig_file = fig_file.replace(tmp_path,'')
   msg = f'  t: {t:3d}     {short_fig_file}.{fig_type}'
   msg = tcolor.GREEN + msg + tcolor.ENDC
   print(msg)


   if create_png :
      ### create the plot workstation
      wkres = ngl.Resources()
      wkres.wkWidth,wkres.wkHeight  = npix,npix
      wks = ngl.open_wks(fig_type,fig_file,wkres)
      plot = [None]*(num_var*num_case)

      ### rotating animation
      if rotating_sphere:
         # t_fac = float( (t-time1)/(time2-time1) )
         t_fac = float( t/time2 )
         # t_fac = float(t)/180.

         res.mpCenterLonF = 0.-t_fac*360.

      # print(f'  res.mpCenterLonF: {res.mpCenterLonF}')

      for c in range(num_case):
         #------------------------------------------------
         #------------------------------------------------
         if num_case>1:
            print('    case: '+case[c])
            scratch_path = os.getenv('HOME')+'/E3SM/scratch'
            input_file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{case[c]}.eam.{htype}.*.nc'

            files = sorted(glob.glob(input_file_path))
            # files = files[0]

            ### temporary fix for when a case is currently running and last file is incomplete
            # print(tcolor.RED+'WARNING: file list truncated to first 20 days!'+tcolor.ENDC)
            # input_file_path = f'{scratch_path}/{case[c]}/run/{case[c]}.eam.h1.0001-01-[0,1]*.nc'

            # ds = xr.open_mfdataset( input_file_path, combine='by_coords', preprocess=drop_rad_band )
            ds = xr.open_mfdataset( files, combine='by_coords', preprocess=drop_rad_band )
         #------------------------------------------------
         #------------------------------------------------
         ncol = ds['ncol']
         mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol.values)], dims='ncol' )
         # if 'lat1' in vars(): mask = mask & (ds['lat'].isel(time=0)>=lat1)
         # if 'lat2' in vars(): mask = mask & (ds['lat'].isel(time=0)<=lat2)
         # if 'lon1' in vars(): mask = mask & (ds['lon'].isel(time=0)>=lon1)
         # if 'lon2' in vars(): mask = mask & (ds['lon'].isel(time=0)<=lon2)
         for v in range(num_var):
            # print('  var: '+var[v])
            ip = v*num_case+c if var_x_case else c*num_var+v
            #-------------------------------------------------------------------------
            # read the data
            #-------------------------------------------------------------------------
            # data = ds[var[v]].isel(time=t).where(mask,drop=True)
            
            # if var[v]=='PRECT':
            #    data =        ds['PRECC'].isel(time=t).where(mask,drop=True)
            #    data = data + ds['PRECL'].isel(time=t).where(mask,drop=True)
            # if var[v]=='VORT850':
            #    U = ds['U850'].isel(time=t).where(mask,drop=True)
            #    V = ds['V850'].isel(time=t).where(mask,drop=True)
            if var[v]=='WSPDBOT':
               U = ds['UBOT'].isel(time=t).where(mask,drop=True)
               V = ds['VBOT'].isel(time=t).where(mask,drop=True)
               data = np.sqrt(U**2+V**2)
            elif var[v]=='WSPD850':
               U = ds['U850'].isel(time=t).where(mask,drop=True)
               V = ds['V850'].isel(time=t).where(mask,drop=True)
               data = np.sqrt(U**2+V**2)
            else:
               data = ds[var[v]].isel(time=t).where(mask,drop=True)

            if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3

            ### print temporal frequency and length
            # dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
            # print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

            ### Calculate time mean
            if 'time' in data.dims : data = data.mean(dim='time')

            ### Calculate area-weighted global mean
            # area = ds['area'].where( mask,drop=True)
            # gbl_mean = ( (data*area).sum() / area.sum() ).values 
            # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

            #-------------------------------------------------------------------------
            #-------------------------------------------------------------------------
            if plot_diff:
               if c==0:
                  data_baseline = data.copy()
               else:
                  data = data - data_baseline
            #-------------------------------------------------------------------------
            # Set colors and contour levels
            #-------------------------------------------------------------------------
            tres = copy.deepcopy(res)
            ### change the color map depending on the variable
            # tres.cnFillPalette = 'ncl_default'
            tres.cnFillPalette = 'MPL_viridis'
            # if var[v] in ['OMEGA']                    : tres.cnFillPalette = 'BlueWhiteOrangeRed'
            # if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
            # if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = 'CBR_wet'
            # if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = 'MPL_viridis'

            # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
            # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
            # tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
            

            ### specify specific contour intervals
            # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,40+1,1)
            if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(12,80+4,4)
            # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
            if var[v]=='LHFLX'               : tres.cnLevels = np.arange(10,400+10,10)
            if var[v]=="TGCLDLWP"            : tres.cnLevels = np.arange(1,100+1,1)*1e-2
            # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2 , 0.25, num=60)
            # if var[v]=="TGCLDIWP"            : tres.cnLevels = np.arange(2,20+2,2)*1e-2
            if var[v]=='TMQ'                 : tres.cnLevels = np.arange(10,100+1,1)
            # if var[v]=='TMQ'                 : tres.cnLevels = np.arange(80,600+5,5)/1e1
            if var[v]=='U850'                : tres.cnLevels = np.arange(-12,12+1,1)
            if var[v]=='UBOT'                : tres.cnLevels = np.arange(-20,20+1,1)
            if var[v]=='PS'                  : tres.cnLevels = np.arange(980,1026+1,1)*1e2
            if var[v]=='TS'                  : tres.cnLevels = np.arange(270,302+1,2)
            if var[v]=='SST'                 : tres.cnLevels = np.arange(272,304+1,1)

            if var[v]=='WSPDBOT': tres.cnLevels = np.arange(10,20+1,1)
            if var[v]=='WSPD850': tres.cnLevels = np.arange(10,20+1,1)

            ### use this for symmetric contour intervals
            if var[v] in ['U','V'] :
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
                                                          cint=None, max_steps=21,              \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=91)

            if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
            
            #-------------------------------------------------------------------------
            if plot_diff and c>0:
               tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
               if var[v]=='TS'  : tres.cnLevels = np.arange(-1,1+0.05,0.05)
               if var[v]=='SST' : tres.cnLevels = np.arange(-1,1+0.05,0.05)
            #-------------------------------------------------------------------------
            #-------------------------------------------------------------------------      
            if 'RCE' in case[c] or 'AQP' in case[c] :
               tres.mpOutlineBoundarySets = "NoBoundaries"
               tres.mpCenterLonF = 0.
            #-------------------------------------------------------------------------
            # Set up cell fill attributes using scrip grid file
            #-------------------------------------------------------------------------
            scripfile = xr.open_dataset(scrip_file_name)
            tres.cnFillMode    = 'CellFill'
            tres.sfXArray      = scripfile['grid_center_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
            tres.sfYArray      = scripfile['grid_center_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
            tres.sfXCellBounds = scripfile['grid_corner_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values 
            tres.sfYCellBounds = scripfile['grid_corner_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
            #-------------------------------------------------------------------------
            # Create plot
            #-------------------------------------------------------------------------
            # plot.append( ngl.contour_map(wks,data.values,tres) )
            # set_subtitles(wks, plot[len(plot)-1], name, '', var[v], font_height=0.01)

            if var[v]=='SST': data = xr.where(data<=-400,np.nan,data)

            plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data.values),tres)
            
            set_subtitles(wks, plot[ip], name[c], '', var[v],font_height=0.015)
            # set_subtitles(wks, plot[ip], '', name[c], '')
      #------------------------------------------------------------------------------------------------
      # Finalize plot
      #------------------------------------------------------------------------------------------------

      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      else:
         # layout = [num_var,num_case]
         # layout = [num_case,num_var]
         layout = [num_var,num_case] if var_x_case else [num_case,num_var]
         if num_case==4 and num_var==1: layout = [2,2]

      ngl.panel(wks,plot[0:len(plot)],layout,pres)
      # del wks

      ### trim white space from image using imagemagik
      fig_file = fig_file+'.'+fig_type
      os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
      # print('\n'+fig_file+'\n')
      ngl.destroy(wks)

      if single_frame: exit()

if create_png : ngl.end()
#---------------------------------------------------------------------------------------------------
# Create animated gif
#---------------------------------------------------------------------------------------------------
if create_gif :
   # cmd = "convert -repage 0x0 -delay 10 -loop 0  figs_scream/scream.animation.map.v1.*png   figs_scream/scream.animation.map.v1.gif"
   # cmd = f'convert -repage 0x0 -delay 10 -loop 0  {tfiles} {gif_file}.gif'
   cmd = f'convert +repage -delay 1 -loop 0  {tfiles} {gif_file}.gif'
   print('\n'+cmd+'\n')
   os.system(cmd)

   tmp_path = os.getenv('HOME')+'/Research/E3SM/'
   short_gif_file = gif_file.replace(tmp_path,'')
   print(f'\n  {short_gif_file}.gif'+'\n')
