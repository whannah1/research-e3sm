#---------------------------------------------------------------------------------------------------
# Create terms of total water budget filtered to remove grid imprinting
# new files are created that match the htype="h2" files, but with htype="hb" 
#---------------------------------------------------------------------------------------------------
import os, glob, ngl, datetime, sys
import xarray as xr, dask, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import gc
#---------------------------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,d=None,s=None,g=None):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); case_dir.append(d); case_sub.append(s); case_grid.append(g)
#---------------------------------------------------------------------------------------------------

add_case('E3SM.PI-CPL.v1.ne30.01',s=f'data_remap_90x180/')
add_case('E3SM.PI-CPL.v2.ne30.01',s=f'data_remap_90x180/')
# add_case('E3SM.PI-CPL.v1.ne30.01')
# add_case('E3SM.PI-CPL.v2.ne30.01')

# use_remap,remap_grid = False,'90x180' # 90x180 / 180x360

# lev = np.array([30,50,100,150,200,300,400,500,600,700,800,850,925,975,1000])
lev = np.array([50,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])

debug   = False

# first_file, num_file = 0, 1
# first_file, num_file = 0, 365*5
first_file, num_file = 0, 365*10

htype = 'h2'

print_stats = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)
for c in range(num_case):
   print('  case: '+case[c])

   data_dir_tmp,data_sub_tmp = None, None
   # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]

   case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir_tmp, data_sub=data_sub_tmp )

   file_path = getattr(case_obj,htype)
   file_list = sorted(glob.glob(file_path))

   # if 'num_file' in locals(): file_list = file_list[first_file:num_file]
   # file_list = file_list[first_file:num_file]
   file_list = file_list[first_file:first_file+num_file]

   # for f in file_list[:5]: print(f)
   # print('...')
   # for f in file_list[-5:]: print(f)
   # exit()
   
   cnt = 0
   for file_name in file_list : 

      file_out = file_name.replace(f'.{htype}.','.hb_mse.')
      # file_out = file_out.replace( os.path.dirname(file_out), '/project/projectdirs/m3312/whannah/'+case[c]+'/atm' )

      ps_file_name = file_name#.replace('.h2.','.h1.')

      def do_the_work(file_name,ps_file_name,file_out):
         global lev
         #-------------------------------------------------------------------------
         # load the data
         #-------------------------------------------------------------------------
         print('    Loading data...')

         ds = xr.open_dataset( file_name )

         # ds_h1 = xr.open_dataset( file_name.replace('.h2.','.h1.') )
         # print(); print(ds['QRL'])
         # print(); print(ds_h1['LHFLX'])
         # exit()

         # Correct timestamp? - shift by mean delta t
         # dtime = ds['time'].diff(dim='time').values.astype('timedelta64[h]').mean()
         # ds['time'] = ds.time.get_index('time') - datetime.timedelta(hours=dtime.astype(np.int)*0.5)

         lat  = ds['lat']
         lon  = ds['lon']
         area = ds['area']
         time = ds['time']

         # Dry static energy
         S = hc.cpd*ds['T' ].where( case_obj.get_mask(ds),drop=True) \
            +hc.g  *ds['Z3'].where( case_obj.get_mask(ds),drop=True) 
         S.attrs['long_name'] = 'Dry Static Energy'
         S.attrs['units']     = 'J/kg'

         # Moist static energy
         H = hc.cpd*ds['T' ].where( case_obj.get_mask(ds),drop=True) \
            +hc.g  *ds['Z3'].where( case_obj.get_mask(ds),drop=True) \
            +hc.Lv *ds['Q' ].where( case_obj.get_mask(ds),drop=True) 
         H.attrs['long_name'] = 'Moist Static Energy'
         H.attrs['units']     = 'J/kg'

         # vertical velocity for vertical advection calculation
         W = ds['OMEGA'].where( case_obj.get_mask(ds),drop=True)

         ds_ps = xr.open_mfdataset( ps_file_name )
         PS = ds_ps['PS']#.isel(time=slice(0,len(ds_ps['time']),3))
         
         #-------------------------------------------------------------------------
         # interpolate vertically
         #-------------------------------------------------------------------------
         def interpolate_vert(X,PS,ds,lev):
            PS_dum = PS.where( case_obj.get_mask(ds_ps),drop=True)
            PS_dum = PS_dum.expand_dims(dim='dummy',axis=len(ds_ps['PS'].dims)).values
            interp_type = 2         # 1 = LINEAR, 2 = LOG, 3 = LOG LOG
            extrap_flag = True
            P0  = ds['P0']  .values/1e2
            hya = ds['hyam'].values
            hyb = ds['hybm'].values
            if 'lat' in X.dims:
               X_new = xr.full_like(X.isel(lev=0),np.nan).drop('lev')
               X_new = X_new.expand_dims(dim={'lev':lev}, axis=X.get_axis_num('lev'))
               X_tmp = ngl.vinth2p( X.values, hya, hyb, lev, PS, \
                                    interp_type, P0, 1, extrap_flag)#[:,:,:,0]
            else:
               X_dum  = X.expand_dims(dim='dummy',axis=len(X.dims))
               X_new = xr.full_like(X.isel(lev=0),np.nan).drop('lev')
               X_new = X_new.expand_dims(dim={'lev':lev}, axis=X.get_axis_num('lev'))
               X_tmp = ngl.vinth2p( X_dum.values, hya, hyb, lev, PS_dum, \
                                    interp_type, P0, 1, extrap_flag)[:,:,:,0]
            X_tmp = np.where(X_tmp==1e30,np.nan,X_tmp)
            # X_new.data = dask.array.from_array( X_tmp, chunks=(1,X.shape[1],X.shape[2]) )
            X_new.data = X_tmp
            return X_new
         #-------------------------------------------------------------------------
         #-------------------------------------------------------------------------
         print('    Interpolating...')

         H = interpolate_vert(H,PS,ds,lev)
         S = interpolate_vert(S,PS,ds,lev)
         W = interpolate_vert(W,PS,ds,lev)

         # Convert lev coordinate to float
         W['lev'] = W['lev']*1.
         S['lev'] = S['lev']*1.
         H['lev'] = H['lev']*1.

         lev = H['lev']      # Recast lev as DataArray

         coords = H.coords
         dims   = H.dims 
         #----------------------------------------------------------------------------
         # Calculate pressure thickness for vertical integral (data is already on pressure levels)
         #----------------------------------------------------------------------------
         ps3d,pl3d = xr.broadcast(PS,lev*100.)

         if 'lat' in W.dims:
            pl3d = pl3d.transpose('time','lev','lat','lon')
            ps3d = ps3d.transpose('time','lev','lat','lon')
         else:
            pl3d = pl3d.transpose('time','lev','ncol')
            ps3d = ps3d.transpose('time','lev','ncol')

         nlev = len(lev)
         tvals = slice(0,nlev-2)
         cvals = slice(1,nlev-1)
         bvals = slice(2,nlev-0)
         
         # Calculate pressure thickness
         dp3d = xr.DataArray( np.full(pl3d.shape,np.nan), coords=pl3d.coords )
         dp3d[:,nlev-1,:] = ps3d[:,nlev-1,:].values - pl3d[:,nlev-1,:].values
         dp3d[:,cvals,:] = pl3d[:,bvals,:].values - pl3d[:,tvals,:].values

         # Deal with cases where levels are below surface pressure
         condition = pl3d[:,cvals,:].values<ps3d[:,cvals,:].values
         new_data  = ps3d[:,bvals,:].values-pl3d[:,tvals,:].values
         dp3d[:,cvals,:] = dp3d[:,cvals,:].where( condition, new_data )

         # Screen out negative dp values
         dp3d = dp3d.where( dp3d>0, np.nan )

         # hc.print_stat(dp3d)
         # exit()
         #-------------------------------------------------------------------------
         #-------------------------------------------------------------------------
         print('    Calculating derivatives...')

         # Temporarily convert lev to Pa
         H['lev'] = H['lev']*100.
         S['lev'] = S['lev']*100.

         # Calculate vertical derivative
         dHdp = H.differentiate(coord='lev')
         dSdp = S.differentiate(coord='lev')

         # Convert lev coord back to hPa
         H['lev'] = H['lev']/100.
         S['lev'] = S['lev']/100.

         dHdp['lev'] = dHdp['lev']/100.
         dSdp['lev'] = dSdp['lev']/100.

         # Set bottom and top levels to missing
         shape_tmp = dHdp[:,0,:].values.shape
         dHdp.values[:, 0,:] = np.full(shape_tmp,np.nan)
         dHdp.values[:,-1,:] = np.full(shape_tmp,np.nan)

         shape_tmp = dSdp[:,0,:].values.shape
         dSdp.values[:, 0,:] = np.full(shape_tmp,np.nan)
         dSdp.values[:,-1,:] = np.full(shape_tmp,np.nan)

         #-------------------------------------------------------------------------
         #-------------------------------------------------------------------------
         print('    Calculating budget terms...')

         W_dHdp = H.copy( data=(W.values*dHdp .values) )
         W_dSdp = S.copy( data=(W.values*dSdp .values) )

         # print()
         # hc.print_stat( W_dHdp*86400. , name='W_dHdp' )
         # hc.print_stat( W_dSdp*86400. , name='W_dSdp' )
         # print()
         
         W_dHdp.attrs['long_name'] = 'W_dHdp  '
         W_dHdp.attrs['units']     = 'J m2 s / kg2'

         W_dSdp.attrs['long_name'] = 'W_dSdp  '
         W_dSdp.attrs['units']     = 'J m2 s / kg2'

         #-------------------------------------------------------------------------
         # load total radiative heating for effective GMS
         #-------------------------------------------------------------------------   
         # QRT = ds['QRS'].where( case_obj.get_mask(ds),drop=True) \
         #      +ds['QRL'].where( case_obj.get_mask(ds),drop=True) 
         # QRT.attrs['long_name'] = 'Total Radiative Heating'
         # QRT.attrs['units']     = 'K/s'

         QRL = ds['QRL'].where( case_obj.get_mask(ds),drop=True)
         QRS = ds['QRS'].where( case_obj.get_mask(ds),drop=True)

         QRL = interpolate_vert(QRL,PS,ds,lev)
         QRS = interpolate_vert(QRS,PS,ds,lev)

         # print(); print(ds['QRS']); exit()

         if print_stats and cnt==0:
            hc.print_stat(QRL*hc.cpd,stat='naxsh',indent=(' '*6),compact=True,name='QRS*cpd')
            hc.print_stat(QRS*hc.cpd,stat='naxsh',indent=(' '*6),compact=True,name='QRL*cpd')
            # QRT = QRS + QRL
            # hc.print_stat(QRT,stat='naxsh',indent=(' '*6),compact=True,name='QRT')
            # exit()
      
         #-------------------------------------------------------------------------
         # load the data
         #-------------------------------------------------------------------------
         print('    Loading data...')

         ds = xr.open_dataset( file_name )

         #-------------------------------------------------------------------------
         # Mass weighted vertical integral
         #-------------------------------------------------------------------------
         print('    Calculating vertically integrated quantities...')

         dp3d_ci = dp3d.sum(dim='lev')

         H_ci      = ( H            * dp3d/hc.g ).sum(dim='lev')
         S_ci      = ( S            * dp3d/hc.g ).sum(dim='lev')
         W_dHdp_ci = ( W_dHdp       * dp3d/hc.g ).sum(dim='lev')
         W_dSdp_ci = ( W_dSdp       * dp3d/hc.g ).sum(dim='lev')
         QRL_ci    = ( QRL * hc.cpd * dp3d/hc.g ).sum(dim='lev') 
         QRS_ci    = ( QRS * hc.cpd * dp3d/hc.g ).sum(dim='lev') 

         dp3d_ci.attrs['long_name']   = 'column integrated dP'
         H_ci.attrs['long_name']      = 'column integrated Moist Static Energy'
         S_ci.attrs['long_name']      = 'column integrated Dry Static Energy'
         W_dHdp_ci.attrs['long_name'] = 'column integrated W_dHdp'
         W_dSdp_ci.attrs['long_name'] = 'column integrated W_dHdp'
         QRL_ci.attrs['long_name']    = 'column integrated QRL'
         QRS_ci.attrs['long_name']    = 'column integrated QRS'

         dp3d_ci.attrs['units']       = 'kg/m/s2'
         H_ci.attrs['units']          = 'J/m2'
         S_ci.attrs['units']          = 'J/m2'
         W_dHdp_ci.attrs['units']     = 'W/m2'
         W_dSdp_ci.attrs['units']     = 'W/m2'
         QRL_ci.attrs['units']        = 'W/m2'
         QRS_ci.attrs['units']        = 'W/m2'


         if print_stats and cnt==0:
            print()
            hc.print_stat(dp3d     ,stat='naxsh',indent=(' '*6),compact=True,name='dp3d     ')
            # hc.print_stat(W        ,stat='naxsh',indent=(' '*6),compact=True,name='W        ')
            # hc.print_stat(S        ,stat='naxsh',indent=(' '*6),compact=True,name='S        ')
            # hc.print_stat(H        ,stat='naxsh',indent=(' '*6),compact=True,name='H        ')
            hc.print_stat(W_dSdp   ,stat='naxsh',indent=(' '*6),compact=True,name='W_dSdp   ')
            hc.print_stat(W_dHdp   ,stat='naxsh',indent=(' '*6),compact=True,name='W_dHdp   ')
            hc.print_stat(QRL      ,stat='naxsh',indent=(' '*6),compact=True,name='QRL')
            hc.print_stat(QRS      ,stat='naxsh',indent=(' '*6),compact=True,name='QRS')
            print()
            hc.print_stat(W_dSdp_ci,stat='naxsh',indent=(' '*6),compact=True,name='W_dSdp_ci')
            hc.print_stat(W_dHdp_ci,stat='naxsh',indent=(' '*6),compact=True,name='W_dHdp_ci')
            hc.print_stat(QRL_ci   ,stat='naxsh',indent=(' '*6),compact=True,name='QRL_ci')
            hc.print_stat(QRS_ci   ,stat='naxsh',indent=(' '*6),compact=True,name='QRS_ci')
            exit()

         #-------------------------------------------------------------------------
         # Write to new file
         #-------------------------------------------------------------------------
         print('    Creating new dataset... ')

         # print('\nWriting filtered data to file: '+tmp_file+'\n')
         ds_out = xr.Dataset( coords=coords )

         # ds_out['W_dSdp'   ] = W_dSdp
         # ds_out['W_dHdp'   ] = W_dHdp
         ds_out['dp3d_ci'  ] = dp3d_ci
         ds_out['QRL_ci'   ] = QRL_ci
         ds_out['QRS_ci'   ] = QRS_ci
         ds_out['H_ci'     ] = H_ci
         ds_out['S_ci'     ] = S_ci  
         ds_out['W_dHdp_ci'] = W_dHdp_ci  
         ds_out['W_dSdp_ci'] = W_dSdp_ci  
         
         print('    writing to file: '+file_out)
         ds_out.to_netcdf(path=file_out,mode='w')

         del H, S, W, PS, ps3d
         del H_ci, S_ci
         del W_dHdp, W_dSdp
         del W_dHdp_ci, W_dSdp_ci
         del ds, ds_out
         gc.collect()
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      do_the_work(file_name,ps_file_name,file_out)
      sys.stdout.flush()
      cnt += 1
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if debug: 
         print()
         exit('EXITING for debugging purposes!\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
