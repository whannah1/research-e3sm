import os, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string
import cmocean
host = hc.get_host()

name,case,case_dir,case_sub,case_grid,scrip_file_list = [],[],[],[],[],[]
def add_case(case_in,n=None,d=None,s=None,g=None,scrip=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = case_in.replace('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.','')
      tmp_name = tmp_name.replace('.',' ')
      tmp_name = tmp_name.replace('_','=')
      if tmp_name=='control': tmp_name = 'fixed-HV nu=1e15 hvss=1 hvsq=6'
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name); case_dir.append(d); case_sub.append(s); case_grid.append(g)
   scrip_file_list.append(scrip)

scrip_path = os.getenv('HOME')+'/E3SM/data_grid'
#-------------------------------------------------------------------------------
# add_case('E3SM.PI-CPL.v1.ne30.01',  n='E3SMv1', scrip=f'{scrip_path}/ne30np4_scrip.nc')
# add_case('E3SM.PI-CPL.v2.ne30.01',  n='E3SMv2', scrip=f'{scrip_path}/ne30pg2_scrip.nc')
add_case('E3SM.PI-CPL.v1.ne30.01',  n='E3SMv1', scrip=f'{scrip_path}/cmip6_90x180_scrip.20181001.nc',s=f'data_remap_90x180')
add_case('E3SM.PI-CPL.v2.ne30.01',  n='E3SMv2', scrip=f'{scrip_path}/cmip6_90x180_scrip.20181001.nc',s=f'data_remap_90x180')
#-------------------------------------------------------------------------------

var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)
add_var('GMS')


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_budget/budget.GMS.map.v1'

lat1,lat2 = -50,50

htype,years,months,first_file,num_files = 'hb_mse',[1,2,3,4,5,6,7,8],[1,2,12],0,0
# htype,years,months,first_file,num_files = 'hb_mse',[1],[1],0,0
# htype,years,months,first_file,num_files = 'hb_mse',[],[],100,1

recalculate = True

var_x_case = True
num_plot_col = 2

use_common_label_bar = False
print_stats = True
stat_indent = ' '*6

# lev = -1
lev = np.array([50,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])

# top_lev = 26 # approx 100hPa for L72 starting from k=0

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var = 2

subtitle_font_height = 0.01

if 'scrip_file_path' not in locals(): scrip_file_path = None

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
# npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*(num_case))
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.018
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

def get_comp(case):
   comp = 'eam'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
v = 0
# for v in range(num_var):

hc.printline()
print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
nGMS_list = []
eGMS_list = []
# if 'lev_list' in locals(): lev = lev_list[v]
for c in range(num_case):
   print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

   if 'remap' in case_sub[c]:
      remap_grid = '90x180'
      use_remap = True
      remap_str = f'remap_{remap_grid}'

   data_dir_tmp,data_sub_tmp = None, None
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]

   case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir_tmp, data_sub=data_sub_tmp )
   # case_obj.set_coord_names(var[v])

   tmp_file_dir  = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/tmp_data'
   tmp_file = f'{tmp_file_dir}/budget.GMS.map.v1.{case[c]}.nc'

   if recalculate:
      #-------------------------------------------------------------------------
      # load data for GMS
      #-------------------------------------------------------------------------   
      # if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      area = case_obj.load_data('area', htype='h1', num_files=1,use_remap=use_remap,remap_str=remap_str)

      htype = 'hb_mse'

      W_dSdp_ci = case_obj.load_data('W_dSdp_ci', htype=htype, years=years, months=months, 
                                     first_file=first_file, num_files=num_files,
                                     use_remap=use_remap,remap_str=remap_str)
      W_dHdp_ci = case_obj.load_data('W_dHdp_ci', htype=htype, years=years, months=months, 
                                     first_file=first_file, num_files=num_files,
                                     use_remap=use_remap,remap_str=remap_str)

      hc.print_time_length(W_dSdp_ci['time'],indent=' '*6)

      if print_stats: hc.print_stat(W_dSdp_ci, stat='naxsh',indent=stat_indent,compact=True,name='W_dSdp_ci')
      if print_stats: hc.print_stat(W_dHdp_ci, stat='naxsh',indent=stat_indent,compact=True,name='W_dHdp_ci')

      GMS_norm = -1*W_dSdp_ci.mean(dim='time')

      nGMS = -1*W_dHdp_ci.mean(dim='time') / GMS_norm

      del W_dSdp_ci, W_dHdp_ci

      #-------------------------------------------------------------------------
      # load radiation for effective GMS - new method using hb_mse
      #-------------------------------------------------------------------------   
      htype = 'hb_mse'
      QRL_ci = case_obj.load_data('QRL_ci', htype=htype, years=years, months=months, 
                                 first_file=first_file, num_files=num_files,
                                 use_remap=use_remap,remap_str=remap_str)
      QRS_ci = case_obj.load_data('QRS_ci', htype=htype, years=years, months=months, 
                                 first_file=first_file, num_files=num_files,
                                 use_remap=use_remap,remap_str=remap_str)
      
      if print_stats: hc.print_stat(QRL_ci, stat='naxsh',indent=stat_indent,compact=True,name='QRL_ci')
      if print_stats: hc.print_stat(QRS_ci, stat='naxsh',indent=stat_indent,compact=True,name='QRS_ci')

      QRL_ci = QRL_ci.mean(dim='time')
      QRS_ci = QRS_ci.mean(dim='time')

      #-------------------------------------------------------------------------
      # Calculate dp for vertical integral - old
      #-------------------------------------------------------------------------
      # htype = 'h0'
      # # p0 = case_obj.load_data('P0',  htype=htype, num_files=1)
      # # ha = case_obj.load_data('hyai',htype=htype, num_files=1)
      # # hb = case_obj.load_data('hybi',htype=htype, num_files=1)
      # PS = case_obj.load_data('PS', htype=htype, years=years, months=months, 
      #                         first_file=first_file, num_files=num_files,
      #                         use_remap=use_remap,remap_str=remap_str)

      # lev_da = xr.DataArray(lev,dims=('lev',))

      # ps3d,pl3d = xr.broadcast(PS,lev_da*100.)

      # if print_stats: hc.print_stat(ps3d,stat='naxsh',indent=(' '*4),compact=True,name='ps3d')
      # if print_stats: hc.print_stat(pl3d,stat='naxsh',indent=(' '*4),compact=True,name='pl3d')

      # if 'ncol' in pl3d.dims: 
      #    pl3d = pl3d.transpose('time','lev','ncol')
      #    ps3d = ps3d.transpose('time','lev','ncol')
      # if 'lat'  in pl3d.dims: 
      #    pl3d = pl3d.transpose('time','lev','lat','lon')
      #    ps3d = ps3d.transpose('time','lev','lat','lon')

      # nlev = len(lev)
      # tvals = slice(0,nlev-2)
      # cvals = slice(1,nlev-1)
      # bvals = slice(2,nlev-0)
      
      # # Calculate pressure thickness
      # dp3d = xr.DataArray( np.full(pl3d.shape,np.nan), coords=pl3d.coords )
      # dp3d[:,nlev-1,:] = ps3d[:,nlev-1,:].values - pl3d[:,nlev-1,:].values
      # dp3d[:,cvals,:] = pl3d[:,bvals,:].values - pl3d[:,tvals,:].values

      # # Deal with cases where levels are below surface pressure
      # condition = pl3d[:,cvals,:].values<ps3d[:,cvals,:].values
      # new_data  = ps3d[:,bvals,:].values-pl3d[:,tvals,:].values
      # dp3d[:,cvals,:] = dp3d[:,cvals,:].where( condition, new_data )

      # del condition, new_data, PS, pl3d

      # # Screen out negative dp values
      # dp3d = dp3d.where( dp3d>0, np.nan )
      #-------------------------------------------------------------------------
      # load radiation for effective GMS - old method
      #-------------------------------------------------------------------------   
      # htype = 'h0'
      # QRL = case_obj.load_data('QRL', htype=htype, lev=lev, extrap_flag=True, 
      #                            years=years, months=months, 
      #                            first_file=first_file, num_files=num_files,
      #                            use_remap=use_remap,remap_str=remap_str)
      # QRS = case_obj.load_data('QRS', htype=htype, lev=lev, extrap_flag=True, 
      #                            years=years, months=months, 
      #                            first_file=first_file, num_files=num_files,
      #                            use_remap=use_remap,remap_str=remap_str)

      # QRT = QRL+QRS; del QRL,QRS

      # # QRT = (QRT*dp/9.81).sum(dim='lev') / (dp/9.81).sum(dim='lev')

      # # if print_stats: hc.print_stat(QRT, stat='naxsh',indent=(' '*4),compact=True,name='QRT pre-integration')

      # # QRT = ( QRT * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev') * hc.cpd
      # QRT = ( QRT * dp3d/hc.g ).sum(dim='lev') * hc.cpd

      # if print_stats: hc.print_stat(QRT, stat='naxsh',indent=(' '*4),compact=True,name='QRT post-integration')
      # if print_stats: hc.print_stat(dp3d,stat='naxsh',indent=(' '*4),compact=True,name='dp3d')

      # del dp3d

      # # QRT = QRT.resample(time='D').mean(dim='time')
      # QRT = QRT.mean(dim='time')

      #-------------------------------------------------------------------------
      # load sfc fluxes for effective GMS
      #-------------------------------------------------------------------------   
      if years==[]:
         htype = 'h1'
      else:
         htype = 'h0'

      LHF = case_obj.load_data('LHFLX', htype=htype, lev=lev, years=years, months=months, 
                                 first_file=first_file, num_files=num_files,
                                 use_remap=use_remap,remap_str=remap_str)
      SHF = case_obj.load_data('SHFLX', htype=htype, lev=lev, years=years, months=months, 
                                 first_file=first_file, num_files=num_files,
                                 use_remap=use_remap,remap_str=remap_str)

      if print_stats: hc.print_stat(LHF,stat='naxsh',indent=stat_indent,compact=True,name='LHF')
      if print_stats: hc.print_stat(SHF,stat='naxsh',indent=stat_indent,compact=True,name='SHF')

      # LHF = LHF.resample(time='D').mean(dim='time')
      # SHF = LHF.resample(time='D').mean(dim='time')
      LHF = LHF.mean(dim='time')
      SHF = SHF.mean(dim='time')

      #-------------------------------------------------------------------------
      # Calculate effectve GMS
      #-------------------------------------------------------------------------
      eGMS = nGMS + ( QRL_ci + QRS_ci + LHF + SHF ) / GMS_norm

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
      ds = xr.Dataset()
      ds['nGMS'] = nGMS
      ds['eGMS'] = eGMS

      print(f'    Writing to file: {tmp_file}')
      ds.to_netcdf(path=tmp_file,mode='w')
   else:
      print(f'    Reading file: {tmp_file}')
      ds = xr.open_mfdataset( tmp_file )
      nGMS = ds['nGMS']
      eGMS = ds['eGMS']

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   # # Get rid of lev dimension
   # if 'lev' in data.dims : data = data.isel(lev=0)

   if print_stats: hc.print_stat(nGMS,stat='naxsh',indent=stat_indent,compact=True,name='GMS')
   if print_stats: hc.print_stat(eGMS,stat='naxsh',indent=stat_indent,compact=True,name='eff GMS')

   # average over time dimension
   # if 'time' in GMS.dims : 
   #    hc.print_time_length(data.time,indent=' '*6)
   #    GMS = GMS.mean(dim='time')

   # Calculate area weighted global mean
   if 'area' in locals() :
      gbl_mean = ( (nGMS*area).sum() / area.sum() ).values 
      print(hc.tcolor.CYAN+f'      Area Weighted Global Mean nGMS : {gbl_mean:6.4}'+hc.tcolor.ENDC)
      gbl_mean = ( (eGMS*area).sum() / area.sum() ).values 
      print(hc.tcolor.CYAN+f'      Area Weighted Global Mean eGMS : {gbl_mean:6.4}'+hc.tcolor.ENDC)
   #----------------------------------------------------------------------------
   # append to data lists
   #----------------------------------------------------------------------------
   if 'lat'  in nGMS.dims: 
      nGMS = nGMS.stack(ncol=("lat", "lon"))
      eGMS = eGMS.stack(ncol=("lat", "lon"))

   nGMS_list.append( nGMS.values )
   eGMS_list.append( eGMS.values )

#---------------------------------------------------------------------------------------------------
# Plot averaged data
#---------------------------------------------------------------------------------------------------
# nGMS_min = np.min([np.nanmin(d) for d in nGMS_list])
# nGMS_max = np.max([np.nanmax(d) for d in nGMS_list])

for c in range(num_case):

   data_dir_tmp,data_sub_tmp = None, None
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]
   
   case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir_tmp, data_sub=data_sub_tmp )
   # case_obj.set_coord_names()

   # ip = v*num_case+c if var_x_case else c*num_var+v
   #----------------------------------------------------------------------------
   # Set colors
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   # tres.cnFillPalette = "MPL_viridis"
   # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
   tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )

   #----------------------------------------------------------------------------
   # Set explicit contour levels
   #----------------------------------------------------------------------------
   # tres.cnLevels = np.linspace(-10,10,21)
   tres.cnLevels = np.arange(-10,10+1,1) / 1e1
   
   #----------------------------------------------------------------------------
   # set non-explicit contour levels
   #----------------------------------------------------------------------------
   if hasattr(tres,'cnLevels') : 
      tres.cnLevelSelectionMode = 'ExplicitLevels'
   # else:
   #    nlev = 21
   #    aboutZero = True
   #    clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
   #                                    returnLevels=False, aboutZero=aboutZero )
   #    if clev_tup is not None: 
   #       cmin,cmax,cint = clev_tup
   #       tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
   #       tres.cnLevelSelectionMode = 'ExplicitLevels'
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   if use_common_label_bar: 
      tres.lbLabelBarOn = False
   else:
      tres.lbLabelBarOn = True

   hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_list[c])

   def get_pid(c,v): return v*num_case+c if var_x_case else c*num_var+v

   plot[get_pid(c,0)] = ngl.contour_map(wks,nGMS_list[c],tres) 
   plot[get_pid(c,1)] = ngl.contour_map(wks,eGMS_list[c],tres) 

   #----------------------------------------------------------------------------
   # set plot subtitles
   #----------------------------------------------------------------------------

   hs.set_subtitles(wks, plot[get_pid(c,0)], name[c], '', '', font_height=subtitle_font_height)
   hs.set_subtitles(wks, plot[get_pid(c,1)], name[c], '', '', font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [num_var,num_case] if var_x_case else [num_case,num_var]

# if not (plot_diff and add_diff):
#    if num_case==1 or num_var==1:
#       layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()
if use_common_label_bar: pnl_res.nglPanelLabelBar = True
pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
