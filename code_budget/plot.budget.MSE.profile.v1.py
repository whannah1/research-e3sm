import os, ngl, copy
import xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
home = os.getenv('HOME')
data_dir,data_sub = None,None
print()

case,name,clr,dsh,mrk = [],[],[],[],[]
def add_case(case_in,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)

var,vclr,vdsh = [],[],[]
def add_var(var_name,clr='black',dsh=0): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh)

# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([50,100,150,200,300,400,500,600,700,750,800,850,875,900,925,975])
# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])

### INCITE2021
# case.append(f'ERAi');name.append('ERAi')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.01',       n='MMF 2D'   ,c='black')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.MOMFB.01', n='MMF 2D+MF',c='red')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.01',      n='MMF 3D'   ,c='green')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.01',n='MMF 3D+MF',c='blue')


### rotating RCE
add_case(f'E3SM.ne30pg2.F-MMFXX-RCEROT.BVT.01',n='MMF RCEROT',c='black')


# add_var('T')
# add_var('Q')
# add_var('U')
# add_var('OMEGA')
# add_var('THETA')
# add_var('CLOUD')
# add_var('CLDLIQ')
# add_var('CLDICE')
# add_var('QRL')
# add_var('QRS')
# add_var('U')
# add_var('V')
# add_var('O3')




# add_var('QRL','blue');add_var('QRS','red')

# add_var('TOT_DDSE','blue')
# add_var('TOT_DQLV','green')
# add_var('QRS','red')

# add_var('DMSE_TOT','black')
# add_var('DMSE_DYN','orange')
# add_var('DMSE_CRM','blue')
# add_var('DMSE_RAD','red')
# add_var('DMSE_GWD','pink')#; add_var('DDSE_GWD','red'); add_var('DQLV_GWD','blue')
# add_var('DMSE_PBL','purple')
# add_var('DMSE_RAY','gray')
# add_var('DDSE_RAY','gray')
# add_var('DMSE_CEF','lightblue')
# add_var('DMSE_RES','magenta')

# add_var('DDSE_QRS','green')
# add_var('DDSE_QRL','blue')
# add_var('DMSE_RAD','red')
# add_var('DDSE_MMF','purple')
# add_var('DDSE_CRM','pink',dsh=1)

# add_var('DMSE_PAC','red')
# add_var('DMSE_PAC1','blue',dsh=1)
# add_var('DMSE_PAC2','green',dsh=1)

# add_var('DMSE_PBC','blue')
# add_var('DMSE_PBC1','blue',dsh=1)
# add_var('DMSE_PBC2','blue',dsh=2)

add_var('DMSE_MMF','purple')
add_var('DMSE_MMF2','purple',dsh=1)

# add_var('DMSE_PAC','red')
# add_var('DMSE_PBC','blue')
add_var('DMSE_RES','green')


### these are all zeros
# add_var('DMSE_DME','pink')
# add_var('DMSE_DAD','purple')
# add_var('DMSE_QN3','lightgreen')



num_plot_col = 1#len(var)



fig_type = "png"
fig_file = home+"/Research/E3SM/figs_budget/budget.MSE.profile.v1"


# lat1,lat2 = -10,10
# lat1,lat2 = 45,90
# lat1,lat2 = 30,60
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50
# lat1,lat2,lon1,lon2 = -30,-20,360-110,360-80
# lat1,lat2,lon1,lon2 = 18,22.5,169,174

xlat,xlon,dy,dx = 60,120,10,10;
# xlat,xlon,dy,dx = 0,180,20,20;
# xlat,xlon,dy,dx = -40,180,2,2;
# if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

# lat1,lat2 = -90,-70
# lat1,lat2,lon1,lon2 = -60,90,0,360


# htype,years,months,first_file,num_files = 'h0',[],[],0,3
# htype,years,months,first_file,num_files = 'h2',[],[],0,90
htype,years,months,first_file,num_files = 'h1',[],[],0,1

plot_diff = False

overlay_vars = True

use_snapshot,ss_t  = False,3
use_height_coord   = False

omit_bot,bot_k     = False,20
omit_top,top_k     = False,10

print_stats        = True
print_profile      = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'diff_mode' not in locals(): diff_mode = 0

if 'name' not in locals() or name==[]: 
   name = [None]*(num_case)

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix=2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
# plot = [None]*(num_var*num_case)
if overlay_vars: 
   plot = [None]*(num_case)
else:
   plot = [None]*(num_var)
res = hs.res_xy()
res.vpWidthF = 0.4

# res.xyMarkLineMode = "MarkLines"
# res.xyMarkerSizeF = 0.008
# res.xyMarker = 16

# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

# res.xyLineThicknessF = 10

if not use_height_coord: 
   res.trYReverse = True
   # res.xyYStyle = 'Log'


# res.trXMinF = 12

# if use_height_coord:
   # res.trYMaxF = 30e3
   # res.trYMinF = 10e3
# else:
#    res.trYMaxF = 100

# if not use_height_coord: res.trYMinF = 800

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(hc.tcolor.GREEN+'  var: '+var[v]+hc.tcolor.ENDC)
   data_list,lev_list = [],[]
   for c in range(num_case):
      print(hc.tcolor.CYAN+'    case: '+case[c]+hc.tcolor.ENDC)
      # case_obj = he.Case( name=case[c], data_dir='/ccs/home/hannah6/E3SM/scratch/old_INCITE2020_cases/' )
      case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub  )
      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      area_name = 'area'
      # if 'DYN_' in tvar : 
      #    if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
      #    if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
      #    if 'pg2' in case[c] or 'pg3' in case[c] : 
      #       case_obj.ncol_name = 'ncol_d'
      #       area_name = 'area_d'

      
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      tnum_files = num_files

      # LWP = case_obj.load_data('TGCLDLWP',htype=htype,first_file=first_file,num_files=tnum_files).mean(dim='time')
      
      if use_height_coord: 
         Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=tnum_files)

      area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=tnum_files,).astype(np.double)
      X    = case_obj.load_data(tvar,     htype=htype,years=years,months=months,first_file=first_file,num_files=tnum_files,lev=lev)

      # print(hc.tcolor.RED+'WARNING: only using a single column!'+hc.tcolor.ENDC)
      # X    =    X.isel(ncol=slice(0,1))
      # area = area.isel(ncol=slice(0,1))

      # hc.print_stat(X,name=f'{var[v]} before averaging',indent='    ')

      # print('WARNING: only using the first two times')
      # X = X.isel(time=slice(0,2))

      if len(X.time)>1:
        if htype=='h0':
           dtime = ( X['time'][-1] - X['time'][0] + (X['time'][1]-X['time'][0]) ).values.astype('timedelta64[M]') + 1
           print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[D]'))+')')
        else:
           dtime = ( X['time'][-1] - X['time'][0] ).values.astype('timedelta64[D]')+1
           print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

      # if use_snapshot or len(X.time)==1:
      if use_snapshot:
         X = X.isel(time=ss_t)
         if use_height_coord: Z = Z.isel(time=ss_t)
      else:
         X = X.mean(dim='time')
         if use_height_coord: Z = Z.mean(dim='time')


      if case[c]=='ERA5':
         X = ( (X*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )#.values 
      else:
         if use_height_coord: Z = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') )#.values 
         X = ( (X*area).sum(dim='ncol') / area.sum(dim='ncol') )#.values 

      # top_lev = 60
      # X = X[top_lev:]

      # if use_height_coord: Z = Z[top_lev:]
      # Z = Z[top_lev:]

      if omit_bot:
         X = X[:bot_k]
         if use_height_coord: Z = Z[:bot_k]

      if omit_top:
         X = X[top_k:]
         if use_height_coord: Z = Z[top_k:]

      if print_stats:
         hc.print_stat(X,name=f'{var[v]} after averaging',indent='    ',compact=True)
         # hc.print_stat(Z,name=f'Z after averaging',indent='    ')

      if print_profile:
         print()
         for xx in X.values: print(f'    {xx}')
         print()

      # gbl_mean = ( (X*area).sum() / area.sum() ).values 
      # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

      data_list.append( X.values )
      if use_height_coord:
         lev_list.append( Z.values )
      else:
         lev_list.append( X['lev'].values )

   data_list_list.append(data_list)
   lev_list_list.append(lev_list)

#-------------------------------------------------------------------------------
# Create plot 1 - overlay all vars for each case
#-------------------------------------------------------------------------------
if overlay_vars:
   for c in range(num_case):
      ip = c
      tres = copy.deepcopy(res)
      data_min = np.min( data_list_list[0][c] )
      data_max = np.max( data_list_list[0][c] )
      for v in range(num_var):
         data_min = np.min([ data_min, np.nanmin(data_list_list[v][c]) ])
         data_max = np.max([ data_max, np.nanmax(data_list_list[v][c]) ])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      
      for v in range(num_var):
         tres.xyLineColor   = vclr[v]
         tres.xyMarkerColor = vclr[v]
         tres.xyDashPattern = vdsh[v]

         tplot = ngl.xy(wks, data_list_list[v][c], lev_list_list[v][c], tres)
         
         # nlev = len(lev_list_list[v][c])
         # print(hc.tcolor.RED+'WARNING - using level index instead of level coordinate!'+hc.tcolor.ENDC)
         # tplot = ngl.xy(wks, data_list_list[v][c], np.arange(1,nlev), tres)

         if v==0 :
            plot[ip] = tplot
         else:
            ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''

      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      hs.set_subtitles(wks, plot[ip], name[c], ctr_str, '', font_height=0.01)
     
#-------------------------------------------------------------------------------
# Create plot 2 - overlay all cases for each var
#-------------------------------------------------------------------------------
if not overlay_vars:
   for v in range(num_var):
      data_list = data_list_list[v]
      lev_list = lev_list_list[v]
      
      tres = copy.deepcopy(res)

      # ip = v*num_case+c
      ip = c*num_var+v
      
      baseline = data_list[0]
      if diff_mode==1 :
         baseline1 = data_list[0]
         baseline2 = data_list[1]
      if diff_mode==2 :
         baseline1 = data_list[0]
         baseline2 = data_list[2]
      if plot_diff:
         for c in range(num_case): 
            if diff_mode==1 :
               if c==0 or c==2 : baseline = baseline1
               if c==1 or c==3 : baseline = baseline2
            if diff_mode==2 :
               if c==0 or c==1 : baseline = baseline1
               if c==2 or c==3 : baseline = baseline2
            data_list[c] = data_list[c] - baseline

      
      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      ip = v

      for c in range(num_case):
         tres.xyLineColor = clr[c]
         tres.xyMarkerColor = clr[c]
         tres.xyDashPattern = dsh[c]

         tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)

         if diff_mode==0 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c>0) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==1 :
            if (c==2 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=1) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==2 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=2) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''
      var_str = var[v]


      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      if plot_diff: var_str += ' (diff)'

      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)
      hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str, font_height=0.01)


#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
lgres = ngl.Resources()
lgres.vpWidthF           = 0.05
lgres.vpHeightF          = 0.08
lgres.lgLabelFontHeightF = 0.012
lgres.lgMonoDashIndex    = True
lgres.lgLineLabelsOn     = False
lgres.lgLineThicknessF   = 8
lgres.lgLabelJust        = 'CenterLeft'
lgres.lgLineColors       = clr
lgres.lgDashIndexes      = dsh

lx,ly = 0.5,0.45
if num_var==2: lx,ly = 0.3,0.45
if num_var==4: lx,ly = 0.05,0.5

# pid = ngl.legend_ndc(wks, len(name), name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [1,len(plot)]
# layout = [int(np.ceil(num_var/4.)),4]
# layout = [num_var,num_case]
# layout = [num_case,num_var]
# layout = [1,num_var]

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]


# if num_var==1  : layout = [int(np.ceil(num_case/2.)),2]
# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]

# if num_case==1 and num_var==4 : layout = [2,2]
# if num_case==1 and num_var==6 : layout = [3,2]

#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------