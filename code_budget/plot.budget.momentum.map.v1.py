import os, copy, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
host = hc.get_host()
data_dir,data_sub = None,None
#-------------------------------------------------------------------------------

### INCITE2021
name,case = [],[]
case.append(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.MOMFB.00'); name.append('E3SM-MMF 2D')
case.append(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.00');name.append('E3SM-MMF 3D')
   
#-------------------------------------------------------------------------------

var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)

# add_var('TOT_DU')
# add_var('DYN_DU')
# add_var('MMF_DU')
# add_var('DUV')
# add_var('RAY_DU')
# add_var('GWD_DU')
# add_var('TAUTMSX')
# add_var('RESIDUAL_DU')

# add_var('TOT_DV')
# add_var('DYN_DV')
# add_var('MMF_DV')
# add_var('DVV')
# add_var('RAY_DV')
# add_var('GWD_DV')
# add_var('TAUTMSX')
# add_var('RESIDVAL_DV')

add_var('DKE_TOT')
add_var('DKE_DYN')
add_var('DKE_MMF')
add_var('DKE_VDF')
add_var('DKE_GWD')
add_var('DKE_RAY')
add_var('DKE_TMS')
add_var('DKE_RES')

del lev_list
# lev = -71
lev = 850

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_budget/budget.momentum.map.v1'

# lat1,lat2 = -60,60

htype,years,months,first_file,num_files = 'h1',[],[],0,0

use_remap,remap_grid = False,'180x360' # 90x180 / 180x360

plot_diff,add_diff = False,False
diff_base = 0

print_stats = True
var_x_case = False
num_plot_col = 2
use_snapshot,ss_t = False,28

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.02 - 0.0012*num_var - 0.0014*(num_case+int(add_diff))
subtitle_font_height = max(subtitle_font_height,0.005)

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1, res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1, res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.018
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   std_list,cnt_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_sub_tmp = data_sub
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      
      case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub_tmp )
      # case_obj.set_coord_names(var[v])

      case_obj.htype = htype
      case_obj.lev = lev
      case_obj.years = years
      case_obj.months = months
      case_obj.first_file = first_file
      case_obj.num_files = num_files
      case_obj.use_remap = use_remap
      case_obj.remap_str = f'remap_{remap_grid}'

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if use_remap:
         lat = case_obj.load_data('lat',htype=htype,use_remap=use_remap,remap_str=f'remap_{remap_grid}')
         lon = case_obj.load_data('lon',htype=htype,use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      else:
         aname = case_obj.area_name
         area = case_obj.load_data(aname,htype=htype)

      data = case_obj.load_data(var[v],htype=htype)
      
      # data = data * 84600.

      # print(case_obj.lev)
      # print(data)
      # hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')
      # exit()

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         if use_snapshot:
            data = data.isel(time=ss_t)
            print(hc.tcolor.RED+'WARNING - plotting snapshot!!!'+hc.tcolor.ENDC)
         else:
            data = data.mean(dim='time')

      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      if case[c]=='TRMM' and 'lon1' not in locals(): 
         data_list.append( ngl.add_cyclic(data.values) )
      else:
         data_list.append( data.values )

      if 'area' in locals() : area_list.append( area.values )
      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub )
      # case_obj.set_coord_names(var[v])

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      tres.cnFillPalette = "BlueWhiteOrangeRed"
      
      # tres.cnLevels = np.linspace(-1,1,21)*0.6
      # tres.cnLevels = np.logspace( -7, -3, num=60)
      # tres.cnLevels = np.arange(-20,20+1,1)
      
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 21
         aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      var_str = var[v]

      lev_str = None
      if lev>0: lev_str = f'{lev}mb'
      if lev<0: lev_str = f'k={(lev*-1)}'
      if lev_str is not None: var_str = f'{lev_str} {var[v]}'

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_remap or case_obj.obs :
         if case[c]=='ERAi': lat,lon = erai_lat,erai_lon
         hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
      else:
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype)
         
      if plot_diff and c==diff_base : base_name = name[c]

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         #----------------------------------------------------------------------
         # set plot subtitles
         #----------------------------------------------------------------------
         ctr_str = ''
         if 'name' not in vars(): case_name = case_obj.short_name
         if 'name'     in vars(): case_name = name[c]

         hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff :
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         
         ### override the level settings and just use auto
         # tres.cnLevelSelectionMode = "AutomaticLevels"

         if use_remap:
            hs.set_cell_fill(tres,case_obj=case_obj,use_remap=True,lat=lat,lon=lon)
         else:
            hs.set_cell_fill(tres,case_obj=case_obj)

         tres.lbLabelBarOn = True

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+1
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)

         #-----------------------------------
         ####################################
         #-----------------------------------
         ctr_str = ''
         # case_name = name[c]+' - '+base_name
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name
         
         # hs.set_subtitles(wks, plot[ipd], case_name, '', var_str+' (Diff)', font_height=subtitle_font_height)
         hs.set_subtitles(wks, plot[ipd], case_name, 'Difference', var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if plot_diff : num_case = num_case+len(diff_case)   # use this to plot both before and after diff


num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]

if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
