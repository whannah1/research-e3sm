#---------------------------------------------------------------------------------------------------
# create a tuning "score card" full of helpful plots
# commands for regridding CERES-EBAF to model grid:
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne30pg2_scrip.nc 
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne45pg2_scrip.nc 
# cd /gpfs/alpine/scratch/hannah6/cli115/Obs/CERES-EBAF
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne30pg2/${FILE}
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne45pg2/${FILE}
#---------------------------------------------------------------------------------------------------
import os, ngl, xarray as xr, numpy as np, glob, copy
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
np.seterr(divide='ignore', invalid='ignore')
np.errstate(divide='ignore', invalid="ignore")
#---------------------------------------------------------------------------------------------------
case1_list, case2_list, name_list = [],[],[]
case_root, case_sub = [],[]
def add_case(case1,case2,n=None,p=None,s=None):
   case1_list.append(case1); case2_list.append(case2); name_list.append(n)
   case_root.append(p); case_sub.append(s)
#---------------------------------------------------------------------------------------------------


tmp_scratch,tmp_sub = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/','run'

case1 = 'E3SM.2024-AQP-CESS-00.FAQP-MMF1.GNUGPU.ne30pg2_ne30pg2.NN_32.SSTP_0K'
case2 = 'E3SM.2024-AQP-CESS-00.FAQP-MMF1.GNUGPU.ne30pg2_ne30pg2.NN_32.SSTP_4K'
add_case(case1,case2, n='MMF ne30',p=tmp_scratch,s=tmp_sub)

case1 = 'E3SM.2024-AQP-CESS-00.FAQP-MMF1.GNUGPU.ne120pg2_ne120pg2.NN_512.SSTP_0K'
case2 = 'E3SM.2024-AQP-CESS-00.FAQP-MMF1.GNUGPU.ne120pg2_ne120pg2.NN_512.SSTP_4K'
add_case(case1,case2, n='MMF ne120',p=tmp_scratch,s=tmp_sub)

case1 = 'E3SM.2024-AQP-CESS-00.FAQP.GNUCPU.ne30pg2_ne30pg2.NN_32.SSTP_0K'
case2 = 'E3SM.2024-AQP-CESS-00.FAQP.GNUCPU.ne30pg2_ne30pg2.NN_32.SSTP_4K'
add_case(case1,case2, n='EAM ne30',p=tmp_scratch,s=tmp_sub)

case1 = 'E3SM.2024-AQP-CESS-00.FAQP.GNUCPU.ne120pg2_ne120pg2.NN_512.SSTP_0K'
case2 = 'E3SM.2024-AQP-CESS-00.FAQP.GNUCPU.ne120pg2_ne120pg2.NN_512.SSTP_4K'
add_case(case1,case2, n='EAM ne120',p=tmp_scratch,s=tmp_sub)


htype,first_file,num_files = 'h0',int(150/10),int(150/10)
# htype,first_file,num_files = 'h0',int(200/10),int(200/10)


#-------------------------------------------------------------------------------
def load_data(var,case,case_root,case_sub):
   global htype,first_file,num_files
   file_list = sorted(glob.glob(f'{case_root}/{case}/{case_sub}/*eam.{htype}*'))
   file_list = file_list[first_file:first_file+num_files]
   ds = xr.open_mfdataset(file_list)
   gbl_mean = ( (ds[var]*ds['area']).sum() / ds['area'].sum() ).values 
   return gbl_mean
#-------------------------------------------------------------------------------
print()
for c in range(len(case1_list)):
   T1    = load_data('TS'  ,case1_list[c],case_root[c],case_sub[c])
   FSNT1 = load_data('FSNT',case1_list[c],case_root[c],case_sub[c])
   FLNT1 = load_data('FLNT',case1_list[c],case_root[c],case_sub[c])
   T2    = load_data('TS'  ,case2_list[c],case_root[c],case_sub[c])
   FSNT2 = load_data('FSNT',case2_list[c],case_root[c],case_sub[c])
   FLNT2 = load_data('FLNT',case2_list[c],case_root[c],case_sub[c])

   N1 = FSNT1 - FLNT1
   N2 = FSNT2 - FLNT2

   ECS = ( N2 - N1 ) / ( T2 - T1 )

   msg = f'{name_list[c]:10}'
   msg+= f'  ECS : {ECS:8.4f}  '
   msg+= f'  N2: {N2:8.2f}  N1: {N1:8.2f}  dN: {(N2-N1):8.4f}'
   msg+= f'  T2: {T2:8.2f}  T1: {T1:8.2f}  dT: {(T2-T1):8.4f}'
   print(msg)
#-------------------------------------------------------------------------------
print()


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------