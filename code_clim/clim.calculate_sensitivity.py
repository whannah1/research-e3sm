#---------------------------------------------------------------------------------------------------
# create a tuning "score card" full of helpful plots
# commands for regridding CERES-EBAF to model grid:
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne30pg2_scrip.nc 
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne45pg2_scrip.nc 
# cd /gpfs/alpine/scratch/hannah6/cli115/Obs/CERES-EBAF
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne30pg2/${FILE}
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne45pg2/${FILE}
#---------------------------------------------------------------------------------------------------
import os, ngl, xarray as xr, numpy as np, glob, copy
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
np.seterr(divide='ignore', invalid='ignore')
np.errstate(divide='ignore', invalid="ignore")
#---------------------------------------------------------------------------------------------------
ref_flag,ctl_flag = [],[]
case,name,clr,dsh,mrk = [],[],[],[],[]
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,m=1,c='black',ctl=False,ref=False):
   global name,case,clr,dsh,mrk
   if n is None: n = case_in
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   ref_flag.append(ref); ctl_flag.append(ctl)
#---------------------------------------------------------------------------------------------------




### 4xCO2 tests on Summit
data_dir = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/'
# data_sub = 'data_remap_90x180'
data_sub = 'run'
case1 = 'E3SM.2023-CO2-TEST-00.GNUGPU.ne30pg2_oECv3.WCYCL1850-MMF1.1xCO2'
case2 = 'E3SM.2023-CO2-TEST-00.GNUGPU.ne30pg2_oECv3.WCYCL1850-MMF1.4xCO2'

yr1,yr2 = 10,20

scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
scrip_ds = xr.open_dataset(scrip_file_path)

#-------------------------------------------------------------------------------
# read the data
#-------------------------------------------------------------------------------

for c,case in enumerate([case1,case2]):
   file_list = sorted(glob.glob(f'{data_dir}/{case}/{data_sub}/*eam.h0*'))
   f1,f2 = yr1*12,yr2*12
   file_list = file_list[f1:f2]
   ds = xr.open_mfdataset(file_list,combine='by_coords',concat_dim='time')
   area = ds['area']
   data = ds['TS']
   area.load()
   data.load()
   gbl_mean = ( (data*area).sum() / area.sum() ).values 
   del data,area

   if c==0: gbl_mean1 = gbl_mean
   if c==1: gbl_mean2 = gbl_mean

delta = gbl_mean2 - gbl_mean1

ECS = delta / 2

print()
print(f'glb ctl : {gbl_mean1}')
print(f'glb exp : {gbl_mean2}')
print(f'ECS     : {ECS}')
print()



#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------