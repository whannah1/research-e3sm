import ngl, os, copy, xarray as xr, numpy as np, glob, pandas
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = '' if n is None else n
   case.append(case_in)
   case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

tmp_scratch,tmp_sub = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch','archive/atm/hist'
add_case('v3.LR.amip_0101',n='E3SMv3',s=tmp_sub,p=tmp_scratch)

htype = 'h0'

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.annual_cycle.v1','png'

#---------------------------------------------------------------------------------------------------
num_var = 1#len(var)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_case*num_var)
res = hs.res_xy()
res.vpHeightF = 0.3
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
res.tmYLLabelFontHeightF = 0.015
res.tmXBLabelFontHeightF = 0.015
res.tiYAxisFontHeightF = 0.02
res.tiXAxisFontHeightF = 0.02

#---------------------------------------------------------------------------------------------------
num_case = len(case)
v = 0
for c in range(num_case):

   print(' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)

   #----------------------------------------------------------------------------
   file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}'
   # file_list = sorted(glob.glob(f'{file_path}/{case[c]}.eam.h0*'))
   file_list = sorted(glob.glob(f'{file_path}/{case[c]}.eam.h0.2*'))
   # file_list = file_list[:12*2]
   #----------------------------------------------------------------------------
   # print()
   # for f in file_list: print(f)
   # print()
   #----------------------------------------------------------------------------
   ds = xr.open_mfdataset( file_list )

   month = ( ds.time.dt.month.values - 1 )%12
   month = np.where(month==0,12,month)

   num_years = int(len(month)/12)

   # print()
   # print(month)
   # print()
   # print(f'num_years: {num_years}')
   # print()
   # exit()

   # data = ds['SW_flux_dn_at_model_top'] - ds['SW_flux_up_at_model_top'] - ds['LW_flux_up_at_model_top']
   data = ds['FSNT'] - ds['FLNT']

   area = ds['area']
   data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )

   data = data.values
   #----------------------------------------------------------------------------
   # plot all data to set axis limits
   tres = copy.deepcopy(res)
   tres.xyLineColor   = -1
   tres.xyDashPattern = 0

   ip = v*num_case+c
   plot[ip] = ngl.xy(wks, month, data, tres)
   #----------------------------------------------------------------------------
   tres.xyLineThicknessF = 2
   tres.xyLineColor = 'blue'

   for y in range(num_years):
      t1 = (y+0)*12
      t2 = (y+1)*12
      ngl.overlay(plot[ip],ngl.xy(wks, month[t1:t2], data[t1:t2], tres))

   #----------------------------------------------------------------------------
   # overlay climatological seasoncycle
   data_mean = np.zeros(12)
   mnth_mean = np.arange(1,12+1)
   for m in range(12):
      data_mean[m] = np.mean(data[m::12])
   tres.xyLineThicknessF = 8
   tres.xyLineColor = 'black'
   ngl.overlay(plot[ip],ngl.xy(wks, month[t1:t2], data[t1:t2], tres))
   #----------------------------------------------------------------------------
   # horizontal line at zero
   tres.xyLineThicknessF = 1
   tres.xyLineColor = 'gray'
   ngl.overlay(plot[ip],ngl.xy(wks, np.array([-1,1])*1e3, np.array([0,0]), tres))
   
#---------------------------------------------------------------------------------------------------

num_plot_col = 1
layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pres = hs.setres_panel()

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
