import os, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs

case,name,clr,dsh,mrk = [],[],[],[],[]
case_dir,case_sub = [],[]
def add_case(case_in,p=None,s=None,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
   case_dir.append(p); case_sub.append(s)

### reduced radiation sensitivity
add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00', n='nx_rad=128',c='red')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_64.00',  n='nx_rad=64' ,c='orange')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_32.00',  n='nx_rad=32' ,c='gold')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_16.00',  n='nx_rad=16' ,c='palegreen')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.00',   n='nx_rad=8'  ,c='green')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_4.00',   n='nx_rad=4'  ,c='cyan')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_2.00',   n='nx_rad=2'  ,c='blue')
add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_1.00',   n='nx_rad=1'  ,c='purple')

lat1,lat2 = -50,50

htype,years,months,first_file,num_files = 'h1',[],[],0,365*5

recalculate = True

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.GEF.v1','png'

temp_dir,temp_file_head = os.getenv('HOME')+'/Research/E3SM/data_temp','clim.GEF.v1'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
res.xyXStyle       = "Log"
res.xyLineColors   = clr
res.xyDashPatterns = dsh


# define precipitation bins
bin_min_ctr = 0.04
bin_min_wid = 0.02
bin_spc_pct = 25.
nbin = 40

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
frq_list = []
amt_list = []
olr_list = []
gef_list = []
val_list = []
bin_list = []
case_name = []

gpcp_cnt = 0

for c in range(num_case):
   print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

   data_dir_tmp,data_sub_tmp = None, None
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]
   case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
   
   bin_tmp_file = f'{temp_dir}/{temp_file_head}.{case[c]}'
   if 'lat1' in locals(): bin_tmp_file += f'.lat1_{lat1}.lat2_{lat2}.nc'
   print('    bin_tmp_file: '+bin_tmp_file )

   if recalculate :
      case_obj = he.Case( name=case[c], time_freq='daily' )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in locals(): case_obj.lat1, case_obj.lat1 = lat1, lat1
      if 'lat2' in locals(): case_obj.lat2, case_obj.lat2 = lat2, lat2

      area = case_obj.load_data('area',htype=htype,num_files=1)

      prc = case_obj.load_data('PRECT',htype=htype,first_file=first_file,num_files=num_files)
      olr = case_obj.load_data('FLNT', htype=htype,first_file=first_file,num_files=num_files)
      
      # convert to daily mean
      prc = prc.resample(time='D').mean(dim='time').compute()
      olr = olr.resample(time='D').mean(dim='time').compute()

      #-------------------------------------------------------------------------
      # Calculate anomalies from daily climatology
      #-------------------------------------------------------------------------
      prc_p = xr.full_like(prc,0)
      olr_p = xr.full_like(olr,0)
      for d in range(365):
         prc_p[d::365] = prc[d::365] - prc[d::365].mean(dim='time')
         olr_p[d::365] = olr[d::365] - olr[d::365].mean(dim='time')

      # hc.print_stat(prc  ,compact=True)
      # hc.print_stat(prc_p,compact=True)
      # exit()

      # gbl_mean = ( (data*area).sum() / area.sum() ).values 
      #-------------------------------------------------------------------------
      # Recreate bin_YbyX here for log bin case
      #-------------------------------------------------------------------------
      bin_min=bin_min_ctr
      bin_spc=bin_min_wid
      bin_spc_log=bin_spc_pct
      #----------------------------------------------------
      # set up bins
      bin_log_wid = np.zeros(nbin)
      bin_log_ctr = np.zeros(nbin)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      bin_coord = xr.DataArray( bin_log_ctr )

      #----------------------------------------------------
      # create output data arrays
      ntime = len(prc['time']) if 'time' in prc.dims else 1   
      
      shape,dims,coord = (nbin,),'bins',[('bins', bin_coord)]

      bin_cnt = xr.DataArray( np.zeros(shape,dtype=prc.dtype), coords=coord, dims=dims )
      bin_amt = xr.DataArray( np.zeros(shape,dtype=prc.dtype), coords=coord, dims=dims )
      bin_frq = xr.DataArray( np.zeros(shape,dtype=prc.dtype), coords=coord, dims=dims )
      bin_val = xr.DataArray( np.zeros(shape,dtype=prc.dtype), coords=coord, dims=dims )
      bin_olr = xr.DataArray( np.zeros(shape,dtype=prc.dtype), coords=coord, dims=dims )
      bin_gef = xr.DataArray( np.zeros(shape,dtype=prc.dtype), coords=coord, dims=dims )

      condition = xr.DataArray( np.full(prc.shape,False,dtype=bool), coords=prc.coords )

      wgt, *__ = xr.broadcast(area,prc)
      wgt = wgt.transpose()

      prc_area_wgt = (prc*wgt) / wgt.sum()
      # olr_area_wgt = (olr*wgt) / wgt.sum()

      ones_area_wgt = xr.DataArray( np.ones(prc.shape), coords=prc.coords )
      ones_area_wgt = ( ones_area_wgt*wgt ) / wgt.sum()

      #----------------------------------------------------
      # Loop through bins
      for b in range(nbin):
         bin_bot = bin_log_ctr[b] - bin_log_wid[b]/2.
         bin_top = bin_log_ctr[b] + bin_log_wid[b]/2.
         condition.values = ( prc.values >=bin_bot )  &  ( prc.values < bin_top )
         bin_cnt[b] = condition.sum()
         if bin_cnt[b]>0 :
            # bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
            bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() / ones_area_wgt.sum()
            # bin_amt[b] = prc_area_wgt.where(condition,drop=True).sum() / (bin_spc_pct/1e2)
            bin_olr[b] = (olr_p*wgt).where(condition,drop=True).sum() / wgt.where(condition,drop=True).sum()
            bin_gef[b] = (olr_p*wgt).where(condition,drop=True).sum() / (bin_log_ctr[b]/86400.*hc.Lv) / wgt.where(condition,drop=True).sum()
      #----------------------------------------------------
      # use a dataset to hold all the output
      dims = ('bins',)
      bin_ds = xr.Dataset()
      bin_ds['bin_olr'] = (dims, bin_olr )
      bin_ds['bin_gef'] = (dims, bin_gef )
      bin_ds['bin_amt'] = (dims, bin_amt )
      bin_ds['bin_frq'] = (dims, bin_frq )
      bin_ds['bin_cnt'] = (dims, bin_cnt )
      bin_ds['bin_pct'] = (dims, bin_cnt/bin_cnt.sum()*1e2 )
      # bin_ds['bin_cnt'] = (dims, bin_cnt.sum(dim='ncol') )
      # bin_ds['bin_pct'] = (dims, bin_cnt.sum(dim='ncol')/bin_cnt.sum()*1e2 )
      bin_ds.coords['bins'] = ('bins',bin_coord)

      # bin_ds['gbl_mean'] = gbl_mean

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
      if os.path.isfile(bin_tmp_file) : os.remove(bin_tmp_file)
      bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
   else:
      bin_ds = xr.open_dataset( bin_tmp_file )
      bin_cnt = bin_ds['bin_cnt']
      bin_olr = bin_ds['bin_olr']
      bin_gef = bin_ds['bin_gef']
      bin_amt = bin_ds['bin_amt']
      bin_frq = bin_ds['bin_frq']
      bin_coord = bin_ds['bins']
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   # frequency = np.ma.masked_invalid( bin_frq.values )
   # amount    = np.ma.masked_invalid( bin_amt.values )
   # values    = np.ma.masked_invalid( bin_val.values )
   
   # if 'gbl_mean' in locals():
   #    print('\n'+f'    Area weighted global mean:  {gbl_mean} ')
   #    print(     f'    sum of rain amount dist:    {np.sum(amount*(bin_spc_pct/1e2))} \n')

   frq_list.append( np.ma.masked_invalid( bin_frq.values ) )
   amt_list.append( np.ma.masked_invalid( bin_amt.values ) )
   olr_list.append( np.ma.masked_invalid( bin_olr.values ) )
   gef_list.append( np.ma.masked_invalid( bin_gef.values ) )
   bin_list.append( bin_coord )
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
res.tiXAxisString = 'Rain Rate [mm/day]'

#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

res.xyExplicitLegendLabels = name
res.pmLegendDisplayMode    = "Always"
res.pmLegendOrthogonalPosF = -1.13
res.pmLegendParallelPosF   =  0.8+0.04
res.pmLegendWidthF         =  0.16
res.pmLegendHeightF        =  0.12   
res.lgBoxMinorExtentF      =  0.16   

res.trXMinF = np.min(bin_list)
res.trXMaxF = np.max(bin_list)

### plot frequency
res.tiYAxisString = 'Frequency [%]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(frq_list) ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)


### plot rain amount
# res.pmLegendDisplayMode = "Never"
# res.tiYAxisString = 'Rain Amount [mm/day]'
# plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(amt_list) ,res) )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

res.pmLegendDisplayMode = 'Never'
res.tiYAxisString = 'FLNT vs PRECT'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(olr_list) ,res) )

res.pmLegendDisplayMode = 'Never'
res.tiYAxisString = 'GEF'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(gef_list) ,res) )

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
