import os, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import copy, cftime, warnings
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)

var_x,var_y,lev_list = [],[],[]
def add_vars(var_x_name,var_y_name,lev=None): 
   # if lev==-1: lev = np.array([0])
   var_x.append(var_x_name)
   var_y.append(var_y_name)
   lev_list.append(lev)
#---------------------------------------------------------------------------------------------------
# add_case('OBS',n='Obs',c='black',) ; obs_case_X,obs_case_Y = 'ERAi','TRMM'

### Rotating RCE for Da Yang
# name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
# pscratch = '/pscratch/sd/w/whannah/e3sm_scratch/perlmutter'
# add_case('E3SM.GNUCPU.ne4pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02.CORI',n='MMF MSE test')
# add_case('E3SM.GNUCPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02.CORI',n='MMF MSE test')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02', n='MMF RCEROT',         p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.02', n='MMF RCEROT FIX-QRT', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRL.02', n='MMF RCEROT FIX-QRT', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRS.02', n='MMF RCEROT FIX-QRT', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.03',n='MMF RCEROT FIX-QRT',c='red', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.03',n='MMF RCEROT CTRL',   c='blue', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')

# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_8_1',  n='8x1',c='red')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_16_1', n='16x1',c='orange')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_1', n='32x1',c='green')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_64_1', n='64x1',c='cyan')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_128_1',n='128x1',c='blue')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_256_1',n='256x1',c='purple')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_512_1',n='512x1',c='pink')

# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_1', n= '32x1',c='red'  ,d=0)
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_64_1', n= '64x1',c='green',d=0)
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_128_1',n='128x1',c='blue' ,d=0)

# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_4', n= '32x4',c='red'  ,d=1)
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_64_4', n= '64x4',c='green',d=1)
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_128_4',n='128x4',c='blue' ,d=1)

add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_1', n= '32x1',c='red'  ,d=0)
add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_4', n= '32x4',c='green',d=0)
add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_32',n= '32x4',c='blue' ,d=0)

#---------------------------------------------------------------------------------------------------

add_vars('CWV','PRECT')
# add_vars('FLNT','DMSE_RES')
# add_vars('FLNT','DMSE_RES_ALT')
# add_vars('MSE','DMSE_RAD')

htype,years,months,first_file,num_files = 'h1',[],[],20,0
# htype,years,months,first_file,num_files = 'h2',[],[],60,20

htype_x,htype_y = htype,htype

# lat1,lat2,lon1,lon2 = -5,5,-5,5
# lat1,lat2 = -5,5
lat1,lat2 = -50,50

recalculate = True

temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'

fig_type,fig_file = 'png',os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.bin.v1'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var_x)
num_case = len(case)

# if 'clr' not in vars(): 
#    if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
#    else : clr = ['black']

# if 'dsh' not in vars(): 
#    if num_case>1 : dsh = np.zeros(num_case)
#    else : dsh = [0]

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*2)
res = hs.res_xy()
res.vpHeightF = 0.3
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
res.xyLineColors   = clr
res.xyDashPatterns = dsh

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_data_dir(c):
   global case_sub
   data_dir_tmp = None
   # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   return data_dir_tmp

def get_data_sub(c):
   global case_dir
   data_sub_tmp = None
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]
   return data_sub_tmp

def get_comp(case):
   comp = 'eam'
   if 'INCITE2019' in case: comp = 'cam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'MAML' in case: comp = 'eam_0001'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  X / Y : '+hc.tcolor.MAGENTA+var_x[v]+hc.tcolor.ENDC+' / '+hc.tcolor.MAGENTA+var_y[v]+hc.tcolor.ENDC)
   # print('  var_x: '+hc.tcolor.MAGENTA+var_x[v]+hc.tcolor.ENDC)
   # print('  var_y: '+hc.tcolor.MAGENTA+var_y[v]+hc.tcolor.ENDC)
   bin_list = []
   cnt_list = []
   val_list = []
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      
      tcase_x, tcase_y = case[c], case[c]   
      if case[c]=="OBS": tcase_x, tcase_y = obs_case_X, obs_case_Y

      case_obj_x = he.Case( name=tcase_x, atm_comp=get_comp(case[c]), data_dir=get_data_dir(c), data_sub=get_data_sub(c)  )
      case_obj_y = he.Case( name=tcase_y, atm_comp=get_comp(case[c]), data_dir=get_data_dir(c), data_sub=get_data_sub(c)  )

      tmp_file = f'{temp_dir}/clim.bin.v1.{case[c]}.{var_x[v]}.{var_y[v]}'
      if 'lat1' in vars(): tmp_file = tmp_file + f'.lat1_{lat1}.lat2_{lat2}'
      if 'lon1' in vars(): tmp_file = tmp_file + f'.lon1_{lat1}.lon2_{lat2}'
      tmp_file = tmp_file + f'.nc'

      print('\n    tmp_file: '+tmp_file+'\n')

      if recalculate :
         #-------------------------------------------------------------------------
         # read the data
         #-------------------------------------------------------------------------
         if 'lev_list' in locals(): lev = lev_list[v]
         if 'lat1' in vars(): case_obj_x.lat1, case_obj_y.lat1 = lat1, lat1
         if 'lat2' in vars(): case_obj_x.lat2, case_obj_y.lat2 = lat2, lat2
         if 'lon1' in vars(): case_obj_x.lon1, case_obj_y.lon1 = lon1, lon1
         if 'lon2' in vars(): case_obj_x.lon2, case_obj_y.lon2 = lon2, lon2

         X = case_obj_x.load_data(var_x[v],htype=htype_x,years=years,months=months,
                                 first_file=first_file,num_files=num_files,lev=lev)
         Y = case_obj_y.load_data(var_y[v],htype=htype_y,years=years,months=months,
                                 first_file=first_file,num_files=num_files,lev=lev)

         #-------------------------------------------------------------------------
         # vertically integrate
         #-------------------------------------------------------------------------
         def vertical_integral(data,average=False):
            
            dp_dims = ('lev','time','ncol')
            hyai = case_obj_x.load_data('hyai',htype=htype)
            hybi = case_obj_x.load_data('hybi',htype=htype)
            nlevi = len(hyai)
            nlevm = nlevi-1
            k_top = 0
            if nlevm==60: k_top = 18 # L60 ~ 95mb
            Pint = ( hyai * P0 + hybi * PS ).transpose('ilev','time','ncol')
            dp = xr.DataArray([ (Pint[k+1,:,:]-Pint[k,:,:]) for k in range(nlevm) ],dims=dp_dims)
            data_slice = data.isel(lev=slice(k_top,nlevm))
            dp_slice   =   dp.isel(lev=slice(k_top,nlevm)) 
            data_out = (data_slice*dp_slice/hc.g).sum(dim='lev')
            if average:
               data_out = data_out / (dp_slice/hc.g).sum(dim='lev')
            return data_out

         # mlev = X['lev'].values
         if 'lev' in X.dims or 'lev' in Y.dims: 
            PS = case_obj_x.load_data('PS',htype=htype,years=years,months=months,
                                      first_file=first_file,num_files=num_files,lev=lev)
            P0 = case_obj_x.load_data('P0',htype=htype,years=years,months=months,
                                      first_file=first_file,num_files=num_files,lev=lev)

         if 'lev' in X.dims: X = vertical_integral(X,average=True)
         if 'lev' in Y.dims: Y = vertical_integral(Y)

         # X = X*86400.
         # Y = Y*86400.
            
         #-------------------------------------------------------------------------
         #-------------------------------------------------------------------------

         ### Convert to daily mean
         # X = X.resample(time='D').mean(dim='time')
         # Y = Y.resample(time='D').mean(dim='time')

         ### print stuff
         # print(); print(X); print(); print(Y); print()
         # hc.print_stat(X,name=var_x[v],stat='naxsh',indent='    ',compact=True)
         # hc.print_stat(Y,name=var_y[v],stat='naxsh',indent='    ',compact=True)

         X = X.resample(time='D').mean(dim='time')
         Y = Y.resample(time='D').mean(dim='time')

         hc.print_stat(X,name=var_x[v],stat='naxsh',indent='    ',compact=True)
         hc.print_stat(Y,name=var_y[v],stat='naxsh',indent='    ',compact=True)

         #-------------------------------------------------------------------------
         #-------------------------------------------------------------------------
         if var_x[v]=='CWV'      : bin_min, bin_max, bin_spc = 30, 70, 2
         # if var_x[v]=='CWV'      : bin_min, bin_max, bin_spc = 2, 70, 2
         if var_x[v]=='FLNT'     : bin_min, bin_max, bin_spc = 120, 300, 5
         
         # for RCEROT
         # if var_x[v]=='MSE'      : bin_min, bin_max, bin_spc = 316, 346, 1
         if var_x[v]=='MSE'      : bin_min, bin_max, bin_spc = 320, 345, 0.5

         bin_ds = hc.bin_YbyX( Y, X, bin_min=bin_min, bin_max=bin_max, bin_spc=bin_spc )

         # print(); print(bin_ds['bins'])
         # print(); print(bin_ds['bin_val'])
         # exit()

         print('writing to file: '+tmp_file)
         bin_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( tmp_file )
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      val_list.append(bin_ds['bin_val'].values)
      cnt_list.append(bin_ds['bin_pct'].values)
      bin_list.append(bin_ds['bins'].values)
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------

   plot_data_x  = np.stack(bin_list)
   plot_data_y1 = np.stack(val_list)
   plot_data_y2 = np.stack(cnt_list)

   # mask NaN values
   plot_data_y1 = np.ma.masked_invalid(plot_data_y1)

   ip1, ip2 = v, v+num_var
   plot[ip1] = ngl.xy(wks,plot_data_x,plot_data_y1,res) 
   plot[ip2] = ngl.xy(wks,plot_data_x,plot_data_y2,res) 

   hs.set_subtitles(wks, plot[ip1], '', '', var_y[v], font_height=0.015)
   hs.set_subtitles(wks, plot[ip2], '', '', var_y[v], font_height=0.015)

#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.15
   lgres.lgLabelFontHeightF = 0.012
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 8
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   lx,ly = 0.2,0.9
   # if num_var==2: lx,ly = 0.3,0.45
   # if num_var==4: lx,ly = 0.05,0.5

   pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [2,num_var]
# layout = [len(cases),len(vpars)]
# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------