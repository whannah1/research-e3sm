import os
import ngl
import subprocess as sp
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
host = hc.get_host()

case =[f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      ]
name = []
for c in case:
   if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4')
   if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2')
   if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4')
   if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4')
   if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2')
   
#-------------------------------------------------------------------------------
var = ['PRECT']
# var = ['PRECT','LHFLX','SHFLX','TMQ','UBOT','TGCLDLWP','TGCLDIWP']


fig_type = "png"
fig_file = os.getenv("HOME")+"/Research/E3SM/figs_clim/clim.cmip.rmse.v1"

# htype,years,months,first_file,num_files = 'h0',[],[],0,1

print_stats = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

subtitle_font_height = 0.015 - 0.0003*num_var - 0.0014*num_case

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case)
res = hs.res_xy()
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
# res.lbLabelFontHeightF           = 0.012
# res.tmXBOn = False
# res.tmYLOn = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   data_list = []
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )

      tvar = var[v]

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------      
      # area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)
      # data = case_obj.load_data(tvar,     htype=htype,years=years,months=months,first_file=first_file,num_files=num_files,lev=lev)

      idir = os.getenv('SCRATCH')+f'/e3sm_scratch/cori-knl/{case[c]}/clim/'
      
      for season in ['ANN']:
         
         file_name = idir+f'{case[c]}_{season}_climo.nc'
         print(file_name)
         ds = xr.open_dataset(file_name)

         area = ds['area']

         derived = False
         if var[v]=='PRECT': derived=True; data = ds['PRECC'] + ds['PRECL']; data = data*1e3*86400. 
         if not derived: data = ds[var[v]]

         # data_interp = he.interpolate_to_pressure(ds,var[v],lev=lev)

         data = data.isel(time=0)

         if print_stats: hc.print_stat(data,name=var[v],stat='naxsh')
            
         # Calculate area weighted global mean

         gbl_mean = ( (data*area).sum() / area.sum() ).values 

         print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

         data_list.append( data.values )

      exit()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   # for d in data_list: print(d.shape)

   data_min = np.min([np.min(d) for d in data_list])
   data_max = np.max([np.max(d) for d in data_list])

   for c in range(num_case):
      case_obj = he.Case( name=case[c] )

      # ip = v*num_case+c
      ip = c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      
      var_str = var[v]
      # if var[v]=="PRECT" : var_str = "Precipitation"

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------

      plot[ip] = ngl.contour_map(wks,data_list[c],tres) 

      #----------------------------------------------------------------------
      #----------------------------------------------------------------------
      ctr_str = ''
      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name
      # if var[v] in ['PRECT','PRECC','PRECL'] and 'gbl_mean' in vars() : 
      #    ctr_str = 'Mean: '+'%.2f'%gbl_mean+' [mm/day]'

      if 'lev_str' in vars() : ctr_str = lev_str

      hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [num_case,num_var]
# layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]
if num_case==1 and num_var==4 : layout = [2,2]

pnl_res = hs.setres_panel()
# pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
