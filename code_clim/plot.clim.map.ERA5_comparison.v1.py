import os, ngl, glob, copy, string, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
host = hc.get_host()

name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,d=None,s=None,g=None,c=None):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); case_dir.append(d); case_sub.append(s); case_grid.append(g)
var,lev_list = [],[]
def add_var(var_name,lev=0): var.append(var_name); lev_list.append(lev)
#-------------------------------------------------------------------------------

### INCITE 2022 TMS check
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# he.default_data_dir = os.getenv('HOME')+'/E3SM/scratch2/'
# add_case('ERA5',d='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5',s='monthly_ne30pg2'); era5_yr1,era5_yr2 = 2005,2014
# add_case('E3SM.INCITE2022-TMS-01.ne30pg2.F2010-MMF1.TMS-OFF',n='MMF TMS-OFF')
# add_case('E3SM.INCITE2022-TMS-01.ne30pg2.F2010-MMF1.TMS-ON', n='MMF TMS-ON' )


### INCITE 2022 coupled runs
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
he.default_data_dir = os.getenv('HOME')+'/E3SM/scratch2/'
# add_case('ERA5',d='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5',s='monthly_ne30pg2'); era5_yr1,era5_yr2 = 1960,1965
add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL1950',     n='E3SM (1950)',c='orange')
add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',n='MMF  (1950)',c='cyan')
add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR',     n='E3SM (20TR)',c='red' )
add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='MMF  (20TR)',c='blue')


### coupled tuning tests for non-MMF
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# # add_case('ERA5',d='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5',s='monthly_ne30pg2'); era5_yr1,era5_yr2 = 1960,1965
# he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950',                                    n='E3SMv2',               c='black')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO',                              n='E3SMv2 (no-chem)',     c='red')
# # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.001D6.nccons_100.0D6',n='E3SMv2 (no-chem) alt1',c='pink')
# # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.01D6.nccons_200.0D6', n='E3SMv2 (no-chem) alt2',c='purple')
# # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.01D6.nccons_40.0D6',  n='E3SMv2 (no-chem) alt3',c='magenta')
# # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.01D6.nccons_70.0D6',  n='E3SMv2 (no-chem) alt4',c='orange')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.ice_sed_200.0',                n='E3SMv2 (no-chem) alt5',c='green')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.ice_sed_1.0',                  n='E3SMv2 (no-chem) alt6',c='green')
# # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',                                n='E3SM-MMF',            c='blue')
# # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QI_3E-05',         n='E3SM-MMF alt1',       c='aquamarine')
# # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QW_5E-04_QI_3E-05',n='E3SM-MMF alt2',       c='cyan')




#-------------------------------------------------------------------------------

add_var('PRECT')
# add_var('TS')
# add_var('FLNT')
# add_var('FSNTOA')
# add_var('TGCLDLWP')
# add_var('TGCLDIWP')
# add_var('AEROD_v')

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.map.ERA5_comparison.v1'

# htype,years,months,first_file,num_files = 'h0',[],[],0,12
htype,years,months,first_file,num_files = 'h0',[],[],5*12,5*12
# htype,years,months,first_file,num_files = 'h0',[],[],5*12,10*12

plot_diff,add_diff = False,False

print_stats = True

var_x_case = True

num_plot_col = len(case)
# num_plot_col = 2

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.008

diff_base = 0
if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
# if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
# npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.01
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'


scrip_ds = xr.open_dataset(scrip_file_path)
res.cnFillMode    = "CellFill"
res.sfXArray      = scrip_ds['grid_center_lon'].values
res.sfYArray      = scrip_ds['grid_center_lat'].values
res.sfXCellBounds = scrip_ds['grid_corner_lon'].values
res.sfYCellBounds = scrip_ds['grid_corner_lat'].values

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list = []
   glb_avg_list = []
   lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})

      if case[c]=='ERA5':

         file_list = []
         for y in range(era5_yr1,era5_yr2+1):
            file_path = f'{case_dir[c]}/{case_sub[c]}/ERA5.monthly.sfc.{y}-*'
            files = sorted(glob.glob(file_path))
            for f in files: file_list.append(f)

         ds = xr.open_mfdataset( file_list, combine='by_coords', concat_dim='time' )

         tvar = None
         if var[v]=='TS'      : tvar = 'skt'
         if var[v]=='PS'      : tvar = 'sp'
         if var[v]=='T'       : tvar = 't'
         if var[v]=='Q'       : tvar = 'q'
         if var[v]=='U'       : tvar = 'u'
         if var[v]=='TGCLDLWP': tvar = 'tclw'
         if var[v]=='TGCLDIWP': tvar = 'tciw'
         if var[v]=='TMQ'     : tvar = 'tcw'
         if var[v]=='CWV'     : tvar = 'tcw'
         if var[v]=='FLNT'    : tvar = 'ttr'
         if var[v]=='FSNT'    : tvar = 'tsr'
         if var[v]=='FSNTOA'  : tvar = 'tsr'    # top_net_solar_radiation
         if var[v]=='LHFLX'   : tvar = 'mslhf'
         if var[v]=='SHFLX'   : tvar = 'msshf'
         if tvar is None: raise ValueError(f'variable {var[v]} does not have a corresponding ERA5 variable?')

         if tvar is None:
            if var[v]=='NET_TOA_RAD': tvar = var[v]; data = ds['tsr'] + ds['ttr']
            if tvar is None: raise ValueError(f'variable {var[v]} not found in ERA5 data?')
         else:
            data = ds[tvar]

         # unit conversions
         if var[v]=='TS':           data = data - 273.15  # convert to Celsius
         if var[v]=='NET_TOA_RAD':  data = data/86400.    # convert J/m2 to W/m2 (only for monthly)
         if var[v]=='FSNTOA':       data = data/86400.    # convert J/m2 to W/m2 (only for monthly)
         if var[v]=='FLNT':         data = data/86400.*-1 # convert J/m2 to W/m2 (only for monthly)

      else:
         case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp )
         case_obj.set_coord_names(var[v])
         data = case_obj.load_data(var[v],htype=htype,ps_htype=htype,lev=lev,
                                         first_file=first_file,num_files=num_files)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         data = data.mean(dim='time')

      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------   
      gbl_mean = ( (data*area).sum() / area.sum() ).values 
      print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
      glb_avg_list.append(gbl_mean)
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      data_list.append( data.values )

      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'BlueWhiteOrangeRed'
      if var[v] in ['U','V','UBOT','VBOT','U850','V850','U200','V200',
                    'MMF_CVT_TEND_T','MMF_CVT_TEND_Q']: 
         tres.cnFillPalette = "BlueWhiteOrangeRed"

      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      if var[v]=='TS'                  : tres.cnLevels = np.arange(0,40+2,2)
      if var[v]=='RH'                  : tres.cnLevels = np.arange(10,100+1,1)
      # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(1,30+1,1)*1e-2
      # if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2)
      if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(0.005,0.155,0.01)
      if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.arange(0.01,0.25,0.015)

      if var[v] in ['U','V']: 
         if plot_diff and not add_diff and c in diff_case :
            if lev==850: tres.cnLevels = np.arange( -8, 8+1,1)
            if lev==200: tres.cnLevels = np.arange(-16,16+2,2)
         else:
            if lev==850: tres.cnLevels = np.arange(-20,20+2,2)
            if lev==200: tres.cnLevels = np.arange(-60,60+6,6)
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 41
         aboutZero = False
         if var[v] in ['SPTLS','SPQTLS','U','V','VOR','DIV',
                       'U850','V850','U200','V200',
                       'MMF_CVT_TEND_T','MMF_CVT_TEND_Q',] : 
            aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      var_str = var[v]

      lev_str = None
      if lev>0: lev_str = f'{lev}mb'
      if lev<0: lev_str = f'k={(lev*-1)}'
      if lev_str is not None:
         var_str = f'{lev_str} {var[v]}'

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      tres.lbLabelBarOn = True
      if use_common_label_bar: tres.lbLabelBarOn = False
         
         
      if plot_diff and c==diff_base : base_name = name[c]

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         #----------------------------------------------------------------------
         # set plot subtitles
         #----------------------------------------------------------------------
         ctr_str = ''
         if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'

         lstr = name[c]
         if case[c]=='ERA5': lstr = f'ERA5 ({era5_yr1}-{era5_yr2})'

         hs.set_subtitles(wks, plot[ip], lstr, '', var_str, center_sub_string=ctr_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)

         tres.lbLabelBarOn = True

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+(num_case-1)
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)

         
         #-----------------------------------
         ctr_str = ''
         ctr_str = 'Diff'
         if glb_avg_list != []: 
            glb_diff = glb_avg_list[c] - glb_avg_list[diff_base]
            ctr_str += f' ({glb_diff:6.4})'

         lstr = name[c]
         
         hs.set_subtitles(wks, plot[ipd], lstr, '', var_str, center_sub_string=ctr_str,font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]


if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### use common label bar
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
