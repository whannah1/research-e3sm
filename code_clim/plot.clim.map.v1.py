import os, ngl, subprocess as sp, numpy as np, xarray as xr, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string
import cmocean
host = hc.get_host()

name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,c=None):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); 
   case_dir.append(p); case_sub.append(s); 
   case_grid.append(g)

#-------------------------------------------------------------------------------
if host=='olcf':

   ### INCITE2021-CMT
   # case.append(f'ERAi');name.append('ERAi')
   # add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.01',       n='MMF 2D')
   # add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.MOMFB.01', n='MMF 2D+MF')
   # # add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.01',      n='MMF 3D')
   # add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.01',n='MMF 3D+MF')

   # add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.MOMFB.02', n='MMF 2D',p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch')
   # add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='MMF 3D',p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch')

   ### water path test for Greg
   # add_case('E3SM.WP-TEST.GNUGPU.ne30pg2_ne30pg2.F-MMFXX.NLEV_60.CRMNX_128.RADNX_8.BVT.00',n='E3SM-MMF')

   ### Ice fall tests for John and Scott
   # add_case('E3SM.ICE-FALL-TEST.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.RADNX_8.BVT.00',            n='control')
   # add_case('E3SM.ICE-FALL-TEST.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.RADNX_8.BVT.REDUCE-FALL.00',n='slow ice fall')
   # add_case('E3SM.ICE-FALL-TEST.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.RADNX_8.BVT.NO-ICE-FALL.00',n='no ice fall')

   ### reduced radiation sensitivity
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00', n='nx_rad=128')
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_64.00',  n='nx_rad=64' )
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_32.00',  n='nx_rad=32' )
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_16.00',  n='nx_rad=16' )
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.00',   n='nx_rad=8'  )
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_4.00',   n='nx_rad=4'  )
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_2.00',   n='nx_rad=2'  )
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_1.00',   n='nx_rad=1'  )

   ### sorted radiation column test
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.00',           n='nx_rad=8')
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00',         n='nx_rad=128')
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.RAD_SORT.00',  n='nx_rad=8+SORT')
   # add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.RAD_SORT.00',n='nx_rad=128+SORT')

   ### CRM hyperviscosity + sedimentation tests
   # add_case('E3SM.INCITE2022-HC-00.ne30pg2_oECv3.F2010-MMF1.2008-10-01.L_125_115.NXY_128x1',       n='E3SM-MMF L125')
   # add_case('E3SM.INCITE2022-HC-00.ne30pg2_oECv3.F2010-MMF1.2008-10-01.L_125_115.NXY_128x1.HV',    n='E3SM-MMF L125 +HV')
   # add_case('E3SM.INCITE2022-HC-00.ne30pg2_oECv3.F2010-MMF1.2008-10-01.L_125_115.NXY_128x1.SED',   n='E3SM-MMF L125 +SED')
   # add_case('E3SM.INCITE2022-HC-00.ne30pg2_oECv3.F2010-MMF1.2008-10-01.L_125_115.NXY_128x1.HV.SED',n='E3SM-MMF L125 +HV+SED')
   # add_case('E3SM.INCITE2022-HC-00.ne30pg2_oECv3.F2010-MMF1.2008-10-01.L_60_50.NXY_64x1',    n='E3SM-MMF')
   # add_case('E3SM.INCITE2022-HC-00.ne30pg2_oECv3.F2010-MMF1.2008-10-01.L_60_50.NXY_64x1.SED',n='E3SM-MMF+SED')
   # add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1',       n='MMF L125')
   # add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED',n='MMF L125 +HV+SED')
   # pscratch,psub = '/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/','run'
   # add_case('E3SM.INCITE2021-CRMHV.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.L125.NXY_512x1.CRMDX_100.BVT.CRMHV.00',n='',p=pscratch,s=psub)
   # add_case('E3SM.INCITE2021-CRMHV.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.L60.NXY_64x1.CRMDX_2000.BVT.00'       ,n='',p=pscratch,s=psub)

   ### INCITE 2022 CESS
   # add_case('E3SM.INCITE2022-CESS.ne45pg2.F2010-MMF1.NXY_32x32.SSTP_0K', n='E3SM-MMF',     c='black')
   # add_case('E3SM.INCITE2022-CESS.ne45pg2.F2010-MMF1.NXY_32x32.SSTP_4K', n='E3SM-MMF +4K', c='black')

   ### SCREAM LR Cess tests
   # add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010-SCREAM-LR.SSTP_0K.NN_64',n='SCREAM +0K')
   # add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010-SCREAM-LR.SSTP_4K.NN_64',n='SCREAM +4K')
   # add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010.SSTP_0K.NN_64',          n='E3SMv2 +0K')
   # add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010.SSTP_4K.NN_64',          n='E3SMv2 +4K')
   
   ### coupled historical experiments (2023)
   # add_case('E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='E3SM-MMF',s='archive/atm/hist')
   # add_case('v2.LR.historical_0101',                                  n='E3SMv2',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/e3smv2_historical',s='archive/atm/hist')
   # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO',                      n='E3SMv2',      c='black')
   # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',                       n='E3SM-MMF',    c='red')
   # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QI_3E-05',n='E3SM-MMF alt',c='blue')

   ### 4xCO2 tests on Summit
   tmp_path,tmp_sub = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch','archive/atm/hist'
   add_case('E3SM.2023-CO2-TEST-01.GNUGPU.ne30pg2_EC30to60E2r2.WCYCL1850-MMF1.1xCO2',n='E3SM-MMF PI 1xCO2',c='blue' ,p=tmp_path,s=tmp_sub)
   add_case('E3SM.2023-CO2-TEST-01.GNUGPU.ne30pg2_EC30to60E2r2.WCYCL1850-MMF1.2xCO2',n='E3SM-MMF PI 2xCO2',c='green',p=tmp_path,s=tmp_sub)
   add_case('E3SM.2023-CO2-TEST-01.GNUGPU.ne30pg2_EC30to60E2r2.WCYCL1850-MMF1.4xCO2',n='E3SM-MMF PI 4xCO2',c='red'  ,p=tmp_path,s=tmp_sub)

   ### PAM global tests
   # pscratch,psub = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/','run'
   # add_case('E3SM.PAM-DEV-2023-37.GNUGPU.ne30pg2_oECv3.F2010-MMF1',             n='MMF+SAM',    p=pscratch,s=psub)
   # add_case('E3SM.PAM-DEV-2023-40.GNUGPU.ne30pg2_oECv3.F2010-MMF2',             n='MMF+PAM NEW',p=pscratch,s=psub)
   # add_case('E3SM.PAM-DEV-2023-40.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_08.DPP_2',n='MMF+PAM NEW',p=pscratch,s=psub)
   # add_case('E3SM.PAM-DEV-2023-40.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_08.DPP_4',n='MMF+PAM NEW dt=8/4',p=pscratch,s=psub)
   # add_case('E3SM.PAM-DEV-2023-40.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_10.DPP_4',n='MMF+PAM NEW dt=10/4',p=pscratch,s=psub)
   # add_case('E3SM.PAM-DEV-2023-38.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_04.DPP_2',n='MMF+PAM OLD',p=pscratch,s=psub)

   ### PAM global tests
   # pscratch,psub = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/','run'
   # add_case('E3SM.2023-PAM-VT-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.VT-off', n='MMF1.VT-off',   p=pscratch,s=psub)
   # add_case('E3SM.2023-PAM-VT-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.VT-on',  n='MMF1.VT-on',    p=pscratch,s=psub)
   # add_case('E3SM.2023-PAM-VT-00.GNUGPU.ne30pg2_oECv3.F2010-MMF2.VT-off', n='MMF2.VT-off',   p=pscratch,s=psub)
   # add_case('E3SM.2023-PAM-VT-00.GNUGPU.ne30pg2_oECv3.F2010-MMF2.VT-on',  n='MMF2.VT-on',    p=pscratch,s=psub)

   # add_case('E3SM.2023-PAM-ENS-00.ne30pg2_EC30to60E2r2.F2010-MMF2.CDT_05.DPP_04.SSC_01.HDT_0060',n='CDT_05.DPP_04.SSC_01.HDT_0060',p=pscratch,s=psub)
   # add_case('E3SM.2023-PAM-ENS-00.ne30pg2_EC30to60E2r2.F2010-MMF2.CDT_05.DPP_02.SSC_01.HDT_0060',n='CDT_05.DPP_02.SSC_01.HDT_0060',p=pscratch,s=psub)
   # add_case('E3SM.2023-PAM-ENS-00.ne30pg2_EC30to60E2r2.F2010-MMF2.CDT_05.DPP_04.SSC_01.HDT_0030',n='CDT_05.DPP_04.SSC_01.HDT_0030',p=pscratch,s=psub)
   # add_case('E3SM.2023-PAM-ENS-00.ne30pg2_EC30to60E2r2.F2010-MMF2.CDT_05.DPP_02.SSC_01.HDT_0030',n='CDT_05.DPP_02.SSC_01.HDT_0030',p=pscratch,s=psub)

#-------------------------------------------------------------------------------
if host=='nersc':

   ### RGMA runs
   # name,case = [],[]
   # case.append('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00');                          name.append('E3SM')
   # case.append('E3SM.RGMA.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.RADNX_4.00'); name.append('E3SM-MMF')   
   # case.append('E3SM.RGMA.ne120pg2_r05_oECv3.FC5AV1C-H01A.00');                      name.append('E3SM hi-res')

   ### INCITE-AQUA-RRM
   # add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.CRMDX_2000.00',  n='MMF dx=2km')
   # add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.CRMDX_200.00',   n='MMF dx=200m')
   # add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.DXSCL_12e-3.00', n='MMF dxscl=12e-3')
   # scrip_file_path = '/global/homes/w/whannah/E3SM/data_grid/RRM_cubeface_grad_ne30x3pg2_scrip.nc'

   ### coupled runs for Jim Benedict
   # add_case('E3SM.PI-CPL.v1.ne30.01',  n='E3SMv1')
   # add_case('E3SM.PI-CPL.v2.ne30.01',  n='E3SMv2')
   
   ### QBO experiments
   # add_case('E3SM.PI-CPL.v1.ne30.01',n='E3SMv1 PI coupled')
   # add_case('E3SM.PI-CPL.v2.ne30.01',n='E3SMv2 PI coupled')
   # add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72.01',       n='E3SM control')
   # add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72-nsu40.01', n='E3SM L72 smoothed')
   # add_case('E3SM.QBO-TEST.F2010.ne30pg2.L80-rsu40.01', n='E3SM L80 refined')

   ### E3SM turbulent mountain stress test
   # add_case('v2.LR.amip_0101',                               n='E3SM')
   #he.default_data_dir='/global/homes/w/whannah/E3SM/scratch'
   #add_case('E3SM.TMS-00.F20TR.ne30pg2_EC30to60E2r2.tms-off',n='E3SMv2'    )#,d='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu')
   #add_case('E3SM.TMS-00.F20TR.ne30pg2_EC30to60E2r2.tms-on', n='E3SMv2+TMS')#,d='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu')
   # add_case('E3SM.TMS-00.F2010-MMF1.ne30pg2_oECv3.tms-off',n='MMF TMS-OFF',d='/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu')
   # add_case('E3SM.TMS-00.F2010-MMF1.ne30pg2_oECv3.tms-on' ,n='MMF TMS-ON' ,d='/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu')

   ### vertical grid tests for new L72
   # add_case('E3SM.L72-NEW-TEST.F-MMFXX.ne30pg2_EC30to60E2r2.L72-v0.00',n='E3SM-MMF L72v0')
   # add_case('E3SM.L72-NEW-TEST.F-MMFXX.ne30pg2_EC30to60E2r2.L72-v1.00',n='E3SM-MMF L72v1')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v0.00',n='E3SM L72v0')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1.00',n='E3SM L72v1')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v2.00',n='E3SM L72v2')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v3.00',n='E3SM L72v3')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v0.HF-OUTPUT.00',n='E3SM L72v0')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1.HF-OUTPUT.00',n='E3SM L72v1')

   ### vertical grid sensitivity tests
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v0.00',    n='E3SM L72v0')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1.00',    n='E3SM L72v1')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1-R2.00', n='E3SM L72v1 R2')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1-R5.00', n='E3SM L72v1 R5')
   # add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1-R10.00',n='E3SM L72v1 R10')

   ### effective radius test
   # add_case('E3SM.EFF-RAD-TEST-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.RLLND_8.0',    n='MMF ER=8')
   # add_case('E3SM.EFF-RAD-TEST-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.RLLND_1.0',    n='MMF ER=1')

   # add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='MMF (20TR)',d='/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu',s='archive/atm/hist')
   # add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='MMF (20TR)',d='/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu',s='archive/atm/hist')

   ### L80 ensemble
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
   # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_0.10.HD_1.50',n='',p=tmp_path,s=tmp_sub)

   ### SciDAC MMF tests w/ new vertical grids
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L60',n='MMF-SAM L60',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L64',n='MMF-SAM L64',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L72',n='MMF L72',p=tmp_path,s=tmp_sub)

   ### PAM tests w/ new vertical grids
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # add_case('E3SM.2023-PAM-DEV-00.ne30pg2_EC30to60E2r2.F2010-MMF2.L60',n='MMF-PAM L60',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-DEV-00.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='MMF-PAM L64',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-DEV-01.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='MMF-PAM L64',p=tmp_path,s=tmp_sub)

   tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L64',n='MMF-SAM',p=tmp_path,s=tmp_sub)
   add_case('E3SM.2023-PAM-DEV-00.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='MMF-PAM',p=tmp_path,s=tmp_sub)

   ### SciDAC L80 ensemble
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
   # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # defaults
   # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_20.HD_1.00',n='',p=tmp_path,s=tmp_sub)

#-------------------------------------------------------------------------------
if host=='anl':

   ### v3 candidate w/ smoothed L72 (chrysalis)
   # add_case('20230127.amip.v3atm.FourthSmoke.chrysalis',          n='v3 '     ,g='ne30pg2',p='/lcrc/group/e3sm/ac.golaz/E3SMv3_dev'  ,s='archive/atm/hist')
   # add_case('20230214.amip.v3atm.FourthSmoke.chrysalis.L72-nsu40',n='v3 L72sm',g='ne30pg2',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='run')

   scrip_file_path = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'
   add_case('20230629.v3alpha02.amip.chrysalis.L72',n='E3SM L72',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')
   add_case('20230629.v3alpha02.amip.chrysalis.L80',n='E3SM L80',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')

#-------------------------------------------------------------------------------

var,lev_list = [],[]
def add_var(var_name,lev=0): var.append(var_name); lev_list.append(lev)



# add_var('TS')
# add_var('TMQ')
add_var('PRECT') 
# add_var('TGCLDLWP'); add_var('TGCLDIWP')
# add_var('PRECSC')
# add_var('PRECC'); add_var('PRECL')
# add_var('P-E')
# add_var('AEROD_v')
# add_var('AODVIS')
# add_var('LHFLX')
# add_var('SHFLX')
# add_var('TGCLDLWP') #; add_var('FSNT')
# add_var('TGCLDIWP')
# add_var('NET_TOA_RAD')
# add_var('FSNT');add_var('FLNT')
# add_var('FSNS');add_var('FLNS')
# add_var('RESTOM');
# add_var('LWCF')
# add_var('SWCF')
# add_var('FSNTOA')
# add_var('TAUXY')
# add_var('U10')
# add_var('T2M')
# add_var('QBOT')
# add_var('WSPD_BOT')
# add_var('CLDLOW')
# add_var('CLDMED')
# add_var('CLDHGH')
# add_var('CLDTOT')
# add_var('U900')
# add_var('U850')
# add_var('U200')

# add_var('MMF_DU',lev=850)


### use for h0
# add_var('V',lev=-63)
# add_var('U',lev=850)
# add_var('U',lev=500)
# add_var('U',lev=200)


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.map.v1'

#-------------------------------------------------------------------------------
lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = -20,0,220,240
# lat1,lat2,lon1,lon2 = 10,30,360-100,360-55

# lat1,lat2,lon1,lon2 = -10,70,180,340             # N. America
# lat1,lat2,lon1,lon2 = 25, 50, 360-125, 360-75    # CONUS
# lat1,lat2,lon1,lon2 = -40,40,90,240              # MC + West Pac
# lat1,lat2,lon1,lon2 = 0,60,50,120                  # India
# lat1,lat2,lon1,lon2 = -45,-45+60,360-120,360-40  # S. America

# lat1,lat2,lon1,lon2 = 0,10,0,10
#-------------------------------------------------------------------------------

htype,years,months,first_file,num_files = 'ha',[],[],100,20
# htype,years,months,first_file,num_files = 'h1',[],[],0,0
# htype,years,months,first_file,num_files = 'h1',[],[],0,5
# htype,years,months,first_file,num_files = 'h0',[],[6,7,8],0,0
# htype,years,months,first_file,num_files = 'h0',[],[],0,10*12
# htype,years,months,first_file,num_files = 'h0',[],[],5*12,5*12


use_remap,remap_grid = False,'90x180' # 90x180 / 180x360

plot_diff,add_diff = False,True

use_snapshot,ss_t    = False,1
chk_significance     = False
print_stats          = True
var_x_case           = False
num_plot_col         = 1#len(case)
use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if case==[]: raise ValueError('ERROR - case list is empty!')
num_var,num_case = len(var),len(case)

# subtitle_font_height = 0.02 - 0.0012*num_var - 0.0014*(num_case+int(add_diff))
# subtitle_font_height = max(subtitle_font_height,0.008)
subtitle_font_height = 0.01

diff_base = 0
if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
# if 'lev' not in vars(): lev = np.array([0])

if 'scrip_file_path' not in locals(): scrip_file_path = None

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
# npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.01
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

# res.mpProjection = 'Mollweide'

def get_comp(case):
   comp = 'eam'
   if 'INCITE2019' in case: comp = 'cam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'MAML' in case: comp = 'eam_0001'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   glb_avg_list = []
   std_list,cnt_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):

      # print(hc.tcolor.RED+'WARNING - code was tampered with! Be careful...'+hc.tcolor.ENDC)
      # if c==0: htype,years,months,first_file,num_files = 'h0',[],[],40*12,5*12
      # if c==1: htype,years,months,first_file,num_files = 'h0',[],[],50*12,5*12

      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp,
                          populate_files=True )
      case_obj.set_coord_names(var[v])

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      # if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      if use_remap or ('CESM' in case[c] and 'ne30' not in case[c]):
         lat = case_obj.load_data('lat',component=get_comp(case[c]),htype=htype,
                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
         lon = case_obj.load_data('lon',component=get_comp(case[c]),htype=htype,
                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      # elif case_obj.obs:
      #    lat = case_obj.load_data('lat',component=comp,htype=htype)
      #    lon = case_obj.load_data('lon',component=comp,htype=htype)
      else:
         aname = case_obj.area_name
         area = case_obj.load_data(aname,component=get_comp(case[c]),htype=htype)

      with dask.config.set(**{'array.slicing.split_large_chunks': True}):
         data = case_obj.load_data(var[v], component=get_comp(case[c]),htype=htype,ps_htype=htype,
                                         years=years,months=months,lev=lev,
                                         first_file=first_file,num_files=num_files,
                                         use_remap=use_remap,remap_str=f'remap_{remap_grid}')

      ### special precipitation water path variables for Greg
      if var[v] in ['TGPRCLWP','TGPRCIWP']:
         if var[v]=='TGPRCLWP': tvar = 'TGCLDLWP'
         if var[v]=='TGPRCIWP': tvar = 'TGCLDIWP'
         tmp_data = case_obj.load_data(tvar, component=get_comp(case[c]),htype=htype,
                                      first_file=first_file,num_files=num_files)
         data = data / tmp_data
      

      if case[c]=='ERAi': erai_lat,erai_lon = data['lat'],data['lon']

      # Special handling of CRM grid variables
      if 'crm_nx' in data.dims : data = data.mean(dim=('crm_nx','crm_ny')).isel(crm_nz=15)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         if use_snapshot:
            data = data.isel(time=ss_t)
            print(hc.tcolor.RED+'WARNING - plotting snapshot!!!'+hc.tcolor.ENDC)
         else:
            if chk_significance :
               std_list.append( data.std(dim='time').values )
               cnt_list.append( float(len(data.time)) )
            data = data.mean(dim='time')

            # print(hc.tcolor.RED+'WARNING - calculating std deviation instead of mean!!!'+hc.tcolor.ENDC)
            # data = data.std(dim='time')


      # if 'DMSE_' in var[v]: 
      #    print(hc.tcolor.RED+'WARNING - plotting absolute value of DMSE terms!!!'+hc.tcolor.ENDC)
      #    data = np.absolute(data)
      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
         # glb_avg_list.append(gbl_mean)
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      # print(); print(data)

      if case[c]=='TRMM' and 'lon1' not in locals(): 
         data_list.append( ngl.add_cyclic(data.values) )
      else:
         data_list.append( data.values )

      if 'area' in locals() : area_list.append( area.values )
      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Check significance using t-test - https://stattrek.com/hypothesis-test/difference-in-means.aspx
   #------------------------------------------------------------------------------------------------
   if plot_diff and chk_significance :
      print('\n  Checking significance of differences...')
      sig_list = []
      for c in range(1,num_case):
         t_crit=1.96  # 2-tail test w/ inf dof & P=0.05
         t_stat = calc_t_stat(D0=data_list[0],D1=data_list[c],
                              S0=std_list[0],S1=std_list[c],
                              N0=cnt_list[0],N1=cnt_list[c],t_crit=t_crit)

         sig_list.append( np.where( np.absolute(t_stat) > t_crit, 1, 0 ) )

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      case_obj.set_coord_names(var[v])
      if 'AQUA-RRM-TEST' in case[c]: case_obj.grid = 'RRM_cubeface_grad_ne4x5_pg2'
      if case_grid[c] is not None: case_obj.grid = case_grid[c]

      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      if var[v] in ['P-E']                      : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = "CBR_wet"
      if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['DYN_QLIQ']                 : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'BlueWhiteOrangeRed'
      # if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
      # if var[v] in ['TS','PS'] : tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      if var[v] in ['U','V','UBOT','VBOT','U850','V850','U200','V200',
                    'MMF_CVT_TEND_T','MMF_CVT_TEND_Q']: 
         tres.cnFillPalette = "BlueWhiteOrangeRed"
         # tres.cnFillPalette = 'MPL_RdYlBu'

      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(2,20+2,2)
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(5,100+5,5) # for std dev
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.logspace( -2, 1.4, num=60).round(decimals=2)
      # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      if var[v]=='P-E'                 : tres.cnLevels = np.linspace(-10,10,21)
      if var[v]=='RH'                  : tres.cnLevels = np.arange(10,100+1,1)
      if var[v]=='TS'                  : tres.cnLevels = np.arange(-55,35+2,2)
      # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(1,30+1,1)*1e-2
      if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2)

      if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(0.005,0.205,0.01)
      if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.arange(0.01,0.25,0.015)

      # if var[v] in ['TGPRCLWP','TGPRCIWP']: tres.cnLevels = np.arange(0.05,1.,0.05)

      if var[v]=="DYN_QLIQ"            : tres.cnLevels = np.logspace( -6, -4, num=40)
      if var[v]=="CLDLIQ"              : tres.cnLevels = np.logspace( -7, -3, num=60)
      if var[v]=='SPTLS'               : tres.cnLevels = np.linspace(-0.001,0.001,81)
      if var[v]=="PHIS"                : tres.cnLevels = np.arange(-0.01,0.01+0.001,0.001)
      if 'MMF_MC' in var[v]            : tres.cnLevels = np.linspace(-1,1,21)*0.01
      if 'OMEGA' in var[v]             : tres.cnLevels = np.linspace(-1,1,21)*0.6

      # if var[v] in ['MMF_DU','MMF_DV','ZMMTU','ZMMTV','uten_Cu','vten_Cu']:
      #    # tres.cnLevels = np.arange(-20,20+1,1)
      #    tres.cnLevels = np.linspace(-2,2,11)

      if var[v] in ['MMF_DU','MMF_DV']: 
         tres.cnLevels = np.linspace(-2,2,11)
         tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

      # if 'DMSE_' in var[v]: 
      #    tres.cnFillPalette = "BlueWhiteOrangeRed"
      #    tres.cnLevels = np.linspace(-0.02,0.02,61)
         # tres.cnLevels = np.logspace( -4, -1, num=20)

      if var[v] in ['U','V']: 
         if plot_diff and not add_diff and c in diff_case :
            if lev==850: tres.cnLevels = np.arange( -8, 8+1,1)
            if lev==200: tres.cnLevels = np.arange(-16,16+2,2)
         else:
            if lev==850: tres.cnLevels = np.arange(-20,20+2,2)
            if lev==200: tres.cnLevels = np.arange(-60,60+6,6)

      ### print color levels
      # if hasattr(tres,'cnLevels') : 
      #    print(f'\ntres.cnLevels:')
      #    msg = ''
      #    for cl in range(len(tres.cnLevels)):
      #       msg += f'{tres.cnLevels[cl]}, '
      #    print(f'[ {msg} ]\n')
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 41
         aboutZero = False
         if var[v] in ['SPTLS','SPQTLS','U','V','VOR','DIV',
                       'U850','V850','U200','V200',
                       'MMF_CVT_TEND_T','MMF_CVT_TEND_Q',] : 
            aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      var_str = var[v]
      if var[v]=='PRECT':     var_str = 'Precipitation'
      # if var[v]=='TMQ':       var_str = 'CWV'
      if var[v]=='TGCLDLWP':  var_str = 'Liq Water Path'
      if var[v]=='TGCLDIWP':  var_str = 'Ice Water Path'
      if var[v]=='MMF_DU':     var_str = 'CRM U Tendency'

      lev_str = None
      if lev>0: lev_str = f'{lev}mb'
      if lev<0: lev_str = f'k={(lev*-1)}'
      # if lev_str is not None and var[v] in ['U','V','OMEGA','T','Q','Z3']:
      if lev_str is not None:
         var_str = f'{lev_str} {var_str}'

      #-------------------------------------------------------------------------
      # turn off land boundaries for RCE and aquaplanet cases
      #-------------------------------------------------------------------------
      if 'RCE' in case[c] or 'AQP' in case[c] :
         tres.mpOutlineBoundarySets = 'NoBoundaries'
         tres.mpCenterLonF = 0.
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_common_label_bar: 
         tres.lbLabelBarOn = False
      else:
         tres.lbLabelBarOn = True

      if use_remap or case_obj.obs \
         or ('CESM' in case[c] and 'ne30' not in case[c]) :
         if case[c]=='ERAi': lat,lon = erai_lat,erai_lon
         hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
      else:
         if case[c]=='v2.LR.amip_0101' : case_obj.grid = 'ne30pg2'
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_path)
         
         
      if plot_diff and c==diff_base : base_name = name[c]

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         # plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres) 
         
         #----------------------------------------------------------------------
         # set plot subtitles
         #----------------------------------------------------------------------
         ctr_str = ''
         if 'name' not in vars(): case_name = case_obj.short_name
         if 'name'     in vars(): case_name = name[c]
            
         # ctr_str = 'Mean: '+'%.2f'%gbl_mean+' [mm/day]'

         # if 'lev' in locals() :
         #    if type(lev) not in [list,tuple]:
         #       if lev>0: ctr_str = f'{lev} mb'

         if var[v] in ['TGPRCLWP','TGPRCIWP']: ctr_str = 'ratio to non-precip'

         if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'

         hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         
         data_list[c] = data_list[c] - data_baseline.values
         
         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
         
         tres.cnLevelSelectionMode = "ExplicitLevels"

         if hasattr(tres,'cnLevels') : del tres.cnLevels
         if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if var[v] in ['MMF_DU','MMF_DV']       : tres.cnLevels = np.linspace(-2,2,11)
         # if var[v]=='TS'                        : tres.cnLevels = np.arange(-40,40+5,5)/1e1
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         
         ### override the level settings and just use auto
         # tres.cnLevelSelectionMode = "AutomaticLevels"

         if use_remap:
            hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
         else:
            hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_path)
            
         # if use_common_label_bar: 
         #    tres.lbLabelBarOn = False
         # else:
         #    tres.lbLabelBarOn = True
         tres.lbLabelBarOn = True

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+(num_case-1)
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         # plot[ipd] = ngl.contour_map(wks,data_list[c],tres)
         plot[ipd] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres)
         
         #-----------------------------------
         ####################################
         # overlay stippling for significance
         ####################################
         #-----------------------------------
         if chk_significance:
            sres = res_stippling
            if use_remap:
               lat2D =               np.repeat( lat.values[...,None],len(lon),axis=1)
               lon2D = np.transpose( np.repeat( lon.values[...,None],len(lat),axis=1) )
               sres.sfXArray      = lon2D
               sres.sfYArray      = lat2D
            else:
               sres.sfXArray      = lon
               sres.sfYArray      = lat
            if np.sum(sig_list[c-1]) != 0:
               ngl.overlay( plot[ip], ngl.contour(wks,sig_list[c-1],sres) )
         #-----------------------------------
         ####################################
         #-----------------------------------
         ctr_str = ''
         # case_name = name[c]+' - '+base_name
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name

         ctr_str = 'Diff'
         if glb_avg_list != []: 
            glb_diff = glb_avg_list[c] - glb_avg_list[diff_base]
            ctr_str += f' ({glb_diff:6.4})'
         
         # hs.set_subtitles(wks, plot[ipd], case_name, '', var_str+' (Diff)', font_height=subtitle_font_height)
         hs.set_subtitles(wks, plot[ipd], case_name, ctr_str, var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if plot_diff : num_case = num_case+len(diff_case)   # use this to plot both before and after diff

# title_str = None
# title_str = 'ANN'
# if months==[1,2,12]: title_str = 'DJF'
# if months==[6,7,8]: title_str = 'JJA'
# if title_str is not None:
#    textres =  ngl.Resources()
#    textres.txFontHeightF =  0.025
#    ngl.text_ndc(wks,title_str,0.5,.7,textres)

num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]


if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### use common label bar
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
