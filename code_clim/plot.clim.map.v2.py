# v2 is a cleaned up version of v1 - functionality is mostly the same
import os, ngl, subprocess as sp, numpy as np, xarray as xr, dask, copy, string, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
host = hc.get_host()
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
scrip_file_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,c=None,scrip_file=None):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); 
   case_dir.append(p); case_sub.append(s);
   scrip_file_list.append(scrip_file)
#-------------------------------------------------------------------------------
var,lev_list,var_str = [],[],[]
def add_var(var_name,lev=0,s=None): 
   var.append(var_name); lev_list.append(lev); 
   if s is None:
      var_str.append(var_name)
   else:
      var_str.append(s)
#-------------------------------------------------------------------------------
fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.map.v2','png'
#-------------------------------------------------------------------------------
if host=='olcf':

   scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'

   ### PAM dev tests
   # add_case('E3SM.PAM-DEV-2023-37.GNUGPU.ne30pg2_oECv3.F2010-MMF1',n='MMF1')
   # add_case('E3SM.PAM-DEV-2023-37.GNUGPU.ne30pg2_oECv3.F2010-MMF2',n='MMF2')
   tmp_scratch = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/'
   # add_case('E3SM.2024-PAM-ENS-01.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_10.DPP_01.HDT_0060.GDT_1200',n='MMF2 01',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-ENS-02.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_10.DPP_01.HDT_0060.GDT_1200',n='MMF2 02',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-ENS-02.GNUGPU.ne30pg2_oECv3.F2010-MMF2.NN_64.CDT_10.DPP_01.HDT_0060.GDT_1200',n='MMF2 02',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-ENS-03.GNUGPU.ne30pg2_oECv3.F2010-MMF2.NN_64.CDT_10.DPP_01.HDT_0060.GDT_1200',n='MMF2 03',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-ENS-04.GNUGPU.ne30pg2_oECv3.F2010-MMF2.NN_64.CDT_10.DPP_01.HDT_0060.GDT_1200',n='MMF2 04',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-ENS-05.GNUGPU.ne30pg2_oECv3.F2010-MMF2.NN_64.CDT_10.DPP_01.HDT_0060.GDT_1200',n='MMF2 05 60',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-ENS-05.GNUGPU.ne30pg2_oECv3.F2010-MMF2.NN_64.CDT_10.DPP_01.HDT_0120.GDT_1200',n='MMF2 05 120',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-ENS-05.GNUGPU.ne30pg2_oECv3.F2010-MMF2.NN_128.CDT_10.DPP_01.HDT_0180.GDT_1200',n='MMF2 05 180',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-00.ne30pg2_oECv3.F2010-MMF1',n='MMF1 00',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-00.ne30pg2_oECv3.F2010-MMF2',n='F2010-MMF2',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-01.ne30pg2_oECv3.F2010-MMF2',n='MMF2 01',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-02.ne30pg2_oECv3.F2010-MMF2.L72',n='MMF2 02 L72',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF1',      n='MMF1',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF2',      n='MMF2 nx=45',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-05.ne30pg2_oECv3.F2010-MMF2.NX_65',n='MMF2 nx=65',p=tmp_scratch,s='run')
   # add_case('E3SM.2024-PAM-CHK-05.ne30pg2_oECv3.F2010-MMF2.NX_75',n='MMF2 nx=75',p=tmp_scratch,s='run')
   
   add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF2',      n='MMF2 CHK-04',p=tmp_scratch,s='run')
   add_case('E3SM.2024-PAM-CHK-06.ne30pg2_oECv3.F2010-MMF2',      n='MMF2 CHK-06',p=tmp_scratch,s='run')

   ### coupled historical experiments (2023)
   # add_case('E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='E3SM-MMF',s='archive/atm/hist')
   # add_case('v2.LR.historical_0101',                                  n='E3SMv2',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/e3smv2_historical',s='archive/atm/hist')
   # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO',                      n='E3SMv2',      c='black')
   # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',                       n='E3SM-MMF',    c='red')
   # add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QI_3E-05',n='E3SM-MMF alt',c='blue')

   ### 2023 coriolis test
   # add_case('E3SM.2023-coriolis-test.GNUGPU.ne30pg2_oECv3.F2010-MMF1.coriolis-off',n='MMF 2D')
   # add_case('E3SM.2023-coriolis-test.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_32_1.coriolis-on',n='MMF 2D + non-trad cor')

#-------------------------------------------------------------------------------
if host=='nersc':
   ### L80 ensemble
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
   # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_0.10.HD_1.50',n='',p=tmp_path,s=tmp_sub)

   ### SciDAC MMF tests w/ new vertical grids
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L60',n='MMF-SAM L60',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L64',n='MMF-SAM L64',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L72',n='MMF L72',p=tmp_path,s=tmp_sub)

   ### PAM tests w/ new vertical grids
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # add_case('E3SM.2023-PAM-DEV-00.ne30pg2_EC30to60E2r2.F2010-MMF2.L60',n='MMF-PAM L60',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-DEV-00.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='MMF-PAM L64',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-DEV-01.ne30pg2_EC30to60E2r2.F2010-MMF2.L64',n='MMF-PAM L64',p=tmp_path,s=tmp_sub)

   ### PAM update tests
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   # add_case('E3SM.2023-PAM-ENS-03.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_02.HDT_0060.GDT_1200',n='PAM E03 dt5 dpp2 hdt060',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-03.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_02.HDT_0120.GDT_1200',n='PAM E03 dt5 dpp2 hdt120',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-03.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_05.HDT_0060.GDT_1200',n='PAM E03 dt5 dpp5 hdt060',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-03.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_05.HDT_0120.GDT_1200',n='PAM E03 dt5 dpp5 hdt120',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-03.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_08.DPP_02.HDT_0060.GDT_1200',n='PAM E03 dt8 dpp2 hdt060',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-03.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_08.DPP_02.HDT_0120.GDT_1200',n='PAM E03 dt8 dpp2 hdt120',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-04.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_02.HDT_0060.GDT_1200',n='PAM E04 dt5 dpp2 hdt060',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-04.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_02.HDT_0090.GDT_1200',n='PAM E04 dt5 dpp2 hdt090',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-05.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_02.HDT_0060.GDT_1200',n='PAM E05 dt5 dpp2 hdt060',p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-PAM-ENS-05.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_05.DPP_02.HDT_0090.GDT_1200',n='PAM E05 dt5 dpp2 hdt090',p=tmp_path,s=tmp_sub)

   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne4pg2_scrip.nc'
   # add_case('E3SM.2023-PAM-ENS-07.GNUGPU.ne4pg2_ne4pg2.F2010-MMF2.CDT_08.DPP_04.HDT_0060.GDT_1200',p=tmp_path,s=tmp_sub,c='red')
   # add_case('E3SM.2023-PAM-ENS-08.GNUGPU.ne4pg2_ne4pg2.F2010-MMF2.CDT_08.DPP_04.HDT_0060.GDT_1200',p=tmp_path,s=tmp_sub,c='green')
   # add_case('E3SM.2023-PAM-ENS-09.GNUGPU.ne4pg2_ne4pg2.F2010-MMF2.CDT_08.DPP_04.HDT_0060.GDT_1200',p=tmp_path,s=tmp_sub,c='blue')

   ### SciDAC L80 ensemble
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
   # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_10.HD_1.00',n='',p=tmp_path,s=tmp_sub) # defaults
   # add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.35.CF_20.HD_1.00',n='',p=tmp_path,s=tmp_sub)

   ### 2023 throttling experiments
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.FAQP-MMF1.NXY_8_8'     ,n='8x8',   p=tmp_path,s=tmp_sub)
   # # add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.FAQP-MMF1.NXY_16_8'    ,n='16x8',  p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.FAQP-MMF1.NXY_32_8'    ,n='32x8',  p=tmp_path,s=tmp_sub)
   # # add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.FAQP-MMF1.NXY_64_8'    ,n='64x8',  p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.FAQP-MMF1.NXY_128_8'   ,n='128x8', p=tmp_path,s=tmp_sub)
   # # add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.FAQP-MMF1.NXY_256_8'   ,n='256x8', p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.FAQP-MMF1.NXY_512_8'   ,n='512x8', p=tmp_path,s=tmp_sub)

   ### 2024 nontraditional coriolis
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','archive/atm/hist'
   # add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-off.NCT-off'               ,n='EAM ',       p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-off'                ,n='EAM NHS',    p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-on'                 ,n='EAM NHS+NCT',p=tmp_path,s=tmp_sub)
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-off.NCT-off' ,n='MMF ',       p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-off'  ,n='MMF NHS',    p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-on'   ,n='MMF NHS+NCT',p=tmp_path,s=tmp_sub)
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','archive/atm/hist'
   # add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.NXY_64_1.NHS-off.NCT-off' ,n='MMF ',       p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.NXY_64_1.NHS-on.NCT-off'  ,n='MMF NHS',    p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.NXY_64_1.NHS-on.NCT-on'   ,n='MMF NHS+NCT',p=tmp_path,s=tmp_sub)


   ### 2024 AQP/aqua CESS runs with grid sensitivity
   # tmp_path_cpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
   # tmp_path_gpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
   # scrip_file_root = '/global/cfs/projectdirs/m3312/whannah/HICCUP/files_grid/'
   # add_case('E3SM.2024-AQP-CESS-00.FAQP.GNUCPU.ne30pg2_ne30pg2.NN_16.SSTP_0K',n='AQP EAM +0K',p=tmp_path_cpu,s='run',scrip_file=f'{scrip_file_root}/scrip_ne30pg2.nc')
   # add_case('E3SM.2024-AQP-CESS-00.FAQP.GNUCPU.ne30pg2_ne30pg2.NN_16.SSTP_4K',n='AQP EAM +4K',p=tmp_path_cpu,s='run',scrip_file=f'{scrip_file_root}/scrip_ne30pg2.nc')

   ### ClimSim hybrid tests
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne4pg2_scrip.nc'
   # tmp_path,tmp_sub = '/pscratch/sd/s/sungduk/e3sm_mlt_scratch/EXP1/ALL','run'
   # add_case('mlp-001' ,n='mlp-001',p=tmp_path,s=tmp_sub)

   ### Jim's L96 test for scidac
   # tmp_path,tmp_sub = '/global/cfs/cdirs/m4310/benedict/forWalter',''
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   # add_case('20240422.v3.F20TR.ne30pg2_r05_IcoswISC30E3r5.L96',n='L96',p=tmp_path,s=tmp_sub)

   ### test run for 2024 E3SM tutorial
   # tmp_path = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   # add_case('E3SM.2024-E3SM-tutorial-hindcast.2023-09-08',n='E3SM',p=tmp_path,s='E3SM.2024-E3SM-tutorial-hindcast.2023-09-08/run')

   ### RCEMIP phase 2 production runs
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   # tmp_path_cpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
   # tmp_path_gpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
   # tmp_sub = 'run'
   # subtitle_font_height = 0.006
   # add_case('E3SM.2024-RCEMIP2.FRCE_295'                 ,n='FRCE_295',                 p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE_300'                 ,n='FRCE_300',                 p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE_305'                 ,n='FRCE_305',                 p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_295dT1p25'        ,n='FRCE-MW_295dT1p25',        p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT0p625'       ,n='FRCE-MW_300dT0p625',       p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT1p25'        ,n='FRCE-MW_300dT1p25',        p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT2p5'         ,n='FRCE-MW_300dT2p5',         p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_305dT1p25'        ,n='FRCE-MW_305dT1p25',        p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_295'            ,n='FRCE-MMF1_295',            p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_300'            ,n='FRCE-MMF1_300',            p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_305'            ,n='FRCE-MMF1_305',            p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_295dT1p25'   ,n='FRCE-MW-MMF1_295dT1p25',   p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT0p625'  ,n='FRCE-MW-MMF1_300dT0p625',  p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT1p25'   ,n='FRCE-MW-MMF1_300dT1p25',   p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT2p5'    ,n='FRCE-MW-MMF1_300dT2p5',    p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_305dT1p25'   ,n='FRCE-MW-MMF1_305dT1p25',   p=tmp_path_gpu,s=tmp_sub)

   # # interlaced RCEMIP cases for better plot organization
   # add_case('E3SM.2024-RCEMIP2.FRCE_295'                 ,n='FRCE_295',                 p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_295'            ,n='FRCE-MMF1_295',            p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE_300'                 ,n='FRCE_300',                 p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_300'            ,n='FRCE-MMF1_300',            p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE_305'                 ,n='FRCE_305',                 p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_305'            ,n='FRCE-MMF1_305',            p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_295dT1p25'        ,n='FRCE-MW_295dT1p25',        p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_295dT1p25'   ,n='FRCE-MW-MMF1_295dT1p25',   p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT0p625'       ,n='FRCE-MW_300dT0p625',       p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT0p625'  ,n='FRCE-MW-MMF1_300dT0p625',  p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT1p25'        ,n='FRCE-MW_300dT1p25',        p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT1p25'   ,n='FRCE-MW-MMF1_300dT1p25',   p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT2p5'         ,n='FRCE-MW_300dT2p5',         p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT2p5'    ,n='FRCE-MW-MMF1_300dT2p5',    p=tmp_path_gpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW_305dT1p25'        ,n='FRCE-MW_305dT1p25',        p=tmp_path_cpu,s=tmp_sub)
   # add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_305dT1p25'   ,n='FRCE-MW-MMF1_305dT1p25',   p=tmp_path_gpu,s=tmp_sub)
   
   
   
   
   
   
   
   

   ### 2024 CRM domain tests for Andrea Jenney
   # pscratch,psub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_32x1.DX_1000',  n='MMF nx=32x1 dx=1km',  p=pscratch,s=psub)
   # # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_64x1.DX_1000',  n='MMF nx=64x1 dx=1km',  p=pscratch,s=psub)
   # # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_128x1.DX_1000', n='MMF nx=128x1 dx=1km', p=pscratch,s=psub)
   # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_256x1.DX_1000', n='MMF nx=256x1 dx=1km', p=pscratch,s=psub)
   # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_32x1.DX_4000',  n='MMF nx=32x1 dx=4km',  p=pscratch,s=psub)
   # # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_64x1.DX_4000',  n='MMF nx=64x1 dx=4km',  p=pscratch,s=psub)
   # # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_128x1.DX_4000', n='MMF nx=128x1 dx=4km', p=pscratch,s=psub)
   # add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_256x1.DX_4000', n='MMF nx=256x1 dx=4km', p=pscratch,s=psub)



   ### 2024 Rotating RCE for Da Yang
   # subtitle_font_height = 0.008
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
   # # add_case('E3SM.2024-RCEROT.FRCEROT-MMF1_320.NX_128x1.DX_1000', n='MMF RCEROT 320K', p=tmp_path,s=tmp_sub)
   # # add_case('E3SM.2024-RCEROT.FRCEROT-MMF1_300.NX_128x1.DX_1000', n='MMF RCEROT 300K', p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-RCEROT-01.FRCEROT-MMF1_320.NX_128x1.DX_1000', n='MMF RCEROT 300K', p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-RCEROT-02.FRCEROT-MMF1_320.NX_128x1.DX_1000', n='MMF RCEROT 320K', p=tmp_path,s=tmp_sub)

   ### 2024 relaxed slab ocean tests
   # subtitle_font_height = 0.008
   scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
   add_case('E3SM.2024-RSO-test-04.F2010',    n='E3SM F2010',     p=tmp_path,s=tmp_sub)
   add_case('E3SM.2024-RSO-test-04.F2010-RSO',n='E3SM F2010+RSO', p=tmp_path,s=tmp_sub)

   ### 2024 ZM gather test
   # scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
   # tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
   # add_case('E3SM.2024-ZM-test-00.F2010',n='ZM-test-00', p=tmp_path,s=tmp_sub)
   # add_case('E3SM.2024-ZM-test-01.F2010',n='ZM-test-01', p=tmp_path,s=tmp_sub)

#-------------------------------------------------------------------------------
if host=='anl':
   ### v3 candidate w/ smoothed L72 (chrysalis)
   add_case('20230127.amip.v3atm.FourthSmoke.chrysalis',          n='v3 '     ,g='ne30pg2',p='/lcrc/group/e3sm/ac.golaz/E3SMv3_dev'  ,s='archive/atm/hist')
   add_case('20230214.amip.v3atm.FourthSmoke.chrysalis.L72-nsu40',n='v3 L72sm',g='ne30pg2',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='run')

#-------------------------------------------------------------------------------

add_var('TS')
add_var('SST')
add_var('PRECT',s='Precipitation')
# add_var('TGCLDLWP',s='Liq Water Path')
# add_var('TGCLDIWP',s='Ice Water Path')
# add_var('Z3',lev=-63)
# add_var('PSL')
# add_var('PRECSC')
# add_var('PRECC'); add_var('PRECL')
# add_var('P-E')
# add_var('TMQ')
# add_var('LHFLX')
# add_var('SHFLX')
# add_var('TS')
# add_var('NET_TOA_RAD')
# add_var('RESTOM')
# add_var('FLUTOA')
# add_var('FSNT'); add_var('FLNT')
# add_var('FSNS'); add_var('FLNS')
# add_var('LWCF'); add_var('SWCF')
# add_var('U10')
# add_var('TBOT')
# add_var('QBOT')
# add_var('TAUX')
# add_var('WSPD_BOT')

# add_var('T',     lev=500)
# add_var('U',     lev=500)
# add_var('CLDLIQ',lev=500)
# add_var('CLDICE',lev=500)

# add_var('Q',     lev=900)
# add_var('Q',     lev=700)
# add_var('Q',     lev=500)


### use for h0
# add_var('U',lev=975)
# add_var('V',lev=975)
# add_var('U',lev=850,s='U850')
# add_var('U',lev=500)
# add_var('U',lev=200,s='U200')

# add_var('OMEGA',lev=500)


#-------------------------------------------------------------------------------
# lat1,lat2 = -30,30
# lat1,lat2,lon1,lon2 = -10,70,180,340             # N. America
# lat1,lat2,lon1,lon2 = 0,40,360-90,360-10             # Atlantic/carribean
# lat1,lat2,lon1,lon2 = 25, 50, 360-125, 360-75    # CONUS
# lat1,lat2,lon1,lon2 = -40,40,90,240              # MC + West Pac
# lat1,lat2,lon1,lon2 = 0,60,50,120                # India
# lat1,lat2,lon1,lon2 = -40,30,360-120,360-40  # S. America
# lat1,lat2,lon1,lon2 = -20,15,360-90,360-60  # S. America - zoom in on Andes
#-------------------------------------------------------------------------------

# htype,years,months,first_file,num_files = 'h1',[],[],180,1#*10
htype,years,months,first_file,num_files = 'h0',[],[],0,0#12*5
# htype,years,months,first_file,num_files = 'h0',[],[],-1,1
# htype,years,months,first_file,num_files = 'h1',[],[],0,0
# htype,years,months,first_file,num_files = 'h5',[],[],0,-1

use_remap,remap_grid = False,'90x180'

plot_diff,add_diff = True,True

use_snapshot,ss_t    = False,-1
print_stats          = True
var_x_case           = True

num_plot_col         = 2 # len(case)

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if case==[]: raise ValueError('ERROR - case list is empty!')
num_var,num_case = len(var),len(case)

if 'subtitle_font_height' not in locals(): subtitle_font_height = 0.01

diff_base = 0

if 'scrip_file_path' not in locals(): scrip_file_path = None

wkres = ngl.Resources()
npix = 2048*2; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in locals() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in locals() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.01
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

# res.mpProjection = 'Mollweide'
res.mpProjection = 'Robinson'
# res.mpProjection = "Orthographic"

def get_comp(case):
   comp = 'eam'
   if 'INCITE2019' in case: comp = 'cam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'MAML' in case: comp = 'eam_0001'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list = []
   glb_avg_list = []
   lat_list,lon_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp,
                          populate_files=True )
      case_obj.set_coord_names(var[v])
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      # if use_remap or ('CESM' in case[c] and 'ne30' not in case[c]):
      #    lat = case_obj.load_data('lat',component=get_comp(case[c]),htype=htype,
      #                            use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      #    lon = case_obj.load_data('lon',component=get_comp(case[c]),htype=htype,
      #                            use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      # else:
      #    aname = case_obj.area_name
      #    area = case_obj.load_data(aname,component=get_comp(case[c]),htype=htype)

      with dask.config.set(**{'array.slicing.split_large_chunks': True}):
         data = case_obj.load_data(var[v], component=get_comp(case[c]),htype=htype,ps_htype=htype,
                                         years=years,months=months,lev=lev,
                                         first_file=first_file,num_files=num_files,
                                         use_remap=use_remap,remap_str=f'remap_{remap_grid}')

         # xx = data.isel(ncol=100).values
         # print()
         # for x in xx: print(x)
         # print()
         # exit()
      #-------------------------------------------------------------------------
      # Special handling of various specific circumstances
      #-------------------------------------------------------------------------
      if case[c]=='ERAi': erai_lat,erai_lon = data['lat'],data['lon']
      if 'crm_nx' in data.dims : data = data.mean(dim=('crm_nx','crm_ny')).isel(crm_nz=15)
      if 'lev' in data.dims : data = data.isel(lev=0)
      #-------------------------------------------------------------------------
      # print stats before time averaging
      #-------------------------------------------------------------------------
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)
      #-------------------------------------------------------------------------
      # average over time dimension
      #-------------------------------------------------------------------------
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6,print_span=True, print_length=False)
         if use_snapshot:
            data = data.isel(time=ss_t); print(hc.tcolor.RED+'WARNING - snapshot mode enabled'+hc.tcolor.ENDC)
         else:
            data = data.mean(dim='time')
      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
         # glb_avg_list.append(gbl_mean)
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      if case[c]=='TRMM' and 'lon1' not in locals(): data = ngl.add_cyclic(data.values)
      
      data_list.append( data.values )
      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()
   #----------------------------------------------------------------------------
   # calculate common limits for consistent contour levels
   #----------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])
   #----------------------------------------------------------------------------
   # Plot averaged data
   #----------------------------------------------------------------------------
   for c in range(num_case):

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp,
                          populate_files=False )
      case_obj.set_coord_names(var[v])
      #-------------------------------------------------------------------------
      # Set color palette
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      if var[v] in ['P-E']                      : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = "CBR_wet"
      if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['DYN_QLIQ']                 : tres.cnFillPalette = "MPL_viridis"
      # if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'BlueWhiteOrangeRed'
      # if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
      # if var[v] in ['TS','PS'] : tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      if var[v] in ['U','V','UBOT','VBOT','U850','V850','U200','V200','MMF_DU','MMF_DV']: 
         # tres.cnFillPalette = "BlueWhiteOrangeRed"
         tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

      #-------------------------------------------------------------------------
      # Set contour levels
      #-------------------------------------------------------------------------
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(4,80+4,4)
      if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(2,20+2,2)
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(5,100+5,5) # for std dev
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.logspace( -3, 2.5, num=60).round(decimals=2)
      # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      if var[v]=='P-E'                 : tres.cnLevels = np.linspace(-10,10,21)
      if var[v]=='RH'                  : tres.cnLevels = np.arange(10,100+1,1)
      # if var[v]=='TS'                  : tres.cnLevels = np.arange(-55,35+2,2)
      # if var[v]=='TS'                  : tres.cnLevels = np.arange(22,32+0.2,0.2)
      # if var[v]=='PS'                  : tres.cnLevels = np.arange(540e2,1040e2+20e2,20e2)
      if var[v]=='PS'                  : tres.cnLevels = np.arange(940,1040+1,1)*1e2
      if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(1,30+1,1)*1e-2
      if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2)

      # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(0.005,0.155,0.01)
      # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(0.005,0.205,0.01)
      # if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.arange(0.01,0.25,0.015)

      if var[v] in ['TGPRCLWP','TGPRCIWP']: tres.cnLevels = np.arange(0.05,1.,0.05)

      if var[v]=='DYN_QLIQ'            : tres.cnLevels = np.logspace( -6, -4, num=40)
      # if var[v]=='CLDLIQ'              : tres.cnLevels = np.logspace( -7, -3, num=60)
      if var[v]=='PHIS'                : tres.cnLevels = np.arange(-0.01,0.01+0.001,0.001)
      if 'MMF_MC' in var[v]            : tres.cnLevels = np.linspace(-1,1,21)*0.01
      if 'OMEGA' in var[v]             : tres.cnLevels = np.linspace(-1,1,21)*0.6
      if var[v] in ['MMF_DU','MMF_DV'] : tres.cnLevels = np.linspace(-2,2,11)

      if var[v] in ['U','V']: 
         if plot_diff and not add_diff and c!=diff_base :
            if lev==850: tres.cnLevels = np.arange( -8, 8+1,1)
            if lev==200: tres.cnLevels = np.arange(-16,16+2,2)
         else:
            if lev==850: tres.cnLevels = np.arange(-20,20+2,2)
            if lev==200: tres.cnLevels = np.arange(-60,60+6,6)
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 41
         aboutZero = False
         if var[v] in ['U','V','VOR','DIV','U850','V850','U200','V200']: 
            aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      # var_str = var[v]
      # if var[v]=='PRECT':     var_str = 'Precipitation'
      # if var[v]=='TMQ':       var_str = 'CWV'
      # if var[v]=='TGCLDLWP':  var_str = 'Liq. Water Path'
      # if var[v]=='TGCLDIWP':  var_str = 'IWP'
      # if var[v]=='MMF_DU':     var_str = 'CRM U Tendency'

      # lev_str = None
      # if lev>0: lev_str = f'{lev}mb'
      # if lev<0: lev_str = f'k={(lev*-1)}'
      # # if lev_str is not None and var[v] in ['U','V','OMEGA','T','Q','Z3']:
      # if lev_str is not None:
      #    var_str = f'{lev_str} {var_str}'

      #-------------------------------------------------------------------------
      # turn off land boundaries for RCE and aquaplanet cases
      #-------------------------------------------------------------------------
      if 'RCE' in case[c] or 'AQP' in case[c] :
         tres.mpOutlineBoundarySets = 'NoBoundaries'
         tres.mpCenterLonF = 0.
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_remap \
      or case_obj.obs \
      or ('CESM' in case[c] and 'ne30' not in case[c]) :
         if case[c]=='ERAi': lat,lon = erai_lat,erai_lon
         hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
      else:
         if scrip_file_list[c] is not None:
            tmp_scrip_file_path = scrip_file_list[c]
         elif scrip_file_path is not None:
            tmp_scrip_file_path = scrip_file_path
         else:
            raise TypeError('A valid SCRIP file path was not provided!')
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=tmp_scrip_file_path)

      tres.lbLabelBarOn = False if use_common_label_bar else True      
         
      # if plot_diff and c==diff_base : base_name = name[c]

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres) 
         
         # set plot subtitles
         ctr_str = ''
         if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'
         hs.set_subtitles(wks, plot[ip], name[c], ctr_str, var_str[v], font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c!=diff_base :
         
         data_list[c] = data_list[c] - data_baseline.values
         
         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

         if hasattr(tres,'cnLevels') : del tres.cnLevels
         # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if var[v] in ['MMF_DU','MMF_DV']       : tres.cnLevels = np.linspace(-2,2,11)
         # if var[v]=='TS'                        : tres.cnLevels = np.arange(-40,40+2,2)/1e1
         # if var[v]=='TS'                        : tres.cnLevels = np.arange(-20,20+1,1)/1e1
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         
         tres.cnLevelSelectionMode = "ExplicitLevels"
         # tres.cnLevelSelectionMode = "AutomaticLevels" # override the level settings and just use auto

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+(num_case-1)
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         tres.lbLabelBarOn = True

         plot[ipd] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres)
         
         ctr_str = 'Diff'
         if glb_avg_list != []: 
            glb_diff = glb_avg_list[c] - glb_avg_list[diff_base]
            ctr_str += f' ({glb_diff:6.4})'
         
         hs.set_subtitles(wks, plot[ipd], name[c], ctr_str, var_str[v], font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]


if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()
if use_common_label_bar: pnl_res.nglPanelLabelBar = True
### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
