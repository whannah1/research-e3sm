import os, ngl, subprocess as sp
import numpy as np, xarray as xr
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy, string
host = hc.get_host()
data_dir,data_sub = None,None
# data_dir,data_sub = '/project/projectdirs/m3312/jlee1046/E3SM/',''
#-------------------------------------------------------------------------------
# if host=='olcf':
   # ????

#-------------------------------------------------------------------------------
if host=='cori':
   
   ### physgrid validation
   # name,case,git_hash = [],[],'cbe53b'
   # # case.append(f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
   # # case.append(f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
   # # case.append(f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
   # # case.append(f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
   # case.append(f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
   # case.append(f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
   # for c in case:
   #    if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4')
   #    if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2')
   #    if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3')
   #    if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4')
   #    if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4')
   #    if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4')
   #    if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2')

   ### RGMA runs
   name,case = [],[]
   # case.append('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00');                          name.append('E3SM lo-res')
   # case.append('E3SM.RGMA.ne120pg2_r05_oECv3.FC5AV1C-H01A.00');                      name.append('E3SM hi-res')
   case.append('E3SM.RGMA.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.RADNX_4.00'); name.append('E3SM MMF')


#-------------------------------------------------------------------------------
var = []
var.append('PRECT')
# var.append('TGCLDLWP')
# var.append('TGCLDIWP')
# var.append('P-E')
# var.append('LHFLX')
# var.append('SHFLX')
# var.append('TMQ')

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.percentile.map.v1'

htype,years,months,first_file,num_files = 'h1',[],[],0,73*2
# htype,years,months,first_file,num_files = 'h1',[],[],0,2

use_remap,remap_grid = False,'180x360' # 90x180 / 180x360

plot_diff,add_diff,diff_base = False,False,0

print_stats  = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.02 - 0.0015*num_var - 0.0014*(num_case+int(add_diff))

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
if plot_diff:
   if add_diff:
      plot = [None]*(num_var*(num_case*2-1))
   else:
      plot = [None]*(num_var*(num_case))
else:
   plot = [None]*(num_var*num_case)
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2


res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.018

res.tmXBOn = False
res.tmYLOn = False

# res.mpGeophysicalLineColor = 'white'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   std_list,cnt_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+case[c])
      if use_remap:
         case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub = f'data_remap_{remap_grid}/' )
      else:
         case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub )

      # case_obj.time_freq = 'daily'
      # case_obj.time_freq = 'monthly'
      # case_obj = he.Case( name=case[c], data_dir='/global/cscratch1/sd/hewang/acme_scratch/cori-knl/' )

      tvar = var[v]
      case_obj.set_coord_names(tvar)
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if use_remap:
         data = case_obj.load_data(tvar,     htype=htype,years=years,months=months,
                                   first_file=first_file,num_files=num_files,lev=lev,
                                   use_remap=True,remap_str=f'remap_{remap_grid}')
         lat = case_obj.load_data('lat',htype=htype,num_files=1,use_remap=True,remap_str=f'remap_{remap_grid}')
         lon = case_obj.load_data('lon',htype=htype,num_files=1,use_remap=True,remap_str=f'remap_{remap_grid}')
      else:
         # area = case_obj.load_data(case_obj.area_name,htype=htype,years=years,months=months,
         #                                              first_file=first_file,num_files=num_files).astype(np.double)
         data = case_obj.load_data(tvar,              htype=htype,years=years,months=months,
                                                      first_file=first_file,num_files=num_files,lev=lev)

      # Special handling of CRM grid variables
      if 'crm_nx' in data.dims : data = data.mean(dim=('crm_nx','crm_ny')).isel(crm_nz=15)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      # stats for sanity checks
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

      hc.print_time_length(data.time,indent=' '*6)


      # calculate percentile
      # if use_remap:
      #    ?
      # else:
      percentile_data = np.zeros(len(data['ncol']))
      for n in data['ncol'].values:
         percentile_data[n] = np.percentile(data.isel(ncol=n).values,1)
         # percentile_data[n] = np.percentile(data.isel(ncol=n).values,99)

      data_list.append( percentile_data )
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      # Calculate area weighted global mean
      # if 'area' in locals() :
      #    gbl_mean = ( (data*area).sum() / area.sum() ).values 
      #    print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

      # # Calculate RMSE
      # if c==0:baseline = data
      # if c>0:
      #    rmse = np.sqrt( np.mean( np.square( data.to_masked_array() - baseline.to_masked_array() )))
      #    print(f'      Root Mean Square Error    : {rmse:6.4}')

      # if case[c]=='TRMM' and 'lon1' not in locals(): 
      #    data_list.append( ngl.add_cyclic(data.values) )
      # else:
      #    data_list.append( data.values )

      # if 'area' in locals(): area_list.append( area.values )
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub )
      case_obj.set_coord_names(tvar)

      if plot_diff and add_diff: 
         # ip = ((c+1)*2-1)*num_var+v
         ip = v*(num_case*2-1) + c
      else:
         ip = v*num_case+c
         # ip = c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      tres.cnFillPalette = "WhiteBlueGreenYellowRed"
      # if not plot_diff or c not in diff_case :
      # if var[v] in ['SPTKE']                    : tres.cnFillPalette = "BlueWhiteOrangeRed"
      # if var[v] in ['OMEGA']                    : tres.cnFillPalette = "BlueWhiteOrangeRed"
      # if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = "WhiteBlueGreenYellowRed"
      if var[v] in ['P-E']                      : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = "CBR_wet"
      if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['DYN_QLIQ']                 : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['TS']                       : tres.cnFillPalette = "BlueRed"
      if var[v] in ['U','V','UBOT','VBOT','U850','V850','U200','V200']: 
         tres.cnFillPalette = "BlueWhiteOrangeRed"


      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(5,100+5,5)
      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(5,205,5)*1e-1
      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(2,502,2)*1e-2
      # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(2,300+2,2)
      # if var[v]=='P-E'                 : tres.cnLevels = np.linspace(-10,10,21)

      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         aboutZero = False
         if var[v] in ['SPTLS','SPQTLS','U','V','VOR','DIV',
                       'U850','V850','U200','V200'] : 
            aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max,  \
                                         cint=None, max_steps=21, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = "AutomaticLevels"   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=21)
            tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      if var[v]=="PRECT"    : var_str = "Precip"
      if var[v]=="TGCLDLWP" : var_str = "LWP"
      if var[v]=="TGCLDIWP" : var_str = "IWP"

      # turn off land boundaries for aquaplanets
      if 'RCE' in case[c] or 'AQP' in case[c] :
         tres.mpOutlineBoundarySets = "NoBoundaries"
         tres.mpCenterLonF = 0.
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_remap:
         hs.set_cell_fill(tres,case_obj=case_obj,use_remap=True,lat=lat,lon=lon)
      else:
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype)

      # Hide the color bar on all but bottom panel
      # if not plot_diff :
         # tres.lbLabelBarOn = False
         # if c==num_case-1 : tres.lbLabelBarOn = True
         # if c!=num_case-1 : 
         #    tres.lbTopMarginF    =  3.
         #    tres.lbBottomMarginF = -3.
         #    tres.lbRightMarginF = 2.
         #    tres.lbLeftMarginF  = 2.
         # else:
         #    tres.lbTopMarginF    =  0.2
         #    tres.lbBottomMarginF = -0.2

      if plot_diff : 
         if c==diff_base : base_name = name[c]
         # if c in diff_case : case_name = case_name+' - '+base_name      

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 
         # plot.append( ngl.contour_map(wks,data.values,tres) )
         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 

         scripfile = case_obj.get_scrip()
         #----------------------------------------------------------------------
         #----------------------------------------------------------------------
         ctr_str = ''
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name
         # if var[v] in ['PRECT','PRECC','PRECL'] and 'gbl_mean' in vars() : 
         #    ctr_str = 'Mean: '+'%.2f'%gbl_mean+' [mm/day]'

         # if 'lev' in locals() :
         #    if type(lev) not in [list,tuple]:
         #       if lev>0: ctr_str = f'{lev} mb'
         
         hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         if add_diff: 
            ip = v*(num_case*2-1) + num_case+c-1
         else:
            ip = v*num_case+c
         data_list[c] = data_list[c] - data_baseline.values

         #if print_stats: hc.print_stat(data,name=f'{var[v]} DIFF',stat='naxsh',indent='    ')

         # tres.cnFillPalette = 'ncl_default'
         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         # tres.cnFillPalette = 'MPL_bwr'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if var[v] in ['TGCLDLWP'] : tres.cnLevels = np.arange(-28,28+4,4)*1e-3
         if var[v] in ['TGCLDIWP'] : tres.cnLevels = np.arange(-10,10+2,2)*1e-3
         if not hasattr(tres,'cnLevels') : 
            # if np.min(data_list[c]).values==np.max(data_list[c]).values : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print('WARNING: Difference is zero!')
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         
         ### override the level settings and just use auto
         # tres.cnLevelSelectionMode = "AutomaticLevels"

         if use_remap:
            hs.set_cell_fill(tres,case_obj=case_obj,use_remap=True,lat=lat,lon=lon)
         else:
            hs.set_cell_fill(tres,case_obj=case_obj)

         tres.lbLabelBarOn = True
         plot[ip] = ngl.contour_map(wks,data_list[c],tres)

         ctr_str = ''
         # case_name = name[c]+' - '+base_name
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name
         
         # hs.set_subtitles(wks, plot[len(plot)-1], case_name, '', var_str, font_height=0.01)
         # hs.set_subtitles(wks, plot[ip], case_name, '', var_str, font_height=0.01)
         
         # hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str+' (diff)', font_height=subtitle_font_height)
         hs.set_subtitles(wks, plot[ip], case_name, '(Diff)', var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
if plot_diff:
   if add_diff: 
      layout = [num_var,num_case*2-1]
   else:
      layout = [num_var,num_case]
      if num_var==1 and len(plot)==4 : layout = [2,2]
else:
   # layout = [num_case,num_var]
   layout = [num_var,num_case]

   if num_var==1 and len(plot)==4 : layout = [2,2]
   # if num_var==1 and num_case==6 : layout = [3,2]
   # if num_case==1 and num_var==4 : layout = [2,2]
   # if num_case==1 and num_var==6 : layout = [3,2]

pnl_res = hs.setres_panel()
pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
pnl_res.nglPanelFigureStringsJust        = "TopLeft"
pnl_res.nglPanelFigureStringsFontHeightF = 0.01
if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015
# pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
