import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs

### Early Science
# case = ['TRMM']
# case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ,'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]
# case = ['GPCP','TRMM','earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ,'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]
# clr = ['gray','black','red','blue']
# dsh = [0,0,0,0]

# case = ['GPCP','INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026']
# name = ['GPCP','INCITE2019']
# clr = ['black','purple']

# case = [
#     'GPCP','TRMM'
#     ,'E3SM.ne30pg2_r05_oECv3.F-MMF1.00'
#     ,'E3SM.ne30pg2_r05_oECv3.F-MMF1.SGS.00'
#     ,'E3SM.ne30pg2_r05_oECv3.F-MMF1.SFC.00'
#     ,'E3SM.ne30pg2_r05_oECv3.F-MMF1.CRM-AC.RAD-AC.00'
#     ]
# name = case.copy()
# for n,c in enumerate(case):
#    if 'F-MMF1.00'               in c: name[n] = 'MMF'
#    if 'F-MMF1.SGS.00'           in c: name[n] = 'MMF+SGS'
#    if 'F-MMF1.SFC.00'           in c: name[n] = 'MMF+SFC'
#    if 'F-MMF1.CRM-AC.RAD-AC.00' in c: name[n] = 'MMF+CRM-AC+RAD-AC'
# clr = ['gray','black','red','green','cyan','blue','purple','pink']


### physgrid validation
name,case,clr,dsh,git_hash = [],[],[],[],'cbe53b'
# case.append(f'GPCP'); name.append('GPCP'); dsh.append(0); clr.append('black')
# case.append(f'TRMM'); name.append('TRMM'); dsh.append(0); clr.append('black')
case.append(f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
case.append(f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
case.append(f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
case.append(f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
# case.append(f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
# case.append(f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
for c in case:
   if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2'); dsh.append(0); clr.append('red')
   if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3'); dsh.append(0); clr.append('green')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4'); dsh.append(0); clr.append('blue')
   if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2'); dsh.append(0); clr.append('red')

### HBR tests
name,case = [],[]
case.append('E3SM.PREQX.ne30pg2_ne30pg2.F-MMF1.RADNX_4.UW-TEST.00')  ; name.append('UW ctrl')
case.append('E3SM.PREQX.ne30pg2_ne30pg2.F-MMF1.RADNX_4.HBR-TEST.00') ; name.append('HBR ctrl')
# case.append('E3SM.PREQX.ne30pg2_ne30pg2.F-MMF1.RADNX_4.HBR-TEST.00') ; name.append('HBR expt')

var = 'PRECT'

fig_type = "png"
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.precip.bin.v1'

lat1 = -50
lat2 =  50

# htype,years,months,num_files = 'h1',[],[],-int(90/5)

# htype,years,months,num_files = 'h1',[0,1,2,3,4],[],0
# htype,years,months,num_files = 'h1',[4],[],0
htype,years,months,num_files = 'h1',[],[],1

recalculate = True

temp_dir = os.getenv("HOME")+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# use this code for checking bin sizes and range
#---------------------------------------------------------------------------------------------------
# bin_min, bin_spc, bin_spc_log, nbin_log = 0.04, 0.02, 25., 40
# # bin_min, bin_spc, bin_spc_log, nbin_log = 0.105, 0.01, 10., 100  # Gabe's 2016 paper
# bin_log_wid = np.zeros(nbin_log)
# bin_log_ctr = np.zeros(nbin_log)
# bin_log_ctr[0] = bin_min
# bin_log_wid[0] = bin_spc
# for b in range(1,nbin_log):
#    bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
#    bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
# nbin = nbin_log
# bin_coord = xr.DataArray( bin_log_ctr )
# ratio = bin_log_wid / bin_log_ctr
# for b in range(nbin_log): print('  center: {:12.6f}      width: {:12.6f}     ratio: {:6.4f}'.format(bin_log_ctr[b],bin_log_wid[b],ratio[b]))
# exit()
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 12
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.xyXStyle = "Log"

if 'clr' not in vars(): 
   clr = ['black']*num_case
   if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   

if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
frq_list = []
amt_list = []
bin_list = []
case_name = []
for c in range(num_case):

   print('\n    case: '+case[c])
   case_obj = he.Case( name=case[c], time_freq='daily' )
   if 'name' in vars():
      case_name.append( name[c] )
   else:
      case_name.append( case_obj.short_name )

   bin_tmp_file = temp_dir+"/clim.precip.bin.v1."+case[c]+"."+var+".lat1_"+str(lat1)+".lat2_"+str(lat2)+".nc"
   print('    bin_tmp_file: '+bin_tmp_file )

   if recalculate :
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
      if 'lat2' in vars() : case_obj.lat2, case_obj.lat2 = lat2, lat2

      X    = case_obj.load_data(var,htype=htype,years=years,months=months,num_files=num_files)
      area = case_obj.load_data("area",htype=htype,num_files=1)

      X = X.resample(time='D').mean(dim='time')

      wgt, *__ = xr.broadcast(area, X) 
      gbl_mean = ( (X*wgt).sum() / wgt.sum() ).values 
      #-------------------------------------------------------------------------
      # Calculate the distribution
      #-------------------------------------------------------------------------
      if var=='PRECT':
         bin_min_ctr = 0.04
         bin_min_wid = 0.02
         bin_spc_pct = 25.
         nbin = 40

      #-------------------------------------------------------------------------
      # Recreate bin_YbyX here for log bin case
      #-------------------------------------------------------------------------
      bin_min=bin_min_ctr
      bin_spc=bin_min_wid
      bin_spc_log=bin_spc_pct
      #----------------------------------------------------
      # set up bins
      bin_log_wid = np.zeros(nbin)
      bin_log_ctr = np.zeros(nbin)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      bin_coord = xr.DataArray( bin_log_ctr )

      #----------------------------------------------------
      # create output data arrays
      ntime = len(X['time']) if 'time' in X.dims else 1   
      
      shape,dims,coord = (nbin,),'bins',[('bins', bin_coord)]

      bin_cnt = xr.DataArray( np.zeros(shape,dtype=X.dtype), coords=coord, dims=dims )
      bin_amt = xr.DataArray( np.zeros(shape,dtype=X.dtype), coords=coord, dims=dims )
      bin_frq = xr.DataArray( np.zeros(shape,dtype=X.dtype), coords=coord, dims=dims )

      condition = xr.DataArray( np.full(X.shape,False,dtype=bool), coords=X.coords )

      wgt, *__ = xr.broadcast(area,X)
      wgt = wgt.transpose()

      X_area_wgt = (X*wgt) / wgt.sum()

      ones_area_wgt = xr.DataArray( np.ones(X.shape), coords=X.coords )
      ones_area_wgt = ( ones_area_wgt*wgt ) / wgt.sum()

      #----------------------------------------------------
      # Loop through bins
      for b in range(nbin):
         bin_bot = bin_log_ctr[b] - bin_log_wid[b]/2.
         bin_top = bin_log_ctr[b] + bin_log_wid[b]/2.
         condition.values = ( X.values >=bin_bot )  &  ( X.values < bin_top )
         bin_cnt[b] = condition.sum()
         if bin_cnt[b]>0 :
            bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
            bin_amt[b] = X_area_wgt.where(condition,drop=True).sum() / (bin_spc_pct/1e2)

      #----------------------------------------------------
      # use a dataset to hold all the output
      dims = ('bins',)
      bin_ds = xr.Dataset()
      bin_ds['bin_amt'] = (dims, bin_amt )
      bin_ds['bin_frq'] = (dims, bin_frq )
      bin_ds['bin_cnt'] = (dims, bin_cnt )
      # bin_ds['bin_pct'] = (dims, bin_cnt.sum(dim='ncol')/bin_cnt.sum()*1e2 )
      bin_ds.coords['bins'] = ('bins',bin_coord)
      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
      bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
   else:
      bin_ds = xr.open_dataset( bin_tmp_file )
      bin_frq = bin_ds['bin_frq']
      bin_amt = bin_ds['bin_amt']
      bin_coord = bin_ds['bins']
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   frequency = np.ma.masked_invalid( bin_frq.values )
   amount    = np.ma.masked_invalid( bin_amt.values )




   # hc.print_stat(frequency)
   # frequency = np.ma.masked_where( frequency<0.1, frequency )
   # hc.print_stat(frequency)
   
   if recalculate :
      print
      print('\n'+f'    Area weighted global mean:  {gbl_mean} ')
      print(     f'    sum of rain amount dist:    {np.sum(amount*(bin_spc_pct/1e2))} \n')

   frq_list.append( frequency    )
   amt_list.append( amount )
   bin_list.append( bin_coord )
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
unit_str = ''
res.tiXAxisString = 'Rain Rate [mm/day]'

var_str = var
if var=="PRECT"      : var_str = "Precipitation"

#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

res.xyExplicitLegendLabels = case_name
res.pmLegendDisplayMode    = "Always"
res.pmLegendOrthogonalPosF = -1.13
res.pmLegendParallelPosF   =  0.8+0.04
res.pmLegendWidthF         =  0.16
res.pmLegendHeightF        =  0.12   
res.lgBoxMinorExtentF      =  0.16   

res.trXMinF = np.min(bin_list)
res.trXMaxF = np.max(bin_list)


frq_list = np.stack(frq_list)
frq_list = np.ma.masked_where( frq_list<0.001, frq_list )
res.xyYStyle = "Log"
res.trYMinF = 0.01 # np.min(frq_list)
# res.trYMaxF = np.max(frq_list)

### plot frequency
res.tiYAxisString = 'Frequency [%]'
# plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(frq_list) ,res)  )
plot.append( ngl.xy(wks, np.stack(bin_list) , frq_list ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)


delattr(res,'xyYStyle')
delattr(res,'trYMinF')
# delattr(res,trYMaxF)

### plot rain amount
res.pmLegendDisplayMode = "Never"
res.tiYAxisString = 'Rain Amount [mm/day]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(amt_list) ,res) )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------