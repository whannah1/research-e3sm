# v1 - original
# v2 - calculate dist for each point first as a way to handle area weighting issues
import os, ngl, xarray as xr, numpy as np, glob
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
home = os.getenv("HOME")
print()
#---------------------------------------------------------------------------------------------------
case,name,clr,dsh,mrk = [],[],[],[],[]
case_dir,case_sub = [],[]
def add_case(case_in,n=None,p=None,s=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
   case_dir.append(p); case_sub.append(s)
#---------------------------------------------------------------------------------------------------
### RGMA runs
# name,case,clr,dsh = [],[],[],[]
# case.append('TRMM'); name.append('TRMM'); clr.append('black'); dsh.append(0)
# case.append('GPCP'); name.append('GPCP'); clr.append('gray'); dsh.append(0)
# case.append('GPCP'); name.append('GPCP'); clr.append('gray'); dsh.append(1)
# case.append('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00');                         name.append('E3SM lo-res');clr.append('red');   dsh.append(0)
# case.append('E3SM.RGMA.ne120pg2_r05_oECv3.FC5AV1C-H01A.00');                     name.append('E3SM hi-res');clr.append('blue');  dsh.append(0)
# case.append('E3SM.RGMA.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.RADNX_4.00');name.append('E3SM MMF')   ;clr.append('magenta');dsh.append(0)

### New CRM orientation tests
# add_case('E3SM.crm_angle_test.ne30pg2_ne30pg2.F-MMF1.BVT.CRMNX_64.CRMDX_2000.CRMDT_10.NLEV_50.CRMNZ_48.orientation_NS'  ,n='MMF NS',  c='red',  d=0)
# add_case('E3SM.crm_angle_test.ne30pg2_ne30pg2.F-MMF1.BVT.CRMNX_64.CRMDX_2000.CRMDT_10.NLEV_50.CRMNZ_48.orientation_EW'  ,n='MMF EW',  c='blue', d=0)
# add_case('E3SM.crm_angle_test.ne30pg2_ne30pg2.F-MMF1.BVT.CRMNX_64.CRMDX_2000.CRMDT_10.NLEV_50.CRMNZ_48.orientation_rand',n='MMF RAND',c='green',d=0)

### Variance transport validation
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.00',     n='MMF',     c='red',  d=0)
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_0.00',n='MMF BVT', c='blue', d=0)
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_1.00',n='MMF FVT1',c='green',d=0)

### INCITE 2022 low-cld Cess
# he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# case.append('GPCP'); name.append('GPCP'); clr.append('gray'); dsh.append(0)
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_0.00',n='MMF default', c='blue', d=1)
# add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_0K', n='MMF UP+0K', c='blue')
# add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_4K', n='MMF UP+4K', c='red')

### reduced radiation sensitivity
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00', n='nx_rad=128',c='red')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_64.00',  n='nx_rad=64' ,c='orange')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_32.00',  n='nx_rad=32' ,c='gold')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_16.00',  n='nx_rad=16' ,c='palegreen')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.00',   n='nx_rad=8'  ,c='green')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_4.00',   n='nx_rad=4'  ,c='cyan')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_2.00',   n='nx_rad=2'  ,c='blue')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_1.00',   n='nx_rad=1'  ,c='purple')

# ### coupled historical
# tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/2023-CPL','archive/atm/hist'
# add_case('E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='E3SM-MMF',p=tmp_path,s=tmp_sub,c='green')
# add_case('E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='E3SM-MMF',p=tmp_path,s=tmp_sub,c='purple')

# case.append('GPCP'); name.append('GPCP'); clr.append('gray'); dsh.append(0)
tmp_path,tmp_sub = '/global/cfs/cdirs/e3smdata/simulations/','archive/atm/hist'
add_case('v2.LR.amip_0101', n='E3SMv2 AMIP 101', d=0, c='red',   p=tmp_path,s=tmp_sub) 
tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/e3smv3_amip','archive/atm/hist'
add_case('v3.LR.amip_0101', n='E3SMv3 AMIP 101', d=0, c='blue',   p=tmp_path,s=tmp_sub)

#---------------------------------------------------------------------------------------------------

# yr1,yr2 = 1950,1959; htype,years,months,first_file,num_files = 'h1',[],[],365* 0,365*1
# yr1,yr2 = 2000,2009; htype,years,months,first_file,num_files = 'h1',[],[],365*50,365*1

# var = 'PRECT'
var = 'PRECL'

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_clim/clim.precip.bin.v2"

lat1,lat2 = -50,50
# lat1,lat2 = -30,30


# htype,years,months,first_file,num_files = 'h1',[],[],0,365
# htype,years,months,first_file,num_files = 'h1',[0,1,2,3,4],[],0,0
# htype,years,months,first_file,num_files = 'h1',[],[],12*5,12

# htype,years,months,first_file,num_files = 'h1',[],[], int((2000-1870)*365/30), 1
htype,years,months,first_file,num_files = 'h1',[2000],[], 0, 0

recalculate = True

temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# use this code for checking bin sizes and range
#---------------------------------------------------------------------------------------------------
# bin_min, bin_spc, bin_spc_log, nbin_log = 0.04, 0.02, 5., 150
# # bin_min, bin_spc, bin_spc_log, nbin_log = 0.04, 0.02, 25., 40
# # bin_min, bin_spc, bin_spc_log, nbin_log = 0.105, 0.01, 10., 100  # Gabe's 2016 paper
# bin_log_wid = np.zeros(nbin_log)
# bin_log_ctr = np.zeros(nbin_log)
# bin_log_ctr[0] = bin_min
# bin_log_wid[0] = bin_spc
# for b in range(1,nbin_log):
#    bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
#    bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
# nbin = nbin_log
# bin_coord = xr.DataArray( bin_log_ctr )
# ratio = bin_log_wid / bin_log_ctr
# for b in range(nbin_log): print('  center: {:12.6f}      width: {:12.6f}     ratio: {:6.4f}'.format(bin_log_ctr[b],bin_log_wid[b],ratio[b]))
# exit()
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.xyXStyle = "Log"

if 'clr' not in vars(): 
   clr = ['black']*num_case
   # if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   

if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
frq_list = []
amt_list = []
bin_list = []
case_name = []

gpcp_cnt = 0

for c in range(num_case):

   print('    case: '+case[c])

   if case[c]=='GPCP':
      
      if gpcp_cnt==0: time_freq,data_dir,data_sub = 'daily',os.getenv('HOME')+'/Data/Obs','daily'
      if gpcp_cnt==1: time_freq,data_dir,data_sub = None,'/global/cfs/cdirs/m3312/jlee1046/GPCP','ne30pg2_regrid/daily'
      if gpcp_cnt==0: use_remap,remap_str = False,None
      if gpcp_cnt==1: use_remap,remap_str = True,'remap_ne30pg2'
      gpcp_cnt += 1
   else:
      time_freq = None
      use_remap,remap_str = False,None

      data_dir,data_sub = None, None
      if case_dir[c] is not None: data_dir = case_dir[c]
      if case_sub[c] is not None: data_sub = case_sub[c]

   case_obj = he.Case( name=case[c], time_freq=time_freq, 
                       data_dir=data_dir, data_sub=data_sub )

   if 'name' in vars():
      case_name.append( name[c] )
   else:
      case_name.append( case_obj.short_name )

   bin_tmp_file = f'{temp_dir}/clim.precip.bin.v2.{case[c]}.{var}'
   if 'lat1' in locals(): bin_tmp_file += f'.lat1_{lat1}.lat2_{lat2}.nc'

   # if case[c]=='E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1': 
   #    if c==0: yr1,yr2 = 1950,1959; htype,years,months,first_file,num_files = 'h1',[],[],365* 0,365*2
   #    if c==1: yr1,yr2 = 2000,2009; htype,years,months,first_file,num_files = 'h1',[],[],365*50,365*2
   #    bin_tmp_file += f'.yr_{yr1}-{yr2}.nc'

   print('    bin_tmp_file: '+bin_tmp_file )

   if recalculate :

      #-------------------------------------------------------------------------
      file_list = []
      for yr in [1999,2000,2001]:
         file_list_tmp = sorted(glob.glob(f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*eam.{htype}.{yr}-*'))
         file_list = file_list + file_list_tmp
      #-------------------------------------------------------------------------
      # file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*eam.{htype}.1999-*'
      # if 'first_file' in globals(): file_list = file_list[first_file:]
      # if 'num_files' in globals(): file_list = file_list[:num_files]
      #-------------------------------------------------------------------------
      ds = xr.open_mfdataset( file_list )
      area = ds['area'].isel(time=0,missing_dims='ignore',drop=True)
      data = ds[var]
      #-------------------------------------------------------------------------
      time_mask = xr.DataArray( np.ones([len(data.time)],dtype=bool), coords=[('time', data.time.values)] )
      if years !=[]: time_mask = time_mask & [ y in years  for y in data['time.year' ].values ]
      if months!=[]: time_mask = time_mask & [ m in months for m in data['time.month'].values ]
      data = data.sel( time=data['time'].where(time_mask,drop=True) )
      #-------------------------------------------------------------------------
      ncol = ds['ncol'].isel(time=0,missing_dims='ignore',drop=True).values
      lat = ds['lat'].isel(time=0,missing_dims='ignore',drop=True)
      lon = ds['lon'].isel(time=0,missing_dims='ignore',drop=True)
      mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
      if 'lat1' in locals(): mask = mask & (lat>=lat1) & (lat<=lat2)
      if 'lon1' in locals(): mask = mask & (lon>=lon1) & (lon<=lon2)
      mask = mask.compute()
      #-------------------------------------------------------------------------
      area = area.compute().where( mask, drop=True)
      data = data.compute().where( mask, drop=True)

      if var=='PRECT': data = data*86400*1e3
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      # case_obj = he.Case( name=case[c], time_freq=time_freq, 
      #                     data_dir=data_dir, data_sub=data_sub )
      # if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
      # if 'lat2' in vars() : case_obj.lat2, case_obj.lat2 = lat2, lat2
      # tvar = var
      # data = case_obj.load_data(tvar,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files)
      # area = case_obj.load_data("area",htype=htype,num_files=1)
      # # convert to daily mean
      # data = data.resample(time='D').mean(dim='time')
      #-------------------------------------------------------------------------
      wgt, *__ = xr.broadcast(area, data) 
      gbl_mean = ( (data*wgt).sum() / wgt.sum() ).values 
      #-------------------------------------------------------------------------
      # Calculate the distribution
      #-------------------------------------------------------------------------
      if var=='PRECT':
         # bin_min_ctr = 0.04
         # bin_min_wid = 0.02
         # bin_spc_pct = 25.
         # nbin = 40

         bin_min_ctr = 0.04
         bin_min_wid = 0.02
         bin_spc_pct = 5.
         nbin = 150

      #-------------------------------------------------------------------------
      # Recreate bin_YbyX here for log bin case
      #-------------------------------------------------------------------------
      bin_min=bin_min_ctr
      bin_spc=bin_min_wid
      bin_spc_log=bin_spc_pct
      #----------------------------------------------------
      # set up bins
      bin_log_wid = np.zeros(nbin)
      bin_log_ctr = np.zeros(nbin)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      bin_coord = xr.DataArray( bin_log_ctr )

      #----------------------------------------------------
      # create output data arrays
      ntime = len(data['time']) if 'time' in data.dims else 1   
      
      shape,dims,coord = (nbin,),'bins',[('bins', bin_coord.values)]

      bin_cnt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
      bin_amt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
      bin_frq = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )

      condition = xr.DataArray( np.full(data.shape,False,dtype=bool), coords=data.coords )

      wgt, *__ = xr.broadcast(area,data)
      wgt = wgt.transpose()

      data_area_wgt = (data*wgt) / wgt.sum()

      ones_area_wgt = xr.DataArray( np.ones(data.shape), coords=data.coords )
      ones_area_wgt = ( ones_area_wgt*wgt ) / wgt.sum()

      #----------------------------------------------------
      # Loop through bins
      for b in range(nbin):
         bin_bot = bin_log_ctr[b] - bin_log_wid[b]/2.
         bin_top = bin_log_ctr[b] + bin_log_wid[b]/2.
         condition.values = ( data.values >=bin_bot )  &  ( data.values < bin_top )
         bin_cnt[b] = condition.sum()
         if bin_cnt[b]>0 :
            bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
            bin_amt[b] = data_area_wgt.where(condition,drop=True).sum() / (bin_spc_pct/1e2)

      #----------------------------------------------------
      # use a dataset to hold all the output
      dims = ('bins',)
      bin_ds = xr.Dataset()
      bin_ds['bin_amt'] = (dims, bin_amt.values )
      bin_ds['bin_frq'] = (dims, bin_frq.values )
      bin_ds['bin_cnt'] = (dims, bin_cnt.values )
      bin_ds['bin_pct'] = (dims, bin_cnt.values/bin_cnt.sum().values*1e2 )
      # bin_ds['bin_cnt'] = (dims, bin_cnt.sum(dim='ncol') )
      # bin_ds['bin_pct'] = (dims, bin_cnt.sum(dim='ncol')/bin_cnt.sum()*1e2 )
      bin_ds.coords['bins'] = ('bins',bin_coord.values)

      bin_ds['gbl_mean'] = gbl_mean

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
      if os.path.isfile(bin_tmp_file) : os.remove(bin_tmp_file)
      bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
   else:
      bin_ds = xr.open_dataset( bin_tmp_file )
      bin_cnt = bin_ds['bin_cnt']
      bin_amt = bin_ds['bin_amt']
      bin_frq = bin_ds['bin_frq']
      bin_coord = bin_ds['bins']
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   frequency = np.ma.masked_invalid( bin_frq.values )
   amount    = np.ma.masked_invalid( bin_amt.values )
   
   if 'gbl_mean' in locals():
      print('\n'+f'    Area weighted global mean:  {gbl_mean} ')
      print(     f'    sum of rain amount dist:    {np.sum(amount*(bin_spc_pct/1e2))} \n')

   frq_list.append( frequency    )
   amt_list.append( amount )
   bin_list.append( bin_coord )
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
res.tiXAxisString = 'Rain Rate [mm/day]'

var_str = var
if var=="PRECT"      : var_str = "Precipitation"

#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

res.xyExplicitLegendLabels = name
res.pmLegendDisplayMode    = "Always"
res.pmLegendOrthogonalPosF = -1.13
res.pmLegendParallelPosF   =  0.8+0.04
res.pmLegendWidthF         =  0.16
res.pmLegendHeightF        =  0.12   
res.lgBoxMinorExtentF      =  0.16   

res.trXMinF = np.min(bin_list)
res.trXMaxF = np.max(bin_list)

### plot frequency
res.tiYAxisString = 'Frequency [%]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(frq_list) ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)


### plot rain amount
res.pmLegendDisplayMode = "Never"
res.tiYAxisString = 'Rain Amount [mm/day]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(amt_list) ,res) )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
