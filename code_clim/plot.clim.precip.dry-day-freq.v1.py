import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy

### Early Science
case,name = ['TRMM','earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ,'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ],['TRMM','E3SM','SP-E3SM']
# case,name = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519','earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ],['E3SM','SP-E3SM']
# case,name = ['TRMM'],['TRMM']
# case,name = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519'],['E3SM']
# case,name = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ],['SP-E3SM']

# case,name = ['INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'],['INCITE2019']
# case,name = ['TRMM','INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'],['TRMM','INCITE2019']

# case,name = ['E3SM_RGMA-NRAD1_ne30pg2_FSP1V1_64x1_1000m_00'],['MMF nx_rad=1']

var, threshold = 'PRECT', 0.1

fig_type = "png"
fig_file = os.getenv("HOME")+"/Research/E3SM/figs_clim/clim.precip.dry-day-freq.v1"

lat1,lat2 = -45,45

# htype,years,months,num_files = 'h1',[],[],1
htype,years,months,num_files = 'h1',[0,1,2,3,4],[],0

recalculate = False
temp_dir = os.getenv("HOME")+'/Research/E3SM/data_temp'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_case)
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012

res.lbTitleString = '[%]'
# res.lbTitleString = f'Dry Day Frequency (<{threshold} mm/day) [%]'
res.lbTitlePosition = 'Bottom'
res.lbTitleFontHeightF = 0.01

# res.mpLimitMode = "LatLon" 
# res.mpMinLatF   = 45 -15
# res.mpMaxLatF   = 45 +15
# res.mpMinLonF   = 180-15
# res.mpMaxLonF   = 180+15

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
print('\n  var: '+var)
for c in range(num_case):
   print('\n    case: '+case[c])

   tmp_file = temp_dir+'/clim.precip.dry-day-freq.v1.'+case[c]+'.'+var
   tmp_file = tmp_file+'.lat1_'+str(lat1)+'.lat2_'+str(lat2)
   tmp_file = tmp_file+'.threshold_'+str(threshold)
   tmp_file = tmp_file+'.nc'
   print('    tmp_file: '+tmp_file )

   case_obj = he.Case( name=case[c] )
   case_obj.time_freq = 'daily'
   if 'DYN_' in var : case_obj.grid = case_obj.grid.replace('pg2','')
   if 'name' in vars() : case_obj.short_name = name[c]

   if 'lat1' in vars() : case_obj.lat1 = lat1
   if 'lat2' in vars() : case_obj.lat2 = lat2
   if 'lon1' in vars() : case_obj.lon1 = lon1
   if 'lon2' in vars() : case_obj.lon2 = lon2

   if recalculate :
      #----------------------------------------------------------------------
      # read the data
      #----------------------------------------------------------------------

      X = case_obj.load_data(var,htype=htype,years=years,months=months,num_files=num_files)
      
      # print(X)
      # print(X.time)

      hc.print_time_length(X.time)

      X = X.where(X<threshold).count(dim='time') / len(X.time) * 1e2
      
      # if 'area' in vars() :
      #    gbl_mean = ( (X*area).sum() / area.sum() ).values 
      #    print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')
      #----------------------------------------------------------------------
      # Write to file 
      #----------------------------------------------------------------------
      print('writing to file: '+tmp_file)
      X.name = var
      X.to_netcdf(path=tmp_file,mode='w')
   else:
      ds = xr.open_dataset( tmp_file )
      X = ds[var]


   #-------------------------------------------------------------------------
   # Set colors and contour levels
   #-------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   
   # tres.cnFillPalette = "WhiteBlueGreenYellowRed"
   # tres.cnFillPalette = "CBR_wet"
   tres.cnFillPalette = "MPL_viridis"
   
   tres.cnLevelSelectionMode = "ExplicitLevels"
   # tres.cnLevels = np.linspace(-0.001,0.001,81)
   tres.cnLevels = np.arange(4,100,4)

   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------
   ip = c
   
   if case[c]=='TRMM' : case_obj.lat, case_obj.lon = X.lat, X.lon

   hs.set_cell_fill(case_obj,tres)

   plot[ip] = ngl.contour_map(wks,X.values,tres) 

   rstr = f'Dry Day Frequency (<{threshold} mm/day)'
   hs.set_subtitles(wks, plot[ip], case_obj.short_name, '', rstr, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [1,num_case]
layout = [num_case,1]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------