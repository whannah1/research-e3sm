# create scatter plot of precip variance to show grid imprinting
# v1 - plot variance against mean (doesn't work)
# v2 - plot precip variance against omega variance
import os
import ngl
import xarray as xr
import numpy as np
from scipy.stats import skew
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
home = os.getenv("HOME")
print()

### Early Science
# case = ['TRMM']
# case = ['GPCP','TRMM','earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ,'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]
# clr = ['gray','black','red','blue']
# dsh = [0,0,0,0]

# case = ['baseline_ns.FC5AV1C-L.ne30.sp1_64x1_1000m.20190711'   \
#        ,'baseline_ew.FC5AV1C-L.ne30.sp1_64x1_1000m.20190711'   \
#        ,'conmom_ns.FC5AV1C-L.ne30.sp1_64x1_1000m.20190709'     \
#        ,'conmom_ew.FC5AV1C-L.ne30.sp1_64x1_1000m.20190709' ]
# clr = ['red','blue','red','blue']
# dsh = [0,0,1,1]

# case = ['E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00']
# name = ['ne30pg2']

case = ['E3SM_SP1_64x1_1000m_ne30_F-EAMv1-AQP1_00','E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00']
name = ['ne30','ne30pg2']
clr = ['red','blue']

var = 'PRECT'

# var = 'DYN_OMEGA'
# lev = 500

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_clim/clim.precip.scatter.v1"


lat1 = -20
lat2 =  20

htype,years,months,num_files = 'h1',[],[],10
# htype,years,months,num_files = 'h1',[],[5,6,7],0

recalculate = True

temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 12
res.xyMarkLineMode = 'Markers'
res.xyMarker = 1

if var == 'PRECT':
   res.xyXStyle = "Log"
   res.xyYStyle = "Log"

if 'clr' not in vars(): 
   clr = ['black']*num_case
   # if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   

if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh

unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
# res.tiXAxisString = '[mm/day]'
# res.tiYAxisString = '[mm/day]'

var_str = var
if var=="PRECT"      : var_str = "Precipitation"
if var=="OMEGA"      : var_str = str(lev)+"mb Omega"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []
for c in range(num_case):

   print('    case: '+case[c])
   case_obj = he.Case( name=case[c], time_freq='daily' )
   if 'name' in vars() : case_obj.short_name = name[c]
   case_name.append( case_obj.short_name )

   tmp_file = temp_dir+"/clim.precip.scatter.v1."+case[c]+"."+var+".lat1_"+str(lat1)+".lat2_"+str(lat2)+".nc"
   print('    tmp_file: '+tmp_file )

   # if recalculate :
   if True:
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      ### uncommentt this to subset the data
      if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
      if 'lat2' in vars() : case_obj.lat2, case_obj.lat2 = lat2, lat2
      if 'lev' not in vars() : lev = np.array([-1])

      X = case_obj.load_data(var,htype=htype,lev=lev,years=years,months=months,num_files=num_files)

      # if 'lev' in X.dims : X = X[0,:]
      if 'lev' in X.dims : X = X.isel(lev=0)

      # print(X)
      # hc.print_stat(X,name=var)
      # exit()

      area = case_obj.load_data("area",htype=htype,num_files=1).values.astype(np.float)
      
      #-------------------------------------------------------------------------
      # Calculate the mean and variance at each point
      #-------------------------------------------------------------------------
      dims   = ('ncol')
      stat_ds = xr.Dataset()
      stat_ds['avg'] = (dims, X.mean(dim='time') )
      stat_ds['std'] = (dims, X.std(dim='time') )
      stat_ds['var'] = (dims, X.var(dim='time') )
      # stat_ds['skw'] = (dims, skew(X,axis=0,nan_policy='omit') )
      stat_ds.coords['ncol'] = ('ncol',X.coords['ncol'])

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
   #    stat_ds.to_netcdf(path=tmp_file,mode='w')
   # else:
   #    stat_ds = xr.open_mfdataset( tmp_file )


   # res.xyExplicitLegendLabels = case_name
   # res.pmLegendDisplayMode    = "Always"
   # res.pmLegendOrthogonalPosF = -1.13
   # res.pmLegendParallelPosF   =  0.8+0.04
   # res.pmLegendWidthF         =  0.16
   # res.pmLegendHeightF        =  0.12   
   # res.lgBoxMinorExtentF      =  0.16   


   xvar,xstr = stat_ds['avg'].values, 'Mean'
   # xvar,xstr = stat_ds['std'].values, 'Std. Deviation'
   # xvar,xstr = stat_ds['var'].values, 'Variance'
   # xvar,xstr = stat_ds['skw'].values, 'Skewness'


   # yvar,ystr = stat_ds['avg'].values, 'Mean'
   # yvar,ystr = stat_ds['std'].values, 'Std. Deviation'
   yvar,ystr = stat_ds['var'].values, 'Variance'
   # yvar,ystr = stat_ds['skw'].values, 'Skewness'
   
   res.tiXAxisString = xstr
   res.tiYAxisString = ystr

   res.trXMinF = np.min(xvar)
   res.trXMaxF = np.max(xvar)

   hc.print_stat(xvar,xstr)
   hc.print_stat(yvar,ystr)
   
   # plot.append( ngl.xy(wks, xvar , yvar ,res)  )
   plot.append( ngl.xy(wks, xvar , yvar ,res)  )

   hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], '', var_str, font_height=0.015)


#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

# res.xyExplicitLegendLabels = case_name
# res.pmLegendDisplayMode    = "Always"
# res.pmLegendOrthogonalPosF = -1.13
# res.pmLegendParallelPosF   =  0.8+0.04
# res.pmLegendWidthF         =  0.16
# res.pmLegendHeightF        =  0.12   
# res.lgBoxMinorExtentF      =  0.16   

# res.trXMinF = np.min(x_list)
# res.trXMaxF = np.max(x_list)

### plot stddev vs mean
# plot.append( ngl.xy(wks, np.stack(x_list) , np.stack(y_list) ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)


# ### plot accumulation
# res.pmLegendDisplayMode = "Never"
# res.tiYAxisString = 'Rain Amount [mm/day]'
# plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(acc_list) ,res) )
# # hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------