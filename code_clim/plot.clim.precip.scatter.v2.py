# create scatter plot of precip variance to show grid imprinting
# v1 - plot variance against mean (doesn't work)
# v2 - plot precip variance against omega variance
import os
import ngl
import xarray as xr
import numpy as np
from scipy.stats import skew
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
home = os.getenv("HOME")
print()

# case = [ 'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]

case = ['E3SM_SP1_64x1_1000m_ne30_F-EAMv1-AQP1_00'  ]
name = ['ne30']

# case = ['E3SM_SP1_64x1_1000m_ne30_F-EAMv1-AQP1_00' ,'E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00' ]
# name = ['ne30','ne30pg2']
# clr = ['red','blue']

var1 = 'PRECT'
var2 = 'OMEGA'
lev = 700

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_clim/clim.precip.scatter.v2"


lat1 =  40
lat2 =  45

htype,years,months,num_files = 'h0',[],[],0
# htype,years,months,num_files = 'h1',[],[5,6,7],0

# recalculate = True

temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 12
res.xyMarkLineMode = 'Markers'
res.xyMarker = 1

# if var == 'PRECT':
#    res.xyXStyle = "Log"
#    res.xyYStyle = "Log"

if 'clr' not in vars(): clr = ['black']*num_case
if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh

unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
# res.tiXAxisString = '[mm/day]'
# res.tiYAxisString = '[mm/day]'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []
for c in range(num_case):

   print('    case: '+case[c])
   case_obj = he.Case( name=case[c], time_freq='daily' )
   if 'name' in vars() : case_obj.short_name = name[c]
   case_name.append( case_obj.short_name )

   # tmp_file = temp_dir+"/clim.precip.scatter.v1."+case[c]+"."+var+".lat1_"+str(lat1)+".lat2_"+str(lat2)+".nc"
   # print('    tmp_file: '+tmp_file )

   # if recalculate :
   if True:
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      ### uncommentt this to subset the data
      if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
      if 'lat2' in vars() : case_obj.lat2, case_obj.lat2 = lat2, lat2
      if 'lev' not in vars() : lev = np.array([-1])
      case_obj.mirror_equator = True

      # area_name = 'area'
      # # if 'DYN_' in var[v] and 'pg2' in case[c] : area_name = 'area_d'
      # area = case_obj.load_data(area_name,htype=htype)
      

      X1 = case_obj.load_data(var1,htype=htype,lev=lev,years=years,months=months,num_files=num_files)
      X2 = case_obj.load_data(var2,htype='h2', lev=lev,years=years,months=months,num_files=num_files)

      if 'lev' in X1.dims : X1 = X1.isel(lev=0)
      if 'lev' in X2.dims : X2 = X2.isel(lev=0)

      # hc.print_stat(X1,name=var1)
      # hc.print_stat(X2,name=var2)
      # exit()

      area = case_obj.load_data("area",htype=htype,num_files=1)#.values.astype(np.float)
      
      #-------------------------------------------------------------------------
      # Calculate the mean and variance at each point
      #-------------------------------------------------------------------------
      dims   = ('ncol')
      stat_ds = xr.Dataset()

      stat_ds['var1'] = (dims, X1.mean(dim='time') )
      stat_ds['var2'] = (dims, X2.mean(dim='time') )
      qstr = 'Mean'

      # stat_ds['var1'] = (dims, hc.median(X1,dim='time') )
      # stat_ds['var2'] = (dims, hc.median(X2,dim='time') )
      # qstr = 'Median'

      # stat_ds['var1'] = (dims, X1.variance(dim='time') )
      # stat_ds['var2'] = (dims, X2.variance(dim='time') )
      # qstr = 'Variance'

      stat_ds.coords['ncol'] = ('ncol',X1.coords['ncol'])

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
   #    stat_ds.to_netcdf(path=tmp_file,mode='w')
   # else:
   #    stat_ds = xr.open_mfdataset( tmp_file )


   # res.xyExplicitLegendLabels = case_name
   # res.pmLegendDisplayMode    = "Always"
   # res.pmLegendOrthogonalPosF = -1.13
   # res.pmLegendParallelPosF   =  0.8+0.04
   # res.pmLegendWidthF         =  0.16
   # res.pmLegendHeightF        =  0.12   
   # res.lgBoxMinorExtentF      =  0.16   


   xvar,xstr = stat_ds['var1'].values, var1+' '+qstr
   yvar,ystr = stat_ds['var2'].values, var2+' '+qstr
   
   res.tiXAxisString = xstr
   res.tiYAxisString = ystr

   res.trXMinF = np.min(xvar)
   res.trXMaxF = np.max(xvar)

   hc.print_stat(xvar,xstr)
   hc.print_stat(yvar,ystr)
   
   # plot.append( ngl.xy(wks, xvar , yvar ,res)  )
   plot.append( ngl.xy(wks, xvar , yvar ,res)  )

   #-------------------------------------------------------------------------
   # Overlay colored dots
   #-------------------------------------------------------------------------
   xvar = stat_ds['var1']
   yvar = stat_ds['var2']

   if "ne4"   in case[c] : area_bins = [ 0.0000, 0.006000, 0.015000, 0.030000 ] 
   if "ne30"  in case[c] : area_bins = [ 0.0000, 0.000100, 0.000300, 0.000600 ] 
   if "ne120" in case[c] : area_bins = [ 0.0000, 0.000007, 0.000015, 0.000050 ] 

   bin_color = ['red','green','blue']

   bins = xr.DataArray( area_bins ) 
   nbin = len(bins)-1
   val_chk = np.isfinite(xvar.values)
   XC = np.zeros(len(bins))
   YC = np.zeros(len(bins))
   for b in range(nbin):
      bin_bot = bins[b]  .values
      bin_top = bins[b+1].values

      condition = xr.DataArray( np.full(area.shape,False,dtype=bool), coords=area.coords )
      condition.values = ( np.where(val_chk,area.values,bin_bot-1e3) >=bin_bot ) \
                        &( np.where(val_chk,area.values,bin_bot-1e3)  <bin_top )
      
      xvar_tmp = xvar.where(condition).values
      yvar_tmp = yvar.where(condition).values

      res.xyMarkerColor = bin_color[b]
      ngl.overlay( plot[len(plot)-1],  ngl.xy(wks, xvar_tmp , yvar_tmp ,res)  )

      XC[b] = xvar.where(condition).mean().values
      YC[b] = yvar.where(condition).mean().values

   for b in range(nbin):
      res.xyMarkerColor = bin_color[b]
      # res.xyMarkerSizeF = 0.1
      res.xyMarker = 16
      XCP = np.array( [ XC[b], XC[b] ] )
      YCP = np.array( [ YC[b], YC[b] ] )
      ngl.overlay( plot[len(plot)-1],  ngl.xy(wks, XCP , YCP ,res)  )

   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------

   hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], '', '', font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------