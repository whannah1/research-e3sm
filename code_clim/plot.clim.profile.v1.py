import os, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------
var,vclr,vdsh = [],[],[]
def add_var(var_name,clr='black',dsh=0): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh)
#---------------------------------------------------------------------------------------------------
# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])
lev = np.arange(20,400+5,5)
# lev = np.array([50,100,150,200,300,400,500,600,700,750,800,850,875,900,925,975])
# lev = np.array([0.1,0.5,1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])
# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
#---------------------------------------------------------------------------------------------------
### reduced radiation sensitivity
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00',         n='nx_rad=128',     c='blue')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.00',           n='nx_rad=8',       c='red')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_1.00',           n='nx_rad=1',       c='red')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.RAD_SORT.00',n='nx_rad=128+SORT',c='cyan',d=1)
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.RAD_SORT.00',  n='nx_rad=8+SORT',  c='magenta',d=1)

### sorted radiation column test
# add_case('E3SM.RAD-SENS.GNUCPU.ne4pg2_ne4pg2.F2010-MMF1.NXY_32x1.RNX_8.00',     n='nx_rad=8',     c='magenta')
# add_case('E3SM.RAD-SENS.GNUCPU.ne4pg2_ne4pg2.F2010-MMF1.NXY_32x1.RNX_8.LRAD2.00',n='nx_rad=8+LRAD',c='cyan')

### INCITE low-cld Cess experiments (UP)
# he.default_data_dir = '/global/cfs/cdirs/m3312/whannah/INCITE2022-LOW-CLD'
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72.01',                     n='E3SM',c='red' ,p='/global/cfs/cdirs/m3312/whannah/2022-QBO-TEST/'    ,s='data_remap_90x180_prs',r=True)
# add_case('E3SM.QBO-TEST.F2010.ne30pg2.L72.01',                     n='E3SM',     c='red' ,p='/global/cfs/cdirs/m3312/whannah/2022-QBO-TEST',s='run')
# add_case('E3SM.TMS-00.F2010-MMF1.ne30pg2_oECv3.tms-on',            n='MMF',      c='blue',p='/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu',s='run')
# add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_0K',n='MMF (UP)', c='cyan',p='/global/cfs/cdirs/m3312/whannah/INCITE2022-LOW-CLD',s='data_remap_90x180',r=True)

### INCITE2021-CMT
# case.append(f'ERAi');name.append('ERAi')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.02',       n='MMF 2D'   ,c='red' ,p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.MOMFB.02', n='MMF 2D+MF',c='red' ,p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.02',      n='MMF 3D'   ,c='blue',p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='MMF 3D+MF',c='blue',p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/')

### L125 low cloud Cess runs
# add_case(f'E3SM.INCITE2022-LOW-CLD-CESS-v3.ne30pg2.F2010-MMF1.SSTP_0K',n='MMF Cess L125 0K',c='blue',p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/',s='run')

### v3 L80 test
# add_case('20230629.v3alpha02.amip.chrysalis.L72',n='L72',c='red', p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')
# add_case('20230629.v3alpha02.amip.chrysalis.L80',n='L80',c='blue',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')

### L80 ensemble
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu','run'
# add_case('E3SM.2023-SCIDAC.ne30pg2_EC30to60E2r2.AMIP.EF_0.10.CF_0.10.HD_1.50',n='',p=tmp_path,s=tmp_sub)


### PAM global tests
# pscratch,psub = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/','run'
# add_case('E3SM.PAM-DEV-2023-37.GNUGPU.ne30pg2_oECv3.F2010-MMF1',             n='MMF+SAM',    p=pscratch,s=psub,c='red')
# add_case('E3SM.PAM-DEV-2023-40.GNUGPU.ne30pg2_oECv3.F2010-MMF2.CDT_08.DPP_2',n='MMF+PAM',p=pscratch,s=psub,c='blue')


### CRM throttling tests
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_32'  ,n='', c='red', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_8'  ,n='', c='green', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_4'  ,n='', c='blue', d=0)

# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_8'   ,n='', c='magenta', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_8'   ,n='', c='red', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_8'  ,n='', c='green', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_8'  ,n='', c='blue', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_8'  ,n='', c='purple', d=0)


# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_1'  ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_4'  ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_8'  ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_16_1'   ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_16_4'   ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_16_8'   ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_1'  ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_4'  ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_8'  ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_1'   ,n='', c='red', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_4'   ,n='', c='green', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_8'   ,n='', c='blue', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_1'  ,n='', c='red', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_4'  ,n='', c='green', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_8'  ,n='', c='blue', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_1'   ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_4'   ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_8'   ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_8_1'    ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_8_4'    ,n='', c='', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_8_8'    ,n='', c='', d=0)

### 4xCO2 tests on Summit
# tmp_path,tmp_sub = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch','archive/atm/hist'
# add_case('E3SM.2023-CO2-TEST-01.GNUGPU.ne30pg2_EC30to60E2r2.WCYCL1850-MMF1.1xCO2',n='E3SM-MMF PI 1xCO2',c='blue' ,p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-CO2-TEST-01.GNUGPU.ne30pg2_EC30to60E2r2.WCYCL1850-MMF1.2xCO2',n='E3SM-MMF PI 2xCO2',c='green',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-CO2-TEST-01.GNUGPU.ne30pg2_EC30to60E2r2.WCYCL1850-MMF1.4xCO2',n='E3SM-MMF PI 4xCO2',c='red'  ,p=tmp_path,s=tmp_sub)

### PAM dev tests
# tmp_scratch = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/'
# add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF1',n='MMF1',p=tmp_scratch,s='run',c='red')
# add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF2',n='MMF2',p=tmp_scratch,s='run',c='blue')


### Rotating RCE for Da Yang
# pscratch,psub = '/pscratch/sd/w/whannah/e3sm_scratch/perlmutter', 'run'
# pscratch,psub = '/global/cfs/cdirs/m1517/dyang/E3SM_MMF',         'data_native'
# pscratch,psub = '/global/cfs/cdirs/m3312/whannah/2022-RCEROT/','run'
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.04', n='E3SM-MMF RCE CTRL',   p=pscratch,s=psub,c='black')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.04', n='E3SM-MMF RCE HOM-RAD',p=pscratch,s=psub,c='red')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.FIX_QRT.04', n='MMF RCEROT FIX-QRT',  p=pscratch,s=psub,c='magenta')
# add_case('E3SM.GNUCPU.ne30pg2.F-EAM-RCEROT.04'              , n='E3SM RCEROT',        p=pscratch,s=psub)
# add_case('E3SM.GNUCPU.ne30pg2.F-EAM-RCEROT.FIX_QRT.04'      , n='E3SM RCEROT FIX-QRT',p=pscratch,s=psub)


### 2024 MMF GWD test
# pscratch,psub = '/global/cfs/cdirs/m4310/whannah/E3SM',''
# add_case('E3SM.2024-MMF-GW-test.ne30pg2_EC30to60E2r2.F20TR-MMF1.L72.CTL',n='MMF CTL',p=pscratch,s=psub,c='red')
# add_case('E3SM.2024-MMF-GW-test.ne30pg2_EC30to60E2r2.F20TR-MMF1.L72.EXP',n='MMF EXP',p=pscratch,s=psub,c='blue')

### 2024 PAM dev tests
# pscratch,psub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2023-PAM-ENS-08.GNUGPU.ne4pg2_ne4pg2.F2010-MMF2.CDT_08.DPP_04.HDT_0060.GDT_1200',p=pscratch,s=psub,c='red')
# add_case('E3SM.2023-PAM-ENS-08.GNUGPU.ne4pg2_ne4pg2.F2010-MMF2.CDT_08.DPP_04.HDT_0060.GDT_1200',p=pscratch,s=psub,c='green')
# add_case('E3SM.2023-PAM-ENS-08.GNUGPU.ne4pg2_ne4pg2.F2010-MMF2.CDT_08.DPP_04.HDT_0060.GDT_1200',p=pscratch,s=psub,c='blue')

### 2024 CRM domain tests for Andrea Jenney
# pscratch,psub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_32x1.DX_1000', d=0,c='red',    p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_64x1.DX_1000', d=0,c='green',  p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_128x1.DX_1000',d=0,c='blue',   p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_256x1.DX_1000',d=0,c='purple', p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_32x1.DX_4000', d=1,c='red',    p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_64x1.DX_4000', d=1,c='green',  p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_128x1.DX_4000',d=1,c='blue',   p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST.FRCE-MMF1_300.NX_256x1.DX_4000',d=1,c='purple', p=pscratch,s=psub)

# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST-01.FRCE-MMF1_300.NX_32x1.DX_4000',n='32x1 4km', d=0,c='red', p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST-01.FRCE-MMF1_300.NX_64x1.DX_4000',n='64x1 4km', d=0,c='green', p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST-01.FRCE-MMF1_300.NX_128x1.DX_4000',n='64x1 4km',d=0,c='blue', p=pscratch,s=psub)
# add_case('E3SM.2024-RCEMIP-DOMAIN-TEST-01.FRCE-MMF1_300.NX_256x1.DX_4000',n='64x1 4km',d=0,c='purple', p=pscratch,s=psub)



### 2024 RCEMIP phase 2
# scratch_cpu = ''
# scratch_gpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
# tsub = 'archive/atm/hist'
# add_case('E3SM.2024-RCEMIP2.FRCE_295',                 n='FRCE_295',               d=0,c='red',     p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE_300',                 n='FRCE_300',               d=0,c='green',   p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE_305',                 n='FRCE_305',               d=0,c='blue',    p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_295dT1p25',        n='FRCE-MW_295dT1p25',      d=0,c='orange',  p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT0p625',       n='FRCE-MW_300dT0p625',     d=0,c='cyan',    p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT1p25',        n='FRCE-MW_300dT1p25',      d=0,c='magenta', p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT2p5',         n='FRCE-MW_300dT2p5',       d=0,c='purple',  p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_305dT1p25',        n='FRCE-MW_305dT1p25',      d=0,c='pink',    p=scratch_cpu,s=tsub)

# add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_295',            n='FRCE-MMF1_295',          d=0,c='red',     p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_300',            n='FRCE-MMF1_300',          d=0,c='green',   p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_305',            n='FRCE-MMF1_305',          d=0,c='blue',    p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_295dT1p25',   n='FRCE-MW-MMF1_295dT1p25', d=0,c='orange',  p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT0p625',  n='FRCE-MW-MMF1_300dT0p625',d=0,c='cyan',    p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT1p25',   n='FRCE-MW-MMF1_300dT1p25', d=0,c='magenta', p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT2p5',    n='FRCE-MW-MMF1_300dT2p5',  d=0,c='purple',  p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_305dT1p25',   n='FRCE-MW-MMF1_305dT1p25', d=0,c='pink',    p=scratch_gpu,s=tsub)

### 2024 SciDAC heating test
# add_case('E3SM.2024-SCIDAC-heating-test-00.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

### 2024 SciDAC benchmark vs AQP
add_case('v3.LR.amip_0101.QBObenchmark.20241008',                          n='EAMv3 benchmark', d=0,c='red',p='/global/cfs/cdirs/m4310/data/sims',s='archive/atm/hist')
add_case('SciDAC.E3SMv3.20241220.SciDACWMH_QBOBnMk.Dtopo.ICWMH.UCWMH.AQP3',n='AQPv3',           d=0,c='blue',p='/global/cfs/cdirs/m4310/data/sims/aqua/xsunlanl',s='archive/atm/hist')


#---------------------------------------------------------------------------------------------------

# add_var('TTEND_CLUBB')

# add_var('RELHUM')
add_var('T')
# add_var('Q')
# add_var('CLDLIQ')
# add_var('CLDICE')
# add_var('U')
# add_var('V')
# add_var('OMEGA')

# add_var('CLDLIQ')
# add_var('CLDICE')
# add_var('RH')
# add_var('MMF_MCUP')

# add_var('MMF_QC')
# add_var('MMF_QTFLX')
# add_var('MMF_QTFLXS')
# add_var('MMF_TKEB')
# add_var('MMF_TKEQC')
# add_var('MMF_TKEQT')
# add_var('MMF_TKES')
# add_var('MMF_TKEW')

# add_var('U')
# add_var('OMEGA')
# add_var('THETA')

# add_var('QRL'); add_var('QRS')
# add_var('U')
# add_var('V')
# add_var('O3')

# add_var('CLOUD')

# add_var('CLDLIQ'); #add_var('MMF_QC')
# add_var('CLDICE'); #add_var('MMF_QI')


# add_var('MMF_QC')
# add_var('MMF_QI')
# add_var('MMF_QR')


# add_var('BUTGWSPEC')


# add_var('CRM_RAD_CLD')
# add_var('CRM_RAD_QC')
# add_var('CRM_RAD_QI')

# add_var('MMF_TLS')
# add_var('MMF_DT')
# add_var('MMF_RHODLS')
# add_var('MMF_RHOVLS')#; add_var('MMF_DQ')
# add_var('MMF_RHOLLS')#; add_var('MMF_DQC')
# add_var('MMF_RHOILS')#; add_var('MMF_DQI')

# add_var('MMF_DQC')
# add_var('MMF_DQI')
# add_var('MMF_DQI_MICRO')
# add_var('MMF_DQI_SGS')
# add_var('MMF_DQI_DYCOR')



# add_var('MMF_DT')
# add_var('MMF_DT_DYCOR')
# add_var('MMF_DT_MICRO')
# add_var('MMF_DT_SGS')

# add_var('MMF_DQI')
# add_var('MMF_DQI_MICRO')

# add_var('MMF_MCUP')
# add_var('MMF_MCDN')
# add_var('MMF_MCUUP')
# add_var('MMF_MCUDN')
# add_var('MMF_MC')
# add_var('MMF_MCU')

# add_var('MMF_MCUP' ,clr='red')
# add_var('MMF_MCUP2',clr='blue')

# add_var('DTCOND'          ,clr='black')
# add_var('ZMDT'            ,clr='red')
# add_var('P3DT'            ,clr='green')
# add_var('TTEND_CLUBB'     ,clr='blue')
# add_var('DTCOND2'         ,clr='black',dsh=1)
# add_var('CLOUDFRAC_CLUBB' ,clr='')
# add_var('WP2_CLUBB'       ,clr='')
# add_var('WP3_CLUBB'       ,clr='')

#---------------------------------------------------------------------------------------------------
# lat1,lat2 = -10,10
lat1,lat2 = -30,30
# lat1,lat2 = 45,90
# lat1,lat2 = 30,60
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50
# lat1,lat2,lon1,lon2 = -30,-20,360-110,360-80
# lat1,lat2,lon1,lon2 = 18,22.5,169,174

# xlat,xlon,dy,dx = 60,120,10,10;
# xlat,xlon,dy,dx = 0,0,10,10;
# xlat,xlon,dy,dx = 0,150,20,20;
# xlat,xlon,dy,dx = -40,180,10,10;
# xlat,xlon,dx,dy = 36, 360-97, 2, 2 # ARM SGP
if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

# lat1,lat2 = -90,-70
# lat1,lat2,lon1,lon2 = -60,90,0,360
#---------------------------------------------------------------------------------------------------

num_plot_col = len(var)
# num_plot_col = 2


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.profile.v1'
# fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.profile.v1a'
# fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.profile.v1b'


# htype,years,months,first_file,num_files = 'ha',[],[],100,20
htype,years,months,first_file,num_files = 'h0',[],[],0,12*5
# htype,years,months,first_file,num_files = 'h2',[],[],10,10
# htype,years,months,first_file,num_files = 'h1g',[],[],950,50#12*10


plot_diff = False

overlay_vars = False

use_snapshot,ss_t  = False,0

use_height_coord   = False

omit_bot,bot_k     = False,-2
omit_top,top_k     = False,30

print_stats        = False
print_profile      = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'diff_mode' not in locals(): diff_mode = 0

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources() ; npix = 2048*2 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
# plot = [None]*(num_var*num_case)
if overlay_vars: 
   plot = [None]*(num_case)
else:
   plot = [None]*(num_var)
res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

if use_height_coord: 
   res.tiYAxisString = 'Height [km]'
else:
   res.tiYAxisString = 'Pressure [hPa]'
   res.trYReverse = True
   # res.xyYStyle = 'Log'

   res.trYMinF = np.min(lev)
   res.trYMaxF = np.max(lev)



# res.trXMinF = 150

# if use_height_coord:
   # res.trYMaxF = 30e3
   # res.trYMinF = 10e3
# else:
#    res.trYMaxF = 100

# if not use_height_coord: res.trYMinF = 800

def get_comp(case):
   comp = 'eam'
   if 'INCITE2019' in case: comp = 'cam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'MAML' in case: comp = 'eam_0001'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp

if use_snapshot:
   print(hc.tcolor.RED+'WARNING - plotting snapshot!!!'+hc.tcolor.ENDC)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(hc.tcolor.GREEN+'  var: '+var[v]+hc.tcolor.ENDC)
   data_list,lev_list = [],[]
   for c in range(num_case):
      print(hc.tcolor.CYAN+'    case: '+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp )

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'
      if var[v]=='MMF_MCU': tvar = 'MMF_MCUUP'
      if var[v]=='DTCOND2': tvar = 'ZMDT'

      area_name = 'area'
      
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      tnum_files = num_files

      if use_height_coord: 
         Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c])

      area = case_obj.load_data(area_name,htype=htype,first_file=first_file,num_files=tnum_files).astype(np.double)
      data = case_obj.load_data(tvar,     htype=htype,first_file=first_file,num_files=tnum_files,lev=lev)

      #-------------------------------------------------------------------------
      # derived variables
      if var[v]=='MMF_MCU': data = data + case_obj.load_data('MMF_MCUDN',htype=htype,first_file=first_file,num_files=tnum_files,lev=lev)
      if var[v]=='DTCOND2': 
         data = data + case_obj.load_data('P3DT',       htype=htype,first_file=first_file,num_files=tnum_files,lev=lev)
         data = data + case_obj.load_data('TTEND_CLUBB',htype=htype,first_file=first_file,num_files=tnum_files,lev=lev)
      #-------------------------------------------------------------------------
      hc.print_time_length(data.time,indent=' '*6)
      # if print_stats: hc.print_stat(data,name=f'{var[v]} before averaging',indent=' '*4,compact=True)
      #-------------------------------------------------------------------------
      if use_snapshot:
         data = data.isel(time=ss_t)
         if use_height_coord: Z = Z.isel(time=ss_t)
      else:

         for n in range(7):
            amax = data.argmax(dim=['time','lev','ncol'])
            data[amax['time'],amax['lev'],amax['ncol']] = 0.
         data = data.isel(time=amax['time'],ncol=amax['ncol'])

         # print(); print(amax)
         # print(); print(amax.keys)
         # print(); print(amax.values)
         # exit()
         # amax = data.argmax()
         # max_pt = data.isel(ncol=np.max(amax.values))

         # data = data.mean(dim='time')
         if use_height_coord: Z = Z.mean(dim='time')
      #-------------------------------------------------------------------------
      if case[c]=='ERA5':
         data = ( (data*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )
      else:
         if 'lat' in data.dims and 'lon' in data.dims:
            data = ( (data*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )
         else:
            if use_height_coord: Z = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') )
            data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      #-------------------------------------------------------------------------
      if omit_bot: data = data[:bot_k]
      if omit_top: data = data[top_k:]
      if use_height_coord and omit_bot: Z = Z[:bot_k]
      if use_height_coord and omit_top: Z = Z[top_k:]
      #-------------------------------------------------------------------------
      # omit interpolated levels if they are zero
      if not use_height_coord:
         if data[ 0]==0 : data = data[1:]
         if data[-1]==0 : data = data[:-2]
      #-------------------------------------------------------------------------
      if print_stats:
         hc.print_stat(data,name=f'{var[v]} after averaging',indent=' '*4,compact=True)

      if print_profile:
         print()
         for x in data.values: print(f'    {x}')
         print()
      #-------------------------------------------------------------------------
      # gbl_mean = ( (data*area).sum() / area.sum() ).values 
      # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')
      #-------------------------------------------------------------------------
      data_list.append( data.values )
      if use_height_coord:
         lev_list.append( Z.values )
      else:
         lev_list.append( data['lev'].values )
      #-------------------------------------------------------------------------
   data_list_list.append(data_list)
   lev_list_list.append(lev_list)

#-------------------------------------------------------------------------------
# Create plot 1 - overlay all vars for each case
#-------------------------------------------------------------------------------
if overlay_vars:
   for c in range(num_case):
      ip = c
      tres = copy.deepcopy(res)
      data_min = np.min( data_list_list[0][c] )
      data_max = np.max( data_list_list[0][c] )
      for v in range(num_var):
         data_min = np.min([ data_min, np.nanmin(data_list_list[v][c]) ])
         data_max = np.max([ data_max, np.nanmax(data_list_list[v][c]) ])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      
      for v in range(num_var):
         tres.xyLineColor   = vclr[v]
         tres.xyMarkerColor = vclr[v]
         tres.xyDashPattern = vdsh[v]
         tplot = ngl.xy(wks, data_list_list[v][c], lev_list_list[v][c], tres)

         if v==0 :
            plot[ip] = tplot
         else:
            ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''

      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      hs.set_subtitles(wks, plot[ip], case_name[c], ctr_str, '', font_height=0.01)
     
#-------------------------------------------------------------------------------
# Create plot 2 - overlay all cases for each var
#-------------------------------------------------------------------------------
if not overlay_vars:
   for v in range(num_var):
      data_list = data_list_list[v]
      lev_list = lev_list_list[v]
      
      tres = copy.deepcopy(res)

      if var[v] in ['O3','Mass_so4']: tres.xyXStyle = 'Log'

      # ip = v*num_case+c
      ip = c*num_var+v
      
      baseline = data_list[0]
      if diff_mode==1 :
         baseline1 = data_list[0]
         baseline2 = data_list[1]
      if diff_mode==2 :
         baseline1 = data_list[0]
         baseline2 = data_list[2]
      if plot_diff:
         for c in range(num_case): 
            if diff_mode==1 :
               if c==0 or c==2 : baseline = baseline1
               if c==1 or c==3 : baseline = baseline2
            if diff_mode==2 :
               if c==0 or c==1 : baseline = baseline1
               if c==2 or c==3 : baseline = baseline2
            data_list[c] = data_list[c] - baseline

      
      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      ip = v

      for c in range(num_case):
         tres.xyLineColor   = clr[c]
         tres.xyMarkerColor = clr[c]
         tres.xyDashPattern = dsh[c]

         tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)

         if diff_mode==0 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c>0) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==1 :
            if (c==2 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=1) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==2 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=2) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''
      var_str = var[v]
      if var[v]=='CLOUD': var_str = 'Cloud Fraction'


      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      if plot_diff: var_str += ' (diff)'

      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)

      hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str, font_height=0.01)


#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.08
   lgres.lgLabelFontHeightF = 0.012
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 8
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   lx,ly = 0.5,0.45
   if num_var==2: lx,ly = 0.3,0.45
   if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [1,len(plot)]
# layout = [int(np.ceil(num_var/4.)),4]
# layout = [num_var,num_case]
# layout = [num_case,num_var]
# layout = [1,num_var]

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]


# if num_var==1  : layout = [int(np.ceil(num_case/2.)),2]
# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]

# if num_case==1 and num_var==4 : layout = [2,2]
# if num_case==1 and num_var==6 : layout = [3,2]

#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
