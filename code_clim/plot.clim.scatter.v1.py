# create scatter plot of precip variance to show grid imprinting
# v1 - plot variance against mean (doesn't work)
# v2 - plot precip variance against omega variance
import os, ngl, sys, copy, xarray as xr, numpy as np, numba
from scipy.stats import skew
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
home = os.getenv("HOME")
print()

def add_case(case_in,n=None,d=0,m=16,c='black'):
   global name,case,clr,dsh,mrk
   case.append(case_in) ; dsh.append(d) ; clr.append(c) ; mrk.append(m)
   if n is None: n = '' ; name.append(n)

name,case,clr,dsh,mrk = [],[],[],[],[]


# add_case('CESM.ne30pg2_ne30pg2_mg17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20'             ,n='SPCAM-SE 64x2km L20',c='red')
add_case('CESM.ne30pg2_ne30pg2_mg17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22'             ,n='SPCAM-SE 64x2km L22',c='red')
# add_case('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20.CRMNZ_20',n='E3SM-MMF L20'       ,c='blue')
add_case('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22.CRMNZ_22',n='E3SM-MMF L22'       ,c='blue')



# xvar, yvar = 'TMQ', 'PRECT'
# xvar, yvar = 'TMQ', 'P-E'
# xvar, yvar = 'TGCLDLWP', 'PRECT'
# xvar, yvar = 'PRECT', 'NET_TOA_RAD'
# xvar, yvar = 'PRECT', 'FLNT'
# xvar, yvar = 'MMF_TLS', 'MMF_DT'
xvar, yvar = 'MMF_QTLS','MMF_DQ'


# lev = -12
# lev = -14

# lev = -71  # 
# lev = -58  # ~ 860 hPa
# lev = 45  # ~ 492 hPa

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_clim/clim.scatter.v1"


# lat1,lat2 = -10, 10
# lat1,lat2 = 50, 60
# xlat,xlon,dy,dx = 0,180,30,50; 
# xlat,xlon,dy,dx = 10,30,30,50; 
# if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

htype,years,months,first_file,num_files = 'h1',[],[],5,1

integrate_vert = True
daily_mean = False
overlay_cases = True
# recalculate = True

# temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

if 'mrk' not in locals():
   mrk = [16]*num_case
elif mrk==[]:
   mrk = [16]*num_case

if 'name' not in locals() or name==[]: 
   name = [None]*(num_case)
   for c in range(num_case): name[c] = '*'+case[c][-30:]

if 'clr' not in vars() : clr = ['black']*num_case
# if 'clr' not in vars() : if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)

if integrate_vert:
   # lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
   # lev = np.array([100,150,200,300,400,500,600,700,800,850,900,950,1000])
   lev = np.array([0])


wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 12
res.xyMarkLineMode = 'Markers'
# res.xyMarkLineMode = 'MarkLines'
res.xyMarker = 1
res.xyDashPattern = 2
# res.xyMarkerSizeF = 0.015
res.xyMarkerSizeF = 0.001
# res.xyXStyle = "Log"
# res.xyYStyle = "Log"
# res.xyLineColors   = clr

lres = hs.res_xy()
lres.xyDashPattern    = 0
lres.xyLineThicknessF = 1
lres.xyLineColor      = "black"

unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
# res.tiXAxisString = '[mm/day]'
# res.tiYAxisString = '[mm/day]'

# var_str = var
# if var=="PRECT"      : var_str = "Precipitation"
# if var=="OMEGA"      : var_str = str(lev)+"mb Omega"


def get_comp(case):
   comp = 'eam'
   if case=='ERA5': comp = None
   if case=='MAC': comp = None
   if 'CESM' in case: comp = 'cam'
   if case=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'
   return comp

#---------------------------------------------------------------------------------------------------
# routine for calculating time derivative
#---------------------------------------------------------------------------------------------------
@numba.njit()
def ddt(data_in,ntime,ncol,dt,data_out,method=1) :
   for t in range(1,ntime):
      # data_out[t,:] = ( data_in[t+1,:] - data_in[t-1,:] ) / dt
      if method==0: data_out[t] = ( data_in[t+1] - data_in[t-1] ) / (dt*2)
      if method==1: data_out[t] = ( data_in[t] - data_in[t-1] ) / (dt)

@numba.njit()
def hybrid_dp3d( PS, P0, hyai, hybi, dp3d ):
   nlev = len(hyai)-1
   (ntime,ncol) = PS.shape
   # dp3d = np.zeros([ntime,nlev,ncol])
   for t in range(ntime):
      for i in range(ncol):
         for k in range(nlev):
            p1 = hyai[k  ]*P0 + hybi[k  ]*PS[t,i]
            p2 = hyai[k+1]*P0 + hybi[k+1]*PS[t,i]
            dp3d[t,k,i] = p2 - p1
   # return dp3d

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []

for c in range(num_case):

   print('    case: '+case[c])
   # case_obj = he.Case( name=case[c], time_freq='daily' )
   case_obj = he.Case( name=case[c] )
   if 'name' in vars() : case_obj.short_name = name[c]
   case_name.append( case_obj.short_name )

   # tmp_file = temp_dir+"/clim.scatter.v1."+case[c]+"."+var+".lat1_"+str(lat1)+".lat2_"+str(lat2)+".nc"
   # print('    tmp_file: '+tmp_file )

   # if recalculate :
   if True:
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      ### uncommentt this to subset the data
      if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
      # if 'lon1' in vars() : case_obj.lon2, case_obj.lon2 = lon2, lon2
      if 'lev' not in vars() : lev = np.array([-1])

      area = case_obj.load_data('area',component=get_comp(case[c]),htype=htype,num_files=1).astype(np.float)
      # X = case_obj.load_data(xvar,component=get_comp(case[c]),htype=htype,lev=lev,years=years,months=months,first_file=first_file,num_files=num_files)
      # Y = case_obj.load_data(yvar,component=get_comp(case[c]),htype=htype,lev=lev,years=years,months=months,first_file=first_file,num_files=num_files)
      X = case_obj.load_data(xvar,component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
      Y = case_obj.load_data(yvar,component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)

      #-------------------------------------------------------------------------
      if integrate_vert:
         ps = case_obj.load_data('PS',component=get_comp(case[c]),htype=htype,first_file=first_file,num_files=num_files)
         # dp3d = hc.calc_dp3d(ps,Y['lev'])
         
         hyai = case_obj.load_data('hyai',component=get_comp(case[c]),htype=htype,num_files=1)
         hybi = case_obj.load_data('hybi',component=get_comp(case[c]),htype=htype,num_files=1)
         # , len(Y['time']), len(Y['ncol']), len(Y['lev'])+1 
         # dp3d = hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values )
         dp3d = np.zeros( Y.shape )
         hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values, dp3d )
         dp3d = xr.DataArray(dp3d,dims=Y.dims)
         # print()
         # print(Y)
         # print()
         # print(dp3d)
         # print()
         
         
         X = ( X * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')
         Y = ( Y * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')
      #-------------------------------------------------------------------------
         



      # print('lev:')
      # print(X['lev'].values)
      # print(Y['lev'].values)
      # print()

      ### area average
      # X = ( (X*area).sum(dim='ncol') / area.sum(dim='ncol') )
      # Y = ( (Y*area).sum(dim='ncol') / area.sum(dim='ncol') )

      # # Convert to daily mean
      if htype=='h1' and daily_mean: 
         X = X.resample(time='D').mean(dim='time')
         Y = Y.resample(time='D').mean(dim='time')

      
      ### combine columns into single dimension
      if 'ncol' in Y.dims: Y.stack( allcols=('ncol','time') )
      if 'ncol' in X.dims: X.stack( allcols=('ncol','time') )

      # print(); print(Y); print()

      # print("start DDT...")
      # sys.stdout.write("\rstart DDT... %i" % i)
      # sys.stdout.flush()

      ### calculate DDT? (I forget why I wrote this block)
      # if yvar=='TMQ':
      #    dt = Y['time'].values[1] - Y['time'].values[0]
      #    dt = dt.total_seconds() / 86400.
      #    ntime = len(Y['time'])
      #    # ncol = len(Y['ncol'])
      #    # dYdt = np.zeros([ntime,ncol], dtype=np.float32)
      #    ncol = 0
      #    dYdt = np.zeros([ntime], dtype=np.float32)
      #    ddt( Y.values, ntime, ncol, dt, dYdt )

      #    for i in range(len(dYdt)):
      #       if dYdt[i]>20: dYdt[i] = 0
         
      #    # t1,t2 = 1,ntime-1
      #    t1,t2 = 1,ntime
      #    # X = X[1:ntime-1,:]
      #    # Y = xr.DataArray( dYdt[1:ntime-1,:], coords=X.coords )
      #    # if xvar != 'P-E': X = X[t1:t2]
      #    X = X[t1:t2]
      #    Y = xr.DataArray( dYdt[t1:t2], coords=X.coords )
         
      


      # print("end DDT\n")
      # sys.stdout.write("\rstart DDT... Done %i" % i)
      # sys.stdout.flush()

      # if 'lev' in X.dims : X = X.isel(lev=0)
      # if 'lev' in Y.dims : Y = Y.isel(lev=0)

      # print some sanity check stuff
      # for t in range(0,5): print(f'  t:  {t}   X: {X[t].values}   Y: {Y[t].values}')

      

      hc.print_stat(X,name=xvar,stat='naxs',compact=True)
      hc.print_stat(Y,name=yvar,stat='naxs',compact=True)
      print()
      
      # exit()

      #-------------------------------------------------------------------------
      # Calculate the mean and variance at each point
      #-------------------------------------------------------------------------
      # dims   = ('ncol')
      # stat_ds = xr.Dataset()
      # stat_ds['avg'] = (dims, X.mean(dim='time') )
      # stat_ds['std'] = (dims, X.std(dim='time') )
      # stat_ds['var'] = (dims, X.var(dim='time') )
      # # stat_ds['skw'] = (dims, skew(X,axis=0,nan_policy='omit') )
      # stat_ds.coords['ncol'] = ('ncol',X.coords['ncol'])

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
   #    stat_ds.to_netcdf(path=tmp_file,mode='w')
   # else:
   #    stat_ds = xr.open_mfdataset( tmp_file )

   x_list.append(X)
   y_list.append(Y)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

x_min = np.min([np.nanmin(d) for d in x_list])
x_max = np.max([np.nanmax(d) for d in x_list])

y_min = np.min([np.nanmin(d) for d in y_list])
y_max = np.max([np.nanmax(d) for d in y_list])

for c in range(num_case):

   X = x_list[c]
   Y = y_list[c]

   # res.xyExplicitLegendLabels = case_name
   # res.pmLegendDisplayMode    = "Always"
   # res.pmLegendOrthogonalPosF = -1.13
   # res.pmLegendParallelPosF   =  0.8+0.04
   # res.pmLegendWidthF         =  0.16
   # res.pmLegendHeightF        =  0.12   
   # res.lgBoxMinorExtentF      =  0.16   


   # xvar,xstr = stat_ds['avg'].values, 'Mean'
   # xvar,xstr = stat_ds['std'].values, 'Std. Deviation'
   # xvar,xstr = stat_ds['var'].values, 'Variance'
   # xvar,xstr = stat_ds['skw'].values, 'Skewness'


   # yvar,ystr = stat_ds['avg'].values, 'Mean'
   # yvar,ystr = stat_ds['std'].values, 'Std. Deviation'
   # yvar,ystr = stat_ds['var'].values, 'Variance'
   # yvar,ystr = stat_ds['skw'].values, 'Skewness'

   res.tiXAxisString = xvar
   res.tiYAxisString = yvar

   if xvar=='P-E': res.tiXAxisString = 'P-E [mm/day]'
   if yvar=='TMQ': res.tiYAxisString = 'd<Q>/dt [mm/day]'

   px = X.stack().values
   py = Y.stack().values

   if overlay_cases:
      x_mag = np.abs(x_max-x_min)
      y_mag = np.abs(x_max-x_min)
      res.trXMinF = x_min - x_mag*0.02
      res.trXMaxF = x_max + x_mag*0.02
      res.trYMinF = y_min - y_mag*0.02
      res.trYMaxF = y_max + y_mag*0.02
   else:
      res.trXMinF = np.min(px)
      res.trXMaxF = np.max(px)

   res.xyLineColor = clr[c]
   res.xyMarkerColor = clr[c]
   res.xyMarker      = mrk[c]
   
   tplot = ngl.xy(wks, px, py, res)
   if overlay_cases:
      if c==0:
         plot.append( tplot )
      else:
         ngl.overlay( plot[len(plot)-1], tplot )
   else:
      plot.append( tplot )

   #----------------------------------------
   # add linear fit

   ### simple and fast method for regression coeff
   a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )

   ### intercept value
   b = np.mean(py) - a*np.mean(px)

   print()
   print(f'    case: {case[c]}')
   print(f'    linear regression a: {a}    b: {b}\n')

   tres = copy.deepcopy(lres)
   tres.xyLineColor = clr[c]
   tres.xyDashPattern    = dsh[c]
   tres.xyLineThicknessF = 6

   px_range = np.abs( np.max(px) - np.min(px) )
   lx = np.array([-1e2*px_range,1e2*px_range])

   tplot = ngl.xy(wks, lx, lx*a+b , tres)
   ngl.overlay( plot[len(plot)-1], tplot )

   #----------------------------------------
   # Add horizontal lines
   ngl.overlay( plot[len(plot)-1], ngl.xy(wks,[0,0],[-1e8,1e8],lres) )
   ngl.overlay( plot[len(plot)-1], ngl.xy(wks,[-1e8,1e8],[0,0],lres) )

   if not overlay_cases:
      hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], '', '', font_height=0.015)


#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

# res.xyExplicitLegendLabels = case_name
# res.pmLegendDisplayMode    = "Always"
# res.pmLegendOrthogonalPosF = -1.13
# res.pmLegendParallelPosF   =  0.8+0.04
# res.pmLegendWidthF         =  0.16
# res.pmLegendHeightF        =  0.12   
# res.lgBoxMinorExtentF      =  0.16   

# res.trXMinF = np.min(x_list)
# res.trXMaxF = np.max(x_list)

### plot stddev vs mean
# plot.append( ngl.xy(wks, np.stack(x_list) , np.stack(y_list) ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)


# ### plot accumulation
# res.pmLegendDisplayMode = "Never"
# res.tiYAxisString = 'Rain Amount [mm/day]'
# plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(acc_list) ,res) )
# # hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------