import os, copy, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)

var,lev_list,mask_flag = [],[],[]
def add_var(var_name,lev=-1,mask=None): 
   var.append(var_name); lev_list.append(lev),mask_flag.append(mask)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

### 2022 coupled historical runs
tmp_scratch = '/global/cfs/cdirs/m3312/whannah/2023-CPL/'
add_case('E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='E3SM-MMF',c='blue',p=tmp_scratch,s='archive/atm/hist')
# tmp_scratch = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/e3smv2_historical'
tmp_scratch = '/global/cfs/cdirs/m3312/whannah/e3smv2_historical'
add_case('v2.LR.historical',n='E3SMv2',c='red',p=tmp_scratch,s='archive/atm/hist')
# add_case('v2.LR.historical_0101',n='E3SMv2',c='red',p=tmp_scratch,s='archive/atm/hist')
# add_case('v2.LR.historical_0151',n='E3SMv2',c='red',p=tmp_scratch,s='archive/atm/hist')
# add_case('v2.LR.historical_0201',n='E3SMv2',c='red',p=tmp_scratch,s='archive/atm/hist')
# add_case('v2.LR.historical_0251',n='E3SMv2',c='red',p=tmp_scratch,s='archive/atm/hist')
# add_case('v2.LR.historical_0301',n='E3SMv2',c='red',p=tmp_scratch,s='archive/atm/hist')

add_var('TS')
# add_var('TS',mask='lnd')
# add_var('TS',mask='ocn')


# htype,years,months,first_file,num_files = 'h0',[],[],0,12*2
htype,years,months,first_file,num_files = 'h0',[],[],0,12*65
# htype,years,months,first_file,num_files = 'h1',[],[],0,1
# htype,years,months,first_file,num_files = 'h0',[],[],0*12,10*12
# htype,years,months,first_file,num_files = 'h1',[],[],0,int(365/2)

# lat1,lat2 = -30,30


fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.timeseries.global_mean.Tsfc.v1','png'

write_file    = False
print_stats   = True
overlay_cases = True

convert_to_daily_mean  = False
convert_to_annual_mean = True

recalculate = False

add_obs_TS = True
anomaly_yr1,anomaly_yr2 = 1961,1990 # match HadCRU
file_obs = '/global/cfs/cdirs/m3312/whannah/obs_data/HadCRU/HadCRUT.5.0.1.0.analysis.anomalies.ensemble_mean.nc'
      # file_obs = '/gpfs/alpine/scratch/hannah6/cli115/Obs/HadCRU/HadCRUT.5.0.1.0.analysis.anomalies.ensemble_mean.nc'

add_trend = False

num_plot_col  = 2

# year_start = 1950 + first_file/12
# year_start = 00 + first_file/12

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

# # overlay_cases with single case causes segfault
# if num_case==1 : overlay_cases = False

if 'clr' not in vars() or clr==[]: clr = ['black']*num_case
if 'dsh' not in vars() or dsh==[]: dsh = np.zeros(num_case)

if 'lev' not in vars(): lev = np.array([0])

if add_obs_TS:
   clr.insert(0,'black')
   dsh.insert(0,0)

#-------------------------------------------------------------------------------
# plot legend in separate file
#-------------------------------------------------------------------------------
if num_case>1:
   legend_file = fig_file+'.legend'
   wkres = ngl.Resources() #; npix = 1024 ; wkres.wkWidth,wkres.wkHeight=npix,npix
   lgd_wks = ngl.open_wks('png',legend_file,wkres)
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.06
   lgres.vpHeightF          = 0.06#*num_case
   lgres.lgLabelFontHeightF = 0.008
   lgres.lgLabelFont        = "courier"
   lgres.lgMonoDashIndex    = False
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 8#16
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   indent = ' '*4
   labels = case_name
   for i in range(len(labels)): labels[i] = indent+labels[i] 

   if add_obs_TS: 
      labels.insert(0,indent+'HadCRU')


   pid = ngl.legend_ndc(lgd_wks, len(labels), labels, 0.5, 0.65, lgres)

   ngl.frame(lgd_wks)
   hc.trim_png(legend_file)

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if overlay_cases:
   plot = [None]*(num_var)
else:
   plot = [None]*(num_case*num_var)

wkres = ngl.Resources()
npix=1024*4; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

if 'legend_file' in locals(): lgd_wks = ngl.open_wks('png',legend_file,wkres)


res = hs.res_xy()
res.vpHeightF = 0.4
# res.vpHeightF = 0.2
res.tmYLLabelFontHeightF   = 0.015
res.tmXBLabelFontHeightF   = 0.015
res.tiXAxisFontHeightF     = 0.015
res.tiYAxisFontHeightF     = 0.015
res.xyLineThicknessF       = 20

lres = hs.res_xy()
lres.xyDashPattern    = 1
lres.xyLineThicknessF = 1
lres.xyLineColor      = "black"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)

   if 'lev_list' in locals(): lev = lev_list[v]

   time_list,data_list = [],[]
   for c in range(num_case):

      tmp_file = os.getenv('HOME')+f'/Research/E3SM/data_temp/clim.timeseries.global_mean.Tsfc.{case[c]}.{var[v]}.nc'
      # '.lev_{lev[0]}.nc'
      # '.lat1_{lat1}.lat2_{lat2}.nc'

      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
      print('    time series file: '+tmp_file)

      if recalculate:

         #----------------------------------------------------------------------
         # set up the case object
         data_dir_tmp,data_sub_tmp = None, None
         # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
         if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         if case_sub[c] is not None: data_sub_tmp = case_sub[c]

         case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )

         tvar = var[v]

         if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
         if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

         #----------------------------------------------------------------------
         # read the data
         tmp_first_file = first_file
         if 'v2.LR.historical' in case[c]: tmp_first_file = 50*12 + first_file

         lat = case_obj.load_data('lat',  htype=htype)
         lon = case_obj.load_data('lon',  htype=htype)
         area = case_obj.load_data('area',htype=htype,num_files=1).astype(np.double)
         data = case_obj.load_data(tvar,  htype=htype, \
                                   first_file=tmp_first_file,\
                                   num_files=num_files,lev=lev)
         
         # time = data.time - data.time[0]

         time_bnds = case_obj.load_data('time_bnds',htype=htype,first_file=tmp_first_file,num_files=num_files)
         time = time_bnds.isel(nbnd=0)

         # print(); print(time)


         # if htype in ['h0'] and convert_to_annual_mean:
         #    num_time = len(data.time)
         #    #print(num_time); print(np.floor(num_time/12)*12); exit()
         #    data = data.isel(time=slice(0,int(np.floor(num_time/12)*12)))

         #----------------------------------------------------------------------
         # deal with land mask and area weighted spatial averaging

         # if 'levgrnd' in data.dims: 
         #    data = data.rename({'lndgrid':'ncol'})
         #    area = area.rename({'lndgrid':'ncol'})

         land_frac = case_obj.load_data('LANDFRAC',htype='h0',first_file=0,num_files=1).astype(np.double)
         if 'time' in land_frac.dims: land_frac = land_frac.isel(time=0)
         land_frac = land_frac / land_frac.max()

         mask = xr.DataArray( np.ones(land_frac.shape,dtype=bool), dims=land_frac.dims )
         if mask_flag[v]=='lnd': mask = mask & (land_frac.values>0.5)
         if mask_flag[v]=='ocn': mask = mask & (land_frac.values<0.5)

         # if mask_flag[v]=='lnd/ocn ratio':
         #    mask = xr.DataArray( np.ones(land_frac.shape,dtype=bool), dims=land_frac.dims )
         #    lnd_mask = mask & (land_frac.values>0.5)
         #    ocn_mask = mask & (land_frac.values<0.5)
         #    # data = data.where( lnd_mask, drop=True) / data.where( ocn_mask, drop=True)
         #    lnd_data = data.where( lnd_mask, drop=True)
         #    ocn_data = data.where( ocn_mask, drop=True)
         #    lnd_area = area.where( lnd_mask, drop=True)
         #    ocn_area = area.where( ocn_mask, drop=True)
         #    lnd_data = ( (lnd_data*lnd_area).sum(dim='ncol') / lnd_area.sum(dim='ncol') )
         #    ocn_data = ( (ocn_data*ocn_area).sum(dim='ncol') / ocn_area.sum(dim='ncol') )
         #    data = lnd_data / ocn_data
         # else:
         
         data = data.where( mask, drop=True)
         data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )

         # if print_stats: hc.print_stat(data,name='',stat='naxs',indent=' '*6,compact=True)
         
         #----------------------------------------------------------------------
         # temporal averaging

         # Convert to daily mean
         if htype in ['h1','h2'] and convert_to_daily_mean: 
            data = data.resample(time='D').mean(dim='time')

         if htype in ['h0'] and convert_to_annual_mean: 
            # # truncate months past the last full year
            extra_months = len(time)%12
            print(f'  extra_months: {extra_months}')
            if extra_months !=0: 
               data = data.isel(time=slice(0,-1-extra_months))
               time = time.isel(time=slice(0,-1-extra_months))
            month_length = time.dt.days_in_month
            month_length['time'] = time
            data['time'] = time
            mn_wgts = month_length.groupby("time.year") / month_length.groupby("time.year").sum()
            data = (data*mn_wgts).resample(time='A').sum('time') / (mn_wgts).resample(time='A').sum(dim='time')

         #----------------------------------------------------------------------
         # Get rid of lev dimension
         if np.all( lev < 0 ) and 'lev' in data.coords : print(f'    lev value: {data.lev.values}')
         # if 'levgrnd' in data.dims : data = data.sum(dim='levgrnd')
         # if 'levgrnd' in data.dims : data = data.isel(levgrnd=9)     # deepest layer
         if 'lev' in data.dims : data = data.isel(lev=0)

         #----------------------------------------------------------------------
      
         tmp_ds = xr.Dataset( coords=data.coords )
         tmp_ds[var[v]] = data
         tmp_ds.to_netcdf(path=tmp_file,mode='w')
      #-------------------------------------------------------------------------
      else:
         if case[c]=='v2.LR.historical':
            v2_amip_ens_list = []
            v2_amip_ens_list.append('v2.LR.historical_0101')
            v2_amip_ens_list.append('v2.LR.historical_0151')
            v2_amip_ens_list.append('v2.LR.historical_0201')
            v2_amip_ens_list.append('v2.LR.historical_0251')
            v2_amip_ens_list.append('v2.LR.historical_0301')
            cnt = 0
            for e,ens_member in enumerate(v2_amip_ens_list):
               tmp_file = os.getenv('HOME')+f'/Research/E3SM/data_temp/clim.timeseries.global_mean.Tsfc.{ens_member}.{var[v]}.nc'
               tmp_ds = xr.open_dataset( tmp_file, use_cftime=True  )
               ens_member_data = tmp_ds[var[v]]
               if cnt==0: 
                  data = xr.zeros_like(ens_member_data)
                  ens_min = ens_member_data.copy()
                  ens_max = ens_member_data.copy()
               else:
                  for t in range(len(ens_member_data)):
                     ens_min[t] = np.min([ens_min[t].values,ens_member_data[t].values])
                     ens_max[t] = np.max([ens_max[t].values,ens_member_data[t].values])
               data = ( data*cnt + ens_member_data ) / (cnt+1)
               cnt += 1
         else:
            tmp_ds = xr.open_dataset( tmp_file, use_cftime=True  )
            data = tmp_ds[var[v]]

      #-------------------------------------------------------------------------
      # #reset time index to start at zero and convert to days
      # dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
      # print('      Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

      #-------------------------------------------------------------------------
      # Make time start at zero
      # year_start = 1950 + first_file/12
      year_start = data['time.year'].values[0]

      data['time'] = ( data['time'] - data['time'][0] ).astype('float') / 86400e9 / 365 + year_start
      
      # # convert to anomaly
      # avg_X = avg_X - avg_X.mean()

      #-------------------------------------------------------------------------
      # redefine values as anomalies
      if var[v]=='TS' and add_obs_TS:
         # use these years to redefine simulation data as anomalies relative to a climatology
         clim_num_years = anomaly_yr2 - anomaly_yr1 + 1
         yr = data['time'].values
         for t,y in enumerate(yr):
            if y>=year_start: t_start=t;break

         mean_for_anomalies = data.isel(time=slice(t_start,t_start+clim_num_years)).mean()
         data = data - mean_for_anomalies
         
         if case[c]=='v2.LR.historical':
            ens_min = ens_min - mean_for_anomalies
            ens_max = ens_max - mean_for_anomalies

      #-------------------------------------------------------------------------
      time_mean = data.mean(dim='time').values
      # print('      Area Weighted Time Mean : '+hc.tcolor.GREEN+f'{time_mean:10.6f}'+hc.tcolor.ENDC)
      print('      Area Weighted Time Mean : '+hc.tcolor.GREEN+f'{time_mean}'+hc.tcolor.ENDC)

      if print_stats: hc.print_stat(data,name='',stat='naxs',indent=' '*6,compact=True)

      data_list.append( data.values )
      time_list.append( data['time'].values )

      #-------------------------------------------------------------------------
      # write to file
      #-------------------------------------------------------------------------
      # if write_file : 
      #    tfile = f'/global/homes/w/whannah/E3SM/scratch/{case[0]}/run/{case[0]}.time_series.{var[v]}.nc'
      #    print('writing to file: '+tfile)
      #    avg_X.name = var[v]
      #    avg_X.to_netcdf(path=tfile,mode='w')
      #    exit()

   #----------------------------------------------------------------------------
   # Add Obs data
   #----------------------------------------------------------------------------
   if var[v]=='TS' and add_obs_TS:
      print(' '*4+'Loading HadCRU data...')
      ds_obs = xr.open_dataset(file_obs)
      data_obs = ds_obs['tas_mean']
      # hc.print_stat(data_obs,name='HadCRU',stat='naxs',indent=' '*6,compact=True)
      xy_dims = ('longitude','latitude')
      xlon, ylat = np.meshgrid(ds_obs['latitude'],ds_obs['longitude'])
      R = hc.earth_radius(ylat)
      dlat = np.deg2rad(np.gradient(ylat, axis=0))
      dlon = np.deg2rad(np.gradient(xlon, axis=1))
      dy,dx = dlat * R , dlon * R * np.cos(np.deg2rad(ylat))
      area_obs = np.absolute(dy*dx) / np.square(R) # calculate area and convert to steridians
      area_obs = xr.DataArray(area_obs,dims=xy_dims).transpose()
      gbl_mean_obs = ( (data_obs*area_obs).sum(dim=xy_dims) / area_obs.sum(dim=xy_dims) )

      if convert_to_annual_mean: 
         time_obs = ds_obs['time_bnds'].isel(bnds=0)
         month_length = time_obs.dt.days_in_month
         month_length['time'] = time_obs
         gbl_mean_obs['time'] = time_obs
         wgts = month_length.groupby("time.year") / month_length.groupby("time.year").sum()
         gbl_mean_obs = (gbl_mean_obs*wgts).resample(time='A').sum('time') / (wgts).resample(time='A').sum(dim='time')

      hc.print_stat(gbl_mean_obs,name='HadCRU',stat='naxs',indent=' '*6,compact=True)

      if convert_to_annual_mean:
         # time_obs = ds_obs['time.year'].isel[slice(0,0,365)]
         time_obs = ds_obs['time.year'].resample(time='Y').mean(dim='time')
         # time_obs = time_obs.resample(time='Y').mean(dim='time')
         # time_obs_yr = ds_obs['time.year']
      else:
         time_obs = ds_obs['time.year'] + ds_obs['time.dayofyear']/365
      
      # time_obs = time_obs - year_start

      # limit extent of obs data to match size of model data
      sim_num_t = np.max([len(d) for d in time_list])


      # print()
      # print(time_obs.values)
      # print()
      # print(time_obs_yr.values)
      # print()
      # print(ds_obs['time.year'].values)
      # print()
      # exit()

      for t,y in enumerate(time_obs):
         # if y>=0: t_start=t;break
         if y>=year_start: t_start=t;break

      # print(); print(f't_start: {t_start}'); print()

      gbl_mean_obs = gbl_mean_obs.isel(time=slice(t_start,t_start+sim_num_t))
      time_obs     = time_obs    .isel(time=slice(t_start,t_start+sim_num_t))

      data_list.insert(0, gbl_mean_obs.values )
      time_list.insert(0, time_obs.values )


      # print();print(ds_obs)
      # print();print(ds_obs.time)
      # print();print(time_obs)
      # print();print(gbl_mean_obs)

      # for i,y in enumerate(time_obs.values):
      #    if i%12==0:
      #       print(f'  {i:4d}  {y}  {gbl_mean_obs.values[i]}')
      #       # if i==12: exit()

      # tres.xyLineColor   = 'black'
      # tres.xyDashPattern = 0
      # ngl.overlay(plot[ip], ngl.xy(wks, time_obs.values, gbl_mean_obs.values , tres) )

      # replace plot with obs-obly plot for debugging
      # tres = copy.deepcopy(res)
      # tres.trXMinF = np.min(time_obs.values)
      # tres.trXMaxF = np.max(time_obs.values)
      # plot[ip] = ngl.xy(wks, time_obs.values, gbl_mean_obs.values , tres)

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.tiXAxisString = 'Time [years]'
   # tres.tiXAxisString = 'Time [years]'
   if convert_to_annual_mean: tres.tiXAxisString = 'Time [years]'

   # # reset start year for all data
   # for t in range(len(time_list)):
   #    time_list[t] = time_list[t] + year_start


   ### Make sure plot bounds are consistent
   # tres.trYMinF = 14.4
   tres.trYMinF = np.min([np.nanmin(d) for d in data_list])
   tres.trYMaxF = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
   tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])

   # print()
   # print(f'tres.trYMinF = {tres.trYMinF}')
   # print(f'tres.trYMaxF = {tres.trYMaxF}')
   # print(f'tres.trXMinF = {tres.trXMinF}')
   # print(f'tres.trXMaxF = {tres.trXMaxF}')
   # print()

   if var[v]=='NET_TOA_RAD':
      tres.trYMinF = -20
      tres.trYMaxF =  20

   tmp_num_case = num_case
   if var[v]=='TS' and add_obs_TS: tmp_num_case = num_case + 1

   for c in range(tmp_num_case):
      ip = c*num_var + v
      if overlay_cases: ip = v
      
      tres.xyLineColor   = clr[c]
      tres.xyDashPattern = dsh[c]
      tplot = ngl.xy(wks, time_list[c], data_list[c], tres)
      
      if overlay_cases: 
         if c==0: 
            plot[ip] = tplot
         else:
            ngl.overlay(plot[ip],tplot)
      else:
         plot[ip] = tplot

      #------------------------------------------------
      # add ensemble spread
      #------------------------------------------------
      ens_case = 'v2.LR.historical'
      if (add_obs_TS and c>0 and case[c-1]==ens_case) or (not add_obs_TS and case[c]==ens_case):
         eres = copy.deepcopy(tres)
         ens_spread_data      = np.zeros([2,len(ens_min)])
         ens_spread_data[0,:] = ens_min.values
         ens_spread_data[1,:] = ens_max.values
         eres.xyLineColor = [0,0,0,0]
         eres.nglXYAboveFillColors = 'orange'    # Indexes into the current color table.
         eres.nglXYBelowFillColors = 'orange'    # Could also use named colors, e.g. "red"
         ngl.overlay(plot[ip], ngl.xy(wks, time_list[c], ens_spread_data, eres) )

      #------------------------------------------------
      # add linear trend
      #------------------------------------------------
      if add_trend:
         px = time_list[c]
         py = data_list[c]
         # simple and fast method for regression coeff and intercept
         a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )
         b = np.mean(py) - a*np.mean(px)

         # print regression info
         # if c==0: print()
         # print(' '*4+f'linear regression a: {a}    b: {b}')
         # if c==(num_case-1): print()

         px_range = np.abs( np.max(px) - np.min(px) )
         lx = np.array([-1e2*px_range,1e2*px_range])

         lres.xyLineColor = clr[c]
         ngl.overlay( plot[ip], ngl.xy(wks, lx, lx*a+b , lres) )
   
   #------------------------------------------------
   #------------------------------------------------


   # if overlay_cases:
   #    ip = v

   #    ### use this for overlaying variables on same plot
   #    for c in range(num_case):
   #       tres.xyLineColor   = clr[c]
   #       tres.xyDashPattern = dsh[c]
   #       tplot = ngl.xy(wks, time_list[c], data_list[c], tres)
   #       if c==0: 
   #          plot[ip] = tplot
   #       else:
   #          ngl.overlay(plot[ip],tplot)

   #       #------------------------------------------------
   #       # add linear trend
   #       #------------------------------------------------
   #       if add_trend:
   #          px = time_list[c]
   #          py = data_list[c]
   #          # simple and fast method for regression coeff and intercept
   #          a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )
   #          b = np.mean(py) - a*np.mean(px)

   #          print(f'\n    linear regression a: {a}    b: {b}\n')

   #          px_range = np.abs( np.max(px) - np.min(px) )
   #          lx = np.array([-1e2*px_range,1e2*px_range])

   #          lres.xyLineColor = clr[c]
   #          ngl.overlay( plot[ip], ngl.xy(wks, lx, lx*a+b , lres) )
   #       #------------------------------------------------
   #       #------------------------------------------------
   # else:
   #    for c in range(num_case):
   #       ip = c*num_var + v
   #       # ip = v*num_case + c
   #       tres.xyLineColor   = clr[c]
   #       tres.xyDashPattern = dsh[c]
   #       plot[ip] = ngl.xy(wks, time_list[c], data_list[c], tres)


   #----------------------------------------------------------------------------
   # Set strings at top of plot
   #----------------------------------------------------------------------------
   var_str = var[v]
   # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
   # if var[v]=="TMQ"   : var_str = "Column Water Vapor [mm]"

   lft_str = ''
   ctr_str = ''
   # if var[v] in ['PRECT','PRECC','PRECL'] : ctr_str = 'Mean: '+'%.2f'%avg_X+' [mm/day]'


   lat_chk,lon_chk = 'lat1' in locals(), 'lon1' in locals()


   if not lat_chk and not lon_chk : 
      if mask_flag[v] is None: ctr_str = 'Global'
      if mask_flag[v]=='lnd' : ctr_str = 'Land Only'
      if mask_flag[v]=='ocn' : ctr_str = 'Ocean Only'
      if mask_flag[v]=='lnd/ocn ratio':ctr_str = 'Land / Ocean Ratio'
   else:
      if lat_chk:      ctr_str += f' {lat1}:{lat2}N '
      if lon_chk:      ctr_str += f' {lon1}:{lon2}E '
   
   

   if overlay_cases:
      hs.set_subtitles(wks, plot[ip], var_str, ctr_str, '', font_height=0.012)
   else:
      hs.set_subtitles(wks, plot[ip], case_name[c], ctr_str, var_str, font_height=0.015)

   #----------------------------------------------------------------------------
   # Ad legend
   #----------------------------------------------------------------------------
   lgres = ngl.Resources()
   lgres.vpWidthF, lgres.vpHeightF  = 0.08, 0.08  
   lgres.lgLabelFontHeightF = 0.01
   lgres.lgLineThicknessF   = 4
   lgres.lgMonoLineColor    = False
   # lgres.lgMonoDashIndex    = True
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh
   lgres.lgLabelJust    = 'CenterLeft'
   # pid = ngl.legend_ndc(wks, len(name), name, 0.5, 0.4, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(name), name, 0.5, 0.1, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(name), name, 0.3, 0.5, lgres)  # 1x2

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
hc.printline()

if 'num_plot_col' in locals():
   if overlay_cases :
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   else:
      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      else:
         layout = [num_var,num_case]
         # layout = [num_case,num_var]
else:
   layout = [num_var,num_case]


ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)

if 'legend_file' in locals(): hc.trim_png(legend_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
