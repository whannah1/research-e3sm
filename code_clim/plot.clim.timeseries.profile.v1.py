import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,d=None,s=None,g=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = case_in.replace('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.','')
      tmp_name = tmp_name.replace('.',' ')
      tmp_name = tmp_name.replace('_','=')
      if tmp_name=='control': tmp_name = 'fixed-HV nu=1e15 hvss=1 hvsq=6'
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name); case_dir.append(d); case_sub.append(s); case_grid.append(g)
var,vclr,vdsh = [],[],[]
def add_var(var_name,clr='black',dsh=0): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh)
#-------------------------------------------------------------------------------

### Rotating RCE for Da Yang
# pscratch = '/pscratch/sd/w/whannah/e3sm_scratch/perlmutter'
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.02',       n='MMF ROT-RCE CTRL'  ,d=pscratch, s='run')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_LW.02',n='MMF ROT-RCE GBL-LW',d=pscratch, s='run')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.FIX_LW.02',n='MMF ROT-RCE FIX-LW',d=pscratch, s='run')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.03', n='MMF ROT-RCE CTRL',    d=pscratch,s='run')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.03', n='MMF ROT-RCE FIX-QRAD',d=pscratch,s='run')
# add_case('E3SM.GNUCPU.ne30pg2.F-EAM-RCEROT.04', n='E3SM RCEROT',d=pscratch,s='run')

### PAM dev tests
add_case('E3SM.PAM-DEV-10.GNUGPU.F2010-MMF-SAM.ne4pg2_ne4pg2'         ,n='SAM'         )
# add_case('E3SM.PAM-DEV-10.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QALL',n='PAM-C F-QALL')
# add_case('E3SM.PAM-DEV-10.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QTOT',n='PAM-C F-QTOT')
# add_case('E3SM.PAM-DEV-11.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QALL',n='PAM-C F-QALL')
# add_case('E3SM.PAM-DEV-11.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QTOT',n='PAM-C F-QTOT')
# add_case('E3SM.PAM-DEV-12.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QALL',n='PAM-C F-QALL')
# add_case('E3SM.PAM-DEV-12.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QTOT',n='PAM-C F-QTOT')
# add_case('E3SM.PAM-DEV-13.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QALL',n='PAM-C F-QALL')
# add_case('E3SM.PAM-DEV-13.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QTOT',n='PAM-C F-QTOT')
# add_case('E3SM.PAM-DEV-14.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QALL',n='PAM-C F-QALL')
# add_case('E3SM.PAM-DEV-15.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QALL',n='PAM-C F-QALL')
add_case('E3SM.PAM-DEV-17.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.CDT_10.F-QALL',n='PAM-C F-QALL')
add_case('E3SM.PAM-DEV-17.GNUCPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.CDT_10.F-QTOT',n='PAM-C F-QTOT')

add_var('T')
add_var('Q')
add_var('CLDLIQ')
add_var('CLDICE')
# add_var('QRS')
# add_var('QRL')

# add_var('MMF_QC')
# add_var('MMF_QI')
# add_var('MMF_QR')

# add_var('MMF_DT')
# add_var('MMF_DQ')
# add_var('MMF_DQC')
# add_var('MMF_DQI')

# add_var('MMF_RHOVLS')
# add_var('MMF_RHOLLS')
# add_var('MMF_RHOILS')

# add_var('Q')
# add_var('O3')
# add_var('U')

# add_var('Q')
# add_var('MMF_RHOVLS')
# add_var('MMF_DQI')

# add_var('MMF_DT_SGS')
# add_var('MMF_DQV_SGS')
# add_var('MMF_DQC_SGS')
# add_var('MMF_DQI_SGS')

# add_var('MMF_QTLS')

# add_var('T')
# add_var('MMF_TLS')
add_var('MMF_DT')
add_var('MMF_DQ')

# add_var('MMF_RHODLS')
# add_var('MMF_RHOVLS')
# add_var('MMF_RHOLLS')
# add_var('MMF_RHOILS')

# num_plot_col = len(var)
num_plot_col = 1

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.timeseries.profile.v1'

# lat1,lat2 = -10,10
lat1,lat2 = -30,30
# lon1,lon2 = -90,-60

# xlat,xlon,dx = 0,150,20 ; lat1,lat2,lon1,lon2 = xlat-dx/2,xlat+dx/2,xlon-dx/2,xlon+dx/2

htype,years,months,first_file,num_files = 'h2',[],[],0,1
# htype,years,months,first_file,num_files = 'h2',[],[],130,30


plot_diff = True    # plot diff from start

use_height_coord   = False

omit_bot,bot_k     = False,20
omit_top,top_k     = False,10

use_common_labelbar = False

var_x_case = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = len(var)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix=4096; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*num_case)
res = hs.res_contour_fill()
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008

res.vpHeightF = 0.2

if use_height_coord: 
   res.trYMaxF = 5
   res.nglYAxisType = "LinearAxis"
else:
   res.trYReverse = True
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   if 'INCITE2019' in case: comp = 'cam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'MAML' in case: comp = 'eam_0001'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,time_list_list,lev_list_list = [],[],[]
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   data_list,time_list,lev_list = [],[],[]
   use_crm_lev = False
   for c in range(num_case):
      print('    case: '+case[c])

      data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      case_obj.set_coord_names(var[v])

      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      area_name = 'area'

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
      if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

      data = case_obj.load_data(tvar,     htype=htype,years=years,months=months,first_file=first_file,num_files=num_files)
      area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)

      #-------------------------------------------------------------------------
      hc.print_time_length(data.time,indent=' '*4)

      # print(' '*4+hc.tcolor.RED+'WARNING: subsetting the time dimension'+hc.tcolor.ENDC)
      # data = data.isel(time=slice(0,10))
      #-------------------------------------------------------------------------

      if 'crm_nx' in data.dims : 
         use_crm_lev = True
         data = data.mean(dim=['crm_nx','crm_ny']).rename({'crm_nz':'lev'})

      if use_height_coord: 
         Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=num_files)

      if np.all( lev < 0 ) : print(f'    lev value: {data.lev.values}')

      data_avg = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      # data_avg = data.max(dim='ncol')

      if use_height_coord: 
         Z_avg = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') ).mean(dim='time')
         Z_avg = Z_avg/1e3 # convert to km

      # if 'time' in data_avg.dims : 
      # avg_X = data_avg.mean(dim='time')
      # print('\n      Area Weighted Global Mean : '+'%f'%avg_X+'\n')

      
      #-------------------------------------------------------------------------
      # Truncate vertical levels
      #-------------------------------------------------------------------------
      if omit_bot:
         data_avg = data_avg[:bot_k]
         if use_height_coord: Z = Z[:bot_k]

      if omit_top:
         data_avg = data_avg[top_k:]
         if use_height_coord: Z = Z[top_k:]
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      hc.print_stat(data_avg,name=var[v],stat='naxs',indent=' '*6,compact=True)

      if plot_diff: data_avg = data_avg - data_avg.isel(time=0)

      if plot_diff: hc.print_stat(data_avg,name=var[v],stat='naxs',indent=' '*6,compact=True)
      #-------------------------------------------------------------------------
      # Add data to list
      #-------------------------------------------------------------------------
      if use_height_coord:
         lev_coord_tmp = Z_avg.values
      else:
         lev_coord_tmp = data_avg['lev'].values

      if use_crm_lev:
         data = data[::-1]
         if use_height_coord:
            nlev_grm,nlev_crm = len(lev_coord_tmp),len(data['lev'].values)
            lev_coord_tmp = lev_coord_tmp[nlev_grm-nlev_crm-1:nlev_grm-1]

      data_list.append( data_avg.transpose().values )
      lev_list.append( lev_coord_tmp )

      # ip = v*num_case+c if var_x_case else c*num_var+v

      # tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      # tres.trYReverse = True
      # tres.sfYArray = gbl_X.lev.values
      # tres.tiXAxisString = 'Time'
      # if htype=='h0': tres.tiXAxisString = 'Time [months]'


      time = data_avg['time']
      time = ( time - time[0] ).astype('float') / 86400e9
      time_list.append(time.values)

   data_list_list.append(data_list)
   time_list_list.append(time_list)
   lev_list_list.append(lev_list)

if use_common_labelbar:
   data_min = np.min( data_list_list[0][c] )
   data_max = np.max( data_list_list[0][c] )
   for v in range(num_var):
      for c in range(num_case):
         data_min = np.min([ data_min, np.nanmin(data_list_list[v][c]) ])
         data_max = np.max([ data_max, np.nanmax(data_list_list[v][c]) ])

for v in range(num_var):
   data_list = data_list_list[v]
   time_list = time_list_list[v]
   lev_list  = lev_list_list[v]
   
   tres = copy.deepcopy(res)
   if htype=='h0': tres.tiXAxisString = 'Time [months]'
   if htype=='h1': tres.tiXAxisString = 'Time [days]'

   #----------------------------------------------------------------------------
   # Set contour/color levels
   #----------------------------------------------------------------------------
   if not use_common_labelbar:
      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])

   tres.cnLevelSelectionMode = 'ExplicitLevels'
   
   aboutZero = True if plot_diff else False
   cmin,cmax,cint,clev = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=21, \
                                              returnLevels=True, aboutZero=aboutZero )
   tres.cnLevels = np.linspace(cmin,cmax,num=21)

   # if plot_diff:
      # baseline = data_list[0].copy()
      # for c in range(1,num_case): data_list[c] = data_list[c] - baseline
      # diff_data_min = np.min([np.nanmin(d) for d in data_list[1:]])
      # diff_data_max = np.max([np.nanmax(d) for d in data_list[1:]])
      # diff_cmin,diff_cmax,diff_cint,diff_clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
      #                                         cint=None, max_steps=21,      \
      #                                         returnLevels=True, aboutZero=True )

   if plot_diff:
      tres.cnFillPalette = 'BlueWhiteOrangeRed'
   else:
      tres.cnFillPalette = "MPL_viridis"
      # res.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
   #----------------------------------------------------------------------------
   # Create plots
   #----------------------------------------------------------------------------
   if v==0:
      print(' '*4+hc.tcolor.RED+'WARNING: subsetting the time dimension to match shortest case'+hc.tcolor.ENDC)
      time_len_min = np.min([len(t) for t in time_list])

   for c in range(num_case):

      tres.lbLabelBarOn = True
      # if c<num_case-1: 
      #    tres.lbLabelBarOn = False
      # else
      #    tres.lbLabelBarOn = True

      

      ip = v*num_case+c if var_x_case else c*num_var+v

      if 'time_len_min' in locals():
         tres.sfYArray = lev_list[c]
         tres.sfXArray = time_list[c][:time_len_min]
         plot[ip] = ngl.contour(wks, data_list[c][:,:time_len_min], tres)
      else:
         tres.sfYArray = lev_list[c]
         tres.sfXArray = time_list[c]
         plot[ip] = ngl.contour(wks, data_list[c], tres)

      #-------------------------------------------------------------------------
      # Set strings at top of plot
      #-------------------------------------------------------------------------
      var_str = var[v]
      # if var[v]=='PRECT' : var_str = 'Precipitation [mm/day]'

      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name

      ctr_str = ''
      # if var[v] in ['PRECT','PRECC','PRECL'] : ctr_str = 'Mean: '+'%.2f'%avg_X+' [mm/day]'
      hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=0.01)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      # if plot_diff and c in diff_case :
      #    X = X-Xo

      #    tres.cnFillPalette = 'ncl_default'
      #    tres.cnLevelSelectionMode = 'ExplicitLevels'
         
      #    if hasattr(tres,'cnLevels') : del tres.cnLevels
      #    # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-12,12+2,2)
      #    if not hasattr(tres,'cnLevels') : 
      #       if X.min().values==X.max().values : 
      #          print('WARNING: Difference is zero!')
      #       else:
      #          cmin,cmax,cint,clev = ngl.nice_cntr_levels(X.min().values,    \
      #                                                     X.max().values,    \
      #                                                     cint=None,         \
      #                                                     max_steps=21,      \
      #                                                     returnLevels=True, \
      #                                                     aboutZero=True )
      #          tres.cnLevels = np.linspace(cmin,cmax,num=21)

      #    hs.set_cell_fill(case_obj,tres)
      #    tres.lbLabelBarOn = True
      #    plot.append( ngl.contour_map(wks,X.values,tres) )

      #    case_name = case_name+' - '+base_name
      #    hs.set_subtitles(wks, plot[len(plot)-1], case_name, '', var_str, font_height=0.01)


      # if plot_diff and c>0: ctr_str = 'Difference'
      
      # hs.set_subtitles(wks, plot[ip], name[c], ctr_str, var[v], font_height=0.015)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
   
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
# layout = [num_var,num_case]

if num_case==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [num_var,num_case] if var_x_case else [num_case,num_var]


# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
