import os, copy, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
case,name,clr,dsh,mrk = [],[],[],[],[]
def add_case(case_in,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
mv_var,mv_lev = [],[]
var,lev_list = [],[]
def add_var(var_name,lev=0): var.append(var_name); lev_list.append(lev)
def add_mv_var(var_name,lev=0): mv_var.append(var_name); mv_lev.append(lev)


### vertical grid tests for new L72
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v0.00',n='E3SM L72v0')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1.00',n='E3SM L72v1')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v2.00',n='E3SM L72v2')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v3.00',n='E3SM L72v3')
add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v0.HF-OUTPUT.00',n='E3SM L72v0',c='red')
add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1.HF-OUTPUT.00',n='E3SM L72v1',c='blue')


# add_var('PRECT')
# add_var('TGCLDLWP')
# add_var('TGCLDIWP')
# add_var('P-E')
# add_var('TS')
# add_var('TBOT'); 
add_var('SHFLX')
# add_var('QBOT'); 
add_var('LHFLX')
add_var('UBOT'); 
# add_var('TAUX')
# add_var('VBOT'); 
# add_var('TAUY')
# add_var('TMQ')
# add_var('FSNS')
# add_var('FLNS')
# add_var('FSNTOA')
# add_var('NET_TOA_RAD')
# add_var('U',lev=850)
# add_var('U',lev=200)

tlev = -71
# add_var('T',lev=tlev)
# add_var('Q',lev=tlev)
# add_var('U',lev=tlev)
# add_var('V',lev=tlev)
# add_var('CLOUD',lev=tlev)
# add_var('CLDLIQ',lev=tlev)
# add_var('MMF_TLS',lev=tlev); add_var('MMF_QTLS',lev=tlev)
# add_var('MMF_DT' ,lev=tlev); add_var('MMF_DQ'  ,lev=tlev)
# add_var('MMF_DU' ,lev=tlev); add_var('MMF_DV'  ,lev=tlev)
# add_var('U' ,lev=tlev); add_var('MMF_DU' ,lev=tlev); add_var('TAUX');

# add_var('T',lev=-71); add_var('SHFLX')
# add_var('Q',lev=-71); add_var('LHFLX')
# add_var('U',lev=-71); add_var('TAUX')
# add_var('V',lev=-71); add_var('TAUY')


htype,years,months,first_file,num_files = 'h1',[],[],0,5
# htype,years,months,num_files = 'h1',[],[],int(100/5)#365


# lat1,lat2 = -30,30
# lat1,lat2 = 30,90
# lon1,lon2 = -90,-60
# lat1,lat2,lon1,lon2 = 30,50,360-125,360-75    # CONUS
# lat1,lat2,lon1,lon2 = -45,-45+60,360-100,360-40  # S. America

clat,clon, dx = -10,360-45, 10 # amazon
lat1,lat2,lon1,lon2 = clat-dx,clat+dx,clon-dx,clon+dx    


land_only = False
ocn_only = False



# mode to find max value of mv_var for first case and use that column for everything
# plots are organized in columns for each mv_var
find_max_val_col = True 
mv_col_shift = 0
# add_mv_var('PRECT')
# add_mv_var('SHFLX')
add_mv_var('LHFLX')
# add_mv_var('TAUX')
# add_mv_var('UBOT')
# add_mv_var('U')

fig_type = "png"
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.timeseries.v1'

write_file    = False
daily_mean    = False
print_stats   = True
overlay_cases = True

num_plot_col  = 1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

# overlay_cases with single case causes segfault
if num_case==1 : overlay_cases = False

if 'name' not in vars(): name = case
if 'clr' not in vars() or clr==[]: clr = ['black']*num_case
if 'dsh' not in vars() or dsh==[]: dsh = np.zeros(num_case)

if 'lev' not in vars(): lev = np.array([0])

if find_max_val_col:
   overlay_cases = True
   num_mv_var = len(mv_var)
   if num_mv_var>1: num_plot_col  = 1
   


if find_max_val_col:
   plot = [None]*(num_mv_var*num_var)
else:
   if overlay_cases:
      plot = [None]*(num_var)
   else:
      plot = [None]*(num_case*num_var)

wkres = ngl.Resources()
npix=4096; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

res = hs.res_xy()
# res.vpHeightF = 0.5
res.vpHeightF = 0.2
res.tmYLLabelFontHeightF         = 0.02
res.tmXBLabelFontHeightF         = 0.02
res.tiXAxisFontHeightF           = 0.02
res.tiYAxisFontHeightF           = 0.02
res.xyLineThicknessF = 10

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def CESM_FV(case):
   CESM_FV = False
   if 'CESM' in case and any([g in case for g in ['f09','f19']]): CESM_FV = True
   return CESM_FV

def get_comp(case):
   comp = 'eam'
   if case=='ERA5': comp = None
   if case=='MAC': comp = None
   if 'CESM' in case: comp = 'cam'
   if case=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
if find_max_val_col :
   case_obj = he.Case( name=case[0] )
   if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
   if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2
   if 'mv_var' in locals():
      data = case_obj.load_data(mv_var[0],component=get_comp(case[0]),htype=htype,years=years,months=months,num_files=num_files,lev=mv_lev[0])
      data = data.where(np.absolute(data)==np.absolute(data.max()), drop=True).squeeze()
      # data = data.where(data==data.max(), drop=True).squeeze()
      icol_mv = data['ncol'].values
   else:
      area = case_obj.load_data('area',component=get_comp(case[0]),htype=htype,num_files=1).astype(np.double)
      icol_mv = area['ncol'].values[0]


   lat = case_obj.load_data('lat',component=get_comp(case[0]),htype=htype)
   lon = case_obj.load_data('lon',component=get_comp(case[0]),htype=htype)

   mv_str = f'{lat.sel(ncol=icol_mv).values:6.2f} N  {lon.sel(ncol=icol_mv).values:6.2f} E'

   print(f'')
   if 'mv_var' in locals():
      print(hc.tcolor.RED+f'  WARNING - using single column located at max value of {mv_var[0]}'+hc.tcolor.ENDC)
   else:
      print(hc.tcolor.RED+f'  WARNING - using single column '+hc.tcolor.ENDC)
   mv_lat = lat.sel(ncol=icol_mv).values
   mv_lon = lon.sel(ncol=icol_mv).values
   print(f'  ncol: {icol_mv}    lat/lon:  {mv_lat:6.2f} / {mv_lon:6.2f} ')
   # print(f'  ncol: {icol_mv}    lat/lon:  {lat.isel(ncol=icol_mv).values:6.2f} / {lon.isel(ncol=icol_mv).values:6.2f} ')
   print(f'')

   # print(icol_mv); exit()
#---------------------------------------------------------------------------------------------------
# find column for max value mode
#---------------------------------------------------------------------------------------------------
if find_max_val_col:
   for m in range(num_mv_var):
   
      c = 0
      case_obj = he.Case( name=case[c] )
      if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
      if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2
      comp = 'eam'
      data = case_obj.load_data(mv_var[m],component=comp,htype=htype,years=years,months=months,
                                 first_file=first_file,num_files=num_files,lev=mv_lev[m])
      ncol_all = data['ncol'].values

      # calculate second derivative to focus on oscillations
      ntime = len(data['time'].values)
      data.load()
      data_alt = xr.zeros_like(data)
      data_alt[1:ntime-1,:] = data[0:ntime-2,:].values - 2*data[1:ntime-1,:].values + data[2:ntime,:].values
      data = data_alt

      data = data.where(np.absolute(data)==np.absolute(data.max()), drop=True).squeeze()
      # data = data.where(data==data.max(), drop=True).squeeze()
      icol_mv = data['ncol'].values

      # shift to get new columns
      if mv_col_shift!= 0: icol_mv = icol_mv+mv_col_shift
      # if icol_mv not in ncol_all:

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# for v in range(num_var):
   
#    hc.printline()
#    print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
#    time_list,data_list = [],[]
#    if 'lev_list' in locals(): lev = lev_list[v]
#    for c in range(num_case):

#       print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
#       # case_obj = he.Case( name=case[c] )

#       case_obj = he.Case( name=case[c] )
#       # case_obj.data_dir=f'/global/homes/w/whannah/E3SM/scratch/{case_obj.name}/{case_obj.data_sub}/'

#       comp = 'eam'

#       tvar = var[v]
#       # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'
#       # if var[v]=='EF': tvar = 'LHFLX'

#       area_name = 'area'
#       # if 'DYN_' in tvar : 
#       #    if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
#       #    if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
#       #    if 'pg2' in case[c] or 'pg3' in case[c] : 
#       #       case_obj.ncol_name = 'ncol_d'
#       #       area_name = 'area_d'

#       if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
#       if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

#       #-------------------------------------------------------------------------
#       # read the data
#       #-------------------------------------------------------------------------
#       data = case_obj.load_data(tvar,     component=get_comp(case[c]),htype=htype,years=years,months=months,num_files=num_files,lev=lev)
#       area = case_obj.load_data(area_name,component=get_comp(case[c]),htype=htype,years=years,months=months,num_files=num_files).astype(np.double)

#       if 'levgrnd' in data.dims: 
#          data = data.rename({'lndgrid':'ncol'})
#          area = area.rename({'lndgrid':'ncol'})

#       lat = case_obj.load_data('lat',component=comp,htype=htype)
#       lon = case_obj.load_data('lon',component=comp,htype=htype)

#       if find_max_val_col: 
#          mv_str = f'{lat.sel(ncol=icol_mv).values:6.2f} N  {lon.sel(ncol=icol_mv).values:6.2f} E'

#          hc.printline()
#          print(hc.tcolor.RED+f'  WARNING - using single column located at max value of {mv_var[m]}'+hc.tcolor.ENDC)
#          print(f'  ncol: {icol_mv}    lat/lon:  {lat.sel(ncol=icol_mv).values:6.2f} / {lon.sel(ncol=icol_mv).values:6.2f} ')
#----------------------------------------------------------------------------
# start main var/case loop
#----------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   time_list,data_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):

      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
      # case_obj = he.Case( name=case[c] )

      if np.all( lev < 0 ) and 'lev' in data.coords : print(f'    lev value: {data.lev.values}')

      case_obj = he.Case( name=case[c] )
      # case_obj.data_dir=f'/global/homes/w/whannah/E3SM/scratch/{case_obj.name}/{case_obj.data_sub}/'

      comp = 'eam'

      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'
      # if var[v]=='EF': tvar = 'LHFLX'

      area_name = 'area'
      # if 'DYN_' in tvar : 
      #    if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
      #    if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
      #    if 'pg2' in case[c] or 'pg3' in case[c] : 
      #       case_obj.ncol_name = 'ncol_d'
      #       area_name = 'area_d'

      if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
      if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      data = case_obj.load_data(tvar,     component=comp,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files,lev=lev)
      area = case_obj.load_data(area_name,component=comp,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)

      # print(hc.tcolor.RED+f'  std: { np.std(data.values)} '+hc.tcolor.ENDC)
      # continue

      # if 'levgrnd' in data.dims: 
      #    data = data.rename({'lndgrid':'ncol'})
      #    area = area.rename({'lndgrid':'ncol'})

      if find_max_val_col: data = data.sel(ncol=icol_mv)

      # if 'CRM_' in var[v]:
      #    z_ind = 72-np.absolute(lev)-1
      #    data = data.isel(crm_ny=0,crm_nz=z_ind)
      #    data = data.isel(crm_nx=slice(16,64)).mean(dim='crm_nx')
      #    data.load()
      #    if data[0,0].values==0 : data[0,:] = data[1,:]

      if print_stats: hc.print_stat(data,name=var[v],stat='naxs',indent='    ',compact=True)

      # Convert to daily mean
      if htype=='h1' and daily_mean: data = data.resample(time='D').mean(dim='time')

      if np.all( lev < 0 ) and 'lev' in data.coords : print(f'    lev value: {data.lev.values}')

      # if var[v]=='EF':
      #       S = case_obj.load_data('SHFLX',htype=htype,years=years,months=months,first_file=first_file,num_files=num_files)
      #       data = data / (data+S)

      # if 'levgrnd' in data.dims : data = data.sum(dim='levgrnd')
      # if 'levgrnd' in data.dims : data = data.isel(levgrnd=9)     # deepest layer

      # if land_only or ocn_only :
      #    land_frac = case_obj.load_data('LANDFRAC',htype='h0',years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)
      #    land_frac = land_frac.isel(time=0)
      # if land_only: data = data*land_frac
      # if ocn_only : data = data*(1-land_frac)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      #reset time index to start at zero and convert to days
      dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
      print('      Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

      if find_max_val_col:
         avg_X = data#.isel(ncol=icol_mv)
      else:
         avg_X = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      
      time_mean = avg_X.mean(dim='time').values
      print('      Area Weighted Time Mean : '+hc.tcolor.GREEN+f'{time_mean:10.6f}'+hc.tcolor.ENDC)
      
      # Make time start at zero
      avg_X['time'] = ( avg_X['time'] - avg_X['time'][0] ).astype('float') / 86400e9

      ### add offset
      # if c==0:
      #    sd = np.std(avg_X.values)
      # else:
      #    avg_X = avg_X + sd*c

      data_list.append( avg_X.values )
      time_list.append( avg_X['time'].values )
      #-------------------------------------------------------------------------
      # write to file
      #-------------------------------------------------------------------------
      if write_file : 
         tfile = f'/global/homes/w/whannah/E3SM/scratch/{case[0]}/run/{case[0]}.time_series.{var[v]}.nc'
         print('writing to file: '+tfile)
         avg_X.name = var[v]
         avg_X.to_netcdf(path=tfile,mode='w')
         exit()


   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.tiXAxisString = 'Time [days]'

   
   ### Make sure plot bounds are consistent
   # tres.trYMinF = np.min(np.stack(data_list)) - np.std(np.stack(data_list))*0.5
   # tres.trYMaxF = np.max(np.stack(data_list)) + np.std(np.stack(data_list))*0.5
   # tres.trXMinF = np.min(np.stack(time_list))
   # tres.trXMaxF = np.max(np.stack(time_list))

   tres.trYMinF = np.min([np.nanmin(d) for d in data_list])
   tres.trYMaxF = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
   tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])

   if overlay_cases:
      if find_max_val_col:
         ip = v*num_mv_var + m
      else:
         ip = v

      # tres.xyLineColors = clr
      # tres.xyDashPatterns = dsh
      # Create plot with multiple curves

      ### use this for overlaying variables on same plot
      for c in range(num_case):
         tres.xyLineColor   = clr[c]
         tres.xyDashPattern = dsh[c]
         tplot = ngl.xy(wks, time_list[c], data_list[c], tres)
         if c==0: 
            plot[ip] = tplot
         else:
            ngl.overlay(plot[ip],tplot)
   else:
      for c in range(num_case):
         ip = c*num_var + v
         # ip = v*num_case + c
         tres.xyLineColor   = clr[c]
         tres.xyDashPattern = dsh[c]
         plot[ip] = ngl.xy(wks, time_list[c], data_list[c], tres)


   #----------------------------------------------------------------------------
   # Set strings at top of plot
   #----------------------------------------------------------------------------
   var_str = var[v]
   # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
   # if var[v]=="TMQ"   : var_str = "Column Water Vapor [mm]"

   lft_str = ''
   ctr_str = ''
   # if var[v] in ['PRECT','PRECC','PRECL'] : ctr_str = 'Mean: '+'%.2f'%avg_X+' [mm/day]'


   lat_chk,lon_chk = 'lat1' in locals(), 'lon1' in locals()


   if not lat_chk and not lon_chk and not find_max_val_col: 
      ctr_str = 'Global'
   else:
      if lat_chk:      ctr_str += f' {lat1}:{lat2}N '
      if lon_chk:      ctr_str += f' {lon1}:{lon2}E '
   if land_only: ctr_str += ' Land Only '
   if ocn_only : ctr_str += ' Ocean Only '


   if find_max_val_col: ctr_str = mv_str

   if overlay_cases:
      hs.set_subtitles(wks, plot[ip], lft_str, ctr_str, var_str, font_height=0.015)
   else:
      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name
      hs.set_subtitles(wks, plot[ip], lft_str, ctr_str, var_str, font_height=0.015)

   #-----------------------------------------------------------------------------
   # Ad legend
   #-----------------------------------------------------------------------------
   lgres = ngl.Resources()
   lgres.vpWidthF, lgres.vpHeightF  = 0.08, 0.08  
   lgres.lgLabelFontHeightF = 0.01
   lgres.lgLineThicknessF   = 4
   lgres.lgMonoLineColor    = False
   # lgres.lgMonoDashIndex    = True
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh
   lgres.lgLabelJust    = 'CenterLeft'
   # pid = ngl.legend_ndc(wks, len(name), name, 0.5, 0.4, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(name), name, 0.5, 0.1, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(name), name, 0.3, 0.5, lgres)  # 1x2

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
hc.printline()


if find_max_val_col and num_mv_var>1:
   layout = [num_var,num_mv_var]
else:
   if overlay_cases:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   else:
      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      else:
         layout = [num_var,num_case]
         # layout = [num_case,num_var]


# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [1,num_var]

if num_var==1 and num_case==4 : layout = [2,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------