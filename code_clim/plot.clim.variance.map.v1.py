import os, ngl, xarray as xr, numpy as np, copy
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean

name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,d=None,s=None,g=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = case_in.replace('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.','')
      tmp_name = tmp_name.replace('.',' ')
      tmp_name = tmp_name.replace('_','=')
      if tmp_name=='control': tmp_name = 'fixed-HV nu=1e15 hvss=1 hvsq=6'
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name); case_dir.append(d); case_sub.append(s); case_grid.append(g)

var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)


### Variance transport validation
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.00',     n='MMF')
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_0.00',n='MMF BVT')
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_1.00',n='MMF FVT1')

### hybrid coefficient transition tests
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm300.pgrad_0.00',n='pm300.pgrad_0')
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm300.pgrad_1.00',n='pm300.pgrad_1')
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm200.pgrad_0.00',n='pm200.pgrad_0')
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm200.pgrad_1.00',n='pm200.pgrad_1')
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm100.pgrad_0.00',n='pm100.pgrad_0')
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm100.pgrad_1.00',n='pm100.pgrad_1')
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm50.pgrad_0.00', n='pm50.pgrad_0')
# add_case('E3SM.HY-PM-TEST.F2010.ne30pg2_EC30to60E2r2.L72_pm50.pgrad_1.00', n='pm50.pgrad_1')

### vertical grid tests for new L72
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v0.00',n='E3SM L72v0')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1.00',n='E3SM L72v1')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v2.00',n='E3SM L72v2')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v3.00',n='E3SM L72v3')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v0.HF-OUTPUT.00',n='E3SM L72v0')
# add_case('E3SM.L72-NEW-TEST.F2010.ne30pg2_EC30to60E2r2.L72-v1.HF-OUTPUT.00',n='E3SM L72v1')

# add_var('PRECT')
# add_var('TS')
# add_var('TREFHT')
# add_var('QREFHT')
# add_var('RHREFHT')
# add_var('TGCLDLWP')
# add_var('TGCLDIWP')
add_var('SHFLX')
add_var('LHFLX')
# add_var('U10')
# add_var('VBOT')
# add_var('U',lev=-71)
# add_var('CLOUD',lev=-71)
# add_var('DYN_OMEGA',lev=500)

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.variance.map.v1'

lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = 0,60,50,120  # India
# lat1,lat2,lon1,lon2 = -45,-45+60,360-120,360-40  # S. America


htype,years,months,first_file,num_files = 'h1',[],[],365*5,365*5

use_remap,remap_grid = False,'90x180' # 90x180 / 180x360

recalculate = True

temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

diff_base = 0

# if 'lev' not in vars(): lev = np.array([0])
if 'lev' not in vars(): lev = np.array([0]*num_var)
# if len(lev) != num_var : lev = [lev]*num_var

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case)
res = hs.res_contour_fill_map()
if 'lat1' in locals(): res.mpMinLatF = lat1
if 'lat2' in locals(): res.mpMaxLatF = lat2
if 'lon1' in locals(): res.mpMinLonF = lon1
if 'lon2' in locals(): res.mpMaxLonF = lon2

### use this for aquaplanets
# res.mpOutlineBoundarySets = "NoBoundaries"
# res.mpCenterLonF = 0.

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012

if 'TRMM' in case :
   res.mpMinLatF = -50
   res.mpMaxLatF =  50

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list = []
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      case_obj.set_coord_names(var[v])
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tvar = var[v]

      # area_name = 'area'
      # if 'DYN_' in tvar : 
      #    if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
      #    if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
      #    if 'pg2' in case[c] or 'pg3' in case[c] : 
      #       case_obj.ncol_name = 'ncol_d'
      #       area_name = 'area_d'

      # if 'lat1' in vars() : case_obj.lat1 = lat1
      # if 'lat2' in vars() : case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1
      # if 'lon2' in vars() : case_obj.lon2 = lon2
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      # tmp_file = temp_dir+f'/clim.variance.v1.{case[c]}.{var[v]}'
      # if 'lat1' in locals(): tmp_file = tmp_file.replace('.nc',f'.lat1_{lat1}.lat2_{lat2}.nc')
      # print('    tmp_file: '+tmp_file+'')

      # if recalculate :
      if True:

         area = case_obj.load_data('area' ,htype=htype,years=years,num_files=num_files).astype(np.double)
         data = case_obj.load_data(tvar,   htype=htype,years=years,num_files=num_files,lev=lev)
         
         # if var[v] == 'CRM_PREC' : X = X[:,0,:15,:].mean(dim='crm_nx')*86400.*1e3

         hc.print_time_length(data.time)

         if 'lev' in data.dims : data = data.isel(lev=0)
         data = data.var(dim='time')
         
         if 'area' in vars() :
            gbl_mean = ( (data*area).sum() / area.sum() ).values 
            print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

      #    print('    writing to file: '+tmp_file)
      #    data.name = var[v]
      #    data.to_netcdf(path=tmp_file,mode='w')
      # else:
      #    ds = xr.open_dataset( tmp_file )
      #    data = ds[var[v]]

      data_list.append( data.values )

      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()
   
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      # ip = v*num_case+c
      ip = c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      # if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = "WhiteBlueGreenYellowRed"

      if var[v] in ['PRECT']        : tres.cnLevels = np.arange(10,1000+10,10)
      if var[v] in ['CRM_PREC']        : tres.cnLevels = np.arange(50,5000+50,50)
      if var[v]=="TGCLDLWP"         : tres.cnLevels = np.arange(0.005,0.1+0.005,0.005)
      
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         aboutZero = False
         clev_tup = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
                                         cint=None, max_steps=21,              \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = "AutomaticLevels"   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=21)
            tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      if var[v]=="PRECT"      : var_str = "Precipitation Variance"
      if var[v]=="TGCLDLWP"   : var_str = "LWP Variance"

      # different color options for difference plot
      if plot_diff and c!=diff_base :
         # tres.cnFillPalette = 'BlueWhiteOrangeRed'
         # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
         tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
         tres.cnLevelSelectionMode = "ExplicitLevels"
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if 'DYN_' in tvar : case_obj.grid = case_obj.grid.replace('pg2','')

      hs.set_cell_fill(tres,case_obj)
   
      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name

      # if case[c]=='TRMM' : 
      #    data = ngl.add_cyclic(data)
      # else:
      #    data = data.values
      
      if plot_diff and c!=diff_base: data_list[c] = data_list[c] - data_baseline.values

      plot[ip] = ngl.contour_map(wks,data_list[c],tres)

      ctr_str = ''
      if plot_diff: ctr_str = 'diff'
      # if var[v] in ['PRECT','PRECC','PRECL'] and 'gbl_mean' in vars() : 
      #    ctr_str = 'Mean: '+'%.2f'%gbl_mean+' [mm/day]'
      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)
      hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=0.015)

      
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
# layout = [num_var,num_case]
layout = [num_case,num_var]

# if num_var==1  : layout = [int(np.ceil(num_case/2.)),2]
if num_var==1  : layout = [num_case,num_var]
if num_case==1 : layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]
# if num_case==1 and num_var==4 : layout = [2,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------