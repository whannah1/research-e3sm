import os, ngl, xarray as xr, numpy as np, glob
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
np.seterr(divide='ignore', invalid='ignore'); np.errstate(divide='ignore', invalid="ignore")
#---------------------------------------------------------------------------------------------------
case,name,clr,dsh,mrk = [],[],[],[],[]
def add_case(case_in,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
#---------------------------------------------------------------------------------------------------
### INCITE 2022 low-cld Cess
# add_case('CERES-EBAF',n='CERES-EBAF',c='gray',d=1,ref=True); obs_remap_sub='clim_ne30pg2'
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_0K', n='MMF +0K', c='blue')
add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_4K', n='MMF +4K', c='red')

htype,first_file,num_files = 'h0',0,12*4

# fig_file = os.getenv("HOME")+"/Research/E3SM/figs_clim/clim.zonal-mean.v1"
fig_file = 'figs_clim/clim.zonal-mean.v1'

bin_dlat = 2

plot_diff = True

# print_rmse  = False
print_stats = True

chk_significance = False # use with plot_diff

num_plot_col = 1

add_legend = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wkres = ngl.Resources()
npix=2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks('png',fig_file,wkres)
plot = [None]

res = hs.res_xy()
res.vpHeightF = 0.3
res.xyLineThicknessF = 10

res.tiXAxisString = 'sin( Latitude )'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_SWCF = []
data_list_LWCF = []
data_list_NTCF = []
bin_list = []
glb_avg_list = []

for c in range(num_case):
   print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
   if case[c]!='CERES-EBAF':
      case_obj = he.Case( name=case[c] )
   #-------------------------------------------------------------------------
   # read the data
   #-------------------------------------------------------------------------
   lat  = case_obj.load_data('lat',  htype=htype,num_files=1)
   area = case_obj.load_data('area', htype=htype,num_files=1).astype(np.double)

   SWCF = case_obj.load_data('SWCF',htype=htype,first_file=first_file,num_files=num_files)
   LWCF = case_obj.load_data('LWCF',htype=htype,first_file=first_file,num_files=num_files)
   NTCF = SWCF + LWCF

   hc.print_time_length(SWCF.time,indent=(' '*6))

   if print_stats:
      hc.print_stat(SWCF,name='SWCF',stat='naxsh',indent=(' '*6),compact=True)
      hc.print_stat(LWCF,name='LWCF',stat='naxsh',indent=(' '*6),compact=True)
      hc.print_stat(NTCF,name='NTCF',stat='naxsh',indent=(' '*6),compact=True)
      if 'area' in locals() :
         gbl_mean_SWCF = ( (SWCF.mean(dim='time')*area).sum() / area.sum() ).values 
         gbl_mean_LWCF = ( (LWCF.mean(dim='time')*area).sum() / area.sum() ).values 
         gbl_mean_NTCF = ( (NTCF.mean(dim='time')*area).sum() / area.sum() ).values 
         # glb_avg_list.append(gbl_mean)
         print(f'      Area Weighted Global Mean SWCF : {hc.tcolor.CYAN}{gbl_mean_SWCF:6.4}{hc.tcolor.ENDC}')
         print(f'      Area Weighted Global Mean LWCF : {hc.tcolor.CYAN}{gbl_mean_LWCF:6.4}{hc.tcolor.ENDC}')
         print(f'      Area Weighted Global Mean NTCF : {hc.tcolor.CYAN}{gbl_mean_NTCF:6.4}{hc.tcolor.ENDC}')
      
   

   # if print_rmse:
   #    if c==0:
   #       baseline_SWCF = SWCF
   #       baseline_LWCF = LWCF
   #       baseline_NTCF = NTCF
   #    if c>0:
   #       RMSE_SWCF = np.sqrt( np.mean( np.square( SWCF.to_masked_array() - baseline_SWCF.to_masked_array() )))
   #       RMSE_LWCF = np.sqrt( np.mean( np.square( LWCF.to_masked_array() - baseline_LWCF.to_masked_array() )))
   #       RMSE_NTCF = np.sqrt( np.mean( np.square( NTCF.to_masked_array() - baseline_NTCF.to_masked_array() )))
   #       print(f'      Root Mean Square Error    : {RMSE_SWCF:6.4}')
   #       print(f'      Root Mean Square Error    : {RMSE_LWCF:6.4}')
   #       print(f'      Root Mean Square Error    : {RMSE_NTCF:6.4}')
   #       # exit()
   #-------------------------------------------------------------------------
   # Calculate time and zonal mean
   #-------------------------------------------------------------------------
   bin_ds_SWCF = hc.bin_YbyX( SWCF.mean(dim='time'), lat, bin_min=-90, bin_max=90, bin_spc=bin_dlat, wgt=area )
   bin_ds_LWCF = hc.bin_YbyX( LWCF.mean(dim='time'), lat, bin_min=-90, bin_max=90, bin_spc=bin_dlat, wgt=area )
   bin_ds_NTCF = hc.bin_YbyX( NTCF.mean(dim='time'), lat, bin_min=-90, bin_max=90, bin_spc=bin_dlat, wgt=area )

   data_list_SWCF.append( bin_ds_SWCF['bin_val'].values )
   data_list_LWCF.append( bin_ds_LWCF['bin_val'].values )
   data_list_NTCF.append( bin_ds_NTCF['bin_val'].values )

   lat_bins = bin_ds_SWCF['bins'].values
   sin_lat_bins = np.sin(lat_bins*np.pi/180.)
   bin_list.append(sin_lat_bins)

   if 'area' in locals(): del area

#----------------------------------------------------------------------------
# Take difference from first case
#----------------------------------------------------------------------------
if plot_diff:
   data_tmp_SWCF = data_list_SWCF
   data_tmp_LWCF = data_list_LWCF
   data_tmp_NTCF = data_list_NTCF
   data_baseline_SWCF = data_list_SWCF[0]
   data_baseline_LWCF = data_list_LWCF[0]
   data_baseline_NTCF = data_list_NTCF[0]
   for c in range(num_case): 
      data_list_SWCF[c] = data_list_SWCF[c] - data_baseline_SWCF
      data_list_LWCF[c] = data_list_LWCF[c] - data_baseline_LWCF
      data_list_NTCF[c] = data_list_NTCF[c] - data_baseline_NTCF
#----------------------------------------------------------------------------
# Create plot
#----------------------------------------------------------------------------
# unit_str = ''
# if pvar[v] in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# if pvar[v] in ['LHFLX','SHFLX']           : unit_str = '[W/m2]'
# res.tiYAxisString = unit_str

res.trXMinF = -1. #np.min( sin_lat_bins )
res.trXMaxF =  1. #np.max( sin_lat_bins )

min_SWCF = np.min([np.nanmin(d) for d in data_list_SWCF])
min_LWCF = np.min([np.nanmin(d) for d in data_list_LWCF])
min_NTCF = np.min([np.nanmin(d) for d in data_list_NTCF])
max_SWCF = np.max([np.nanmax(d) for d in data_list_SWCF])
max_LWCF = np.max([np.nanmax(d) for d in data_list_LWCF])
max_NTCF = np.max([np.nanmax(d) for d in data_list_NTCF])

# res.trYMinF = np.min([min_SWCF,min_LWCF,min_NTCF])
# res.trYMaxF = np.max([max_SWCF,max_LWCF,max_NTCF])

res.trYMinF = -10
res.trYMaxF =  10

lat_tick = np.array([-90,-60,-30,0,30,60,90])
res.tmXBMode = "Explicit"
res.tmXBValues = np.sin( lat_tick*3.14159/180. )
res.tmXBLabels = lat_tick

# plot[v] = ngl.xy(wks, np.stack(bin_list), np.ma.masked_invalid(  np.stack(data_list) ), res)

for c in range(1,num_case):
   res.xyDashPattern = 0
   res.xyLineColor='red'  ;tplot_SWCF=ngl.xy(wks,bin_list[c],np.ma.masked_invalid(data_list_SWCF[c]),res)
   res.xyLineColor='blue' ;tplot_LWCF=ngl.xy(wks,bin_list[c],np.ma.masked_invalid(data_list_LWCF[c]),res)
   res.xyLineColor='black';tplot_NTCF=ngl.xy(wks,bin_list[c],np.ma.masked_invalid(data_list_NTCF[c]),res)

   plot[0] = tplot_SWCF
   ngl.overlay( plot[0], tplot_LWCF )
   ngl.overlay( plot[0], tplot_NTCF )
   
   # add reference lines
   lres = hs.res_xy()
   lres.xyLineThicknessF = 1
   lres.xyLineColor = 'gray'
   xx,yy = [0,0],[-1e5,1e5]
   ngl.overlay( plot[0], ngl.xy(wks, xx, yy, lres) )
   ngl.overlay( plot[0], ngl.xy(wks, yy, xx, lres) )

   # if c==0:
   #    plot[v] = tplot
   # else:
   #    ngl.overlay( plot[v], tplot )

# for c in range(num_case):
#    res.xyLineColor   = clr[c]
#    res.xyDashPattern = dsh[c]
#    tplot = ngl.xy(wks, bin_list[c], np.ma.masked_invalid( data_list[c] ), res)
#    if c==0:
#       plot[v] = tplot
#    else:
#       ngl.overlay( plot[v], tplot )

# var_str = pvar[v]
# hs.set_subtitles(wks, plot[v], "", "", var_str, font_height=0.005)


#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pres = hs.setres_panel()
pres.nglFrame = False
pres.nglPanelRight = 0.5
ngl.panel(wks,plot,layout,pres)
#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1 and add_legend:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.03*num_case
   lgres.lgLabelFontHeightF = 0.008
   lgres.lgLabelFont        = "courier"
   lgres.lgMonoDashIndex    = False
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 16
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   labels = name
   # max_label_len = max([len(n) for n in name])+2
   # for n,nn in enumerate(name):
   #    labels[n] = f'  {nn:{max_label_len}}'
   #    if num_pvar==1: labels[n] += f'  {glb_avg_list[n]:6.1f}'

   # ndc_T,ndc_B,ndc_L,ndc_R = ngl.get_bounding_box(plot[0])
   # ndcx = ndc_R + 0.02
   # ndcy = np.average(np.array([ndc_T,ndc_B]))

   ndcx,ndcy = 0.5,0.65

   pid = ngl.legend_ndc(wks, len(labels), labels, ndcx, ndcy, lgres)

   # legend_id_list = [None]*num_pvar
   # for v in range(num_pvar):
   #    # ndcx, ndcy = ngl.datatondc(plot[v], 1., 0. )
   #    ndc_T,ndc_B,ndc_L,ndc_R = ngl.get_bounding_box(plot[v])
   #    ndcx = ndc_R + 0.02
   #    ndcy = np.average(np.array([ndc_T,ndc_B]))
   #    print(f'v: {v}   ndcx / ndcy: {ndcx} / {ndcy}')
   #    legend_id_list[v] = ngl.legend_ndc(wks, len(labels), labels, ndcx, ndcy, lgres)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
ngl.frame(wks)
ngl.end()

hc.trim_png(fig_file)
# print(f'\n{fig_file}.png\n')
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------