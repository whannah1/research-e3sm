#---------------------------------------------------------------------------------------------------
# Plot the zonal mean of the specified variables
#---------------------------------------------------------------------------------------------------
import os, ngl, xarray as xr, numpy as np, cmocean
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import copy, cftime, warnings
import cmocean
#---------------------------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=None,c=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name); case_dir.append(p); case_sub.append(s); case_grid.append(g)

var,lev_list = [],[]
def add_var(var_name,lev=-1): 
   if lev==-1: lev = np.array([0])
   var.append(var_name); lev_list.append(lev)
#---------------------------------------------------------------------------------------------------

### INCITE2021-CMT
# case.append(f'ERAi');name.append('ERAi')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.01',       n='MMF 2D')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.MOMFB.01', n='MMF 2D+MF')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.01',      n='MMF 3D')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.01',n='MMF 3D+MF')

# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.MOMFB.02', n='MMF 2D',p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch')
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='MMF 3D',p='/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch')

### Rotating RCE for Da Yang
# name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
# pscratch = '/pscratch/sd/w/whannah/e3sm_scratch/perlmutter'
# # add_case('E3SM.GNUCPU.ne4pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02.CORI',n='MMF MSE test')
# # add_case('E3SM.GNUCPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02.CORI',n='MMF MSE test 02')
# # add_case('E3SM.GNUCPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02a.CORI',n='MMF MSE test 02a')
# # add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02', n='MMF RCEROT',         p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# # add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.02', n='MMF RCEROT FIX-QRT', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# # add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRL.02', n='MMF RCEROT FIX-QRT', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# # add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRS.02', n='MMF RCEROT FIX-QRT', p='/global/cfs/cdirs/m1517/dyang/E3SM_MMF',s='data_native')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.02', n='MMF RCEROT',         p=pscratch,s='run')
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.02', n='MMF RCEROT FIX-QRT', p=pscratch,s='run')

# pscratch,psub = '/global/cfs/cdirs/m3312/whannah/2022-RCEROT/','run'
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.04', n='MMF RCEROT CTRL',   p=pscratch,s=psub)
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.04', n='MMF RCEROT HOM-RAD',p=pscratch,s=psub)
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.FIX_QRT.04', n='MMF RCEROT FIX-QRT',  p=pscratch,s=psub,c='magenta')

### INCITE-AQUA-RRM
# add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.CRMDX_2000.00',  n='MMF dx=2km'     )
# add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.DXSCL_12e-3.00', n='MMF dxscl=12e-3')
# add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.CRMDX_200.00',   n='MMF dx=200m'    )

### 2023 revisting CRM orientation
#add_case('E3SM.2023-ORIENT-00.GNUGPU.ne30pg2_oECv3.FAQP-MMF1.OA_90', n='MMF N/S')
#add_case('E3SM.2023-ORIENT-00.GNUGPU.ne30pg2_oECv3.FAQP-MMF1.OA_00', n='MMF E/W')

### 2023 coriolis test
# add_case('E3SM.2023-coriolis-test.GNUGPU.ne30pg2_oECv3.F2010-MMF1.coriolis-off',n='MMF 2D')
# add_case('E3SM.2023-coriolis-test.GNUGPU.ne30pg2_oECv3.F2010-MMF1.coriolis-on', n='MMF 2D w/ CRM coriolios')
# add_case('E3SM.2023-coriolis-test.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_32_1.coriolis-on',n='MMF 2D + non-trad cor')

#add_case('E3SM.2023-coriolis-test.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_32_32.coriolis-off',n='MMF 3D')
#add_case('E3SM.2023-coriolis-test.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_32_32.coriolis-on', n='MMF 3D w/ CRM coriolios')


### SciDAC MMF tests w/ new vertical grids
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L60',n='MMF L60')#,p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L64',n='MMF L64')#,p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L72',n='MMF L72')#,p=tmp_path,s=tmp_sub)

### Jim's L96 test for scidac
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_path,tmp_sub = '/global/cfs/cdirs/m4310/whannah/E3SM','archive/atm/hist'
# add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L72' ,n='v2 L72',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-v2-AMIP.ne30pg2_EC30to60E2r2.L80' ,n='v2 L80',p=tmp_path,s=tmp_sub)
# tmp_path,tmp_sub = '/global/cfs/cdirs/m4310/benedict/forWalter',''
# add_case('20240422.v3.F20TR.ne30pg2_r05_IcoswISC30E3r5.L96',  n='v3 L96',p=tmp_path,s=tmp_sub)

### reduced radiation sensitivity
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00', n='nx_rad=128')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_64.00',  n='nx_rad=64' )
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_32.00',  n='nx_rad=32' )
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_16.00',  n='nx_rad=16' )
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.00',   n='nx_rad=8'  )
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_4.00',   n='nx_rad=4'  )
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_2.00',   n='nx_rad=2'  )
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_1.00',   n='nx_rad=1'  )

### sorted radiation column test
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.00',           n='nx_rad=8')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.RAD_SORT.00',  n='nx_rad=8+SORT')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_8.GDT_120.00',   n='nx_rad=8+dt2m')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00',         n='nx_rad=128')
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.RAD_SORT.00',n='nx_rad=128+SORT')

### INCITE 2022 coupled runs
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO',                      n='E3SMv2'      )#,c='black')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',                       n='E3SM-MMF'    )#,c='red')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QI_3E-05',n='E3SM-MMF alt')#,c='blue')


### L80 vs L72 AMIP
# scrip_file_path = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'
# add_case('20230629.v3alpha02.amip.chrysalis.L72',n='E3SM L72',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')
# add_case('20230629.v3alpha02.amip.chrysalis.L80',n='E3SM L80',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')

### PAM dev tests
# tmp_scratch = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/'
# add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF1',      n='MMF1',p=tmp_scratch,s='run')
# add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF2',      n='MMF2 nx=45',p=tmp_scratch,s='run')
# add_case('E3SM.2024-PAM-CHK-05.ne30pg2_oECv3.F2010-MMF2.NX_65',n='MMF2 nx=65',p=tmp_scratch,s='run')
# add_case('E3SM.2024-PAM-CHK-05.ne30pg2_oECv3.F2010-MMF2.NX_75',n='MMF2 nx=75',p=tmp_scratch,s='run')


### 2024 nontraditional coriolis
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2024-coriolis.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.U-off.W-off'    ,n='MMF NCT ctrl',  p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.U-on.W-off'     ,n='MMF NCT U-only',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.U-on.W-off.S-on',n='MMF NCT U-only+ESMT',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.U-off.W-on'     ,n='MMF NCT W-only',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.U-on.W-on.S-off',n='MMF NCT',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.U-on.W-on.S-on' ,n='MMF NCT+ESMT',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-off.SU-off.SW-off' ,n='MMF control', p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-on.XW-on.SU-off.SW-off'   ,n='MMF XU+XW',   p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-off.SU-off.SW-on'  ,n='MMF SW',   p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-off.SU-on.SW-off'  ,n='MMF SU',   p=tmp_path,s=tmp_sub)

# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-off.SU-off.SW-off' ,n='MMF ',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-on.SU-off.SW-off'  ,n='MMF XW',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-on.XW-off.SU-off.SW-off'  ,n='MMF XU',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-on.XW-on.SU-off.SW-off'   ,n='MMF XU+XW',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-off.SU-on.SW-off'  ,n='MMF SU',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-off.SU-off.SW-on'  ,n='MMF SW',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-01.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.XU-off.XW-off.SU-on.SW-on'   ,n='MMF SU+SW',p=tmp_path,s=tmp_sub)


### SCREAMv1 decadal
# tmp_scratch = f'/lustre/orion/cli115/proj-shared/brhillman/e3sm_scratch'
# add_case('decadal-production-20240305.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.run1', p=tmp_scratch,s='run')
# htype=None; file_prefix = 'output.scream.decadal.monthlyAVG_ne30pg2.AVERAGE.nmonths_x1'
# first_file,num_files = 0,0


### 2024 nontraditional coriolis
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_sub = 'run'
# cpu_path = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# gpu_path = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
# add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-off.NCT-off'               ,n='EAM ',       p=cpu_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-off'                ,n='EAM NHS',    p=cpu_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-on'                 ,n='EAM NHS+NCT',p=cpu_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-off.NCT-off' ,n='MMF ',       p=gpu_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-off'  ,n='MMF NHS',    p=gpu_path,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-on'   ,n='MMF NHS+NCT',p=gpu_path,s=tmp_sub)


### 2024 RCEMIP phase 2
# scratch_cpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# scratch_gpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
# tsub = 'archive/atm/hist'
# add_case('E3SM.2024-RCEMIP2.FRCE_295',                 d=0,c='red',     p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE_300',                 d=0,c='green',   p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE_305',                 d=0,c='blue',    p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_295dT1p25',        d=0,c='orange',  p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT0p625',       d=0,c='cyan',    p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT1p25',        d=0,c='magenta', p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT2p5',         d=0,c='purple',  p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_305dT1p25',        d=0,c='pink',    p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_295',            d=1,c='red',     p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_300',            d=1,c='green',   p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MMF1_305',            d=1,c='blue',    p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_295dT1p25',   d=1,c='orange',  p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT0p625',  d=1,c='cyan',    p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT1p25',   d=1,c='magenta', p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT2p5',    d=1,c='purple',  p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_305dT1p25',   d=1,c='pink',    p=scratch_gpu,s=tsub)

# # interlaced RCEMIP cases for better plot organization
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_295dT1p25',        n='FRCE-MW_295dT1p25',      d=0,c='orange',  p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_295dT1p25',   n='FRCE-MW-MMF1_295dT1p25', d=1,c='orange',  p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT0p625',       n='FRCE-MW_300dT0p625',     d=0,c='cyan',    p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT0p625',  n='FRCE-MW-MMF1_300dT0p625',d=1,c='cyan',    p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT1p25',        n='FRCE-MW_300dT1p25',      d=0,c='magenta', p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT1p25',   n='FRCE-MW-MMF1_300dT1p25', d=1,c='magenta', p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_300dT2p5',         n='FRCE-MW_300dT2p5',       d=0,c='purple',  p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_300dT2p5',    n='FRCE-MW-MMF1_300dT2p5',  d=1,c='purple',  p=scratch_gpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW_305dT1p25',        n='FRCE-MW_305dT1p25',      d=0,c='pink',    p=scratch_cpu,s=tsub)
# add_case('E3SM.2024-RCEMIP2.FRCE-MW-MMF1_305dT1p25',   n='FRCE-MW-MMF1_305dT1p25', d=1,c='pink',    p=scratch_gpu,s=tsub)



# add_case('E3SM.2024-SCIDAC-heating-test-00.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
# add_case('E3SM.2024-SCIDAC-heating-test-01.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')
# add_case('E3SM.2024-SCIDAC-heating-test-02.F2010',n='E3SM',d=0,c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu',s='run')

#---------------------------------------------------------------------------------------------------

# lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,
#                550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
lev = np.array([1,2,4,6,10,30,50,100,150,200,300,400,500,600,700,800,850,925,950])
# lev = np.array([5,10,30,50,100,150,200,300,400,500,600,700,800,850,925,975,1000])


# add_var('U')
# add_var('V')
# add_var('omega')
# add_var('T_mid')
# add_var('qv')
# add_var('qc')
# add_var('RelativeHumidity')
# add_var('qi')
# add_var('qr')
# add_var('tke')
# add_var('nc')
# add_var('ni')
# add_var('nr')


# add_var('U')
# add_var('V')
# add_var('OMEGA')
# add_var('T')
# add_var('Q')
# add_var('CLDLIQ')
# add_var('CLDICE')
# add_var('CLOUD')
# add_var('QRS')
# add_var('QRL')

# add_var('U')
# add_var('V')
# add_var('OMEGA')



# lon1,lon2 = -10,10          # inside refined region
# lon1,lon2 = -10+180,10+180  # outside refined region

plot_diff = True

var_x_case = False

num_plot_col = 2

use_common_label_bar = True

htype,years,months,first_file,num_files = 'h1',[],[],0,2
# htype,years,months,first_file,num_files = 'h2',[],[],0,1
# htype,years,months,first_file,num_files = 'h3',[],[],500,4*50
# htype,years,months,first_file,num_files = 'h0',[],[],0,3


fig_file = os.getenv("HOME")+"/Research/E3SM/figs_clim/clim.zonal-mean.v2"

res='ne30'
if 'res' not in locals(): res = 'ne30'
if res=='ne4'  : lat1, lat2, dlat = -80., 80., 10
if res=='ne30' : lat1, lat2, dlat = -88., 88., 4
if res=='ne120': lat1, lat2, dlat = -88., 88., 2

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks('png',fig_file)
plot = [None]*(num_var*num_case)
res = hs.res_contour_fill()
res.vpHeightF = 0.3
res.trYReverse = True
res.tiXAxisString = 'Latitude'
# res.tiXAxisString = 'sin( Latitude )'
res.tiYAxisString = 'Pressure [hPa]'

res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.lbLabelFontHeightF     = 0.015


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_data_dir(c):
   global case_sub
   data_dir_tmp = None
   # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   return data_dir_tmp

def get_data_sub(c):
   global case_dir
   data_sub_tmp = None
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]
   return data_sub_tmp

def get_comp(case):
   comp = 'eam'
   if 'INCITE2019' in case: comp = 'cam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'MAML' in case: comp = 'eam_0001'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print()
   print(f'  var: {hc.tcolor.MAGENTA}{var[v]}{hc.tcolor.ENDC}')
   data_list = []
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), populate_files=True,
                          data_dir=get_data_dir(c), data_sub=get_data_sub(c)  )

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      lat  = case_obj.load_data('lat',  htype=htype)
      area = case_obj.load_data('area', htype=htype).astype(np.double)
      data = case_obj.load_data(var[v],htype=htype, years=years, months=months, first_file=first_file, num_files=num_files, lev=lev )
      # data = case_obj.load_data(var[v],htype=htype, years=years, months=months, first_file=first_file, num_files=num_files)

      hc.print_stat(data,compact=True)

      # if 'time' in data.dims : hc.print_time_length(data.time,indent=' '*6)

      if 'ilev' in data.dims : data = data.rename({'ilev':'lev'})

      #-------------------------------------------------------------------------
      # Calculate time and zonal mean
      #-------------------------------------------------------------------------

      # print()
      # print(data)
      # print()

      with warnings.catch_warnings():
         warnings.simplefilter("ignore", category=RuntimeWarning)

         bin_ds = hc.bin_YbyX( data.mean(dim='time', skipna=True), lat, \
                               bin_min=lat1, bin_max=lat2, \
                               bin_spc=dlat, wgt=area, keep_lev=True )

      lat_bins = bin_ds['bins'].values
      # sin_lat_bins = np.sin(lat_bins*np.pi/180.)

      # print()
      # print(bin_ds['bin_val'])
      # print()

      data_binned = np.ma.masked_invalid( bin_ds['bin_val'].transpose().values )

      # print()
      # print(data_binned)
      # print()
      # exit()

      # if plot_diff :
      #    if c==0:
      #       data_baseline = data_binned.copy()
      #    else:
      #       data_binned = data_binned - data_baseline
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      
      data_list.append( data_binned )

   #------------------------------------------------------------------------------------------------
   # Plot zonally averaged data
   #------------------------------------------------------------------------------------------------
   if plot_diff:
      # Find min and max difference values for setting color bar
      tmp_data = data_list - data_list[0]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[0]
      diff_data_min = np.min([np.min(d) for d in tmp_data])
      diff_data_max = np.max([np.max(d) for d in tmp_data])
      # diff_data_min = -2. * np.std(tmp_data)
      # diff_data_max =  2. * np.std(tmp_data)

      # print(f'diff_data_min: {diff_data_min}')
      # print(f'diff_data_max: {diff_data_max}')

   for c in range(num_case):
      ip = v*num_case+c if var_x_case else c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors, contour levels, and plot strings
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      
      tres.lbLabelBarOn = False if use_common_label_bar else True

      # if var[v] in ['OMEGA']  : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v]=="OMEGA"      : tres.cnLevels = np.linspace(-1,1,21)*0.1
      # if var[v] in ['Q','DYN_Q']: tres.cnLevels = np.linspace(1,16,16)
      if var[v] in ['MMF_DU','MMF_DV']: tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
      if var[v] in ['MMF_DU','MMF_DV']: tres.cnLevels = np.linspace(-2,2,11)

      if plot_diff and c>0 : 
         # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
         tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

      # if plot_diff and c>0 : 
      #    if var[v]=="U" : tres.cnLevels = np.linspace(-10,10,11)

      if hasattr(tres,'cnLevels') and not (plot_diff and c>0) : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         if (var[v] in ['U','V']) or (plot_diff and c>0) : 
            aboutZero = True
         else:
            aboutZero = False
         
         if plot_diff and c>0 : 
            aboutZero = True
            data_min = diff_data_min
            data_max = diff_data_max
         else:
            data_min = np.min([np.min(d) for d in data_list])
            data_max = np.max([np.max(d) for d in data_list])
         
         cmin,cmax,cint,clev = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=21, \
                                                    returnLevels=True,aboutZero=aboutZero )
         tres.cnLevels = np.linspace(cmin,cmax,num=21)
         tres.cnLevelSelectionMode = "ExplicitLevels"

      # if plot_diff and c>0 : 
         # if var[v]=="U" : tres.cnLevels = np.linspace(-10,10,11)

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------

      tres.sfXArray = lat_bins

      lat_tick = np.array([-90,-60,-30,0,30,60,90])
      res.tmXBMode = "Explicit"
      res.tmXBValues = lat_tick # np.sin( lat_tick*3.14159/180. )
      res.tmXBLabels = lat_tick

      # if 'lev' in vars() : tres.sfYCStartV = min( bin_ds['lev'].values )
      # if 'lev' in vars() : tres.sfYCEndV   = max( bin_ds['lev'].values )
      # tres.sfYCStartV = min( bin_ds['lev'].values )
      # tres.sfYCEndV   = max( bin_ds['lev'].values )
      tres.sfYArray = bin_ds['lev'].values

      if plot_diff and c >0 :
         # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
         tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
         plot[ip] = ngl.contour(wks, data_list[c] - data_list[0], tres)
      else:
         plot[ip] = ngl.contour(wks, data_list[c], tres)

      var_str = var[v]
      # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
      if var[v]=='MMF_DU':     var_str = 'CRM U Tendency'
      if plot_diff and c>0 : var_str = var_str+' (diff)'

      name_str = name[c]
      if plot_diff and c==0 : name0 = name_str
      # if plot_diff and c >0 : name_str = name_str+' - '+name0

      hs.set_subtitles(wks, plot[ip], name_str, '', var_str, font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

if num_case==1 or num_var==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [num_var,num_case] if var_x_case else [num_case,num_var]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
pnl_res.nglPanelXWhiteSpacePercent = 0

if use_common_label_bar: pnl_res.nglPanelLabelBar = True

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
