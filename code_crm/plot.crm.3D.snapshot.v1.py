import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import glob
home = os.getenv("HOME")
print()


case = ['INCITE2020.DEBUG.GNUGPU.ne4pg2_ne4pg2.F-MMFXX.NLEV_50.CRMNX_32.CRMNY_32.20201021.01']
name = ['MMF']

# svar = 'CRM_U'
# cvar = 'CRM_QC'

var = []
var.append('CRM_QV')
var.append('CRM_QC')
# var.append('CRM_W')


# lev = 850

fig_type = 'png'
fig_file = home+"/Research/E3SM/figs_crm/crm.3D.snapshot.v1"


# xlat, xlon = -15, 0#360-90

lat1,lat2,lon1,lon2 = 0,20,160,180
# lat1,lat2,lon1,lon2 = -15,-10.5,360-134,360-127
# lat1,lat2,lon1,lon2 = 18,22.5,169,174

crm_zlev = 10

htype,nfile = 'h2',-1

loc_tag = '' # '_98.5w_to_95.6w_35.3n_to_38.4n'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = len(var)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008

res.cnFillMode = 'RasterFill'

cres = hs.res_contour()

vres = hs.res_default()
# vres.vcFillArrowsOn           = False
# vres.vcMonoFillArrowFillColor = False
# vres.vcFillArrowEdgeColor     = 1
# vres.vcFillArrowWidthF        = 0.055
# vres.vcMinFracLengthF = 0.33
vres.vcMinDistanceF = 0.01
vres.vcRefMagnitudeF  = 5.0
vres.vcRefLengthF     = 0.01

# Factor to emphasize vertical motion vectors
w_vector_fac = 1.

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
v = 0
for c in range(num_case):
   print('    case: '+case[c])
   case_obj = he.Case( name=case[c] )
   for v in range(num_var):
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------

      # if len(glob.glob(case_obj.h3))>0 : ds = xr.open_mfdataset( case_obj.h3 )

      # filename = sorted( glob.glob( getattr(case_obj,htype) ) )[nfile]

      # ds = xr.open_mfdataset( getattr(case_obj,htype), combine='by_coords' )
      # ds = xr.open_dataset( filename )

      # mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )

      # lat = ds['lat']
      # lon = ds['lon']

      #-------------------------------------------------------------------------
      # Find the nearest column using great circle distances
      #-------------------------------------------------------------------------
      # cos_dist = np.sin(xlat*hc.deg_to_rad)*np.sin(lat*hc.deg_to_rad) + \
      #            np.cos(xlat*hc.deg_to_rad)*np.cos(lat*hc.deg_to_rad)*np.cos((lon-xlon)*hc.deg_to_rad)
      # cos_dist = np.where( cos_dist> 1.0,  1.0, cos_dist )
      # cos_dist = np.where( cos_dist<-1.0, -1.0, cos_dist )
      # dist = np.arccos( cos_dist )
      # icol = np.where( dist==np.min(dist) )[0][0]

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      lat = case_obj.load_data('lat',htype=htype,num_files=nfile)
      lon = case_obj.load_data('lon',htype=htype,num_files=nfile)

      ncol_idx = lon['ncol'] 
      ncol_srt = xr.DataArray( np.empty(len(ncol_idx)) )
      stride = 3
      # for i in [0,4,8,12]:
      for i in [0,3,6]:
         ncol_tmp = ncol_idx.sortby(lat,ascending=False)[i:i+stride]
         lon_tmp = lon.sortby(lat,ascending=False)[i:i+stride]
         ncol_srt[i:i+stride] = ncol_tmp.sortby( lon_tmp )
      # for n in range(len(lat['ncol'])) : print(f'  {ncol_idx[n].values}    {ncol_srt[n].values}    {(ncol_idx[n]-ncol_srt[n]).values}')
      # exit()

      # ncol = len(lat['ncol'])
      # for n in range(ncol) : print(f'  {lat[n].values}  {lon[n].values}  {lon2[n].values}')
      # exit()
      
      # Q = case_obj.load_data('CRM_QV',htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # T = case_obj.load_data('CRM_T' ,htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # C = case_obj.load_data('CRM_QC',htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # I = case_obj.load_data('CRM_QI',htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # U = case_obj.load_data('CRM_U' ,htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # W = case_obj.load_data('CRM_W' ,htype=htype,num_files=nfile).isel(time=0,crm_ny=0)

      # Convert water mixing ratios to g/kg
      # Q = Q*1e3
      # C = C*1e3
      # I = I*1e3

      # ncol = len(C['ncol'])

      

      # X = case_obj.load_data(var,htype=htype,num_files=nfile).isel(time=0,crm_ny=0).isel(crm_nz=slice(1,nlev))
      X = case_obj.load_data(var[v],htype=htype,num_files=nfile).isel(time=0,crm_nz=crm_zlev)

      hc.print_stat( X, name=var[v], indent='    ' )

      # Z = case_obj.load_data('Z3',htype=htype,num_files=nfile).isel(time=0).isel(lev=slice(72-nlev,72))
      # Z = Z.sortby('lev', ascending=False) /1e3


      # for k in range(58) : print(f'  {Z.isel(ncol=0,lev=k).values:8.2f}  {X.isel(ncol=0,crm_nx=0,crm_nz=k).values:6.2f}')
      # exit()

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      # W = ds['CRM_W' +loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol})#.max(dim='ncol'+loc_tag).max(dim='time')
      
      # t = 0
      # t = W.argmax(dim='time').values 

      # W = W.isel(time=t)
      # U = ds['CRM_U' +loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol,'time':t})
      # S = ds[svar+loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol,'time':t})
      # C = ds[cvar+loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol,'time':t})
      
      # # ignore upper levels
      # W = W.isel(crm_nz=slice(0,30))
      # U = U.isel(crm_nz=slice(0,30))
      # S = S.isel(crm_nz=slice(0,30))
      # C = C.isel(crm_nz=slice(0,30))

      # array.isel(x=ind.item())

      # print( np.argmax( W.values, axis= ) )
      # exit()

      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      # res.cnFillPalette = "BlueWhiteOrangeRed"
      # res.cnFillPalette = "WhiteBlueGreenYellowRed"
      # res.cnFillPalette = "CBR_wet"
      # res.cnFillPalette = "MPL_viridis"

      # if svar=="CRM_U"                 : res.cnLevels = np.linspace(-10,10,41)

      # if hasattr(res,'cnLevels')    : res.cnLevelSelectionMode = "ExplicitLevels"

      # res.cnLevelSelectionMode = "ExplicitLevels"

      # var_str = var
      # if pvar[v]=="PRECT" : var_str = "Precipitation [mm/day]"
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      # hs.set_cell_fill(case_obj,res)


      # plot.append( ngl.contour(wks,S.values,res) )
      # ngl.overlay( plot[len(plot)-1], ngl.contour(wks,C.values,cres) )
      # ngl.overlay( plot[len(plot)-1], ngl.vector(wks,U.values,W.values*w_vector_fac,vres) )

      # if 'name' in vars():
      #    case_name = name[c]
      # else:
      #    case_name = case_obj.short_name

      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, '', svar+' / '+cvar, font_height=0.01)

      # num_var = 0
      # ncol = 3

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(200,310,10)
      # for n in range(ncol) : plot.append( ngl.contour(wks,T.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(2,18+2,2)
      # for n in range(ncol) : plot.append( ngl.contour(wks,Q.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(0,2.1,0.1)
      # for n in range(ncol) : plot.append( ngl.contour(wks,C.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(0,0.02,0.001)
      # for n in range(ncol) : plot.append( ngl.contour(wks,I.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "ncl_default"   
      # res.cnLevels = np.linspace(-4,4,21)
      # for n in range(ncol) : plot.append( ngl.contour(wks,W.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "ncl_default"   
      # # res.cnLevels = np.linspace(-4,4,21)
      # for n in range(ncol) : plot.append( ngl.contour(wks,U.isel(ncol=n).values,res) )

      
      res.cnFillPalette = 'MPL_viridis'
      
      if var=='CRM_QV' : res.cnFillPalette,res.cnLevels = 'MPL_viridis',np.arange(4,12+1,1)
      # if var=='CRM_QC' : res.cnFillPalette,res.cnLevels = 'MPL_viridis',np.arange(0,2.1,0.1)
      # if var=='CRM_QC' : res.cnFillPalette,res.cnLevels = 'WhiteBlueGreenYellowRed',np.arange(0.05,2.05,0.05)
      # if var=='CRM_U'  : res.cnFillPalette,res.cnLevels = 'ncl_default',np.linspace(-4,4,21)

      # res.tmYLMode   = "Manual"
      # res.tmYLTickStartF   = 0
      # res.tmYLTickEndF     = 20
      # res.tmYLTickSpacingF = 1


      # for n in ncol_srt.values :

      num_col = len(X['ncol'].values)
      print(f'    num_col: {num_col}')
      
      for n in X['ncol'].values :
         if np.all(X.sel(ncol=n).values==0) : X.sel(ncol=n).values[0]=-0.001


         # hc.print_stat( X.sel(ncol=n), name=var[v], indent='    ' )

         # res.sfYArray = Z.sel(ncol=n).values
         # plot.append( ngl.contour(wks,X.sel(ncol=n).values,res) )

         plot.append( ngl.contour(wks,X.sel(ncol=n).values,res) )
         

         tlat = lat.sel(ncol=n).values
         tlon = lon.sel(ncol=n).values
         # hs.set_subtitles(wks, plot[len(plot)-1],'',f'{tlat:5.1f}N {tlon:5.1f}E','', font_height=0.01)
         hs.set_subtitles(wks, plot[len(plot)-1],'',f'{tlat:5.1f}N {tlon:5.1f}E',var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if len(ncol.values)==16 : layout = [4,4]
# if len(ncol.values)==9  : layout = [3,3]
layout = [num_var,num_col]
# layout = [1,num_case]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------