import os, ngl, copy, glob, xarray as xr, numpy as np, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
name,case = [],[]
def add_case(case_in,n=None,c=None):
   global name,case
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name)

var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------


add_case('E3SM.2023-PAM-ENS-02.ne30pg2_EC30to60E2r2.F2010-MMF2.CDT_10.DPP_04.SSC_01.HDT_0060.GDT_1200',n='MMF-PAM')


# add_var('CRM_U')
add_var('CRM_W')
add_var('CRM_T')
# add_var('CRM_QV')
# add_var('CRM_QC')
# add_var('CRM_QI')


# svar = 'CRM_U'  # shading
# cvar = 'CRM_QC' # contours


# lev = 850

ncol_idx = int(384/4)-40
snapshot_t = 2
crm_lev_top = 49 # top level (omit higher levels)
use_pert = False

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_crm/crm.snapshot.v1'

var_x_case = True
num_plot_col = 3

htype,nfile = 'h2',1

loc_tag = '' # '_98.5w_to_95.6w_35.3n_to_38.4n'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_case*num_var)
res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF         = 0.01
res.tmXBLabelFontHeightF         = 0.01
res.lbLabelFontHeightF           = 0.01

cres = hs.res_contour()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

print('\nIdentify location of max vertical velocity')
w_max_time_list = [None]*num_case
w_max_ncol_list = [None]*num_case
for c in range(num_case):
   hc.printline()
   print(' '*2+'case: '+case[c])
   #----------------------------------------------------------------------------
   # read the data
   file_path = f'/ccs/home/hannah6/E3SM/scratch2/{case[c]}/run/{case[c]}.eam.{htype}.0001-01-01-00000.nc'
   ds = xr.open_dataset(file_path).isel({'crm_ny':0})
   #----------------------------------------------------------------------------
   # Identify max vertical velocity
   w = ds['CRM_W']
   # w_argmax_dict = w.argmax(dim=w.dims)
   # w_max_time_list.append( w_argmax_dict['time'].values )
   # w_max_ncol_list.append( w_argmax_dict['ncol'].values )
   # print(f'  max time/ncol : {w_max_time_list[c]} / {w_max_ncol_list[c]}')

   # find Nth max value
   for n in range(1):
      if n>0: w[ w_max_time_list[c], :, :, w_max_ncol_list[c] ] = 0.
      wmax = w.max().values
      wmin = w.min().values
      if wmax>abs(wmin): 
         w_argmax_dict = w.argmax(dim=w.dims)
      else:
         wmax = wmin
         w_argmax_dict = w.argmin(dim=w.dims)
      w_max_time_list[c] = w_argmax_dict['time'].values
      w_max_ncol_list[c] = w_argmax_dict['ncol'].values
      # wmax = w.isel(time=w_max_time_list[c],ncol=w_max_ncol_list[c]).max().values
      print(f'  max time/ncol : {w_max_time_list[c]:2d} / {w_max_ncol_list[c]:8d}   wmax: {wmax:6.2f}')
#-------------------------------------------------------------------------------
print('\nLoading data and plot')
for v in range(num_var):
   print(' '*2+f'var: {hc.tcolor.MAGENTA}{var[v]}{hc.tcolor.ENDC}')
   data_list, zlev_list = [],[]
   for c in range(num_case):
      print(' '*4+f'case: {hc.tcolor.CYAN}{case[c]}{hc.tcolor.ENDC}')
      file_path = f'/ccs/home/hannah6/E3SM/scratch2/{case[c]}/run/{case[c]}.eam.{htype}.0001-01-01-00000.nc'
      ds = xr.open_dataset(file_path).isel({'crm_ny':0})
      data = ds[var[v]].isel(time=w_max_time_list[c],ncol=w_max_ncol_list[c])
      zlev = ds['Z3'  ].isel(time=w_max_time_list[c],ncol=w_max_ncol_list[c])
      gcm_nlev,crm_nlev = 60,50
      data = data.isel(crm_nz=slice(1,crm_nlev))
      zlev = zlev.isel(lev=slice(gcm_nlev-(crm_nlev),gcm_nlev)).sortby('lev', ascending=False)
      data_list.append( data.values )
      zlev_list.append( zlev.values/1e3 )
   #----------------------------------------------------------------------------
   # Create plot
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   tres = copy.deepcopy(res)
   nlev = 21
   aboutZero = False
   if var[v] in ['CRM_U','CRM_W']: aboutZero = True
   cmin,cmax,cint = ngl.nice_cntr_levels(data_min, data_max, max_steps=nlev, aboutZero=aboutZero )
   tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
   tres.cnLevelSelectionMode = 'ExplicitLevels'

   if aboutZero:
      tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
   else:
      tres.cnFillPalette = 'MPL_viridis'
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
   
   for c in range(num_case):
      ip = v*num_case+c if var_x_case else c*num_var+v
      tres.sfYArray = zlev_list[c]
      plot[ip] = ngl.contour( wks, data_list[c], tres ) 
      hs.set_subtitles(wks, plot[ip],name[c],'',var[v], font_height=0.015)
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
if var_x_case:
   layout = [num_var,num_case]
else:
   layout = [num_case,num_var]

if (num_case==1 or num_var==1): layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------