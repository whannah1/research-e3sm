import os
import ngl
import xarray as xr
import xrft
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import glob
home = os.getenv("HOME")
print()


case,name = ['E3SM_TEST-CRM-WIND_GPU_ne30pg2_FSP1V1_00'],['MMF']

# var = 'CLDLIQ'
# var = 'SPQTLS'
var = 'V'

calculate_anomalies = False

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_crm/crm.snapshot.profile.v1"


# lat1,lat2,lon1,lon2 = -15,-10.5,360-134,360-127
lat1,lat2,lon1,lon2 = 18,22.5,169,174

htype,nfile = 'h1',-1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
# num_pvar = len(pvar)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
# res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF         = 0.012
res.tmXBLabelFontHeightF         = 0.012

lres = hs.res_xy()
lres.xyLineThicknessF = 1
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
v = 0
for c in range(num_case):
   print('    case: '+case[c])
   case_obj = he.Case( name=case[c] )
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   if 'lat1' in vars() : case_obj.lat1 = lat1
   if 'lat2' in vars() : case_obj.lat2 = lat2
   if 'lon1' in vars() : case_obj.lon1 = lon1
   if 'lon2' in vars() : case_obj.lon2 = lon2

   lat = case_obj.load_data('lat',htype=htype,num_files=nfile)
   lon = case_obj.load_data('lon',htype=htype,num_files=nfile)

   ncol_idx = lon['ncol'] 
   ncol_srt = xr.DataArray( np.empty(len(ncol_idx)) )
   stride = 3
   # for i in [0,4,8,12]:
   for i in [0,3,6]:
      ncol_tmp = ncol_idx.sortby(lat,ascending=False)[i:i+stride]
      lon_tmp = lon.sortby(lat,ascending=False)[i:i+stride]
      ncol_srt[i:i+stride] = ncol_tmp.sortby( lon_tmp )


   # for n in range(len(lat['ncol'])) : print(f'  {ncol_idx[n].values}    {ncol_srt[n].values}    {(ncol_idx[n]-ncol_srt[n]).values}')
   # exit()


   Z = case_obj.load_data('Z3',htype=htype,num_files=nfile).isel(time=0)
   Z = Z/1e3
   Z['units'] = 'km'
   # Z = Z.sortby('lev', ascending=False) /1e3

   if var=='WSPEED':
      U = case_obj.load_data('U',htype=htype,num_files=nfile).isel(time=0)
      V = case_obj.load_data('V',htype=htype,num_files=nfile).isel(time=0)
      data = np.sqrt(U**2+V**2)
      print(data)
   else:
      data = case_obj.load_data(var,htype=htype,num_files=nfile).isel(time=0)

   # hc.print_stat(data)
   # exit()

   nlev = 30
   if 'lev' in data.dims : 
      data = data.isel(lev=slice(72-nlev,72-1))
      Z    =    Z.isel(lev=slice(72-nlev,72-1))

   if 'crm_ny' in data.dims : data = data.isel(crm_ny=0)
   if 'crm_nz' in data.dims : data = data.isel(crm_nz=slice(1,nlev))
   
   # if var in ['Q'] : data = data*1e3
   if var in ['CRM_QV','CRM_QC','CRM_QI'] : data = data*1e3
   if var in ['SPDQ','SPQTLS'] : data = data*1e3*3600

   ncol = data['ncol']

   if calculate_anomalies : data = data - data.mean(dim='ncol')
         
   # data = data.isel(crm_nz=5)
   # data_fft = xrft.power_spectrum(data,dim=['crm_nx']).compute()
   # data_fft = data_fft.isel(freq_crm_nx=0)
   # data = data_fft

   # xx = data['freq_crm_nx'].values
   # for x in range(len(xx)) : print(f'{x}  {xx[x]:8.4f}  {data[x,0].values}')
   # exit()

   # for k in range(58) : print(f'  {Z.isel(ncol=0,lev=k).values:8.2f}  {data.isel(ncol=0,crm_nx=0,crm_nz=k).values:6.2f}')
   # exit()

   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------

   res.trXMinF = np.min(data.values)
   res.trXMaxF = np.max(data.values)

   for n in ncol_srt.values : 
      xx = data.sel(ncol=n)
      zz = Z.sel(ncol=n)

      # res.xyLineColor = 'black'
      # plot.append( ngl.xy(wks,xx.values,zz.values,res) )

      # res.nglXYLeftFillColors  = 'red'
      # res.nglXYRightFillColors = 'blue'

      res.nglXYLeftFillColors  = 'blue'
      res.nglXYRightFillColors = 'red'

      xx_stack = np.stack([xx.values,xx.values*0])
      plot.append( ngl.xy(wks,xx_stack,zz.values,res) )

      # for k in range(1,nlev-1):
      #    ngl.overlay( plot[len(plot)-1], ngl.xy(wks,x_coord,data.sel(ncol=n,crm_nz=k).values[33:],res) )

      
      # xx1 = xx.where(xx>0,drop=True)
      # zz1 = zz.where(xx>0,drop=True)
      # xx2 = xx.where(xx<0,drop=True)
      # zz2 = zz.where(xx<0,drop=True)
      # res.xyLineColor = 'red'
      # if len(xx1)>1 : ngl.overlay( plot[len(plot)-1], ngl.xy(wks,xx1.values,zz1.values,res) )
      # res.xyLineColor = 'blue'
      # if len(xx2)>1 : ngl.overlay( plot[len(plot)-1], ngl.xy(wks,xx2.values,zz2.values,res) )

      ngl.overlay( plot[len(plot)-1], ngl.xy(wks,zz.values*0,zz.values,lres) )

      tlat = lat.sel(ncol=n).values
      tlon = lon.sel(ncol=n).values
      hs.set_subtitles(wks, plot[len(plot)-1],'',f'{tlat:5.1f}N {tlon:5.1f}E','', font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
textres               =  ngl.Resources()
textres.txFontHeightF =  0.025
ngl.text_ndc(wks,var,0.5,.9,textres)

if len(ncol.values)==16 : layout = [4,4]
if len(ncol.values)==9  : layout = [3,3]
# layout = [num_var,ncol]
# layout = [1,num_case]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------