import os
import ngl
import xarray as xr
import xrft
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import glob
home = os.getenv("HOME")
print()


case,name = ['E3SM_TEST-CRM-WIND_GPU_ne30pg2_FSP1V1_00'],['MMF']

var = 'CRM_QV'

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_crm/crm.snapshot.spectrum.v1"


lat1,lat2,lon1,lon2 = -15,-10.5,360-134,360-127

htype,nfile = 'h1',-1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
# num_pvar = len(pvar)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
# res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008

cres = hs.res_contour()

vres = hs.res_default()
# vres.vcFillArrowsOn           = False
# vres.vcMonoFillArrowFillColor = False
# vres.vcFillArrowEdgeColor     = 1
# vres.vcFillArrowWidthF        = 0.055
# vres.vcMinFracLengthF = 0.33
vres.vcMinDistanceF = 0.01
vres.vcRefMagnitudeF  = 5.0
vres.vcRefLengthF     = 0.01

# Factor to emphasize vertical motion vectors
w_vector_fac = 1.

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
v = 0
for c in range(num_case):
   print('    case: '+case[c])
   case_obj = he.Case( name=case[c] )
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   if 'lat1' in vars() : case_obj.lat1 = lat1
   if 'lat2' in vars() : case_obj.lat2 = lat2
   if 'lon1' in vars() : case_obj.lon1 = lon1
   if 'lon2' in vars() : case_obj.lon2 = lon2

   lat = case_obj.load_data('lat',htype=htype,num_files=nfile)
   lon = case_obj.load_data('lon',htype=htype,num_files=nfile)

   ncol_idx = lon['ncol'] 
   ncol_srt = xr.DataArray( np.empty(len(ncol_idx)) )
   for i in [0,4,8,12]:
      ncol_tmp = ncol_idx.sortby(lat,ascending=False)[i:i+4]
      lon_tmp = lon.sortby(lat)[i:i+4]
      ncol_srt[i:i+4] = ncol_tmp.sortby( lon_tmp )


   # for n in range(len(lat['ncol'])) : print(f'  {ncol_idx[n].values}    {ncol_srt[n].values}    {(ncol_idx[n]-ncol_srt[n]).values}')
   # exit()


   nlev = 10

   # Z = case_obj.load_data('Z3',htype=htype,num_files=nfile).isel(time=0).isel(lev=slice(72-nlev,72))
   # Z = Z.sortby('lev', ascending=False) /1e3

   data = case_obj.load_data(var,htype=htype,num_files=nfile).isel(time=0,crm_ny=0).isel(crm_nz=slice(0,nlev))
   if var in ['CRM_QV','CRM_QC','CRM_QI'] : data = data*1e3
   ncol = data['ncol']
         
   # data = data.isel(crm_nz=5)
   data_fft = xrft.power_spectrum(data,dim=['crm_nx']).compute()
   # data_fft = data_fft.isel(freq_crm_nx=0)
   data = data_fft

   # xx = data['freq_crm_nx'].values
   # for x in range(len(xx)) : print(f'{x}  {xx[x]:8.4f}  {data[x,0].values}')
   # exit()

   # for k in range(58) : print(f'  {Z.isel(ncol=0,lev=k).values:8.2f}  {data.isel(ncol=0,crm_nx=0,crm_nz=k).values:6.2f}')
   # exit()

   #-------------------------------------------------------------------------
   # Set colors and contour levels
   #-------------------------------------------------------------------------
   # res.cnFillPalette = "BlueWhiteOrangeRed"
   # res.cnFillPalette = "WhiteBlueGreenYellowRed"
   # res.cnFillPalette = "CBR_wet"
   # res.cnFillPalette = "MPL_viridis"

   # if svar=="CRM_U"                 : res.cnLevels = np.linspace(-10,10,41)

   # if hasattr(res,'cnLevels')    : res.cnLevelSelectionMode = "ExplicitLevels"

   # res.cnLevelSelectionMode = "ExplicitLevels"

   # var_str = var
   # if pvar[v]=="PRECT" : var_str = "Precipitation [mm/day]"
   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------
   
   # if var=='CRM_QV' : res.cnFillPalette,res.cnLevels = 'MPL_viridis',np.arange(1,18+1,1)
   # if var=='CRM_QC' : res.cnFillPalette,res.cnLevels = 'MPL_viridis',np.arange(0,2.1,0.1)
   # if var=='CRM_QC' : res.cnFillPalette,res.cnLevels = 'WhiteBlueGreenYellowRed',np.arange(0.05,2.05,0.05)
   # if var=='CRM_U'  : res.cnFillPalette,res.cnLevels = 'ncl_default',np.linspace(-4,4,21)

   # res.tmYLMode   = "Manual"
   # res.tmYLTickStartF   = 0
   # res.tmYLTickEndF     = 20
   # res.tmYLTickSpacingF = 1


   res.trYMaxF = np.max(data.isel(freq_crm_nx=slice(33,63)).values)
   # res.trYMinF = 1e-2
   # res.xyYStyle = "Log"

   x_coord = data['freq_crm_nx'].values[33:]
   res.trXMinF = np.min(x_coord)
   res.trXMaxF = np.max(x_coord)
   res.xyXStyle = "Log"

   

   for n in ncol_srt.values : 
      # if np.all(data.sel(ncol=n).values==0) : data.sel(ncol=n).values[0]=-0.001
      # res.sfYArray = Z.sel(ncol=n).values
      # plot.append( ngl.contour(wks,data.sel(ncol=n).values,res) )
      
      # print(data.sel(ncol=n))
      
      plot.append( ngl.xy(wks,x_coord,data.sel(ncol=n,crm_nz=0).values[33:],res) )
      for k in range(1,nlev-1):
         ngl.overlay( plot[len(plot)-1], ngl.xy(wks,x_coord,data.sel(ncol=n,crm_nz=k).values[33:],res) )

      tlat = lat.sel(ncol=n).values
      tlon = lon.sel(ncol=n).values
      hs.set_subtitles(wks, plot[len(plot)-1],'',f'{tlat:5.1f}N {tlon:5.1f}E','', font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
if len(ncol.values)==16 : layout = [4,4]
# layout = [num_var,ncol]
# layout = [1,num_case]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------