import os, ngl, copy, glob, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
name,case = [],[]
def add_case(case_in,n=None,c=None):
   global name,case
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name)

var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# add_case('E3SM.INCITE2021-CRMHV.GNUCPU.ne4_ne4.F-MMFXX-SCM-ARM97.NXY_32x1.CRMDX_2000.CRMDT_10.NXRAD_4.00'      ,n='MMF')
# add_case('E3SM.INCITE2021-CRMHV.GNUCPU.ne4_ne4.F-MMFXX-SCM-ARM97.NXY_32x1.CRMDX_2000.CRMDT_10.NXRAD_4.CRMHV.00',n='MMF+CRMHV')

# add_case('E3SM.INCITE2021-CRMHV.GNUCPU.ne4_ne4.F-MMFXX-SCM-ARM97.NXY_256x1.CRMDX_200.CRMDT_10.NXRAD_4.00'      ,n='MMF')
# add_case('E3SM.INCITE2021-CRMHV.GNUCPU.ne4_ne4.F-MMFXX-SCM-ARM97.NXY_256x1.CRMDX_200.CRMDT_10.NXRAD_4.CRMHV.00',n='MMF+CRMHV')

# add_case('E3SM.INCITE2021-CRMHV.GNUCPU.ne4_ne4.F-MMFXX-SCM-ARM97.NXY_32x32.CRMDX_1000.CRMDT_10.NXRAD_4.00'      ,n='MMF')
# add_case('E3SM.INCITE2021-CRMHV.GNUCPU.ne4_ne4.F-MMFXX-SCM-ARM97.NXY_32x32.CRMDX_1000.CRMDT_10.NXRAD_4.CRMHV.00',n='MMF+CRMHV')

### PAM dev tests
# add_case('E3SM.PAM-DEV-10.GNUGPU.F2010-MMF-SAM.ne4pg2_ne4pg2'         ,n='SAM')
# add_case('E3SM.PAM-DEV-15.GNUGPU.F2010-MMF-PAM-C.ne4pg2_ne4pg2.F-QALL',n='PAM')

add_case('E3SM.2023-PAM-ENS-02.ne30pg2_EC30to60E2r2.F2010-MMF2.CDT_10.DPP_04.SSC_01.HDT_0060.GDT_1200',n='PAM')



# add_var('CRM_T')
# add_var('CRM_QV')
# add_var('CRM_QC')
# add_var('CRM_QI')
add_var('CRM_U')
add_var('CRM_W')


# svar = 'CRM_U'  # shading
# cvar = 'CRM_QC' # contours


# lev = 850

ncol_idx = int(384/4)-40
snapshot_t = 2
crm_lev_top = 49 # top level (omit higher levels)
use_pert = False

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_crm/crm.snapshot.v1'

var_x_case = True

# xlat, xlon = -15, 0#360-90

# lat1,lat2,lon1,lon2 = -15,-10.5,360-134,360-127
# lat1,lat2,lon1,lon2 = 18,22.5,169,174
# clat,clon = 80,80 ; dx=10 ; lat1,lat2,lon1,lon2 = clat-dx,clat+dx,clon-dx,clon+dx

htype,nfile = 'h2',1

loc_tag = '' # '_98.5w_to_95.6w_35.3n_to_38.4n'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_case*num_var)
res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008

res.tmYLMode   = "Manual"
res.tmYLTickStartF   = 0
res.tmYLTickEndF     = 30
res.tmYLTickSpacingF = 1

cres = hs.res_contour()

vres = hs.res_default()
# vres.vcFillArrowsOn           = False
# vres.vcMonoFillArrowFillColor = False
# vres.vcFillArrowEdgeColor     = 1
# vres.vcFillArrowWidthF        = 0.055
# vres.vcMinFracLengthF = 0.33
vres.vcMinDistanceF = 0.01
vres.vcRefMagnitudeF  = 5.0
vres.vcRefLengthF     = 0.01

# Factor to emphasize vertical motion vectors
w_vector_fac = 1.

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   X_list, Z_list = [],[]
   lat_list, lon_list = [],[]
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------

      # if len(glob.glob(case_obj.h3))>0 : ds = xr.open_mfdataset( case_obj.h3 )

      # filename = sorted( glob.glob( getattr(case_obj,htype) ) )[nfile]

      # ds = xr.open_mfdataset( getattr(case_obj,htype), combine='by_coords' )
      # ds = xr.open_dataset( filename )

      # mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )

      # lat = ds['lat']
      # lon = ds['lon']

      #-------------------------------------------------------------------------
      # Find the nearest column using great circle distances
      #-------------------------------------------------------------------------
      # cos_dist = np.sin(xlat*hc.deg_to_rad)*np.sin(lat*hc.deg_to_rad) + \
      #            np.cos(xlat*hc.deg_to_rad)*np.cos(lat*hc.deg_to_rad)*np.cos((lon-xlon)*hc.deg_to_rad)
      # cos_dist = np.where( cos_dist> 1.0,  1.0, cos_dist )
      # cos_dist = np.where( cos_dist<-1.0, -1.0, cos_dist )
      # dist = np.arccos( cos_dist )
      # icol = np.where( dist==np.min(dist) )[0][0]

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      lat = case_obj.load_data('lat',htype=htype,num_files=nfile)
      lon = case_obj.load_data('lon',htype=htype,num_files=nfile)

      #-------------------------------------------------------------------------
      # create sorted column list for matching plot arrangement to global grid
      #-------------------------------------------------------------------------
      # ncol_idx = lon['ncol'] 
      # ncol_srt = xr.DataArray( np.empty(len(ncol_idx)) )
      # stride = 3
      # # for i in [0,4,8,12]:
      # for i in [0,3,6]:
      #    ncol_tmp = ncol_idx.sortby(lat,ascending=False)[i:i+stride]
      #    lon_tmp = lon.sortby(lat,ascending=False)[i:i+stride]
      #    ncol_srt[i:i+stride] = ncol_tmp.sortby( lon_tmp )
      # for n in range(len(lat['ncol'])) : print(f'  {ncol_idx[n].values}    {ncol_srt[n].values}    {(ncol_idx[n]-ncol_srt[n]).values}')
      # exit()

      # ncol = len(lat['ncol'])
      # for n in range(ncol) : print(f'  {lat[n].values}  {lon[n].values}  {lon2[n].values}')
      # exit()
      
      # Q = case_obj.load_data('CRM_QV',htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # T = case_obj.load_data('CRM_T' ,htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # C = case_obj.load_data('CRM_QC',htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # I = case_obj.load_data('CRM_QI',htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # U = case_obj.load_data('CRM_U' ,htype=htype,num_files=nfile).isel(time=0,crm_ny=0)
      # W = case_obj.load_data('CRM_W' ,htype=htype,num_files=nfile).isel(time=0,crm_ny=0)

      # Convert water mixing ratios to g/kg
      # Q = Q*1e3
      # C = C*1e3
      # I = I*1e3

      # ncol = len(C['ncol'])
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      X = case_obj.load_data(var[v],htype=htype,num_files=nfile).isel(time=snapshot_t,crm_ny=0)
      Z = case_obj.load_data('Z3',  htype=htype,num_files=nfile).isel(time=snapshot_t)

      # print(); print(X); print()

      # X = X.isel(crm_nz=slice(1,crm_lev_top))
      # Z = Z.isel(lev=slice(72-crm_lev_top,72))


      Z = Z.sortby('lev', ascending=False) /1e3

      if var in ['CRM_QV','CRM_QC','CRM_QI'] : X = X*1e3
      # ncol = len(X['ncol'])
      ncol = X['ncol']

      # exit(X)

      # for k in range(58) : print(f'  {Z.isel(ncol=0,lev=k).values:8.2f}  {X.isel(ncol=0,crm_nx=0,crm_nz=k).values:6.2f}')
      # exit()

      if use_pert: 
         X.load()
         for n in range(len(ncol)):
            X[:,:,n] = X[:,:,n] - X[:,:,n].mean(dim='crm_nx')
         

      # print(hc.tcolor.RED+'WARNING: only using a single column!'+hc.tcolor.ENDC)
      X = X.isel(ncol=ncol_idx)
      Z = Z.isel(ncol=ncol_idx)
      tlat = lat.isel(ncol=ncol_idx).values
      tlon = lon.isel(ncol=ncol_idx).values

      X_list.append( X.values )
      Z_list.append( Z.values )

      lat_list.append( tlat )
      lon_list.append( tlon )

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      # W = ds['CRM_W' +loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol})#.max(dim='ncol'+loc_tag).max(dim='time')
      
      # t = 0
      # t = W.argmax(dim='time').values 

      # W = W.isel(time=t)
      # U = ds['CRM_U' +loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol,'time':t})
      # S = ds[svar+loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol,'time':t})
      # C = ds[cvar+loc_tag].isel({'crm_ny':0,'ncol'+loc_tag:icol,'time':t})
      
      # # ignore upper levels
      # W = W.isel(crm_nz=slice(0,30))
      # U = U.isel(crm_nz=slice(0,30))
      # S = S.isel(crm_nz=slice(0,30))
      # C = C.isel(crm_nz=slice(0,30))

      # array.isel(x=ind.item())

      # print( np.argmax( W.values, axis= ) )
      # exit()

      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      # res.cnFillPalette = "BlueWhiteOrangeRed"
      # res.cnFillPalette = "WhiteBlueGreenYellowRed"
      # res.cnFillPalette = "CBR_wet"
      # res.cnFillPalette = "MPL_viridis"

      # if svar=="CRM_U"                 : res.cnLevels = np.linspace(-10,10,41)

      # if hasattr(res,'cnLevels')    : res.cnLevelSelectionMode = "ExplicitLevels"

      res.cnLevelSelectionMode = "ExplicitLevels"

      # var_str = var
      # if pvar[v]=="PRECT" : var_str = "Precipitation [mm/day]"
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      # hs.set_cell_fill(case_obj,res)


      # plot.append( ngl.contour(wks,S.values,res) )
      # ngl.overlay( plot[len(plot)-1], ngl.contour(wks,C.values,cres) )
      # ngl.overlay( plot[len(plot)-1], ngl.vector(wks,U.values,W.values*w_vector_fac,vres) )

      # if 'name' in vars():
      #    case_name = name[c]
      # else:
      #    case_name = case_obj.short_name

      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, '', svar+' / '+cvar, font_height=0.01)

      # num_var = 0
      # ncol = 3

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(200,310,10)
      # for n in range(ncol) : plot.append( ngl.contour(wks,T.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(2,18+2,2)
      # for n in range(ncol) : plot.append( ngl.contour(wks,Q.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(0,2.1,0.1)
      # for n in range(ncol) : plot.append( ngl.contour(wks,C.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "MPL_viridis"
      # res.cnLevels = np.arange(0,0.02,0.001)
      # for n in range(ncol) : plot.append( ngl.contour(wks,I.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "ncl_default"   
      # res.cnLevels = np.linspace(-4,4,21)
      # for n in range(ncol) : plot.append( ngl.contour(wks,W.isel(ncol=n).values,res) )

      # num_var = num_var+1
      # res.cnFillPalette = "ncl_default"   
      # # res.cnLevels = np.linspace(-4,4,21)
      # for n in range(ncol) : plot.append( ngl.contour(wks,U.isel(ncol=n).values,res) )

      
      
      # if var=='CRM_QV' : res.cnFillPalette,res.cnLevels = 'MPL_viridis',np.arange(1,18+1,1)
      # # if var=='CRM_QC' : res.cnFillPalette,res.cnLevels = 'MPL_viridis',np.arange(0,2.1,0.1)
      # if var=='CRM_QC' : res.cnFillPalette,res.cnLevels = 'WhiteBlueGreenYellowRed',np.arange(0.05,2.05,0.05)
      # if var=='CRM_U'  : res.cnFillPalette,res.cnLevels = 'ncl_default',np.linspace(-4,4,21)

      #-------------------------------------------------------------------------
      # Create plot - use this version for sorted column index
      #-------------------------------------------------------------------------
      # for n in ncol_srt.values : 
      #    if np.all(X.sel(ncol=n).values==0) : X.sel(ncol=n).values[0]=-0.001
      #    res.sfYArray = Z.sel(ncol=n).values
      #    plot.append( ngl.contour(wks,X.sel(ncol=n).values,res) )
      #    tlat = lat.sel(ncol=n).values
      #    tlon = lon.sel(ncol=n).values
      #    hs.set_subtitles(wks, plot[len(plot)-1],'',f'{tlat:5.1f}N {tlon:5.1f}E','', font_height=0.01)


      
   #-------------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in X_list])
   data_max = np.max([np.nanmax(d) for d in X_list])

   for c in range(num_case):

      tres = copy.deepcopy(res)

      nlev = 21
      aboutZero = False
      if var[v] in ['CRM_U','CRM_W']: aboutZero = True
      clev_tup = ngl.nice_cntr_levels(data_min, data_max,  \
                  cint=None, max_steps=nlev, returnLevels=False, aboutZero=aboutZero )
      cmin,cmax,cint = clev_tup
      tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
      tres.cnLevelSelectionMode = 'ExplicitLevels'

      X = X_list[c]
      Z = Z_list[c]
      tlat = lat_list[c]
      tlon = lon_list[c]
         
      if var_x_case:
         ip = v*num_case+c
      else:
         ip = c*num_var+v

      plot[ip] = ngl.contour( wks, X, tres ) 


      hs.set_subtitles(wks, plot[ip],name[c],f'{tlat:5.1f}N {tlon:5.1f}E',var[v], font_height=0.01)

print()
print(f'      {tlat:5.1f}N {tlon:5.1f}E')

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [len(plot),1]
# if len(ncol.values)==16 : layout = [4,4]
# if len(ncol.values)==9  : layout = [3,3]
# layout = [num_var,ncol]
if var_x_case:
   layout = [num_var,num_case]
else:
   layout = [num_case,num_var]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------