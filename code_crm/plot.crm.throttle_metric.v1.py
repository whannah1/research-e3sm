import os, ngl, subprocess as sp, numpy as np, xarray as xr, dask, numba, copy, string, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
host = hc.get_host()
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
clr,dsh,mrk = [],[],[]
nx_list,ny_list = [],[]
def add_case(case_in,n=None,p=None,s=None,d=0,m=16,c='black'):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = case_in if n is None else n
   case.append(case_in); 
   case_dir.append(p); case_sub.append(s);
   dsh.append(d) ; clr.append(c) ; mrk.append(m) 
   if '2023-THR-TEST' in case_in:
      nxy_idx = case_in.find('NXY_')
      nx_list.append( int(case_in[nxy_idx+4:].split('_')[0]) )
      ny_list.append( int(case_in[nxy_idx+4:].split('_')[1]) )
      name.append(f'{nx_list[-1]}x{ny_list[-1]}')
   else:
      name.append(tmp_name)
#-------------------------------------------------------------------------------
var,lev_list = [],[]
def add_var(var_name,lev=0): var.append(var_name); lev_list.append(lev)
#-------------------------------------------------------------------------------
fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.map.v2','png'
#-------------------------------------------------------------------------------

add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_8_1',  n='',c='red')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_16_1', n='',c='red')
add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_1', n='',c='green')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_64_1', n='',c='red')
add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_128_1',n='',c='blue')
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_256_1',n='',c='red')
add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_512_1',n='',c='purple')

# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_1', n='',c='green')
# add_case('E3SM.2023-THR-TEST-01.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_1', n='',c='cyan')

# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_4', n='',c='green',g=1)
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_64_4', n='',c='green',g=1)
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_128_4',n='',c='green',g=1)

# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_32_32',n='',c='blue',g=2)
# # add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_64_16',n='',c='blue',g=2)
# add_case('E3SM.2023-THR-TEST-00.ne30pg2_EC30to60E2r2.FAQP-MMF1.NXY_128_8',n='',c='blue',g=2)

# xvar, yvar = 'TMQ', 'PRECT'

# xvar, yvar = 'PRECT', 'MMF_MC'
# xvar, yvar = 'PRECT', 'MMF_MCDN'
# xvar, yvar = 'PRECT', 'MMF_MCUDN'

xvar, yvar = 'MMF_TLS',  'MMF_DT'
# xvar, yvar = 'MMF_QTLS', 'MMF_DQ'


# add_var('MMF_QPEVP')
# add_var('MMF_MC')
# add_var('MMF_MCUP')
# add_var('MMF_MCDN')
# add_var('MMF_MCUUP')
# add_var('MMF_MCUDN')



# lev = -71  # 
# lev = -58  # ~ 860 hPa
lev = 45  # ~ 492 hPa

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_crm/crm.throttle_metric.v1','png'


lat1,lat2 = -30, 30

htype,years,months,first_file,num_files = 'h0',[],[],0,3
# htype,years,months,first_file,num_files = 'h2',[],[],30,5

vert_integrate = True
daily_mean     = False
overlay_cases  = True

# recalculate = True
# temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)
if var==[]: num_var = 1

if 'mrk' not in locals():mrk = [16]*num_case
# if mrk==[]: mrk = [16]*num_case

if 'name' not in locals() or name==[]: 
   name = [None]*(num_case)
   for c in range(num_case): name[c] = '*'+case[c][-30:]

if 'clr' not in vars() : clr = ['black']*num_case
# if 'clr' not in vars() : if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)

if vert_integrate:
   # lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
   # lev = np.array([100,150,200,300,400,500,600,700,800,850,900,950,1000])
   lev = np.array([0])


wks = ngl.open_wks(fig_type,fig_file)
if overlay_cases:
   plot = [None]
else:
   plot = [None]*(num_case)
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 12
res.xyMarkLineMode = 'Markers'
# res.xyMarkLineMode = 'MarkLines'
res.xyMarker = 1
res.xyDashPattern = 2
# res.xyMarkerSizeF = 0.015
res.xyMarkerSizeF = 0.001
# res.xyXStyle = "Log"
# res.xyYStyle = "Log"
# res.xyLineColors   = clr

lres = hs.res_xy()
lres.xyDashPattern    = 0
lres.xyLineThicknessF = 1
lres.xyLineColor      = "black"

unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
# res.tiXAxisString = '[mm/day]'
# res.tiYAxisString = '[mm/day]'

#---------------------------------------------------------------------------------------------------
# routine for calculating time derivative
#---------------------------------------------------------------------------------------------------
@numba.njit()
def ddt(data_in,ntime,ncol,dt,data_out,method=1) :
   for t in range(1,ntime):
      # data_out[t,:] = ( data_in[t+1,:] - data_in[t-1,:] ) / dt
      if method==0: data_out[t] = ( data_in[t+1] - data_in[t-1] ) / (dt*2)
      if method==1: data_out[t] = ( data_in[t] - data_in[t-1] ) / (dt)

@numba.njit()
def hybrid_dp3d( PS, P0, hyai, hybi, dp3d ):
   nlev = len(hyai)-1
   (ntime,ncol) = PS.shape
   # dp3d = np.zeros([ntime,nlev,ncol])
   for t in range(ntime):
      for i in range(ncol):
         for k in range(nlev):
            p1 = hyai[k  ]*P0 + hybi[k  ]*PS[t,i]
            p2 = hyai[k+1]*P0 + hybi[k+1]*PS[t,i]
            dp3d[t,k,i] = p2 - p1
   # return dp3d

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []

for v in range(num_var):
   for c in range(num_case):

      print(' '*2+'case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      # case_obj = he.Case( name=case[c], time_freq='daily' )
      case_obj = he.Case( name=case[c] )
      if 'name' in vars() : case_obj.short_name = name[c]
      case_name.append( case_obj.short_name )

      # tmp_file = temp_dir+"/clim.scatter.v1."+case[c]+"."+var+".lat1_"+str(lat1)+".lat2_"+str(lat2)+".nc"
      # print('    tmp_file: '+tmp_file )

      # if recalculate :
      if True:
         #-------------------------------------------------------------------------
         # read the data
         #-------------------------------------------------------------------------
         ### uncommentt this to subset the data
         if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
         if 'lon1' in vars() : case_obj.lon2, case_obj.lon2 = lon2, lon2
         if 'lev' not in vars() : lev = np.array([-1])

         # xvar = nx_list[c]
         # yvar = var[v]

         area = case_obj.load_data('area',htype=htype,num_files=1).astype(float)
         X = case_obj.load_data(xvar,htype=htype,lev=lev,first_file=first_file,num_files=num_files)
         Y = case_obj.load_data(yvar,htype=htype,lev=lev,first_file=first_file,num_files=num_files)

         #-------------------------------------------------------------------------
         if vert_integrate:
            ps = case_obj.load_data('PS',htype=htype,first_file=first_file,num_files=num_files)
            hyai = case_obj.load_data('hyai',htype=htype,num_files=1)
            hybi = case_obj.load_data('hybi',htype=htype,num_files=1)
            dp3d = np.zeros( Y.shape )
            hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values, dp3d )
            dp3d = xr.DataArray(dp3d,dims=Y.dims)
            X = ( X * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')
            Y = ( Y * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')
         #-------------------------------------------------------------------------

         if 'lev' in X.dims: X = X.isel(lev=0)
         if 'lev' in Y.dims: Y = Y.isel(lev=0)

         ### area average
         # X = ( (X*area).sum(dim='ncol') / area.sum(dim='ncol') )
         # Y = ( (Y*area).sum(dim='ncol') / area.sum(dim='ncol') )

         # # Convert to daily mean
         if htype=='h1' and daily_mean: 
            X = X.resample(time='D').mean(dim='time')
            Y = Y.resample(time='D').mean(dim='time')
         
         ### combine columns into single dimension
         if 'ncol' in X.dims: X.stack( allcols=('ncol','time') )
         if 'ncol' in Y.dims: Y.stack( allcols=('ncol','time') )
         
         hc.print_stat(X,name=xvar,stat='naxs',compact=True,indent=' '*6)
         hc.print_stat(Y,name=yvar,stat='naxs',compact=True,indent=' '*6)
         print()
         
         #-------------------------------------------------------------------------
         # Write to file 
         #-------------------------------------------------------------------------
      #    stat_ds.to_netcdf(path=tmp_file,mode='w')
      # else:
      #    stat_ds = xr.open_mfdataset( tmp_file )

      x_list.append(X)
      y_list.append(Y)

   #-------------------------------------------------------------------------------
   #-------------------------------------------------------------------------------
   x_min = np.min([np.nanmin(d) for d in x_list])
   x_max = np.max([np.nanmax(d) for d in x_list])
   y_min = np.min([np.nanmin(d) for d in y_list])
   y_max = np.max([np.nanmax(d) for d in y_list])
   for c in range(num_case):

      X,Y = x_list[c],y_list[c]

      if len(X.dims)>1: X = X.stack(x=X.dims)
      if len(Y.dims)>1: Y = Y.stack(x=Y.dims)

      # print(); print(X)
      # print(); print(Y)
      # exit()

      res.tiXAxisString = xvar
      res.tiYAxisString = yvar

      px = X.stack().values
      py = Y.stack().values

      if overlay_cases:
         x_mag = np.abs(x_max-x_min)
         y_mag = np.abs(y_max-y_min)
         res.trXMinF = x_min - x_mag*0.02
         res.trXMaxF = x_max + x_mag*0.02
         res.trYMinF = y_min - y_mag*0.02
         res.trYMaxF = y_max + y_mag*0.02
      else:
         res.trXMinF = np.min(px)
         res.trXMaxF = np.max(px)

      res.xyLineColor = clr[c]
      res.xyMarkerColor = clr[c]
      res.xyMarker      = mrk[c]
      
      tplot = ngl.xy(wks, px, py, res)
      if overlay_cases:
         pidx = 0
         if c==0:
            plot[0] = tplot
         else:
            ngl.overlay( plot[pidx], tplot )
      else:
         pidx = c
         plot[pidx] = tplot

      #----------------------------------------
      # add linear fit
      if True:
         ### simple and fast method for regression coeff
         a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )

         ### intercept value
         b = np.mean(py) - a*np.mean(px)

         print(' '*2+f'{name[c]}  lin-reg  a: {a}    b: {b}')

         tres = copy.deepcopy(lres)
         tres.xyLineColor = clr[c]
         tres.xyDashPattern    = dsh[c]
         tres.xyLineThicknessF = 6

         px_range = np.abs( np.max(px) - np.min(px) )
         lx = np.array([-1e2*px_range,1e2*px_range])

         tplot = ngl.xy(wks, lx, lx*a+b , tres)
         ngl.overlay( plot[pidx], tplot )

      #----------------------------------------
      # Add horizontal lines
      ngl.overlay( plot[pidx], ngl.xy(wks,[0,0],[-1e8,1e8],lres) )
      ngl.overlay( plot[pidx], ngl.xy(wks,[-1e8,1e8],[0,0],lres) )

      #----------------------------------------
      # add plot strings
      if not overlay_cases:
         hs.set_subtitles(wks, plot[pidx], case_name[c], '', '', font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------