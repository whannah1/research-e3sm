import os, ngl, subprocess as sp, numpy as np, xarray as xr, dask, numba, copy, string, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
host = hc.get_host()
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
clr,dsh,mrk = [],[],[]
nx_list,ny_list = [],[]
group = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,m=16,c='black'):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = case_in if n is None else n
   case.append(case_in); case_dir.append(p); case_sub.append(s)
   group.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m) 
   if '2023-THR-TEST' in case_in:
      nxy_idx = case_in.find('NXY_')
      nx_list.append( int(case_in[nxy_idx+4:].split('_')[0]) )
      ny_list.append( int(case_in[nxy_idx+4:].split('_')[1]) )
      name.append(f'{nx_list[-1]}x{ny_list[-1]}')
   else:
      name.append(tmp_name)
#-------------------------------------------------------------------------------
var,var_name,variance_flag,lev_list = [],[],[],[]
def add_var(var_in,name=None,variance=False,lev=0): 
   var.append(var_in); lev_list.append(lev)
   if name is None: name = var_in
   var_name.append(name); variance_flag.append(variance)
#-------------------------------------------------------------------------------
fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_clim/clim.map.v2','png'
#-------------------------------------------------------------------------------
# Define groups of cases - plot one line per group

# gname,gclr,gdsh,gnum = '32x32','black',0,0
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_32'    ,n=gname, c=gclr, d=gdsh, g=gnum)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_32'    ,n=gname, c=gclr, d=gdsh, g=gnum)

gname,gclr,gdsh,gnum = 'Nx1','red',0,1
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_8_1'    ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_16_1'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_1'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_1'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_1'  ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_1'  ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_1'  ,n=gname, c=gclr, d=gdsh, g=gnum)

gname,gclr,gdsh,gnum = 'Nx4','green',0,2
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_8_4'    ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_16_4'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_4'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_4'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_4'  ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_4'  ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_4'  ,n=gname, c=gclr, d=gdsh, g=gnum)

gname,gclr,gdsh,gnum = 'Nx8','blue',0,3
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_8_8'    ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_16_8'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_8'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_8'   ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_8'  ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_8'  ,n=gname, c=gclr, d=gdsh, g=gnum)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_8'  ,n=gname, c=gclr, d=gdsh, g=gnum)


add_var('PRECT',      name='Precipitation')
add_var('NET_COL_RAD',name='Column Net Rad')
add_var('NET_COL_FLX',name='Column Net Rad+Sfc Fluxes')
# add_var('NET_TOM_RAD',name='TOM Net LW+SW')
# add_var('FLNT',       name='TOM Net LW')
# add_var('FSNT',       name='TOM Net SW')
# add_var('TMQ',      name='Column Water Vapor',  variance=False)
# add_var('MMF_MC',   name='CRM Mass Flux',       variance=False)
# add_var('MMF_MCUP', name='MMF_MCUP',   variance=False)
# add_var('MMF_MCUUP',name='MMF_MCUUP',  variance=False)
# add_var('MMF_MCDN', name='MMF_MCDN',   variance=False)
# add_var('MMF_MCUDN',name='MMF_MCUDN',  variance=False)

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_crm/crm.throttle_metric.v2','png'

htype,years,months,first_file,num_files = 'h0',[],[],0,12
# htype,years,months,first_file,num_files = 'h1',[],[],0,int(365/5)

vert_integrate = True
daily_mean     = False

write_to_file = True
out_file_path = 'data_temp/crm.throttle_metric.v2.nc'

num_plot_col = 2
# num_plot_col = len(var)

# recalculate = True
# temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)
if var==[]: num_var = 1

if 'mrk' not in locals():mrk = [16]*num_case
# if mrk==[]: mrk = [16]*num_case

if 'name' not in locals() or name==[]: 
   name = [None]*(num_case)
   for c in range(num_case): name[c] = '*'+case[c][-30:]

if 'clr' not in vars() : clr = ['black']*num_case
# if 'clr' not in vars() : if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)

if vert_integrate:
   # lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
   # lev = np.array([100,150,200,300,400,500,600,700,800,850,900,950,1000])
   lev = np.array([0])


wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*2)
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 12
res.xyMarkLineMode = 'Markers'
# res.xyMarkLineMode = 'MarkLines'
res.xyMarker = 16
# res.xyDashPattern = 2
res.xyMarkerSizeF = 0.0002
# res.xyLineColors   = clr

res.xyXStyle = "Log"
# res.xyYStyle = "Log"

lres = hs.res_xy()

pmres  = ngl.Resources()
pmres.gsMarkerSizeF       = 10.0
pmres.gsMarkerColor       = 'red'
pmres.gsMarkerIndex       = 16
pmres.gsMarkerThicknessF  = 2
pmres.gsLineThicknessF    = 4

unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
# res.tiXAxisString = '[mm/day]'
# res.tiYAxisString = '[mm/day]'

#---------------------------------------------------------------------------------------------------
# routine for calculating time derivative
#---------------------------------------------------------------------------------------------------
@numba.njit()
def ddt(data_in,ntime,ncol,dt,data_out,method=1) :
   for t in range(1,ntime):
      # data_out[t,:] = ( data_in[t+1,:] - data_in[t-1,:] ) / dt
      if method==0: data_out[t] = ( data_in[t+1] - data_in[t-1] ) / (dt*2)
      if method==1: data_out[t] = ( data_in[t] - data_in[t-1] ) / (dt)

@numba.njit()
def hybrid_dp3d( PS, P0, hyai, hybi, dp3d ):
   nlev = len(hyai)-1
   (ntime,ncol) = PS.shape
   # dp3d = np.zeros([ntime,nlev,ncol])
   for t in range(ntime):
      for i in range(ncol):
         for k in range(nlev):
            p1 = hyai[k  ]*P0 + hybi[k  ]*PS[t,i]
            p2 = hyai[k+1]*P0 + hybi[k+1]*PS[t,i]
            dp3d[t,k,i] = p2 - p1
   # return dp3d

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   x1_list = []
   x2_list = []
   y_list = []
   # lev_list, amp_list = [],[]
   hc.printline()
   print(' '*2+'var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   for c in range(num_case):
      print(' '*4+'case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','archive/atm/hist'
      case_obj = he.Case( name=case[c], data_dir=tmp_path, data_sub=tmp_sub, populate_files=True )

      # tmp_file = temp_dir+f'/throttle_metric.v2.{case[c]}.{var[v]}'
      # if 'lat1' in locals(): tmp_file += f'.lat1_{lat1}.lat2_{lat2}'
      # if 'lon1' in locals(): tmp_file += f'.lon1_{lon1}.lon2_{lon2}'
      # tmp_file += '.nc'
      # print('    tmp_file: '+tmp_file )

      # if recalculate :
      if True:
         #----------------------------------------------------------------------
         # read the data
         #----------------------------------------------------------------------
         ### uncommentt this to subset the data
         if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
         if 'lon1' in vars() : case_obj.lon2, case_obj.lon2 = lon2, lon2
         if 'lev' not in vars() : lev = np.array([-1])

         # xvar = nx_list[c]
         yvar = var[v]

         area = case_obj.load_data('area',htype=htype,num_files=1).astype(float)
         data = case_obj.load_data(yvar,htype=htype,lev=lev,first_file=first_file,num_files=num_files)
         #----------------------------------------------------------------------
         # adjust units
         # if 'MMF_MC' in var[v]: data = data*86400. # convert kg/m2/s => kg/m2/day
         #----------------------------------------------------------------------
         if vert_integrate and 'lev' in data.dims:
            ps = case_obj.load_data('PS',htype=htype,first_file=first_file,num_files=num_files)
            hyai = case_obj.load_data('hyai',htype=htype,num_files=1)
            hybi = case_obj.load_data('hybi',htype=htype,num_files=1)
            dp3d = np.zeros( data.shape )
            hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values, dp3d )
            dp3d = xr.DataArray(dp3d,dims=data.dims)
            data = ( data * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')
         if 'lev' in data.dims: data = data.isel(lev=0)
         #----------------------------------------------------------------------
         # Convert to daily mean
         if htype=='h1' and daily_mean: 
            data = data.resample(time='D').mean(dim='time')

         # calculate temporal variance
         if variance_flag[v]: data = data.var(dim='time')

         # spatial average
         data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )

         # combine columns into single dimension
         if 'ncol' in data.dims: data.stack( allcols=('ncol','time') )
         
      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
      #    tmp_ds.to_netcdf(path=tmp_file,mode='w')
      # else:
      #    tmp_ds = xr.open_mfdataset( tmp_file )

      x1_list.append( float(nx_list[c]) )
      x2_list.append( float(nx_list[c]) * float(ny_list[c]) )
      y_list.append(np.mean(data.values))

   # print()
   # print();print(x_list)
   # print();print(y_list)
   # print()
   # exit()

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   
   tmp_y_list  = y_list
   tmp_x1_list = x1_list
   tmp_x2_list = x2_list
   if len(y_list)==1:
      tmp_y_list  = [ tmp_y_list [0], tmp_y_list [0] ]
      tmp_x1_list = [ tmp_x1_list[0], tmp_x1_list[0] ]
      tmp_x2_list = [ tmp_x2_list[0], tmp_x2_list[0] ]

   res.tiXAxisString = 'Longest CRM Dimension Length'
   plot[v*2+0] = ngl.xy(wks, tmp_x1_list, tmp_y_list, res)

   res.tiXAxisString = 'Total CRM columns'
   plot[v*2+1] = ngl.xy(wks, tmp_x2_list, tmp_y_list, res)

   #----------------------------------------
   # add colored lines based on group number
   unique_group = []
   for g in group:
      if g not in unique_group: 
         unique_group.append(g)
   for ug in unique_group:
      gidx = []
      for i,g in enumerate(group):
         if g==ug: gidx.append(i)

      lres.xyLineThicknessF = 4
      lres.xyLineColor      = clr[gidx[0]]
      lres.xyDashPattern    = dsh[gidx[0]]

      if len(gidx)==1:

         lx = [ x1_list[0], x1_list[0] ]
         ly = [  y_list[0],  y_list[0] ]
         tplot = ngl.xy(wks, lx, ly, lres)
         ngl.overlay( plot[v*2+0], tplot )

         lx = [ x2_list[0], x2_list[0] ]
         ly = [  y_list[0],  y_list[0] ]
         tplot = ngl.xy(wks, lx, ly, lres)
         ngl.overlay( plot[v*2+1], tplot )

      else:
         
         lx = np.array([ x1_list[g] for g in gidx ])
         ly = np.array([ y_list[g] for g in gidx ])
         tplot = ngl.xy(wks, lx, ly, lres)
         ngl.overlay( plot[v*2+0], tplot )

         lx = np.array([ x2_list[g] for g in gidx ])
         ly = np.array([ y_list[g] for g in gidx ])
         tplot = ngl.xy(wks, lx, ly, lres)
         ngl.overlay( plot[v*2+1], tplot )

   #----------------------------------------
   # Add colored markers
   dum = []
   for c in range(num_case):
      pmres.gsLineColor   = clr[c]
      pmres.gsMarkerColor = clr[c]
      dum.append( ngl.add_polymarker(wks, plot[v*2+0], x1_list[c], y_list[c], pmres) )
      dum.append( ngl.add_polymarker(wks, plot[v*2+1], x2_list[c], y_list[c], pmres) )
   
   #----------------------------------------
   # Add horizontal lines
   # lres.xyDashPattern    = 0
   # lres.xyLineThicknessF = 1
   # lres.xyLineColor      = "black"
   # ngl.overlay( plot[pidx], ngl.xy(wks,[0,0],[-1e8,1e8],lres) )
   # ngl.overlay( plot[pidx], ngl.xy(wks,[-1e8,1e8],[0,0],lres) )

   #----------------------------------------
   # add plot strings
   hs.set_subtitles(wks, plot[v*2+0], '', '', var_name[v], font_height=0.015)
   hs.set_subtitles(wks, plot[v*2+1], '', '', var_name[v], font_height=0.015)

   #----------------------------------------------------------------------------
   # add data for file output
   if write_to_file:
      # Create file if this is the first variable
      if v==0: ds_out = xr.Dataset()
      for ug in unique_group:
         # collect data for this group
         gidx = []
         for i,g in enumerate(group):
            if g==ug: gidx.append(i)
         lx1 = np.array([ x1_list[g] for g in gidx ])
         lx2 = np.array([ x2_list[g] for g in gidx ])
         ly  = np.array([  y_list[g] for g in gidx ])
         # write group data to file for this variable
         grp_name = name[gidx[0]]
         if v==0: 
            group_coord = np.arange(len(lx1))
            ds_out[f'crm{grp_name}'] = group_coord
            ds_out[f'crm{grp_name}_longest_dim'] = xr.DataArray(lx1,coords={f'crm{grp_name}':group_coord})
            ds_out[f'crm{grp_name}_total_dim']   = xr.DataArray(lx2,coords={f'crm{grp_name}':group_coord})
         ds_out[f'crm{grp_name}_{var[v]}']       = xr.DataArray(ly ,coords={f'crm{grp_name}':group_coord})

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)

if write_to_file:
   ds_out.to_netcdf(path=out_file_path,mode='w')
   print()
   print(f'  output file: {out_file_path}')
   print()
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------