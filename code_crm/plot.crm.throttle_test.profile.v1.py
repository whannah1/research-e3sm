import os, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None

case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)

var,varstr,vclr,vdsh = [],[],[],[]
def add_var(var_name,s=None,clr='black',dsh=0): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh)
   varstr.append(s)

lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])
# lev = np.array([50,100,150,200,300,400,500,600,700,750,800,850,875,900,925,975])
# lev = np.array([0.1,0.5,1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])
# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])

#---------------------------------------------------------------------------------------------------
### CRM throttling tests


# suffix = '32xM'
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_1'   ,n='32x1', c='red', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_4'   ,n='32x4', c='green', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_8'   ,n='32x8', c='blue', d=0)

# suffix = 'fixed_NxM'
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_32'  ,n='32x32', c='red', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_8'  ,n='128x8', c='green', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_4'  ,n='256x4', c='blue', d=0)

# suffix = 'Nx8'
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_8'   ,n='32x8',  c='magenta', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_8'   ,n='64x8',  c='red', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_8'  ,n='128x8', c='green', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_8'  ,n='256x8', c='blue', d=0)
# add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_8'  ,n='512x8', c='purple', d=0)

suffix = 'Nx1'
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_32_1'   ,n='32x1',  c='magenta', d=0)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_64_1'   ,n='64x1',  c='red', d=0)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_128_1'  ,n='128x1', c='green', d=0)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_256_1'  ,n='256x1', c='blue', d=0)
add_case('E3SM.2023-THR-TEST-02.ne4pg2_oQU480.F2010-MMF1.NXY_512_1'  ,n='512x1', c='purple', d=0)

#---------------------------------------------------------------------------------------------------

# add_var('T')
# add_var('Q')

# add_var('U')
# add_var('V')
# add_var('OMEGA')

add_var('CLDLIQ'  ,s='Cloud Liquid Amount')
add_var('CLDICE'  ,s='Cloud Ice Amount')
add_var('RH'      ,s='Rel. Humidity')
add_var('MMF_MCUP',s='CRM Upward Mass Flux')


# add_var('MMF_QC')
# add_var('MMF_QTFLX')
# add_var('MMF_QTFLXS')
# add_var('MMF_TKEB')
# add_var('MMF_TKEQC')
# add_var('MMF_TKEQT')
# add_var('MMF_TKES')
# add_var('MMF_TKEW')

# add_var('CLOUD')

# add_var('CLDLIQ')
# add_var('CLDICE')
# add_var('RH')

# add_var('MMF_QC')
# add_var('MMF_QI')
# add_var('MMF_QR')

# add_var('MMF_MCUP')
# add_var('MMF_MCDN')
# add_var('MMF_MCUUP')
# add_var('MMF_MCUDN')
# add_var('MMF_MC')
# add_var('MMF_MCU')

#---------------------------------------------------------------------------------------------------

if 'suffix' not in locals(): raise NameError('The variable "suffix" must be defined')

num_plot_col = len(var)
# num_plot_col = 4

fig_file,fig_type = os.getenv('HOME')+f'/Research/E3SM/figs_crm/crm.throttle_test.profile.v1.{suffix}','png'

htype,years,months,first_file,num_files = 'h0',[],[],0,12

plot_diff = False

use_height_coord   = False

# omit_bot,bot_k     = False,-2
# omit_top,top_k     = False,30

print_stats        = False
print_profile      = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)
res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
res.tmYLLabelFontHeightF = 0.02
res.tmXBLabelFontHeightF = 0.02
res.tiXAxisFontHeightF   = 0.02
res.tiYAxisFontHeightF   = 0.02

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

if use_height_coord: 
   res.tiYAxisString = 'Height [km]'
else:
   res.tiYAxisString = 'Pressure [hPa]'
   res.trYReverse = True
   # res.xyYStyle = 'Log'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(hc.tcolor.GREEN+'  var: '+var[v]+hc.tcolor.ENDC)
   data_list,lev_list = [],[]
   for c in range(num_case):
      print(hc.tcolor.CYAN+'    case: '+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'
      if tvar=='MMF_MCU': tvar = 'MMF_MCUUP'

      #-------------------------------------------------------------------------
      # read the data
         
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      tnum_files = num_files

      if use_height_coord: 
         Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c])

      area = case_obj.load_data('area',htype=htype,first_file=first_file,num_files=tnum_files).astype(np.double)
      data = case_obj.load_data(tvar,  htype=htype,first_file=first_file,num_files=tnum_files,lev=lev)

      #-------------------------------------------------------------------------
      # Handle derived variables
      
      if tvar=='MMF_MCU': 
         data = data + case_obj.load_data('MMF_MCUDN',htype=htype,first_file=first_file,num_files=tnum_files,lev=lev)
      
      #-------------------------------------------------------------------------
      
      hc.print_time_length(data.time,indent=' '*6)
      # if print_stats: hc.print_stat(data,name=f'{var[v]} before averaging',indent=' '*4,compact=True)

      data = data.mean(dim='time')
      if use_height_coord: Z = Z.mean(dim='time')

      if case[c]=='ERA5' or 'ncol' not in data.dims:
         data = ( (data*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )
      else:
         if use_height_coord: Z = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') )
         data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )

      #-------------------------------------------------------------------------
      # vertical subset logic

      # if omit_bot:
      #    data = data[:bot_k]
      #    if use_height_coord: Z = Z[:bot_k]

      # if omit_top:
      #    data = data[top_k:]
      #    if use_height_coord: Z = Z[top_k:]
      
      #-------------------------------------------------------------------------

      if print_stats: hc.print_stat(data,name=f'{var[v]} after averaging',indent=' '*4,compact=True)

      if print_profile:
         print()
         for xx in data.values: print(f'    {xx}')
         print()

      # gbl_mean = ( (data*area).sum() / area.sum() ).values 
      # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

      data_list.append( data.values )
      if use_height_coord:
         lev_list.append( Z.values )
      else:
         lev_list.append( data['lev'].values )

   data_list_list.append(data_list)
   lev_list_list.append(lev_list)


#-------------------------------------------------------------------------------
# Create plot - overlay all cases for each var
#-------------------------------------------------------------------------------

for v in range(num_var):
   data_list = data_list_list[v]
   lev_list = lev_list_list[v]
   
   tres = copy.deepcopy(res)

   if var[v] in ['O3','Mass_so4']: tres.xyXStyle = 'Log'

   # ip = v*num_case+c
   ip = c*num_var+v
   
   baseline = data_list[0]
   if plot_diff:
      for c in range(num_case): 
         data_list[c] = data_list[c] - baseline

   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = data_min
   tres.trXMaxF = data_max
   ip = v

   for c in range(num_case):
      tres.xyLineColor   = clr[c]
      tres.xyMarkerColor = clr[c]
      tres.xyDashPattern = dsh[c]

      tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)
      
      if (c==1 and plot_diff) or (c==0 and not plot_diff) :
         plot[ip] = tplot
      elif (plot_diff and c>0) or not plot_diff:
         ngl.overlay(plot[ip],tplot)

   ### add vertical line
   lres = hs.res_xy()
   lres.xyLineThicknessF = 1
   lres.xyDashPattern = 0
   lres.xyLineColor = 'black'
   ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

   ctr_str = ''
   # var_str = var[v]
   # if var[v]=='CLOUD': var_str = 'Cloud Fraction'

   if 'lat1' in locals(): 
      lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
      lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
      ctr_str += f' {lat1_str}:{lat2_str} '
   if 'lon1' in locals(): 
      lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
      lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
      ctr_str += f' {lon1_str}:{lon2_str} '

   if plot_diff: var_str += ' (diff)'

   
   # hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str, font_height=0.008)
   hs.set_subtitles(wks, plot[ip], '', ctr_str, varstr[v], font_height=0.008)


#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.08
   lgres.lgLabelFontHeightF = 0.008
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 10
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   lx,ly = 0.13,0.55
   # if num_var==2: lx,ly = 0.3,0.45
   # if num_var==4: lx,ly = 0.05,0.5

   labels = case_name
   for l,lbl in enumerate(labels): labels[l] = f'  {lbl}'

   pid = ngl.legend_ndc(wks, len(labels), labels, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------


layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

pres.nglPanelFigureStrings            = list(string.ascii_lowercase)
pres.nglPanelFigureStringsJust        = "TopLeft"
pres.nglPanelFigureStringsFontHeightF = 0.01

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
