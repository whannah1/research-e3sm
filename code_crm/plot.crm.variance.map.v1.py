import os
import ngl
import subprocess as sp
import xarray as xr
import xrft
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
host = sp.check_output(["dnsdomainname"],universal_newlines=True).strip()

if 'ornl.gov' in host :
   # case,name = ['INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'],['INCITE2019']
   case,name = ['E3SM_TEST-CRM-WIND_GPU_ne30pg2_FSP1V1_00'],['MMF']

# var = ['TGCLDLWP','CRM_QV']
var = ['CRM_QV','CRM_QV']
# var = ['CRM_QV','CRM_QV','CRM_QV','CRM_QV']

fig_type = "png"
fig_file = os.getenv("HOME")+"/Research/E3SM/figs_crm/clim.variance.map.v1"

lat1,lat2 = -50,50

htype,first_file,num_files = 'h1',0,-1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case)
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1#-1
if 'lat2' in vars() : res.mpMaxLatF = lat2#+1
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

### use this for aquaplanets
# res.mpCenterLonF,res.mpOutlineBoundarySets = 0.,"NoBoundaries"

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )
      # case_obj.time_freq = 'daily'

      # ip = v*num_case+c
      ip = c*num_var+v
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      # area = case_obj.load_data('area',htype=htype,first_file=first_file,num_files=num_files).astype(np.double)
      data = case_obj.load_data(var[v],htype=htype,first_file=first_file,num_files=num_files)
      
      if 'time'   in data.dims : data = data.isel(time=0)
      if 'lev'    in data.dims : data = data.isel(lev=0)
      if 'crm_nz' in data.dims : 
         # data = data.isel(crm_ny=0,crm_nz=slice(1,5)).var(dim=['crm_nx','crm_nz'])
         # data = data.isel(crm_ny=0,crm_nz=5).var(dim=['crm_nx'])
         # if v==0 : data = data.isel(crm_ny=0,crm_nz=5).var(dim=['crm_nx'])
         # if v==1 : data = data.isel(crm_ny=0,crm_nz=5).var(dim=['crm_nx'])

         data = data.isel(crm_ny=0,crm_nz=5)
         data_fft = xrft.power_spectrum(data,dim=['crm_nx']).compute()
         if v==0 : data_fft = data_fft.isel(freq_crm_nx=33)
         if v==1 : data_fft = data_fft.isel(freq_crm_nx=63)
         # data_fft = data_fft.isel(freq_crm_nx=0) - data_fft.isel(freq_crm_nx=63)
         # if v==num_var-1 : 
         #    data_fft = data_fft.isel(freq_crm_nx=63)
         # else:
         #    data_fft = data_fft.isel(freq_crm_nx=v)
         data = data_fft
         
         # print(data)
         # print()
         # hc.print_stat(data)
         # exit()

      # if 'time' in data.dims : 
      #    hc.print_time_length(X.time)
      #    data = data.mean(dim='time')
         

      # if 'area' in vars() :
      #    gbl_mean = ( (data*area).sum() / area.sum() ).values 
      #    print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      # if var[v] in ['SPTKE']                    : tres.cnFillPalette = "BlueWhiteOrangeRed"
      # if var[v] in ['OMEGA']                    : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = "WhiteBlueGreenYellowRed"
      if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = "CBR_wet"
      if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = "MPL_viridis"

      if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,20+1,1)
      if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2 , 0.25, num=60)

      # if var[v]=='CRM_QV' : tres.cnLevels = np.logspace( -7 , -6, num=30)
      if var[v]=='CRM_QV' : tres.cnLevels = np.logspace( -7 , -4, num=30)
      
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      # else:
      #    aboutZero = False
      #    if var[v] in ['SPTLS','SPQTLS','U','V'] : aboutZero = True
      #    clev_tup = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
      #                                    cint=None, max_steps=21,              \
      #                                    returnLevels=False, aboutZero=aboutZero )
      #    if clev_tup==None: 
      #       tres.cnLevelSelectionMode = "AutomaticLevels"   
      #    else:
      #       cmin,cmax,cint = clev_tup
      #       tres.cnLevels = np.linspace(cmin,cmax,num=21)
      #       tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------

      if 'DYN_' in var[v] : case_obj.grid = case_obj.grid.replace('pg2','')

      hs.set_cell_fill(case_obj,tres)

      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name

      plot[ip] = ngl.contour_map(wks,data.values,tres) 


      case_obj.ncol_name ='ncol_d'
      case_obj.grid = case_obj.grid.replace('pg2','np4')
      scripfile = case_obj.get_scrip()
      
      ctr_str = ''
      # if var[v] in ['PRECT','PRECC','PRECL'] and 'gbl_mean' in vars() : 
      #    ctr_str = 'Mean: '+'%.2f'%gbl_mean+' [mm/day]'
      hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=0.01)


#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [num_case,num_var]
layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]
if num_case==1 and num_var==4 : layout = [2,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
