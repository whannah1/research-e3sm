#! /usr/bin/env python
#===================================================================================================
# This script downloads the latest Hadley Center global gridded temperature data
#===================================================================================================
import os, gzip, time, datetime
import urllib3
# import requests
# from urllib3 import request
# import wget
home = os.getenv('HOME') 
#===================================================================================================

dst_dir = '/global/cfs/cdirs/m3312/whannah/obs_data/HadCRU'

# src_dir = 'http://www.cru.uea.ac.uk/cru/data/temperature'
src_dir = 'https://crudata.uea.ac.uk/cru/data/temperature'

file_name = 'HadCRUT.5.0.1.0.analysis.anomalies.ensemble_mean.nc'

#-------------------------------------------------------------------------------
# specify file paths

if not os.path.exists(dst_dir) : os.makedirs(dst_dir)

# os.chdir(dst_dir)
# now = datetime.date.today()
# print(now.strftime('\nToday : %a, %b %d, %Y'))

src_file = f'{src_dir}/{file_name}'
dst_file = f'{dst_dir}/{file_name}'

print()
print('  src: '+src_file)
print('  dst: '+dst_file)
print()

#-------------------------------------------------------------------------------
# get the data

# response = request.urlretrieve("https://instagram.com/favicon.ico", "instagram.ico")

# urllib3.urlretrieve(src_file,filename=dst_file)

# response = requests.get(src_file)
# open(dst_file, "wb").write(response.content)

# response = wget.download(src_file,dst_file)

http = urllib3.PoolManager()
r = http.request('GET', src_file, preload_content=False)
out = open(dst_file, 'wb')
out.write(r.read())
r.release_conn()

#===================================================================================================
#===================================================================================================
