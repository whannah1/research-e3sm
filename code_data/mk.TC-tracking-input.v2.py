import os
import sys
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import subprocess
home = os.getenv("HOME")
from glob import glob
print()

case_list = ['earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m']

# fig_type = "png"
# fig_file = home+"/Research/E3SM/figs_grid/grid.se.profile.v1"

# htype,years,months,num_files = 'h0',[],[],0
# months = [6,7,8]

lat1,lat2 = 0,50
lon1,lon2 = 360-105,360-10

verbose = True

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# Vertical intrepolation options
interp_type = 2         # 1=LINEAR, 2=LOG, 3=LOG-LOG
extrap_flag = False

var = ['time','lat','lon','area','hyam','hybm','hyai','hybi','P0',
       'date','datesec','time_bnds','date_written','time_written',
       'ndbase','nsbase','nbdate','nbsec'
      ]

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for case in case_list:
   print('    case: ' + case )
   case_obj = he.Case(name=case)

   odir = '/gpfs/alpine/scratch/hannah6/cli115/analysis_files/{}/'.format(case)
   # odir = '/gpfs/alpine/proj-shared/cli115/hannah6/tmp_data/'+case[c]+'/'
   # odir = '/gpfs/alpine/scratch/hannah6/cli115/analysis_files/'+case[c]+'/'
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------

   # h0_files = case_obj.hist_path+'*.cam.h0.*'
   # h1_files = case_obj.hist_path+'*.cam.h1.000[2-7]*'
   # h2_files = case_obj.hist_path+'*.cam.h2.000[2-7]*'

   h0_files = subprocess.check_output('ls '+case_obj.hist_path+'*.cam.h0.*',        shell=True,stderr=subprocess.STDOUT,universal_newlines=True ).split("\n")
   h1_files = subprocess.check_output('ls '+case_obj.hist_path+'*.cam.h1.000[2-7]*',shell=True,stderr=subprocess.STDOUT,universal_newlines=True ).split("\n")
   h2_files = subprocess.check_output('ls '+case_obj.hist_path+'*.cam.h2.000[2-7]*',shell=True,stderr=subprocess.STDOUT,universal_newlines=True ).split("\n")

   # Discard last entry if empty
   if h1_files[-1]=='' : h1_files.pop()
   if h2_files[-1]=='' : h2_files.pop()

   # h1_files = h1_files[:5]
   # h2_files = h2_files[:5]

   ds0 = xr.open_dataset( h0_files[0] )

   for file1, file2 in zip(h1_files, h2_files):
      time_stamp = file1.split('.')[-2]
      ofile = odir+case+'.tc_tracking_input.'+time_stamp+'.nc'
      if verbose: 
         print('\n    input file  :  '+file1)
         print(  '    output file :  '+ofile+'\n')
      #----------------------------------------------------
      # open dataset
      ds1 = xr.open_dataset(file1)
      ds2 = xr.open_dataset(file2)
      #----------------------------------------------------
      # create mask

      # note: there may be a more efficient way to do this, but no biggie
      mask = xr.full_like( ds2['ncol'], True, dtype=bool )
      if 'lat1' in vars() : mask = mask & (ds2['lat']>=lat1) & (ds2['lat']<=lat2)
      if 'lon1' in vars() : mask = mask & (ds2['lon']>=lon1) & (ds2['lon']<=lon2)
      #----------------------------------------------------
      # Select variables to keep and apply mask
      if verbose : sys.stdout.write('  Creating base output dataset... ')
      if verbose : sys.stdout.flush()

      ds2 = ds2.where(mask, drop=True)  # can do this once
      ds_out = ds2[var]

      #----------------------------------------------------
      # Copy PS
      # ds_out['PS'] = ds1['PS'].where(mask,drop=True).resample(time='3H').mean(dim='time')
      # ds_out['PS'].attrs['units'] = ds1['PS'].attrs['units']
      # ds_out['PS'].attrs['long_name'] = ds1['PS'].attrs['long_name']
      
      # alternate approach: 
      ds_out['PS'] = ds1['PS'].where(mask, drop=True).sel(time=ds_out['time']) 

      if verbose : sys.stdout.write('done\n')
      if verbose : sys.stdout.flush()
      #----------------------------------------------------
      # Create pmid for PSL extrapolation
      pmid = ds_out['hyam'] * ds_out['P0'] + ds_out['hybm'] * ds_out['PS']
      pmid.attrs['units'] = 'Pa'
      #----------------------------------------------------
      # extrapolate to get sea level pressure (PSL) 
      # This is too slow!!! ds_out['PSL'] = he.calc_PSL( pmid, ds0['PHIS'].where(mask,drop=True).isel(time=0), ds_out['PS'], ds2['T'].where( mask, drop=True) )
      # print('Calculating PSL...')
      if verbose : sys.stdout.write('  Calculating PSL... ')
      if verbose : sys.stdout.flush()

      T_tmp = ds2['T']
      phis_tmp = ds0['PHIS'].where(mask,drop=True).isel(time=0)
      ntime = len( T_tmp['time'] )
      ncol  = len( T_tmp['ncol'] )
      nlev  = len( T_tmp['lev'] )
      T_tmp = T_tmp.isel(lev=nlev-1).transpose('time','ncol')
      pmid  = pmid.isel(lev=nlev-1).transpose('time','ncol')
      ds_out['PSL'] = xr.full_like(ds_out['PS'],np.nan)
      he.calc_PSL_numba( ntime, nlev, ncol, pmid.values, phis_tmp.values, ds_out['PS'].values, T_tmp.values, ds_out['PSL'].values )
      
      if verbose : sys.stdout.write('done\n')
      if verbose : sys.stdout.flush()
      #----------------------------------------------------
      # Set up vertical interpolation inputs
      P0 = ds2['P0'].values/1e2
      PS_tmp = ds_out['PS'].expand_dims(dim='dummy',axis=len(ds_out['PS'].dims)).values
      Z_tmp  = ds2['Z3'].expand_dims(dim='dummy',axis=len(ds2['Z3'].dims)).load()
      #----------------------------------------------------
      # Vertically interpolate Z for thickness calculation
      if verbose : sys.stdout.write('  Calculating Z300... ')
      if verbose : sys.stdout.flush()

      Z_out = ngl.vinth2p( Z_tmp.values, ds2['hyam'].values, ds2['hybm'].values, \
                           np.array([float(300)]), PS_tmp, interp_type, P0, 1, extrap_flag)[:,0,:,0]
      ds_out['Z300'] = xr.DataArray( Z_out, dims=['time','ncol'], coords=[('time',Z_tmp['time']),('ncol', Z_tmp['ncol'])] )
      ds_out['Z300'].attrs['units'] = ds2['Z3'].attrs['units']
      ds_out['Z300'].attrs['long_name'] = '300 mb '+ds2['Z3'].attrs['long_name']
      
      if verbose : sys.stdout.write('done\n')
      if verbose : sys.stdout.write('  Calculating Z300... ')
      if verbose : sys.stdout.flush()

      Z_out = ngl.vinth2p( Z_tmp.values, ds2['hyam'].values, ds2['hybm'].values, \
                           np.array([float(500)]), PS_tmp, interp_type, P0, 1, extrap_flag)[:,0,:,0]
      ds_out['Z500'] = xr.DataArray( Z_out, dims=['time','ncol'], coords=[('time',Z_tmp['time']),('ncol', Z_tmp['ncol'])] )
      ds_out['Z500'].attrs['units'] = ds2['Z3'].attrs['units']
      ds_out['Z500'].attrs['long_name'] = '500 mb '+ds2['Z3'].attrs['long_name']

      if verbose : sys.stdout.write('done\n')
      if verbose : sys.stdout.flush()
      #----------------------------------------------------
      # Get near-surface wind
      if verbose : sys.stdout.write('  Calculating UBOT and VBOT... ')
      if verbose : sys.stdout.flush()

      ds_out['UBOT'] = ds_out['U'].isel(lev=-1)
      ds_out['VBOT'] = ds_out['U'].isel(lev=-1)

      if verbose : sys.stdout.write('done\n')
      if verbose : sys.stdout.flush()
      #----------------------------------------------------
      # write to file
      if verbose : sys.stdout.write('  Writing dataseet to file... ')
      if verbose : sys.stdout.flush()

      encoding = {v: {'dtype': 'float32', '_FillValue': -9999.0} for v in ds_out.data_vars}
      ds_out.to_netcdf(path=ofile,mode='w',engine='netcdf4', encoding=encoding)

      if verbose : sys.stdout.write('done\n')
      if verbose : sys.stdout.flush()
      #----------------------------------------------------
      # print file name
      print("\n"+ofile+"\n")

      exit()
      

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------