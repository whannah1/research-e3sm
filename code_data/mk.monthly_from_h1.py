import os,ngl,copy,subprocess as sp
import xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import datetime, time
home,scratch = os.getenv('HOME'),os.getenv('SCRATCH')

# case = 'E3SM.ne30pg2_r05_oECv3.F-MMF1.01'
case = 'E3SM.ne30pg2_r05_oECv3.F-MMF1.CRM-AC.RAD-AC.01'

idir = f'{scratch}/e3sm_scratch/cori-knl/{case}/run'
odir = f'{scratch}/e3sm_scratch/cori-knl/{case}/run'

search_str = f'*cam.h1.*'
files = sp.check_output(f'ls {idir}/{search_str}',shell=True,universal_newlines=True ).split('\n')
if files[-1]=='' : files.pop()

def drop_rad_band(ds): 
  if 'lwband' in ds.variables: ds = ds.drop(['lwband'])
  if 'swband' in ds.variables: ds = ds.drop(['swband'])
  return ds
ds = xr.open_mfdataset(files,combine='by_coords',preprocess=drop_rad_band)
print()
print(ds)
print()
ds = ds.resample(time='D').mean('time')
# ds = ds.resample(time='MS').mean('time')    # Convert to monthly mean

print(ds)
exit()

# ------------------------------------------------------------------------------
def get_files(search_str):
  # print(f'search_str: {search_str}')
  files = sp.check_output(f'ls {idir}/{search_str}',shell=True,universal_newlines=True ).split('\n')
  if files[-1]=='' : files.pop()
  return files
# ------------------------------------------------------------------------------

# ifiles = get_files('*cam.h1.*')
# num_months = len(ifiles)*5/30

# for m in range(num_months):
for y in range(1,2):
  for m in range(1,12+1):

    ifiles = get_files( f'*cam.h1.{y:04}-{m:02}*' )

    for f in ifiles: print(f)
    print()
    
    ofile = f'{odir}/ARM-SGP.monthly.{yr}.nc'

    print(f'  {yr}    {ofile}')

    # Get list of daily files
    files = sp.check_output(f'ls {idir}/*.{yr}*',shell=True,universal_newlines=True ).split('\n')
    if files[-1]=='' : files.pop()

    # Load dataset
    def avg_by_station(ds): return ds.mean(dim='station_number').resample(time='D').mean('time',skipna=True)
    ds_obs = xr.open_mfdataset(files[:365*2],combine='by_coords',preprocess=avg_by_station)

    data_obs = ds_obs['volumetric_water_content']
    data_obs = data_obs.resample(time='MS').mean('time',skipna=True)    # Convert to monthly mean

    # delete old file
    if os.path.isfile(ofile) : os.remove(ofile)

    data_obs.to_netcdf(ofile)