import os
import ngl, xarray as xr, numpy as np, subprocess as sp
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None):
   global name,case,case_dir,case_sub
   case.append(case_in); name.append(n)
   case_dir.append(p); case_sub.append(s)
#-------------------------------------------------------------------------------
# case = ['earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m']
# case = ['INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026']

### physgrid validation
# name,case,git_hash = [],[],'cbe53b'
# case.append(f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# case.append(f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# case.append(f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# case.append(f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# case.append(f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# case.append(f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# for c in case:
#    if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4')
#    if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2')
#    if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3')
#    if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4')
#    if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4')
#    if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4')
#    if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2')


# tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/2023-CPL/','data_remap_90x180'
# tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/2023-CPL/','archive/atm/hist'
# add_case('E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='E3SM-MMF',p=tmp_path,s=tmp_sub)

### PAM dev tests
tmp_scratch = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/'
add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF1',n='MMF1 L72',p=tmp_scratch,s='run')
add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF2',n='MMF2 L72',p=tmp_scratch,s='run')

# var = ['Q','U','V']
var = ['PRECT']

season = 'JJA'

# fig_type = "png"
# fig_file = os.getenv("HOME")+"/Research/E3SM/figs_grid/grid.se.profile.v1"

# htype,years,months,num_files = 'h0',[],[],0
# months = [6,7,8]

lat1,lat2 = 50,50

# lat1,lat2 = 35,40
# lon1,lon2 = 360-95,360-90

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

for c in ['time','lat','lon','area','hyam','hybm','hyai','hybi'] : var.append(c)
for c in ['date','datesec','time_bnds','date_written','time_written','ndbase','nsbase','nbdate','nbsec'] : var.append(c)

def diurnal(ds) : return ds.groupby('time.hour').mean(dim='time')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# for v in range(num_var):
#    hc.printline()
#    print('  var: '+var[v])
for c in range(num_case):
   print('    case: '+case[c])

   data_dir_tmp,data_sub_tmp = None, None
   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]

   case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp, populate_files=False )

   # odir = '/gpfs/alpine/proj-shared/cli115/hannah6/tmp_data/'+case[c]+'/'   # this might have caused issue for batch script?
   # odir = '/gpfs/alpine/cli115/proj-shared/hannah6/tmp_data/'+case[c]+'/'
   odir = os.getenv('HOME')+'/Research/E3SM/data_temp'
   ofile = f'{odir}/diurnal.comp.v1.{case[c]}.{season}.nc'
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   # for yr in [3,4,5,6,7] :

   #    # Find files for each year
   #    files = sp.check_output('ls '+case_obj.hist_path+'*.cam.h2.000'+str(yr)+'*'\
   #             ,shell=True,stderr=sp.STDOUT,universal_newlines=True ).split("\n")

   # files = sp.check_output(f'ls {case_obj.hist_path}/*.eam.h1.200[0-9]*',shell=True,stderr=sp.STDOUT,universal_newlines=True ).split("\n")
   files = sp.check_output(f'ls {case_obj.hist_path}/*.eam.h1.000[0-4]*',shell=True,stderr=sp.STDOUT,universal_newlines=True ).split("\n")
   # Discard last entry if empty
   if files[-1]=='' : files.pop()

   # files = files[:5]
   # print(files)

   # open dataset
   ds = xr.open_mfdataset( files, combine='by_coords' )

   da = ds[var[0]]
   if 'lev'  in da.dims: da = da.isel(lev=0)
   if 'ilev' in da.dims: da = da.isel(ilev=0)
   if 'nbnd' in da.dims: da = da.isel(nbnd=0)

   print(); print(da)

   # create spatial mask and apply
   ncol = ds['ncol']
   mask = xr.full_like( ncol, True, dtype=bool )
   mask = mask & (ds['lat']>=lat1) & (ds['lat']<=lat2)
   if 'lon1' in locals(): mask = mask & (ds['lon']>=lon1) & (ds['lon']<=lon2)
   if 'time' in mask.dims: mask = mask.isel(time=0)

   # da = da.where( mask, drop=True)

   # # group by hour and season
   # ds = ds.groupby('time.season').apply(diurnal)
   # ds = ds.sel(season=season)

   da.load()

   # print(); print(da); print()

   # # group by hour and season
   # da = da.groupby('time.season')

   da = da.sel(time=(da['time.season']==season))

   da.load()

   # print('-'*80)
   # print(); print(da); print()
   # print('-'*80)

   # # da = da.sel(season=season)
   # da = da.sel(time=da.time.dt.season==season)

   # print(); print(da); print()

   # da = da.apply(diurnal)

   da = da.groupby('time.hour').mean(dim='time')

   # write to file
   da.to_netcdf(path=ofile,mode='w')

   # print file name
   print("\n"+ofile+"\n")
      

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------



#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------