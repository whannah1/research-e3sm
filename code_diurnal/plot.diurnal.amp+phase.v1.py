import os, ngl, xarray as xr, numpy as np, subprocess as sp, numba, itertools
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None):
   global name,case,case_dir,case_sub
   case.append(case_in); name.append(n)
   case_dir.append(p); case_sub.append(s)
#-------------------------------------------------------------------------------
### PAM dev tests
tmp_scratch = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/'
add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF1',n='MMF1',p=tmp_scratch,s='run')
# add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF2',n='MMF2',p=tmp_scratch,s='run')
#-------------------------------------------------------------------------------

var = ['PRECT']

season = 'JJA'

lat1,lat2 = 50,50

# number of files to load
first_file, num_files = 365*1, 365*1

# htype,years,months,num_files = 'h0',[],[],0
# months = [6,7,8]

recalculate_composite = False

fig_file,fig_type = os.getenv("HOME")+'/Research/E3SM/figs_diurnal/diurnal.amp+phase.v1','png'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

def calc_diurnal_comp(da): 
  # return ds.groupby('time.hour').mean(dim='time')
  da = da.sel(time=(da['time.season']==season))
  da = da.groupby('time.hour').mean(dim='time')
  da.load()
  return da

# @numba.njit()
# def correlate(data1, data2):
#   corr_coef = np.zeros(data1.shape)
#   np.correlate(data1,data2)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
  hc.printline()
  print('  var: '+var[v])
  for c in range(num_case):
    print('    case: '+case[c])

    # odir = '/gpfs/alpine/proj-shared/cli115/hannah6/tmp_data/'+case[c]+'/'   # this might have caused issue for batch script?
    # odir = '/gpfs/alpine/cli115/proj-shared/hannah6/tmp_data/'+case[c]+'/'
    tmp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
    tmp_file = f'{tmp_dir}/diurnal.comp.v1.{case[c]}.{var[v]}.{season}.nc'

    print("\n"+tmp_file+"\n")

    #---------------------------------------------------------------------------
    if recalculate_composite:

      files = sp.check_output(f'ls {case_dir[c]}/{case[c]}/{case_sub[c]}/*.eam.h1.*', \
              shell=True,stderr=sp.STDOUT,universal_newlines=True ).split("\n")
      if files[-1]=='' : files.pop() # Discard last list entry if empty
      files = files[first_file:first_file+num_files]

      ds = xr.open_mfdataset( files, combine='by_coords' )

      da = ds[var[v]]
      if 'lev'  in da.dims: da = da.isel(lev=0)
      if 'ilev' in da.dims: da = da.isel(ilev=0)
      if 'nbnd' in da.dims: da = da.isel(nbnd=0)

      # create spatial mask and apply
      ncol = ds['ncol']
      mask = xr.full_like( ncol, True, dtype=bool )
      mask = mask & (ds['lat']>=lat1) & (ds['lat']<=lat2)
      if 'lon1' in locals(): mask = mask & (ds['lon']>=lon1) & (ds['lon']<=lon2)
      if 'time' in mask.dims: mask = mask.isel(time=0)

      da = calc_diurnal_comp(da)

      # write to file
      da.to_netcdf(path=tmp_file,mode='w')

    else:

      ds = xr.open_mfdataset( tmp_file )
      da = ds[var[v]].load()

    
    # print(); print(da); print()
    # exit()
    
    #---------------------------------------------------------------------------
    # Prepare for phase calculation

    # Calculate diurnal amplitude for normalization
    tmp_amp = da.max(dim='hour') - da.min(dim='hour')

    # remove mean and normalize the diurnal signal
    da_norm = ( da - da.mean(dim='hour') ) / tmp_amp

    # print()
    # print(da_norm)
    # print()
    # exit()
    #---------------------------------------------------------------------------
    # Calculate diurnal phase - hour of max correlation using first harmonic only

    hr = np.arange(24)     # local solar time coord
    hh = 2 * np.pi * hr/24 # local solar time coord in radians
    dh = 1

    for h in range(len(hr)):
      # calculate sine wave correlation at each phase
      tsin = np.sin(2.*np.pi*(hr - h*dh)/24.)

      if h==2: exit()

    
    # tamp := conform(tV,iamp,(/1,2/))         ; 3D amplitude
    # hh2  := conform(tV,hh,(/0/))             ; 3D time coord in radians

    # # correlate data with a sine wave of all possible phases    
    # cor := new(dims,float)
    # do h = 0,nh-1
    #   tsin := sin(2.*pi*(hr - h*dh)/24.)
    #   cor(h,:,:) = escorc_n(tV,tsin,0,0 )
    

    # Find hour of maximum correlation

    # use temporary sine function with estimated phase shift to get final amplitude

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------



#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------