#---------------------------------------------------------------------------------------------------
# Plot the zonal mean of the specified variables
#---------------------------------------------------------------------------------------------------
import os, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
np.seterr(divide='ignore', invalid='ignore')
np.errstate(divide='ignore', invalid="ignore")
print()

case,name,clr,dsh,mrk = [],[],[],[],[]
def add_case(case_in,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)

### Variance transport validation
add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.00',     n='E3SM-MMF')
add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_0.00',n='E3SM-MMF + variance transport')
# add_case('E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_1.00',n='MMF FVT1')

pvar,lev_list = [],[]
def add_var(var_name,lev=-1): pvar.append(var_name); lev_list.append(lev)

# add_var('PRECT')
# add_var('TMQ')
# add_var('LHFLX')
# add_var('SHFLX')
# add_var('P-E')
# add_var('TGCLDLWP')
add_var('TGCLDIWP')

# add_var('U',lev=850)
# add_var('U',lev=200)


dy = 4
lat1,lat2 = 40-dy,40+dy
lon1,lon2 = 360-120,360-80

dlon = 2

iyr = 3
# htype,years,months,first_file,num_files = 'h1',[],[],365*iyr+152,90 # JJA
htype,years,months,first_file,num_files = 'h1',[],[],365*iyr+152,3 # for testing
# months = np.arange(1,1+1,1)

fig_file = os.getenv('HOME')+'/Research/E3SM/figs_diurnal/diurnal.snapshot.v1'

plot_diff = False

print_rmse  = False
print_stats = False

chk_significance = False # use with plot_diff

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_pvar = len(pvar)
num_case = len(case)

wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.2
res.xyLineThicknessF = 8

if 'clr' not in vars(): 
   if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   else : clr = ['black']

# if num_case>1 and 'dsh' not in vars(): dsh = np.arange(0,num_case,1)
if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]
res.xyLineColors   = clr
res.xyDashPatterns = dsh
res.tiXAxisString = 'Longitude'

class tcolor:
   ENDC,RED,GREEN,YELLOW,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[33m','\033[35m','\033[36m'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
msg_list = []
for v in range(num_pvar):
   print('  var: '+hc.tcolor.MAGENTA+pvar[v]+hc.tcolor.ENDC)
   data_list,std_list,cnt_list = [],[],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      case_obj = he.Case( name=case[c] )

      case_obj.mirror_equator = True
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
      if 'lon1' in vars() : case_obj.lon1,case_obj.lin2 = lon1,lon2
      if 'lev'  in vars() : case_obj.lev  = lev

      lon  = case_obj.load_data('lon',  htype=htype,num_files=1)
      print(); print(np.min(lon.values))
      print(); print(np.max(lon.values))
      exit()
      area = case_obj.load_data('area', htype=htype,num_files=1).astype(np.double)
      data = case_obj.load_data(pvar[v],htype=htype,years=years,months=months,
                                first_file=first_file,num_files=num_files)

      hc.print_time_length(data.time,indent=(' '*6))

      # if print_stats: 
      #    msg = hc.print_stat(data,name=pvar[v],stat='naxsh',indent=(' '*6),compact=True)
      #    msg_list.append('  case: '+case[c]+'\n'+msg)
      #    if 'area' in vars() :
      #       gbl_mean = ( (data*area).sum() / area.sum() ).values 
      #       print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

      # if print_rmse:
      #    if c==0:baseline = data
      #    if c>0:
      #       rmse = np.sqrt( np.mean( np.square( data.to_masked_array() - baseline.to_masked_array() )))
      #       print(f'      Root Mean Square Error    : {rmse:6.4}')
      #       exit()
      #-------------------------------------------------------------------------
      # Calculate time and zonal mean
      #-------------------------------------------------------------------------
      bin_ds = hc.bin_YbyX( data, lon, keep_time=True,
                            bin_min=np.min(lon.values)+dlon,
                            bin_max=np.max(lon.values)-dlon,
                            bin_spc=dlon, wgt=area )

      print(); print(bin_ds); print()
      exit()
      data_list.append( bin_ds['bin_val'].values )
      
      std_list.append( bin_ds['bin_std'].values )
      cnt_list.append( bin_ds['bin_cnt'].values )

      lon_bins = bin_ds['bins'].values

   #----------------------------------------------------------------------------
   # Take difference from first case
   #----------------------------------------------------------------------------
   if plot_diff :
      data_tmp = data_list
      data_baseline = data_list[0]
      for c in range(num_case): data_list[c] = data_list[c] - data_baseline
   # #----------------------------------------------------------------------------
   # # Check significance using t-test
   # # https://stattrek.com/hypothesis-test/difference-in-means.aspx
   # #----------------------------------------------------------------------------
   # if plot_diff and chk_significance :
   #    for c in range(1,num_case):

   #       N0,N1 = cnt_list[0],cnt_list[c]
   #       # using number of months for N might make more sense?
   #       if num_files>0: N0,N1 = num_files,num_files  
   #       if len(years)>0: N0,N1 = len(years)*12,len(years)*12
   #       S0,S1 = std_list[0],std_list[c]
         
   #       # Standard error
   #       SE = np.sqrt( S0**2/N0 + S1**2/N1 )

   #       hc.print_stat(SE,name='SE',indent='    ')

   #       # Degrees of freedom
   #       DF = (S0**2/N0 + S1**2/N1)**2       \
   #           /( ( (S0**2/N0)**2 / (N0-1) )   \
   #             +( (S1**2/N1)**2 / (N1-1) ) )

   #       # t-statistic - aX is the difference now
   #       t_stat = data_list[c] / SE

   #       hc.print_stat(t_stat,name='t statistic',indent='    ')

   #       # Critical t-statistic
   #       t_crit = 2.24   # 2-tail test w/ inf dof & P=0.05

   #       for i in range(len(lon_bins)):
   #          msg = f'  lon: {lon_bins[i]}   t_stat: {t_stat[i]}   '
   #          if np.absolute(t_stat[i])>t_crit: msg = msg+tcolor.RED+'SIGNIFICANT'+tcolor.ENDC
   #          print(msg)

   #       # sig = new(dimsizes(aX),float)
   #       # sig = where(abs(t_stat).gt.t_crit,1,0)
   #       # copy_VarCoords(aX,sig)
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   unit_str = ''
   if pvar[v] in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
   if pvar[v] in ['LHFLX','SHFLX']           : unit_str = '[W/m2]'
   if pvar[v] in ['TGCLDLWP','TGCLDIWP']     : unit_str = '[kg/m2]'
   res.tiYAxisString = unit_str

   # lon_tick = np.array([-90,-60,-30,0,30,60,90])
   # res.tmXBMode = "Explicit"
   # res.tmXBValues = np.sin( lon_tick*3.14159/180. )
   # res.tmXBLabels = lon_tick

   # make a copy to double length of longitude
   nlon = len(lon_bins)
   tmp_bins = np.empty(nlon*2)
   tmp_bins[0:nlon] = lon_bins-np.max(lon_bins)
   tmp_bins[nlon:]  = lon_bins+dlon
   lon_bins = tmp_bins
   for c in range(0,num_case):
      tmp_data = np.empty(nlon*2)
      tmp_data[0:nlon] = data_list[c]
      tmp_data[nlon:]  = data_list[c]
      data_list[c] = tmp_data
   res.tmXBMode = "Explicit"
   lon_tick_vals = [-180,-270,-90,0,90,270,180]
   res.tmXBValues = np.array(lon_tick_vals)
   res.tmXBLabels = lon_tick_vals

   # res.trXMinF = np.min( lon_bins )
   # res.trXMaxF = np.max( lon_bins )

   res.trXMinF = -180
   res.trXMaxF =  180

   plot.append( ngl.xy(wks, lon_bins, np.ma.masked_invalid(  np.stack(data_list) ), res) )

   var_str = pvar[v]
   if pvar[v]=='PRECT' : var_str = 'Precipitation'
   if pvar[v]=='TGCLDLWP' : var_str = 'Cloud Liq. Water Path'
   
   cstr = ''
   # degree_sym = '~S~o~N~'
   # if case_obj.mirror_equator:
   #    cstr = f'{lat1}-{lat2}{degree_sym} (both hemispheres)'
   # else:
   #    cstr = f'{lat1}:{lat2}N'
   hs.set_subtitles(wks, plot[v], '', cstr, var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [num_pvar,1]
# layout = [np.ceil(len(plot)/2),2]
# layout = [1,num_pvar]
# if num_pvar==4 : layout = [2,2]
# if num_pvar==6 : layout = [3,2]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

# print()
# for msg in msg_list: print(msg)

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------