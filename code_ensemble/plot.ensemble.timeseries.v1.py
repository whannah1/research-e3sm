import os, copy, ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs

case = [
      # 'E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.FC5AV1C-L.base',
      # 'E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.FC5AV1C-L.test',
      # 'E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.FC5AV1C-L.pertlim-max_1e-10',
      # 'E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.FC5AV1C-L.pertlim-max_0.1',
      'E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.F-MMF1.base',
      'E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.F-MMF1.test',
      ]
# name = ['ne4pg2 FC5AV1C-L']
clr = ['red','green','blue']

var = ['PRECT','T850']
# var = ['PRECT','T850','Q850','U850']
# var = ['FSNT','FLNT','FLNS','FSNS','TMQ','PRECT','LHFLX','SHFLX','TGCLDLWP','TGCLDIWP']
# var = ['UBOT','VBOT','TBOT','QBOT']
# var = ['Z100','Z500','Z700','T500','T850','Q850']
# var = ['U200','U850','V200','V850','OMEGA500','OMEGA850']

# lev = -70  # -58 ~ 860 hPa / -45 ~ 492 hPa / 

# clat,clon, dx = 36, 360-97, 5 # ARM SGP
# lat1,lat2,lon1,lon2 = clat-dx,clat+dx,clon-dx,clon+dx    
lat1,lat2,lon1,lon2 = 30,50,360-125,360-75    # CONUS
# lat1,lat2,lon1,lon2 = -30,30,60,120           # IO
# lat1,lat2,lon1,lon2 = -30,30,120,180          # WP

htype,years,num_files = 'h1',[],0

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_ensemble/ensemble.timeseries.v1'

print_stats = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

ninst = [None]*num_case

if 'name' not in vars(): name = case
if 'clr' not in vars(): clr = ['black']*num_case
if 'dsh' not in vars(): dsh = np.zeros(num_case)

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_var
res = hs.res_xy()
res.vpHeightF = 0.5
res.tmYLLabelFontHeightF         = 0.02
res.tmXBLabelFontHeightF         = 0.02
res.tiXAxisFontHeightF           = 0.02
res.tiYAxisFontHeightF           = 0.02

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   time_list,data_list,lat_list = [],[],[]
   for c in range(num_case):
      print('    case: '+case[c])
      # parse case name to get number of instances
      for s in case[c].split('.'):
         if 'ENS_' in s: ninst[c] = int(s.replace('ENS_',''))
      if ninst[c] is None: raise ValueError('Case name must include \'ENS_N\' where N is the number of instances')
      case_obj = he.Case( name=case[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1 = lat1 ; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1 ; case_obj.lon2 = lon2

      for i in range(ninst[c]):

         data = case_obj.load_data(var[v],component=f'cam_{(i+1):04}',htype=htype,years=years,num_files=num_files,lev=lev)
         area = case_obj.load_data('area',component=f'cam_{(i+1):04}',htype=htype,years=years,num_files=num_files).astype(np.double)

         # Convert to daily mean
         data = data.resample(time='D').mean(dim='time')

         # hc.print_stat(data)
         if i==0: hc.print_time_length(data.time)
         if print_stats: hc.print_stat(data,stat='nxh',compact=True,indent='    ',name='')
         

         avg_data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )

         # Make time start at zero to avoid seg fault
         avg_data['time'] = ( avg_data['time'] - avg_data['time'][0] ).astype('float') / 86400e9

         data_list.append( avg_data.values )
         time_list.append( avg_data['time'].values )
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.tiXAxisString = 'Time [days]'

   # Make sure plot bounds are consistent
   tres.trYMinF = np.min(np.stack(data_list)) - np.std(np.stack(data_list))*0.5
   tres.trYMaxF = np.max(np.stack(data_list)) + np.std(np.stack(data_list))*0.5
   tres.trXMinF = np.min(np.stack(time_list))
   tres.trXMaxF = np.max(np.stack(time_list))
   
   cnt = 0
   for c in range(num_case):
      tres.xyLineColor = clr[c]
      tres.xyDashPattern = dsh[c]
      for i in range(ninst[c]):
         tplot = ngl.xy(wks, time_list[cnt], data_list[cnt], tres)
         cnt += 1
         if c==0 and i==0 :
            plot[v] = tplot
         else:
            ngl.overlay( plot[v], tplot )

   #----------------------------------------------------------------------------
   # Set strings at top of plot
   #----------------------------------------------------------------------------
   var_str = var[v]
   # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
   # if var[v]=="TMQ"   : var_str = "Column Water Vapor [mm]"

   hs.set_subtitles(wks, plot[v], '', var_str, '', font_height=0.015)

   #-----------------------------------------------------------------------------
   # Ad legend
   #-----------------------------------------------------------------------------
   lgres = ngl.Resources()
   lgres.vpWidthF, lgres.vpHeightF  = 0.08, 0.08  
   lgres.lgLabelFontHeightF = 0.015
   lgres.lgLineThicknessF   = 4
   lgres.lgMonoLineColor    = False
   # lgres.lgMonoDashIndex    = True
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh
   lgres.lgLabelJust    = 'CenterLeft'
   # pid = ngl.legend_ndc(wks, len(name), name, 0.5, 0.1, lgres)  # 3x2

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [1,len(plot)]
layout = [np.ceil(len(plot)/2),2]

if num_var==1 and num_case==4 : layout = [2,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------