# plot FFT coef for 2*dt oscillations
import os, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string, xrft 
host = hc.get_host()
data_dir,data_sub = None,None
# data_dir,data_sub = '/project/projectdirs/m3312/jlee1046/E3SM/',''
var,lev_list = [],[]
def add_var(var_name,lev=-1): var.append(var_name); lev_list.append(lev)
#-------------------------------------------------------------------------------
   
### ???
name,case = [],[]
case.append('');name.append('E3SM')

#-------------------------------------------------------------------------------

# add_var('PRECT')
# add_var('TMQ')
# add_var('LHFLX')
# add_var('SHFLX')
# add_var('TAUX');add_var('U',lev=-71)
# add_var('TAUY');add_var('V',lev=-71)

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_fft/fft.map.v1'

# lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = -45,-45+60,360-120,360-40  # S. America

# htype,years,months,first_file,num_files = 'h0',[],[],0,1
htype,years,months,first_file,num_files = 'h2',[],[],1,-5

use_remap,remap_grid = False,'180x360' # 90x180 / 180x360

plot_diff,add_diff = False,False
diff_base = 0
# diff_case = [1,2]

chk_significance  = False
print_stats = False

var_x_case = False
num_plot_col = 2

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.02 - 0.0012*num_var - 0.0014*(num_case+int(add_diff))
subtitle_font_height = max(subtitle_font_height,0.005)

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
if 'lev' not in vars(): lev = np.array([0])

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1, res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1, res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.018

# res.mpGeophysicalLineColor = 'white'

res.tmXBOn = False
res.tmYLOn = False

### Sterographic Polar View
# res.mpProjection         = 'Stereographic'
# res.mpEllipticalBoundary = True
# res.mpCenterLatF         = -90.
# res.mpLimitMode          = 'Angles'
# res.mpBottomAngleF       = 50
# res.mpLeftAngleF         = 50
# res.mpRightAngleF        = 50
# res.mpTopAngleF          = 50

# res.mpLimitMode = "LatLon" 
# res.mpMinLatF   = 45 -15
# res.mpMaxLatF   = 45 +15
# res.mpMinLonF   = 180-15
# res.mpMaxLonF   = 180+15

def get_comp(case):
   comp = 'eam'
   if 'RGMA' in case: comp = 'cam'
   if 'CESM' in case: comp = 'cam'
   if 'MAML' in case: comp = 'eam_0001'   
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   std_list,cnt_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_sub_tmp = data_sub
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir, data_sub=data_sub_tmp )

      # case_obj.time_freq = 'daily'
      # case_obj.time_freq = 'monthly'
      # case_obj = he.Case( name=case[c], data_dir='/global/cscratch1/sd/hewang/acme_scratch/cori-knl/' )

      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      case_obj.set_coord_names(tvar)

      if case[c]=='ERAi':
         num_files = 0
         years = [0,1,2,3]
         mn = [1,2,3,4,5,6,7]

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      # if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      if use_remap or ('CESM' in case[c] and 'ne30' not in case[c]):
         lat = case_obj.load_data('lat',component=get_comp(case[c]),htype=htype,
                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
         lon = case_obj.load_data('lon',component=get_comp(case[c]),htype=htype,
                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      # elif case_obj.obs:
      #    lat = case_obj.load_data('lat',component=comp,htype=htype)
      #    lon = case_obj.load_data('lon',component=comp,htype=htype)
      else:
         aname = case_obj.area_name
         area = case_obj.load_data(aname,component=get_comp(case[c]),htype=htype)

      if case[c]=='ERAi':
         num_files = 0
         years = [0,1,2,3]
         mn = [1,2,3,4,5,6,7]

      data = case_obj.load_data(tvar, component=get_comp(case[c]),htype=htype,ps_htype=htype,
                                      years=years,months=months,lev=lev,
                                      first_file=first_file,num_files=num_files,
                                      use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      
      if case[c]=='ERAi': erai_lat,erai_lon = data['lat'],data['lon']

      ### accomodate difference in file output freq
      if htype=='h1' and case[c]=='E3SM.ne30pg2_r05_oECv3.F-MMF1.201223.RRTMGP.crm64.nrad16.00':
         data = data.isel(time=slice(int(first_file*24/3),int(num_files*24/3)))

      if var[v]=='TIMINGF': data = data.where(data!=0,drop=True)

      # Special handling of CRM grid variables
      if 'crm_nx' in data.dims : data = data.mean(dim=('crm_nx','crm_ny')).isel(crm_nz=15)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      ### calculate second derivative to focus on oscillations
      # print(hc.tcolor.RED+'WARNING - calculating second derivative!!!'+hc.tcolor.ENDC)
      # ntime = len(data['time'].values)
      # data.load()
      # data_alt = xr.zeros_like(data)
      # data_alt[1:ntime-1,:] = data[0:ntime-2,:].values - 2*data[1:ntime-1,:].values + data[2:ntime,:].values
      # data = data_alt

      ### plot FFT coef for 2*dt oscillations
      print(hc.tcolor.RED+'WARNING - calculating power spectrum!!!'+hc.tcolor.ENDC)
      data.load()
      data_fft = xrft.power_spectrum(data,dim=['time']).compute()
      data = data_fft.isel(freq_time=0)

      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)   
         if chk_significance :
            std_list.append( data.std(dim='time').values )
            cnt_list.append( float(len(data.time)) )
         data = data.mean(dim='time')

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      # Calculate area weighted global mean
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

      # # Calculate RMSE
      # if c==0:baseline = data
      # if c>0:
      #    rmse = np.sqrt( np.mean( np.square( data.to_masked_array() - baseline.to_masked_array() )))
      #    print(f'      Root Mean Square Error    : {rmse:6.4}')

      if case[c]=='TRMM' and 'lon1' not in locals(): 
         data_list.append( ngl.add_cyclic(data.values) )
      else:
         data_list.append( data.values )

      if 'area' in locals() : area_list.append( area.values )
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Check significance using t-test - https://stattrek.com/hypothesis-test/difference-in-means.aspx
   #------------------------------------------------------------------------------------------------
   if plot_diff and chk_significance :
      print('\n  Checking significance of differences...')
      sig_list = []
      for c in range(1,num_case):
         t_crit=1.96  # 2-tail test w/ inf dof & P=0.05
         t_stat = calc_t_stat(D0=data_list[0],D1=data_list[c],
                              S0=std_list[0],S1=std_list[c],
                              N0=cnt_list[0],N1=cnt_list[c],t_crit=t_crit)

         sig_list.append( np.where( np.absolute(t_stat) > t_crit, 1, 0 ) )

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir, data_sub=data_sub )
      case_obj.set_coord_names(tvar)

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['P-E']                      : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = "CBR_wet"
      if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['DYN_QLIQ']                 : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['TS']                       : tres.cnFillPalette = "BlueRed"
      if var[v] in ['U','V','UBOT','VBOT','U850','V850','U200','V200',
                    'MMF_CVT_TEND_T','MMF_CVT_TEND_Q']: 
         tres.cnFillPalette = "BlueWhiteOrangeRed"
         # tres.cnFillPalette = 'MPL_RdYlBu'

      if calculate_FFT:
         print(hc.tcolor.RED+'WARNING - setting data limits based on std deviation!!!'+hc.tcolor.ENDC)
         data_min = 0#-2*np.std(data_list[0])
         data_max = 0+2*np.std(data_list[0])
         tres.cnFillPalette = "MPL_viridis"

      if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(2,20+2,2)
      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.logspace( -2, 1.31, num=60).round(decimals=2)
      # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      if var[v]=='P-E'                 : tres.cnLevels = np.linspace(-10,10,21)
      # if var[v]=="TS"                  : tres.cnLevels = np.arange(0,40+2,2)
      if var[v]=="RH"                  : tres.cnLevels = np.arange(10,100+1,1)
      if var[v]=="TGCLDIWP"            : tres.cnLevels = np.arange(1,30+1,1)*1e-2
      if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2)
      if var[v]=="DYN_QLIQ"            : tres.cnLevels = np.logspace( -6, -4, num=40)
      if var[v]=="CLDLIQ"              : tres.cnLevels = np.logspace( -7, -3, num=60)
      if var[v]=='SPTLS'               : tres.cnLevels = np.linspace(-0.001,0.001,81)
      if var[v]=="PHIS"                : tres.cnLevels = np.arange(-0.01,0.01+0.001,0.001)
      if 'MMF_MC' in var[v]            : tres.cnLevels = np.linspace(-1,1,21)*0.01
      if 'OMEGA' in var[v]             : tres.cnLevels = np.linspace(-1,1,21)*0.6

      if var[v] in ['MMF_DU','MMF_DV','ZMMTU','ZMMTV','uten_Cu','vten_Cu']:
         tres.cnLevels = np.arange(-20,20+1,1)

      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 21
         aboutZero = False
         if var[v] in ['SPTLS','SPQTLS','U','V','VOR','DIV',
                       'U850','V850','U200','V200',
                       'MMF_CVT_TEND_T','MMF_CVT_TEND_Q',] : 
            aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      var_str = var[v]
      if var[v]=='PRECT':     var_str = 'Precipitation'
      if var[v]=='TMQ':       var_str = 'CWV'
      if var[v]=='TGCLDLWP':  var_str = 'LWP'
      if var[v]=='TGCLDIWP':  var_str = 'IWP'
      # if 'lev' in locals() and  var_str = f'{lev} mb {var[v]}'

      lev_str = None
      if lev>0: lev_str = f'{lev}mb'
      if lev<0: lev_str = f'k={(lev*-1)}'
      if lev_str is not None and var[v] in ['U','V','OMEGA','T','Q','Z3']:
         var_str = f'{lev_str} {var[v]}'
      # if var[v]=='U':         var_str = f'{lev_str} Zonal Wind'
      # if var[v]=='V':         var_str = f'{lev_str} Meridional Wind'
      # if var[v]=='OMEGA':     var_str = f'{lev_str} Omega'

      if 'RCE' in case[c] or 'AQP' in case[c] :
         tres.mpOutlineBoundarySets = 'NoBoundaries'
         tres.mpCenterLonF = 0.
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_remap \
         or ('CESM' in case[c] and 'ne30' not in case[c]) \
         or case[c] in ['ERAi']:
         if case[c]=='ERAi': lat,lon = erai_lat,erai_lon
         hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
      else:
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype)
         
      if plot_diff : 
         if c==diff_base : base_name = name[c]
         # if c in diff_case : case_name = case_name+' - '+base_name      

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         #----------------------------------------------------------------------
         #----------------------------------------------------------------------
         ctr_str = ''
         if 'name' not in vars(): case_name = case_obj.short_name
         if 'name'     in vars(): case_name = name[c]
            
         # ctr_str = 'Mean: '+'%.2f'%gbl_mean+' [mm/day]'

         # if 'lev' in locals() :
         #    if type(lev) not in [list,tuple]:
         #       if lev>0: ctr_str = f'{lev} mb'

         hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         
         data_list[c] = data_list[c] - data_baseline.values

         # tres.cnFillPalette = 'ncl_default'
         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         # tres.cnFillPalette = 'MPL_bwr'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         # if var[v] in ['TGCLDLWP'] : tres.cnLevels = np.arange(-28,28+4,4)*1e-3
         # if var[v] in ['TGCLDIWP'] : tres.cnLevels = np.arange(-10,10+2,2)*1e-3
         if not hasattr(tres,'cnLevels') : 
            # if np.min(data_list[c]).values==np.max(data_list[c]).values : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
         
         ### override the level settings and just use auto
         # tres.cnLevelSelectionMode = "AutomaticLevels"

         if use_remap:
            hs.set_cell_fill(tres,case_obj=case_obj,use_remap=True,lat=lat,lon=lon)
         else:
            hs.set_cell_fill(tres,case_obj=case_obj)

         tres.lbLabelBarOn = True

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+1
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)

         #-----------------------------------
         ####################################
         # overlay stippling for significance
         ####################################
         #-----------------------------------
         if chk_significance:
            sres = res_stippling
            if use_remap:
               lat2D =               np.repeat( lat.values[...,None],len(lon),axis=1)
               lon2D = np.transpose( np.repeat( lon.values[...,None],len(lat),axis=1) )
               sres.sfXArray      = lon2D
               sres.sfYArray      = lat2D
            else:
               sres.sfXArray      = lon
               sres.sfYArray      = lat
            if np.sum(sig_list[c-1]) != 0:
               ngl.overlay( plot[ip], ngl.contour(wks,sig_list[c-1],sres) )
         #-----------------------------------
         ####################################
         #-----------------------------------
         ctr_str = ''
         # case_name = name[c]+' - '+base_name
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name
         
         # hs.set_subtitles(wks, plot[ipd], case_name, '', var_str+' (Diff)', font_height=subtitle_font_height)
         hs.set_subtitles(wks, plot[ipd], case_name, 'Difference', var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if plot_diff : num_case = num_case+len(diff_case)   # use this to plot both before and after diff

# title_str = None
# title_str = 'ANN'
# if months==[1,2,12]: title_str = 'DJF'
# if months==[6,7,8]: title_str = 'JJA'
# if title_str is not None:
#    textres =  ngl.Resources()
#    textres.txFontHeightF =  0.025
#    ngl.text_ndc(wks,title_str,0.5,.7,textres)

num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]

if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
