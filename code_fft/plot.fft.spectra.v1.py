import os, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string
import xrft 
host = hc.get_host()
data_dir,data_sub = None,None
# data_dir,data_sub = '/project/projectdirs/m3312/jlee1046/E3SM/',''
#-------------------------------------------------------------------------------
case,name,clr,dsh,mrk = [],[],[],[],[]
def add_case(case_in,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   case.append(case_in) ; dsh.append(d) ; clr.append(c) ; mrk.append(m)
   if n is None: n = '' ; name.append(n)

### flux debugging shit

case_head = 'E3SM.MMF-SFC-TEST.GNUGPU.ne30pg2_ne30pg2.F-MMFXX.RADNX_4'
# case.append(f'{case_head}.DT_1200.MMF_HB.HBML_30.01');                           name.append('MMF 10min');
# case.append(f'{case_head}.DT_1200.MMF_HB.HBML_30.flux_avg.01');                  name.append('MMF 10min+FA');
# case.append(f'{case_head}.DT_1200.MMF_HB.HBML_30.flux_avg.01');                  name.append('MMF 20min+FA');
# case.append(f'{case_head}.DT_1200.MMF_HB.HBML_30.BVT.GCM_DIFF.flux_avg.tms.01'); name.append('MMF 20min+BVT+TMS+DIFF');

add_case('E3SM.MMF-SFC-TEST.GNUGPU.ne30pg2_ne30pg2.F-MMFXX.RADNX_4.DT_1200.flux_avg.CTRL',             d=0,c='black', n='MMF old')
add_case('E3SM.MMF-SFC-TEST.GNUGPU.ne30pg2_ne30pg2.F-MMFXX.RADNX_4.DT_1200.MMF_HB.HBML_10.flux_avg.01',d=0,c='red',   n='MMF new')

# add_case(f'E3SM.SFC-TEST.GNUCPU.ne30pg2_ne30pg2.FC5AV1C-L.00',          d=0,c='black', n='L72')
# add_case(f'E3SM.SFC-TEST.GNUCPU.ne30pg2_ne30pg2.FC5AV1C-L.flux_avg.00', d=0,c='red',   n='L72+srf_flux_avg')
# add_case(f'E3SM.SFC-TEST.GNUCPU.ne30pg2_ne30pg2.FC5AV1C-L.alt_vgrid.00',d=0,c='green',n='L72 alt')
# add_case(f'E3SM.SFC-TEST.GNUCPU.ne30pg2_ne30pg2.FC5AV1C-L.NLEV_50.00',  d=1,c='blue', n='L50')


#-------------------------------------------------------------------------------

var,lev_list,landocn_list = [],[],[]
def add_var(var_name,lev=-1,land_only=False,ocean_only=False): 
   var.append(var_name); lev_list.append(lev)
   if not land_only and not ocean_only:
      landocn_list.append(None)
   else:
      if land_only and ocean_only: exit('what do you think you\'re doing?!?!')
      if land_only:  landocn_list.append('land')
      if ocean_only: landocn_list.append('ocean')

# add_var('PRECT')
# add_var('TGCLDLWP')
# add_var('TGCLDIWP')
# add_var('P-E')
# add_var('TS')
# add_var('LHFLX')
# add_var('SHFLX')
# add_var('TMQ')
# add_var('FSNS')
# add_var('FLNS')
# add_var('FSNTOA')
# add_var('NET_TOA_RAD')
# add_var('QBOT')
# add_var('WSPD',lev=-71)
# add_var('WSPD',lev=850)
# add_var('WSPD_BOT')
# add_var('TAUX')

# add_var('UBOT')
# add_var('UBOT',land_only=True)
# add_var('UBOT',ocean_only=True)

add_var('SHFLX')
add_var('WSPD_BOT')



fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_fft/fft.spectra.v1'


lat1,lat2 = -60,60


# htype,years,months,first_file,num_files = 'h0',[],[],0,1
htype,years,months,first_file,num_files = 'h1',[],[],0,30


use_remap,remap_grid = False,'180x360' # 90x180 / 180x360


# max_period = 2*24*3600
max_period = 24*3600

plot_diff = False

print_stats = True

# var_x_case = False

num_plot_col = 1

land_only = False
ocn_only = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

subtitle_font_height = 0.02 - 0.0015*num_var

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)
   
res = hs.res_xy()
res.vpHeightF = 0.2
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
# res.lbLabelFontHeightF           = 0.018

# res.tmXBOn = False
# res.tmYLOn = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):

   if landocn_list[v] is not None:
      if landocn_list[v]=='land' : land_only=True; ocn_only=False
      if landocn_list[v]=='ocean': land_only=False; ocn_only=True


   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   freq_list = []
   std_list,cnt_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]

   for c in range(num_case):

      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      comp = 'eam'
      if 'RGMA' in case[c]: comp = 'cam'
      if 'CESM' in case[c]: comp = 'cam'

      data_sub_tmp = data_sub
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      
      case_obj = he.Case( name=case[c], atm_comp=comp, data_dir=data_dir, data_sub=data_sub_tmp )

      # case_obj.time_freq = 'daily'
      # case_obj.time_freq = 'monthly'
      # case_obj = he.Case( name=case[c], data_dir='/global/cscratch1/sd/hewang/acme_scratch/cori-knl/' )

      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      case_obj.set_coord_names(tvar)
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2


      data = case_obj.load_data(tvar, component=comp,htype=htype,ps_htype=htype,
                                      years=years,months=months,lev=lev,
                                      first_file=first_file,num_files=num_files,
                                      use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      
      ### sanity checking stuff
      # print()
      # print(data)
      # print()
      # print(data['time'])
      # print()
      # print(data['time'][0].values)
      # print(data['time'][1].values)
      # print(data['time'][1].values - data['time'][0].values)
      # print()
      # exit()


      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      if land_only or ocn_only :
         land_frac = case_obj.load_data('LANDFRAC',htype='h0',years=years,months=months,
                                       first_file=first_file,num_files=num_files).astype(np.double)
         land_frac = land_frac.isel(time=0)
         if land_only: data = data*land_frac
         if ocn_only : data = data*(1-land_frac)


      # convert time coord to seconds
      # data['time'] = ( data['time'] - data['time'][0] ) / 1e9
      # print(data); print()

      ### calculate FFT
      data.load()
      data_fft = xrft.power_spectrum(data,dim=['time'], detrend='linear').compute()
      
      data = data_fft.mean(dim='ncol')
      # data = data_fft.isel(ncol=0)

      # hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')
      # hc.print_stat(data['freq_time'],name='freq',stat='naxsh',indent='    ')
      # exit()

      #-------------------------------------------------------------------------
      # sanity check for FFT output
      #-------------------------------------------------------------------------

      # if v==0 and c==0: 
      #    print()
      #    tdim = len(data['freq_time'])
      #    print_len = 6
      #    tstart_list = [ 0, int(tdim/2)-int(print_len/2), tdim-print_len ]
      #    for tt,tstart in enumerate( tstart_list ):
      #       for f,frq in enumerate(data['freq_time'][tstart:tstart+print_len].values): 
      #          if frq==0:
      #             period = 'inf'
      #          else:
      #             period = f'{(1/frq):8.3f}'
      #          ff = f+tstart
      #          print(f'  {ff}    {frq:8.4e}    {period}')
      #       if tt<len(tstart_list): print('...')
      #    # exit()

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      ### Calculate area weighted global mean
      # if 'area' in locals() :
      #    gbl_mean = ( (data*area).sum() / area.sum() ).values 
      #    print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

      nfreq = int( len(data['freq_time'].values)/2 )



      data = data[:nfreq]
      # data = data[int(nfreq/2):]
      data = data.reindex(freq_time=data.freq_time[::-1])

      # flip the sign of the frequency
      data['freq_time'] = -1 * data['freq_time']

      # limit the frequency range 
      if 'max_period' in locals():
         max_freq = 1/max_period
         max_freq_f = 0
         for f,freq in enumerate(data['freq_time'].values):
            if freq>max_freq:
               max_freq_f = f
               break
         data = data[max_freq_f:]


      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

      data_list.append( data.values )
      freq_list.append( data['freq_time'].values *86400. )

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   
   if plot_diff:
      for c in range(num_case): 
         if c==0: baseline = data_list[c]
         data_list[c] = data_list[c] - baseline

   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   freq_min = np.min([np.nanmin(f) for f in freq_list])
   freq_max = np.max([np.nanmax(f) for f in freq_list])

   res.xyYStyle = "Log"

   var_str = var[v]
   if var[v]=='PRECT':     var_str = 'Precipitation'
   if var[v]=='TMQ':       var_str = 'CWV'
   if var[v]=='TGCLDLWP':  var_str = 'LWP'
   if var[v]=='TGCLDIWP':  var_str = 'IWP'
   if 'lev' in locals() and var[v] in ['U','V']: var_str = f'{lev} mb {var[v]}'


   

   for c in range(num_case):
      # case_obj = he.Case( name=case[c], atm_comp=comp, data_dir=data_dir, data_sub=data_sub )
      # case_obj.set_coord_names(tvar)

      # ip = v*num_case+c if var_x_case else c*num_var+v
      ip = v
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      
      tres.trYMinF = data_min
      tres.trYMaxF = data_max

      tres.trXMinF = freq_min
      tres.trXMaxF = freq_max
      
      tres.xyLineColor   = clr[c]
      tres.xyMarkerColor = clr[c]
      tres.xyDashPattern = dsh[c]

      tplot = ngl.xy(wks, freq_list[c], data_list[c], tres) 

      if c==0 :
         plot[ip] = tplot
      else:
         ngl.overlay(plot[ip],tplot)

      #-------------------------------------------------------------------------
      # overlay line to indicate specific periods
      #-------------------------------------------------------------------------
      if v==0 and c==0:
         xres = hs.res_xy()
         xres.xyLineColor = 'gray'
         # xres.xyDashPattern = 1

      # for i,p in enumerate( np.array([1,2,3]) ):
      #    xres.xyDashPattern = i
      #    frq = 1/(p*3600)
      #    if c==0: print(f'    plotting marker for period,frequency: {p} hours , {frq} / s')
      #    xx = np.array([frq,frq])
      #    yy = np.array([-1e8,1e8])
      #    tplot = ngl.xy(wks, xx, yy, xres) 
      #    ngl.overlay(plot[ip],tplot)


      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

   ctr_str = ''
   lat_chk,lon_chk = 'lat1' in locals(), 'lon1' in locals()

   if not lat_chk and not lon_chk and not find_max_val_col: 
      ctr_str = 'Global'
   else:
      if lat_chk:      ctr_str += f' {lat1}:{lat2}N '
      if lon_chk:      ctr_str += f' {lon1}:{lon2}E '
   if land_only: ctr_str += ' Land Only '
   if ocn_only : ctr_str += ' Ocean Only '
   hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
