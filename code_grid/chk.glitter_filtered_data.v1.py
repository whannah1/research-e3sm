#-------------------------------------------------------------------------------
# v1 - initial exploration
# v2 - using kyle's filter factory, plot precip glitter metric 
# v3 - using kyle's filter factory, plot GLL node differential as function of # neighbors
#-------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import numba
home = os.getenv("HOME")
print()

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_clim/clim.map.v1"


case = "E3SM_RCEMIP_300K_ne30_SP1_64x1_1km_s1_00"
ifile = home+"/Data/E3SM_local/"+case+"/atm/"+case+".cam.h1.0001-01-10-00000.nc"

num_neighbors  = 32
find_neighbors = True

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
infile = xr.open_dataset(ifile)
lev = infile.variables["lev"].values
lat = infile.variables["lat"].values
lon = infile.variables["lon"].values
ncol = len(lat)
gll_area = infile.variables['area']
W = infile.variables['OMEGA'][0,:,:]
P = infile.variables['PRECT'][0,:]

# Adjust units
P.values = P.values*86400.*1e3
P.attrs['units'] = 'mm/day'

if "_ne30_" in case : grid = "ne30np4"
sfile = home+'/E3SM/data_grid/'+grid+'_scrip.nc'
if not os.path.isfile(sfile) : sfile = home+'/Research/E3SM/data_grid/'+grid+'_scrip.nc'
scripfile = xr.open_dataset(sfile)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
if "_ne30_" in case : edge_bin_min, edge_bin_max = 0.0001, 0.0003

W_gll_avg = np.empty([2,len(lev)])
W_gll_avg[0,:] = W.where( gll_area>edge_bin_max ).mean( dim=('ncol') )
W_gll_avg[1,:] = W.where( gll_area<edge_bin_min ).mean( dim=('ncol') )

#-------------------------------------------------------------------------------
# Find neighbors
#-------------------------------------------------------------------------------
neighbor_file_name = home+"/Research/E3SM/data_neighbors/"+ \
                     grid+"_nearest-neighbors_"+str(num_neighbors).zfill(2)+".nc"
if not os.path.isfile(neighbor_file_name) : find_neighbors = True
if find_neighbors:
   print("finding nearest neighbors...")
   neighbor_id = hc.find_nearest_neighbors_on_sphere(lat,lon,num_neighbors)
   neighbor_ds = xr.Dataset()
   neighbor_ds['neighbor_id'] = (('ncol','neighbors'), neighbor_id)
   # neighbor_ds = xr.Dataset({ 'neighbor_id': (['ncol','neighbors'],neighbor_id) })
   neighbor_ds.to_netcdf(path=neighbor_file_name,mode='w')
else:
   neighbor_ds = xr.open_dataset(neighbor_file_name)
   neighbor_id = neighbor_ds.variables['neighbor_id'].values

#-------------------------------------------------------------------------------
# Define function for smoothing
#-------------------------------------------------------------------------------
@numba.njit()
def smooth_njit(X,X_b,area,neighbor_id,nb_wgt,ncol,lev):
   for n in range(0,ncol) :
      nb_id = neighbor_id[:,n]
      nb_sum = 0.
      for g in range(0,num_neighbors): 
         nb_wgt[g] = area[g]
         nb_sum = nb_sum + nb_wgt[g]
      nb_wgt[:] = nb_wgt[:] / nb_sum
      for k in range(0,len(lev)):
         for g in range(0,num_neighbors):
            X_b[k,n] = X_b[k,n] + X[k,nb_id[g]] * nb_wgt[g]
   return X_b

# Brute force method
def smooth(X,area,neighbor_id):
   X_b = np.empty(X.shape)
   nb_wgt = np.empty([num_neighbors])
   for n in range(0,ncol) :
      nb_id = neighbor_id[:,n]
      nb_wgt = area[nb_id] / np.sum( area[nb_id] )
      for k in range(0,len(lev)):
         X_b[k,n] = sum( X[k,nb_id] * nb_wgt )
   return X_b

#-------------------------------------------------------------------------------
# Smooth by averaging nearest neighbors
#-------------------------------------------------------------------------------
print("Smoothing...")

W_b = W*0
nb_wgt = np.empty([num_neighbors])
smooth_njit(W.values,W_b.values,gll_area.values,neighbor_id,nb_wgt,ncol,lev)

# Brute force method
# W_b = smooth(W,gll_area,neighbor_id)

print("done.")

W_b_avg = np.empty([2,len(lev)])
W_b_avg[0,:] = W_b.where( gll_area>edge_bin_max ).mean( dim=('ncol') )
W_b_avg[1,:] = W_b.where( gll_area<edge_bin_min ).mean( dim=('ncol') )


W_p = W - W_b

W_p_avg = np.empty([2,len(lev)])
W_p_avg[0,:] = W_p.where( gll_area>edge_bin_max ).mean( dim=('ncol') )
W_p_avg[1,:] = W_p.where( gll_area<edge_bin_min ).mean( dim=('ncol') )

#-------------------------------------------------------------------------------
# Plot GLL averaged omega profiles
#-------------------------------------------------------------------------------
wks = ngl.open_wks(fig_type,fig_file)
res = ngl.Resources()
res.nglDraw  = False
res.nglFrame = False
res.tmYLMinorOn = False
res.tmXBMinorOn = False
res.xyLineThicknessF = 6
res.trYReverse = True
res.xyDashPatterns = [0,1]
# Create the profile plot
# plot = ngl.xy(wks,X.mean(dim='ncol').values,lev.values,res)
# plot = ngl.xy(wks,W_gll_avg,lev,res)

plot = []
plot.append( ngl.xy(wks,W_gll_avg,lev,res) )
plot.append( ngl.xy(wks,W_b_avg  ,lev,res) )
plot.append( ngl.xy(wks,W_p_avg  ,lev,res) )


# Add line at zero
res.xyDashPattern    = 0
res.xyLineThicknessF = 1
res.xyLineColor      = "black"
# ngl.overlay(plot, ngl.xy(wks,[0,0],[-1e8,1e8],res) )


ngl.panel(wks,plot[0:3],[1,3])

# ngl.draw(plot)
# ngl.frame(wks)
ngl.end()

#-------------------------------------------------------------------------------
# Plot map of precip
#-------------------------------------------------------------------------------
# wks = ngl.open_wks(fig_type,fig_file)
# res = ngl.Resources()
# res.cnFillOn         = True
# res.cnLinesOn        = False
# res.cnLineLabelsOn   = False
# res.lbOrientation    = "horizontal"
# res.mpGridAndLimbOn  = False
# res.cnFillPalette    = "WhiteBlueGreenYellowRed"
# res.mpProjection     = "Satellite"
# res.mpPerimOn        = False
# res.cnFillMode       = "CellFill"
# res.sfXArray         = scripfile.variables['grid_center_lon'].values
# res.sfYArray         = scripfile.variables['grid_center_lat'].values
# res.sfXCellBounds    = scripfile.variables['grid_corner_lon'].values
# res.sfYCellBounds    = scripfile.variables['grid_corner_lat'].values
# res.mpOutlineBoundarySets = "NoBoundaries"
# res.mpFillOn              = False
# dc = 2
# if var=="PRECT" : res.cnLevels = np.arange(dc,50+dc,dc)
# if hasattr(res,'cnLevels') : res.cnLevelSelectionMode = "ExplicitLevels"
# plot = ngl.contour_map(wks,X,res)
# ngl.end()
# cg.trim_png(fig_file)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------