#-------------------------------------------------------------------------------
# v1 - initial exploration
# v2 - using kyle's filter factory, plot precip glitter metric 
# v3 - using kyle's filter factory, plot GLL node differential as function of # neighbors
#-------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
home = os.getenv("HOME")
print()

### Glitter analysis cases
cases = ['E3SM_TEST_GPU_SP1_ne30_64x1_4km_00'\
        # ,'E3SM_TEST_GPU_SP1_ne30_64x1_1km_00'\
        ]

var = ['PRECT']

# NN = np.arange(0,8+1,2)
NN = np.array([0,4,8,12,16])
# NN = np.array([0,8])

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_grid/chk.glitter_filtered_data.v2"


#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
wks = ngl.open_wks(fig_type,fig_file)
plot = []

res = hs.res_xy()
res.xyLineThicknessF       = 12
res.xyMarkLineMode         = "MarkLines"
res.xyMarker               = 16
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01

if "ne4"   in cases[0] : area_bins = [ 0.0000, 0.006000, 0.015000, 0.030000 ]
if "ne30"  in cases[0] : area_bins = [ 0.0000, 0.000100, 0.000300, 0.000600 ]
if "ne120" in cases[0] : area_bins = [ 0.0000, 0.000007, 0.000015, 0.000050 ]

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_v = len(var)
num_c = len(cases)
for v in range(num_v):
   #----------------------------------------------------------------------------
   # Loop through cases to load data without plotting
   #----------------------------------------------------------------------------
   for c in range(num_c):
      case_obj = he.Case( name=cases[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      data = case_obj.load_data(var[v]).astype(np.double)
      area = case_obj.load_data("area").astype(np.double)
      lat  = case_obj.load_data('lat').values.astype(np.double)
      lon  = case_obj.load_data('lon').values.astype(np.double)
      # lev  = data['lev' ]
      time = data['time']

      for n in range(len(NN)) :
         #-------------------------------------------------------------------------
         # filter the data
         #-------------------------------------------------------------------------
         data_filtered = data.copy(data=data)
         if n>0 :
            neighbor_obj = ff.Neighbors( lat, lon, NN[n] )
            filter_opts = {} 
            filter_opts['sigma'] = 100.0
            filter_type = 'gauss'  # gauss / boxcar
            Filter = ff.filter_factory(neighbor_obj, area.values[0,:], filter_opts, filter_type) 
            for t in range(len(time)):
               # for k in range(len(lev)):
               #    data_filtered[t,k,:] = Filter.filter( data[t,k,:].values )
               data_filtered[t,:] = Filter.filter( data[t,:].values )

         #-------------------------------------------------------------------------
         # bin the data by cell area
         #-------------------------------------------------------------------------   
         bin_ds = hc.bin_YbyX( data_filtered, area, area_bins, bin_mode="explicit" )

         X = bin_ds['bin'].values
         Y = bin_ds['bin_val'].values

         print('n: '+str(n)+'   NN: '+str(NN[n]) )
         print(Y)
         print()

         #-------------------------------------------------------------------------
         # Create plot
         #-------------------------------------------------------------------------
         res.tiXAxisString = ''
         res.tiYAxisString = ''
         # res.xyLineColor   = clr[c]
         # res.xyMarkerColor = clr[c]
         res.tmXBMode      = "Explicit"
         res.tmXBLabels    = ["Corner","Edge","Center"]
         res.tmXBValues    = X

         # res.xyDashPattern = n
         res.xyMarkers     = ngl.NhlNewMarker(wks, str(NN[n]) , 22, 0.0, 0.0, 1.3125, 1.5, 0.0)

         tplot = ngl.xy(wks,X,Y,res)
      
         if n==0 :
            res.tiYAxisString = var[v]
            plot.append( tplot )
         else:
            ngl.overlay(plot[v*num_c+c],tplot)
   

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------