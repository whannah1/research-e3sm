#-------------------------------------------------------------------------------
# v1 - initial exploration
# v2 - using kyle's filter factory, plot precip glitter metric 
# v3 - using kyle's filter factory, plot GLL node differential as function of # neighbors
#-------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
home = os.getenv("HOME")
print()

### Early Science
case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']

### Glitter analysis cases
# case = ['E3SM_TEST_GPU_SP1_ne30_64x1_4km_00' \
#        ,'E3SM_TEST_GPU_SP1_ne30_64x1_1km_00' ]
# clr = ['red','blue','red','blue']
# dsh = [0,0,1,1]

var = ['PRECT']
# var,lev = ['OMEGA'],500
# var,lev = ['PRECT','OMEGA'],500


NN = np.arange(2,32+1,1)
# NN = np.array([0,4,8,12,16])

fig_file = home+"/Research/E3SM/figs_grid/chk.glitter_filtered_data.v3"

# months = [1]
num_years = 1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = len(var)
num_case = len(case)

wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF              = np.float32(0.4)
res.xyLineThicknessF       = 8
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.tiXAxisFontHeightF     = 0.015
res.tiYAxisFontHeightF     = 0.015
res.tmXMajorGrid           = True


if 'clr' not in vars(): 
   if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   else : clr = ['black']

# if num_case>1 and 'dsh' not in vars(): dsh = np.arange(0,num_case,1)
if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]
res.xyLineColors   = clr
res.xyDashPatterns = dsh

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+var[v])
   data_list = []
   stat_list = []
   #----------------------------------------------------------------------------
   # Loop through cases to load data without plotting
   #----------------------------------------------------------------------------
   for c in range(num_case):
      print('    case: '+case[c])

      area_bins = he.get_GLL_area_bins(case[c])
      
      case_obj = he.Case( name=case[c] )

      if 'lev' in vars(): case_obj.lev = lev

      filter_opts = {} 
      filter_opts['sigma'] = 100.0
      filter_type = 'boxcar'  # gauss / boxcar

      # reg_coeff = np.empty(len(NN))
      differential = np.empty(len(NN))
      significance = np.zeros(len(NN))
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      
      ### Use hourly data
      # case_obj.hist_path = '/project/projectdirs/m3312/crjones/e3sm/'+case_obj.name+'/hourly_2d_hist/'
      # data = case_obj.load_data(var[v],htype='h1')[:24,:]
      # case_obj.hist_path = '/project/projectdirs/m3312/crjones/e3sm/'+case_obj.name+'/'

      data = case_obj.load_data(var[v],htype='h0')
      # data = case_obj.load_data(var[v],htype='h0',num_years=num_years)
      # data = case_obj.load_data(var[v],htype='h0',num_years=num_years,months=months)
      area = case_obj.load_data("area").astype(np.double)
      lat  = case_obj.load_data('lat').values.astype(np.double)
      lon  = case_obj.load_data('lon').values.astype(np.double)
      # lev  = data['lev' ]
      time = data['time']

      # Set string to describe variable
      if c==0 :
         if 'lev' in data.coords : 
            var_str = str(data.coords['lev'].values)+"mb "+var[v]
         else:
            var_str = var[v]
         if var[v]=='PRECT' : var_str = 'Precipitation'

      for n in range(len(NN)) :
         #----------------------------------------------------------------------
         # filter the data
         #----------------------------------------------------------------------
         data_filtered = data.copy(data=data)
         if n>0 :
            neighbor_obj = ff.Neighbors( lat, lon, NN[n] )
            Filter = ff.filter_factory(neighbor_obj, area.values, filter_type, filter_opts) 
            for t in range(len(time)):
               # for k in range(len(lev)):
               #    data_filtered[t,k,:] = Filter.filter( data[t,k,:].values )
               data_filtered[t,:] = Filter.filter( data[t,:].values )

         #----------------------------------------------------------------------
         # Diagnose glitter by regressing data onto grid area
         #----------------------------------------------------------------------
         ### Numpy method - slow
         # a = np.linalg.lstsq( area.values, data_filtered.values, rcond=-1)
         # a = np.linalg.solve( area.values, data_filtered.values )

         ### alternative method - slow
         # linear_regressor = LinearRegression()
         # linear_regressor.fit( area.values, data_filtered.values )

         # reg = LinearRegression().fit(X, y)
         # reg.score(X, y)
         # print(linear_regressor)

         ### manual method - simple and fast
         # a = np.cov( area.values.flatten(), data_filtered.values.flatten() )[1,0] / np.var( area.values )
         
         # reg_coeff[n] = a 

         # print("n: "+str(n)+"  a: "+str(a))
         #----------------------------------------------------------------------
         # Diagnose glitter using difference of node global averages
         #----------------------------------------------------------------------
         bin_ds = hc.bin_YbyX( data_filtered, area, area_bins, bin_mode="explicit" )
         differential[n] = bin_ds['bin_val'].values[0] - bin_ds['bin_val'].values[2]

         # Sample sizes and standard deviations
         n1 = bin_ds['bin_cnt'].values[0]
         n2 = bin_ds['bin_cnt'].values[2]
         s1 = bin_ds['bin_std'].values[0]
         s2 = bin_ds['bin_std'].values[2]

         # Degrees of freedom
         dof = (s1**2/n1 + s2**2/n2)**2 / ( (s1**2/n1)**2/(n1-1) + (s2**2/n2)**2/(n2-1) )

         # standard error
         se = np.sqrt( (s1**2/n1) + (s2**2/n2) )

         # t-statistic
         tval = differential[n] / se

         # Critical t-statistic for significance
         tval_crit = 1.9599   # 2-tail test w/ inf dof & P=0.5

         if np.abs(tval)<tval_crit : significance[n] = 1

         #----------------------------------------------------------------------
         #----------------------------------------------------------------------

      # data_list.append( reg_coeff )
      data_list.append( differential )
      stat_list.append( significance )

   # Stack the data to make sure axes are set correctly
   X = np.array(NN,dtype=np.float)
   Y = np.stack(data_list)
   S = np.stack(stat_list)
   
   res.trYMaxF = np.max([ 0., np.max(Y)+np.std(Y)*0.1 ])
   res.trYMinF = np.min([ 0., np.min(Y)-np.std(Y)*0.1 ])

   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------
   for c in range(num_case):
      res.tiXAxisString = '# Neighbors'
      # res.tiYAxisString = 'Regression Coeff.'
      res.tiYAxisString = 'GLL Node Differential'
      if var[v]=='PRECT' : res.tiYAxisString = res.tiYAxisString+' [mm/day]'
      res.xyLineColor   = clr[c]
      res.xyMarkerColor = clr[c]
      res.xyDashPattern = dsh[c]
      res.tmXBMode      = "Explicit"
      res.tmXBValues    = X[::2]
      res.tmXBLabels    = res.tmXBValues
      res.xyMarkLineMode= "Lines"

      tplot = ngl.xy(wks,X,Y[c,:],res)
      if c==0 :
         plot.append( tplot )
      else:
         ngl.overlay(plot[v],tplot)      

      res.xyMarkLineMode= "Markers"
      res.xyMarkerColor = clr[c]
      res.xyMarkerSizeF = 0.01

      res.xyMarker      = 16
      ngl.overlay(plot[v],ngl.xy(wks,X, np.where(S[c,:]==1,Y[c,:],-999) ,res))
      # res.xyMarker      = 5   # X
      res.xyMarker      = 4   # O
      ngl.overlay(plot[v],ngl.xy(wks,X, np.where(S[c,:]==0,Y[c,:],-999) ,res))

   # Add horz line at 0   
   lres = hs.res_xy()
   lres.xyLineThicknessF = 1
   ngl.overlay(plot[v],ngl.xy(wks,X,X*0,lres))

   # hs.set_subtitles(wks, plot[v], var_str, '', ('filter_type='+filter_type), font_height=0.015)
   hs.set_subtitles(wks, plot[v], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [1,num_var]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------