#---------------------------------------------------------------------------------------------------
# v1 - Plot the zonal mean to check for drift signal
# v2 - plot time vs latitude drift of zonal mean for given model level
#---------------------------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
home = os.getenv("HOME")
print()

case  = ['E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_01']

# lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([10,30,50,100,150,200,300,400,500,600,700,800,850,925,975,1000])
lev = np.array([0])

var = ['T']

# years, months, num_files = [],[5,6,7],0

fig_file = home+"/Research/E3SM/figs_grid/grid.pg.drift.v1"

dlat = 5
lat1 = -85.
lat2 =  85.


#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.trYReverse = True
res.tiXAxisString = 'Latitude'
res.tiYAxisString = 'Pressure [hPa]'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('var: '+var[v])
   for c in range(num_case):
      print('  case: '+case[c])
      case_obj = he.Case( name=case[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------

      f0 = 1
      f1 = -1

      pvar = var[v]
      dvar = 'DYN_'+var[v]

      files = case_obj.get_hist_file_list(htype='h1')

      print()
      print('Files:')
      print('  '+files[f0])
      print('  '+files[f0])
      print()

      ds0 = xr.open_dataset( files[f0] )
      ds1 = xr.open_dataset( files[f1] )

      lat_p  = ds0['lat']
      lat_d  = ds0['lat_d']
      area_p = ds0['area_p']#[0,:]
      area_d = ds0['area_d']

      data_phy_0 = ds0[pvar].mean(dim='time')
      data_phy_1 = ds1[pvar].mean(dim='time')
      data_dyn_0 = ds0[dvar].mean(dim='time')
      data_dyn_1 = ds1[dvar].mean(dim='time')

      # print()
      # print(data_dyn_0)
      # print()
      # print(data_dyn_1)
      # print()
      # exit()

      # lat_p  = case_obj.load_data('lat'   ,htype='h1')
      # lat_d  = case_obj.load_data('lat_d' ,htype='h1')
      # area_p = case_obj.load_data('area'  ,htype='h1').astype(np.double)
      # area_d = case_obj.load_data('area_d',htype='h1').astype(np.double)
      # data_phy_0 = case_obj.load_data(pvar,htype='h1', num_files=f1,lev=lev ).isel(time=1)
      # data_phy_1 = case_obj.load_data(pvar,htype='h1', num_files=f2,lev=lev ).isel(time=-1)
      # data_dyn_0 = case_obj.load_data(dvar,htype='h1', num_files=f1,lev=lev ).isel(time=1)
      # data_dyn_1 = case_obj.load_data(dvar,htype='h1', num_files=f2,lev=lev ).isel(time=-1)

      lat_d = lat_d.rename({'ncol_d':'ncol'})
      area_d = area_d.rename({'ncol_d':'ncol'})
      data_dyn_0 = data_dyn_0.rename({'ncol_d':'ncol'})
      data_dyn_1 = data_dyn_1.rename({'ncol_d':'ncol'})

      #-------------------------------------------------------------------------
      # Calculate time and zonal mean
      #-------------------------------------------------------------------------
      print("calculating zonal mean of initial dyn")
      bin_ds_dyn_0 = hc.bin_YbyX( data_dyn_0, lat_d, bin_min=lat1, bin_max=lat2, bin_spc=dlat, wgt=area_d )
      
      print("calculating zonal mean of final dyn")
      bin_ds_dyn_1 = hc.bin_YbyX( data_dyn_1, lat_d, bin_min=lat1, bin_max=lat2, bin_spc=dlat, wgt=area_d )

      print("calculating zonal mean of initial phys")
      bin_ds_phy_0 = hc.bin_YbyX( data_phy_0, lat_p, bin_min=lat1, bin_max=lat2, bin_spc=dlat, wgt=area_p )
      
      print("calculating zonal mean of final phys")
      bin_ds_phy_1 = hc.bin_YbyX( data_phy_1, lat_p, bin_min=lat1, bin_max=lat2, bin_spc=dlat, wgt=area_p )
      

      lat_bins = bin_ds_phy_0['bins'].values

      D0 = np.ma.masked_invalid( (bin_ds_phy_0['bin_val']-bin_ds_dyn_0['bin_val']).transpose().values )
      D1 = np.ma.masked_invalid( (bin_ds_phy_1['bin_val']-bin_ds_dyn_1['bin_val']).transpose().values )

      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # if var[v] in ['OMEGA']  : tres.cnFillPalette = "BlueWhiteOrangeRed"
      # if var[v]=="OMEGA"      : tres.cnLevels = np.linspace(-1,1,21)*0.1

      # if plot_diff and c>0 : 
      #    if var[v]=="U" : tres.cnLevels = np.linspace(-10,10,11)

      # if hasattr(tres,'cnLevels') : 
      #    tres.cnLevelSelectionMode = "ExplicitLevels"
      # else:
      #    if var[v] in ['SPTLS','SPQTLS','U','V'] : 
      #       aboutZero = True
      #    else:
      #       aboutZero = False
      #    cmin,cmax,cint,clev = ngl.nice_cntr_levels( X.min(), X.max(),    \
      #                                               cint=None, max_steps=21,  \
      #                                               returnLevels=True,        \
      #                                               aboutZero=aboutZero )
      #    tres.cnLevels = np.linspace(cmin,cmax,num=21)
      #    tres.cnLevelSelectionMode = "ExplicitLevels"

      num_cstep = 21
      cmin,cmax,cint,clev = ngl.nice_cntr_levels( D1.min(), D1.max(),         \
                           cint=None, max_steps=num_cstep, returnLevels=True, \
                           aboutZero=True )
      tres.cnLevels = np.linspace(cmin,cmax,num=num_cstep)
      tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------

      tres.sfXCStartV = min( lat_bins )
      tres.sfXCEndV   = max( lat_bins )
      tres.sfYCStartV = min( bin_ds_dyn_0['lev'].values )
      tres.sfYCEndV   = max( bin_ds_dyn_0['lev'].values )

      plot.append( ngl.contour(wks, D0, tres) )
      hs.set_subtitles(wks, plot[len(plot)-1], case[c], '', 'Initial Diff', font_height=0.015)

      plot.append( ngl.contour(wks, D1, tres) )
      hs.set_subtitles(wks, plot[len(plot)-1], case[c], '', 'Final Diff', font_height=0.015)

      # var_str = var[v]
      # if plot_diff and c>0 : var_str = var_str+' (diff)'

      # name_str = case_obj.short_name
      # if plot_diff and c==0 : name0 = name_str
      # if plot_diff and c >0 : name_str = name_str+' - '+name0

      # hs.set_subtitles(wks, plot[len(plot)-1], name_str, '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [num_case*num_var,2]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------