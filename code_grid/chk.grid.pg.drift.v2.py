#---------------------------------------------------------------------------------------------------
# v1 - Plot the zonal mean to check for drift signal
# v2 - plot time vs latitude drift of zonal mean for given model level
#---------------------------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
import glob
import cftime
home = os.getenv("HOME")
print()

case = ['E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_01']
name = ['ne30pg2 MMF w/ 2-way tend']

var = ['T']

# years, months, num_files = [],[5,6,7],0

fig_file = home+"/Research/E3SM/figs_grid/grid.pg.drift.v2"

dlat = 5
lat1 = -85.
lat2 =  85.

klev = 58  # ~ 860 hPa
# klev = 45  # ~ 492 hPa

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_contour_fill()
res.vpHeightF = 0.4
res.trYReverse = True
res.tiXAxisString = 'Latitude'
res.tiYAxisString = 'Time [days]'

if 'name' not in vars() : name = case

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('var: '+var[v])
   for c in range(num_case):
      print('  case: '+case[c])
      case_obj = he.Case( name=case[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      pvar = var[v]
      dvar = 'DYN_'+var[v]
      
      file_list = sorted(glob.glob(getattr(case_obj,'h1')))
      # Remove "remapped" files
      for file_name in file_list : 
         if '.remap.' in file_name : file_list.remove(file_name)
      ds = xr.open_mfdataset( file_list )

      lat_p  = ds['lat']
      lat_d  = ds['lat_d']
      area_p = ds['area_p']
      area_d = ds['area_d']

      data_phy = ds[pvar].isel(lev=klev)
      data_dyn = ds[dvar].isel(lev=klev)

      lat_d = lat_d.rename({'ncol_d':'ncol'})
      area_d = area_d.rename({'ncol_d':'ncol'})
      data_dyn = data_dyn.rename({'ncol_d':'ncol'})

      #-------------------------------------------------------------------------
      # Calculate time and zonal mean
      #-------------------------------------------------------------------------

      area_d_bin = area_d.assign_coords(ncol=lat_d.isel(time=0,drop=True)).groupby_bins('ncol',np.arange(lat1,lat2,dlat)).sum(dim='ncol')
      area_p_bin = area_p.assign_coords(ncol=lat_p.isel(time=0,drop=True)).groupby_bins('ncol',np.arange(lat1,lat2,dlat)).sum(dim='ncol')
      
      dyn_bin = (data_dyn*area_d).assign_coords(ncol=lat_d.isel(time=0,drop=True)).groupby_bins('ncol',np.arange(lat1,lat2,dlat)).sum(dim='ncol')
      phy_bin = (data_phy*area_p).assign_coords(ncol=lat_p.isel(time=0,drop=True)).groupby_bins('ncol',np.arange(lat1,lat2,dlat)).sum(dim='ncol')

      dyn_bin = dyn_bin / area_d_bin
      phy_bin = phy_bin / area_p_bin

      diff = ( phy_bin - dyn_bin ).rename({'ncol_bins':'lat'})
      
      time = diff['time']
      time = ( time - time[0] ).astype('float') / 86400e9

      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # if var[v] in ['OMEGA']  : tres.cnFillPalette = "BlueWhiteOrangeRed"
      # if var[v]=="OMEGA"      : tres.cnLevels = np.linspace(-1,1,21)*0.1

      num_cstep = 21
      cmin,cmax,cint = ngl.nice_cntr_levels( diff.min().values, diff.max().values, aboutZero=True )
      tres.cnLevels = np.linspace(cmin,cmax,num=num_cstep)
      tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      
      # tres.sfXArray = diff['lat'].values
      # tres.sfYArray = diff['lat'].values

      # tres.sfXCStartV = diff['lat'].min().values
      # tres.sfXCEndV   = diff['lat'].max().values
      tres.sfYCStartV = np.min( time.values )
      tres.sfYCEndV   = np.max( time.values )

      plot.append( ngl.contour(wks, diff, tres) )
      hs.set_subtitles(wks, plot[len(plot)-1], name[c], '', 'Phys - Dyn', font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [num_case*num_var,2]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------