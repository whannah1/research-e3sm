# This script provides a visual check of the method for finding neighbors
# in all methods the great circle bearing is used to sort the neighbors
# v1 - use nearest neighbors based on distance
# v2 - use the SCRIP file corner information to determine edge and corner neighbors
import os, ngl, sys, numba, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import filter_factory as ff
import pg_checkerboard_utilities as pg

case,name = [],[]
case_head = 'E3SM.VT-TEST.ne30pg2_ne30pg2'
crm_config = 'CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_1'

# case.append('ERA5'); name.append('ERA5')
# case.append('MAC');  name.append('MAC')
# case.append(f'CESM.f09_g17.FSPCAMS');                       name.append('SPCAM-FV')
# case.append(f'CESM.ne30pg2_ne30pg2_mg17.FSPCAMS');          name.append('SPCAM-SE')
case.append(f'{case_head}.F-MMF1.{crm_config}.00');         name.append('MMF')


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/chk.neighbors.v1'

htype,first_file,num_files = 'h1',0,1

lat1,lat2,lon1,lon2 = 30,40,140,150

num_pts = 16

rng_seed = 500

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wkres = ngl.Resources()
npix=2048; wkres.wkWidth,wkres.wkHeight=npix,npix # use this for plotting all patterns w/ rotation
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_case*num_pts)

res = hs.res_default()
res.mpGridAndLimbOn = False
res.mpLimitMode = 'LatLon'

# res = hs.res_contour_fill_map()
# res.tmXBOn = False
# res.tmYLOn = False
# res.lbTitlePosition = 'Bottom'
# res.lbTitleFontHeightF = 0.01
# res.lbTitleString = 'Count'

# res.mpMinLatF,res.mpMaxLatF = lat1,lat2
# res.mpMinLonF,res.mpMaxLonF = lon1,lon2

### for drawing grid cells
pgres = ngl.Resources()
pgres.nglDraw,pgres.nglFrame = False,False
pgres.gsLineColor = 'black'
# pgres.gsFillColor = -1
# pgres.gsLineThicknessF = 3.0

### for labelling nearest neighbors
txres               = ngl.Resources()
# txres.txFont        = "helvetica-bold"
# txres.txFontColor   = "coral4"
txres.txFontHeightF = 0.02
# txres.txJust        = "BottomCenter"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# @numba.njit()
def sort_neighbors( neighbors, bearing ):
   num_col = neighbors.shape[0]
   neighbors_sorted = np.zeros(neighbors.shape)
   for i in range(num_col): neighbors_sorted[i,:] = neighbors[i, np.argsort(bearing[i,:]) ]
   return neighbors_sorted

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for c in range(num_case):
   print('\n  case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
   case_obj = he.Case( name=case[c], time_freq='daily' )
   #----------------------------------------------------------------------------
   # load the coordinate data
   #----------------------------------------------------------------------------
   
   comp = 'eam'
   if case[c]=='ERA5': comp = None
   if 'CESM' in case[c]: comp = 'cam'
   if case[c]=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'

   use_remap = False
   remap_str=f'remap_ne30pg2'
   if case[c]=='ERA5': use_remap = True
   #----------------------------------------------------------------------------
   # get lat and lon coordinates
   #----------------------------------------------------------------------------

   lat = case_obj.load_data('lat',htype=htype,num_files=1,component=comp,use_remap=use_remap,remap_str=remap_str)
   lon = case_obj.load_data('lon',htype=htype,num_files=1,component=comp,use_remap=use_remap,remap_str=remap_str)

   if case[c] in ['MAC'] or 'CESM' in case[c] :
      nlat,nlon = len(lat), len(lon)
      ncol = len(lat) * len(lon)
      ilat,ilon = lat,lon
      lat,lon = np.zeros(ncol), np.zeros(ncol)
      for j in range(nlat):
         for i in range(nlon):
            n = j*nlon + i
            lat[n],lon[n] = ilat[j],ilon[i]
      lat = xr.DataArray(lat,coords=[np.arange(ncol)],dims='ncol')
      lon = xr.DataArray(lon,coords=[np.arange(ncol)],dims='ncol')

   ncol = len(lat)
   #----------------------------------------------------------------------------
   # find nearest neighbors
   #----------------------------------------------------------------------------

   neighbor_obj = ff.Neighbors( lat, lon, 40 )
   neighbors = neighbor_obj.neighbors#[:,1:]
   bearing   = neighbor_obj._bearings#[:,1:]
   # distance  = neighbor_obj._distances[:,1:]/1e3

   # if 'CESM' in case[c] and any([g in case for g in ['f09','f19']]):
   #    raise RuntimeError('YOU FORGOT TO WRITE THE CODE FOR THIS SECTION!!!')

   
   

   # sort neighbors by bearing
   # neighbors_sorted = sort_neighbors( neighbors[:,1:8+1], bearing[:,1:8+1] )
   bear = bearing[:,1:8+1]
   bear = np.where(bear<0,bear+360,bear)
   neighbors_sorted = sort_neighbors( neighbors[:,1:8+1], bear )

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   np.random.seed(rng_seed*1000 + c^2)
      
   pts_ind = np.random.choice( np.arange(ncol), size=num_pts, replace=False )

   for n in range(num_pts):
      xn = pts_ind[n]

      ip = c*num_pts + n

      tres = copy.deepcopy(res)

      xlat = lat[xn].values
      xlon = lon[xn].values
      dx = 10

      print(f'  lat/lon: {xlat} / {xlon}')

      tres.mpMinLatF = np.max([xlat-dx,-90])
      tres.mpMaxLatF = np.min([xlat+dx, 90])
      tres.mpMinLonF = xlon-dx
      tres.mpMaxLonF = xlon+dx

      if 'CESM' in case[c] or case[c] in ['MAC'] :
         scripfile = xr.open_dataset(os.getenv('HOME')+'/E3SM/data_grid/fv0.9x1.25_070727.nc')
         corner_lon = scripfile['grid_corner_lon'][:,:].values
         corner_lat = scripfile['grid_corner_lat'][:,:].values
         # nlat,nlon = len(lat), len(lon)
         # ncol = len(lat) * len(lon)
         # ilat,ilon = lat,lon
         # lat,lon = np.zeros(ncol), np.zeros(ncol)
         # for j in range(nlat):
         #    for i in range(nlon):
         #       n = j*nlon + i
         #       lat[n],lon[n] = ilat[j],ilon[i]
      else:
         scripfile = case_obj.get_scrip()
         corner_lon = scripfile['grid_corner_lon'][:,:].values
         corner_lat = scripfile['grid_corner_lat'][:,:].values

      plot[ip] = ngl.map(wks,tres)

      for nn in neighbors[xn,:] :
         nn = int(nn)
         cx,cy = corner_lon[nn,:], corner_lat[nn,:]
         xx = np.array([ cx[0], cx[1], cx[2], cx[3], cx[0]])
         yy = np.array([ cy[0], cy[1], cy[2], cy[3], cy[0]])
         dummy = ngl.add_polyline(wks,plot[ip],xx,yy,pgres)

      for nn,nnid in enumerate(neighbors_sorted[xn,:]) :
         i = int(nnid)
         dummy = ngl.add_text(wks, plot[ip], f'{nn+1}', lon[i], lat[i], txres)
         # if lon[i]==xlon and lat[i]==xlat: text = 'X'

      dummy = ngl.add_text(wks, plot[ip], f'{0}', xlon, xlat, txres)


      subtitle_font_height = 0.01
      hs.set_subtitles(wks, plot[ip], name[c], '', '', font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [num_case,num_pts]

if num_case==1: layout = [int(np.ceil(num_pts/4.)),4]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------