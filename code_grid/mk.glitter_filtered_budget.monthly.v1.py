#---------------------------------------------------------------------------------------------------
# Create terms of total water budget filtered to remove grid imprinting
# new files are created that match the htype="h2" files, but with htype="hb" 
#---------------------------------------------------------------------------------------------------
import os
import glob
import ngl
import xarray as xr
import dask
import numpy as np
import pandas as pd
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
import datetime
home = os.getenv("HOME")
print()

### Early Science
case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519']
# case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']

debug = False

if debug :
   years = [1]
   months = [1]
else:
   years = [1,2,3,4,5]
   months = [1,2,3,4,5,6,7,8,9,10,11,12]

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)
for c in range(num_case):
   print('  case: '+case[c])
   case_obj = he.Case( name=case[c] )

   # grab lat and lon since I forgot to add then to the hgb ffiles
   file_path = getattr(case_obj,'h2')
   file_list = sorted(glob.glob(file_path))
   ds = xr.open_mfdataset( file_list[0] )
   lat  = ds['lat']
   lon  = ds['lon']
   area = ds['area']

   # filter options
   num_neighbors = 16
   filter_type = 'boxcar'  # gauss / boxcar
   filter_opts = {}
   Filter = ff.filter_factory( ff.Neighbors(lat,lon,num_neighbors), area, filter_type, filter_opts ) 

   # Locate all hgb files
   file_path = getattr(case_obj,'hgb')
   file_list = sorted(glob.glob(file_path))
   
   #----------------------------------------------------------------------------
   # Loop through valid months
   #----------------------------------------------------------------------------
   for yr in years :
      for mn in months :

         # Specify output file name
         file_out = '/project/projectdirs/m3312/whannah/'+case[c]+'/atm/'
         file_out = file_out+case[c]+'.cam.hgb0.{0:04}-{1:02}.nc'.format(yr,mn)

         print('    '+file_out)

         # Filter file list for current and "nearby" months
         yr1,mn1 = yr,mn-1
         yr2,mn2 = yr,mn+1
         if mn==1  : yr1,mn1 = yr-1,12  
         if mn==12 : yr2,mn2 = yr+1,1
         date1 = '{0:04}-{1:02}'.format(yr1,mn1)
         date2 = '{0:04}-{1:02}'.format(yr ,mn )
         date3 = '{0:04}-{1:02}'.format(yr2,mn2)
         date_list = [date1,date2,date3]

         tmp_file_list = []
         for date in date_list :
            for f in file_list:
               if date in f :
                  # print('        '+f)
                  tmp_file_list.append(f)

         # Open dataset
         ds = xr.open_mfdataset( tmp_file_list )
         
         # ds_out = ds.resample('M').mean()
         # ds_out = ds.sel(time=slice(pd.datetime(yr, mn, 1), pd.datetime(yr, mn, 31)))

         time_mask = xr.DataArray( np.ones([len(ds['time'])],dtype=bool), dims={'time':ds['time']} )
         time_mask = time_mask & [ y == yr for y in ds['time.year'].values ]
         time_mask = time_mask & [ m == mn for m in ds['time.month'].values ]

         
         ds_out = ds.sel( time=ds['time'].where(time_mask,drop=True) ).mean(dim='time')

         time_out = ds['time'].sel( time=ds['time'].where(time_mask,drop=True) ).mean(dim='time')
         ds_out = ds_out.assign_coords( time=time_out ).expand_dims( 'time', axis=0 )

         ds_out['lat']  = lat 
         ds_out['lon']  = lon 
         ds_out['area'] = area

         #----------------------------------------------------------------------
         # load precip + evap, filter, and add to dataset
         #----------------------------------------------------------------------
         if True :

            h1_file_list = tmp_file_list
            h1_file_list = [ t.replace('hgb','h1') for t in h1_file_list ]
            if case[c]=='earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' :
               dir1 = '/project/projectdirs/m3312/whannah/'+case[c]+'/atm/'
               dir2 = '/project/projectdirs/m3312/crjones/e3sm/'+case[c]+'/hourly_2d_hist/'
               h1_file_list = [ t.replace(dir1,dir2) for t in h1_file_list ]

            ds1 = xr.open_mfdataset( h1_file_list )

            P = ds1['PRECT'].where( case_obj.get_mask(ds1),drop=True)
            E = ds1['LHFLX'].where( case_obj.get_mask(ds1),drop=True)

            # Convert units
            P = P * 86400. * 1e3
            E = E / 2.5104e6 / 1e3  * 86400. * 1e3

            # hc.printline()
            # hc.print_stat(P,name='precip')
            # hc.print_stat(E,name='evap')
            # hc.printline()

            Pb = P.copy( data=np.full( P.shape, np.nan ) )
            Eb = E.copy( data=np.full( E.shape, np.nan ) )
            for t in range(len(P['time'])):
               Pb[t,:] = Filter.filter( P[t,:].values )
               Eb[t,:] = Filter.filter( E[t,:].values )
            Pp = P - Pb
            Ep = E - Eb

            time_mask = xr.DataArray( np.ones([len(ds1['time'])],dtype=bool), dims={'time':ds1['time']} )
            time_mask = time_mask & [ y == yr for y in ds1['time.year'].values ]
            time_mask = time_mask & [ m == mn for m in ds1['time.month'].values ]
            
            P  = P .sel( time=ds1['time'].where(time_mask,drop=True) ).mean(dim='time')
            Pb = Pb.sel( time=ds1['time'].where(time_mask,drop=True) ).mean(dim='time')
            Pp = Pp.sel( time=ds1['time'].where(time_mask,drop=True) ).mean(dim='time')
            E  = E .sel( time=ds1['time'].where(time_mask,drop=True) ).mean(dim='time')
            Eb = Eb.sel( time=ds1['time'].where(time_mask,drop=True) ).mean(dim='time')
            Ep = Ep.sel( time=ds1['time'].where(time_mask,drop=True) ).mean(dim='time')

            P  = P .assign_coords( time=time_out ).expand_dims( 'time', axis=0 )
            Pb = Pb.assign_coords( time=time_out ).expand_dims( 'time', axis=0 )
            Pp = Pp.assign_coords( time=time_out ).expand_dims( 'time', axis=0 )
            E  = E .assign_coords( time=time_out ).expand_dims( 'time', axis=0 )
            Eb = Eb.assign_coords( time=time_out ).expand_dims( 'time', axis=0 )
            Ep = Ep.assign_coords( time=time_out ).expand_dims( 'time', axis=0 )

            P .attrs['units'] = 'mm/day'
            Pb.attrs['units'] = 'mm/day'
            Pp.attrs['units'] = 'mm/day'
            E .attrs['units'] = 'mm/day'
            Eb.attrs['units'] = 'mm/day'
            Ep.attrs['units'] = 'mm/day'

            P .attrs['long_name'] = 'Precipitation'
            Pb.attrs['long_name'] = 'Precipitation filtered'
            Pp.attrs['long_name'] = 'Precipitation residual'
            E .attrs['long_name'] = 'Evaporation'
            Eb.attrs['long_name'] = 'Evaporation filtered'
            Ep.attrs['long_name'] = 'Evaporation residual'

            ds_out['PRECT' ] = P 
            ds_out['PRECTb'] = Pb
            ds_out['PRECTp'] = Pp
            ds_out['LHFLX' ] = E 
            ds_out['LHFLXb'] = Eb
            ds_out['LHFLXp'] = Ep

         #----------------------------------------------------------------------
         #----------------------------------------------------------------------

         # Write to file
         ds_out.to_netcdf(path=file_out,mode='w')

         if debug: 
            print()
            exit('EXITING for debugging purposes!\n')
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------