#---------------------------------------------------------------------------------------------------
# Create terms of total water budget filtered to remove grid imprinting
# new files are created that match the htype="h2" files, but with htype="hb" 
#---------------------------------------------------------------------------------------------------
import os
import glob
import ngl
import xarray as xr
import dask
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
import datetime
home = os.getenv("HOME")
print()

### Glitter analysis cases
# case = ['E3SM_TEST_GPU_SP1_ne30_64x1_4km_00' ,'E3SM_TEST_GPU_SP1_ne30_64x1_1km_00']

### Early Science
# case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519']
case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']

# filter options
num_neighbors = 16
filter_type = 'boxcar'  # gauss / boxcar
filter_opts = {}

fig_file = home+"/Research/E3SM/figs_grid/glitter_filtered_budget.v1"

lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([30,50,100,150,200,300,400,500,600,700,800,850,925,975,1000])


debug   = True
mk_plot = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)
for c in range(num_case):
   print('  case: '+case[c])
   case_obj = he.Case( name=case[c] )

   # file_path = '/global/homes/w/whannah/E3SM/earlyscience_lowres/3hourly_3d_hist/earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415.cam.h2.0005-08-24-00000.nc'
   
   file_path = getattr(case_obj,'h2')
   file_list = sorted(glob.glob(file_path))
   
   for file_name in file_list : 
      file_out = file_name.replace('.h2.','.hgb.')
      file_out = file_out.replace( os.path.dirname(file_out), '/project/projectdirs/m3312/whannah/'+case[c]+'/atm' )

      ps_file_name = file_name.replace('.h2.','.h1.')
      if case[c]=='earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' :
         ps_file_name = ps_file_name.replace('3hourly_3d_hist','hourly_2d_hist')
      #-------------------------------------------------------------------------
      # load the data
      #-------------------------------------------------------------------------
      print('    Loading data...')

      ds = xr.open_mfdataset( file_name )

      # Correct timestamp? - shift by mean delta t
      # dtime = ds['time'].diff(dim='time').values.astype('timedelta64[h]').mean()
      # ds['time'] = ds.time.get_index('time') - datetime.timedelta(hours=dtime.astype(np.int)*0.5)

      lat  = ds['lat']
      lon  = ds['lon']
      area = ds['area']
      time = ds['time']
      
      Q = ds['Q'     ].where( case_obj.get_mask(ds),drop=True) \
         +ds['CLDLIQ'].where( case_obj.get_mask(ds),drop=True) \
         +ds['CLDICE'].where( case_obj.get_mask(ds),drop=True) 
      Q.attrs['long_name'] = 'Total Water'
      Q.attrs['units']     = 'kg/kg'

      W = ds['OMEGA'].where( case_obj.get_mask(ds),drop=True)

      ds_ps = xr.open_mfdataset( ps_file_name )
      PS = ds_ps['PS'].isel(time=slice(0,len(ds_ps['time']),3))
      
      #-------------------------------------------------------------------------
      # interpolate vertically
      #-------------------------------------------------------------------------
      def interpolate_vert(X,PS,ds,lev):
         PS_dum = PS.where( case_obj.get_mask(ds_ps),drop=True)
         PS_dum = PS_dum.expand_dims(dim='dummy',axis=len(ds_ps['PS'].dims)).values
         interp_type = 1         # 1 = LINEAR, 2 = LOG, 3 = LOG LOG
         extrap_flag = False
         P0  = ds['P0']  .values/1e2
         hya = ds['hyam'].values
         hyb = ds['hybm'].values
         X_dum  = X.expand_dims(dim='dummy',axis=len(X.dims))
         X_new = xr.full_like(X.isel(lev=0),np.nan).drop('lev')
         X_new = X_new.expand_dims(dim={'lev':lev}, axis=X.get_axis_num('lev'))
         X_tmp = ngl.vinth2p( X_dum, hya, hyb, lev, PS_dum, \
                              interp_type, P0, 1, extrap_flag)[:,:,:,0]
         X_tmp = np.where(X_tmp==1e30,np.nan,X_tmp)
         # X_new.data = dask.array.from_array( X_tmp, chunks=(1,X.shape[1],X.shape[2]) )
         X_new.data = X_tmp
         return X_new
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      print('    Interpolating...')

      Q = interpolate_vert(Q,PS,ds,lev)
      W = interpolate_vert(W,PS,ds,lev)

      # Convert lev coordinate to float
      W['lev'] = W['lev']*1.
      Q['lev'] = Q['lev']*1.

      lev = Q['lev' ]      # Recast lev as DataArray

      coords = Q.coords
      dims   = Q.dims 
      #----------------------------------------------------------------------------
      # Calculate pressure thickness for vertical integral (data is already on pressure levels)
      #----------------------------------------------------------------------------
      ps3d,pl3d = xr.broadcast(PS,lev*100.)

      pl3d = pl3d.transpose('time','lev','ncol')
      ps3d = ps3d.transpose('time','lev','ncol')

      nlev = len(lev)
      tvals = slice(0,nlev-2)
      cvals = slice(1,nlev-1)
      bvals = slice(2,nlev-0)
      
      # Calculate pressure thickness
      dp3d = xr.DataArray( np.full(pl3d.shape,np.nan), coords=pl3d.coords )
      dp3d[:,nlev-1,:] = ps3d[:,nlev-1,:].values - pl3d[:,nlev-1,:].values
      dp3d[:,cvals,:] = pl3d[:,bvals,:].values - pl3d[:,tvals,:].values

      # Deal with cases where levels are below surface pressure
      condition = pl3d[:,cvals,:].values<ps3d[:,cvals,:].values
      new_data  = ps3d[:,bvals,:].values-pl3d[:,tvals,:].values
      dp3d[:,cvals,:] = dp3d[:,cvals,:].where( condition, new_data )

      # Screen out negative dp values
      dp3d = dp3d.where( dp3d>0, np.nan )

      # hc.print_stat(dp3d)
      # exit()
      #-------------------------------------------------------------------------
      # Filter the data
      #-------------------------------------------------------------------------
      print('    Filtering data...')

      # Q['lev'] = Q['lev']*100.
      # ddpQ = Q.differentiate(coord='lev')
      # Q['lev'] = Q['lev']/100.
      # shape_tmp = ddpQ[:,0,:].values.shape
      # ddpQ .values[:, 0,:] = np.full(shape_tmp,np.nan)
      # ddpQ .values[:,-1,:] = np.full(shape_tmp,np.nan)

      Filter = ff.filter_factory( ff.Neighbors(lat,lon,num_neighbors), area, filter_type, filter_opts ) 
      # ddpQb = ddpQ.copy( data=np.full(Q.shape,np.nan) )
      # ddpQp = ddpQ.copy( data=np.full(Q.shape,np.nan) )
      Qb = Q.copy( data=np.full(Q.shape,np.nan) )
      Qp = Q.copy( data=np.full(Q.shape,np.nan) )
      Wb = W.copy( data=np.full(Q.shape,np.nan) )
      Wp = W.copy( data=np.full(Q.shape,np.nan) )
      for t in range(len(time)):
         for k in range(len(lev)): 
            # ddpQb.values[t,k,:] = Filter.filter( ddpQ.values[t,k,:] )
            Qb.values[t,k,:] = Filter.filter( Q.values[t,k,:] )
            Wb.values[t,k,:] = Filter.filter( W.values[t,k,:] )
      # ddpQp = ddpQ - ddpQb
      Qp = Q.copy( data=( Q.values - Qb.values ) )
      Wp = Q.copy( data=( W.values - Wb.values ) )

      # print()
      # hc.print_stat(Q)
      # print()
      # hc.print_stat(Qb)
      # print()
      # hc.print_stat(Qp)
      # print()
      # exit()

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      print('    Calculating derivatives...')

      # Temporarily convert lev to Pa
      Q ['lev'] = Q ['lev']*100.
      Qb['lev'] = Qb['lev']*100.
      Qp['lev'] = Qp['lev']*100.

      # Calculate vertical derivative
      ddpQ  =  Q.differentiate(coord='lev')
      ddpQb = Qb.differentiate(coord='lev')
      ddpQp = Qp.differentiate(coord='lev')

      # Convert lev coord back to hPa
      Q ['lev'] = Q ['lev']/100.
      Qb['lev'] = Qb['lev']/100.
      Qp['lev'] = Qp['lev']/100.

      ddpQ ['lev'] = ddpQ ['lev']/100.
      ddpQb['lev'] = ddpQb['lev']/100.
      ddpQp['lev'] = ddpQp['lev']/100.

      # Set bottom and top levels to missing
      shape_tmp = ddpQ[:,0,:].values.shape
      ddpQ .values[:, 0,:] = np.full(shape_tmp,np.nan)
      ddpQb.values[:, 0,:] = np.full(shape_tmp,np.nan)
      ddpQp.values[:, 0,:] = np.full(shape_tmp,np.nan)
      ddpQ .values[:,-1,:] = np.full(shape_tmp,np.nan)
      ddpQb.values[:,-1,:] = np.full(shape_tmp,np.nan)
      ddpQp.values[:,-1,:] = np.full(shape_tmp,np.nan)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      wks = ngl.open_wks("x11",fig_file)
      plot = []
      res = hs.res_xy()
      res.xyLineThicknessF = 8
      res.trYReverse = True

      for i in range(12) :
         vname = ''
         if i== 0 : tmp, vname = Q     *1e3, 'Q'
         if i== 1 : tmp, vname = Qb    *1e3, 'Qb'
         if i== 2 : tmp, vname = Qp    *1e3, 'Qp'
         if i== 3 : tmp, vname = ddpQ  *1e3, 'ddpQ'
         if i== 4 : tmp, vname = ddpQb *1e3, 'ddpQb'
         if i== 5 : tmp, vname = ddpQp *1e3, 'ddpQp'
         if i== 6 : tmp, vname = W    *86400.,  'W'
         if i== 7 : tmp, vname = Wb   *86400., 'Wb'
         if i== 8 : tmp, vname = Wp   *86400., 'Wp'
         if i== 9 : tmp, vname = Wb*ddpQb *1e3*86400. , 'Wb*ddpQb'
         if i==10 : tmp, vname = Wb*ddpQp *1e3*86400. , 'Wb*ddpQp'
         if i==11 : tmp, vname = Wp*ddpQb *1e3*86400. , 'Wp*ddpQb'
         
         # tmp = tmp.mean(dim=['time','ncol']).values
         tmp = tmp[1,:,10].values 
         # tmp = tmp[:,:,400].mean(dim=['time']).values
         # tmp = tmp[:,:,1:1000].mean(dim='time').mean(dim='ncol').values

         tmp = np.ma.masked_invalid( tmp )
         plot.append( ngl.xy(wks, tmp ,lev.values,res) )
         hs.set_subtitles(wks, plot[len(plot)-1], vname, "", "")

      ngl.panel(wks,plot,[4,3],hs.setres_panel())
      ngl.end()
      hc.trim_png(fig_file)

      exit()
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      print('    Calculating budget terms...')

      # W_ddpQ   = W *ddpQ .copy()
      # Wb_ddpQb = Wb*ddpQb.copy()
      # Wb_ddpQp = Wb*ddpQp.copy()
      # Wp_ddpQb = Wp*ddpQb.copy()
      # Wp_ddpQp = Wp*ddpQp.copy()

      W_ddpQ   = Q.copy( data=(W .values*ddpQ .values) )
      Wb_ddpQb = Q.copy( data=(Wb.values*ddpQb.values) )
      Wb_ddpQp = Q.copy( data=(Wb.values*ddpQp.values) )
      Wp_ddpQb = Q.copy( data=(Wp.values*ddpQb.values) )
      Wp_ddpQp = Q.copy( data=(Wp.values*ddpQp.values) )

      print()
      hc.print_stat( W_ddpQ  *1e3*86400. , name='W_ddpQ' )
      hc.print_stat( Wb_ddpQb*1e3*86400. , name='Wb_ddpQb' )
      hc.print_stat( Wb_ddpQp*1e3*86400. , name='Wb_ddpQp' )
      hc.print_stat( Wp_ddpQb*1e3*86400. , name='Wp_ddpQb' )
      hc.print_stat( Wp_ddpQp*1e3*86400. , name='Wp_ddpQp' )
      print()
      

      W_ddpQ  .attrs['long_name'] = 'W_ddpQ  '
      Wb_ddpQb.attrs['long_name'] = 'Wb_ddpQb'
      Wb_ddpQp.attrs['long_name'] = 'Wb_ddpQp'
      Wp_ddpQb.attrs['long_name'] = 'Wp_ddpQb'
      Wp_ddpQp.attrs['long_name'] = 'Wp_ddpQp'
      W_ddpQ  .attrs['units']     = 'kg/kg/s'
      Wb_ddpQb.attrs['units']     = 'kg/kg/s'
      Wb_ddpQp.attrs['units']     = 'kg/kg/s'
      Wp_ddpQb.attrs['units']     = 'kg/kg/s'
      Wp_ddpQp.attrs['units']     = 'kg/kg/s'

      # W_ddpQ_b   = Q.copy(data=np.empty(Q.shape))
      # Wb_ddpQb_b = Q.copy(data=np.empty(Q.shape))
      # Wp_ddpQp_b = Q.copy(data=np.empty(Q.shape))
      # Wb_ddpQp_b = Q.copy(data=np.empty(Q.shape))
      # Wp_ddpQb_b = Q.copy(data=np.empty(Q.shape))
      # for t in range(len(time)):
      #    for k in range(len(lev)): 
      #       W_ddpQ_b  [t,k,:] = Filter.filter( W_ddpQ  [t,k,:].values )
      #       Wb_ddpQb_b[t,k,:] = Filter.filter( Wb_ddpQb[t,k,:].values )
      #       Wp_ddpQp_b[t,k,:] = Filter.filter( Wp_ddpQp[t,k,:].values )
      #       Wb_ddpQp_b[t,k,:] = Filter.filter( Wb_ddpQp[t,k,:].values )
      #       Wp_ddpQb_b[t,k,:] = Filter.filter( Wp_ddpQb[t,k,:].values )

      #-------------------------------------------------------------------------
      # Mass weighted vertical integral
      #-------------------------------------------------------------------------
      print('    Calculating vertically integrated quantities...')

      Q_ci = ( Q * dp3d ).sum(dim='lev') *1e3
      Q_ci.attrs['long_name'] = 'column integrated total water'
      Q_ci.attrs['units']     = 'mm'

      g = 9.81
      rho = 1000.

      W_ddpQ_ci   = ( W_ddpQ   * dp3d ).sum(dim='lev') / (g*rho) * 1e3 * 86400.
      Wb_ddpQb_ci = ( Wb_ddpQb * dp3d ).sum(dim='lev') / (g*rho) * 1e3 * 86400.
      Wb_ddpQp_ci = ( Wb_ddpQp * dp3d ).sum(dim='lev') / (g*rho) * 1e3 * 86400.
      Wp_ddpQb_ci = ( Wp_ddpQb * dp3d ).sum(dim='lev') / (g*rho) * 1e3 * 86400.
      Wp_ddpQp_ci = ( Wp_ddpQp * dp3d ).sum(dim='lev') / (g*rho) * 1e3 * 86400.

      W_ddpQ_ci  .attrs['long_name'] = 'column integrated W_ddpQ'
      Wb_ddpQb_ci.attrs['long_name'] = 'column integrated Wb_ddpQb'
      Wb_ddpQp_ci.attrs['long_name'] = 'column integrated Wb_ddpQp'
      Wp_ddpQb_ci.attrs['long_name'] = 'column integrated Wp_ddpQb'
      Wp_ddpQp_ci.attrs['long_name'] = 'column integrated Wp_ddpQp'
      W_ddpQ_ci  .attrs['units']     = 'mm/day'
      Wb_ddpQb_ci.attrs['units']     = 'mm/day'
      Wb_ddpQp_ci.attrs['units']     = 'mm/day'
      Wp_ddpQb_ci.attrs['units']     = 'mm/day'
      Wp_ddpQp_ci.attrs['units']     = 'mm/day'
      #-------------------------------------------------------------------------
      # Write to new file
      #-------------------------------------------------------------------------
      print('    Creating new dataset... ')

      # print('\nWriting filtered data to file: '+tmp_file+'\n')
      ds_out = xr.Dataset( coords=coords )

      ds_out['W_ddpQ'  ] = W_ddpQ   
      ds_out['Wb_ddpQb'] = Wb_ddpQb 
      ds_out['Wb_ddpQp'] = Wb_ddpQp 
      ds_out['Wp_ddpQb'] = Wp_ddpQb 
      ds_out['Wp_ddpQp'] = Wp_ddpQp 

      # ds_out['W_ddpQ_b'  ] = W_ddpQ_b  
      # ds_out['Wb_ddpQb_b'] = Wb_ddpQb_b
      # ds_out['Wp_ddpQp_b'] = Wp_ddpQp_b
      # ds_out['Wb_ddpQp_b'] = Wb_ddpQp_b
      # ds_out['Wp_ddpQb_b'] = Wp_ddpQb_b

      ds_out['Q_ci'       ] = Q_ci  
      ds_out['W_ddpQ_ci'  ] = W_ddpQ_ci  
      ds_out['Wb_ddpQb_ci'] = Wb_ddpQb_ci
      ds_out['Wb_ddpQp_ci'] = Wb_ddpQp_ci
      ds_out['Wp_ddpQb_ci'] = Wp_ddpQb_ci
      ds_out['Wp_ddpQp_ci'] = Wp_ddpQp_ci

      print('    writing to file: '+file_out)
      ds_out.to_netcdf(path=file_out,mode='w')

      

      #----------------------------------------------------------------------------
      # Profile time average terms
      #----------------------------------------------------------------------------
      if mk_plot :

         X1 = W_ddpQ
         X2 = ( Wb_ddpQb + Wb_ddpQp + Wp_ddpQb + Wp_ddpQp )
         X3 = ( Wb_ddpQp + Wp_ddpQb + Wp_ddpQp )

         hc.print_stat( X1, fmt='e', name='W_ddpQ' )
         hc.print_stat( X2, fmt='e', name='Wb_ddpQb + Wb_ddpQp + Wp_ddpQb + Wp_ddpQp' )
         hc.print_stat( X3, name="Wb_ddpQp + Wp_ddpQb + Wp_ddpQp" )
         hc.print_stat( (X1-X2), name="Diff", fmt='e')
         hc.print_stat( (X1-X3), name="Diff2", fmt='e')
         hc.print_stat( np.absolute(X1-X2), name="Diff1 (abs)", fmt='e')
         hc.print_stat( np.absolute(X1-X3), name="Diff2 (abs)", fmt='e')

         wks = ngl.open_wks("x11",fig_file)
         plot = []
         res = hs.res_xy()
         res.xyLineThicknessF = 8
         res.xyDashPatterns = [0,1,2]
         res.trYReverse = True

         pX = np.array([ X1.mean(dim=['time','ncol']).values \
                        ,X2.mean(dim=['time','ncol']).values \
                        ,X3.mean(dim=['time','ncol']).values \
                        ])
         # res.xyLineColors = ["black","red","blue"]
         plot.append( ngl.xy(wks,pX,lev.values,res) )
         hs.set_subtitles(wks, plot[len(plot)-1], "Mean", "", "")


         # D1 = np.absolute(X1-X2)
         # D2 = np.absolute(X1-X3)
         # pX = np.array([ D1.mean(dim=['time','ncol']).values \
         #                ,D2.mean(dim=['time','ncol']).values \
         #                ])
         # res.xyDashPatterns = [0,0]
         # plot.append( ngl.xy(wks,pX,lev.values,res) )
         # hs.set_subtitles(wks, plot[len(plot)-1], "Mean Absolute Difference", "", "")

         ngl.panel(wks,plot,[1,len(plot)],hs.setres_panel())
         ngl.end()
         hc.trim_png(fig_file)
      #----------------------------------------------------------------------------
      #----------------------------------------------------------------------------
      if debug: 
         print()
         exit('EXITING for debugging purposes!\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------