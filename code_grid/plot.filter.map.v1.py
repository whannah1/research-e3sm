import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
import copy
from timeit import default_timer as timer
home = os.getenv("HOME")
print()

### Early Science
case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']
# case = ['early_science']
# case = ['earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m']

var = ['PRECT']

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_grid/filter.map.v1"


lat1,lat2 = -62,62

htype,years,months,num_files = 'h0',[1,2,3,4,5],[],0

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

filter_opts = {} 
filter_opts['sigma'] = 100.0
filter_type = 'boxcar'  # gauss / boxcar
num_neighbors = 16

if 'lev' not in vars(): lev = np.array([-1])

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1+2
if 'lat2' in vars() : res.mpMaxLatF = lat2-2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012

### Clean up the plot appearence
# res.lbLabelBarOn = False
# res.tmXBOn = False
# res.tmYLOn = False

# res.mpLimitMode = "LatLon" 
# res.mpMinLatF   = 20 -15
# res.mpMaxLatF   = 20 +15
# res.mpMinLonF   = 180-15
# res.mpMaxLonF   = 180+15

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )

      area_name = 'area'
      if 'DYN_' in var[v] : 
         case_obj.grid = case_obj.grid.replace('pg2','np4')
         case_obj.ncol_name = 'ncol_d'
         area_name = 'area_d'

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      ### uncommentt this to subset the data
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1
      # if 'lon2' in vars() : case_obj.lon2 = lon2
      lat  = case_obj.load_data('lat',    htype=htype,years=years,months=months,num_files=num_files).values.astype(np.double)
      lon  = case_obj.load_data('lon',    htype=htype,years=years,months=months,num_files=num_files).values.astype(np.double)
      area = case_obj.load_data(area_name,htype=htype,years=years,months=months,num_files=num_files).astype(np.double)
      X    = case_obj.load_data(var[v],   htype=htype,years=years,months=months,num_files=num_files,lev=lev).mean(dim='time')

      
      # Can't interpolate the DYN variables unless we have DYN_PS
      # if 'DYN_' in var[v] : 
      # Also, just pick a set level if lev wasnt specified
      if 'lev' in X.coords :
         if len( X.coords['lev'] )>1 :
            # for k in range(len(X['lev'])) : print(str(k)+'   '+str(X['lev'][k].values))
            klev = 58  # ~ 860 hPa
            print("\nSelecting random model level: "+str(X['lev'][klev].values)+" hPa \n")
            X = X.isel(lev=klev)
         else:
            X = X.isel(lev=0)
      
      gbl_mean = ( (X*area).sum() / area.sum() ).values 
      print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

      #-------------------------------------------------------------------------
      # Filter the data
      #-------------------------------------------------------------------------
      Xf = X.copy(data=X)
      Xp = X.copy(data=X)
      neighbor_obj = ff.Neighbors( lat, lon, num_neighbors )
      Filter = ff.filter_factory(neighbor_obj, area.values, filter_type, filter_opts) 
      # for t in range(len(time)):
         # Xf[t,:] = Filter.filter( X[t,:].values )

      Xf.values = Filter.filter( X.values )

      Xp = X - Xf
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      if var[v] in ['OMEGA']                    : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = "WhiteBlueGreenYellowRed"
      if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = "CBR_wet"

      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,20+1,1)
      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(0.2,15+0.2,0.3)
      if var[v]=="TS"                  : tres.cnLevels = np.arange(0,40+2,2)
      if var[v]=="SPTKE"               : tres.cnLevels = np.linspace(0,4,11)
      if var[v]=="OMEGA"               : tres.cnLevels = np.linspace(-1,1,21)*0.1
      if var[v]=="DYN_OMEGA"           : tres.cnLevels = np.linspace(-1,1,11)*0.000001

      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         aboutZero = False
         if var[v] in ['SPTLS','SPQTLS','U','V'] : aboutZero = True
         cmin,cmax,cint,clev = ngl.nice_cntr_levels(X.min().values, X.max().values,       \
                                                    cint=None, max_steps=21,              \
                                                    returnLevels=True, aboutZero=aboutZero )
         tres.cnLevels = np.linspace(cmin,cmax,num=91)
         tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      hs.set_cell_fill(case_obj,tres)

      # tres.lbLabelBarOn = False

      # plot.append( ngl.contour_map(wks,X.values,tres) )
      tres.cnLevels = np.arange(1,20+1,1)
      plot.append( ngl.contour_map(wks,Xf.values,tres) )
      hs.set_subtitles(wks, plot[len(plot)-1], 'SP-E3SM', '', 'Filtered Precipitation', font_height=0.02)

      # tres.lbLabelBarOn = True
      tres.cnLevels = np.linspace(-4.,4.,num=21)
      tres.cnFillPalette = "BlueWhiteOrangeRed"
      plot.append( ngl.contour_map(wks,Xp.values,tres) )
      hs.set_subtitles(wks, plot[len(plot)-1], 'SP-E3SM', '', 'Residual Precipitation', font_height=0.02)

      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name

      # ctr_str = ''
      # if var[v] in ['PRECT','PRECC','PRECL'] : 
      #    ctr_str = 'Mean: '+'%.2f'%gbl_mean+' [mm/day]'
      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [len(plot),1]
# layout = [num_var,num_case*2]
# layout = [num_case*2,num_var]

if num_var==1 and num_case==4 : layout = [2,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------