#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
import copy
home = os.getenv("HOME")
print()

### Glitter analysis cases
# case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']
# case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519']
case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415','earlyscience.FC5AV1C-L.ne30.E3SM.20190519']
casename = ['SP-E3SM','E3SM']


fig_file = home+"/Research/E3SM/figs_grid/glitter.budget.bar.v1"

# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([50,100,150,200,300,400,500,600,700,800,850,925,975,1000])


debug = False

if debug:
   years, months, num_files = [1],[],0
else:
   years, months, num_files = [1,2,3,4,5],[],0

var = ['W_ddpQ_ci','Wb_ddpQb_ci','Wb_ddpQp_ci','Wp_ddpQb_ci','Wp_ddpQp_ci']
# var = ['PRECT','PRECTb','PRECTp','LHFLX','LHFLXb','LHFLXp']
# var = ['PRECT','PRECTb','PRECTp']
# for v in range(len(var)) : var[v] = var[v] + '_ci'
clr = ['black','gray']
# clr = ['red','blue','green']

#---------------------------------------------------------------------------------------------------
# Set up fancy labels for equation terms
#---------------------------------------------------------------------------------------------------
bv = 28
Ph = 14
Qh = 16
Wh = 20
Wstr = '~F33~w~F21~'
Qstr = 'q~B1~t~N~ '
partial = '~F34~6~F21~'+'~B1~p~N~'
dQ   = partial+' '+Qstr
Qprm = partial+' '+Qstr+'\''
Wprm = ''+Wstr+'\''
Qbar = partial+'~H+'+str(Qh)+'~~V+'+str(bv)+'~_~H-'+str(Qh)+'~~V-'+str(bv)+'~'+Qstr
Wbar = '~H+'+str(Wh)+'~~V+'+str(bv)+'~_~H-'+str(Wh)+'~~V-'+str(bv)+'~'+Wstr
dPstr = '~F34~6~F21~p'

var_labels = []
for v in range(len(var)):
   var_labels.append( var[v] )
   if var[v]=='W_ddpQ'      : var_labels[v] = '-'   +'~F18~O~F21~'      +Wstr+' '+dQ  +'~F18~P~F21~'
   if var[v]=='Wb_ddpQb'    : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~'+Wbar+' '+Qbar+'~F18~P~F21~'
   if var[v]=='Wb_ddpQp'    : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~'+Wbar+' '+Qprm+'~F18~P~F21~'
   if var[v]=='Wp_ddpQb'    : var_labels[v] = '-'   +'~F18~O~F21~'      +Wprm+' '+Qbar+'~F18~P~F21~'
   if var[v]=='Wp_ddpQp'    : var_labels[v] = '-'   +'~F18~O~F21~'      +Wprm+' '+Qprm+'~F18~P~F21~'
   if var[v]=='W_ddpQ_ci'   : var_labels[v] = '-'   +'~F18~O~F21~'      +Wstr+' '+Qstr+'~F18~P~F21~'
   if var[v]=='Wb_ddpQb_ci' : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~'+Wbar+' '+Qbar+'~F18~P~F21~'
   if var[v]=='Wb_ddpQp_ci' : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~'+Wbar+' '+Qprm+'~F18~P~F21~'
   if var[v]=='Wp_ddpQb_ci' : var_labels[v] = '-'   +'~F18~O~F21~'      +Wprm+' '+Qbar+'~F18~P~F21~'
   if var[v]=='Wp_ddpQp_ci' : var_labels[v] = '-'   +'~F18~O~F21~'      +Wprm+' '+Qprm+'~F18~P~F21~'
   if var[v]=='PRECTb'      : var_labels[v] = ' ~H+'+str(Ph)+'~~V+'+str(bv)+'~_~H-'+str(Ph)+'~~V-'+str(bv)+'~'+'P'
   if var[v]=='PRECTp'      : var_labels[v] = ' P\''
   

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# lev = xr.DataArray(lev,dims={'lev':lev})

num_case = len(case)
num_var  = len(var)
data_list = []
data_conf = []
for c in range(num_case):
   print('  case: '+case[c])
   case_obj = he.Case( name=case[c] )
      
   area = case_obj.load_data("area",htype='h2', years=years, months=months, num_files=num_files)
   # PS   = case_obj.load_data("PS"  ,htype='h1', years=years, months=months)

   # PS = PS.isel(time=slice(0,len(PS['time']),3))

   #----------------------------------------------------------------------------
   # Calculate pressure thickness (data is already on pressure levels)
   #----------------------------------------------------------------------------
   # ps3d,pl3d = xr.broadcast(PS/100.,lev)

   # pl3d = pl3d.transpose('time','lev','ncol')
   # ps3d = ps3d.transpose('time','lev','ncol')

   # nlev = len(lev)
   # tvals = slice(0,nlev-2)
   # cvals = slice(1,nlev-1)
   # bvals = slice(2,nlev-0)
   
   # # Calculate pressure thickness
   # dp3d = xr.DataArray( np.full(pl3d.shape,np.nan), coords=pl3d.coords )
   # dp3d[:,nlev-1,:] = ps3d[:,nlev-1,:].values - pl3d[:,nlev-1,:].values
   # dp3d[:,cvals,:] = pl3d[:,bvals,:].values - pl3d[:,tvals,:].values

   # # Deal with cases where levels are below surface pressure
   # condition = pl3d[:,cvals,:].values<ps3d[:,cvals,:].values
   # new_data  = ps3d[:,bvals,:].values-pl3d[:,tvals,:].values
   # dp3d[:,cvals,:] = dp3d[:,cvals,:].where( condition, new_data )

   # # Screen out negative dp values
   # dp3d = dp3d.where( dp3d>0, np.nan )

   #----------------------------------------------------------------------------
   # Vertically integrate and average
   #----------------------------------------------------------------------------
   for v in range(num_var):

      X = case_obj.load_data(var[v], htype='hgb0', years=years, months=months, num_files=num_files )

      # print('\nWARNING: using sub-daily data!!! \n')
      # X = case_obj.load_data(var[v], htype='hgb', years=years, months=months, num_files=num_files )

      # file_path = '/project/projectdirs/m3312/whannah/earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415/atm/'
      # file_path = file_path + 'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415.cam.hgb0.0001-01*'
      # ds = xr.open_mfdataset( file_path )
      # X = ds[var[v]]

      if 'ddpQ' in var[v] : X = X * -1. / 100.  # vert derivative is per hPa instead of Pa

      # X = ( X * dp3d ).sum(dim='lev')                 # Mass weighted vertical integral
      X = ( X * area ).sum(dim='ncol') / area.sum()   # Area weighted spatial average
      N = X.count()
      S = X.std()
      X = X.mean()                                    # Time mean

      data_list.append( X.values )

      #----------------------------------------------------------------------------
      #----------------------------------------------------------------------------
      # Critical t-statistic for significance
      # tval_crit = 1.960   # 2-tail test w/ inf dof & P=0.01
      tval_crit = 2.24   # 2-tail test w/ inf dof & P=0.05

      conf = tval_crit * S / np.sqrt( N )
      data_conf.append( conf.values ) 

      #----------------------------------------------------------------------------
      #----------------------------------------------------------------------------

hc.printline()
for i in range(len(data_list)) : 
   print( '    {0:12.6f}  +/-  {1:8.6f} '.format( data_list[i], data_conf[i] ) )
hc.printline()

#-------------------------------------------------------------------------------
# Bar chart of column integrated means
#-------------------------------------------------------------------------------

# def get_bar(x,y,dx,ymin=0.,bar_width_perc=0.6):
#    dxp = (dx * bar_width_perc)/2.
#    xbar = np.array([x-dxp,x+dxp,x+dxp,x-dxp,x-dxp])
#    ybar = np.array([ ymin, ymin,    y,    y, ymin])
#    return xbar,ybar

wks = ngl.open_wks('png',fig_file)
res = hs.res_xy()
res.nglDraw = True
res.nglFrame = False
res.tiYAxisString = 'Column Total Water Tendency [mm/day]'
# res.vpHeightF             = 0.90
# res.vpWidthF              = 0.80

pgres = ngl.Resources()
pgres.gsFillColor = 'red'
x = np.arange(0,num_var,1,dtype=float)

# res.trYMinF               = np.min(data_conf)-np.std(data_conf)*0.1
# res.trYMaxF               = np.max(data_conf)+np.std(data_conf)*0.1 
res.trYMinF               = np.min(data_list)-np.max(data_conf)-np.std(data_list)*0.1
res.trYMaxF               = np.max(data_list)+np.max(data_conf)+np.std(data_list)*0.1 
res.trXMinF               = np.min(x)-1
res.trXMaxF               = np.max(x)+1

res.tmXBMode         = "Explicit"
res.tmXBLabelAngleF  = -50.
res.tmXBValues       = x
res.tmXBLabels = var_labels
res.xyMarkLineMode = "Lines"


dx = 0.5
for v in range(num_var):
   for c in range(num_case):
      ymin = 0.0 #[0.,0.,0.,0.,0.]
      bar_width_perc=0.6
      dxp = (dx * bar_width_perc)/2.
      xx = x[v]
      # yy = data_list[v*num_case+c]
      yy = data_list[c*num_var+v]
      xbar = np.array([ xx-dxp, xx+dxp, xx+dxp, xx-dxp, xx-dxp])
      ybar = np.array([ ymin, ymin,  yy,  yy, ymin])

      # Shift to accomadate multiple cases
      # for b in range(len(xbar)) : xbar[b] = xbar[b] + (c+1-num_case)*dxp*2
      for b in range(len(xbar)) : xbar[b] = xbar[b] - dxp*(num_case-1) + dxp*2*c

      ### plot polygon outline
      # xbar,ybar = get_bar(x[i],y[i],dx)
      plot = ngl.xy(wks,xbar,ybar,res)

      # tplot = ngl.xy(wks,xbar,ybar,res)
      # if c==0 :
      #    plot = tplot 
      # else:
      #    ngl.overlay(plot,tplot)

      ### Add filled polygon
      pgres.gsFillColor = clr[c]
      # pgres.gsFillIndex = c
      ngl.polygon(wks,plot,xbar,ybar,pgres)
      # ngl.polyline(wks,plot,xbar,ybar)

      ### add confidence limits
      cx = x[v] - dxp*(num_case-1) + dxp*2*c
      cc = data_conf[c*num_var+v]
      cy = [yy-cc,yy+cc]

      tres = hs.res_xy()
      tres = copy.deepcopy(res)
      # tres.nglDraw = True
      # tres.trXMinF = res.trXMinF
      # tres.trXMaxF = res.trXMaxF
      tres.xyLineColor = 'red'

      ngl.overlay(plot, ngl.xy(wks,[cx,cx],cy,tres) )
      
      # tres.xyMarkLineMode = "MarkLines"
      # tres.xyMarker = 7
      cdx = dxp/3.
      ngl.overlay(plot, ngl.xy(wks,[cx-cdx,cx+cdx],[yy-cc,yy-cc],tres) )
      # tres.xyMarker = 8
      ngl.overlay(plot, ngl.xy(wks,[cx-cdx,cx+cdx],[yy+cc,yy+cc],tres) )
#-------------------------------------------------------------------------------
# Add horz line at 0
#-------------------------------------------------------------------------------
lres = hs.res_xy()
lres.xyLineThicknessF = 1
xx = [-1e3,1e3]
yy = [0.,0.]
ngl.overlay(plot,ngl.xy(wks,xx,yy,lres))

#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
lgres = ngl.Resources()
lgres.vpWidthF          = 0.1
lgres.vpHeightF         = 0.15
# lgres.lgOrientation     = "Horizontal"
# lgres.lgLabelAlignment   = "AboveItems"
lgres.lgLabelFontHeightF = 0.03
lgres.lgMonoDashIndex    = True
lgres.lgLineLabelsOn     = False
lgres.lgLineThicknessF   = 80
lgres.lgLineColors       = clr
for c in range(len(casename)) : casename[c] = '  '+casename[c]
pid = ngl.legend_ndc(wks, len(casename), casename, 0.6, 0.9, lgres)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

# ngl.panel(wks,plot,[1,len(plot)],hs.setres_panel())
ngl.draw(plot)
ngl.frame(wks)
ngl.end()
hc.trim_png(fig_file)
