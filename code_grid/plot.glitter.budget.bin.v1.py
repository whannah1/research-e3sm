#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
home = os.getenv("HOME")
print()

### Glitter analysis cases
# case,case_name = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415'],['SP-E3SM']
# case,case_name = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519'],['E3SM']
case,case_name = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519','earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415'], ['E3SM','SP-E3SM']


fig_file = home+"/Research/E3SM/figs_grid/glitter.budget.bin.v1"

debug = True

if debug:
   years, months, num_files = [2],[],0
else:
   years, months, num_files = [1,2,3,4,5],[],0

lat1 = -30
lat2 =  30

var_x = 'CWV'

# var1 = ['Wb_ddpQb_ci','Wb_ddpQp_ci','Wp_ddpQb_ci','Wp_ddpQp_ci','PRECTb','PRECTp']
# clr = ['black','red','green','blue']
# var1 = ['PRECTb','PRECTp']
# var1 = ['Wb_ddpQb_ci','glitter_sum','PRECTb','PRECTp']
var1 = ['W_ddpQ_ci','Wb_ddpQb_ci']#,'glitter_sum']
# var1 = ['Wb_ddpQb_ci','glitter_sum']
clr = ['black','blue','red']
# var1 = ['Wb_ddpQb_ci','PRECTb']
# var2 = ['glitter_sum','PRECTp']
# clr = ['red','blue']
# dsh = [0,0,1,1]

# for v in range(len(var)) : 
#    if 'ddpQ' in var[v] : var[v] = var[v] + '_ci'

# var = ['glitter_sum','PRECTp']
# clr = ['red','blue']

# normalize = False

if 'case_name' not in vars() : exit('ERROR: case_name not defined')

num_grp = 1

recalculate = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
temp_dir = home+"/Research/E3SM/data_temp"

wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4

res.tiXAxisString = 'Column Water Vapor [mm]'
res.tiYAxisString = 'Column Total Water Tendency [mm/day]'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

num_case = len(case)

for g in range(num_grp) :
   if g==0 : var = var1
   if g==1 : var = var2
   num_var  = len(var)
   data_avg = []
   data_cnt = []
   for c in range(num_case):
      print('  case: '+case[c])
      case_obj = he.Case( name=case[c] )

      if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
      if 'lat2' in vars() : case_obj.lat2, case_obj.lat2 = lat2, lat2


      for v in range(num_var):
         bin_tmp_file = temp_dir+"/glitter.budget.bin.v1."+case[c]+"."+var[v]
         bin_tmp_file = bin_tmp_file+".lat1_"+str(lat1)+".lat2_"+str(lat2)
         bin_tmp_file = bin_tmp_file+".debug_"+str(debug)
         bin_tmp_file = bin_tmp_file+".nc"

         print('    X / Y : '+var_x+' / '+var[v])
         print('    bin_tmp_file: '+bin_tmp_file )

         if recalculate :
            if v==0 : 
               X = case_obj.load_data(var_x,htype='h1', years=years, months=months, num_files=num_files )
               X = X.resample(time='D').mean(dim='time')
            #-------------------------------------------------------------------
            # Load data and bin average
            #-------------------------------------------------------------------
            # lat  = case_obj.load_data('lat',  htype='h0')
            area = case_obj.load_data("area").astype(np.double)
            
            derived = False

            if var[v]=='glitter_sum':
               htype = 'hgb'
               Y =   case_obj.load_data('Wb_ddpQp_ci', htype=htype, years=years, months=months, num_files=num_files )  
               Y = Y+case_obj.load_data('Wp_ddpQb_ci', htype=htype, years=years, months=months, num_files=num_files )  
               Y = Y+case_obj.load_data('Wp_ddpQp_ci', htype=htype, years=years, months=months, num_files=num_files )  
               Y = Y * -1. / 100.
               derived = True
               # Convert to daily mean
               Y = Y.resample(time='D').mean(dim='time')
            
            if not derived :
               htype = 'hgb'
               if 'PRECT' in var[v]:
                  htype = 'h1'
                  lat = case_obj.load_data('lat',  htype=htype, num_files=num_files)
                  lon = case_obj.load_data('lon',  htype=htype, num_files=num_files)

                  ### filter options
                  num_neighbors = 16
                  filter_type = 'boxcar'  # gauss / boxcar
                  filter_opts = {}
                  Filter = ff.filter_factory( ff.Neighbors(lat,lon,num_neighbors), area, filter_type, filter_opts ) 

                  P = case_obj.load_data('PRECT', htype=htype, years=years, months=months, num_files=num_files )  
                  # Convert to daily mean
                  P = P.resample(time='D').mean(dim='time')
                  if var[v]=='PRECT' : Y = P
                  if var[v]=='PRECTb' or var[v]=='PRECTp' :
                     Pb = P.copy( data=np.full( P.shape, np.nan ) )
                     for t in range(len(P['time'])):
                        Pb[t,:] = Filter.filter( P[t,:].values )
                     if var[v]=='PRECTb' : Y = Pb
                     if var[v]=='PRECTp' : Y = P - Pb

               else:
                  Y = case_obj.load_data(var[v], htype=htype, years=years, months=months, num_files=num_files )  
                  # Convert to daily mean
                  Y = Y.resample(time='D').mean(dim='time')
               if 'PRECT' in var[v] or 'LHFLX' in var[v] : Y = Y * -1.
               if 'ddpQ'  in var[v] : Y = Y * -1.      
               if '_ci'   in var[v] : Y = Y / 100. # vert derivative is per hPa instead of Pa

            if var_x=='CWV' : bin_min, bin_max, bin_spc = 2, 70, 2

            bin_ds = hc.bin_YbyX( Y, X, bin_min=bin_min, bin_max=bin_max, bin_spc=bin_spc, wgt=area )

            #-------------------------------------------------------------------
            # Write to file 
            #-------------------------------------------------------------------
            bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
         else:
            bin_ds = xr.open_mfdataset( bin_tmp_file )

         data_avg.append( bin_ds['bin_val'].values )
         data_cnt.append( bin_ds['bin_cnt'].values )

         Xbin = bin_ds['bins'].values

         # Print the values for a sanity check
         for i in range(len(bin_ds['bins'].values)) : 
            bin = bin_ds['bins'][i].values
            val = bin_ds['bin_val'][i].values
            cnt = bin_ds['bin_cnt'][i].values
            print('      {0:6.1f}  {1:10.4f}  {2:10}'.format( bin, val, cnt ))
         print()

   #------------------------------------------------------------------------------------------------
   #------------------------------------------------------------------------------------------------


   Ybin = np.ma.masked_invalid( np.stack( data_avg ) )

   buff = np.absolute( Ybin.max() - Ybin.min() ) * 0.05
   res.trYMinF = Ybin.min() - buff
   res.trYMaxF = Ybin.max() + buff

   ### use this for putting all lines on single panel with good y-axis range
   res.xyMonoLineColor = True
   res.xyLineColor = -1
   plot.append( ngl.xy(wks,Xbin,Ybin,res) )

   for c in range(num_case):
      res.xyMonoLineColor = False
      res.xyDashPattern = c
      # res.xyDashPatterns = dsh
      res.xyLineColors   = clr
      y1 = num_var*(c)
      y2 = num_var*(c+1)#-1
      
      ngl.overlay(plot[len(plot)-1], ngl.xy(wks,Xbin,Ybin[y1:y2,:],res) )
      # plot.append( ngl.xy(wks,Xbin,Ybin[y1:y2,:],res) )

      # hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], '', '',font_height=0.015)

      # if g==0 : right_str = 'Filtered Tendencies'
      # if g==1 : right_str = 'Grid Imprinting Tendencies'
      # hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], '', right_str,font_height=0.015)


   # Add horz line at 0
   lres = hs.res_xy()
   lres.xyLineThicknessF = 1
   xx = [-1e3,1e3]
   yy = [0.,0.]
   ngl.overlay(plot[len(plot)-1],ngl.xy(wks,xx,yy,lres))


   # Add plot of bin count
   Ybin = np.ma.masked_invalid( np.stack( data_cnt ) )
   # convert count to percent
   for i in range(Ybin.shape[0]): Ybin[i,:] = Ybin[i,:] / np.sum( Ybin[i,:] ) * 100.
   buff = np.absolute( Ybin.max() - Ybin.min() ) * 0.1
   res.trYMinF = Ybin.min() #- buff
   res.trYMaxF = Ybin.max() + buff
   res.xyMonoLineColor = True
   res.xyLineColor = -1
   res.tiYAxisString = 'Frequency of Occurence [%]'
   plot.append( ngl.xy(wks,Xbin,Ybin,res) )
   for c in range(num_case):
      res.xyLineColor   = 'black'
      res.xyDashPattern = c
      ngl.overlay(plot[len(plot)-1], ngl.xy(wks,Xbin,Ybin[num_var*(c),:],res) )

   ngl.overlay(plot[len(plot)-1],ngl.xy(wks,xx,yy,lres))


#---------------------------------------------------------------------------------------------------
# Add Legend
#---------------------------------------------------------------------------------------------------
bv = 28
Ph = 18
Qh = 16
Wh = 20
Wstr = '~F33~w~F21~'
Qstr = 'q~B1~t~N~ '
partial = '~F34~6~F21~'+'~B1~p~N~'
dQ   = partial+Qstr
Qprm = partial+' '+Qstr+'\''
Wprm = ''+Wstr+'\''
Qbar = partial+'~H+'+str(Qh)+'~~V+'+str(bv)+'~_~H-'+str(Qh)+'~~V-'+str(bv)+'~'+Qstr
Wbar = '~H+'+str(Wh)+'~~V+'+str(bv)+'~_~H-'+str(Wh)+'~~V-'+str(bv)+'~'+Wstr
# Wbar = '~H+10~-~H-10~'+Wbar
dPstr = '~F34~6~F21~p'



for g in range(num_grp) :
   if g==0 : var = var1
   if g==1 : var = var2
   var_labels = []
   for v in range(len(var)):
      var_labels.append( var[v] )

      if var[v]=='PRECTb'      : var_labels[v] = ' ~H+'+str(Ph)+'~~V+'+str(bv)+'~_~H-'+str(Ph)+'~~V-'+str(bv)+'~'+'P'
      if var[v]=='PRECTp'      : var_labels[v] = '  P\''
      if var[v]=='W_ddpQ_ci'   : var_labels[v] = ' -'   +'~F18~O~F21~'       +Wstr+' '+Qstr+'~F18~P~F21~'  
      if var[v]=='Wb_ddpQb_ci' : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~' +Wbar+' '+Qbar+'~F18~P~F21~'  
      if var[v]=='Wb_ddpQp_ci' : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~' +Wbar+' '+Qprm+'~F18~P~F21~'  
      if var[v]=='Wp_ddpQb_ci' : var_labels[v] = ' -'   +'~F18~O~F21~'       +Wprm+' '+Qbar+'~F18~P~F21~'  
      if var[v]=='Wp_ddpQp_ci' : var_labels[v] = ' -'   +'~F18~O~F21~'       +Wprm+' '+Qprm+'~F18~P~F21~'  
      if var[v]=='glitter_sum' : var_labels[v] = ' ~H+10~-~F18~O~F21~~H-10~'+Wbar+' '+Qprm+'~F18~P~F21~' \
                                                +' -~F18~O~F21~'+Wprm+'' +Qbar+'~F18~P~F21~' \
                                                +' -~F18~O~F21~'+Wprm+' '+Qprm+'~F18~P~F21~' 

   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.1
   lgres.vpHeightF          = 0.15
   lgres.lgLabelFontHeightF = 0.012
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 12
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr[::-1]

   if g==0 : gx, gy = 0.1, 0.65
   # if g==0 : gx, gy = 0.09, 0.82
   # if g==1 : gx, gy = 0.09, 0.47

   pid = ngl.legend_ndc(wks, len(var_labels), var_labels[::-1], gx, gy, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [len(plot),1]
layout = [1,len(plot)]
# layout = [num_grp,num_case]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
