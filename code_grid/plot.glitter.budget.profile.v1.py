#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
home = os.getenv("HOME")
print()

### Glitter analysis cases
case,case_name = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415'],['SP-E3SM']
# case,case_name = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519'],['E3SM']
# case, casename = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415','earlyscience.FC5AV1C-L.ne30.E3SM.20190519'], ['SP-E3SM','E3SM']

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_grid/glitter.budget.profile.v1"


years,months,num_files = [],[],1
# years,months,num_files = [1,2,3,4,5],[],0

lat1 = -10
lat2 =  10

# var = ['W_ddpQ','Wb_ddpQb','Wb_ddpQp','Wp_ddpQb','Wp_ddpQp']
var = ['Wb_ddpQp','Wp_ddpQb','Wp_ddpQp']

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
wks = ngl.open_wks(fig_type,fig_file)
plot = []

res = hs.res_xy()
res.xyLineThicknessF       = 12
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.trYReverse = True

lres = hs.res_xy()
lres.xyLineThicknessF      = 1
lres.xyDashPattern         = 0
lres.xyLineColor           = 'black'


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# file_path = '/project/projectdirs/m3312/whannah/earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415/atm/' \
#            +'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415.cam.hgb.0001-01-01-00000.nc'
# ds = xr.open_dataset(file_path)
# print()
# hc.print_stat( ds['W_ddpQ']  *1e3*86400. , name='W_ddpQ' )
# hc.print_stat( ds['Wb_ddpQb']*1e3*86400. , name='Wb_ddpQb' )
# hc.print_stat( ds['Wb_ddpQp']*1e3*86400. , name='Wb_ddpQp' )
# hc.print_stat( ds['Wp_ddpQb']*1e3*86400. , name='Wp_ddpQb' )
# hc.print_stat( ds['Wp_ddpQp']*1e3*86400. , name='Wp_ddpQp' )
# print()
# exit()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var = len(var)
data_list = []
for c in range(num_case):
   print('  case: '+case[c])
   case_obj = he.Case( name=case[c] )

   if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
   if 'lat2' in vars() : case_obj.lat2, case_obj.lat2 = lat2, lat2

   for v in range(num_var):
      area = case_obj.load_data( "area", htype='h0',   years=years, months=months, num_files=num_files).astype(np.double)
      X    = case_obj.load_data( var[v], htype='hgb', years=years, months=months, num_files=num_files)
      lev = X['lev'].values

      X = ( (X*area).sum(dim='ncol',skipna=True) / area.sum(dim='ncol') ).mean(dim='time',skipna=True)

      X = X * -1. * 1e3 * 86400.

      hc.print_stat( X*-1. , name=var[v] )

      data_list.append( X.values )

#----------------------------------------------------------------------------
# Create plot
#----------------------------------------------------------------------------

# Y = np.stack(data_list)
Y = np.ma.masked_invalid( np.stack(data_list) )

res.trXMinF = Y.min()
res.trXMaxF = Y.max()

res.tiYAxisString = '[hPa]'
res.tiXAxisString = '[g/kg/day]'

# res.xyLineColors  = ['black','blue','red']
# res.xyLineColors  = ['red','green','blue','red','green','blue']
# res.xyLineColors  = ['black','gray','gray','red','green','blue']
# res.xyDashPatterns = [0,0,0,1,1,1]
# res.xyDashPattern = c

# plot.append( ngl.xy(wks,Y,lev,res) )

for v in range(num_var) :
   vals = [  ]
   # for i in range(v,(num_var-1)*num_case+v,num_var) : vals.append(i)
   vals = [v]

   res.xyDashPatterns = [0,1,2]
   plot.append( ngl.xy(wks,Y[vals,:],lev,res) )

   ngl.overlay(plot[len(plot)-1], ngl.xy(wks,lev*0.,lev,lres) )
   
   hs.set_subtitles(wks, plot[v], '', '', var[v],font_height=0.015)


#----------------------------------------------------------------------------
# Add vertical line at zero
#----------------------------------------------------------------------------

# ngl.overlay(plot[0], ngl.xy(wks,lev*0.,lev,lres) )
# hs.set_subtitles(wks, plot[v], '', '', var[v],font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [len(plot),1]
layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------