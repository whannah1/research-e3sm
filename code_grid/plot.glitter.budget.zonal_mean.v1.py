#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import filter_factory as ff
home = os.getenv('HOME')
print()

### Glitter analysis cases
# case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']
# case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519']
case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519','earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']
case_name = ['E3SM','SP-E3SM']


fig_file = home+'/Research/E3SM/figs_grid/glitter.budget.zonal_mean.v1'

dlat = 5
lat1 = -85.
lat2 =  85.

debug = False
calculate = True

if debug:
   years, months, num_files = [],[],1
else:
   years, months, num_files = [1,2,3,4,5],[],0



var1 = ['Wb_ddpQb_ci','PRECTb']
clr1 = ['red','blue']
dsh1 = [0,0]
var2 = ['Wb_ddpQp_ci','Wp_ddpQb_ci','Wp_ddpQp_ci','PRECTp']
clr2 = ['orange','cyan','green','purple']
dsh2 = [0,0,0,0,0]
num_grp = 2
right_str = ['Filtered Tendencies','Grid Imprinting Tendencies']

# var1 = var1[::-1]
# clr1 = clr1[::-1]
# dsh1 = dsh1[::-1]
# var2 = var2[::-1]
# clr2 = clr2[::-1]
# dsh2 = dsh2[::-1]

# var1 = ['Wp_ddpQb_ci-PRECTp','Wp_ddpQb_ci','PRECTp']
# clr1 = ['black','red','blue']
# num_grp = 1
# right_str = ['']

# var1 = ['PRECT']
# num_grp = 1
# right_str = ['','']


# var1 = ['glitter_sum','PRECTp']
# clr = ['red','blue']
# num_grp = 1

# normalize = False

if 'case_name' not in vars() : exit('ERROR: case_name not defined')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4

res.tiXAxisString = 'Latitude'
res.tiYAxisString = 'Column Total Water Tendency [mm/day]'


if 'dsh1' not in vars(): dsh1 = [0]      *len(var1)
if 'clr1' not in vars(): clr1 = ['black']*len(var1)

if 'var2' in vars(): 
   if 'dsh2' not in vars(): dsh2 = [0]      *len(var2)
   if 'clr2' not in vars(): clr2 = ['black']*len(var2)

temp_dir = home+'/Research/E3SM/data_temp'
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)
for g in range(num_grp) :
   if g==0 : var,clr,dsh = var1,clr1,dsh1
   if g==1 : var,clr,dsh = var2,clr2,dsh2
   num_var  = len(var)
   data_list = []
   for c in range(num_case):
      print('  case: '+case[c])
      case_obj = he.Case( name=case[c] )

      #----------------------------------------------------------------------------
      # Load data and zonally average
      #----------------------------------------------------------------------------
      for v in range(num_var):
         tmp_file = temp_dir+'/glitter.budget.zonal_mean.v1.'+case[c]+'.'+var[v]+'.nc'
         print('    var: '+var[v]+'       ( '+tmp_file+' )')


         if calculate :

            lat  = case_obj.load_data('lat',  htype='h0')
            area = case_obj.load_data('area').astype(np.double)
            
            derived = False

            if var[v]=='glitter_sum':
               X =   case_obj.load_data('Wb_ddpQp_ci', htype='hgb0', years=years, months=months, num_files=num_files )  
               X = X+case_obj.load_data('Wp_ddpQb_ci', htype='hgb0', years=years, months=months, num_files=num_files )  
               X = X+case_obj.load_data('Wp_ddpQp_ci', htype='hgb0', years=years, months=months, num_files=num_files )  
               X = X * -1. / 100.
               derived = True

            if var[v]=='Wp_ddpQb_ci-PRECTp':
               X =   case_obj.load_data('Wp_ddpQb_ci', htype='hgb0', years=years, months=months, num_files=num_files )  
               X = X * -1. / 100.
               X = X-case_obj.load_data('PRECTp', htype='hgb0', years=years, months=months, num_files=num_files )  
               derived = True
            
            if not derived :
               X = case_obj.load_data(var[v], htype='hgb0', years=years, months=months, num_files=num_files )  
               if 'PRECT' in var[v] or 'LHFLX' in var[v] : X = X * -1.
               if 'ddpQ'  in var[v] : X = X * -1.      
               if '_ci'   in var[v] : X = X / 100. # vert derivative is per hPa instead of Pa


            # file_path = '/project/projectdirs/m3312/whannah/earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415/atm/'
            # file_path = file_path + 'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415.cam.hgb0.0001-*'
            # ds = xr.open_mfdataset( file_path )
            # X = ds[var[v]]
         
            bin_ds = hc.bin_YbyX( X.mean(dim='time'), lat, bin_min=lat1, bin_max=lat2, bin_spc=dlat, wgt=area )

            # if normalize :
            #    norm_var = ''
            #    if var[v] in ['glitter_sum']     : norm_var = 'W_ddpQ_ci'
            #    if var[v] in ['PRECTb','PRECTp'] : norm_var = 'PRECT'
            #    if var[v] in ['LHFLXb','LHFLXp'] : norm_var = 'LHFLX'
            #    if norm_var != '' :
            #       N = case_obj.load_data(norm_var, htype='hgb0', years=years, months=months, num_files=num_files ) 
            #       if 'PRECT' in norm_var or 'LHFLX' in var[v] : N = N * -1.
            #       if 'ddpQ'  in norm_var : N = N * -1.      
            #       if '_ci'   in norm_var : N = N / 100. # vert derivative is per hPa instead of Pa
            #       # X = X / np.absolute( N )
            #       # X = X / N
            #       # X.values = np.ma.masked_outside(X.values,-10.,10.)
            #       norm_bin_ds = hc.bin_YbyX( N.mean(dim='time'), lat, bin_min=lat1, bin_max=lat2, bin_spc=dlat, wgt=area )
            #       bin_ds['bin_val'].values = bin_ds['bin_val'].values / norm_bin_ds['bin_val'].values
            #       bin_ds['bin_val'].values = np.ma.masked_outside( bin_ds['bin_val'].values ,-1.,1.)
            #    else:
            #       exit('ERROR: norm_var not defined for var = '+var[v])
         
            #-------------------------------------------------------------------
            # Write to file 
            #-------------------------------------------------------------------
            bin_ds.to_netcdf(path=tmp_file,mode='w')
         else:
            bin_ds = xr.open_mfdataset( tmp_file )
         

         data_list.append( bin_ds['bin_val'].values )

         lat_bins = bin_ds['bins'].values

         #----------------------------------------------------------------------------
         # Check significance
         #----------------------------------------------------------------------------
         # print()
         # tval_crit = 2.24   # 2-tail test w/ inf dof & P=0.05
         # for b in range(len(bin_ds['bins'])) :

         #    conf = tval_crit * bin_ds['bin_val'].values[b] / np.sqrt( bin_ds['bin_cnt'].values[b] )

         #    up = bin_ds['bin_val'].values[b] + conf
         #    dn = bin_ds['bin_val'].values[b] - conf

         #    msg = '  b:{0:3}   {1:5.1}  '.format(b,bin_ds['bins'].values[b])
         #    if up>0. and dn<0. :
         #       msg = msg+' NOT SIGNIFICANT!'
         #    print(msg)
         # print()

   # hc.printline()
   # for tmp in data_list : print('    '+str(tmp))
   # hc.printline()

   #---------------------------------------------------------------------------------------------------
   #---------------------------------------------------------------------------------------------------

   Y = np.ma.masked_invalid( np.stack(data_list) )

   buff = np.absolute( Y.max() - Y.min() ) * 0.1
   res.trYMinF = Y.min() - buff
   res.trYMaxF = Y.max() + buff

   ### use this for putting all lines on single panel with good y-axis range
   # res.xyMonoLineColor = True
   # res.xyLineColor = -1
   # plot.append( ngl.xy(wks,lat_bins,Y,res) )

   for c in range(num_case):
      res.xyMonoLineColor = False
      # res.xyDashPattern = c
      res.xyDashPatterns = dsh
      res.xyLineColors   = clr
      y1 = num_var*(c)
      y2 = num_var*(c+1)#-1
      # ngl.overlay(plot[len(plot)-1], ngl.xy(wks,lat_bins,Y[y1:y2,:],res) )

      # plot.append( ngl.xy(wks,lat_bins,Y[y1:y2,:],res) )

      sin_lat_bins = np.sin( lat_bins * np.pi/180. )
      res.trXMinF = np.min( sin_lat_bins )
      res.trXMaxF = np.max( sin_lat_bins )

      lat_tick = np.array([-90.,-60.,-30.,0.,30.,60.,90.])
      res.tmXBMode = 'Explicit'
      res.tmXBValues = np.sin( lat_tick * np.pi/180. )
      res.tmXBLabels = lat_tick
      res.trXMinF = -1. #min(sin_lat)
      res.trXMaxF =  1. #max(sin_lat)

      plot.append( ngl.xy(wks,sin_lat_bins,Y[y1:y2,:],res) )

      # hs.set_subtitles(wks, plot[len(plot)-1], '', '', case_name[c],font_height=0.015)
      # if g==0 : right_str = 'Filtered Tendencies'
      # if g==1 : right_str = 'Grid Imprinting Tendencies'
      hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], '', right_str[g],font_height=0.015)


      # Add horz line at 0
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      xx = [-1e3,1e3]
      yy = [0.,0.]
      ngl.overlay(plot[len(plot)-1],ngl.xy(wks,xx,yy,lres))


hs.set_plot_labels(wks, plot, font_height=0.015, justify='right')

#---------------------------------------------------------------------------------------------------
# Add Legend
#---------------------------------------------------------------------------------------------------
# bv = 28
# Qh = 16
# Wh = 20
# Wstr = '~F33~w~F21~'
# Qstr = 'q~B1~t~N~ '
# Qprm = 'd'+Qstr+'\''
# Wprm = '-'+Wstr+'\''
# Qbar =  '~H+'+str(Qh)+'~d~V+'+str(bv)+'~_~H-'+str(Qh)+'~~V-'+str(bv)+'~'+Qstr
# Wbar = '-~H+'+str(Wh)+'~~V+'+str(bv)+'~_~H-'+str(Wh)+'~~V-'+str(bv)+'~'+Wstr

bv = 28
Ph = 18
Qh = 16
Wh = 20
Wstr = '~F33~w~F21~'
Qstr = 'q~B1~t~N~ '
partial = '~F34~6~F21~'+'~B1~p~N~'
dQ   = partial+Qstr
Qprm = partial+' '+Qstr+'\''
Wprm = ''+Wstr+'\''
Qbar = partial+'~H+'+str(Qh)+'~~V+'+str(bv)+'~_~H-'+str(Qh)+'~~V-'+str(bv)+'~'+Qstr
Wbar = '~H+'+str(Wh)+'~~V+'+str(bv)+'~_~H-'+str(Wh)+'~~V-'+str(bv)+'~'+Wstr
# Wbar = '~H+10~-~H-10~'+Wbar
dPstr = '~F34~6~F21~p'



for g in range(num_grp) :
   if g==0 : var,clr,dsh = var1,clr1,dsh1
   if g==1 : var,clr,dsh = var2,clr2,dsh2
   var_labels = []
   for v in range(len(var)):
      var_labels.append( var[v] )

      # if var[v]=='PRECTb'      : var_labels[v] = ' ~H+'+str(Wh)+'~~V+'+str(bv)+'~_~H-'+str(Wh)+'~~V-'+str(bv)+'~'+'P'
      # # if var[v]=='PRECTb'      : var_labels[v] = ' P'
      # if var[v]=='PRECTp'      : var_labels[v] = ' P\''
      # if var[v]=='W_ddpQ_ci'   : var_labels[v] = ' '+Wstr+' '+Qstr+'/dp'
      # if var[v]=='Wb_ddpQb_ci' : var_labels[v] = ' '+Wbar+'' +Qbar+'/dp'
      # if var[v]=='Wb_ddpQp_ci' : var_labels[v] = ' '+Wbar+' '+Qprm+'/dp'
      # if var[v]=='Wp_ddpQb_ci' : var_labels[v] = ' '+Wprm+'' +Qbar+'/dp'
      # if var[v]=='Wp_ddpQp_ci' : var_labels[v] = ' '+Wprm+' '+Qprm+'/dp'
      # if var[v]=='glitter_sum' : var_labels[v] = ' '+Wbar+' '+Qprm+'/dp ' \
      #                                           +' '+Wprm+'' +Qbar+'/dp ' \
      #                                           +' '+Wprm+' '+Qprm+'/dp ' 

      if var[v]=='PRECT'       : var_labels[v] = '  P'
      if var[v]=='PRECTb'      : var_labels[v] = ' ~H+'+str(Ph)+'~~V+'+str(bv)+'~_~H-'+str(Ph)+'~~V-'+str(bv)+'~'+'P'
      if var[v]=='PRECTp'      : var_labels[v] = '  P\''
      if var[v]=='W_ddpQ_ci'   : var_labels[v] = ' -'   +'~F18~O~F21~'       +Wstr+' '+Qstr+'~F18~P~F21~'  
      if var[v]=='Wb_ddpQb_ci' : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~' +Wbar+' '+Qbar+'~F18~P~F21~'  
      if var[v]=='Wb_ddpQp_ci' : var_labels[v] = '~H+10~-~F18~O~F21~~H-10~' +Wbar+' '+Qprm+'~F18~P~F21~'  
      if var[v]=='Wp_ddpQb_ci' : var_labels[v] = ' -'   +'~F18~O~F21~'       +Wprm+' '+Qbar+'~F18~P~F21~'  
      if var[v]=='Wp_ddpQp_ci' : var_labels[v] = ' -'   +'~F18~O~F21~'       +Wprm+' '+Qprm+'~F18~P~F21~'  
      if var[v]=='glitter_sum' : var_labels[v] = ' ~H+10~-~F18~O~F21~~H-10~'+Wbar+' '+Qprm+'~F18~P~F21~' \
                                                +' -~F18~O~F21~'+Wprm+'' +Qbar+'~F18~P~F21~' \
                                                +' -~F18~O~F21~'+Wprm+' '+Qprm+'~F18~P~F21~' 
      if var[v]=='Wp_ddpQb_ci-PRECTp': var_labels[v] = ' -~F18~O~F21~'+Wprm+' '+Qbar+'~F18~P~F21~ - P\''

   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.1
   # lgres.vpHeightF          = 0.07
   lgres.vpHeightF          = 0.04*len(var)
   lgres.lgLabelFontHeightF = 0.012
   # lgres.lgMonoDashIndex    = True
   lgres.lgDashIndexes      = dsh
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 12
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr

   if g==0 : gx, gy = 0.09, 0.82+0.01
   # if g==1 : gx, gy = 0.09, 0.3
   if g==1 : gx, gy = 0.09, 0.47+0.01

   pid = ngl.legend_ndc(wks, len(var_labels), var_labels, gx, gy, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [len(plot),1]
# layout = [1,len(plot)]
layout = [num_grp,num_case]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------