# define all possible categories of neighbor cell configurations (using sign of diff from center) 
# to check for existence of checkboard as a pattern that occurs too often
# v1 - ???
import os, ngl, sys, numba, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import filter_factory as ff, pg_checkerboard_utilities as pg

case,name,clr,dsh,mrk = [],[],[],[],[]
def add_case(case_in,n=None,d=0,m=1,c='black'):
   global name,case,clr,dsh,mrk
   if n is None: n = '' 
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)

case_head = 'E3SM.VT-TEST.ne30pg2_ne30pg2'
crm_config = 'CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_1'

# add_cased('ERA5'); name.append('ERA5')
# add_case('MAC');  name.append('MAC')
add_case(f'CESM.f09_g17.FSPCAMS'              ,n='SPCAM-FV',c='red')
add_case(f'CESM.ne30pg2_ne30pg2_mg17.FSPCAMS' ,n='SPCAM-SE',c='orange')
# add_case('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00');    name.append('E3SM')
add_case(f'{case_head}.F-MMF1.{crm_config}.00'    ,n='MMF'    ,c='blue')
add_case(f'{case_head}.F-MMF1.{crm_config}.BVT.00',n='MMF+BVT',c='cyan')
# add_case(f'{case_head}.F-MMF1.{crm_config}.FVT_08.00');  name.append('MMF+FVT8')
# add_case(f'{case_head}.F-MMFXX.{crm_config}.00');        name.append('MMFXX')
# add_case(f'{case_head}.F-MMFXX.{crm_config}.BVT.00');    name.append('MMFXX+BVT')
# add_case(f'{case_head}.F-MMFXX.{crm_config}.FVT_08.00'); name.append('MMFXX+FVT8')

# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20'); name.append('SPCAM-FV L20')
# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22'); name.append('SPCAM-FV L22')
# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_38'); name.append('SPCAM-FV L38')
# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_40'); name.append('SPCAM-FV L40')
# case.append('CESM.ne30pg2_ne30pg2_mg17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20'); name.append('SPCAM-SE 64x2km L20')
# case.append('CESM.ne30pg2_ne30pg2_mg17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22'); name.append('SPCAM-SE 64x2km L22'); clr.append('red')

# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20.CRMNZ_20'); name.append('E3SM-MMF L20')
# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22.CRMNZ_22'); name.append('E3SM-MMF L22'); clr.append('blue')
# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_38.CRMNZ_38'); name.append('E3SM-MMF L38')
# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_40.CRMNZ_40'); name.append('E3SM-MMF L40')

var = []
# var.append('PRECT')
var.append('TGCLDLWP')
# var.append('TMQ')
# var.append('U850')

# lat1,lat2 = -50,50
xlat,xlon,dy,dx = 0,180,30,50; lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/grid.autocorrelation.v1'

htype,first_file,num_files = 'h1',0,30

max_lag = 24*4

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var  = len(var)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)

res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
# res.xyMarkerSizeF = 0.008
# res.xyMarker = 16

# res.tmXBAutoPrecision = False
# res.tmXBPrecision = 2

# res.tmXBOn = False
# res.tmYLOn = False
# res.lbTitlePosition = 'Bottom'
# res.lbTitleFontHeightF = 0.01
# res.lbTitleString = 'Count'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def CESM_FV(case):
   CESM_FV = False
   if 'CESM' in case and any([g in case for g in ['f09','f19']]): CESM_FV = True
   return CESM_FV

def get_comp(case):
   comp = 'eam'
   if case=='ERA5': comp = None
   if case=='MAC': comp = None
   if 'CESM' in case: comp = 'cam'
   if case=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# @numba.njit()
def autocorr(data,length):
# def acf(x, length=20):
   shp = data.shape
   num_dim = len(shp)
   corr = np.zeros(length)
   corr[0] = 1.

   for i in range(1,length):
      #-------------------------------------------------------------------------
      if num_dim==1:
         corr[i] = np.corrcoef( data[:-i], data[i:] )[0,1]
      #-------------------------------------------------------------------------
      if num_dim==2:
         cnt = 0
         for j in range(shp[1]):
            s1 = np.std(data[:-i,j])
            s2 = np.std(data[i:,j])
            if s1!=0 and s2!=0:
               corr[i] = corr[i] + np.corrcoef( data[:-i,j], data[i:,j] )[0,1]
               cnt += 1
         corr[i] = corr[i]/cnt
      #-------------------------------------------------------------------------
      if num_dim==3:
         cnt = 0
         for j in range(shp[1]):
            for k in range(shp[2]):
               s1 = np.std(data[:-i,j,k])
               s2 = np.std(data[i:,j,k])
               if s1!=0 and s2!=0:
                  corr[i] = corr[i] + np.corrcoef( data[:-i,j,k], data[i:,j,k] )[0,1]
                  cnt += 1
         corr[i] = corr[i]/cnt
      #-------------------------------------------------------------------------
   return corr

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var) :
   print('  var: '+hc.tcolor.GREEN+f'{var[v]:10}'+hc.tcolor.ENDC )
   corr_list = []
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for c in range(num_case):
      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
      # case_obj = he.Case( name=case[c], time_freq='daily' )
      case_obj = he.Case( name=case[c] )

      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      data = case_obj.load_data(var[v],htype=htype,component=get_comp(case[c]),
                                   num_files=num_files,first_file=first_file,
                                   # years=years,months=months,#lev=lev,
                                   # use_remap=use_remap,remap_str=remap_str
                                   )
      
      hc.print_time_length(data.time,indent=' '*6)

      corr = autocorr(data.values,length=max_lag) 

      corr_list.append(corr)

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------

   tres = copy.deepcopy(res)
   data_min = np.min([np.nanmin(d) for d in corr_list])
   data_max = np.max([np.nanmax(d) for d in corr_list])
   tres.trYMinF = data_min
   tres.trYMaxF = data_max

   ip = v

   for c in range(num_case):
      tres.xyLineColor = clr[c]
      tres.xyDashPattern = dsh[c]
      # tres.xyMarkerColor = clr[c]
      

      tplot = ngl.xy(wks, np.arange(max_lag), corr_list[c], tres)

      
      if c==0:
         plot[ip] = tplot
      else:
         ngl.overlay(plot[ip],tplot)


   hs.set_subtitles(wks, plot[ip], '', '', var[v], font_height=0.01)

   ### add vertical line
   # lres = hs.res_xy()
   # lres.xyLineThicknessF = 1
   # lres.xyDashPattern = 0
   # lres.xyLineColor = 'black'
   # ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

# num_plot_col = 1
# layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
layout = [1,len(plot)]

pnl_res = hs.setres_panel()
# pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------