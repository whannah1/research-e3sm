import os, ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
home = os.getenv('HOME')
print()

case =[
      f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      ]
name,clr,dsh = [],[],[]
for c in case:
   if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2'); dsh.append(0); clr.append('red')
   if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3'); dsh.append(0); clr.append('green')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4'); dsh.append(0); clr.append('blue')
   if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2'); dsh.append(0); clr.append('red')

var = ['DYN_OMEGA']

fig_type = 'png'
fig_file = home+'/Research/E3SM/figs_grid/grid.pdf.dyn_omega.v1'

htype,ps_htype,years,first_file,num_files = 'h2','h1',[3,4],0,0
# htype,ps_htype,years,first_file,num_files = 'h2','h1',[],0,1

recalculate = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
wks = ngl.open_wks(fig_type,fig_file)
plot = []

res = hs.res_xy()
res.vpHeightF              = 0.3
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.tiXAxisFontHeightF     = 0.01
res.tiYAxisFontHeightF     = 0.01

res.trXReverse = True
res.trYLog = True

# res.xyLineThicknessF       = 12
# res.xyMarkLineMode         = 'MarkLines'
# res.xyMarker               = 16
# res.xyMarkerSizeF          = 0.015


if 'name' not in locals(): name = case
if 'ps_htype' not in locals(): ps_htype = htype

area_bins = xr.DataArray([ 0.0000, 0.000100, 0.000300, 0.000600 ])

bin_min, bin_max, bin_spc = -2.5, -0.05, 0.05

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(len(var)):
   print('  var: '+var[v])
   for c in range(len(case)):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )

      temp_dir = os.getenv('HOME')+"/Research/E3SM/data_temp"
      pdf_tmp_file = temp_dir+f'/pdf.dyn_omega.v1.{case[c]}.{var[v]}.nc'
      # .lat1_{lat1}.lat2_{lat2}.lon1_{lon1}.lon2_{lon2}
      print('\n    pdf_tmp_file: '+pdf_tmp_file+'\n')

      if recalculate :
         #----------------------------------------------------------------------
         # read the data
         #----------------------------------------------------------------------
         # if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
         # if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

         area_name = 'area'
         if any(pg in case[c] for pg in ['pg2','pg3','pg4'] ):
            if 'DYN_' in var[v] : area_name='area_d';case_obj.ncol_name='ncol_d'

         data = case_obj.load_data(var[v],htype=htype,ps_htype=ps_htype,years=years,first_file=first_file,num_files=num_files)
         area = case_obj.load_data(area_name,htype=htype,years=years,first_file=first_file,num_files=num_files)

         # focus on tropospheric data
         data = data.isel(lev=slice(20,72))

         #----------------------------------------------------------------------
         # bin the data by cell area
         #----------------------------------------------------------------------         
         Y_list,X_list = [],[]
         nbin = len(area_bins)-1
         for b in range(nbin):

            bin_bot = area_bins[b]  .values
            bin_top = area_bins[b+1].values
            condition = xr.DataArray( np.full(area.shape,False,dtype=bool), coords=area.coords )
            condition.values = ( area.values >=bin_bot )  &  ( area.values < bin_top )

            data_tmp = data.where(condition,drop=True)

            bin_ds = hc.bin_YbyX( data_tmp, data_tmp, 
                                  bin_min=bin_min, bin_max=bin_max, 
                                  bin_spc=bin_spc, keep_lev=False )

            X_list.append( bin_ds['bins'].values )
            Y_list.append( bin_ds['bin_cnt'].values / bin_ds['bin_cnt'].sum().values ) 

         #----------------------------------------------------------------------
         # Write to file
         #----------------------------------------------------------------------
         print()
         print( np.array(X_list).shape )
         print()
         tmp_ds = xr.Dataset()
         # tmp_ds['bin_val'] = ( ('bins','lev'), bin_val )
         tmp_ds['bin_min'] = bin_min
         tmp_ds['bin_max'] = bin_max
         tmp_ds['bin_spc'] = bin_spc
         tmp_ds['area_bins'] = area_bins
         tmp_ds['X_list'] = ( ('node','bins'), np.array(X_list) )
         tmp_ds['Y_list'] = ( ('node','bins'), np.array(Y_list) )
         tmp_ds.to_netcdf(path=pdf_tmp_file,mode='w')
      else:
         tmp_ds = xr.open_dataset( pdf_tmp_file )
         X_list = tmp_ds['X_list']
         Y_list = tmp_ds['Y_list']
      #-------------------------------------------------------------------------
      # Create plot - 
      #-------------------------------------------------------------------------
      if not recalculate: 
         res.trYMinF = np.min([np.min(d) for d in Y_list])
         res.trYMaxF = np.max([np.max(d) for d in Y_list])
         res.xyLineColors = ['red','green','blue']
         X = np.stack(X_list)
         Y = np.stack(Y_list)
         Y = np.ma.masked_where(Y==0,Y)

         ### get rid of edge node data
         res.xyLineColors = ['red','blue']
         X = X[[0,2],:]
         Y = Y[[0,2],:]

         # # separate panel for each case
         # plot.append( ngl.xy(wks, X, Y, res) )
         # hs.set_subtitles(wks, plot[len(plot)-1], name[c], '', var[v],font_height=0.015)

         # Overlay cases
         res.xyDashPattern = c
         tplot = ngl.xy(wks, X, Y, res)
         if c==0: plot.append( tplot )
         if c>0:  ngl.overlay( plot[len(plot)-1], tplot )
         hs.set_subtitles(wks, plot[len(plot)-1], '', '', var[v],font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
if not recalculate: 

   # if 'name' in vars():
   #    lgres = ngl.Resources()
   #    lgres.vpWidthF          = 0.1
   #    lgres.vpHeightF         = 0.12
   #    # lgres.lgOrientation     = "Horizontal"
   #    # lgres.lgLabelAlignment   = "AboveItems"
   #    lgres.lgLabelFontHeightF = 0.015
   #    lgres.lgMonoDashIndex    = True
   #    lgres.lgLineLabelsOn     = False
   #    lgres.lgLineThicknessF   = 12
   #    lgres.lgLineColors       = clr
   #    pid = ngl.legend_ndc(wks, len(name), name, 0.2, 0.5, lgres)


   layout = [len(plot),1]
   ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
   ngl.end()

   hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------