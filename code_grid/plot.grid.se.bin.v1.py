import os, ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
home = os.getenv('HOME')
print()

case =[
      f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      # f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      ]
name,clr,dsh = [],[],[]
for c in case:
   if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2'); dsh.append(0); clr.append('red')
   if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3'); dsh.append(0); clr.append('green')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4'); dsh.append(0); clr.append('blue')
   if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2'); dsh.append(0); clr.append('red')

# var = ['PRECT']
var = ['DYN_OMEGA']
# var = ['Kh_zt']

# lev = 700
lev = -58  # ~ 860 hPa
# lev = -45  # ~ 492 hPa

fig_type = 'png'
fig_file = home+'/Research/E3SM/figs_grid/grid.se.bin.v1'

# lat1,lat2 = -60,60
# lon1,lon2 = -140,-50

# num_years = 5
# htype,ps_htype,years,first_file,num_files = 'h2','h1',[],int(365/5),6
htype,ps_htype,years,first_file,num_files = 'h2','h1',[3],0,0

recalculate = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

wks = ngl.open_wks(fig_type,fig_file)
plot = []

res = hs.res_xy()
res.vpHeightF              = 0.3
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.tiXAxisFontHeightF     = 0.01
res.tiYAxisFontHeightF     = 0.01

res.xyLineThicknessF       = 12
res.xyMarkLineMode         = 'MarkLines'
res.xyMarker               = 16
res.xyMarkerSizeF          = 0.015

if 'ne4'   in case[0] : area_bins = [ 0.0000, 0.006000, 0.015000, 0.030000 ] 
if 'ne30'  in case[0] : area_bins = [ 0.0000, 0.000100, 0.000300, 0.000600 ] 
if 'ne120' in case[0] : area_bins = [ 0.0000, 0.000007, 0.000015, 0.000050 ] 

if 'ps_htype' not in locals(): ps_htype = htype
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(len(var)):
   print('  var: '+var[v])
   Y_list,X_list = [],[]
   for c in range(len(case)):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )

      temp_dir = os.getenv('HOME')+"/Research/E3SM/data_temp"
      bin_tmp_file = temp_dir+f'/grid.se.bin.v1.{case[c]}.{var[v]}.nc'
      print('\n    bin_tmp_file: '+bin_tmp_file+'\n')

      if recalculate :
         #----------------------------------------------------------------------
         # read the data
         #----------------------------------------------------------------------
         # if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
         # if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2

         area_name = 'area'
         if any(pg in case[c] for pg in ['pg2','pg3','pg4'] ):
            if 'DYN_' in var[v] : area_name='area_d';case_obj.ncol_name='ncol_d'
            if 'DIV'  in var[v] : area_name='area_d';case_obj.ncol_name='ncol_d'

         data = case_obj.load_data(var[v],htype=htype,ps_htype=ps_htype,years=years,first_file=first_file,num_files=num_files,lev=lev)
         area = case_obj.load_data(area_name,htype=htype,years=years,first_file=first_file,num_files=num_files)

         if 'lev' in data.coords:
            if data['lev'].size>1 : data = data.isel(lev=0)

         hc.print_stat(data,name=var[v],indent='    ')
         # exit()

         #----------------------------------------------------------------------
         # bin the data by cell area
         #----------------------------------------------------------------------

         bin_ds = hc.bin_YbyX( data, area, bins=area_bins, bin_mode='explicit' )

         #----------------------------------------------------------------------
         # write to tmp file
         #----------------------------------------------------------------------
         bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( bin_tmp_file )

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      X = bin_ds['bins'].values
      Y = bin_ds['bin_val'].values

      #-------------------------------------------------------------------------
      # Check significance
      #-------------------------------------------------------------------------
      differential = bin_ds['bin_val'].values[0] - bin_ds['bin_val'].values[2]
      
      # Sample sizes and standard deviations
      n1 = bin_ds['bin_cnt'].values[0]
      n2 = bin_ds['bin_cnt'].values[2]
      s1 = bin_ds['bin_std'].values[0]
      s2 = bin_ds['bin_std'].values[2]

      # Degrees of freedom
      dof = (s1**2/n1 + s2**2/n2)**2 / ( (s1**2/n1)**2/(n1-1) + (s2**2/n2)**2/(n2-1) )

      # standard error
      se = np.sqrt( (s1**2/n1) + (s2**2/n2) )

      # t-statistic
      tval = differential / se

      # Critical t-statistic for significance
      tval_crit = 1.9599   # 2-tail test w/ inf dof & P=0.5

      if np.abs(tval)>tval_crit : 
         print('    Bin differential is significant')
      else:
         print('    Bin differential is NOT significant!')

      X_list.append( X )
      Y_list.append( Y )

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   
   res.trYMinF = np.min([np.min(d) for d in Y_list])
   res.trYMaxF = np.max([np.max(d) for d in Y_list])

   for c in range(len(case)):
      case_obj = he.Case( name=case[c] )

      X = X_list[c]
      Y = Y_list[c]
      
      # res.trXMinF = area_bins[ 0].values      # these won't work with explicit tickmarks
      # res.trXMaxF = area_bins[-1].values      # these won't work with explicit tickmarks
      res.tiXAxisString = 'GLL Node Area'
      res.tiYAxisString = ''
      if var[v]=='PRECT' : res.tiYAxisString = '[mm/day]'
      res.xyLineColor   = clr[c]
      res.xyMarkerColor = clr[c]
      res.tmXBMode      = 'Explicit'
      res.tmXBLabels    = ['Corner','Edge','Middle']
      res.tmXBValues    = X

      res.trXMinF = 0.
      res.trXMaxF = X.max() + X.min()

      tplot = ngl.xy(wks,X,Y,res)
   
      if c == 0 :
         plot.append( tplot )
      else:
         ngl.overlay(plot[v],tplot)


      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case[c]
         # case_name = case_obj.short_name

      # hs.set_subtitles(wks, plot[len(plot)-1], case_obj.short_name, '', var)
      var_str = var[v]
      if var[v]=='PRECT' : var_str = "Precipitation"
      hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str,font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

if 'name' in vars():
   lgres = ngl.Resources()
   lgres.vpWidthF          = 0.1
   lgres.vpHeightF         = 0.15
   # lgres.lgOrientation     = "Horizontal"
   # lgres.lgLabelAlignment   = "AboveItems"
   lgres.lgLabelFontHeightF = 0.015
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 12
   lgres.lgLineColors       = clr
   pid = ngl.legend_ndc(wks, len(name), name, 0.2, 0.7, lgres)


layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------