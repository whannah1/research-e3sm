import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
home = os.getenv("HOME")
print()

### physgrid validation
name,case,clr,dsh,git_hash = [],[],[],[],'cbe53b'
# case.append(f'GPCP'); name.append('GPCP'); dsh.append(0); clr.append('black')
# case.append(f'TRMM'); name.append('TRMM'); dsh.append(0); clr.append('black')
case.append(f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
# case.append(f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
# case.append(f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
# case.append(f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
# case.append(f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
# case.append(f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}') 
for c in case:
   if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2'); dsh.append(0); clr.append('red')
   if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3'); dsh.append(0); clr.append('green')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4'); dsh.append(0); clr.append('blue')
   if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4'); dsh.append(0); clr.append('black')
   if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2'); dsh.append(0); clr.append('red')


var = ['DYN_OMEGA']

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_grid/grid.se.profile.v1"

# plev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975])
plev = np.array([50,100,150,200,300,400,500,600,700,800,850,925,950])

# lat1 =   0
# lat2 =  40
# lon1 = -140
# lon2 = -50

# htype,years,num_files = 'h0',[1,2,3,4,5],0
htype,years,num_files = 'h0',[],0

chk_significance = False


#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
wks = ngl.open_wks(fig_type,fig_file)
plot = []

res = hs.res_xy()
res.xyLineThicknessF       = 16
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.trYReverse = True

if "ne4"   in case[0] : area_bins = [ 0.0000, 0.006000, 0.015000, 0.030000 ] 
if "ne30"  in case[0] : area_bins = [ 0.0000, 0.000100, 0.000300, 0.000600 ] 
if "ne120" in case[0] : area_bins = [ 0.0000, 0.000007, 0.000015, 0.000050 ] 

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_var  = len(var)
num_case = len(case)
if 'plev' not in vars() : plev = np.array([0])
# plev = plev*1.0
for v in range(num_var):
   data_list = []
   #----------------------------------------------------------------------------
   # Loop through cases to load data without plotting
   #----------------------------------------------------------------------------
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      area_name = 'area'
      if 'DYN_' in var[v] and 'pg2' in case[c] : area_name = 'area_d'
      if 'DIV'  in var[v] and 'pg2' in case[c] : area_name = 'area_d'

      area = case_obj.load_data(area_name,htype=htype,years=years,num_files=num_files)
      data = case_obj.load_data(var[v],htype=htype,years=years,lev=plev,num_files=num_files)

      # Turn "Q" into total water
      if var[v]=='Q':
         data = data + case_obj.load_data('CLDLIQ',htype=htype,years=years,lev=plev,num_files=num_files)
         data = data + case_obj.load_data('CLDICE',htype=htype,years=years,lev=plev,num_files=num_files)
         data = data*1e3 # convert to g/kg

      
      # hc.print_stat(data, name=var[v])
      # hc.print_stat(area, name='area')
      # print( data*area )
      # exit()

      if 'ncol_d' in data.dims : data = data.rename({'ncol_d':'ncol'})
      if 'ncol_d' in area.dims : area = area.rename({'ncol_d':'ncol'})

      gbl_mean = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      if 'time' in gbl_mean.dims : gbl_mean = gbl_mean.mean(dim='time')
      print('\n    Area Weighted Global Mean : ')
      tlev = data['lev'].values
      # for k in range(len(tlev)): print('    {:8.2f}   {:12.6} '.format( tlev[k], gbl_mean.values[k]) )
      # print()

      #-------------------------------------------------------------------------
      # bin the data by cell area
      #-------------------------------------------------------------------------         
      # bin_ds = hc.bin_YbyX( data, area, bins=area_bins, bin_mode="explicit" )
      bin_ds = hc.bin_YbyX( data, area, bins=area_bins, bin_mode="explicit", wgt=area )


      # bin_ds['bin_val'] = bin_ds['bin_val'] - gbl_mean


      if c==0: lev = bin_ds['lev'].values

      # for k in range(len(tlev)): 
      #    print('    {0:8.2}   {1:12.6} {2:12.6}  {3:12.6} ' \
      #           .format( tlev[k], bin_ds['bin_val'].values[0,k] \
      #                           , bin_ds['bin_val'].values[1,k] \
      #                           , bin_ds['bin_val'].values[2,k] ) )

      ### subtract mean?
      # if var[v]=='Q' : 
      #    for k in range(len(lev)) : 
      #       bin_ds['bin_val'][:,k] = bin_ds['bin_val'][:,k] - bin_ds['bin_val'][:,k].mean()

      # print('WARNING: middle bin values are being masked out')
      # bin_ds['bin_val'][1,:] = np.nan
         
      # Append new data to list
      data_list.append( bin_ds['bin_val'].values )

      #----------------------------------------------------------------------------
      # Check significance
      #----------------------------------------------------------------------------
      if chk_significance : 
         print()
         for k in range(len(bin_ds['lev'])) :
            differential = bin_ds['bin_val'].values[0,k] - bin_ds['bin_val'].values[2,k]
            
            # Sample sizes and standard deviations
            n1 = bin_ds['bin_cnt'].values[0,k]
            n2 = bin_ds['bin_cnt'].values[2,k]
            s1 = bin_ds['bin_std'].values[0,k]
            s2 = bin_ds['bin_std'].values[2,k]

            # Degrees of freedom
            dof = (s1**2/n1 + s2**2/n2)**2 / ( (s1**2/n1)**2/(n1-1) + (s2**2/n2)**2/(n2-1) )

            # standard error
            se = np.sqrt( (s1**2/n1) + (s2**2/n2) )

            # t-statistic
            tval = differential / se

            # Critical t-statistic for significance
            tval_crit = 1.9599   # 2-tail test w/ inf dof & P=0.5

            msg = 'k:{0:03}   {1:4.2f} hPa   {2:8.4f}  '.format(k, bin_ds['lev'][k].values, bin_ds['bin_val'].values)
            if np.abs(tval)>tval_crit : msg = msg+' SIGNIFICANT!'
            print(msg)
         print()
      #----------------------------------------------------------------------------
      #----------------------------------------------------------------------------
   # Stack the data to make sure axes are set correctly
   Y = np.ma.masked_invalid( np.stack(data_list) )
   buff = np.absolute( Y.max() - Y.min() ) * 0.05
   res.trXMinF = Y.min() - buff
   res.trXMaxF = Y.max() + buff

   if var[v]=='DYN_OMEGA' : res.trXMinF = -0.0025
   #----------------------------------------------------------------------------
   # Loop back through cases and create plot
   #----------------------------------------------------------------------------
   for c in range(num_case):
      res.tiYAxisString = '[hPa]'
      res.tiXAxisString = ''
      if var[v]=='OMEGA' : res.tiXAxisString = '[hPa/s]'
      res.xyLineColor   = clr[c]
      res.xyDashPatterns = [2,1,0]

      tplot = ngl.xy(wks,Y[c,:,:],lev,res)
   
      if c == 0 :
         plot.append( tplot )
         # ngl.overlay(plot[v],ngl.xy(wks,Y*0.,lev,lres))
         # ngl.overlay(plot[v],tplot)
         # hs.set_subtitles(wks, plot[len(plot)-1], "", "", var[v])
      else:
         ngl.overlay(plot[v],tplot)

      # Add vertical line at zero
      if v == num_var-1 :
         lres = hs.res_xy()
         lres.xyLineThicknessF      = 1
         lres.xyDashPattern         = 0
         lres.xyLineColor           = 'black'
         ngl.overlay(plot[v], ngl.xy(wks,lev*0.,lev,lres) )
         hs.set_subtitles(wks, plot[v], '', '', var[v],font_height=0.015)
#---------------------------------------------------------------------------------------------------
# Add Legend
#---------------------------------------------------------------------------------------------------
lgres = ngl.Resources()
lgres.vpWidthF          = 0.1
lgres.vpHeightF         = 0.1
# lgres.vpWidthF          = 0.2
# lgres.vpHeightF         = 0.15
# lgres.lgOrientation     = "Horizontal"
# lgres.lgLabelAlignment   = "AboveItems"
lgres.lgLabelFontHeightF = 0.015
lgres.lgMonoDashIndex    = True
lgres.lgLineLabelsOn     = False
lgres.lgLineThicknessF   = 12
lgres.lgLineColors       = clr
pid = ngl.legend_ndc(wks, len(name), name, 0.2, 0.9, lgres)

lgres.vpWidthF          = 0.1
lgres.vpHeightF         = 0.12
lgres.lgMonoLineColor   = True
lgres.lgMonoDashIndex   = False
lgres.lgLineColor       = 'black'
node_names              = ['Middle','Edge','Corner']
pid = ngl.legend_ndc(wks, len(node_names), node_names, 0.2, 0.8, lgres)
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------