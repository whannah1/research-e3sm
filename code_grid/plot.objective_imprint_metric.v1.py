# create scatter plot of precip variance to show grid imprinting
# v1 - plot a cumulative time series of the norm of the distance of median values to the phase space centroid
# v2 - identify all columns on a cube face and perform sptial spectrum analysis on rows of point parallel to cube edge
# v3 - use structure functions
# v4 - compare each phsygrid cell to the difference between adjacent and diagonal cells
import os
import ngl
import xarray as xr
import numpy as np
from scipy.stats import skew
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
home = os.getenv("HOME")
print()

# case = [ 'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]

case = ['E3SM_SP1_64x1_1000m_ne30_F-EAMv1-AQP1_00' \
       ,'E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00' \
       ,'E3SM_SP1_64x1_1000m_ne30pg3_F-EAMv1-AQP1_00' \
       ] 
name = ['ne30np4','ne30pg2','ne30pg3']

clr = ['red','green','blue','purple']
# clr = ['red','orange','green','cyan','blue','purple','pink']

num_zone = 1

var1 = 'PRECT'
var2 = 'OMEGA'
# lev = np.array([700])
lev = -60

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_grid/gridobjective_imprint_metric.v1"


# lat1 =  40
# lat2 =  45

# htype,years,months,num_files,stride = 'h1',[],[],365,5
htype,years,months,num_files,stride = 'h0',[],[],12,1

# recalculate = True

temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
# res.vpHeightF = 0.4
res.xyLineThicknessF = 12
# res.xyMarkLineMode = 'Markers'
# res.xyMarker = 1


if 'clr' not in vars(): clr = ['black']*num_case
if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
# res.xyDashPatterns = dsh


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []
for c in range(num_case):
   print('  case: '+case[c])
   case_obj = he.Case( name=case[c] )
   case_name.append( case_obj.short_name )

   data_list = []
   time_list = []
   for z in range(num_zone):

      if z==0 : lat1,lat2 = 40,45
      if z==1 : lat1,lat2 = 50,55
      if z==2 : lat1,lat2 = 30,35
      if z==3 : lat1,lat2 = 20,25

      print('    zone lat: '+str(lat1)+' - '+str(lat2))

      # tmp_file = temp_dir+"/clim.precip.scatter.v1."+case[c]+"."+var+".lat1_"+str(lat1)+".lat2_"+str(lat2)+".nc"
      # print('    tmp_file: '+tmp_file )

      # if recalculate :
      if True:
         #----------------------------------------------------------------------
         # read the data
         #----------------------------------------------------------------------
         ### uncommentt this to subset the data
         if 'lev' not in vars() : lev = np.array([-1])
         case_obj.lat1 = lat1
         case_obj.lat2 = lat2
         case_obj.mirror_equator = True

         # area_name = 'area'
         # # if 'DYN_' in var[v] and 'pg2' in case[c] : area_name = 'area_d'
         # area = case_obj.load_data(area_name,htype=htype)

         # print('    Loading '+var1+'...')
         # X1 = case_obj.load_data(var1,htype=htype,lev=lev,years=years,months=months,num_files=num_files)
         # print('    Loading '+var2+'...')
         # X2 = case_obj.load_data(var2,htype=htype,lev=lev,years=years,months=months,num_files=num_files)

         # Convert to daily mean
         # print('    Resampling...')
         # X1 = X1.resample(time='D').mean(dim='time')
         # X2 = X2.resample(time='D').mean(dim='time')\

         

         # create 'data window length' coordinate for plot, start from 1
         # time = ( X1['time'][1:] - X1['time'][0] ).astype('float').values / 86400e9

         time = case_obj.load_data('time',htype=htype,years=years,months=months,num_files=num_files)
         if htype!='h0' : time = time.resample(time='D').mean(dim='time')
         time = ( time[1:] - time[0] ).astype('float').values / 86400e9
         
         # if 'lev' in X1.dims : X1 = X1.isel(lev=0)
         # if 'lev' in X2.dims : X2 = X2.isel(lev=0)

         # hc.print_stat(X1,name=var1)
         # hc.print_stat(X2,name=var2)
         # exit()

         area = case_obj.load_data("area",htype=htype,num_files=1)#.values.astype(np.float)
         
         #----------------------------------------------------------------------
         # Loop over averaging windows
         #----------------------------------------------------------------------
         num_time = len(time)+1
         norm_list = []
         
         # print('    Calculating metric...')
         for t in range(0,num_time,stride) :
            print('      t: '+str(t))
            #-------------------------------------------------------------------
            # Load data one file at a time
            #-------------------------------------------------------------------
            X1 = case_obj.load_data(var1,htype=htype,lev=lev,years=years,months=months,num_files=stride,first_file=t)
            X2 = case_obj.load_data(var2,htype=htype,lev=lev,years=years,months=months,num_files=stride,first_file=t)

            if 'lev' in X1.dims : X1 = X1.isel(lev=0)
            if 'lev' in X2.dims : X2 = X2.isel(lev=0)

            # X1 = X1.resample(time='D').mean(dim='time')
            # X2 = X2.resample(time='D').mean(dim='time')

            X1 = X1.mean(dim='time')
            X2 = X2.mean(dim='time')

            #-------------------------------------------------------------------
            # Calculate the time mean at each point
            #-------------------------------------------------------------------
            # dims   = ('ncol')
            # stat_ds = xr.Dataset()

            # M1 = X1.isel(time=slice(0,t)).mean(dim='time')
            # M2 = X2.isel(time=slice(0,t)).mean(dim='time')

            if t==0 :
               M1 = X1 
               M2 = X2 
               cnt = 1.
            else:
               # M1 = ( M1*cnt + X1.isel(time=0) )/(cnt+1)
               # M2 = ( M2*cnt + X2.isel(time=0) )/(cnt+1)
               M1 = ( M1*cnt + X1 )/(cnt+1)
               M2 = ( M2*cnt + X2 )/(cnt+1)
               cnt = cnt+1.

            #-------------------------------------------------------------------
            # Calculate the "centroid" in the phase space 
            #-------------------------------------------------------------------
            C1 = M1.mean(dim='ncol').values
            C2 = M2.mean(dim='ncol').values

            N = np.sqrt( np.sum((M1-C1)**2.) + np.sum((M2-C2)**2.) )

            #-------------------------------------------------------------------
            # Add to the cumulative time series
            #-------------------------------------------------------------------
            norm_list.append( N.values )

         #----------------------------------------------------------------------
         # Append the cumulative time series to the data list for plotting
         #----------------------------------------------------------------------
         print()
         print('  lengths: '+str(len(norm_list)) +'  '+ str(len(time[::stride])) )
         print()

         data_list.append( norm_list )
         time_list.append( time[::stride] )

      #    #----------------------------------------------------------------------
      #    # Write to file 
      #    #----------------------------------------------------------------------
      #    stat_ds.to_netcdf(path=tmp_file,mode='w')
      # else:
      #    stat_ds = xr.open_mfdataset( tmp_file )

   print('  Done.')

   print()
   hc.print_stat( np.stack(data_list) )
   print()

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   if htype!='h0' : 
      res.tiXAxisString = 'Data Window Length [days]'
   else:
      res.tiXAxisString = 'Data Window Length [months]'
   res.tiYAxisString = 'Norm'

   res.xyXStyle = "Log"
   res.xyYStyle = "Log"

   # lat_tick = np.array([-90.,-60.,-30.,0.,30.,60.,90.])
   # res.tmXBMode = 'Explicit'
   # res.tmXBValues = np.sin( lat_tick )
   # res.tmXBLabels = str( lat_tick )

   res.xyDashPattern = c
   
   tplot = ngl.xy(wks, np.stack(time_list), np.ma.masked_invalid(  np.stack(data_list) ), res)

   if c==0 :
      res.trXMinF = np.min( np.stack(time_list) )
      res.trXMaxF = np.max( np.stack(time_list) )
      plot.append( tplot )
   else:
      ngl.overlay( plot[len(plot)-1], tplot )
      
   # region_str = str(lat1)+'N : '+str(lat2)+'N'
   # hs.set_subtitles(wks, plot[len(plot)-1], '', '', region_str, font_height=0.015)
   

#-------------------------------------------------------------------------------
# Finalize plot
#-------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------