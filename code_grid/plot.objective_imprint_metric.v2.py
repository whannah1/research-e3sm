# create scatter plot of precip variance to show grid imprinting
# v1 - plot a cumulative time series of the norm of the distance of median values to the phase space centroid
# v2 - identify all columns on a cube face and perform sptial spectrum analysis on rows of point parallel to cube edge
# v3 - use structure functions
# v4 - compare each phsygrid cell to the difference between adjacent and diagonal cells
import os
import ngl
import xarray as xr
import numpy as np
from scipy.stats import skew
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
home = os.getenv("HOME")
print()

# case = [ 'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]

case = ['E3SM_SP1_64x1_1000m_ne30_F-EAMv1-AQP1_00' \
       # ,'E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00' \
       # ,'E3SM_SP1_64x1_1000m_ne30pg3_F-EAMv1-AQP1_00' \
       ] 
name = ['ne30np4','ne30pg2','ne30pg3']

clr = ['red','green','blue','purple']
# clr = ['red','orange','green','cyan','blue','purple','pink']

var1 = 'PRECT'
var2 = 'OMEGA'
# lev = np.array([700])
lev = -60

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_grid/gridobjective_imprint_metric.v2"


# lat1 =  40
# lat2 =  45

htype,years,months,num_files = 'h0',[],[],1

# recalculate = True

temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

# wks = ngl.open_wks(fig_type,fig_file)
# plot = []
# res = hs.res_xy()
# # res.vpHeightF = 0.4
# res.xyLineThicknessF = 12
# # res.xyMarkLineMode = 'Markers'
# # res.xyMarker = 1

# if 'clr' not in vars(): clr = ['black']*num_case
# if 'dsh' not in vars(): 
#    if num_case>1 : dsh = np.zeros(num_case)
#    else : dsh = [0]

# res.xyLineColors   = clr
# res.xyDashPatterns = dsh


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []
for c in range(num_case):
   print('  case: '+case[c])
   case_obj = he.Case( name=case[c] )
   case_name.append( case_obj.short_name )

   data_list = []
   #----------------------------------------------------------------------
   # Load latlon file and identify cube corners
   #----------------------------------------------------------------------
   if 'ne4_'  in case[c]: ne,npg,latlon_file =  4,4,home+'/E3SM/data_grid/ne4np4_latlon.nc'
   if 'ne30_' in case[c]: ne,npg,latlon_file = 30,4,home+'/E3SM/data_grid/ne30np4_latlon.nc'

   ds = xr.open_dataset(latlon_file)

   # Identify cube corners
   subcell_corners = ds['element_corners'].astype(int).values
   (ind,cnt) = np.unique(subcell_corners, return_counts=True) 
   cube_corner = ds['ncol'].values[np.where(cnt==3)]

   cube_corner_lat = ds['lat'][cube_corner].values
   cube_corner_lon = ds['lon'][cube_corner].values

   # for n in range(len(cube_corner)): print(f"  {cube_corner_lat[n]:6.2f}  {cube_corner_lon[n]:6.2f}")

   #----------------------------------------------------------------------
   # Pick a point and find the corners for that face
   #----------------------------------------------------------------------
   xlat = 1.
   xlon = 181.
   dist = []
   for n in range(len(cube_corner)):
      dist.append( hc.calc_great_circle_distance(xlat, cube_corner_lat[n], xlon, cube_corner_lon[n]) )
   
   # for n in range(len(dist)): print(f"  {dist[n]:6.2f}  {cube_corner_lat[n]:6.2f}  {cube_corner_lon[n]:6.2f}")

   # Use nearest cube corners to define face corner distances
   face_corner_dist = np.sort( dist )[:4]

   face_corner_lat = []
   face_corner_lon = []
   for n in range(4) :
      nn = np.where(dist==face_corner_dist[n])
      face_corner_lat.append( cube_corner_lat[nn] )
      face_corner_lon.append( cube_corner_lon[nn] )

   # for n in range(4): print(f"  {face_corner_lat[n]:6.2f}  {face_corner_lon[n]:6.2f}")
   #----------------------------------------------------------------------
   # Plot points on sphere with lines connecting corners
   #----------------------------------------------------------------------
   wks = ngl.open_wks('x11','test')
   res = hs.res_contour_fill_map()
   # res.mpOutlineBoundarySets = "NoBoundaries"
   # res.mpProjection     = "Satellite"
   plot = ngl.map(wks,res)

   exit()
   #----------------------------------------------------------------------
   # Isolate all points on the face
   #----------------------------------------------------------------------
   lat = ds['lat'].values
   lon = ds['lon'].values

   lat_min = np.min(face_corner_lat)
   lat_max = np.max(face_corner_lat)
   lon_min = np.min(face_corner_lon)
   lon_max = np.max(face_corner_lon)

   condition = np.array( [True]*len(lat) )
   condition = condition & (lat>=lat_min) & (lat<=lat_max)
   condition = condition & (lon>=lon_min) & (lon<=lon_max)
   
   if npg!=4 : expected_pts = (npg*ne)**2          # for FV physgrid
   if npg==4 : expected_pts = ((npg-1)*ne+1)**2    # for GLL grid
      
   print(f'  lat min/max:       {lat_min:6.2f} / {lat_max:6.2f}')
   print(f'  lon min/max:       {lon_min:6.2f} / {lon_max:6.2f}')
   print(f'  total points:      {len(lat)}')
   print(f'  cube face points:  {np.sum(condition)}      (should be {expected_pts})' )
   

   exit('\nExiting')

   #----------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------
   if 'lev' not in vars() : lev = np.array([-1])

   X1 = case_obj.load_data(var1,htype=htype,lev=lev,years=years,months=months,num_files=num_files)
   X2 = case_obj.load_data(var2,htype=htype,lev=lev,years=years,months=months,num_files=num_files)

   if 'lev' in X1.dims : X1 = X1.isel(lev=0)
   if 'lev' in X2.dims : X2 = X2.isel(lev=0)

   # X1 = X1.resample(time='D').mean(dim='time')
   # X2 = X2.resample(time='D').mean(dim='time')

   X1 = X1.mean(dim='time')
   X2 = X2.mean(dim='time')

   #-------------------------------------------------------------------
   # Calculate the time mean at each point
   #-------------------------------------------------------------------
   

   #----------------------------------------------------------------------
   #----------------------------------------------------------------------


   # print()
   # hc.print_stat( np.stack(data_list) )
   # print()

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   res.xyDashPattern = c
   
   tplot = ngl.xy(wks, np.stack(time_list), np.ma.masked_invalid(  np.stack(data_list) ), res)

   if c==0 :
      res.trXMinF = np.min( np.stack(time_list) )
      res.trXMaxF = np.max( np.stack(time_list) )
      plot.append( tplot )
   else:
      ngl.overlay( plot[len(plot)-1], tplot )
      
#-------------------------------------------------------------------------------
# Finalize plot
#-------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------