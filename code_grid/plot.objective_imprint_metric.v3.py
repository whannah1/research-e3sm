# create scatter plot of precip variance to show grid imprinting
# v1 - plot a cumulative time series of the norm of the distance of median values to the phase space centroid
# v2 - identify all columns on a cube face and perform sptial spectrum analysis on rows of point parallel to cube edge
# v3 - use structure functions
# v4 - compare each phsygrid cell to the difference between adjacent and diagonal cells
import os
import ngl
import xarray as xr
import numpy as np
from scipy.stats import skew
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import sys
sys.path.append('/ccs/home/hannah6/Research/E3SM')
import filter_factory as ff
home = os.getenv("HOME")
print()

case = [ \
       # '3D_CONV_TEST.GPU.ne30pg2.F-EAMv1-AQP1.SP1_64x1_1000m' \
       '3D_CONV_TEST.GPU.ne30pg2.F-EAMv1-AQP1.SP1_64x1R_1000m' \
       ,'3D_CONV_TEST.GPU.ne30pg2.F-EAMv1-AQP1.SP1_4x64_1000m' \
       ,'3D_CONV_TEST.GPU.ne30pg2.F-EAMv1-AQP1.SP1_16x64_1000m' \
       # ,'3D_CONV_TEST.GPU.ne30pg2.F-EAMv1-AQP1.SP1_64x64_1000m' \
      ]
name = [ \
       # 'MMF 64x1' \
       'MMF 1x64' \
       ,'MMF 4x64' \
       ,'MMF 16x64' \
       # ,'MMF 64x64' \
      ]

# clr = ['red','blue']
# clr = ['red','red','blue','purple']
# dsh = [0,1,0,0,0,0,0,0,0]
clr = ['red','green','blue']
# clr = ['red','orange','green','cyan','blue','purple','pink']

# var = ['PRECT','TGCLDLWP']
var = ['TGCLDLWP']
# var = ['PRECT','OMEGA']
# lev = -60
# var,lev = 'OMEGA',np.array([700])
# var,lev = 'OMEGA',-60

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_grid/objective_imprint_metric.v3"

# htype,years,months,num_files = 'h0',[],[],12
htype,years,months,num_files = 'h1',[],[],4

# recalculate = True

# lat1,lat2 =  30,50
lat1,lat2 =  10,30

temp_dir = home+"/Research/E3SM/data_temp"


# radial distance bins (km)
bin_min = 90
bin_max = 300
bin_spc = 5

plot_histogram = False

recalculate = True

verbose = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var = len(var)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
# res.vpHeightF = 0.4
res.xyLineThicknessF = 12
# res.xyMarkLineMode = 'Markers'
# res.xyMarker = 1

if 'clr' not in vars(): clr = ['black']*num_case
if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []
for c in range(num_case):
   print('case: '+case[c])
   case_obj = he.Case( name=case[c] )
   case_name.append( case_obj.short_name )

   data_list = []
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   if 'lev' not in vars() : lev = np.array([-1])
   if 'lat1' in vars(): case_obj.lat1 = lat1
   if 'lat2' in vars(): case_obj.lat2 = lat2

   case_obj.mirror_equator = True

   lat = case_obj.load_data('lat',htype=htype,num_files=1)
   lon = case_obj.load_data('lon',htype=htype,num_files=1)

   ncol = len(lat)

   #----------------------------------------------------------------------------
   # calculate distances
   #----------------------------------------------------------------------------

   num_neighbors = 20
   neighbor_obj = ff.Neighbors( lat, lon, num_neighbors+1 )

   dist = neighbor_obj._distances[:,1:]/1e3

   coords = [lat['ncol'],np.arange(0,num_neighbors).astype(int)]
   dist = xr.DataArray(dist,coords=coords,dims=['ncol','neighbors'])

   if verbose : hc.print_stat( dist ,stat='naxsh', name=f'\ndistances w/ {num_neighbors} neighbors [km]')

   #----------------------------------------------------------------------------
   # Set up distance bins
   #----------------------------------------------------------------------------
   # num_bin = 40.
   # bin_spc = dist.std() / 10
   # bin_min = dist.min()
   # bin_max = bin_min + bin_spc*num_bin

   #----------------------------------------------------------------------------
   # Plot histogram of distances
   #----------------------------------------------------------------------------
   if plot_histogram:
      bin_ds = hc.bin_YbyX( dist.stack(stack_dim=('ncol','neighbors')), \
                            dist.stack(stack_dim=('ncol','neighbors')), \
                            bin_min=bin_min, bin_max=bin_max, bin_spc=bin_spc )
      
      x_list.append(bin_ds['bins'])
      y_list.append(bin_ds['bin_cnt'])
      
      continue
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for v in range(num_var) :
      #-------------------------------------------------------------------------
      # Calculate difference of data between neighbors
      #-------------------------------------------------------------------------

      data = case_obj.load_data(var[v],htype=htype,lev=lev,num_files=num_files)#.mean(dim='time')
      # if 'lev' in X.dims : X = X.isel(lev=0)

      # diff = np.empty( dist.shape )
      diff = np.empty( (data.shape[0],data.shape[1],dist.shape[1]) )
      for i in range(ncol) :
         nn = neighbor_obj.neighbors[i,1:]
         # diff[:,i,:] = np.abs(( data[:,i] - data[:,nn] ).values)  # first order
         diff[:,i,:] = ( np.abs(( data[:,i] - data[:,nn] ).values) )**2.  # second order

      
      # diff = xr.DataArray(diff,coords=coords,dims=['ncol','neighbors'])

      coords = [np.arange(0,data.shape[0]).astype(int),lat['ncol'],np.arange(0,num_neighbors).astype(int)]
      diff = xr.DataArray(diff,coords=coords,dims=['time','ncol','neighbors'])

      if verbose : hc.print_stat(diff,stat='naxsh',name='\ndiff')

      #-------------------------------------------------------------------------
      # create scatter plot
      #-------------------------------------------------------------------------
      # sres = hs.res_xy()
      # sres.nglDraw, sres.nglFrame = True, True
      # sres.xyMarkLineMode, sres.xyMarker = 'Markers',1
      # splot = ngl.xy(wks, dist.stack(stack_dim=('ncol','neighbors')).values, \
      #                     diff.stack(stack_dim=('ncol','neighbors')).values, sres)  
      # ngl.end()
      # hc.trim_png(fig_file)
      # exit()
      #-------------------------------------------------------------------------
      # Bin the difference by distance
      #-------------------------------------------------------------------------

      bin_ds = hc.bin_YbyX( diff.stack(stack_dim=('ncol','neighbors')), \
                            dist.stack(stack_dim=('ncol','neighbors')), \
                            bin_min=bin_min, bin_max=bin_max, bin_spc=bin_spc )

      # hc.bin_YbyX( data, area, bins=area_bins, bin_mode='explicit' )
      # bin_ds = hc.bin_YbyX( X, X, bin_min=bin_min_ctr,bin_spc=bin_min_wid,    \
      #                             bin_spc_log=bin_spc_pct, nbin_log=nbin,     \
      #                             bin_mode='log', wgt=area, verbose=False )
      
      print('\nbins:')
      print(bin_ds['bins'].values)

      if verbose : hc.print_stat( bin_ds['bin_val'],name='\nbin_val' )

      x_list.append(bin_ds['bins'])
      y_list.append(bin_ds['bin_val'])

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# res.xyDashPattern = c

res.trXMinF = np.min( np.stack(x_list) )
res.trXMaxF = np.max( np.stack(x_list) )
# res.trYMinF = np.min( np.stack(Y_list) )
# res.trYMaxF = np.max( np.stack(Y_list) )

res.tiYAxisString = 'S'
res.tiXAxisString = 'R [km]'

if plot_histogram:
   res.tiYAxisString = 'Count'

plot.append( ngl.xy(wks, np.stack(x_list), np.ma.masked_invalid(  np.stack(y_list) ), res) )

# hs.set_subtitles(wks, plot[len(plot)-1], '', '', '', font_height=0.01)

# ngl.overlay( plot[len(plot)-1], tplot )
      
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------