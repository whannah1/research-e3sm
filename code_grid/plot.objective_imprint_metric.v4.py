# create scatter plot of precip variance to show grid imprinting
# v1 - plot a cumulative time series of the norm of the distance of median values to the phase space centroid
# v2 - identify all columns on a cube face and perform sptial spectrum analysis on rows of point parallel to cube edge
# v3 - use structure functions
# v4 - compare each phsygrid cell to the difference between adjacent and diagonal cells
import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import sys
sys.path.append(os.getenv("HOME")+'/Research/E3SM')
import filter_factory as ff
import numba
import copy

case = ['E3SM_TEST-HVx1_SP1_64x16_1000m_ne30pg2_F-EAMv1-AQP1_01' \
       ,'E3SM_RGMA_ne30pg2_FSP1V1_64x1_1000m_00'
       # ,'E3SM_TEST-DAMP_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00' \
       # ,'E3SM_TEST-NO-PERTURB_SP1_4x1_1000m_ne30pg2_F-EAMv1-AQP1_00' \
       ]
name = case

# case,name = ['QPC4_1deg.32x4km'],['SP-CAM']
# /global/cscratch1/sd/whannah/acme_scratch/cori-knl/QPC4_1deg.32x4km.cam.h1.0001-01-01-00000.nc

# var = ['PRECT']
var = ['TGCLDLWP']

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/objective_imprint_metric.v4'

htype,num_files = 'h1',1

# lat1,lat2 = -30,30

temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'

verbose = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var = len(var)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*2*num_case
res = hs.res_contour_fill_map()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# @numba.njit()
def calculate_metric_numba(data,neighbors,bearing,metric):
   
   for i in range(ncol) :
      nn = neighbors[i,:]
      sort_ind = np.argsort(bearing[i,:])
      nn = nn[sort_ind]
      side_group = nn[0::2]
      diag_group = nn[1::2]

      ### 1rst version - difference of averages for corner/side groups
      # metric[i] = np.abs( ( np.sum(data[diag_group]) + data[i] )/5. - np.mean(data[side_group]) ) 
      
      ### 2nd version - 
      # get the min and max values of the local data
      nn_max = np.max( data[nn] )
      nn_min = np.min( data[nn] )
      # measure the absolute value of the distance between the two 
      # possible checkerboard patterns going from nn_min to nn_max
      dist_from_check1 = np.sum(np.abs(data[diag_group]-nn_max))  \
                       + np.sum(np.abs(data[side_group]-nn_min))  \
                       + np.abs(data[i]-nn_max)                   
      dist_from_check2 = np.sum(np.abs(data[diag_group]-nn_min))  \
                       + np.sum(np.abs(data[side_group]-nn_max))  \
                       + np.abs(data[i]-nn_min)                   
      # find the smaller of those two
      dist_from_check = np.min( ( dist_from_check1, dist_from_check2 ) )
      # take the inverse of the minimum distance, so that a perfect checkerboard pattern (either one) is inf 
      inv_of_dist = 1/dist_from_check
      # multiply by the variation over the neihborhood (max minus min values)
      var_in_hood = np.sum( np.abs( nn_max - nn_min ) )
      metric[i] = inv_of_dist * var_in_hood
      # if there is no variation, the metric will now be nan (because it's (1/0)*0 ). replace with 0 instead
      # if np.isnan(metric[i]) : metric[i] = 0
   return
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []
case_name = []
for c in range(num_case):
   print('\ncase: '+case[c])
   case_obj = he.Case( name=case[c] )
   case_name.append( case_obj.short_name )

   data_list = []
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   if 'lev' not in vars() : lev = np.array([-1])
   if 'lat1' in vars(): case_obj.lat1 = lat1
   if 'lat2' in vars(): case_obj.lat2 = lat2

   # case_obj.mirror_equator = True

   lat = case_obj.load_data('lat',htype=htype,num_files=1)
   lon = case_obj.load_data('lon',htype=htype,num_files=1)

   ncol = len(lat)

   #----------------------------------------------------------------------------
   # calculate distances
   #----------------------------------------------------------------------------
   num_neighbors = 8
   neighbor_obj = ff.Neighbors( lat, lon, num_neighbors+1 )

   dist = neighbor_obj._distances[:,1:]/1e3
   bear = neighbor_obj._bearings[:,1:]

   # convert neighbor data to xarray DataArrays
   coords = [lat['ncol'],np.arange(0,num_neighbors).astype(int)]
   dist = xr.DataArray(dist,coords=coords,dims=['ncol','neighbors'])
   bear = xr.DataArray(bear,coords=coords,dims=['ncol','neighbors'])

   if verbose : 
      hc.print_stat( dist ,stat='naxsh', name=f'\nGC distances w/ {num_neighbors} neighbors [km]')
      hc.print_stat( bear ,stat='naxsh', name=f'\nGC bearings  w/ {num_neighbors} neighbors [km]')

   
   # nn = 2
   # # nn = 30*2*3+15
   # tbear = bear[nn,:]
   # tdist = dist[nn,:]
   # tdist = tdist.sortby(tbear)
   # tbear = tbear.sortby(tbear)
   # for n in range(num_neighbors) : print(f'  {n:2d}     {tbear[n].values:7.2f}     {tdist[n].values:7.2f}')
   # exit()
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for v in range(num_var) :
      #-------------------------------------------------------------------------
      # Load data
      #-------------------------------------------------------------------------
      data = case_obj.load_data(var[v],htype=htype,lev=lev,num_files=num_files)

      # data = data.mean(dim='time')
      data = data.isel(time=-1)

      #-------------------------------------------------------------------------
      # Calculate average of neighbor groups
      #-------------------------------------------------------------------------

      # metric = data.copy(deep=True).compute()
      metric = xr.full_like(data, np.nan).load()

      calculate_metric_numba(data.values,neighbor_obj.neighbors,bear.values,metric.values)

      # for i in range(ncol) :
      #    nn = neighbor_obj.neighbors[i,0:]
      #    sort_ind = np.argsort(bear[i,:].values)
      #    nn = nn[sort_ind]

      #    side_group = nn[0::2]
      #    diag_group = nn[1::2]

      #    # metric[:,i] = np.abs( data.isel(ncol=side_group).mean(dim='ncol').values - data.isel(ncol=diag_group).mean(dim='ncol').values )
      #    metric[i] = np.abs( data.isel(ncol=side_group).mean(dim='ncol').values - data.isel(ncol=diag_group).mean(dim='ncol').values )

      
      hc.print_stat(metric,name='metric')
      # exit()

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      hs.set_cell_fill(case_obj,res)

      # plot.append( ngl.contour_map(wks, metric.mean(dim='time').values, res) )

      res.cnFillPalette = "MPL_viridis"
      
      dres = copy.deepcopy(res)
      mres = copy.deepcopy(res)

      dres.cnLevelSelectionMode = "ExplicitLevels"
      if var[v]=='TGCLDLWP': dres.cnLevels = np.logspace( -2 , 0.25, num=60)
      
      plot[0*num_case+c] = ngl.contour_map(wks, data,   dres) 

      mres.cnLevelSelectionMode = "ExplicitLevels"
      mres.cnLevels = np.linspace( 0 , 1, num=20)

      plot[1*num_case+c] = ngl.contour_map(wks, metric, mres) 

      hs.set_subtitles(wks, plot[0*num_case+c], name[c], '', var[v],             font_height=0.01)
      hs.set_subtitles(wks, plot[1*num_case+c], name[c], '', f'{var[v]} Metric', font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [2,num_case]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------