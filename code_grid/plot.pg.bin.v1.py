import os
import subprocess as sp
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
host = sp.check_output(["dnsdomainname"],universal_newlines=True).strip()
#---------------------------------------------------------------------------------------------------
# v1 - use log bins to plot distribution of individual points
# v2 - bin one variables by another using linearly spaced bins
#---------------------------------------------------------------------------------------------------
if 'ornl.gov' in host :
   case,name = ['INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'],['INCITE2019']
else:
   case,name = ['E3SM_RGMA_ne30pg2_FSP1V1_64x1_1000m_00' ],['MMF RGMA control']
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# var = ['TGCLDLWP']
var = ['PRECT']

clr_var = 'PRECT'


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/pg.bin.v1'


# lat1,lat2,lon1,lon2 = 15,20,185,190
lat1,lat2,lon1,lon2 = 16,17,360-172,360-170
# lat1,lat2,lon1,lon2 = 0,30,120,200

# htype,years,months,num_files = 'h0',[],[],24
# htype,years,first_file,num_files = 'h1',[],(365*4/5),6
htype,years,first_file,num_files = 'h1',[1,2,3,4,5],0,0

recalculate = False
temp_dir = os.getenv("HOME")+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
# plot = [None]*(num_var*num_case)
plot = [None]*(num_var)
res = hs.res_xy()
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
# res.trYReverse = True


if 'clr' not in vars(): 
   clr = ['black']*num_case
   # if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)

if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh

res.xyXStyle = "Log"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   frq_list = []
   amt_list = []
   bin_list = []
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )
      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      area_name = 'area'
      if 'DYN_' in tvar : 
         if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
         if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
         if 'pg2' in case[c] or 'pg3' in case[c] : 
            case_obj.ncol_name = 'ncol_d'
            area_name = 'area_d'
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      bin_tmp_file = temp_dir+f'/pg.bin.v1.{case[c]}.{var[v]}.lat1_{lat1}.lat2_{lat2}.lon1_{lon1}.lon2_{lon2}.nc'
      print('\n    bin_tmp_file: '+bin_tmp_file+'\n')

      if recalculate :

         # area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)
         # data = case_obj.load_data(tvar, htype=htype,first_file=first_file,num_files=num_files)
         
         area = case_obj.load_data(area_name,htype=htype,num_files=1)
         data = case_obj.load_data(tvar,htype=htype,years=years,first_file=first_file,num_files=num_files)

         ncol = data.ncol

         hc.print_time_length(data.time)
         # hc.print_stat(data,name=var[v],indent='    ')
      
         #-------------------------------------------------------------------------
         # Use log bins
         #-------------------------------------------------------------------------
         if var[v]=='TGCLDLWP':
            bin_min_ctr = 1e-6
            bin_min_wid = 1e-6
            bin_spc_pct = 20.
            nbin = 70
         if var[v]=='PRECT':
            bin_min_ctr = 0.04
            bin_min_wid = 0.02
            bin_spc_pct = 25.
            nbin = 40
         #-------------------------------------------------------------------------
         # Recreate bin_YbyX here for log bin case
         #-------------------------------------------------------------------------
         bin_min=bin_min_ctr
         bin_spc=bin_min_wid
         bin_spc_log=bin_spc_pct
         #----------------------------------------------------
         # set up bins
         bin_log_wid = np.zeros(nbin)
         bin_log_ctr = np.zeros(nbin)
         bin_log_ctr[0] = bin_min
         bin_log_wid[0] = bin_spc
         for b in range(1,nbin):
            bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
            bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
         bin_coord = xr.DataArray( bin_log_ctr )

         # hc.print_stat(bin_coord,name='bin_coord')

         #----------------------------------------------------
         # create output data arrays
         ntime = len(data['time']) if 'time' in data.dims else 1   
         
         # shape,dims,coord = (nbin,),'bins',[('bins', bin_coord)]
         shape,dims,coord = (len(ncol),nbin),('ncol','bins'),[('ncol', ncol),('bins', bin_coord)]

         bin_cnt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
         bin_frq = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
         # bin_amt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )

         condition = xr.DataArray( np.full(data.shape,False,dtype=bool), coords=data.coords )

         wgt, *__ = xr.broadcast(area,data)
         wgt = wgt.transpose()
         data_area_wgt = (data*wgt) / wgt.sum()
         ones_area_wgt = xr.DataArray( np.ones(data.shape), coords=data.coords )
         ones_area_wgt = ( ones_area_wgt*wgt ) / wgt.sum()

         #----------------------------------------------------
         # Loop through bins
         for b in range(nbin):
            # print(f'b:{b}')
            bin_bot = bin_log_ctr[b] - bin_log_wid[b]/2.
            bin_top = bin_log_ctr[b] + bin_log_wid[b]/2.
            condition.values = ( data.values >=bin_bot )  &  ( data.values < bin_top )
            # bin_cnt[b] = condition.sum()
            # if bin_cnt[b]>0 :
            #    bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
            #    # bin_amt[b] = data_area_wgt.where(condition,drop=True).sum() / (bin_spc_pct/1e2)

            for n in range(len(ncol)):
               # print(f'  n:{n}')
               bin_cnt[n,b] = condition[:,n].sum()
               if bin_cnt[n,b]>0 :
                  bin_frq[n,b] = ones_area_wgt.where(condition[:,n],drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
                  # bin_amt[n,b] = data_area_wgt.where(condition.isel(ncol=n),drop=True).sum() / (bin_spc_pct/1e2)
               # bin_cnt[n,b] = condition.isel(ncol=n).sum()
               # if bin_cnt[n,b]>0 :
               #    bin_frq[n,b] = ones_area_wgt.where(condition.isel(ncol=n),drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
               #    # bin_amt[n,b] = data_area_wgt.where(condition.isel(ncol=n),drop=True).sum() / (bin_spc_pct/1e2)

         #----------------------------------------------------
         # use a dataset to hold all the output
         bin_ds = xr.Dataset()
         # bin_ds['bin_amt'] = (dims, bin_amt )
         # bin_ds['bin_frq'] = (dims, bin_frq )
         # bin_ds['bin_cnt'] = (dims, bin_cnt )
         # bin_ds['bin_pct'] = (dims, bin_cnt.sum(dim='ncol')/bin_cnt.sum()*1e2 )
         # bin_ds.coords['bins'] = ('bins',bin_coord)
         bin_ds['bin_frq'] = bin_frq 
         bin_ds['bin_cnt'] = bin_cnt 
         # bin_ds.coords['bins'] = bin_coord
         #-------------------------------------------------------------------------
         # Write to file 
         #-------------------------------------------------------------------------
         bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( bin_tmp_file )
         bin_frq = bin_ds['bin_frq']
         # bin_amt = bin_ds['bin_amt']
         bin_coord = bin_ds['bins']
         ncol = bin_ds['ncol']

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      clr_data = case_obj.load_data(clr_var, htype='h0',years=years).mean(dim='time')
      bin_frq = bin_frq.sortby(clr_data)
      
      frequency = np.ma.masked_invalid( bin_frq.values )
      # amount    = np.ma.masked_invalid( bin_amt.values )

      frq_list.append( frequency )
      # amt_list.append( amount    )
      bin_list.append( bin_coord )

   #-------------------------------------------------------------------------
   # Set colors and contour levels
   #-------------------------------------------------------------------------
   tres = copy.deepcopy(res)

   # ip = v*num_case+c
   # ip = c*num_var+v
   ip = v
   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------

   if 'DYN_' in tvar : case_obj.grid = case_obj.grid.replace('pg2','')

   tres.trXMinF = np.min(bin_list)
   tres.trXMaxF = np.max(bin_list)

   # print()
   # print(frq_list.shape)

   bin_list = np.stack(bin_list)
   frq_list = np.stack(frq_list)
   # frq_list = np.ma.masked_where( frq_list<0.001, frq_list )

   # res.xyYStyle = "Log"

   tres.tiYAxisString = 'Frequency [%]'

   ngl.define_colormap(wks,'ncl_default')
   tres.xyLineColors = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,len(ncol),dtype=int)
   
   tplot = ngl.xy(wks,  bin_list[0,:], frq_list[0,:,:] ,tres)  
   
   # plot.append(tplot)
   plot[ip] = tplot

   
   # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)
   # hs.set_subtitles(wks, plot[ip], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
layout = [1,len(plot)]
# layout = [num_var,num_case]
# layout = [num_case,num_var]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------