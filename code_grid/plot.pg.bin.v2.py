import os
import subprocess as sp
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
host = sp.check_output(["dnsdomainname"],universal_newlines=True).strip()
#---------------------------------------------------------------------------------------------------
# v1 - use log bins to plot distribution of individual points
# v2 - bin one variables by another using linearly spaced bins
#---------------------------------------------------------------------------------------------------
if 'ornl.gov' in host :
   case,name = ['INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'],['INCITE2019']
else:
   case,name = ['E3SM_RGMA_ne30pg2_FSP1V1_64x1_1000m_00' ],['MMF RGMA control']
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

xvar = ['CWV']
yvar = ['PRECT']

clr_var = 'PRECT'

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/pg.bin.v2'


lat1,lat2,lon1,lon2 = 16,17,360-172,360-170

# htype,years,months,num_files = 'h0',[],[],12
# htype,years,first_file,num_files = 'h1',[],int(365*4/5),6
# htype,years,first_file,num_files = 'h1',[1,2,3,4,5],0,0
htype,years,first_file,num_files = 'h1',[5],0,0


recalculate = True
temp_dir = os.getenv("HOME")+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var  = len(yvar)
num_case = len(case)

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
# plot = [None]*(num_var*num_case)
plot = [None]*(num_var)
res = hs.res_xy()


if 'clr' not in vars(): 
   clr = ['black']*num_case
   # if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)

if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh
# res.xyXStyle = "Log"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  xvar: '+xvar[v])
   print('  yvar: '+yvar[v])
   frq_list = []
   amt_list = []
   bin_list = []
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )
      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      area_name = 'area'
      if 'DYN_' in xvar[v] : 
         if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
         if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
         if 'pg2' in case[c] or 'pg3' in case[c] : 
            case_obj.ncol_name = 'ncol_d'
            area_name = 'area_d'
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      bin_tmp_file = temp_dir+f'/pg.bin.v2.{case[c]}.{xvar[v]}.{yvar[v]}.lat1_{lat1}.lat2_{lat2}.lon1_{lon1}.lon2_{lon2}.nc'
      print('\n    bin_tmp_file: '+bin_tmp_file+'\n')

      if recalculate :

         # area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)
         # data = case_obj.load_data(tvar, htype=htype,first_file=first_file,num_files=num_files)
         
         area = case_obj.load_data(area_name,htype=htype,num_files=1)

         Vx = case_obj.load_data(xvar[v],htype=htype,years=years,first_file=first_file,num_files=num_files)
         Vy = case_obj.load_data(yvar[v],htype=htype,years=years,first_file=first_file,num_files=num_files)

         ncol = Vx.ncol

         hc.print_time_length(Vx.time)
         # hc.print_stat(Vy,name=var[v],indent='    ')
      
         #-------------------------------------------------------------------------
         #-------------------------------------------------------------------------
         if xvar[v]=='CWV' : bin_min, bin_max, bin_spc = 2, 70, 2
         #-------------------------------------------------------------------------
         # Recreate bin_YbyX here 
         #-------------------------------------------------------------------------
         nbin    = np.round( ( bin_max - bin_min + bin_spc )/bin_spc ).astype(np.int)
         bins    = np.linspace(bin_min,bin_max,nbin)
         bin_coord = xr.DataArray( bins )

         # hc.print_stat(bin_coord,name='bin_coord')

         #----------------------------------------------------
         # create output data arrays
         ntime = len(Vx['time']) if 'time' in Vx.dims else 1   
         
         # shape,dims,coord = (nbin,),'bins',[('bins', bin_coord)]
         shape,dims,coord = (len(ncol),nbin),('ncol','bins'),[('ncol', ncol),('bins', bin_coord)]

         bin_cnt = xr.DataArray( np.zeros(shape,dtype=Vx.dtype), coords=coord, dims=dims )
         bin_val = xr.DataArray( np.zeros(shape,dtype=Vx.dtype), coords=coord, dims=dims )
         bin_cnt = xr.DataArray( np.zeros(shape,dtype=Vx.dtype), coords=coord, dims=dims )

         condition = xr.DataArray( np.full(Vx.shape,False,dtype=bool), coords=Vx.coords )

         wgt, *__ = xr.broadcast(area,Vx)
         wgt = wgt.transpose()
         Vy_area_wgt = (Vy*wgt) / wgt.sum()

         
         print()
         print(Vy_area_wgt)
         print()
         #----------------------------------------------------
         # Loop through bins
         for b in range(nbin):
            bin_bot = bin_min - bin_spc/2. + bin_spc*(b  )
            bin_top = bin_min - bin_spc/2. + bin_spc*(b+1)
            condition.values = ( Vx.values >=bin_bot )  &  ( Vx.values < bin_top )
            # bin_cnt[b,:] = Vy.where(condition,drop=True).count(dim=avg_dims)
            for n in range(len(ncol)):
               bin_cnt[n,b] = condition[:,n].sum()
               if bin_cnt[n,b]>0 :
                  bin_val[n,b] = Vy_area_wgt[:,n].where(condition[:,n],drop=True).mean(dim='time', skipna=True )
                  # bin_val[n,b] = ( (Vy[:,n]*wgt[n]).where(condition[:,n],drop=True).sum( dim='ncol', skipna=True ) \
                  #                          / wgt[n].where(condition[:,n],drop=True).sum( dim='ncol', skipna=True ) \
                  #                ).mean(dim='time', skipna=True )

         #----------------------------------------------------
         # use a dataset to hold all the output
         bin_ds = xr.Dataset()
         bin_ds['bin_val'] = bin_val
         bin_ds['bin_cnt'] = bin_cnt 
         #-------------------------------------------------------------------------
         # Write to file 
         #-------------------------------------------------------------------------
         bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( bin_tmp_file )
         bin_val = bin_ds['bin_val']
         bin_cnt = bin_ds['bin_cnt']
         bin_coord = bin_ds['bins']
         ncol = bin_ds['ncol']

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      clr_data = case_obj.load_data(clr_var, htype='h0',years=years,num_files=num_files).mean(dim='time')
      bin_val = bin_val.sortby(clr_data)
      
      frequency = np.ma.masked_invalid( bin_val.values )
      count     = np.ma.masked_invalid( bin_cnt.values )

      val_list.append( frequency )
      cnt_list.append( count    )
      bin_list.append( bin_coord )

   #-------------------------------------------------------------------------
   # Set colors and contour levels
   #-------------------------------------------------------------------------
   tres = copy.deepcopy(res)

   # ip = v*num_case+c
   # ip = c*num_var+v
   ip = v
   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------

   if 'DYN_' in tvar : case_obj.grid = case_obj.grid.replace('pg2','')

   tres.trXMinF = np.min(bin_list)
   tres.trXMaxF = np.max(bin_list)

   # print()
   # print(val_list.shape)

   bin_list = np.stack(bin_list)
   val_list = np.stack(val_list)

   tres.tiYAxisString = 'Frequency [%]'

   ngl.define_colormap(wks,'ncl_default')
   tres.xyLineColors = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,len(ncol),dtype=int)
   
   tplot = ngl.xy(wks,  bin_list[0,:], val_list[0,:,:] ,tres)  
   
   # plot.append(tplot)
   plot[ip] = tplot

   
   # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)
   # hs.set_subtitles(wks, plot[ip], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
layout = [1,len(plot)]
# layout = [num_var,num_case]
# layout = [num_case,num_var]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------