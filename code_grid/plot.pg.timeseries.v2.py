import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
home = os.getenv("HOME")
print()

### Early Science
# case = ['TRMM','earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ,'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]
# case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ,'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]
# case = ['earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415']
# case = ['early_science']
# case = ['earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m']


# case = [ \
#        # 'E3SM_RGMA_ne30pg2_FSP1V1_64x1_1000m_00' \
#        'E3SM_RGMA-NRAD1_ne30pg2_FSP1V1_64x1_1000m_00' \
#        # ,'E3SM_RGMA-HVx4_ne30pg2_FSP1V1_64x1_1000m_00' \
#        # ,'E3SM_RGMA-DAMP_ne30pg2_FSP1V1_64x1_1000m_00' \
#        # ,'E3SM_RGMA-PR-20_ne30pg2_FSP1V1_64x1_1000m_00' \
#        # ,'E3SM_RGMA-GCM-DIFF_ne30pg2_FSP1V1_64x1_1000m_00' \
#        ]
# name = [ \
#        # 'MMF control' \
#        'MMF nx_rad=1' \
#        # ,'MMF 4xHV' \
#        # ,'MMF Damped' \
#        # ,'MMF Pr=20' \
#        # ,'MMF w/ GCM DIFF' \
#        ]

case,name = ['E3SM_TEST-CRM-WIND_GPU_ne30pg2_FSP1V1_00'],['MMF ']
# case,name = ['E3SM_TEST-CRM-FRICTION_GPU_ne30pg2_FSP1V1_00'],['MMF w/ crm fric']

# case,name = ['E3SM_TEST-CRM-WIND_GPU_ne30pg2_FSP1V1_00','E3SM_TEST-CRM-FRICTION_GPU_ne30pg2_FSP1V1_00'],['MMF ne30pg2','MMF ne30pg2 w/ friction']


# var = ['CLDLIQ']
# var = ['CLDICE']
# var = ['RH']
# var = ['OMEGA']
var = ['CRM_U']



fig_type = "png"
fig_file = "figs_grid/pg.timeseries.v2"+f'.{case[0]}.{var[0]}'
print(fig_file)


# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50
# lat1,lat2,lon1,lon2 = 0,30,120,200
lat1,lat2,lon1,lon2 = 10,20,130,150
# lat1,lat2,lon1,lon2 = 12,20,174,182

# htype,years,months,num_files = 'h0',[],[],24
# htype,first_file,num_files = 'h1',10,1
htype,first_file,num_files = 'h1',0,5


num_col = 3

# timestep1 = 4*48-5
# timestep2 = 4*48+5



#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]

wks = ngl.open_wks(fig_type,fig_file)
# plot = [None]*(num_var*num_case)
plot = []
res = hs.res_contour_fill()
res.tmYLLabelFontHeightF = 0.01
res.tmXBLabelFontHeightF = 0.01
res.trYReverse = True

# res.trXMinF = 12
res.trYMinF = 100.

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )
      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name
      tres = copy.deepcopy(res)
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      area_name = 'area'
      if 'DYN_' in tvar : 
         if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
         if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
         if 'pg2' in case[c] or 'pg3' in case[c] : 
            case_obj.ncol_name = 'ncol_d'
            area_name = 'area_d'

      # ip = v*num_case+c
      ip = c*num_var+v
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      # Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=num_files).mean(dim='time')

      # area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)

      S = case_obj.load_data('TGCLDLWP',htype=htype,first_file=first_file,num_files=num_files).mean(dim='time')
      # S = case_obj.load_data('PRECT',htype=htype,first_file=first_file,num_files=num_files).mean(dim='time')
      
      X = case_obj.load_data(tvar, htype=htype,first_file=first_file,num_files=num_files)

      # print(LWP)

      if 'crm_nx' in X.dims : 
        # calculate root-mean-square
        X = np.sqrt( np.square(X).mean(dim=('crm_nx','crm_ny')) )
        # X = X.mean(dim=('crm_nx','crm_ny'))
        # X = X.min(dim=('crm_nx','crm_ny'))
        X = X.rename({'crm_nz':'lev'})
        tres.trYReverse = False

      # X = X.isel(time=slice(timestep1,timestep2))
      print(X)

      # print(X[0,50,:5].values)
      # print(X[0,50,-5:].values)

      X = X.sortby(S)

      if var[v]=='T' : 
        X = X - X.mean(dim='time')
        X = X - X.mean(dim='ncol')

      # print(X[0,50,:5].values)
      # print(X[0,50,-5:].values)

      # print(X)
      # exit()
   

      if htype=='h0':
         dtime = ( X['time'][-1] - X['time'][0] + (X['time'][1]-X['time'][0]) ).values.astype('timedelta64[M]') + 1
         print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[D]'))+')')
      else:
         dtime = ( X['time'][-1] - X['time'][0] ).values.astype('timedelta64[D]')+1
         print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')


      # X = X.mean(dim='time')

      # X = X[:5,:2]


      hc.print_stat(X)

      # gbl_mean = ( (X*area).sum() / area.sum() ).values 
      # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      
      if var[v] in ['CLDLIQ','CLDICE']      : tres.cnFillPalette = 'MPL_viridis'
      
      if var[v]=='CLDLIQ' : tres.cnLevels = np.logspace( -7 , -3, num=20)
      if var[v]=='CLDICE' : tres.cnLevels = np.logspace( -6 , -3, num=20)
      if var[v]=='RH'     : tres.cnLevels = np.arange(4,100+4,4)
      if var[v]=='OMEGA'  : tres.cnLevels = np.arange(-0.24,0.24+0.04,0.04)
      if var[v]=='T'      : tres.cnLevels = np.arange(-3,3+0.2,0.2)
      if var[v]=='SPTLS'  : tres.cnLevels = np.linspace(-0.0004,0.0004, 61)
      # if var[v]=='CRM_U'  : tres.cnLevels = np.linspace(-15,15, 31)
      if var[v]=='CRM_U'  : tres.cnLevels = np.arange(0,30,2)
      if var[v]=='CRM_W'  : tres.cnLevels = np.linspace(-10,10, 31)

      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      
      tres.cnFillMode = "RasterFill"

      time = X.time
      time = ( time - time[0] ).astype('float') / 86400e9

      tres.sfXArray = time.values
      tres.sfYArray = X.lev.values

      tres.tiXAxisString = 'Time [days]'

      ctr_str = ''
      var_str = var[v]
      
      for n in range(num_col) : 
         plot.append( ngl.contour(wks, X.transpose().isel(ncol=n).values, tres) )
         hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)
      for n in range(num_col) : 
         plot.append( ngl.contour(wks, X.transpose().isel(ncol=-n-1).values, tres) )
         hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)


      # for n in range(len(X.ncol)) :
      #    if LWP.isel(ncol=n)<0.02 :
      #       # tres.xyLineColor = clr[n]
      #       tres.xyLineColor = 'red'
      #       # tres.xyDashPattern = 1
      #    else:
      #       tres.xyLineColor = 'blue'
      #       # tres.xyDashPattern = 0
      #    # ngl.overlay(plot[ip], ngl.xy(wks,X.isel(ncol=n).values,X.lev.values,tres) )
      #    ngl.overlay(plot[ip], ngl.xy(wks,X.isel(ncol=n).values,Z.isel(ncol=n).values,tres) )
         

      
      
      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)
      


#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [len(plot)/2,len(plot)/2]
# layout = [num_var,num_case]
# layout = [num_case,num_var]

# if num_var==1  : layout = [int(np.ceil(num_case/2.)),2]
# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]

if num_case==1 and num_var==4 : layout = [2,2]
if num_case==1 and num_var==6 : layout = [3,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------