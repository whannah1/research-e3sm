  # define all possible categories of neighbor cell configurations (using sign of diff from center) 
# to check for existence of checkboard as a pattern that occurs too often
# v1 - ???
import os, ngl, sys, numba, copy, xarray as xr, numpy as np, re
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import filter_factory as ff
import pg_checkerboard_utilities as pg

case,name, clr = [],[],[]
case_head = 'E3SM.VT-TEST.ne30pg2_ne30pg2'
crm_config = 'CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_1'

# case.append('ERA5'); name.append('ERA5'); clr.append('black')
# case.append('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00');    name.append('E3SM'); clr.append('gray')
# case.append('E3SM.RGMA.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.RADNX_4.00');name.append('MMF'); clr.append('gray')

# case.append(f'{case_head}.F-MMF1.{crm_config}.00');         name.append('MMF');      clr.append('red')
# case.append(f'{case_head}.F-MMF1.{crm_config}.BVT.00');     name.append('MMF+BVT');  clr.append('green')
# case.append(f'{case_head}.F-MMF1.{crm_config}.FVT_08.00');  name.append('MMF+FVT8'); clr.append('blue')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.00');        name.append('MMFXX')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.BVT.00');    name.append('MMFXX+BVT')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.FVT_08.00'); name.append('MMFXX+FVT8')


### CHX experiments
# name,case = [],[]
# case.append(f'E3SM.CHX.ne30pg2_ne30pg2.FC5AV1C-L.00'); name.append('E3SM')
# case.append(f'E3SM.CHX.ne30pg2_ne30pg2.F-MMF1.00'); name.append('MMF')

### HV sensitivity
name,case,clr,pat = [],[],[],[]
def add_case(case_in,c,p=0):
   global name,case,clr
   case.append(case_in); clr.append(c); pat.append(p)
   name.append(case_in.replace('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.',''))
# add_case('MAC',c='black')

# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_25e13.hvss_1',         c='orange',p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_5e14.hvss_1',          c='red',   p=0)
add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.control',                          c='black', p=0)  # default
add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_1e15.hvss_2.hvsq_12',  c='green', p=0)
add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_1e15.hvss_4.hvsq_24',  c='blue',  p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_2e15.hvss_2',          c='green', p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_4e15.hvss_4',          c='blue',  p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_8e15.hvss_8',          c='purple',p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_16e15.hvss_16',        c='pink',  p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_2e15.hvss_2.hvsq_12',          c='green', p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_4e15.hvss_4.hvsq_24',          c='blue',  p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_8e15.hvss_8.hvsq_48',          c='purple',p=0)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.fixed-HV.nu_16e15.hvss_16.hvsq_96',        c='pink',  p=0)

pattern = 4
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.tensor-HV.hvs_3.nu_10e-9.hvss_1',  c='orange',p=pattern)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.tensor-HV.hvs_3.nu_20e-9.hvss_1',  c='red',   p=pattern)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.tensor-HV.hvs_3.nu_45e-9',         c='black', p=pattern) # default
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.tensor-HV.hvs_3.nu_90e-9.hvss_2',  c='green', p=pattern)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.tensor-HV.hvs_3.nu_18e-8.hvss_4',  c='blue',  p=pattern)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.tensor-HV.hvs_3.nu_36e-8.hvss_8',  c='purple',p=pattern)
# add_case('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.tensor-HV.hvs_3.nu_72e-8.hvss_16', c='pink',  p=pattern)


var = []
# var.append('PRECT')
var.append('TGCLDLWP')
# var.append('TGCLDIWP')
# var.append('TMQ')
# var.append('LHFLX')
# var.append('FLNT')
# var.append('FSNT')
# var.append('Q850')
# var.append('T850')
# var.append('U850')


use_regional_subset = False
lat1,lat2,lon1,lon2 = -20,40,80,180+30


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/pg_checkerboard.bar_chart.v1'
tmp_file = os.getenv('HOME')+'/Research/E3SM/data_temp/pg_checkerboard.bar_chart.v1'

# htype,first_file,num_files = 'h1',0,180
# htype,first_file,num_files = 'h1',3,31
htype,first_file,num_files = 'h1',0,0

temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'

recalculate       = True

use_daily         = True
convert_to_freq   = True   # convert count to fractional occurrence
show_as_diff      = False   # calculate difference from first case
show_as_ratio     = False

subset_min_length = 6

combine_sets = True
combine_mode = 1     # 1 = chk vs no chk / 2 = continuity metric
alt_chk_label = True

var_x_case        = False   # controls plot panel arrangement
print_stats       = False   #
verbose           = True   #

sort_sets = True

#---------------------------------------------------------------------------------------------------
# generate sets of possible neighbor states
#---------------------------------------------------------------------------------------------------
num_neighbors = 8

if combine_sets: alt_chk_label = False

# rotate_sets = True
# sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1]]),dims=['set','neighbors'] )

# rotate_sets = False
# sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1], [1,0,1,0,1,0,1,0]]),dims=['set','neighbors'] )

### full set of patterns
rotate_sets = True; sets = pg.all_possible_sets

# use for plotting sum of sets
dummy_set = xr.DataArray(np.array([[-1]*num_neighbors]),dims=['set','neighbors'] )

(num_set,set_len) = sets.shape
set_coord,nn_coord = np.arange(num_set),np.arange(set_len)
sets.assign_coords(coords={'set':set_coord,'neighbors':nn_coord})

set_labels = pg.get_set_labels(sets)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def CESM_FV(case):
   CESM_FV = False
   if 'CESM' in case and any([g in case for g in ['f09','f19']]): CESM_FV = True
   return CESM_FV

def get_comp(case):
   comp = 'eam'
   if case=='ERA5': comp = None
   if case=='MAC': comp = None
   if 'CESM' in case: comp = 'cam'
   if case=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
# sort sets according to partial checkerboard
#---------------------------------------------------------------------------------------------------

sort_idx = [-1]*num_set
if sort_sets:
   ### reorder sets to put partial checkerboards on one side
   nox_sets = []
   chk_sets = []
   for s in range(num_set): 
      if pg.is_partial_checkerboard(sets[s,:].values,subset_length=subset_min_length):
         chk_sets.append(sets[s,:].values)
      else:
         nox_sets.append(sets[s,:].values)
   # for s in range(len(nox_sets)):
   #    print(f'  {nox_sets[s]}')
   # exit()
   sets_sorted = xr.DataArray(np.array( nox_sets + chk_sets), dims=['set','neighbors'] )
   for s in range(num_set):
      for ss in range(num_set):
         if np.all(sets[s,:].values==sets_sorted[ss,:].values): 
            sort_idx[ss] = s
   ### useful for checking sort_idx
   # for s in range(num_set): print(f'  {set_labels[s]}    {set_labels[sort_idx[s]]}')
   # exit()
   set_labels_sorted = []
   for s in range(num_set):
      set_labels_sorted.append( set_labels[sort_idx[s]] )
   set_labels = set_labels_sorted
else:
   for s in range(num_set): sort_idx[s] = s

if alt_chk_label:
   found_first = False; pchk_start = -1
   for s in range(len(sets)):
      ss = sort_idx[s]
      if pg.is_partial_checkerboard(sets[ss,:],subset_length=subset_min_length): 
         if not found_first: 
            pchk_start = s
            found_first = True; break
else:
   pchk_start = num_set 


#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var  = len(var)
num_set  = len(sets)

temp_dir = os.getenv("HOME")+'/Research/E3SM/data_temp'

wkres = ngl.Resources()
# npix=2**13; wkres.wkWidth,wkres.wkHeight=npix,npix # use this for plotting all patterns w/ rotation
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var)
res = hs.res_xy()
res.vpHeightF = 0.4
if convert_to_freq: res.tiYAxisString = 'Fractional Occurrence'

pgres = ngl.Resources()
# pgres.nglDraw,pgres.nglFrame = True,False
pgres.nglDraw,pgres.nglFrame = False,False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
cnt_ds_list = []
for c in range(num_case):
   print('\n  case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
   case_obj = he.Case( name=case[c], time_freq='daily' )
   if 'lev' not in vars() : lev = np.array([-1])

   comp = 'eam'
   if case[c]=='EAR5': comp = None
   if case[c]=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'


   use_remap = False
   remap_str=f'remap_ne30pg2'
   if case[c]=='ERA5': use_remap = True

   #----------------------------------------------------------------------------
   # calculate neighbor distances
   #----------------------------------------------------------------------------
   if recalculate :

      if verbose: print(hc.tcolor.YELLOW+'    recalculating pattern occurrence...'+hc.tcolor.ENDC)

      #----------------------------------------------------------------------------
      # New method based on SCRIP file to find connected neighbors
      #----------------------------------------------------------------------------
      if recalculate :

         if CESM_FV(case[c]) or case[c] in ['MAC'] :
            scripfile = xr.open_dataset(os.getenv('HOME')+'/E3SM/data_grid/fv0.9x1.25_070727.nc')
         else:
            scripfile = case_obj.get_scrip()

         ( neighbors, bearings ) = pg.get_neighbors_and_bearings(scripfile)

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for v in range(num_var) :
      if use_daily:
         case_tmp_file = f'{tmp_file}.daily.{case[c]}.{var[v]}.nc'
      else:
         case_tmp_file = f'{tmp_file}.{case[c]}.{var[v]}.nc'
      print('    var: '+hc.tcolor.GREEN+f'{var[v]:10}'+hc.tcolor.ENDC+'    '+case_tmp_file.replace(os.getenv('HOME'),'~'))
      if recalculate :
         # lat_list,lon_list = [],[]
         # cnt_list,frq_list = [],[]
         #----------------------------------------------------------------------
         #----------------------------------------------------------------------

         if verbose: print('      loading data...')
         data = case_obj.load_data(var[v],htype=htype,lev=lev,component=comp,
                                   num_files=num_files,first_file=first_file,
                                   use_remap=use_remap,remap_str=remap_str)

         if case[c] in ['MAC'] :
            data = data.stack(ncol=("lat", "lon"))
            data['ncol'] = np.arange(ncol)
         

         ncol = len(data['ncol'])

         # Convert to daily mean
         if use_daily:
            data = data.resample(time='D').mean(dim='time')

         # ERA5 daily data comes in monthly files, so take the subset here
         if case[c]=='ERA5': data = data.isel(time=slice(0,num_files))

         if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

         if verbose: print('      calculating anomalies...')
         nn_anomalies = pg.remove_neighbor_mean_numba( data.values, neighbors, bearings, sort_by_bearing=False)

         # define array of neighbor states
         nn_states = np.sign( nn_anomalies )
         nn_states = xr.where(nn_states<=0,0,nn_states)
         nn_states = nn_states.astype(type(sets.values[0,0]))

         if verbose: print('      counting sets...')
         cnt_list = pg.count_sets_per_col_numba( nn_states, sets.values, ncol, rotate_sets )
         #----------------------------------------------------------------------
         #---------------------------------------------------------------------- 
         dims = ('ncol','set')
         cnt_ds = xr.Dataset()
         cnt_ds['cnt'] = ( dims, cnt_list )
         cnt_ds.coords['ncol'] = ('ncol',data['ncol'].values)
         cnt_ds.coords['set'] = ('set',set_coord)
         cnt_ds['num_time'] = len(data['time'].values)

         print('      writing to file: '+case_tmp_file)
         cnt_ds.to_netcdf(path=case_tmp_file,mode='w')
      else:
         cnt_ds = xr.open_dataset( case_tmp_file )

      #-------------------------------------------------------------------------
      # apply regional subset
      #-------------------------------------------------------------------------
      if use_regional_subset: 
         lat = case_obj.load_data('lat',htype=htype,num_files=1,component=comp,use_remap=use_remap,remap_str=remap_str)
         lon = case_obj.load_data('lon',htype=htype,num_files=1,component=comp,use_remap=use_remap,remap_str=remap_str)

         mask = xr.DataArray( np.ones(len(cnt_ds['ncol']),dtype=bool), coords=[cnt_ds['ncol']] )
         mask = mask & (lat>=lat1) & (lat<=lat2)
         mask = mask & (lon>=lon1) & (lon<=lon2)
         
         num_time = cnt_ds['num_time']

         cnt_ds = cnt_ds['cnt'].where(mask,drop=True).to_dataset()

         cnt_ds['num_time'] = num_time
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      ### sum across all columsn
      cnt_ds['cnt'] = cnt_ds['cnt'].sum(dim='ncol')

      ### convert to frequency
      if convert_to_freq:
         cnt_ds['cnt'] = cnt_ds['cnt'] / cnt_ds['num_time'] / len(cnt_ds['ncol']) #* 100.

      if print_stats: hc.print_stat(cnt_ds['cnt'],name='final count dataset',stat='naxs',indent='    ')

      cnt_ds_list.append(cnt_ds)


#---------------------------------------------------------------------------------------------------
# combine sets
#---------------------------------------------------------------------------------------------------

### combine sets that contain partial checkerboard
if combine_sets and combine_mode==1:
   cnt_ds_list_tmp = []
   for cnt_ds in cnt_ds_list:
      chk_idx,nox_idx = [],[]
      for s in range(num_set):
         if pg.is_partial_checkerboard(sets[s,:],subset_length=subset_min_length):
            chk_idx.append(s)
         else:
            nox_idx.append(s)

      ds_sum_nox = cnt_ds.isel(set=nox_idx).sum(dim='set')
      ds_sum_chk = cnt_ds.isel(set=chk_idx).sum(dim='set')
      ds_sum_nox = ds_sum_nox.expand_dims('set').assign_coords(coords={'set':np.array([0])})
      ds_sum_chk = ds_sum_chk.expand_dims('set').assign_coords(coords={'set':np.array([1])})
      # cnt_ds = xr.concat( [ ds_sum_nox, ds_sum_chk ], 'set' )
      cnt_ds = xr.concat( [ ds_sum_chk ], 'set' ) ### only show partial chx

      cnt_ds_list_tmp.append(cnt_ds)

   cnt_ds_list = cnt_ds_list_tmp
   # set_labels = ['no checkerboard','partial checkerboard']
   set_labels = ['partial checkerboard'] ### only show partial chx
   num_set = len(set_labels)
   sort_idx = [c for c in range(num_set)]

   
   

### combine sets based on a measure of discontinuities (# of local min/max)
if combine_sets and combine_mode==2:
   chk_idx,nox_idx = [],[]
   continuity_idx = [[] for _ in range(5)]
   for s in range(num_set):
      tset = sets[s,:].values
      cont = np.sum(np.abs(tset - np.roll(tset,1)))
      continuity_idx[int(cont/2)].append(s)

   cnt_ds_list_tmp = []
   for cnt_ds in cnt_ds_list:
      ds_tmp_0 = cnt_ds.isel(set=continuity_idx[0]).sum(dim='set')
      ds_tmp_1 = cnt_ds.isel(set=continuity_idx[1]).sum(dim='set')
      ds_tmp_2 = cnt_ds.isel(set=continuity_idx[2]).sum(dim='set')
      ds_tmp_3 = cnt_ds.isel(set=continuity_idx[3]).sum(dim='set')
      ds_tmp_4 = cnt_ds.isel(set=continuity_idx[4]).sum(dim='set')
      
      cnt_ds = xr.concat( [ ds_tmp_0, ds_tmp_1, ds_tmp_2, ds_tmp_3, ds_tmp_4 ], 'set' )

      cnt_ds_list_tmp.append(cnt_ds)

   cnt_ds_list = cnt_ds_list_tmp
   
   # sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1], [1,0,1,0,1,0,1,0]]),dims=['set','neighbors'] )
   set_labels = [f'{c*2}' for c in range(5)]
   sort_idx = [c for c in range(5)]
   num_set = len(set_labels)

#---------------------------------------------------------------------------------------------------
# Difference from first case
#---------------------------------------------------------------------------------------------------
if show_as_diff or show_as_ratio:
   for v in range(num_var):
      c = 0
      baseline = cnt_ds_list[c*num_var+v]['cnt'].values
      for c in range(0,num_case):
         if show_as_diff:  cnt_ds_list[c*num_var+v]['cnt'] = cnt_ds_list[c*num_var+v]['cnt'] - baseline
         if show_as_ratio: cnt_ds_list[c*num_var+v]['cnt'] = cnt_ds_list[c*num_var+v]['cnt'] / baseline

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_min,data_max = 1e10,0
for cnt_ds in cnt_ds_list:
   data_min = np.min([ data_min, np.min(cnt_ds['cnt'].values) ])
   data_max = np.max([ data_max, np.max(cnt_ds['cnt'].values) ])
if not show_as_diff and not show_as_ratio: data_min = 0

x_values = np.arange(0,num_set,1,dtype=float)

x_min = np.min(x_values)-1.5
x_max = np.max(x_values)+1.5

res.trYMinF,res.trYMaxF = data_min, data_max
res.trXMinF,res.trXMaxF = x_min, x_max

res.tmXBLabelAngleF  = -90.
res.tmXBMode         = 'Explicit'
res.tmXBValues       = x_values[:pchk_start]
res.tmXBLabels       = set_labels[:pchk_start]
res.xyMarkLineMode   = 'Lines'
res.tmXBLabelFontHeightF = 0.0005
res.tmXBLabelFontColor = 'black'

if combine_sets:
   res.tmXBLabelAngleF  = -45.
   res.tmXBLabelFontHeightF = 0.0020

# resources for multi-colored axis lables
# tres = copy.deepcopy(res)
if alt_chk_label:
   res.tmXBLabelFontColor = 'gray'
   tres = hs.res_default()
   tres.vpHeightF = 0.4
   tres.tmYLOn = False
   tres.trYMinF,tres.trYMaxF = data_min, data_max
   tres.trXMinF,tres.trXMaxF = x_min, x_max
   tres.tmXBLabelAngleF       = -90.
   tres.tmXBLabelFontHeightF = 0.0005
   tres.tmXBMode             = 'Explicit'
   tres.tmXBLabelFontColor   = 'black'
   tres.tmXBValues           = x_values[pchk_start:]
   tres.tmXBLabels           = set_labels[pchk_start:]

   # print(f'  data_min: {data_min}  data_max: {data_max}')
   # ### special option to only show partial checkerboard and reset vertical extent
   # data_max = 0
   # for c in range(num_case):
   #    print()
   #    for s in range(num_set):
   #       ss = sort_idx[s]
   #       cnt_ds = cnt_ds_list[c]
   #       if ss>=pchk_start:
   #          data_max = np.max([ data_max, cnt_ds['cnt'][ss].values ])
   #          val = cnt_ds['cnt'][ss].values
   #          print(f'  val: {val:8.4f}  data_max: {data_max:8.4f}')
   # print(f'  data_min: {data_min}  data_max: {data_max}')
   # data_max = 0.03
   # res.trYMinF,res.trYMaxF = data_min, data_max
   # res.trXMinF,res.trXMaxF = x_values[pchk_start]-0.5, x_max
   # tres.trYMinF,tres.trYMaxF = res.trYMinF,res.trYMaxF
   # tres.trXMinF,tres.trXMaxF = res.trXMinF,res.trXMaxF
 
dx = 0.3
if show_as_ratio: 
   ymin = 1.0
else:
   ymin = 0.0
bar_width_perc=0.6
dxp = (dx * bar_width_perc)/2.

for v in range(num_var):
   # c = 0
   # yy_baseline = cnt_ds_list[c*num_var+v]['cnt'].values
   for c in range(num_case):
      yy_all_sets = cnt_ds_list[c*num_var+v]['cnt'].values
      for s in range(num_set):

         ss = sort_idx[s]

         xx = x_values[s]
         yy = yy_all_sets[ss]

         xbar = np.array([ xx-dxp, xx+dxp, xx+dxp, xx-dxp, xx-dxp])
         ybar = np.array([ ymin, ymin,  yy,  yy, ymin])

         # Shift to accomadate multiple cases
         for b in range(len(xbar)) : xbar[b] = xbar[b] - dxp*(num_case-1) + dxp*2*c

         ### plot polygon outline
         tplot = ngl.xy(wks,xbar,ybar,res)
         if c==0 and s==0:
            plot[v] = tplot
         else:
            ngl.overlay( plot[v], tplot )

         ### Add filled polygon
         pgres.gsFillColor = clr[c]
         pgres.gsFillIndex = 0
         polygon_dummy = ngl.add_polygon(wks,plot[v],xbar,ybar,pgres)

         ### overlay pattern
         if pat[c]>0:
            pgres.gsFillColor = 'black'
            pgres.gsFillIndex = pat[c]
            polygon_dummy = ngl.add_polygon(wks,plot[v],xbar,ybar,pgres)

      # overlay for colored axis lables       
      if alt_chk_label: ngl.overlay( plot[v], ngl.blank_plot(wks, tres) )
   
   subtitle_font_height = 0.01
   hs.set_subtitles(wks, plot[v], '', '', var[v], font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

# layout = [1,num_var]
layout = [int(np.ceil(num_var/3.)),3]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------