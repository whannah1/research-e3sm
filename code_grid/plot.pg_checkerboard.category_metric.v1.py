# define all possible categories of neighbor cell configurations (using sign of diff from center) 
# to check for existence of checkboard as a pattern that occurs too often
# v1 - ???
import os, ngl, sys, numba, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import filter_factory as ff
import pg_checkerboard_utilities as pg

case,name = [],[]
case_head = 'E3SM.CVT-TEST.ne30pg2_r05_oECv3.F-MMF1'
case.append(f'{case_head}.CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_4.00');             name.append('MMF')
# case.append(f'{case_head}.CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_4.BCVT.00');        name.append('MMF+BVT')

# case,name = ['QPC4_1deg.32x4km'],['SP-CAM']
# /global/cscratch1/sd/whannah/acme_scratch/cori-knl/QPC4_1deg.32x4km.cam.h1.0001-01-01-00000.nc

# var = ['PRECT']
var = ['TGCLDLWP']

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/objective_imprint_metric.v4'

# htype,first_file,num_files = 'h1',0,10
htype,first_file,num_files = 'h1',0,180

# lat1,lat2,lon1,lon2 = -30,40,120,360-30
lat1,lat2,lon1,lon2 = 15,25,160,180

temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'

verbose = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var = len(var)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_case
res = hs.res_contour_fill_map()

class tcolor:
   ENDC,RED,GREEN,YELLOW,MAGENTA,CYAN,BOLD = '\033[0m','\033[31m','\033[32m','\033[33m','\033[35m','\033[36m','\033[1m'


#---------------------------------------------------------------------------------------------------
# generate sets of possible neighbor states
#---------------------------------------------------------------------------------------------------

# sets = pg.generate_sets(8)

#shortcut for 8 neighbor case
# sets = xr.DataArray(np.array(
#       [[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 1],[0, 0, 0, 0, 0, 0, 1, 1],[0, 0, 0, 0, 0, 1, 0, 1],
#        [0, 0, 0, 0, 0, 1, 1, 1],[0, 0, 0, 0, 1, 0, 0, 1],[0, 0, 0, 0, 1, 0, 1, 1],[0, 0, 0, 0, 1, 1, 0, 1],
#        [0, 0, 0, 0, 1, 1, 1, 1],[0, 0, 0, 1, 0, 0, 0, 1],[0, 0, 0, 1, 0, 0, 1, 1],[0, 0, 0, 1, 0, 1, 0, 1],
#        [0, 0, 0, 1, 0, 1, 1, 1],[0, 0, 0, 1, 1, 0, 0, 1],[0, 0, 0, 1, 1, 0, 1, 1],[0, 0, 0, 1, 1, 1, 0, 1],
#        [0, 0, 0, 1, 1, 1, 1, 1],[0, 0, 1, 0, 0, 1, 0, 1],[0, 0, 1, 0, 0, 1, 1, 1],[0, 0, 1, 0, 1, 0, 1, 1],
#        [0, 0, 1, 0, 1, 1, 0, 1],[0, 0, 1, 0, 1, 1, 1, 1],[0, 0, 1, 1, 0, 0, 1, 1],[0, 0, 1, 1, 0, 1, 0, 1],
#        [0, 0, 1, 1, 0, 1, 1, 1],[0, 0, 1, 1, 1, 0, 1, 1],[0, 0, 1, 1, 1, 1, 0, 1],[0, 0, 1, 1, 1, 1, 1, 1],
#        [0, 1, 0, 1, 0, 1, 0, 1],[0, 1, 0, 1, 0, 1, 1, 1],[0, 1, 0, 1, 1, 0, 1, 1],[0, 1, 0, 1, 1, 1, 1, 1],
#        [0, 1, 1, 0, 1, 1, 1, 1],[0, 1, 1, 1, 0, 1, 1, 1],[0, 1, 1, 1, 1, 1, 1, 1],[1, 1, 1, 1, 1, 1, 1, 1]])
#       ,dims=['set','neighbors'] )

sets = xr.DataArray(np.array([[0, 1, 0, 1, 0, 1, 0, 1],[1, 0, 1, 0, 1, 0, 1, 0]]),dims=['set','neighbors'] )

# sets = xr.DataArray(np.array([[0, 1, 0, 1, 0, 1, 0, 1],[1, 0, 1, 0, 1, 0, 1, 0]
#                             ,[0, 0, 1, 0, 1, 0, 1, 0] # 
#                             ,[0, 1, 0, 1, 0, 1, 0, 0]
#                             ,[1, 0, 1, 0, 1, 0, 0, 0]
#                             ,[0, 1, 0, 1, 0, 0, 0, 1]
#                             ,[1, 0, 1, 0, 0, 0, 1, 0]
#                             ,[0, 1, 0, 0, 0, 1, 0, 1]
#                             ,[1, 0, 0, 0, 1, 0, 1, 0] 
#                             ,[0, 0, 0, 0, 1, 0, 1, 0] #
#                             ,[0, 0, 0, 1, 0, 1, 0, 0]
#                             ,[0, 0, 1, 0, 1, 0, 0, 0]
#                             ,[0, 1, 0, 1, 0, 0, 0, 0]
#                             ,[1, 0, 1, 0, 0, 0, 0, 0]
#                             ,[0, 1, 0, 0, 0, 0, 0, 1]
#                             ,[1, 0, 0, 0, 0, 0, 1, 0] 
#                             ])
#                      ,dims=['set','neighbors'] )

(num_set,set_len) = sets.shape
set_coord,nn_coord = np.arange(num_set),np.arange(set_len)
sets.assign_coords(coords={'set':set_coord,'neighbors':nn_coord})


# print(); print(sets); print()

# (num_set,set_len) = sets.shape
# for s in range(num_set): print(sets[s,:].values)

# exit()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list,y_list = [],[]
for c in range(num_case):
   print('\ncase: '+case[c])
   case_obj = he.Case( name=case[c] )

   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   if 'lev' not in vars() : lev = np.array([-1])
   if 'lat1' in vars(): case_obj.lat1,case_obj.lat2 = lat1,lat2
   if 'lon1' in vars(): case_obj.lon1,case_obj.lon2 = lon1,lon2

   lat = case_obj.load_data('lat',htype=htype,num_files=1)
   lon = case_obj.load_data('lon',htype=htype,num_files=1)

   ncol = len(lat)

   #----------------------------------------------------------------------------
   # calculate distances
   #----------------------------------------------------------------------------
   num_neighbors = 8
   neighbor_obj = ff.Neighbors( lat, lon, num_neighbors+1 )

   dist = neighbor_obj._distances[:,1:]/1e3
   bear = neighbor_obj._bearings[:,1:]

   # convert neighbor data to xarray DataArrays
   coords = [lat['ncol'],np.arange(0,num_neighbors).astype(int)]
   dist = xr.DataArray(dist,coords=coords,dims=['ncol','neighbors'])
   bear = xr.DataArray(bear,coords=coords,dims=['ncol','neighbors'])

   if verbose : 
      hc.print_stat( dist ,stat='naxsh', name=f'\nGC distances w/ {num_neighbors} neighbors [km]')
      hc.print_stat( bear ,stat='naxsh', name=f'\nGC bearings  w/ {num_neighbors} neighbors [km]')

   
   # nn = 2
   # # nn = 30*2*3+15
   # tbear = bear[nn,:]
   # tdist = dist[nn,:]
   # tdist = tdist.sortby(tbear)
   # tbear = tbear.sortby(tbear)
   # for n in range(num_neighbors) : print(f'  {n:2d}     {tbear[n].values:7.2f}     {tdist[n].values:7.2f}')
   # exit()
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for v in range(num_var) :
      #-------------------------------------------------------------------------
      # Load data
      #-------------------------------------------------------------------------
      data = case_obj.load_data(var[v],htype=htype,lev=lev,num_files=num_files,first_file=first_file)

      # data = data.mean(dim='time')
      # data = data.isel(time=-1)

      #-------------------------------------------------------------------------
      # Calculate average of neighbor groups
      #-------------------------------------------------------------------------

      # metric = data.copy(deep=True).compute()
      # metric = xr.full_like(data, np.nan).load()

      # calculate_metric_numba(data.values,neighbor_obj.neighbors,bear.values,metric.values)


      # count = xr.DataArray( np.zeros([ncol,num_set]), dims=['ncol','set'] )
      # count = xr.DataArray( np.zeros([1,num_set]), dims=['ncol','set'] )

      ### print lat and lon to find pairs
      # for i in range(2,ncol) :
      #    if lat[i].values>17 and lat[i].values<19:
      #       print(f'\ni: {i}  lat: {lat[i].values:6.2f}  lon: {lon[i].values:6.2f}')
      # exit()

      table_msg = ['']*(num_set+1)
      table_ncol = 0
      header_len = []

      lat_list = []
      lon_list = []
      cnt_list = []
      frq_list = []

      print()
      print(f'ncol: {ncol}')
      print()

      # for i in range(0,ncol) :
      # for i in range(30,40) :
      # for i in [6,8] :
      # for i in [36,39] :
      for i in [15,16] : # < good pair

         
         print(f'  i: {i}  lat: {lat[i].values:6.2f}  lon: {lon[i].values:6.2f}  ')
         # mean = data.isel(ncol=i).mean().values
         # hc.print_stat( data.isel(ncol=i), compact=True, stat='as', name='' )
         # print()

         lat_list.append(lat.values[i])
         lon_list.append(lon.values[i])

         sort_ind = np.argsort(neighbor_obj._bearings[i,1:])
         nn = neighbor_obj.neighbors[i,1:]
         nn = nn[sort_ind]
         # side_group = nn[0::2]
         # diag_group = nn[1::2]

         nn_values = data.isel(ncol=nn[:]) - data.isel(ncol=i) 

         nn_anomalies = pg.remove_mean_numba( nn_values.values, data.isel(ncol=i).values )
         
         nn_states = np.sign( nn_anomalies )

         # nn_states = np.where(nn_states<=0,0,1)
         nn_states = xr.where(nn_states<=0,0,nn_states)

         # print(); exit(nn_states)

         # print(type(sets.values[0,0]))
         # print(type(nn_states.values[0,0]))
         nn_states = nn_states.astype(type(sets.values[0,0]))
         # print(nn_states.values)
         # print(type(nn_states.values[0,0]))
         # exit()


         # count[i,:] = pg.count_sets( nn_states, sets )
         # count[i,:] = pg.count_sets_numba( nn_states, sets.values )
         # set_count = count.sum(dim='ncol')
         # set_freq = set_count / set_count.sum() * 100.


         count = pg.count_sets_numba( nn_states, sets.values )
         # set_count = count
         # set_freq = count / count.sum() * 100.


         set_count = count
         set_freq = set_count / np.sum(set_count) * 100.

         cnt_list.append(set_count)
         frq_list.append(set_freq)
         

         # print()
         # c = 0; line = '  '
         # for s in range(num_set):
         #    msg = f'{sets[s].values}'
         #    msg+= f'  {int(set_count[s]):8d}'
         #    msg+= tcolor.BOLD+f'  {set_freq[s]:8.2f}%'+tcolor.ENDC
         #    if np.all(sets[s].values==np.array([0,1,0,1,0,1,0,1])): msg = tcolor.RED  + msg + tcolor.ENDC
         #    if np.all(sets[s].values==np.array([1,0,1,0,1,0,1,0])): msg = tcolor.CYAN + msg + tcolor.ENDC
         #    line += msg+'        '
         #    c += 1
         #    if c==1: # set number of columns here
         #       print(line)
         #       c = 0; line = '  '

         ### use for pair of points
         for s in range(num_set):
            if table_ncol==0:
               table_msg[1+s] += f'{sets[s].values}    '
               if s==0: header_len.append(len(table_msg[1+s]))
            table_msg[1+s] += f'  '
            table_msg[1+s] += f'  {int(set_count[s]):>5d}'
            table_msg[1+s] += f'  {set_freq[s]:8.2f}%'
            if s==0: header_len.append( len(table_msg[1+s]) - sum(header_len) )
         table_ncol+=1
            

      ### set header strings - use for pair of points
      hmsg = 'pattern'
      table_msg[0] = f'{hmsg:^{header_len[0]}}'
      for l,ll in enumerate(header_len[1:]):
         hmsg = f'{lat_list[l]:5.1f} N / {lon_list[l]:5.1f} E'
         table_msg[0] += f'{hmsg:>{ll}}'
      print()
      for line in table_msg: print(line)
      exit()

      ### use for large group of points
      total_set_count = np.zeros(num_set)
      for i in range(len(cnt_list)): total_set_count += cnt_list[i]
      set_freq = total_set_count / np.sum(total_set_count) * 100.   
      print()
      c = 0; line = '  '
      for s in range(num_set):
         msg = f'{sets[s].values}'
         msg+= f'  {int(total_set_count[s]):8d}'
         msg+= tcolor.BOLD+f'  {set_freq[s]:8.2f}%'+tcolor.ENDC
         if np.all(sets[s].values==np.array([0,1,0,1,0,1,0,1])): msg = tcolor.RED  + msg + tcolor.ENDC
         if np.all(sets[s].values==np.array([1,0,1,0,1,0,1,0])): msg = tcolor.CYAN + msg + tcolor.ENDC
         line += msg+'        '
         c += 1
         if c==1: # set number of columns here
            print(line)
            c = 0; line = '  '


      print()
      print(f'ncol: {ncol}')
      print()
      total_count = np.sum(total_set_count)
      print(f'total_count: {total_count}')
      print()

      exit()


      # for i in range(ncol) :
      #    nn = neighbor_obj.neighbors[i,0:]
      #    sort_ind = np.argsort(bear[i,:].values)
      #    nn = nn[sort_ind]

      #    side_group = nn[0::2]
      #    diag_group = nn[1::2]

      #    # metric[:,i] = np.abs( data.isel(ncol=side_group).mean(dim='ncol').values - data.isel(ncol=diag_group).mean(dim='ncol').values )
      #    metric[i] = np.abs( data.isel(ncol=side_group).mean(dim='ncol').values - data.isel(ncol=diag_group).mean(dim='ncol').values )

      
      # hc.print_stat(metric,name='metric')

      # exit()

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      # hs.set_cell_fill(case_obj,res)

      # # plot.append( ngl.contour_map(wks, metric.mean(dim='time').values, res) )

      # res.cnFillPalette = "MPL_viridis"
      
      # dres = copy.deepcopy(res)
      # mres = copy.deepcopy(res)

      # dres.cnLevelSelectionMode = "ExplicitLevels"
      # if var[v]=='TGCLDLWP': dres.cnLevels = np.logspace( -2 , 0.25, num=60)
      
      # plot[0*num_case+c] = ngl.contour_map(wks, data,   dres) 

      # mres.cnLevelSelectionMode = "ExplicitLevels"
      # mres.cnLevels = np.linspace( 0 , 1, num=20)

      # plot[1*num_case+c] = ngl.contour_map(wks, metric, mres) 

      # hs.set_subtitles(wks, plot[0*num_case+c], name[c], '', var[v],             font_height=0.01)
      # hs.set_subtitles(wks, plot[1*num_case+c], name[c], '', f'{var[v]} Metric', font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

# layout = [2,num_case]
# ngl.panel(wks,plot,layout,hs.setres_panel())
# ngl.end()

# hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------