# plot distribution of counts through multiple re-sampling (i.e. bootstrap methods)
# v1 - ???
import os, ngl, sys, numba, copy, xarray as xr, numpy as np, re
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import filter_factory as ff
import pg_checkerboard_utilities as pg

case,name, clr = [],[],[]
case_head = 'E3SM.VT-TEST.ne30pg2_ne30pg2'
crm_config = 'CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_1'

case.append('ERA5'); name.append('ERA5'); clr.append('black')
# case.append('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00');    name.append('E3SM'); clr.append('gray')
case.append(f'{case_head}.F-MMF1.{crm_config}.00');         name.append('MMF');      clr.append('red')
case.append(f'{case_head}.F-MMF1.{crm_config}.BVT.00');     name.append('MMF+BVT');  clr.append('green')
# case.append(f'{case_head}.F-MMF1.{crm_config}.FVT_08.00');  name.append('MMF+FVT8'); clr.append('blue')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.00');        name.append('MMFXX')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.BVT.00');    name.append('MMFXX+BVT')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.FVT_08.00'); name.append('MMFXX+FVT8')


var = []
# var.append('PRECT')
var.append('TGCLDLWP')
# var.append('TGCLDIWP')
# var.append('TMQ')
# var.append('LHFLX')
# var.append('FLNT')
# var.append('FSNT')
# var.append('Q850')
# var.append('T850')
# var.append('U850')

n_resample, sample_size = 100, 31


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/pg_checkerboard.distribution.v1'
tmp_file = os.getenv('HOME')+f'/Research/E3SM/data_temp/pg_checkerboard.distribution.v1.n_{n_resample}.s_{int(sample_size)}'

# htype,first_file,num_files = 'h1',0,180
htype,first_file,num_files = 'h1',0,31
# htype,first_file,num_files = 'h1',0,6 # use for E3SM case

temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'

recalculate       = False 
use_daily         = True
convert_to_freq   = True   # convert count to fractional occurrence
# show_as_diff      = False   # calculate difference from first case
# show_as_ratio     = False

combine_sets = True
combine_mode = 1     # 1 = chk vs no chk / 2 = ???

var_x_case        = False   # controls plot panel arrangement
print_stats       = False   #
verbose           = True   #

sort_sets = True
#---------------------------------------------------------------------------------------------------
# generate sets of possible neighbor states
#---------------------------------------------------------------------------------------------------
num_neighbors = 8

# rotate_sets = True
# sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1]]),dims=['set','neighbors'] )

# rotate_sets = False
# sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1], [1,0,1,0,1,0,1,0]]),dims=['set','neighbors'] )

### full set of patterns
rotate_sets = True; sets = xr.DataArray(np.array(
      [[0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,1], [0,0,0,0,0,0,1,1], [0,0,0,0,0,1,0,1],
       [0,0,0,0,0,1,1,1], [0,0,0,0,1,0,0,1], [0,0,0,0,1,0,1,1], [0,0,0,0,1,1,0,1],
       [0,0,0,0,1,1,1,1], [0,0,0,1,0,0,0,1], [0,0,0,1,0,0,1,1], [0,0,0,1,0,1,0,1],
       [0,0,0,1,0,1,1,1], [0,0,0,1,1,0,0,1], [0,0,0,1,1,0,1,1], [0,0,0,1,1,1,0,1],
       [0,0,0,1,1,1,1,1], [0,0,1,0,0,1,0,1], [0,0,1,0,0,1,1,1], [0,0,1,0,1,0,1,1],
       [0,0,1,0,1,1,0,1], [0,0,1,0,1,1,1,1], [0,0,1,1,0,0,1,1], [0,0,1,1,0,1,0,1],
       [0,0,1,1,0,1,1,1], [0,0,1,1,1,0,1,1], [0,0,1,1,1,1,0,1], [0,0,1,1,1,1,1,1],
       [0,1,0,1,0,1,0,1], [0,1,0,1,0,1,1,1], [0,1,0,1,1,0,1,1], [0,1,0,1,1,1,1,1],
       [0,1,1,0,1,1,1,1], [0,1,1,1,0,1,1,1], [0,1,1,1,1,1,1,1], [1,1,1,1,1,1,1,1]])
      ,dims=['set','neighbors'] )

### parital checkboard sets
# rotate_sets = True; sets = xr.DataArray(np.array(
#       [[0,0,0,0,0,1,0,1], [0,0,0,0,1,0,1,1], [0,0,0,0,1,1,0,1], [0,0,0,1,0,1,0,1], 
#        [0,0,0,1,0,1,1,1], [0,0,0,1,1,1,0,1], [0,0,1,0,0,1,0,1], [0,0,1,0,1,0,1,1], 
#        [0,0,1,0,1,1,0,1], [0,0,1,0,1,1,1,1], [0,0,1,1,0,1,0,1], [0,0,1,1,1,1,0,1], 
#        [0,1,0,1,0,1,0,1], [0,1,0,1,0,1,1,1], [0,1,0,1,1,0,1,1], [0,1,0,1,1,1,1,1]])
#       ,dims=['set','neighbors'] )

# use for plotting sum of sets
dummy_set = xr.DataArray(np.array([[-1]*num_neighbors]),dims=['set','neighbors'] )

(num_set,set_len) = sets.shape
set_coord,nn_coord = np.arange(num_set),np.arange(set_len)
sets.assign_coords(coords={'set':set_coord,'neighbors':nn_coord})

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def is_partial_checkerboard(set_in,subset_length=4):
   found = False
   for l in range(len(set_in)):
      if found: break
      s_rot = np.concatenate( ( set_in[l:], set_in[:l] ) ) # rotate the set
      subset1 = [0,1]*int(subset_length/2)
      subset2 = [1,0]*int(subset_length/2)
      if np.all(s_rot[:subset_length]==subset1): found = True
      if np.all(s_rot[:subset_length]==subset2): found = True
   return found 


set_labels = []
for s in range(num_set):
   label = f'{sets[s,:].values}'
   # if is_partial_checkerboard(sets[s,:]): label = '*'+label
   set_labels.append(label)

sort_idx = [-1]*num_set

if sort_sets:

   # reorder sets to put partial checkerboards on one side
   nox_sets = []
   chk_sets = []
   for s in range(num_set): 
      if is_partial_checkerboard(sets[s,:].values):
         chk_sets.append(sets[s,:].values)
      else:
         nox_sets.append(sets[s,:].values)

   sets_sorted = xr.DataArray(np.array( nox_sets + chk_sets), dims=['set','neighbors'] )

   for s in range(num_set):
      for ss in range(num_set):
         if np.all(sets[s,:].values==sets_sorted[ss,:].values): 
            sort_idx[ss] = s

   set_labels_sorted = []
   for s in range(num_set):
      set_labels_sorted.append( set_labels[sort_idx[s]] )
   set_labels = set_labels_sorted

else:
   
   for s in range(num_set): sort_idx[s] = s


# pchk_start = num_set 

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var  = len(var)
num_set  = len(sets)

temp_dir = os.getenv("HOME")+'/Research/E3SM/data_temp'

wkres = ngl.Resources()
# npix=2**13; wkres.wkWidth,wkres.wkHeight=npix,npix # use this for plotting all patterns w/ rotation
wks = ngl.open_wks(fig_type,fig_file,wkres)
res = hs.res_xy()
res.vpHeightF = 0.4
# if convert_to_freq: res.tiYAxisString = 'Fractional Occurrence'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
@numba.njit()
def count_sets_per_col_numba(nn_states,sets,ncol):
   cnt_out = np.zeros((ncol,num_set))
   for i in range(ncol) :
      # count the occurrence of sets and calculate frequency
      count = pg.count_sets_numba( nn_states[:,i,:], sets, rotate_sets=rotate_sets )
      cnt_out[i,:] = count
   return cnt_out
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
cnt_ds_list = []
for c in range(num_case):
   print('\n  case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
   case_obj = he.Case( name=case[c], time_freq='daily' )
   if 'lev' not in vars() : lev = np.array([-1])

   comp = 'eam'
   if case[c]=='EAR5': comp = None
   if case[c]=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'


   use_remap = False
   remap_str=f'remap_ne30pg2'
   if case[c]=='ERA5': use_remap = True
   #----------------------------------------------------------------------------
   # calculate neighbor distances
   #----------------------------------------------------------------------------
   if recalculate :

      if verbose: print(hc.tcolor.YELLOW+'    recalculating pattern occurrence...'+hc.tcolor.ENDC)

      lat = case_obj.load_data('lat',htype=htype,num_files=1,component=comp,use_remap=use_remap,remap_str=remap_str)
      lon = case_obj.load_data('lon',htype=htype,num_files=1,component=comp,use_remap=use_remap,remap_str=remap_str)
      ncol = len(lat)

      neighbor_obj = ff.Neighbors( lat, lon, num_neighbors+1 )

      dist = neighbor_obj._distances[:,1:]/1e3
      bear = neighbor_obj._bearings[:,1:]

      ### check if distances are reasonable
      # hc.print_stat(dist,stat='naxsh'); exit()

      # convert neighbor data to xarray DataArrays
      coords = [lat['ncol'],np.arange(0,num_neighbors).astype(int)]
      dist = xr.DataArray(dist,coords=coords,dims=['ncol','neighbors'])
      bear = xr.DataArray(bear,coords=coords,dims=['ncol','neighbors'])

      # if verbose : 
      #    hc.print_stat( dist ,stat='naxsh', name=f'\nGC distances w/ {num_neighbors} neighbors [km]')
      #    hc.print_stat( bear ,stat='naxsh', name=f'\nGC bearings  w/ {num_neighbors} neighbors [km]')

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for v in range(num_var) :
      if use_daily:
         case_tmp_file = f'{tmp_file}.daily.{case[c]}.{var[v]}.nc'
      else:
         case_tmp_file = f'{tmp_file}.{case[c]}.{var[v]}.nc'
      print('    var: '+hc.tcolor.GREEN+f'{var[v]:10}'+hc.tcolor.ENDC+'    '+case_tmp_file.replace(os.getenv('HOME'),'~'))
      if recalculate :
         # lat_list,lon_list = [],[]
         # cnt_list,frq_list = [],[]
         #----------------------------------------------------------------------
         #----------------------------------------------------------------------

         if verbose: print('      loading data...')
         data = case_obj.load_data(var[v],htype=htype,lev=lev,component=comp,
                                   num_files=num_files,first_file=first_file,
                                   use_remap=use_remap,remap_str=remap_str)

         # Convert to daily mean
         if use_daily:
            data = data.resample(time='D').mean(dim='time')

         # ERA5 daily data comes in monthly files, so take the subset here
         if case[c]=='ERA5': data = data.isel(time=slice(0,num_files))

         if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

         if verbose: print('      calculating anomalies...')
         nn_anomalies = pg.remove_neighbor_mean_numba( data.values, 
                                                       neighbor_obj.neighbors[:,1:], 
                                                       neighbor_obj._bearings[:,1:] )

         # define array of neighbor states
         nn_states = np.sign( nn_anomalies )
         nn_states = xr.where(nn_states<=0,0,nn_states)
         nn_states = nn_states.astype(type(sets.values[0,0]))

         if verbose: print('      counting sets...')

         np.random.seed(v*1000 + c^2)

         time_ind = np.arange(0,len(data['time'].values))

         cnt_list = np.zeros((n_resample,num_set))
         for n in range(n_resample):
            if verbose: print(f'        sample # {n}')
            
            # cnt_list = count_sets_per_col_numba( nn_states, sets.values, ncol )

            # ncol_sample_ind = np.random.choice( data['ncol'].values, size=int(sample_size), replace=True )
            # tmp_cnt = count_sets_per_col_numba( nn_states[:,ncol_sample_ind,:], sets.values, len(ncol_sample_ind) )
            # cnt_list[n,:] = np.sum( tmp_cnt, axis=0)

            time_sample_ind = np.random.choice( time_ind, size=int(sample_size), replace=True )
            tmp_cnt = count_sets_per_col_numba( nn_states[time_sample_ind,:,:], sets.values, ncol )
            cnt_list[n,:] = np.sum( tmp_cnt, axis=0) # sum over ncol

         #----------------------------------------------------------------------
         #---------------------------------------------------------------------- 
         dims = ('sample','set')
         cnt_ds = xr.Dataset()
         cnt_ds['cnt'] = (dims, cnt_list )
         cnt_ds.coords['ncol'] = ('ncol',data['ncol'].values)
         cnt_ds.coords['set'] = ('set',set_coord)
         cnt_ds['num_time'] = len(data['time'].values)
         cnt_ds['n_resample'] = n_resample
         cnt_ds['sample_size'] = sample_size
         

         print('      writing to file: '+case_tmp_file)
         cnt_ds.to_netcdf(path=case_tmp_file,mode='w')
      else:
         cnt_ds = xr.open_dataset( case_tmp_file )

      
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      # cnt_ds['cnt'] = cnt_ds['cnt'].sum(dim='ncol')

      ### convert to frequency
      if convert_to_freq:
         # cnt_ds['cnt'] = cnt_ds['cnt'] / cnt_ds['num_time'] / len(cnt_ds['ncol'])
         # cnt_ds['cnt'] = cnt_ds['cnt'] / cnt_ds['num_time'] / cnt_ds['sample_size']
         cnt_ds['cnt'] = cnt_ds['cnt'] / cnt_ds['sample_size'] / len(cnt_ds['ncol'])

      if print_stats: hc.print_stat(cnt_ds['cnt'],name='final count dataset',stat='naxs',indent='    ')

      cnt_ds_list.append(cnt_ds)

      # hc.print_stat(cnt_ds['cnt']); exit()

#---------------------------------------------------------------------------------------------------
# combine sets
#---------------------------------------------------------------------------------------------------

### combine sets that contain partial checkerboard
if combine_sets and combine_mode==1:
   cnt_ds_list_tmp = []
   for cnt_ds in cnt_ds_list:
      chk_idx,nox_idx = [],[]
      for s in range(num_set):
         if is_partial_checkerboard(sets[s,:]):
            chk_idx.append(s)
         else:
            nox_idx.append(s)

      ds_sum_nox = cnt_ds.isel(set=nox_idx).sum(dim='set')
      ds_sum_chk = cnt_ds.isel(set=chk_idx).sum(dim='set')
      ds_sum_nox = ds_sum_nox.expand_dims('set').assign_coords(coords={'set':np.array([0])})
      ds_sum_chk = ds_sum_chk.expand_dims('set').assign_coords(coords={'set':np.array([1])})
      cnt_ds = xr.concat( [ ds_sum_nox, ds_sum_chk ], 'set' )

      cnt_ds_list_tmp.append(cnt_ds)

   cnt_ds_list = cnt_ds_list_tmp
   
   # sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1], [1,0,1,0,1,0,1,0]]),dims=['set','neighbors'] )
   set_labels = ['no checkerboard','partial checkerboard']
   num_set = len(set_labels)
   sort_idx = [c for c in range(num_set)]


#---------------------------------------------------------------------------------------------------
# Difference from first case
#---------------------------------------------------------------------------------------------------
# if show_as_diff or show_as_ratio:
#    for v in range(num_var):
#       c = 0
#       baseline = cnt_ds_list[c*num_var+v]['cnt'].values
#       for c in range(0,num_case):
#          if show_as_diff:  cnt_ds_list[c*num_var+v]['cnt'] = cnt_ds_list[c*num_var+v]['cnt'] - baseline
#          if show_as_ratio: cnt_ds_list[c*num_var+v]['cnt'] = cnt_ds_list[c*num_var+v]['cnt'] / baseline

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_min_list,data_max_list = np.full(num_set,1e10),np.full(num_set,0.0)
for cnt_ds in cnt_ds_list:
   for s in range(num_set):
      data_min_list[s] = np.min([ data_min_list[s], np.min(cnt_ds['cnt'][s,:].values) ])
      data_max_list[s] = np.max([ data_max_list[s], np.max(cnt_ds['cnt'][s,:].values) ])

# x_values = np.arange(0,num_set,1,dtype=float)

# x_min = np.min(x_values)-1
# x_max = np.max(x_values)+1

# res.trYMinF,res.trYMaxF = data_min, data_max
# res.trXMinF,res.trXMaxF = x_min, x_max

# res.tmXBLabelAngleF  = -90.
# res.tmXBMode         = 'Explicit'
# res.tmXBValues       = x_values
# res.tmXBLabels       = set_labels
# res.xyMarkLineMode   = 'Lines'
# res.tmXBLabelFontHeightF = 0.0005
# res.tmXBLabelFontColor = 'black'

# if combine_sets:
#    res.tmXBLabelAngleF  = -45.
#    res.tmXBLabelFontHeightF = 0.0020


plot = [None]*(num_var*num_set)

nbin = 200

for v in range(num_var):
   for s in range(num_set):
      ip = v*num_set + s
      # ip = s*num_var + v
      for c in range(num_case):

         data_min = data_min_list[s]         
         data_max = data_max_list[s]

         tmp_data = cnt_ds_list[c*num_var+v]['cnt'][s,:]

         # hc.print_stat(tmp_data,stat='naxs')
         print(); print(tmp_data); print()

         bin_ds = hc.bin_YbyX( tmp_data, tmp_data, bin_min=data_min, bin_max=data_max, bin_spc=(data_max-data_min)/nbin )

         xx = bin_ds['bins'].values
         yy = bin_ds['bin_cnt'].values

         ### plot distribution as line
         res.xyLineColor = clr[c]
         tplot = ngl.xy(wks, xx, yy, res)

         if c==0:
            plot[ip] = tplot
         else:
            ngl.overlay( plot[ip], tplot )
   
      subtitle_font_height = 0.015
      hs.set_subtitles(wks, plot[ip], var[v], '', set_labels[s], font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [num_set,num_var]
# layout = [num_var,num_set]
# layout = [int(np.ceil(len(plot)/3.)),3]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------