# define all possible categories of neighbor cell configurations (using sign of diff from center) 
# to check for existence of checkboard as a pattern that occurs too often
# v1 - ???
import os, ngl, sys, numba, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import filter_factory as ff, pg_checkerboard_utilities as pg

case,name = [],[]
case_head = 'E3SM.VT-TEST.ne30pg2_ne30pg2'
crm_config = 'CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_1'

# case.append('ERA5'); name.append('ERA5')
case.append('MAC');  name.append('MAC')
# case.append(f'CESM.f09_g17.FSPCAMS');                       name.append('SPCAM-FV')
# case.append(f'CESM.ne30pg2_ne30pg2_mg17.FSPCAMS');          name.append('SPCAM-SE')
# case.append('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00');    name.append('E3SM')
# case.append(f'{case_head}.F-MMF1.{crm_config}.00');         name.append('MMF')
# case.append(f'{case_head}.F-MMF1.{crm_config}.BVT.00');     name.append('MMF+BVT')
# case.append(f'{case_head}.F-MMF1.{crm_config}.FVT_08.00');  name.append('MMF+FVT8')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.00');        name.append('MMFXX')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.BVT.00');    name.append('MMFXX+BVT')
# case.append(f'{case_head}.F-MMFXX.{crm_config}.FVT_08.00'); name.append('MMFXX+FVT8')

# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20'); name.append('SPCAM-FV L20')
# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22'); name.append('SPCAM-FV L22')
# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_38'); name.append('SPCAM-FV L38')
# case.append('CESM.f09_g17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_40'); name.append('SPCAM-FV L40')
# case.append('CESM.ne30pg2_ne30pg2_mg17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20'); name.append('SPCAM-SE 64x2km L20')
# case.append('CESM.ne30pg2_ne30pg2_mg17.FSPCAMS.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22'); name.append('SPCAM-SE 64x2km L22')

# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_20.CRMNZ_20'); name.append('E3SM-MMF L20')
# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_22.CRMNZ_22'); name.append('E3SM-MMF L22')
# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_38.CRMNZ_38'); name.append('E3SM-MMF L38')
# case.append('E3SM.vert-test.ne30pg2_ne30pg2.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.NLEV_40.CRMNZ_40'); name.append('E3SM-MMF L40')

# var = ['PRECT']
var = ['TGCLDLWP']
# var = ['TGCLDIWP','FLNT','FSNT','TMQ','T850','T500','U850'] # no checkerboarding here

# lat1,lat2 = -50,50
# xlat,xlon,dy,dx = 15,150,10,20; lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/pg_checkerboard.map.v1'
tmp_file = os.getenv('HOME')+'/Research/E3SM/data_temp/pg_checkerboard.map.v1'

# htype,first_file,num_files = 'h1',0,180
htype,first_file,num_files = 'h1',0,20#31
# htype,years,months,first_file,num_files = 'h1',[],[1],0,0

temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'

recalculate       = False 
make_plot         = True

add_set_sum       = False   # create additional plot with sum across all sets
combine_sets      = True   # combine sets that contain partial checkerboard
convert_to_freq   = True   # convert count to fractional occurrence

common_colorbar   = True   # use common set of color levels for all plots
var_x_case        = False   # controls plot panel arrangement
print_stats       = False   #
verbose           = True   #

neighbor_method = 2  # 1 = old distance based / 2 = new SCRIP file based
#---------------------------------------------------------------------------------------------------
# generate sets of possible neighbor states
#---------------------------------------------------------------------------------------------------
num_neighbors = 8

if 'years'  not in locals(): years  = []
if 'months' not in locals(): months = []

# rotate_sets = True
# sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1]]),dims=['set','neighbors'] )

# rotate_sets = False
# sets = xr.DataArray(np.array([[0,1,0,1,0,1,0,1], [1,0,1,0,1,0,1,0]]),dims=['set','neighbors'] )

rotate_sets = True; sets = pg.all_possible_sets

# use for plotting sum of sets
dummy_set = xr.DataArray(np.array([[-1]*num_neighbors]),dims=['set','neighbors'] )

(num_set,set_len) = sets.shape
set_coord,nn_coord = np.arange(num_set),np.arange(set_len)
sets.assign_coords(coords={'set':set_coord,'neighbors':nn_coord})

set_labels = pg.get_set_labels(sets)

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var,num_set = len(case),len(var),len(sets)

temp_dir = os.getenv("HOME")+'/Research/E3SM/data_temp'

wkres = ngl.Resources()
# npix=2**13; wkres.wkWidth,wkres.wkHeight=npix,npix # use this for plotting all patterns w/ rotation
npix=4096; wkres.wkWidth,wkres.wkHeight=npix,npix # use this for plotting all patterns w/ rotation
wks = ngl.open_wks(fig_type,fig_file,wkres)

res = hs.res_contour_fill_map()
res.tmXBOn = False
res.tmYLOn = False
res.lbTitlePosition = 'Bottom'
res.lbTitleFontHeightF = 0.01
res.lbTitleString = 'Count'

if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# #print partial checkerboard sets and exit
# chk_idx = []
# for s in range(num_set):
#    if pg.is_partial_checkerboard( sets[s,:] ): 
#       print(sets[s,:].values)
# exit()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def CESM_FV(case):
   CESM_FV = False
   if 'CESM' in case and any([g in case for g in ['f09','f19']]): CESM_FV = True
   return CESM_FV

def get_comp(case):
   comp = 'eam'
   if case=='ERA5': comp = None
   if case=='MAC': comp = None
   if 'CESM' in case: comp = 'cam'
   if case=='E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00': comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
cnt_ds_list = []
for c in range(num_case):
   print('\n  case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
   case_obj = he.Case( name=case[c], time_freq='daily' )

   #----------------------------------------------------------------------------
   # load the coordinate data
   #----------------------------------------------------------------------------
   if 'lev' not in vars() : lev = np.array([-1])

   use_remap = False
   remap_str=f'remap_ne30pg2'
   if case[c]=='ERA5': use_remap = True
   # if case[c]=='MAC':  use_remap = True; remap_str=f''
   # if case[c]=='MAC':  remap_str=f''

   #----------------------------------------------------------------------------
   # calculate neighbor distances
   #----------------------------------------------------------------------------
   if recalculate :

      if verbose: print(hc.tcolor.YELLOW+'    recalculating pattern occurrence...'+hc.tcolor.ENDC)

      # lat = case_obj.load_data('lat',htype=htype,num_files=1,component=get_comp(case[c]),use_remap=use_remap,remap_str=remap_str)
      # lon = case_obj.load_data('lon',htype=htype,num_files=1,component=get_comp(case[c]),use_remap=use_remap,remap_str=remap_str)
      # ncol = len(lat)

      # if case[c] in ['MAC'] or CESM_FV(case[c]) :
      #    nlat,nlon = len(lat), len(lon)
      #    ncol = len(lat) * len(lon)
      #    ilat,ilon = lat,lon
      #    lat,lon = np.zeros(ncol), np.zeros(ncol)
      #    for j in range(nlat):
      #       for i in range(nlon):
      #          n = j*nlon + i
      #          lat[n],lon[n] = ilat[j],ilon[i]
      #    lat = xr.DataArray(lat,coords=[np.arange(ncol)],dims='ncol')
      #    lon = xr.DataArray(lon,coords=[np.arange(ncol)],dims='ncol')
      
      #-------------------------------------------------------------------------
      # old distance based method
      #-------------------------------------------------------------------------
      # if neighbor_method==1:
      #    # if CESM_FV:
      #       # ??????
      #    # else:
      #    if True:
      #       neighbor_obj = ff.Neighbors( lat, lon, num_neighbors+1 )
      #       neighbors = neighbor_obj.neighbors[:,1:]
      #       bearings  = neighbor_obj._bearings[:,1:]
      #       # distance  = neighbor_obj._distances[:,1:]/1e3

      #    ### convert neighbor data to xarray DataArrays
      #    # coords = [lat['ncol'],np.arange(0,num_neighbors).astype(int)]
      #    # distance = xr.DataArray(distance,coords=coords,dims=['ncol','neighbors'])
      #    # bearings = xr.DataArray(bearings,coords=coords,dims=['ncol','neighbors'])

      #    # if verbose : 
      #    #    hc.print_stat( distance,stat='naxsh', name=f'\nGC distances w/ {num_neighbors} neighbors [km]')
      #    #    hc.print_stat( bearings,stat='naxsh', name=f'\nGC bearings  w/ {num_neighbors} neighbors [km]')
      #-------------------------------------------------------------------------
      # New method based on SCRIP file to find connected neighbors
      #-------------------------------------------------------------------------
      if neighbor_method==2:

         if CESM_FV(case[c]) or case[c] in ['MAC'] :
            scripfile = xr.open_dataset(os.getenv('HOME')+'/E3SM/data_grid/fv0.9x1.25_070727.nc')
         else:
            scripfile = case_obj.get_scrip()

         ( neighbors, bearings ) = pg.get_neighbors_and_bearings(scripfile)


   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   for v in range(num_var) :
      case_tmp_file = f'{tmp_file}.{case[c]}.{var[v]}.nm_{neighbor_method}.nc'
      print('    var: '+hc.tcolor.GREEN+f'{var[v]:10}'+hc.tcolor.ENDC+'    '+case_tmp_file.replace(os.getenv('HOME'),'~'))
      if recalculate :
         #----------------------------------------------------------------------
         #----------------------------------------------------------------------

         if verbose: print('      loading data...')
         data = case_obj.load_data(var[v],htype=htype,lev=lev,component=get_comp(case[c]),
                                   num_files=num_files,first_file=first_file,
                                   years=years,months=months,#lev=lev,
                                   use_remap=use_remap,remap_str=remap_str)
         
         if case[c] in ['MAC'] or CESM_FV(case[c]) :
            data = data.stack(ncol=("lat", "lon"))
            data['ncol'] = np.arange(ncol)
            
            # idata = data.copy()
            # data = np.zeros([len(idata['time']),ncol])
            # for j in range(nlat):
            #    for i in range(nlon):
            #       data[:,n] = idata[:,j,i]
            # data = xr.DataArray(data,coords=[idata['time'],lat['ncol']],dims=['time','ncol'])

         
         # Convert to daily mean
         # if case[c] not in ['MAC']: 
         # data = data.resample(time='D').mean(dim='time')

         # ERA5 daily data comes in monthly files of 3-hourly samples, so take the subset here
         # if case[c]=='ERA5': data = data.isel(time=slice(0,num_files))
         if case[c]=='ERA5': data = data.isel(time=slice(0,num_files*8))

         if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

         if verbose: print('      calculating anomalies...')
         if neighbor_method==1: nn_anomalies = pg.remove_neighbor_mean_numba( data.values, neighbors, bearings, sort_by_bearing=True )
         if neighbor_method==2: nn_anomalies = pg.remove_neighbor_mean_numba( data.values, neighbors, bearings, sort_by_bearing=False)

         # define array of neighbor states
         nn_states = np.sign( nn_anomalies )
         nn_states = xr.where(nn_states<=0,0,nn_states)
         nn_states = nn_states.astype(type(sets.values[0,0]))

         if verbose: print('      counting sets...')
         cnt_list = pg.count_sets_per_col_numba( nn_states, sets.values, ncol )
         #----------------------------------------------------------------------
         #---------------------------------------------------------------------- 
         dims = ('ncol','set')
         cnt_ds = xr.Dataset()
         cnt_ds['cnt'] = (dims, cnt_list )
         cnt_ds.coords['ncol'] = ('ncol',data['ncol'].values)
         cnt_ds.coords['set'] = ('set',set_coord)
         cnt_ds['num_time'] = len(data['time'].values)

         print('      writing to file: '+case_tmp_file)
         cnt_ds['num_files'] = num_files
         cnt_ds.to_netcdf(path=case_tmp_file,mode='w')
      else:
         cnt_ds = xr.open_dataset( case_tmp_file )

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      ### add new set as sum across sets
      if add_set_sum:
         ds_sum = cnt_ds.sum(dim='set')
         ds_sum = ds_sum.expand_dims('set').assign_coords(coords={'set':np.array([num_set])})
         cnt_ds = xr.concat( [ cnt_ds, ds_sum ], 'set' )

      # ### combine sets that contain partial checkerboard
      # if combine_sets:
      #    # check for partial checkerboard
      #    chk_idx,nox_idx = [],[]
      #    for i,s in enumerate(sets.values):
      #       found = False
      #       for l in range(set_len):
      #          if found: break
      #          s_rot = np.concatenate( ( s[l:], s[:l] ) ) # rotate the set
      #          # if np.all(s_rot[:4]==[0,1,0,1]): found = True
      #          # if np.all(s_rot[:4]==[1,0,1,0]): found = True
      #          # if np.all(s_rot[:5]==[1,0,1,0,1]): found = True
      #          # if np.all(s_rot[:5]==[0,1,0,1,0]): found = True
      #          if np.all(s_rot[:6]==[0,1,0,1,0,1]): found = True
      #          if np.all(s_rot[:6]==[1,0,1,0,1,0]): found = True
      #          if found: chk_idx.append(i)
      #       if not found: nox_idx.append(i)
      #    ds_sum_nox = cnt_ds.isel(set=nox_idx).sum(dim='set')
      #    ds_sum_chk = cnt_ds.isel(set=chk_idx).sum(dim='set')
      #    ds_sum_nox = ds_sum_nox.expand_dims('set').assign_coords(coords={'set':np.array([0])})
      #    ds_sum_chk = ds_sum_chk.expand_dims('set').assign_coords(coords={'set':np.array([1])})
      #    cnt_ds = xr.concat( [ ds_sum_nox, ds_sum_chk ], 'set' ); num_set = 2
      #    # cnt_ds = ds_sum_chk; num_set = 1

      # ### combine sets that contain partial checkerboard vs no checkerboard
      # if combine_sets:
      #    chk_idx,nox_idx = [],[]
      #    for s in range(num_set):
      #       if pg.is_partial_checkerboard( sets[s,:] ):
      #          chk_idx.append(s)
      #       else:
      #          nox_idx.append(s)
      #    ds_sum_nox = cnt_ds.isel(set=nox_idx).sum(dim='set')
      #    ds_sum_chk = cnt_ds.isel(set=chk_idx).sum(dim='set')
      #    ds_sum_nox = ds_sum_nox.expand_dims('set').assign_coords(coords={'set':np.array([0])})
      #    ds_sum_chk = ds_sum_chk.expand_dims('set').assign_coords(coords={'set':np.array([1])})
      #    cnt_ds = xr.concat( [ ds_sum_nox, ds_sum_chk ], 'set' )

      ### combine sets that contain only partial checkerboard (omit no checkerboard cases)
      if combine_sets:
         chk_idx = []
         for s in range(num_set):
            if pg.is_partial_checkerboard( sets[s,:] ): chk_idx.append(s)
         ds_sum_chk = cnt_ds.isel(set=chk_idx).sum(dim='set')
         ds_sum_chk = ds_sum_chk.expand_dims('set').assign_coords(coords={'set':np.array([1])})
         cnt_ds = ds_sum_chk

      ### convert to frequency
      if convert_to_freq:
         cnt_ds['cnt'] = cnt_ds['cnt'] / cnt_ds['num_time'] #* 100.
         res.lbTitleString = 'Fractional Occurrence'

      if print_stats: hc.print_stat(cnt_ds['cnt'],name='final count dataset',stat='naxs',indent='    ')

      cnt_ds_list.append(cnt_ds)

if combine_sets:
   # set_labels = ['no checkerboard','partial checkerboard']
   set_labels = ['partial checkerboard']
   num_set = len(set_labels)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

if not make_plot: exit('\nmake_plot=False, so aborting the script now.\n')

if add_set_sum: num_set += 1; sets = xr.concat([sets,dummy_set],dim='set')

res.cnFillPalette = "MPL_viridis"

if common_colorbar:
   data_min = np.zeros(num_set)
   data_max = np.zeros(num_set)
   for cnt_ds in cnt_ds_list:
      for s in range(num_set):
         data_min[s] = np.min([ data_min[s], np.min(cnt_ds['cnt'].isel(set=s).values) ])
         data_max[s] = np.max([ data_max[s], np.max(cnt_ds['cnt'].isel(set=s).values) ])
   
   # if combine_sets and num_set==2:
   #    data_min[0],data_min[0] = 0.,0.
   #    data_max[1],data_max[1] = 1.,0.5

   if combine_sets and num_set==1:
      data_min[0] = 0.
      data_max[0] = 0.1

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
list_cnt = 0
plot = [None]*(num_var*num_case*num_set)
for c in range(num_case):
   case_obj = he.Case( name=case[c], time_freq='daily' )

   for v in range(num_var) :

      tres = copy.deepcopy(res)

      if case[c] in ['MAC'] or CESM_FV(case[c]) :
         lat = case_obj.load_data('lat',htype=htype,num_files=1,component=get_comp(case[c]),use_remap=use_remap,remap_str=remap_str)
         lon = case_obj.load_data('lon',htype=htype,num_files=1,component=get_comp(case[c]),use_remap=use_remap,remap_str=remap_str)
         nlat,nlon = len(lat), len(lon)
         ncol = len(lat) * len(lon)
         ilat,ilon = lat,lon
         lat,lon = np.zeros(ncol), np.zeros(ncol)
         for j in range(nlat):
            for i in range(nlon):
               n = j*nlon + i
               lat[n],lon[n] = ilat[j],ilon[i]
         # lat = xr.DataArray(lat,coords=[np.arange(ncol)],dims='ncol')
         # lon = xr.DataArray(lon,coords=[np.arange(ncol)],dims='ncol')
         # hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
         tres.cnFillMode    = "RasterFill"
         tres.sfXArray      = lon
         tres.sfYArray      = lat
         # tres.sfXCellBounds = scripfile['grid_corner_lon'].values
         # tres.sfYCellBounds = scripfile['grid_corner_lat'].values
      else:
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype)

      if case[c]=='ERA5':
         scripfile = xr.open_dataset( os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc' )
         tres.cnFillMode    = "CellFill"
         tres.sfXArray      = scripfile['grid_center_lon'].values
         tres.sfYArray      = scripfile['grid_center_lat'].values
         tres.sfXCellBounds = scripfile['grid_corner_lon'].values
         tres.sfYCellBounds = scripfile['grid_corner_lat'].values
   
      cnt_ds = cnt_ds_list[list_cnt]
      list_cnt += 1

      for s in range(num_set):

         if common_colorbar:
            nlev = 21; aboutZero = False
            (cmin,cmax,cint) = ngl.nice_cntr_levels(data_min[s], data_max[s], 
                                                   cint=None, max_steps=nlev, 
                                                   returnLevels=False, aboutZero=aboutZero )
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

         if num_var>1:
            ip = s*num_var+v if var_x_case else v*num_set+s
         else:
            ip = s*num_case+c if var_x_case else c*num_set+s

         plot[ip] = ngl.contour_map(wks, cnt_ds['cnt'].isel(set=s).values, tres) 

         # if all(sets[s,:]==dummy_set):
         #    set_str = f'set: (sum of sets)'
         # else:
         #    set_str = f'set: {sets[s,:].values}'

         set_str = set_labels[s]

         subtitle_font_height = 0.01
         if num_set>3: subtitle_font_height = 0.002
         if (num_var*num_case*num_set)>4: subtitle_font_height = 0.008
         if num_set>12: subtitle_font_height = 0.002
         hs.set_subtitles(wks, plot[ip], name[c], set_str, var[v], font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

if num_var>1:
   layout = [num_set,num_var] if var_x_case else [num_var,num_set]
else:
   layout = [num_set,num_case] if var_x_case else [num_case,num_set]


if num_case==1 and num_set>3: 
   num_plot_col = 6
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

if num_var==1 and num_set==1: 
   num_plot_col = 2
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------