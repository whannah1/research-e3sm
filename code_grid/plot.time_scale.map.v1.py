import os, ngl, subprocess as sp
import numpy as np, xarray as xr
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy, string
host = hc.get_host()
data_dir,data_sub = None,None
# data_dir,data_sub = '/project/projectdirs/m3312/jlee1046/E3SM/',''

name,case = [],[]
case.append(f'E3SM.GNUGPU.ne30pg2_ne30pg2.F-MMFXX-AQP1.NLEV_50.CRMNX_32.CRMDX_3200.CRMDT_10.RADNX_4.00');name.append('E3SM-MMF')
case.append(f'E3SM.GNU.ne30pg2_ne30pg2.F-EAMv1-AQP1.NLEV_50.00');name.append('E3SM')
   
#-------------------------------------------------------------------------------
var = []
# var.append('PRECT')
var.append('TGCLDLWP')

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/time_scale.map.v1'

htype,years,months,first_file,num_files = 'h1',[],[],0,30

use_remap,remap_grid = False,'180x360'
plot_diff,add_diff = False,True
print_stats = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.02 - 0.0015*num_var - 0.0014*(num_case+int(add_diff))

if 'diff_base' not in locals(): diff_base = 0
if 'diff_case' not in locals(): diff_case = [(i+1) for i in range(num_case-1)]
if 'lev'       not in locals(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
if plot_diff:
   if add_diff:
      plot = [None]*(num_var*(num_case*2-1))
   else:
      plot = [None]*(num_var*(num_case))
else:
   plot = [None]*(num_var*num_case)
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.018

res.tmXBOn,res.tmYLOn = False,False
# res.mpGeophysicalLineColor = 'white'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   std_list,cnt_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+case[c])
      if use_remap:
         case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub = f'data_remap_{remap_grid}/' )
      else:
         case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub )

      tvar = var[v]

      case_obj.set_coord_names(tvar)
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      # if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2
      
      if use_remap:
         data = case_obj.load_data(tvar,     htype=htype,years=years,months=months,
                                   first_file=first_file,num_files=num_files,lev=lev,
                                   use_remap=True,remap_str=f'remap_{remap_grid}')
         lat = case_obj.load_data('lat',htype=htype,num_files=1,use_remap=True,remap_str=f'remap_{remap_grid}')
         lon = case_obj.load_data('lon',htype=htype,num_files=1,use_remap=True,remap_str=f'remap_{remap_grid}')
      else:
         # area = case_obj.load_data(case_obj.area_name,htype=htype,years=years,months=months,
         #                                              first_file=first_file,num_files=num_files).astype(np.double)
         data = case_obj.load_data(tvar,              htype=htype,ps_htype=htype,years=years,months=months,
                                                      first_file=first_file,num_files=num_files,lev=lev)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ')

      # calculate time scale
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         # data = data.mean(dim='time')

         # tau = np.correlate(data.values,data.values,'full')

         # nlag = mxlag*2+1
         mxlag = 8*10
         nlag = mxlag+1
         ncol = len(data['ncol'])
         ntime = len(data['time'])
         tau_lag = xr.DataArray(data=np.zeros([nlag,ncol]),dims=('lag','ncol'))
         
         # for l in range(-mxlag,mxlag)
         for l in range(0,mxlag+1):
            data1 = data.isel(time=slice(0,ntime-1-l)).drop('time')
            data2 = data.isel(time=slice(l,ntime-1)  ).drop('time')
            tau_lag[l,:] = xr.corr(data1,data2,dim='time')

         tau = tau_lag.sum(dim='lag')*(3./24.)

      else:
         raise ValueError('No time dimension in data')
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------

      # Calculate area weighted global mean
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

      # if case[c]=='TRMM' and 'lon1' not in locals(): 
      #    data_list.append( ngl.add_cyclic(data.values) )
      # else:
      #    data_list.append( data.values )

      data_list.append( tau )

      # if 'area' in locals() : area_list.append( area.values )

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])


   for c in range(num_case):
      case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub )
      case_obj.set_coord_names(tvar)

      if plot_diff and add_diff: 
         # ip = ((c+1)*2-1)*num_var+v
         ip = v*(num_case*2-1) + c
      else:
         ip = v*num_case+c
         # ip = c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      # tres.cnFillPalette = "WhiteBlueGreenYellowRed"
      # tres.cnFillPalette = "BlueWhiteOrangeRed"
      # tres.cnFillPalette = "CBR_wet"
      # tres.cnFillPalette = "BlueRed"

      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,20+1,1)
      # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.arange(2,30+2,2)*1e-2
      # if var[v]=="TGCLDIWP"            : tres.cnLevels = np.arange(2,20+2,2)*1e-2
      # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2 , 0.25, num=60)
      
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         aboutZero = False
         clev_tup = ngl.nice_cntr_levels(data_min, data_max,  \
                                         cint=None, max_steps=21, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup is None: 
            tres.cnLevelSelectionMode = "AutomaticLevels"   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=21)
            tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      # if var[v]=="PRECT"    : var_str = "Precip"
      # if var[v]=="TGCLDLWP" : var_str = "LWP"
      # if var[v]=="TGCLDIWP" : var_str = "IWP"

      if 'RCE' in case[c] or 'AQP' in case[c] :
         tres.mpOutlineBoundarySets = "NoBoundaries"; tres.mpCenterLonF = 0.
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_remap:
         hs.set_cell_fill(tres,case_obj=case_obj,use_remap=True,lat=lat,lon=lon)
      else:
         hs.set_cell_fill(tres,case_obj=case_obj,htype=htype)

      if plot_diff : 
         if c==diff_base : base_name = name[c]
         # if c in diff_case : case_name = case_name+' - '+base_name      

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 
         # plot.append( ngl.contour_map(wks,data.values,tres) )
         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 

         scripfile = case_obj.get_scrip()

         #----------------------------------------------------------------------
         #----------------------------------------------------------------------
         ctr_str = ''
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name
         
         hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         if add_diff: 
            ip = v*(num_case*2-1) + num_case+c-1
         else:
            ip = v*num_case+c
         data_list[c] = data_list[c] - data_baseline.values

         # tres.cnFillPalette = 'ncl_default'
         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         # tres.cnFillPalette = 'MPL_bwr'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if var[v] in ['TGCLDLWP'] : tres.cnLevels = np.arange(-28,28+4,4)*1e-3
         if var[v] in ['TGCLDIWP'] : tres.cnLevels = np.arange(-10,10+2,2)*1e-3
         if not hasattr(tres,'cnLevels') : 
            # if np.min(data_list[c]).values==np.max(data_list[c]).values : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print('WARNING: Difference is zero!')
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)

         if use_remap:
            hs.set_cell_fill(tres,case_obj=case_obj,use_remap=True,lat=lat,lon=lon)
         else:
            hs.set_cell_fill(tres,case_obj=case_obj)

         tres.lbLabelBarOn = True
         plot[ip] = ngl.contour_map(wks,data_list[c],tres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
if plot_diff:
   if add_diff: 
      layout = [num_var,num_case*2-1]
   else:
      layout = [num_var,num_case]
      if num_var==1 and len(plot)==4 : layout = [2,2]
else:
   # layout = [num_case,num_var]
   layout = [num_var,num_case]

   if num_var==1 and len(plot)==4 : layout = [2,2]
   # if num_var==1 and num_case==6 : layout = [3,2]

if num_case==1 :
   if num_var==4 : layout = [2,2]
   if num_var==6 : layout = [3,2]

pnl_res = hs.setres_panel()
pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
pnl_res.nglPanelFigureStringsJust        = "TopLeft"
pnl_res.nglPanelFigureStringsFontHeightF = 0.01
if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015
# pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
