import os, ngl, subprocess as sp
import numpy as np, xarray as xr
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy, string
host = hc.get_host()
data_dir,data_sub = None,None
# data_dir,data_sub = '/project/projectdirs/m3312/jlee1046/E3SM/',''

name,case,clr = [],[],[]
case.append(f'E3SM.GNUGPU.ne30pg2_ne30pg2.F-MMFXX-AQP1.NLEV_50.CRMNX_32.CRMDX_3200.CRMDT_10.RADNX_4.00');name.append('E3SM-MMF');clr.append('blue')
# case.append(f'E3SM.GNU.ne30pg2_ne30pg2.F-EAMv1-AQP1.NLEV_50.00');name.append('E3SM');clr.append('red')

#-------------------------------------------------------------------------------
pvar = []
# pvar.append('PRECT')
# pvar.append('TGCLDLWP')
# pvar.append('FLNT')
# pvar.append('WSPD') # advective timescale
# pvar.append('MMF_DQ'); pvar.append('MMF_QTLS')
pvar.append('MMF_DT'); pvar.append('MMF_TLS')
lev = 950

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_grid/time_scale.zonal_mean.v1'

htype,years,months,first_file,num_files = 'h1',[],[],0,30

dlat = 5

plot_diff = False
add_diff = False
print_stats = True

chk_significance = False # use with plot_diff

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_pvar = len(pvar)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.3
res.xyLineThicknessF = 8

if 'clr' not in locals(): 
   if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   else : clr = ['black']

# if num_case>1 and 'dsh' not in locals(): dsh = np.arange(0,num_case,1)
if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]
res.xyLineColors   = clr
res.xyDashPatterns = dsh

# res.tiXAxisString = 'Latitude'
res.tiXAxisString = 'sin( Latitude )'

class tcolor:
   ENDC,RED,GREEN,YELLOW,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[33m','\033[35m','\033[36m'


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
msg_list = []
for v in range(num_pvar):
   print('var: '+pvar[v])
   data_list,std_list,cnt_list = [],[],[]
   for c in range(num_case):
      print('  case: '+case[c])
      case_obj = he.Case( name=case[c] )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      # if 'lon1' in locals() : case_obj.lon1 = lon1
      # if 'lon2' in locals() : case_obj.lon2 = lon2
      # if 'lev'  in locals() : case_obj.lev  = lev

      lat  = case_obj.load_data('lat',  htype=htype,num_files=1)
      area = case_obj.load_data('area', htype=htype,num_files=1).astype(np.double)
      data = case_obj.load_data(pvar[v],htype=htype,years=years,months=months,num_files=num_files,lev=lev)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      hc.print_time_length(data.time)

      if print_stats: 
         msg = hc.print_stat(data,name=pvar[v],stat='naxsh',indent='  ',compact=True)
         msg_list.append('  case: '+case[c]+'\n'+msg)
         if 'area' in locals() :
            gbl_mean = ( (data*area).sum() / area.sum() ).values 
            print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

      # advective timescale
      if 'WSPD' in pvar[v]:
         print('WARNING - experimental adjustment of the data!')
         radius = 6.371e6
         area = np.sqrt( area*(radius*radius) )
         hc.print_stat(area,name='area')
         hc.print_stat(data,name='initial data')
         tau = area/data /86400.

      # calculate time scale
      elif 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         # data = data.mean(dim='time')

         # tau = np.correlate(data.values,data.values,'full')

         # nlag = mxlag*2+1
         mxlag = 8*5
         nlag = mxlag+1
         ncol = len(data['ncol'])
         ntime = len(data['time'])
         tau_lag = xr.DataArray(data=np.zeros([nlag,ncol]),dims=('lag','ncol'))
         
         # for l in range(-mxlag,mxlag)
         for l in range(0,mxlag+1):
            data1 = data.isel(time=slice(0,ntime-1-l)).drop('time')
            data2 = data.isel(time=slice(l,ntime-1)  ).drop('time')
            tau_lag[l,:] = xr.corr(data1,data2,dim='time')

         # tau = tau_lag.sum(dim='lag')*(3./24.)

         tau = xr.DataArray(np.zeros([ncol]),dims=('ncol'))
         for n in range(ncol):
            for l in range(0,mxlag+1):
               if tau_lag[l,n]>0:
                  tau[n] += tau_lag[l,n]*(3./24.)
               else:
                  continue

      else:
         raise ValueError('No time dimension in data')
      #-------------------------------------------------------------------------
      # Calculate time and zonal mean
      #-------------------------------------------------------------------------
      # bin_ds = hc.bin_YbyX( data.mean(dim='time'), lat, lat_bins, bin_mode="explicit", wgt=area )
      # bin_ds = hc.bin_YbyX( data.mean(dim='time'), lat, bin_min=-88, bin_max=88, bin_spc=dlat, wgt=area )
      bin_ds = hc.bin_YbyX( tau, lat, bin_min=-90, bin_max=90, bin_spc=dlat, wgt=area )

      data_list.append( bin_ds['bin_val'].values )
      
      std_list.append( bin_ds['bin_std'].values )
      cnt_list.append( bin_ds['bin_cnt'].values )

      lat_bins = bin_ds['bins'].values

      sin_lat_bins = np.sin(lat_bins*np.pi/180.)

   #----------------------------------------------------------------------------
   # Take difference from first case
   #----------------------------------------------------------------------------
   if plot_diff :
      data_tmp = data_list
      data_baseline = data_list[0]
      for c in range(num_case): data_list[c] = data_list[c] - data_baseline
   #----------------------------------------------------------------------------
   # Check significance using t-test
   # https://stattrek.com/hypothesis-test/difference-in-means.aspx
   #----------------------------------------------------------------------------
   if plot_diff and chk_significance :
      for c in range(1,num_case):

         N0,N1 = cnt_list[0],cnt_list[c]
         # using number of months for N might make more sense?
         if num_files>0: N0,N1 = num_files,num_files  
         if len(years)>0: N0,N1 = len(years)*12,len(years)*12
         S0,S1 = std_list[0],std_list[c]
         
         # Standard error
         SE = np.sqrt( S0**2/N0 + S1**2/N1 )

         hc.print_stat(SE,name='SE',indent='    ')

         # Degrees of freedom
         DF = (S0**2/N0 + S1**2/N1)**2       \
             /( ( (S0**2/N0)**2 / (N0-1) )   \
               +( (S1**2/N1)**2 / (N1-1) ) )

         # t-statistic - aX is the difference now
         t_stat = data_list[c] / SE

         hc.print_stat(t_stat,name='t statistic',indent='    ')

         # Critical t-statistic
         t_crit = 2.24   # 2-tail test w/ inf dof & P=0.05

         for i in range(len(lat_bins)):
            msg = f'  lat: {lat_bins[i]}   t_stat: {t_stat[i]}   '
            if np.absolute(t_stat[i])>t_crit: msg = msg+tcolor.RED+'SIGNIFICANT'+tcolor.ENDC
            print(msg)

         # sig = new(dimsizes(aX),float)
         # sig = where(abs(t_stat).gt.t_crit,1,0)
         # copy_VarCoords(aX,sig)
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   unit_str = ''
   # if pvar[v] in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
   # if pvar[v] in ['LHFLX','SHFLX']           : unit_str = '[W/m2]'
   res.tiYAxisString = unit_str

   res.trXMinF = -1. #np.min( sin_lat_bins )
   res.trXMaxF =  1. #np.max( sin_lat_bins )

   lat_tick = np.array([-90,-60,-30,0,30,60,90])
   res.tmXBMode = "Explicit"
   res.tmXBValues = np.sin( lat_tick*3.14159/180. )
   res.tmXBLabels = lat_tick

   plot.append( ngl.xy(wks, sin_lat_bins, np.ma.masked_invalid(  np.stack(data_list) ), res) )

   var_str = pvar[v]
   if pvar[v]=='PRECT' : var_str = 'Precipitation'
   if 'WSPD' in pvar[v]: var_str = 'dvective timescale'
   hs.set_subtitles(wks, plot[v], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [num_pvar,1]
layout = [np.ceil(len(plot)/2),2]
# layout = [1,num_pvar]
if num_pvar==4 : layout = [2,2]
if num_pvar==6 : layout = [3,2]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

print()
for msg in msg_list: print(msg)

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
