import os
import ngl
import copy
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs

### case and name for plot title
# case,name = ['E3SM_HINDCAST-TEST_2016-08-01_ne30pg2_FC5AV1C-L_01'], 'HINDCAST 2016-08-01'
# case,name = ['E3SM_HINDCAST-TEST_2016-08-01_ne30pg2_F-MMF1_01'], 'MMF HINDCAST 2016-08-01'

case = ['E3SM_HINDCAST-TEST_2016-08-01_ne30pg2_FC5AV1C-L_01'
       ,'E3SM_HINDCAST-TEST_2016-08-01_ne30pg2_F-MMF1_01']
name = ['E3SM 2016-08-01','MMF 2016-08-01']

### full path of input file
scratch_path = '/global/cscratch1/sd/whannah/acme_scratch/cori-knl/'

### scrip file for native grid plot
scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'
# scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30np4_scrip.nc'

### list of variables to plot
var = ['TGCLDLWP','PRECT']
# var = ['OMEGA850']
# var = ['OMEGA850','TMQ','TGCLDLWP','PRECT']

lev = 850

### single time index to load (no averaging)
time1,time2 = 0, 48
# time1 = 20
# time2 = time1+5

# create_png,create_gif = True,False
create_png,create_gif = True,True

### output figure type and name
fig_type = 'png'

lat1,lat2 = -60,60
# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_hindcast/hindcast.animation.map.v1'+f'.{var[0]}.{lat1}_{lat2}'
gif_file = os.getenv('HOME')+'/Research/E3SM/figs_hindcast/hindcast.animation.map.v1'+f'.{var[0]}'

# lat1,lat2,lon1,lon2 = 0,40,360-160,360-80     # East Pac
# lat1,lat2,lon1,lon2 = -30,0,360-90,360-60     # S. America
# lat1,lat2,lon1,lon2 = -45,0,90,180            # Australia
# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.animation.map.v1'+f'.{var[0]}.lat_{lat1}_{lat2}.lon_{lon1}_{lon2}'


# print()
# print(f'input_file_path: {input_file_path}')
# print()

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

### set oup the plot resources
res = hs.res_contour_fill_map()

res.tmYLLabelFontHeightF         = 0.01
res.tmXBLabelFontHeightF         = 0.01

pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

res.mpGeophysicalLineColor = 'white'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# ds = xr.open_mfdataset( input_file_path, combine='by_coords', compat='override', data_vars=var)
# ncol = ds['ncol']
# mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
# if 'lat1' in vars(): mask = mask & (ds['lat']>=lat1)
# if 'lat2' in vars(): mask = mask & (ds['lat']<=lat2)
# if 'lon1' in vars(): mask = mask & (ds['lon']>=lon1)
# if 'lon2' in vars(): mask = mask & (ds['lon']<=lon2)

tfiles = ''
for t in range(time1,time2):
   fig_file = gif_file+'.'+str(t).zfill(3)
   tfiles = tfiles+'  '+f'{fig_file}.{fig_type}'
   print(f'  t: {t:3d}     {fig_file}.{fig_type}')

   if create_png :
      ### create the plot workstation
      wks = ngl.open_wks(fig_type,fig_file)
      # plot = []
      plot = [None]*(num_var*num_case)

      for c in range(num_case):
         input_file_path = f'{scratch_path}/{case[c]}/run/*cam.h1*'
         ds = xr.open_mfdataset( input_file_path, combine='by_coords', compat='override', data_vars=var)
         ncol = ds['ncol']
         mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
         if 'lat1' in vars(): mask = mask & (ds['lat']>=lat1)
         if 'lat2' in vars(): mask = mask & (ds['lat']<=lat2)
         if 'lon1' in vars(): mask = mask & (ds['lon']>=lon1)
         if 'lon2' in vars(): mask = mask & (ds['lon']<=lon2)
         if 'lat1' in vars() : res.mpMinLatF = lat1
         if 'lat2' in vars() : res.mpMaxLatF = lat2
         if 'lon1' in vars() : res.mpMinLonF = lon1
         if 'lon2' in vars() : res.mpMaxLonF = lon2
         for v in range(num_var):
            # print('  var: '+var[v])
            # ip = v*num_case+c
            ip = c*num_var+v
            #-------------------------------------------------------------------------
            # read the data
            #-------------------------------------------------------------------------         

            # data_interp = he.interpolate_to_pressure(ds,var[v],lev=lev)
            # data = data_interp.isel(time=t).where(mask,drop=True)

            data = ds[var[v]].isel(time=t).where(mask,drop=True)

            if var[v] in ['PRECT','PRECC','PRECL'] : 
               data = data*86400.*1e3
               data['units'] = 'mm/day'

            # hc.print_stat(data)

            # for variables that are blank at t=0, just use t=1
            if t==0 :
               if np.all(data.values==0) : 
                  data = ds[var[v]].isel(time=t+1).where(mask,drop=True)

            ### Calculate time mean
            # if 'time' in data.dims : data = data.mean(dim='time')

            ### Calculate area-weighted global mean
            # area = ds['area'].where( mask,drop=True)
            # gbl_mean = ( (data*area).sum() / area.sum() ).values 
            # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

            #-------------------------------------------------------------------------
            # Set colors and contour levels
            #-------------------------------------------------------------------------
            tres = copy.deepcopy(res)
            ### change the color map depending on the variable
            tres.cnFillPalette = 'ncl_default'
            if var[v] in ['OMEGA']                    : tres.cnFillPalette = 'BlueWhiteOrangeRed'
            if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
            if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = 'CBR_wet'
            if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = 'MPL_viridis'
            

            ### specify specific contour intervals
            if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,60+1,1)
            if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
            # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.arange(1,100+1,1)*1e-2
            if var[v]=='TGCLDLWP'            : tres.cnLevels = np.logspace( -2 , 0.25, num=60)
            # if var[v]=="TGCLDIWP"            : tres.cnLevels = np.arange(2,20+2,2)*1e-2
            if var[v]=='TMQ'                 : tres.cnLevels = np.arange(2,62+1,1)

            if var[v] in ['OMEGA850','OMEGA500'] : tres.cnLevels = np.arange(-100,100+2,2)*1e-2

            ### use this for symmetric contour intervals
            # if var[v] in ['U','V'] :
            #    cmin,cmax,cint,clev = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
            #                                               cint=None, max_steps=21,              \
            #                                               returnLevels=True, aboutZero=True )
            #    tres.cnLevels = np.linspace(cmin,cmax,num=91)

            if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
            
            #-------------------------------------------------------------------------
            # Set up cell fill attributes using scrip grid file
            #-------------------------------------------------------------------------
            scripfile = xr.open_dataset(scrip_file_name)
            tres.cnFillMode    = 'CellFill'
            tres.sfXArray      = scripfile['grid_center_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
            tres.sfYArray      = scripfile['grid_center_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
            tres.sfXCellBounds = scripfile['grid_corner_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values 
            tres.sfYCellBounds = scripfile['grid_corner_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
            
            #-------------------------------------------------------------------------
            # Create plot
            #-------------------------------------------------------------------------
            # plot.append( ngl.contour_map(wks,data.values,tres) )
            # hs.set_subtitles(wks, plot[len(plot)-1], name, '', var[v], font_height=0.015)
            plot[ip] = ngl.contour_map(wks,data.values,tres)
            hs.set_subtitles(wks, plot[ip], name[c], '', var[v], font_height=0.015)
      #------------------------------------------------------------------------------------------------
      # Finalize plot
      #------------------------------------------------------------------------------------------------
      # layout = [len(plot),1]
      # if num_var==4 : layout = [2,2]
      # layout = [num_var,num_case]
      layout = [num_case,num_var]
      ngl.panel(wks,plot[0:len(plot)],layout,pres)
      # del wks

      ### trim white space from image using imagemagik
      fig_file = fig_file+'.'+fig_type
      os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
      # print('\n'+fig_file+'\n')
      ngl.destroy(wks)

if create_png : ngl.end()
#---------------------------------------------------------------------------------------------------
# Create animated gif
#---------------------------------------------------------------------------------------------------
if create_gif :
   # cmd = "convert -repage 0x0 -delay 10 -loop 0  figs_scream/scream.animation.map.v1.*png   figs_scream/scream.animation.map.v1.gif"
   cmd = 'convert -repage 0x0 -delay 10 -loop 0  '+tfiles+' '+gif_file+'.gif'
   print('\n'+cmd+'\n')
   os.system(cmd)
   print('\n  '+gif_file+'.gif'+'\n')
