import os, ngl, copy, glob, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#-------------------------------------------------------------------------------
he.default_data_dir=os.getenv('HOME')+'/E3SM/scratch'
he.default_data_sub='run'
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub,clr,dsh,mrk = [],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,d=0,m=1,c='black'):
  global name,case,case_dir,case_sub
  tmp_name = case_in if n is None else n
  case.append(case_in); name.append(tmp_name)
  case_dir.append(p); case_sub.append(s);
  dsh.append(d) ; clr.append(c) ; mrk.append(m)
#-------------------------------------------------------------------------------

# init_date = '2013-10-01'
# add_case('E3SM.2022-SCREAMv1-COMP.F2010-MMF1.ne30pg2_oECv3.2013-10-01',n='MMF',c='red')

### test new SST method "use_all"
init_date = '2005-06-01'
add_case('E3SM.2022-HICCUP-SST-TEST-00.F2010.ne30pg2_oECv3',n='CTL',c='red')
add_case('E3SM.2022-HICCUP-SST-TEST-01.F2010.ne30pg2_oECv3',n='EXP',c='blue')


# grid = '180x360'
grid = '90x180'  # use this for ne30pg2!

obs_path = os.getenv('HOME')+f'/HICCUP/data_scratch/ERA5_validation.*.{init_date}.remap_{grid}.nc'

# list of variables to plot
var,lev = [],[]

htype = 'h1'
var.append(('TS','TS')); lev.append(-999)

# var.append(('Z500','z')); lev.append(500)
# var.append(('T500','t')); lev.append(500)
# var.append(('T850','t')); lev.append(850)
# var.append(('Q850','q')); lev.append(850)
# var.append(('U850','u')); lev.append(850)
# var.append(('U200','u')); lev.append(200)

# var.append(('V850','v')); lev.append(850)
# var.append(('V200','v')); lev.append(200)

# htype = 'h2'
# var.append(('T' ,'t')); lev.append(850)
# var.append(('Q' ,'q')); lev.append(850)
# var.append(('T' ,'t')); lev.append(500)
# var.append(('Z3','z')); lev.append(500)
# var.append(('U' ,'u')); lev.append(850)
# var.append(('V' ,'v')); lev.append(850)
# var.append(('U' ,'u')); lev.append(200)
# var.append(('V' ,'v')); lev.append(200)


# single time index to load (no averaging) - time max used for reference "climate" calculation
spd = 8
time1,time2 = 0,spd*60
# time2_ref = spd*1  # use first day as reference state (acc only)
time2_ref = time2  # use entire record as reference climate (acc only)

resample_daily = True

create_legend = False

verbose = True

# output figure type and name
fig_type = 'png'

# land_only = True
ocean_only = True

# lat1,lat2 = -30,30
lat1,lat2 = -60,60
# lat1,lat2 = 30,90  # South pole
# lat1,lat2 = -90,-30  # South pole
# lat1,lat2,lon1,lon2 = 30,50,360-125,360-75    # CONUS

method = 'acc'  # acc / rmse

fig_file = os.getenv('HOME')+f'/Research/E3SM/figs_hindcast/hindcast.fx_skill.v1.{method}'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
class tcolor: ENDC,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'

num_var = len(var)
num_case = len(case)

if 'name' not in vars(): name = case

if 'clr' not in vars(): clr = ['black']*num_case
if 'dsh' not in vars(): dsh = np.zeros(num_case)

# create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = []
# plot = [None]*(num_var*num_case)

# set oup the plot resources
res = hs.res_xy()
if resample_daily: 
  res.tiXAxisString = '[days]'
else:
  res.tiXAxisString = '[hours]'
res.xyLineThicknessF = 6
# res.xyDashPatterns = [0,1,2,3,4,5,6]
res.xyLineColors = clr
res.xyDashPatterns = dsh

# Open obs dataset
# ds_obs = xr.open_mfdataset(obs_path,combine='by_coords')
# ds_obs = ds_obs.rename({'latitude':'lat','longitude':'lon'})
# ds_obs = ds_obs.isel(time=slice(time1,time2))

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# for v,(mvar,ovar) in enumerate(var.items()):
for v,(mvar,ovar) in enumerate(var):

  print('\n  var: '+tcolor.MAGENTA+mvar+tcolor.ENDC+f'   lev={lev[v]}')

  obs_file = os.getenv('HOME')+f'/HICCUP/data_scratch/ERA5_validation.{ovar.upper()}.{init_date}.remap_{grid}.nc'
  print(f'  obs_file: {obs_file}')
  fx_metric_list = []
  time_list = []
  for c in range(num_case):
    # case_obj = he.Case( name=case[c] )

    data_dir_tmp,data_sub_tmp = None, None
    data_sub_tmp = f'data_remap_{grid}/'
    if case_dir[c] is not None: data_dir_tmp = case_dir[c]
    if case_sub[c] is not None: data_sub_tmp = case_sub[c]

    case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp )

    # ip = v*num_case+c
    ip = c*num_var+v

    ovar_tmp = ovar
    if ovar=='TS': ovar_tmp = 'skt'

    # Load Obs data
    ds_obs = xr.open_dataset(obs_file).rename({'latitude':'lat','longitude':'lon'})
    # ds_obs = ds_obs.isel(time=slice(time1,time2))
    obs_time = ds_obs['time'].isel(time=slice(time1,time2)) # this needs to match length of model data
    data_an = ds_obs[ovar_tmp].isel(time=slice(time1,time2))
    
    # Select the level - be careful as some fields do not have level coord
    if 'level' in data_an.coords: data_an = data_an.sel({'level':lev[v]})
    
    if ovar=='z': data_an = data_an/hc.g
    if ovar=='q': data_an = data_an*1e3

    # Assign coordinates
    data_an = data_an.assign_coords({'lat':ds_obs['lat'],'lon':ds_obs['lon']})
    if 'lat1' in locals(): data_an = data_an.sel(lat=slice(lat1,lat2))
    if 'lon1' in locals(): data_an = data_an.sel(lon=slice(lon1,lon2))

    # define reference state
    data_ref = data_an.isel(time=slice(time1,time2_ref))

    #---------------------------------------------------------------------------
    # read the data
    #---------------------------------------------------------------------------         
    # if 'lat1' in vars() : case_obj.lat1 = lat1
    # if 'lat2' in vars() : case_obj.lat2 = lat2
    # if 'lon1' in vars() : case_obj.lon1 = lon1
    # if 'lon2' in vars() : case_obj.lon2 = lon2

    # scratch_path = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/'
    # input_file_path = f'{scratch_path}/{case}/run/*cam.h1.2018-01-01-00000.nc'
    # ds = xr.open_mfdataset( input_file_path )
    # data1 = ds[var[v]].isel(time=0)#.where(mask,drop=True)
    # data2 = ds[var[v]].isel(time=-1)#.where(mask,drop=True)

    area = case_obj.load_data('area', htype=htype,lev=lev[v],use_remap=True,remap_str=f'remap_{grid}')

    data_fc = case_obj.load_data(mvar, htype=htype,lev=lev[v],use_remap=True,remap_str=f'remap_{grid}')

    if mvar=='TS': data_fc = data_fc + 273.15

    if 'lat1' in locals(): data_fc = data_fc.sel(lat=slice(lat1,lat2))
    if 'lon1' in locals(): data_fc = data_fc.sel(lon=slice(lon1,lon2))

    if 'plev' in data_fc.coords: data_fc = data_fc.sel({'plev':lev[v]*1e2})

    # print(); print(data_fc); exit()

    data_fc = data_fc.resample(time='3H').mean(dim='time')
    data_fc = data_fc.isel(time=slice(time1,time2))

    # method for dealing with time mismatch due to short runs
    if len(data_an['time'])>len(data_fc['time']):
      data_an = data_an.isel(time=slice(0,len(data_fc['time'])))


    # load to avoid dask array
    data_fc.load()
    data_an.load()
    #---------------------------------------------------------------------------
    # downsample
    #---------------------------------------------------------------------------
    # data_ref = data_ref.resample(time='6H').mean(dim='time')
    # data_an  = data_an.resample(time='6H').mean(dim='time')
    # data_fc  = data_fc.resample(time='6H').mean(dim='time')

    if resample_daily:
      data_ref = data_ref.resample(time='1D').mean(dim='time')
      data_an  = data_an.resample( time='1D').mean(dim='time')
      data_fc  = data_fc.resample( time='1D').mean(dim='time')

    #---------------------------------------------------------------------------
    # Regional subset
    #---------------------------------------------------------------------------
    # if land_only :
    #   land_frac = case_obj.load_data('LANDFRAC',htype='h1',use_remap=True,remap_str=f'remap_{grid}').astype(np.double)
    #   data = data*land_frac
    if ocean_only:
      ocn_frac = case_obj.load_data('OCNFRAC', htype='h0',lev=lev[v],
                                    use_remap=True,remap_str=f'remap_{grid}')
      ocn_frac = ocn_frac.isel(time=0)
      if 'lat1' in locals(): ocn_frac = ocn_frac.sel(lat=slice(lat1,lat2))
      if 'lon1' in locals(): ocn_frac = ocn_frac.sel(lon=slice(lon1,lon2))
      data_fc  = data_fc *ocn_frac.values
      data_an  = data_an *ocn_frac.values
      data_ref = data_ref*ocn_frac.values

    #---------------------------------------------------------------------------
    # Check that coordinates match
    #---------------------------------------------------------------------------
    if not np.all( data_fc.lon.values == data_an.lon.values ):
      print(); print(data_fc.lon) ; print(); print(data_an.lon) ; print()
      raise ValueError('ERROR: longitude coordinates do not match!')
    if not np.all( data_fc.lat.values == data_an.lat.values ):
      print(); print(data_fc.lat) ; print(); print(data_ref.lat) ; print()
      raise ValueError('ERROR: latitude coordinates do not match!')

    # # check for Nans and Infs
    # num_nan = np.isnan(data_an.values).sum()
    # num_inf = np.isinf(data_an.values).sum()
    # print(); print(f'data_an     nan cnt: {num_nan}  inf cnt: {num_inf}')
    # num_nan = np.isnan(data_fc.values).sum()
    # num_inf = np.isinf(data_fc.values).sum()
    # print(); print(f'data_fc     nan cnt: {num_nan}  inf cnt: {num_inf}')

    #---------------------------------------------------------------------------
    # print stuff prior to calculating forecast metrics
    #---------------------------------------------------------------------------
    print()
    print(f'    case: {tcolor.GREEN}{case[c]}{tcolor.ENDC}')
    if verbose: 
      hc.print_stat(data_an,stat='nxh',compact=True,indent='    ',name=f'Obs   - {ovar}')
      hc.print_stat(data_fc,stat='nxh',compact=True,indent='    ',name=f'Model - {mvar}')
    
    #---------------------------------------------------------------------------
    # Calculate forecast metric and add to list
    #---------------------------------------------------------------------------
    # Make time coordinate consistent
    data_fc['time'] = data_an['time'] # obs_time

    # calculate ACC metric using anomalies from reference state
    if method=='acc':
      data_fc = data_fc - data_ref.mean(dim=['time']).values
      data_an = data_an - data_ref.mean(dim=['time']).values
      acc = ( data_fc * data_an ).sum(dim=['lat','lon']).values 
      denom_fc = np.sqrt( (data_fc**2).sum(dim=['lat','lon']).values )
      denom_an = np.sqrt( (data_an**2).sum(dim=['lat','lon']).values )
      acc = acc / ( denom_fc * denom_an )
      
      # acc = ( data_fc * data_an ).sum(dim=['lat','lon']).values 
      # hc.print_stat(acc,stat='nxh',compact=True,indent='    ',name=f'{method}')
      # # acc = acc / np.sqrt( (data_fc**2).sum(dim=['lat','lon']).values * (data_an**2).sum(dim=['lat','lon']).values )
      # acc = (data_fc**2).sum(dim=['lat','lon']).values 
      # hc.print_stat(acc,stat='nxh',compact=True,indent='    ',name=f'{method}')
      # acc = (data_an**2).sum(dim=['lat','lon']).values 
      # hc.print_stat(acc,stat='nxh',compact=True,indent='    ',name=f'{method}')
      # exit()
      fx_metric_list.append( acc )

    # calculate root-mean-square-error from reanalysis
    if method=='rmse':
      rmse = np.sqrt( np.square( data_fc - data_an ).mean(dim=['lat','lon']) )
      fx_metric_list.append( rmse )

    if verbose: hc.print_stat(fx_metric_list[-1],stat='nxh',compact=True,indent='    ',name=f'{method}')

    # define time coordinate used for plotting
    days_from_zero = ( data_fc['time'] - data_fc['time'][0] ).astype('float')
    if resample_daily:
      days_from_zero= days_from_zero / 86400e9 # convert from nanoseconds to days
    else:
      days_from_zero = days_from_zero / 3600e9 # convert from nanoseconds to hours
    time_list.append( days_from_zero )

  #-----------------------------------------------------------------------------
  # Create plot
  #-----------------------------------------------------------------------------
  if method=='acc':
    res.trYMaxF = 1
    res.trYMinF = np.min(fx_metric_list) - np.std(fx_metric_list)
    res.tiYAxisString = f'Skill'
  if method=='rsme':
    res.tiYAxisString = f'RMSE'


  plot.append( ngl.xy(wks,np.stack(time_list),np.stack(fx_metric_list),res) )

  cstr = mvar
  if 'lat1' in locals() and 'lat2' in locals():
    lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
    lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
    if ocean_only: 
      cstr += f' ({lat1_str}:{lat2_str} Ocean only)'
    else:
      cstr += f' ({lat1_str}:{lat2_str})'
  hs.set_subtitles(wks, plot[len(plot)-1], '', cstr, '', font_height=0.015)



  # xyres = hs.res_xy()
  # plot.append( ngl.xy(wks,time.values,max_pt.values,xyres) )
  #-----------------------------------------------------------------------------
  # Ad legend
  #-----------------------------------------------------------------------------
  if create_legend: 
    lgres = ngl.Resources()
    # lgres.vpWidthF, lgres.vpHeightF  = 0.1, 0.12
    # lgres.lgLabelFontHeightF = 0.015
    # lgres.lgLineThicknessF   = 16
    lgres.vpWidthF, lgres.vpHeightF  = 0.05, 0.1 
    lgres.lgLabelFontHeightF = 0.008
    # lgres.lgLineThicknessF   = 4
    lgres.lgLineThicknessF   = res.xyLineThicknessF
    lgres.lgMonoLineColor    = False
    # lgres.lgMonoDashIndex    = True
    lgres.lgLineColors       = clr
    lgres.lgDashIndexes      = dsh
    lgres.lgLabelJust    = 'CenterLeft'
    
    # Set legend position
    lx,ly = 0.2, 0.4
    if len(var)==2: lx,ly = 0.2,0.9  # 1x2
    # if len(var)==2: lx,ly = 0.55,0.9  # 2x1
    if len(var)==4: lx,ly = 0.3, 0.9  # 2x2
    if len(var)==6: lx,ly = 0.2,0.8  # 2x3
    if len(var)==8: lx,ly = 0.14,0.64  # 2x4
    # if len(var)==6: lx,ly = 0.16,0.8  # 3x2

    pid = ngl.legend_ndc(wks, len(name), name, lx, ly, lgres)  

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5
# layout = [len(plot),1]
if len(plot)<=3:
  layout = [1,len(plot)]
else:
  layout = [2,np.ceil(len(plot)/2)]
# layout = [np.ceil(len(plot)/2),2]
# layout = [num_case,len(plot)/num_case]
ngl.panel(wks,plot[0:len(plot)],layout,pres)

hc.trim_png(fig_file)

