import os, ngl, copy, glob, xarray as xr, numpy as np, pandas as pd
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs

# he.default_data_dir='/global/homes/w/whannah/E3SM/scratch/'
# he.default_data_sub='run/'
# scratch_path = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/'

def add_case(case_in,n=None,d=0,m=16,c='black'):
   global name,case,clr,dsh,mrk
   case.append(case_in) ; dsh.append(d) ; clr.append(c) ; mrk.append(m)
   # if n is None: n = '' ; name.append(n)

### INCITE test runs
# ~/E3SM/chk.logs.py INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1
# ~/E3SM/chk.logs.py INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32
name,case,clr,dsh,mrk = [],[],[],[],[]
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_50_47.DT_5e0.2008-10-01'    ) # done / regrided

add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_1600.L_50_47.DT_5e0.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_50_47.DT_5e-1.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_50_47.DT_5e0.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_200.L_50_47.DT_5e-1.2008-10-01')

add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_1600.L_125_115.DT_2e0.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_125_115.DT_5e-1.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_125_115.DT_2e0.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_200.L_125_115.DT_5e-1.2008-10-01')

# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_1600.L_250_230.DT_5e-1.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_250_230.DT_5e-1.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_250_230.DT_5e-1.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_200.L_250_230.DT_5e-1.2008-10-01')

# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_1600.L_50_47.DT_5e0.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_50_47.DT_5e-1.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_50_47.DT_5e0.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_200.L_50_47.DT_5e-1.BVT.2008-10-01')

# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_1600.L_125_115.DT_2e0.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_125_115.DT_5e-1.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_125_115.DT_2e0.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_200.L_125_115.DT_5e-1.BVT.2008-10-01')

# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_1600.L_250_230.DT_5e-1.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_250_230.DT_5e-1.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_1600.L_250_230.DT_5e-1.BVT.2008-10-01')
# add_case('INCITE2020.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.DX_200.L_250_230.DT_5e-1.BVT.2008-10-01')

# build short name based on case name
for c in case:
  n = 'E3SM-MMF '
  for p in reversed([p.split('_') for p in c.split('.')]):
    # if p[0]=='NXY': n += f'{p[1]}'
    if p[0]=='L'  : n += f' L{p[1]}'
    # if p[0]=='DX' : n += f' dx={p[1]}'
  name.append(n)

first_file,num_files = 0,5

grid = '180x360'

# list of variables to plot
var,lev = {},[]
var.update({'FSNTOA':'FSNTOA'}); lev.append(0)     # absorbed SW

print_stats = False

plot_diff_from_first_case = False


# lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = 30,50,360-125,360-75    # CONUS
lat1,lat2,lon1,lon2 = -60,30,360-140,360-60

# output figure type and name
fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_hindcast/hindcast.map.ceres.v1'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'name' not in locals(): name = case
if 'clr'  not in locals(): clr = ['black']*num_case
if 'dsh'  not in locals(): dsh = np.zeros(num_case)

# create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case)

# set oup the plot resources
res = hs.res_contour_fill_map()
res.vpHeightF = 0.4
res.lbLabelFontHeightF           = 0.018

if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

for v,(mvar,ovar) in enumerate(var.items()):
  print('\n  var: '+mvar)
  data_list,lat_list,lon_list = [],[],[]
  for c in range(num_case):
    print('  case: '+case[c])
    
    # parse the case name to get the initialization date
    [iyr,imn,idy] = case[c].split('.')[-1].split('-')

    obs_dir   = os.getenv('HOME')+f'/Data/Obs/CERES'
    obs_files = glob.glob( f'{obs_dir}/CERES_SYN1deg-Day_Terra-Aqua-MODIS_Ed4.1_Subset_{iyr}{imn}{idy}*.nc' )

    case_obj = he.Case( name=case[c] )

    #---------------------------------------------------------------------------
    # Load model data first to get time range
    #---------------------------------------------------------------------------
    if ovar is None:
      data_fc = case_obj.load_data(mvar, htype='h1',lev=lev[v])
      if 'lev' in data_fc.dims: data_fc = data_fc.isel(lev=0)
    else:
      data_fc = case_obj.load_data(mvar, htype='h1',lev=lev[v],
                                  first_file=first_file,num_files=num_files,
                                  use_remap=True,remap_str=f'data_remap_{grid}')

    # regional subset
    if 'lat1' in locals(): data_fc = data_fc.sel(lat=slice(lat1,lat2))
    if 'lon1' in locals(): data_fc = data_fc.sel(lon=slice(lon1,lon2))

    # convert time coord to datetime64
    data_fc['time'] = data_fc.indexes['time'].to_datetimeindex(unsafe=True) 

    # drop last time point (partial day)
    if data_fc['time'][-1].dt.hour.values == 0 : 
      data_fc = data_fc.isel(time=slice(0,-1),drop=True)

    # convert to daily mean for comparison with CERES
    data_fc = data_fc.resample(time='24H',closed='left').mean(dim='time')

    # add 12 hour offset to match CERES convention
    data_fc['time'] = data_fc['time'] + np.timedelta64(12,'h')

    if print_stats: hc.print_stat(data_fc,name=f'Model - {mvar}',indent='    ',compact=True,stat='nax')

    #---------------------------------------------------------------------------
    # Load Obs data
    #---------------------------------------------------------------------------
    if ovar is not None:

      ds_obs = xr.open_mfdataset(obs_files)
      
      if ovar=='FSNTOA':
        data_an = ds_obs['toa_solar_all_daily'] - ds_obs['toa_sw_all_daily']
      else:
        data_an = ds_obs[ovar]

      # Assign coordinates
      data_an = data_an.assign_coords({'lat':ds_obs['lat'],'lon':ds_obs['lon']})

      # regional subset
      if 'lat1' in locals(): data_an = data_an.sel(lat=slice(lat1,lat2))
      if 'lon1' in locals(): data_an = data_an.sel(lon=slice(lon1,lon2))

      # subset time to match model data
      data_an = data_an.sel(time=data_fc['time'])

      # print stats to check for units issues
      if print_stats: hc.print_stat(data_an,name=f'Obs   - {ovar}',indent='    ',compact=True,stat='nax')

    #---------------------------------------------------------------------------
    # Calculate difference
    #---------------------------------------------------------------------------
    if ovar is not None: 
      data_diff = data_fc - data_an
    # else:
    #   data_diff = data_fc - data_fc.isel(time=0)

    data_list.append( data_diff.mean(dim='time').values )

    if ovar is not None: 
      lat_list.append( data_fc['lat'].values )
      lon_list.append( data_fc['lon'].values )
  #-----------------------------------------------------------------------------
  # Create plot
  #-----------------------------------------------------------------------------
  tres = res
  # tres.cnFillPalette = 'ncl_default'
  tres.cnFillPalette = 'BlueWhiteOrangeRed'
  # tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
  # tres.cnFillPalette = 'CBR_wet'
  if mvar in ['TGCLDLWP','TGCLDIWP']: tres.cnFillPalette = 'MPL_viridis'

  aboutZero = True
  if ovar is None : aboutZero = False
  clev_tup = ngl.nice_cntr_levels(np.min(data_list), np.max(data_list),
                                 cint=None, max_steps=11, 
                                 returnLevels=False, aboutZero=aboutZero )
  if clev_tup==None: 
    tres.cnLevelSelectionMode = "AutomaticLevels"   
  else:
    cmin,cmax,cint = clev_tup
    tres.cnLevels = np.linspace(cmin,cmax,num=21)
    tres.cnLevelSelectionMode = "ExplicitLevels"


  if mvar in ['PRECT','PRECC']   : tres.cnLevels = np.arange(2,60+2,2)

  if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = "ExplicitLevels"

  for c in range(num_case):

    if ovar is None: 
      case_obj = he.Case( name=case[c] )
      hs.set_cell_fill(tres,case_obj)
    else:
      tres.sfYArray = lat_list[c]
      tres.sfXArray = lon_list[c]
      tres.trXMinF,tres.trXMaxF = np.min(tres.sfXArray), np.max(tres.sfXArray)

    
    ip = v*num_case+c

    plot[ip] = ngl.contour_map(wks,data_list[c][:,:],tres) 

    hs.set_subtitles(wks, plot[ip], name[c], f'', mvar+' Bias', font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

layout = [num_var,num_case]
# num_col = 2 ; layout = [np.ceil(len(plot)/num_col),num_col]

ngl.panel(wks,plot[0:len(plot)],layout,pres)

hc.trim_png(fig_file)

