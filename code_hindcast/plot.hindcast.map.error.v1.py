import os, ngl, copy, glob, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs

he.default_data_dir=os.getenv('HOME')+'/E3SM/scratch/'
he.default_data_sub='run/'

# scratch_path = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/' ### NERSC
scratch_path = '/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/' ### OLCF

case,name,clr,dsh = [],[],[],[]
def add_case(case_in,n='',c='black',d=0):
   global name,case
   case.append(case_in); name.append(n); clr.append(c); dsh.append(d)

var,lev_list = [],[]
def add_var(var_name,obs_var_name=None,lev=-1): var.update({var_name:obs_var_name}); lev_list.append(lev)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
init_date = '2008-10-01'
case.append('E3SM.INCITE2021-HC.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_1600.L_50_48.DT_5e0.BVT.2008-10-01'); name.append('L50/1.6km')
case.append('E3SM.INCITE2021-HC.HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_125_115.DT_5e-1.BVT.2008-10-01'); name.append('L115/0.2km')
case.append('E3SM.INCITE2021-HC.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.DX_200.L_250_230.DT_2e-1.BVT.2008-10-01'); name.append('L250/0.2km')


grid = '180x360'  # use this for ne30pg2!
# obs_path = os.getenv('HOME')+f'/HICCUP/data_scratch/ERA5_validation.*.2016-08-01.remap_{grid}.nc'
# obs_path = os.getenv('HOME')+f'/HICCUP/data_scratch/ERA5_validation.*.2016-08-01.remap_{grid}.nc'

# scrip file for native grid plot
# scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'
# scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30np4_scrip.nc'

# list of variables to plot
var,lev = {},[]
# add_var('Z3',obs_var_name='z',lev=500)
# add_var('T',obs_var_name='t',lev=850)
# add_var('T850',obs_var_name='t',lev=850)
# add_var('Q850',obs_var_name='q',lev=850)
# add_var('U',obs_var_name='u',lev=200)
# add_var('U200',obs_var_name='u',lev=200)
# add_var('V850',obs_var_name='v',lev=850)
# add_var('V200',obs_var_name='v',lev=200)
# add_var('TS'  ,obs_var_name='TS')
# add_var('PS'  ,obs_var_name='sp')
# add_var('CLDLIQ',lev=500)
# add_var('CLDICE',lev=300)
# add_var('PRECT')
# add_var('LHFLX')
# add_var('QBOT')
# add_var('FLNT')
# add_var('FSNT')
# add_var('FSNS')
# add_var('FLNS')
# add_var('TGCLDLWP')
# add_var('TGCLDIWP')
# add_var('LWCF')
# add_var('SWCF')
# add_var('T850')
# add_var('Q850')
# add_var('OMEGA850')



# single time index to load (no averaging)
# time_list = [24]
# time_list = [0,6,12,18,24]
time_list = [0,24,24*2,24*3,24*4]

verbose = False

plot_diff_from_first_case = False

# output figure type and name
fig_type = 'png'

# land_only = True

# lat1,lat2 = -60,-30
# lat1,lat2,lon1,lon2 = 30,50,360-125,360-75    # CONUS

fig_file = os.getenv('HOME')+'/Research/E3SM/figs_hindcast/hindcast.map.error.v1'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)
num_time = len(time_list)

if 'name' not in vars(): name = case

if 'clr' not in vars(): clr = ['black']*num_case
if 'dsh' not in vars(): dsh = np.zeros(num_case)

# create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case*num_time)

# set oup the plot resources
res = hs.res_contour_fill_map()
res.vpHeightF = 0.4
# res.tiYAxisString = 'Time [hours]'
# res.tiXAxisString = 'Latitude'


# Open obs dataset
# ds_obs = xr.open_mfdataset(obs_path,combine='by_coords')
# ds_obs = ds_obs.rename({'latitude':'lat','longitude':'lon'})
# ds_obs = ds_obs.isel(time=slice(time1,time2))

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

for v,(mvar,ovar) in enumerate(var.items()):
  print('\n  var: '+mvar)
  if ovar is not None:
    ovar_tmp = ovar.capitalize()
    # ovar_tmp = mvar.capitalize()
    # if mvar=='TS': ovar_tmp='TS'
    # if mvar=='PS': ovar_tmp='PS'
    obs_file = os.getenv('HOME')+f'/HICCUP/data_scratch/ERA5_validation.{ovar_tmp}.{init_date}.remap_{grid}.nc'
    if mvar=='TS': ovar='skt'
    # obs_file = '/global/cscratch1/sd/whannah/e3sm_scratch/init_files/'
    # obs_file += 'HICCUP.atm_era5.2016-08-01.ne30np4.L72.remap_90x180.nc'
    print(f'  obs_file: {obs_file}')
  data_list,lat_list,lon_list = [],[],[]
  for c in range(num_case):
    print('    case: '+case[c])
    case_obj = he.Case( name=case[c] )

    # Load Obs data
    if ovar is not None:
      ds_obs = xr.open_dataset(obs_file)
      if 'latitude' in ds_obs.dims: ds_obs = ds_obs.rename({'latitude':'lat','longitude':'lon'})
      # ds_obs = ds_obs.isel(time=slice(time1,time2))
      data_an = ds_obs[ovar]
      # Select the level - be careful as some fiels do not have level coord
      if 'level' in ds_obs[ovar].coords: 
        data_an = data_an.sel({'level':lev_list[v]})
      if ovar=='z': data_an = data_an/9.81

      # Assign coordinates
      data_an = data_an.assign_coords({'lat':ds_obs['lat'],'lon':ds_obs['lon']})
      if 'lat1' in locals(): data_an = data_an.sel(lat=slice(lat1,lat2))
      if 'lon1' in locals(): data_an = data_an.sel(lon=slice(lon1,lon2))

      if mvar in ['Q850']: data_an = data_an*1e3

      # downsample again to 6-hour
      data_an = data_an.resample(time='6H').mean(dim='time')

      # define time to be 0 at start
      data_an['time'] = ( data_an['time'] - data_an['time'][0] )  / np.timedelta64(1, 'h')

      data_an = data_an.sel(time=time_list)

      if verbose: hc.print_stat(data_an,name=f'Obs   - {ovar}')
    #---------------------------------------------------------------------------
    # read the data
    #---------------------------------------------------------------------------         
    # if 'lat1' in vars() : case_obj.lat1 = lat1
    # if 'lat2' in vars() : case_obj.lat2 = lat2
    # if 'lon1' in vars() : case_obj.lon1 = lon1
    # if 'lon2' in vars() : case_obj.lon2 = lon2

    # input_file_path = f'{scratch_path}/{case}/run/*cam.h1.2018-01-01-00000.nc'
    # ds = xr.open_mfdataset( input_file_path )
    # data1 = ds[var[v]].isel(time=0)#.where(mask,drop=True)
    # data2 = ds[var[v]].isel(time=-1)#.where(mask,drop=True)

    if ovar is None:
      data_fc = case_obj.load_data(mvar, htype='h1',lev=lev_list[v])
      if 'lev' in data_fc.dims: data_fc = data_fc.isel(lev=0)
    else:
      data_fc = case_obj.load_data(mvar, htype='h1',lev=lev_list[v],use_remap=True,remap_str=f'remap_{grid}')

    if 'lat1' in locals(): data_fc = data_fc.sel(lat=slice(lat1,lat2))
    if 'lon1' in locals(): data_fc = data_fc.sel(lon=slice(lon1,lon2))

    data_fc = data_fc.resample(time='3H').mean(dim='time')
    # data_fc = data_fc.isel(time=slice(time1,time2))

    # downsample again to 6-hour
    data_fc = data_fc.resample(time='6H').mean(dim='time')

    # # Calculate difference from start
    # if ovar is None: 
    #   data_fc = data_fc - data_fc.isel(time=0)


    data_fc['time'] = ( data_fc['time'] - data_fc['time'][0] )  / np.timedelta64(1, 'h')
    data_fc = data_fc.sel(time=time_list)

    if verbose: hc.print_stat(data_fc,name=f'Model - {mvar}')

    if mvar in ['TS']:   data_fc = data_fc+273.15

    # print stats to check for units issues
    hc.print_stat(data_fc,stat='nx',name=f'fx {mvar}',indent='    ')
    if ovar is not None: hc.print_stat(data_an,stat='nx',name=f'an {ovar}',indent='    ')

    # Calculate difference from obs
    if ovar is not None: 
      data_fc = data_fc - data_an
    # else:
    #   data_fc = data_fc - data_fc.isel(time=0)

    ### address dimension issues
    if 'lev' in data_fc.dims: data_fc = data_fc.isel(lev=0)

    # print(data_fc)
    # print()
    # print(data_fc.values.shape)
    # print()
    # exit()

    data_list.append( data_fc.values )
    # time_list.append( time.values )
    if ovar is not None: 
      lat_list.append( data_fc['lat'].values )
      lon_list.append( data_fc['lon'].values )

  #-----------------------------------------------------------------------------
  # Create plot
  #-----------------------------------------------------------------------------
  tres = res
  # tres.cnFillPalette = 'ncl_default'
  tres.cnFillPalette = 'BlueWhiteOrangeRed'
  # tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
  # tres.cnFillPalette = 'CBR_wet'
  if mvar in ['TGCLDLWP','TGCLDIWP']: tres.cnFillPalette = 'MPL_viridis'

  aboutZero = True
  if ovar is None : aboutZero = False
  clev_tup = ngl.nice_cntr_levels(np.min(data_list), np.max(data_list),
                                 cint=None, max_steps=21, 
                                 returnLevels=False, aboutZero=aboutZero )
  if clev_tup==None: 
    tres.cnLevelSelectionMode = "AutomaticLevels"   
  else:
    cmin,cmax,cint = clev_tup
    tres.cnLevels = np.linspace(cmin,cmax,num=21)
    tres.cnLevelSelectionMode = "ExplicitLevels"


  if mvar in ['PRECT','PRECC']   : tres.cnLevels = np.arange(2,60+2,2)

  if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = "ExplicitLevels"

  for c in range(num_case):

    if ovar is None: 
      case_obj = he.Case( name=case[c] )
      hs.set_cell_fill(tres,case_obj)
    else:
      # tres.tiYAxisString = f'{mvar} Skill'
      # tres.tiYAxisString = f'Skill'

      # tres.trXMinF = -1.
      # tres.trXMaxF =  1.
      # tlat_tick = np.array([-90,-60,-30,0,30,60,90])
      # tres.tmXBMode = 'Explicit'
      # tres.tmXBValues = np.sin( lat_tick*np.pi/180. )
      # tres.tmXBLabels = lat_tick
      
      # tres.sfXArray = np.sin(data_fc['lat'].values*np.pi/180.)
      # tres.sfYArray = time.values
      # tres.sfXArray = np.sin( lat_list[c] *np.pi/180.)
      # tres.sfYArray = time_list[c]
      tres.sfYArray = lat_list[c]
      tres.sfXArray = lon_list[c]
      tres.trXMinF,tres.trXMaxF = np.min(tres.sfXArray), np.max(tres.sfXArray)

    for t in range(num_time):
      # ip = v*num_case+c
      ip = t*num_case*num_var+c*num_var+v

      # print(data_list[c][t,:,:].shape)
      # exit()

      if ovar is None: 

        if c>0 and plot_diff_from_first_case : 
          # Calculate difference from first case
          data_list[c][t,:] = data_list[c][t,:] - data_list[0][t,:]
          # tres.cnLevelSelectionMode = "AutomaticLevels"
          clev_tup = ngl.nice_cntr_levels(np.min(data_list[c][t,:]), np.max(data_list[c][t,:]),
                                 cint=None, max_steps=21, returnLevels=False, aboutZero=True )
          cmin,cmax,cint = clev_tup
          tres.cnLevels = np.linspace(cmin,cmax,num=21)
          tres.cnLevelSelectionMode = "ExplicitLevels"

        # plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c][t,:]  ),tres) 
        plot[ip] = ngl.contour_map(wks,data_list[c][t,:],tres) 
      else:
        # plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c][t,:,:]),tres) 
        plot[ip] = ngl.contour_map(wks,data_list[c][t,:,:],tres) 

      time_hr = time_list[t]
      hs.set_subtitles(wks, plot[ip], name[c], f'time: {time_hr} hr', mvar, font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5
if num_time>1:
  layout = [num_time,num_var*num_case]
else:
  layout = [num_case,num_var]
ngl.panel(wks,plot[0:len(plot)],layout,pres)

hc.trim_png(fig_file)

