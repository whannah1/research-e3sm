import os
import ngl
import copy
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs

he.default_data_dir='/global/homes/w/whannah/E3SM/scratch/'
he.default_data_sub='run/'

### case name for plot title


### full path of input file
scratch_path = '/global/cscratch1/sd/whannah/acme_scratch/cori-knl/'
case = ['E3SM_HINDCAST-TEST_2018-01-01_ne30_FC5AV1C-L_02','E3SM_HINDCAST-TEST_2018-01-01_ne30_FC5AV1C-L_03']
name = 'HINDCAST-TEST 2018-01-01'

# print(input_file_path)
# exit()

### scrip file for native grid plot
# scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'
scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30np4_scrip.nc'

### list of variables to plot
# var = ['PRECT']
# var = ['TMQ']
var = ['PS']

lev = 850

### single time index to load (no averaging)
time1,time2 = 0, -1

# create_png,create_gif = True,False
create_png,create_gif = False,True

### output figure type and name
fig_type = 'png'

# lat1,lat2 = -60,60
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_hindcast/hindcast.ps_diff.v1'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

### set oup the plot resources
res = hs.res_contour_fill_map()

# res.mpGeophysicalLineColor = 'white'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = []
# plot = [None]*(num_var*num_case)

for v in range(num_var):
   print('  var: '+var[v])
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )

      # ip = v*num_case+c
      ip = c*num_var+v
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------         
      # input_file_path = f'{scratch_path}/{case}/run/*cam.h1.2018-01-01-00000.nc'
      # ds = xr.open_mfdataset( input_file_path )
      # data1 = ds[var[v]].isel(time=0)#.where(mask,drop=True)
      # data2 = ds[var[v]].isel(time=-1)#.where(mask,drop=True)

      lat = case_obj.load_data('lat', htype='h1',num_files=1)
      lon = case_obj.load_data('lon', htype='h1',num_files=1)
      data = case_obj.load_data(var[v], htype='h1',num_files=1,lev=lev)

      time = data['time']
      time = ( time - time[0] ).astype('float') / 86400e9

      amax = ( data - data.isel(time=0) ).argmax(dim='ncol')
      # amax = data.argmax(dim='ncol')

      
      max_pt = data.isel(ncol=np.max(amax.values))

      xlat = lat.isel(ncol=np.max(amax.values)).values
      xlon = lon.isel(ncol=np.max(amax.values)).values


      # print(max_pt)
      # print(f'lat/lon : {xlat}  {xlon}')
      # exit()

      case_obj.lat1 = xlat-2
      case_obj.lat2 = xlat+2
      case_obj.lon1 = xlon-2
      case_obj.lon2 = xlon+2
      data = case_obj.load_data(var[v], htype='h1',num_files=1,lev=lev)

      # topo = case_obj.load_data('Z3', htype='h1',num_files=1)
      # topo = topo.isel(lev=-1,time=0)

      TS = case_obj.load_data('TS', htype='h1',num_files=1)
      TS = TS.isel(time=0) + 273
      

      data1 = data.isel(time=time1)
      data2 = data.isel(time=time2)
      diff = data2 - data1

      #-------------------------------------------------------------------------
      # Set up cell fill attributes using scrip grid file
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)

      tres.mpLimitMode = "LatLon" 
      tres.mpMinLatF   = case_obj.lat1
      tres.mpMaxLatF   = case_obj.lat2
      tres.mpMinLonF   = case_obj.lon1
      tres.mpMaxLonF   = case_obj.lon2

      hs.set_cell_fill(case_obj,tres)

      #------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      dres = copy.deepcopy(tres)

      ### change the color map
      tres.cnFillPalette = 'MPL_viridis'
      dres.cnFillPalette = 'ncl_default'

      ### specify specific contour intervals
      # tres.cnLevels = np.arange(2,62+1,1)

      ### use symmetric contour intervals for difference
      cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff.min().values, 
                                                 diff.max().values,
                                                 cint=None, 
                                                 max_steps=21,
                                                 returnLevels=True, 
                                                 aboutZero=True )
      dres.cnLevels = np.linspace(cmin,cmax,num=31)

      if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      plot.append( ngl.contour_map(wks,data1.values,tres) )
      hs.set_subtitles(wks, plot[len(plot)-1], name, '', f't = {time1}', font_height=0.01)

      plot.append( ngl.contour_map(wks,data2.values,tres) )
      hs.set_subtitles(wks, plot[len(plot)-1], name, '', f't = {time2}', font_height=0.01)

      # plot.append( ngl.contour_map(wks,diff.values,dres) )
      # hs.set_subtitles(wks, plot[len(plot)-1], name, 'difference', f't = {time2}', font_height=0.01)

      # tres.cnFillPalette = 'ncl_default'
      # tres.cnLevels = np.arange(255-10,255+10+1,1)

      # plot.append( ngl.contour_map(wks,TS.values,tres) )
      # hs.set_subtitles(wks, plot[len(plot)-1], name, '', 'TS', font_height=0.01)

      xyres = hs.res_xy()
      plot.append( ngl.xy(wks,time.values,max_pt.values,xyres) )
#------------------------------------------------------------------------------------------------
# Finalize plot
#------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5
# layout = [len(plot),1]
# layout = [1,len(plot)]
layout = [num_case,len(plot)/num_case]
ngl.panel(wks,plot[0:len(plot)],layout,pres)

hc.trim_png(fig_file)

