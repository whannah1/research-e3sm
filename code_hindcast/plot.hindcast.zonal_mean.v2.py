import os
import ngl
import copy
import glob
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs

he.default_data_dir='/global/homes/w/whannah/E3SM/scratch/'
he.default_data_sub='run/'

scratch_path = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/'


case = [
       'E3SM_HINDCAST_2016-08-01_ne30pg2_FC5AV1C-L_00'
       ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF2_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_MSA0_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_MSA4_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_FLUX-BYPASS_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_CRM-AC_00'
       ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_CRM-AC_RAD-AC_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_CRM-AC_ESMT_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_ESMT_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_RRTMG_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_RADCOL64_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_RADCOL1_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_SGS_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_SFC_00'
       ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_SGS_SFC_00'
       # ,'E3SM_HINDCAST_2016-08-01_ne30pg2_F-MMF1_CRM-AC_ESMT_SGS_SFC_00'
       ]
name = [
       'E3SM'
       ,'MMF1'
       # ,'MMF2'
       # ,'MMF1+FLUX-BYPASS'
       # ,'MMF1+CRM-AC'
       ,'MMF1+CRM-AC+RAD-AC'
       # ,'MMF1+CRM-AC+ESMT'
       # ,'MMF1+ESMT'
       # ,'MMF1+RRTMG'
       # ,'MMF1+RadCol=64'
       # ,'MMF1+RadCol=1'
       # ,'MMF1+SGS'
       # ,'MMF1+SFC'
       ,'MMF1+SFC+SGS'
       # ,'MMF1+CRM-AC+RAD-AC+SFC+SGS'
       ]

grid = '90x180'  # use this for ne30pg2!
obs_path = os.getenv('HOME')+f'/HICCUP/data_scratch/ERA5_validation.*.2016-08-01.remap_{grid}.nc'

# scrip file for native grid plot
# scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'
# scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30np4_scrip.nc'

# list of variables to plot
var = {}
# var.update({'Z3':'z'})
# var.update({'T':'t'})
var.update({'Q':'q'})
# var.update({'U':'u'})
# var.update({'V':'v'})
# var.update({'OMEGA':None})
# var.update({'QRS':None})
# var.update({'QRL':None})



# single time index to load (no averaging)
time_list = [6,12,24,48]

verbose = False

# output figure type and name
fig_type = 'png'

lat1,lat2 = -60,90

# land_only = True

fig_file = os.getenv('HOME')+'/Research/E3SM/figs_hindcast/hindcast.zonal_mean.v2'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)
num_time = len(time_list)

if 'name' not in vars(): name = case

if 'clr' not in vars(): clr = ['black']*num_case
if 'dsh' not in vars(): dsh = np.zeros(num_case)

# create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case*num_time)

# set oup the plot resources
res = hs.res_contour_fill()
res.vpHeightF = 0.3
# res.tiYAxisString = 'Time [hours]'
# res.tiXAxisString = 'Latitude'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

for v,(mvar,ovar) in enumerate(var.items()):
  print('\n  var: '+mvar)
  if ovar is not None:
    obs_file = os.getenv('HOME')+f'/HICCUP/data_scratch/ERA5_validation.{ovar.capitalize()}.2016-08-01.remap_{grid}.nc'
    print(f'  obs_file: {obs_file}')
  data_list, lat_list, lev_list = [],[],[]
  for c in range(num_case):
    print('    case: '+case[c])
    case_obj = he.Case( name=case[c] )


    # # Load Obs data
    # if ovar is not None:
    #   ds_obs = xr.open_dataset(obs_file).rename({'latitude':'lat','longitude':'lon'})
    #   data_an = ds_obs[ovar]
    #   # Select the level - be careful as some fiels do not have level coord
    #   # if 'level' in ds_obs[ovar].coords: 
    #   #   data_an = data_an.sel({'level':lev[v]})
    #   if ovar=='z': data_an = data_an/9.81

    #   # Assign coordinates
    #   data_an = data_an.assign_coords({'lat':ds_obs['lat'],'lon':ds_obs['lon']})

    #   if mvar in ['Q850']: data_an = data_an*1e3

    #   # downsample again to 6-hour
    #   data_an = data_an.resample(time='6H').mean(dim='time')

    #   # define time to be 0 at start
    #   data_an['time'] = ( data_an['time'] - data_an['time'][0] )  / np.timedelta64(1, 'h')

    #   data_an = data_an.sel(time=time_list)

    #   # zonally average
    #   data_an = data_an.mean(dim='lon')

    #   if verbose: hc.print_stat(data_an,name=f'Obs   - {ovar}')

    ovar = None
    #---------------------------------------------------------------------------
    # read the data
    #---------------------------------------------------------------------------

    # input_file_path = f'{scratch_path}/{case}/run/*cam.h1.2018-01-01-00000.nc'
    # ds = xr.open_mfdataset( input_file_path )
    # data1 = ds[var[v]].isel(time=0)#.where(mask,drop=True)
    # data2 = ds[var[v]].isel(time=-1)#.where(mask,drop=True)

    data_fc = case_obj.load_data(mvar, htype='h1',use_remap=True,remap_str=f'remap_{grid}')

    if 'lat1' in locals(): data_fc = data_fc.sel(lat=slice(lat1,lat2))
    if 'lon1' in locals(): data_fc = data_fc.sel(lon=slice(lon1,lon2))

    # Cut off the upper levels for a better look at what's happening in troposphere
    data_fc = data_fc.sel(lev=slice(100,1050))

    data_fc = data_fc.resample(time='3H').mean(dim='time')

    # downsample again to 6-hour
    # data_fc = data_fc.resample(time='6H').mean(dim='time')


    # zonally average
    data_fc = data_fc.mean(dim='lon')

    # Calculate difference from t=0
    data_fc = data_fc - data_fc.isel(time=0)

    data_fc['time'] = ( data_fc['time'] - data_fc['time'][0] )  / np.timedelta64(1, 'h')
    data_fc = data_fc.sel(time=time_list)

    if verbose: hc.print_stat(data_fc,name=f'Model - {mvar}')

    # print stats to check for units issues
    hc.print_stat(data_fc,stat='nx',name=f'fx {mvar}')
    if ovar is not None: hc.print_stat(data_an,stat='nx',name=f'an {ovar}')

    
    # if ovar is not None: 
    #   data_fc = data_fc - data_an
    # else:
    #   data_fc = data_fc - data_fc.isel(time=0)


    data_list.append( data_fc.values )
    # time_list.append( time.values )
    lat_list.append( data_fc['lat'].values )
    lev_list.append( data_fc['lev'].values )

  #-----------------------------------------------------------------------------
  # Create plot
  #-----------------------------------------------------------------------------
  aboutZero = True
  # aboutZero = False
  # if var[v] in [''] : aboutZero = True
  clev_tup = ngl.nice_cntr_levels(np.min(data_list), np.max(data_list),
                                 cint=None, max_steps=21, 
                                 returnLevels=False, aboutZero=aboutZero )
  if clev_tup==None: 
    tres.cnLevelSelectionMode = "AutomaticLevels"   
  else:
    cmin,cmax,cint = clev_tup
    res.cnLevels = np.linspace(cmin,cmax,num=21)
    res.cnLevelSelectionMode = "ExplicitLevels"

  for c in range(num_case):

    # res.tiYAxisString = f'{mvar} Skill'
    # res.tiYAxisString = f'Skill'

    # Settings for sin(lat) axis
    # res.trXMinF = -1.
    # res.trXMaxF =  1.
    lat_tick = np.array([-90,-60,-30,0,30,60,90])
    res.tmXBMode = 'Explicit'
    res.tmXBValues = np.sin( lat_tick*np.pi/180. )
    res.tmXBLabels = lat_tick

    # Use explicit level tickmarks
    lev_tick = np.array([800,400,200,100,50,10])
    res.tmYLMode = 'Explicit'
    res.tmYLValues = [float(k) for k in lev_tick]
    res.tmYLLabels = lev_tick

    res.trYReverse = True
    res.sfXArray = np.sin( lat_list[c] *np.pi/180.)
    # res.sfXArray = lat_list[c]
    # convert lev list to int to get rid of extra zeros after decimal
    res.sfYArray = lev_list[c]

    for t in range(num_time):
      # ip = v*num_case+c
      ip = t*num_case*num_var+c*num_var+v
      
      plot[ip] = ngl.contour(wks,data_list[c][t,:,:],res) 

      time_hr = time_list[t]
      hs.set_subtitles(wks, plot[ip], name[c], f'time: {time_hr} hr', mvar, font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5
layout = [num_time,num_var*num_case]
ngl.panel(wks,plot[0:len(plot)],layout,pres)

hc.trim_png(fig_file)

