#---------------------------------------------------------------------------------------------------
# Plot the zonal mean of the specified variables
#---------------------------------------------------------------------------------------------------
import os, ngl, copy, xarray as xr, numpy as np, string
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import cmocean
np.seterr(divide='ignore', invalid='ignore')
np.errstate(divide='ignore', invalid="ignore")
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = case_in.replace('E3SM.HV-SENS.ne30pg2_ne30pg2.FC5AV1C-L.','')
      tmp_name = tmp_name.replace('.',' ')
      tmp_name = tmp_name.replace('_','=')
      if tmp_name=='control': tmp_name = 'fixed-HV nu=1e15 hvss=1 hvsq=6'
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name);
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
pvar,lev_list = [],[]
def add_var(var_name,lev=-1): 
   pvar.append(var_name); lev_list.append(lev)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

# E3SM-HR dev tests
# add_case('20240827.v3.F2010-TMSOROC05-Z0015.ne120pg2_r025_icos30.Nomassfluxadj.n0256t120x1.kdreg2.hy',       n='ne120pg2 20240827 effgw_beres=0.25', p='/global/cfs/cdirs/e3sm/ndk/ms13-oct7/',s='run')
add_case('20241009.v3.F2010-TMSOROC05-Z0015.ne120pg2_r025_icos30.gw_dust_mods.n0256t120x1.coilr4.kdreg2.hy', n='ne120pg2 20241009 effgw=0.10', p='/global/cfs/cdirs/e3sm/ndk/tmpdata',s='run')
add_case('20241014.v3.F2010.ne120pg2_r025_icos30.pm-cpu.no_MCSP',                                            n='ne120pg2 20241014 no_MCSP',    p='/pscratch/sd/w/whannah/E3SMv3_dev',  s='run')

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


add_var('PRECT')
add_var('FLUT')
# add_var('U850')
# add_var('U',lev=850)
# add_var('U200')
# add_var('TGCLDLWP')
# add_var('TGCLDIWP')

# add_var('DMSE_RAD',lev=850)
# add_var('DMSE_RAD',lev=100)

# lev  = 850
# lon1 = -140
# lon2 = -50


htype,years,months,first_file,num_files = 'h1',[],[],0,11

fig_file = os.getenv('HOME')+'/Research/E3SM/figs_hov/hov.v1'

lat1,lat2 = -15,15
# lat1,lat2 = -5,5
# lon1,lon2 = 90,360-30
dlon = 1


recalculate = True

# time_mean_opt    = 'pentad' # none / daily / pentad / 10d
time_mean_opt    = '1D' # none / 1D / 5D / 10D

plot_diff        = False
add_phase_lines  = False

print_stats          = True
print_stats_summary  = False

use_common_label_bar = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
tmp_file = os.getenv('HOME')+'/Research/E3SM/data_temp/hov.v1'

num_pvar = len(pvar)
num_case = len(case)

wkres = ngl.Resources()
npix=2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks('png',fig_file,wkres)

plot = [None]*(num_pvar*num_case)
res = hs.res_contour_fill()
# res.vpHeightF = 0.3
res.vpWidthF = 0.2

if 'clr' not in vars(): 
   if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   else : clr = ['black']

# if num_case>1 and 'dsh' not in vars(): dsh = np.arange(0,num_case,1)
if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.tiYAxisString = 'Days'
res.tiXAxisString = 'Longitude'
# res.tiXAxisString = 'sin( Latitude )'
res.lbLabelFontHeightF           = 0.012

res.trYReverse = True

# use this for reference phase speed lines
lres = hs.res_xy()
lres.xyLineThicknessF = 4
lres.xyDashPattern = 2
# lres.xyLineColor = 'red'
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   if 'INCITE2019'      in case: comp = 'cam'
   if 'RGMA'            in case: comp = 'cam'
   if 'CESM'            in case: comp = 'cam'
   if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
msg_list = []
for v in range(num_pvar):
   print('  var: '+hc.tcolor.MAGENTA+pvar[v]+hc.tcolor.ENDC)
   data_list,std_list,cnt_list,time_list = [],[],[],[]
   lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      mode = ''

      if htype=='h1' and time_mean_opt=='daily' : mode = '.daily'
      if htype=='h1' and time_mean_opt=='pentad': mode = '.pentad'

      case_tmp_file = f'{tmp_file}.{case[c]}.{pvar[v]}{mode}.nc'

      if recalculate:
         case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), data_dir=data_dir_tmp, data_sub=data_sub_tmp )
         case_obj.set_coord_names(pvar[v])
         #----------------------------------------------------------------------
         # read the data
         #----------------------------------------------------------------------
         if 'lat1' in vars() : case_obj.lat1,case_obj.lat2 = lat1,lat2
         if 'lon1' in vars() : case_obj.lon1,case_obj.lon2 = lon1,lon2
         if 'lev'  in vars() : case_obj.lev  = lev

         # lat  = case_obj.load_data('lat',  htype=htype)
         lon  = case_obj.load_data('lon',  htype=htype)
         area = case_obj.load_data('area', htype=htype).astype(np.double)
         data = case_obj.load_data(pvar[v],htype=htype,lev=lev,first_file=first_file,num_files=num_files)

         # test_ncol = 100
         # print(data.isel(ncol=test_ncol).values)

         # Convert to daily mean
         # if htype=='h1' and time_mean_opt=='daily' : data = data.resample(time='D').mean(dim='time')
         # if htype=='h1' and time_mean_opt=='pentad': data = data.resample(time='5D').mean(dim='time')
         if htype=='h1' and time_mean_opt!='none': data = data.resample(time=time_mean_opt).mean(dim='time')

         # print(); print(data.isel(ncol=test_ncol).values)
         # exit()

         # Get rid of lev dimension
         if 'lev' in data.dims : data = data.isel(lev=0)

         hc.print_time_length(data.time)

         if print_stats: 
            msg = hc.print_stat(lon,name='longitude',stat='nx',indent='    ',compact=True)
            msg = hc.print_stat(data,name=pvar[v],stat='naxsh',indent='    ',compact=True)
            msg_list.append('  case: '+case[c]+'\n'+msg)
            # if 'area' in vars() :
            #    gbl_mean = ( (data*area).sum() / area.sum() ).values 
            #    print(f'      Area Weighted Global Mean : {gbl_mean:6.4}')

         #----------------------------------------------------------------------
         # aggregate data into logitude bins, preserving time dimension
         #----------------------------------------------------------------------
         # print(f'  min/max lon values: {lon.min().values}  /  {lon.max().values}')
         bin_ds = hc.bin_YbyX( data, lon, bin_min=lon.min().values, bin_max=lon.max().values
                              ,bin_spc=dlon, wgt=area, keep_time=True )

         bin_ds = bin_ds.drop_vars(['bin_std'])

         bin_ds['bin_val'] = bin_ds['bin_val'].transpose('time','bin')
         bin_ds['bin_cnt'] = bin_ds['bin_cnt'].transpose('time','bin')

         bin_ds['time'] = data['time']


         # for t in range(len(data['time'])):
         #    bin_ds = hc.bin_YbyX( data.isel(time=t), lon, bin_min=bin_min, bin_max=bin_max, bin_spc=dlon, wgt=area )
         #    if t==0:
         #       print(f't: {t}')
         #       num_time = len(data['time'].values)
         #       num_bins = len(bin_ds['bins'].values)
         #       bin_ds_out = xr.Dataset( { 'bin_val': (['time','bins'],np.zeros([num_time,num_bins])) }
         #                                ,coords={'time':data['time'],'bins':bin_ds['bins']} )
         #    bin_ds_out['bin_val'][t,:] = bin_ds['bin_val'][:]
         # data_list.append( bin_ds_out['bin_val'].values )
         # # std_list.append( bin_ds_out['bin_std'].values )
         # # cnt_list.append( bin_ds_out['bin_cnt'].values )
         # lon_bins = bin_ds_out['bins'].values
         # # lat_bins = bin_ds['bins'].values
         # # sin_lat_bins = np.sin(lat_bins*np.pi/180.)

         #----------------------------------------------------------------------
         # write to file
         #----------------------------------------------------------------------
         print('      writing to file: '+case_tmp_file)
         bin_ds.to_netcdf(path=case_tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( case_tmp_file )

      # hc.print_stat(bin_ds['bin_val'],name='bin_val',stat='naxsh',indent='    ',compact=True)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      data_list.append( bin_ds['bin_val'].values )
      time_list.append( bin_ds['time'] )

      if c==0: lon_bins = bin_ds['bin'].values


   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   #----------------------------------------------------------------------------
   # Take difference from first case
   #----------------------------------------------------------------------------
   if plot_diff :
      data_tmp = data_list
      data_baseline = data_list[0]
      for c in range(num_case): data_list[c] = data_list[c] - data_baseline
   
   #----------------------------------------------------------------------------
   # Set color levels
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.cnFillPalette = "MPL_viridis"

   # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
   # tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

   if pvar[v]=='PRECT' and time_mean_opt=='10D': tres.cnLevels = np.arange(1,15+1,1)
   if pvar[v]=='PRECT' and time_mean_opt=='5D' : tres.cnLevels = np.arange(1,20+1,1)
   if pvar[v]=='PRECT' and time_mean_opt=='1D' : tres.cnLevels = np.arange(2,40+2,2)
   # if pvar[v]=='LHFLX' : tres.cnLevels = np.arange(240,430,5)/1e1
   # if pvar[v]=='FLNT'  : tres.cnLevels = np.arange(120,280+10,10)

   tres.cnLevelSelectionMode = 'ExplicitLevels'
   if not hasattr(tres,'cnLevels') : 
      nlev = 21
      aboutZero = False
      if pvar[v] in ['U','U850'] : 
         aboutZero = True
      clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                      returnLevels=False, aboutZero=aboutZero )
      if clev_tup==None: 
         tres.cnLevelSelectionMode = 'AutomaticLevels'   
      else:
         cmin,cmax,cint = clev_tup
         tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
   
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   unit_str = ''
   if pvar[v] in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
   if pvar[v] in ['LHFLX','SHFLX']           : unit_str = '[W/m2]'

   if use_common_label_bar: 
      tres.lbLabelBarOn = False
   else:
      tres.lbLabelBarOn = True
   
   # tres.tiYAxisString = unit_str

   # tres.trXMinF = -1. 
   # tres.trXMaxF =  1. 
   # lat_tick = np.array([-90,-60,-30,0,30,60,90])
   # tres.tmXBMode = "Explicit"
   # tres.tmXBValues = np.sin( lat_tick*3.14159/180. )
   # tres.tmXBLabels = lat_tick

   # tres.trXMinF = np.min( lon_bins )
   # tres.trXMaxF = np.max( lon_bins )

   tres.sfXArray = lon_bins

   var_str = pvar[v]
   # if pvar[v]=="PRECT" : var_str = "Precipitation"

   for c in range(num_case):
      # ip = c*num_pvar+v
      ip = v*num_case+c

      time = time_list[c]
      time = ( time - time[0] ).astype('float') / 86400e9
      tres.sfYArray = time.values

      # make a copy to double length of longitude
      if 'lon1' not in locals():
         shp1 = data_list[c].shape
         shp2 = (shp1[0],shp1[1]*2)
         tmp_data = np.empty(shp2)
         tmp_data[:,0:shp1[1]] = data_list[c]
         tmp_data[:,shp1[1]:] = data_list[c]
         new_lon_bins = np.empty(shp2[1])
         # new_lon_bins[0:shp1[1]] = lon_bins
         # new_lon_bins[shp1[1]:]  = lon_bins+np.max(lon_bins)+dlon
         new_lon_bins[0:shp1[1]] = lon_bins-np.max(lon_bins)
         new_lon_bins[shp1[1]:]  = lon_bins+dlon
         tres.sfXArray = new_lon_bins
         tres.tmXBMode = "Explicit"
         lon_tick_vals = [-180,-270,-90,0,90,270,180]
         tres.tmXBValues = np.array(lon_tick_vals)
         tres.tmXBLabels = lon_tick_vals
      else:
         tmp_data = data_list[c]

      # plot[ip] = ngl.contour(wks, np.ma.masked_invalid(  data_list[c] ), tres) 
      plot[ip] = ngl.contour(wks, tmp_data, tres) 

      #-------------------------------------------------------------------------
      # overlay phase speed lines
      if add_phase_lines:
         max_day = int(np.max(time))
         # phs_spd,sday1,sday2,sday_frq = -15.,0         ,max_day*2,3  # westward phase speed
         phs_spd,sday1,sday2,sday_frq =  15.,-max_day*2,max_day  ,5  # eastward phase speed
         phs_spd  = phs_spd * 86400./111e3 # convert m/s => deg/day
         for start_day in range( sday1, sday2, sday_frq ):
            phs_y0   = np.min( lon_bins ) - phs_spd*start_day # intercept
            phs_line = phs_spd*time + phs_y0
            phs_plot = ngl.xy(wks, phs_line.values, time.values, lres)
            ngl.overlay( plot[ip] , phs_plot )

      #-------------------------------------------------------------------------
      # add plot strings
      #-------------------------------------------------------------------------
      hs.set_subtitles(wks, plot[ip], name[c], '', var_str, font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [np.ceil(len(plot)/2),2]

# layout = [num_case,num_pvar]
layout = [num_pvar,num_case]

pnl_res = hs.setres_panel()

### use common label bar
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

### add panel labels
pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
pnl_res.nglPanelFigureStringsJust        = "TopLeft"
pnl_res.nglPanelFigureStringsFontHeightF = 0.01
if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

# pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

if print_stats_summary:
   print()
   for msg in msg_list: print(msg)

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
