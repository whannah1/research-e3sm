import os
import ngl
import subprocess as sp
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy

git_hash = 'cbe53b'
case =[
      f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}',
      f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}',
      f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}',
      f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}',
      # f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}',
      # f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}',
      ]
name = []
for c in case:
   if 'E3SM.PGVAL.ne30_r05_oECv3'    in c: name.append('ne30np4')
   if 'E3SM.PGVAL.ne30pg2_r05_oECv3' in c: name.append('ne30pg2')
   if 'E3SM.PGVAL.ne30pg3_r05_oECv3' in c: name.append('ne30pg3')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3' in c: name.append('ne30pg4')
   if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4')
   if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2')



#-------------------------------------------------------------------------------
# var = ['aice'] # ice area  (aggregate)
# var = ['hi'] # grid cell mean ice thickness / ice volume per unit grid cell area
# var = ['hs'] # grid cell mean snow thickness
# var = ['Tsfc'] # snow/ice surface temperature
# var = ['qi','qs'] # internal ice / snow heat content / MISSING VALUES!
# var = ['alvdr','alvdf'] # visible direct/diffuse albedo
# var = ['evap','evap_ai'] # evaporative water flux
# var = ['snow','rain'] # rainfall / snowfall rate
# var = ['fswabs'] # snow/ice/ocn absorbed solar flux
# var = ['fswthru'] # SW thru ice to ocean
var = ['fsurf_ai','fcondtop_ai'] # net surface heat flux / top surface conductive heat flux
# var = [''] #

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_ice/ice.map.v1'

# htype,years,months,first_file,num_files = 'h0',[],[1,2,12],0,0
# htype,years,months,first_file,num_files = 'h0',[],[6,7,8],0,0
htype,years,months,first_file,num_files = 'h',[],[],0,0


# lat1,lat2 = 60,90
# lat1,lat2,lon1,lon2 = 60,80,0,20

plot_diff = True
add_diff = False
diff_base = 0
# diff_case = [1,2]

use_snapshot = False
rm_zonal_mean = False

print_stats = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

# subtitle_font_height = 0.015 - 0.0003*(num_var*num_case)
subtitle_font_height = 0.02 - 0.0003*num_var - 0.0014*num_case

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]

wks = ngl.open_wks(fig_type,fig_file)
if plot_diff:
   if add_diff:
      plot = [None]*(num_var*(num_case*2-1))
   else:
      plot = [None]*(num_var*(num_case))
else:
   plot = [None]*(num_var*num_case)
res = hs.res_contour_fill_map()
# if 'lat1' in vars() : res.mpMinLatF = lat1
# if 'lat2' in vars() : res.mpMaxLatF = lat2
# if 'lon1' in vars() : res.mpMinLonF = lon1
# if 'lon2' in vars() : res.mpMaxLonF = lon2


res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012

# res.mpGeophysicalLineColor = 'white'

res.tmXBOn = False
res.tmYLOn = False

res.mpProjection         = 'Stereographic'
res.mpEllipticalBoundary = True
res.mpCenterLatF         = 90.
res.mpLimitMode          = 'Angles'
res.mpBottomAngleF       = 50
res.mpLeftAngleF         = 50
res.mpRightAngleF        = 50
res.mpTopAngleF          = 50

# res.mpLimitMode = "LatLon" 
# res.mpMinLatF   = 45 -15
# res.mpMaxLatF   = 45 +15
# res.mpMinLonF   = 180-15
# res.mpMaxLonF   = 180+15


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   data_list,lat_list,lon_list = [],[],[]

   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c] )
      area_name = 'tarea'
      case_obj.ncol_name ='ni'
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      area = case_obj.load_data(area_name,component='cice',htype=htype,years=years,months=months,first_file=first_file,num_files=num_files).astype(np.double)
      data = case_obj.load_data(var[v],   component='cice',htype=htype,years=years,months=months,first_file=first_file,num_files=num_files)

      if 'nj' in data.dims: data = data.isel(nj=0)

      if 'time' in data.dims : hc.print_time_length(data.time,indent=' '*6)

      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh')

      # average over time
      if 'time' in data.dims : 
         if use_snapshot:
            data = data.isel(time=0)
            print('WARNING - plotting snapshot!!!')
         else:
            data = data.mean(dim='time')
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
         
      # Calculate area weighted global mean
      if 'area' in vars() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(f'\n      Area Weighted Global Mean : {gbl_mean:6.4}\n')
      
      data_list.append( data.values )

      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()
   #------------------------------------------------------------------------------------------------
   #------------------------------------------------------------------------------------------------
   # Open scrip file for Ocean grid
   scripfile = xr.open_dataset('/project/projectdirs/e3sm/inputdata/ocn/mpas-o/oEC60to30v3/ocean.oEC60to30v3.scrip.181106.nc')

   scripfile['grid_center_lon'] = scripfile['grid_center_lon']*180/3.14159
   scripfile['grid_center_lat'] = scripfile['grid_center_lat']*180/3.14159
   scripfile['grid_corner_lon'] = scripfile['grid_corner_lon']*180/3.14159
   scripfile['grid_corner_lat'] = scripfile['grid_corner_lat']*180/3.14159

   if 'lat1' in vars():
      # Create maks with coordinates that match scrip file
      mask = xr.DataArray( np.ones(len(scripfile['grid_size']),dtype=bool), coords=[scripfile['grid_size']], dims='grid_size' )
      # Replace mask values
      mask.values = case_obj.get_mask(ds=xr.open_dataset(case_obj.get_hist_file_list(component='cice')[0])).isel(nj=0)
      # Apply mask to dataset
      scripfile = scripfile.where(mask,drop=True)

   # Set plot grid resources based on masked scrip coordinates
   res.sfXArray      = scripfile['grid_center_lon'].values
   res.sfYArray      = scripfile['grid_center_lat'].values
   res.sfXCellBounds = scripfile['grid_corner_lon'].values
   res.sfYCellBounds = scripfile['grid_corner_lat'].values
   res.cnFillMode    = "CellFill"
   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   # for d in data_list: print(d.shape)

   data_min = np.min([np.min(d) for d in data_list])
   data_max = np.max([np.max(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.min(d) for d in tmp_data])
      diff_data_max = np.max([np.max(d) for d in tmp_data])

   for c in range(num_case):
      case_obj = he.Case( name=case[c] )
      case_obj.ncol_name ='ni'
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      ip = v*num_case+c
      # ip = c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      # tres.cnFillPalette = "CBR_wet"
      # tres.cnFillPalette = "BlueWhiteOrangeRed"

      # if var[v] in ['PRECT','PRECC'] : tres.cnLevels = np.arange(1,20+1,1)
      # if var[v]=='SPTLS': tres.cnLevels = np.linspace(-0.001,0.001,81)
      
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         aboutZero = False
         if var[v] in ['U','V'] : 
            aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max,  \
                                         cint=None, max_steps=21, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = "AutomaticLevels"   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=21)
            tres.cnLevelSelectionMode = "ExplicitLevels"

      var_str = var[v]
      # if var[v]=="PRECT" : var_str = "Precipitation"

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------

      if plot_diff : 
         if c==diff_base : base_name = name[c]    

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 
         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 

         #----------------------------------------------------------------------
         #----------------------------------------------------------------------
         ctr_str = ''
         case_name = name[c] if 'name' in vars() else case_obj.short_name

         if 'lev' in locals():
            if len(lev)==1:
               if lev>0: ctr_str = f'{lev} mb'

         hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :

         if add_diff: 
            # ip = ((c+1)*2-1)*num_var+v
            ip = v*(num_case*2-1) + num_case+c-1
         else:
            ip = v*num_case+c
            # ip = c*num_var+v
         data_list[c] = data_list[c] - data_baseline.values

         #if print_stats: hc.print_stat(data,name=f'{var[v]} DIFF')

         tres.cnFillPalette = 'ncl_default'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-12,12+2,2)
         if not hasattr(tres,'cnLevels') : 
            # if np.min(data_list[c]).values==np.max(data_list[c]).values : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print('WARNING: Difference is zero!')
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)
               # tres.cnLevelSelectionMode = "AutomaticLevels"

         tres.lbLabelBarOn = True
         plot[ip] = ngl.contour_map(wks,data_list[c],tres)

         ctr_str = ''
         # case_name = name[c]+' - '+base_name
         if 'name' in vars():
            case_name = name[c]
         else:
            case_name = case_obj.short_name
         
         hs.set_subtitles(wks, plot[ip], case_name, 'Diff', var_str, font_height=subtitle_font_height)

      
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if plot_diff : num_case = num_case+len(diff_case)   # use this to plot both before and after diff

# title_str = None
# title_str = 'ANN'
# if months==[1,2,12]: title_str = 'DJF'
# if months==[6,7,8]: title_str = 'JJA'

# if title_str is not None:
#    textres =  ngl.Resources()
#    textres.txFontHeightF =  0.025
#    ngl.text_ndc(wks,title_str,0.5,.7,textres)

if plot_diff:
   # layout = [num_case,num_var]
   layout = [num_var,num_case]
   # if add_diff: layout = [num_case*2,num_var]
   if add_diff: layout = [num_var,num_case*2-1]
else:
   # layout = [num_case,num_var]
   layout = [num_var,num_case]

   # if num_var==1 and num_case==4 : layout = [2,2]
   # if num_var==1 and num_case==6 : layout = [3,2]
   if num_case==1 and num_var==4 : layout = [2,2]

pnl_res = hs.setres_panel()
# pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
