import os,ngl,copy,subprocess as sp
import xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
import datetime, time
home = os.getenv('HOME')

case = ['E3SM.ne30pg2_r05_oECv3.F-MMF1.01','E3SM.ne30pg2_r05_oECv3.F-MMF1.CRM-AC.RAD-AC.01']
name = ['MMF','MMF+CRM-AC']

var = ['H2OSOI']

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_land/land.soil_moisture.ARM-SGP"

clat,clon, dx = 36, 360-97, 2.5 # ARM SGP
lat1,lat2,lon1,lon2 = clat-dx,clat+dx,clon-dx,clon+dx

htype,years,months,num_files = 'h0',[],[],0

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

wks_res = ngl.Resources()
wks_res.wkColorMap = "MPL_cool"
plot = []
wks = ngl.open_wks(fig_type,fig_file,wks_res)

res = hs.res_xy()
res.vpHeightF                    = 0.4
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.xyLineThicknessF             = 12.

color_map = ngl.get_MDfloat_array(wks,"wkColorMap")
num_color = len(color_map[:,0])

#---------------------------------------------------------------------------------------------------
# Load Obs Data
#---------------------------------------------------------------------------------------------------
# scratch = os.getenv('SCRATCH')
# files = sp.check_output(f'ls {scratch}/ARM-SGP/OKM_monthly/*',shell=True,universal_newlines=True ).split('\n')
files = sp.check_output(f'ls {home}/Data/Obs/ARM/OKM_monthly/*',shell=True,universal_newlines=True ).split('\n')
if files[-1]=='' : files.pop()
# def avg_by_station(ds): return ds.mean(dim='station_number').resample(time='D').mean('time',skipna=True)
# ds_obs = xr.open_mfdataset(files[:365*2],combine='by_coords',preprocess=avg_by_station)
ds_obs = xr.open_mfdataset(files[:365*2],combine='by_coords')
data_obs = ds_obs['volumetric_water_content']
# data_obs = data_obs.resample(time='MS').mean('time',skipna=True)    # Convert to monthly mean
max_lev = len(data_obs['depth'])
time = ( data_obs['time'] - data_obs['time'][0] ).astype('float') / 86400e9 / 365

# for y in range(20):
#   print()
#   print(f'year: {y}')
#   print()
#   print(data_obs.values[y*12:y*12+12+1])

# print(time.min().values)
# print(time.max().values)
# print()
# print(data_obs.min().values)
# print(data_obs.max().values)
# exit()

print('\nARM Soil Depths [cm]:')
for d in data_obs['depth'].values: print(f'  {d}')
#---------------------------------------------------------------------------------------------------
# Plot Obs Data
#---------------------------------------------------------------------------------------------------
plot = [None]
tres = copy.deepcopy(res)
# tres.trYMinF = np.min(data_obs[:,:max_lev].values)
# tres.trYMaxF = np.max(data_obs[:,:max_lev].values)
tres.trYMinF,tres.trYMaxF = 0.1,0.35

# # Plot long time series of all soil levels
# tres.tiXAxisString = 'Time [years]'
# clr = np.arange(20,num_color,max_lev).astype(int)
# tres.xyLineColor = clr[0]
# plot.append( ngl.xy(wks, time.values, data_obs[:,0].values, tres) )
# for k in range(1,max_lev) :
#   tres.xyLineColor = clr[k]
#   ngl.overlay( plot[len(plot)-1], ngl.xy(wks, time.values, data_obs[:,k].values, tres) )

# overlay yearly time series of top level
nyear = 20
clr = np.linspace(20,num_color,nyear+2).astype(int)
tres.tmXBMode = 'Explicit'
tres.tmXBValues = np.arange(0,12)
tres.tmXBLabels = ['J','F','M','A','M','J','J','A','S','O','N','D']
tres.tiXAxisString = 'Month'
for y in range(0,nyear+1) :
  tres.xyLineColor = clr[y]
  t1, t2 = y*12, y*12+12
  # tplot = ngl.xy(wks, time[t1:t2].values, data_obs[t1:t2,0].values, tres)
  tplot = ngl.xy(wks, np.arange(0,12), data_obs[t1:t2,0].values, tres)
  if y==0: 
    plot[0] = tplot
  else:
    ngl.overlay( plot[0], tplot )

hs.set_subtitles(wks, plot[len(plot)-1], 'ARM-SGP OKM', '', 'Vol. Soil Moisture', font_height=0.015)


# ngl.panel(wks,plot[0:len(plot)-1],[1,1],hs.setres_panel())
ngl.panel(wks,plot,[1,1],hs.setres_panel())
ngl.end()
hc.trim_png(fig_file)

exit()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   for c in range(num_case):
      print('    case: '+case[c])
      case_obj = he.Case( name=case[c], grid='r05' )

      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      area_name = 'area'
      # if 'DYN_' in tvar : 
      #    if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
      #    if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
      #    if 'pg2' in case[c] or 'pg3' in case[c] : 
      #       case_obj.ncol_name = 'ncol_d'
      #       area_name = 'area_d'

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      ### uncommentt this to subset the data
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      
      data = case_obj.load_data(tvar,     component='clm2',htype=htype,years=years,months=months,num_files=num_files,lev=lev)
      area = case_obj.load_data(area_name,component='clm2',htype=htype,years=years,months=months,num_files=num_files).astype(np.double)


      # if 'levgrnd' in data.dims : data = data.sum(dim='levgrnd')
      # if 'levgrnd' in data.dims : data = data.isel(levgrnd=9)     # deepest layer
      
      if 'lndgrid' in data.dims:
            data = data.rename({'lndgrid':'ncol'})
            area = area.rename({'lndgrid':'ncol'})

      data = data.rename({'levgrnd':'lev'})


      dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
      time_msg = '    Time length: '+str(dtime)
      # if dtime>60 : time_msg += '  ('+str(dtime.astype('timedelta64[M]'))+')\n'
      # if dtime>365: time_msg += '  ('+str(dtime.astype('timedelta64[Y]'))+')\n'
      print(time_msg)

      # X = X.mean(dim='time')

      if 'ncol' in data.dims:
            data_avg = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      elif 'lat' in data.dims:
            data_avg = ( (data*area).sum(dim=('lat','lon')) / area.sum(dim=('lat','lon')) )

      print()
      print(data_avg)
      print()

      max_lev = 0
      for k in range(1,len(data_avg.lev)) :
            if np.all( data_avg[:,k].values < 0.001 ) : continue
            max_lev = k
            # print(f'  {k:2i}   {data_avg[:,k].values}')
      # exit()

      # if 'time' in data_avg.dims : 
      # avg_X = data_avg.mean(dim='time')
      # print('\n      Area Weighted Global Mean : '+'%f'%avg_X+'\n')

      #-------------------------------------------------------------------------
      # Load spin up data
      #-------------------------------------------------------------------------
      # idir = '~/Research/E3SM/'
      # ifile = idir+'E3SM_PG-LAND-SPINUP_ne120pg2_FC5AV1C-H01A_00.clm2.h0.'+var[v]+'.nc'

      # ds = xr.open_dataset(ifile)

      # # area = ds['area'].rename({'lndgrid':'ncol'})
      # data = ds[var[v]].rename({'lndgrid':'ncol','levgrnd':'lev'})

      # gbl_baseline = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )

      # time_baseline = ( gbl_baseline['time'] - gbl_baseline['time'][0] ).astype('float') / 86400e9 / 365

      # print()
      # print(gbl_baseline)
      # print()
      # exit()
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.tiXAxisString = 'Time [days]'
      tres.tiXAxisString = 'Time [years]'

      time = ( data_avg['time'] - data_avg['time'][0] ).astype('float') / 86400e9 / 365

      tres.trYMinF = np.min(data_avg[:,:max_lev].values)
      tres.trYMaxF = np.max(data_avg[:,:max_lev].values)

      if var[v]=='H2OSOI': tres.trYMinF,tres.trYMaxF = 0.15,0.31

      clr = np.arange(20,num_color,max_lev).astype(int)

      tres.xyLineColor = clr[0]
      plot.append( ngl.xy(wks, time.values, data_avg[:,0].values, tres) )

      for k in range(1,max_lev) :
            tres.xyLineColor = clr[k]
            ngl.overlay( plot[len(plot)-1], ngl.xy(wks, time.values, data_avg[:,k].values, tres) )
      #-------------------------------------------------------------------------
      # Combine with baseline
      #-------------------------------------------------------------------------
      # # Shift the time to line up with baseline
      # # time = time+365*3
      # time = time+3

      # tres.trXMinF = np.min( [np.min(time.values),             np.min(time_baseline.values)] )
      # tres.trXMaxF = np.max( [np.max(time.values),             np.max(time_baseline.values)] )
      # tres.trYMinF = np.min( [np.min(data_avg[:,:max_lev].values),np.min(gbl_baseline[:,:max_lev].values)] )
      # tres.trYMaxF = np.max( [np.max(data_avg[:,:max_lev].values),np.max(gbl_baseline[:,:max_lev].values)] )

      # tres.xyDashPattern = 1
      # tres.xyLineColor = clr[0]
      # plot.append( ngl.xy(wks, time_baseline.values, gbl_baseline[:,0].values, tres) )
      # for k in range(1,max_lev) :
      #       tres.xyLineColor = clr[k]
      #       ngl.overlay( plot[len(plot)-1], ngl.xy(wks, time_baseline.values, gbl_baseline[:,k].values, tres) )

      # tres.xyDashPattern = 0
      # for k in range(0,max_lev) :
      #       tres.xyLineColor = clr[k]
      #       ngl.overlay( plot[len(plot)-1], ngl.xy(wks, time.values, data_avg[:,k].values, tres) )

      #-------------------------------------------------------------------------
      # Set strings at top of plot
      #-------------------------------------------------------------------------
      var_str = var[v]

      if 'name' in vars():
         case_name = name[c]
      else:
         case_name = case_obj.short_name

      ctr_str = ''
      hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.015)
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
layout = [num_var,num_case]
# layout = [num_case,num_var]

# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

if num_var==1 and num_case==4 : layout = [2,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------