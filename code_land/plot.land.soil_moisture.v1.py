import os, copy, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)

var,lev_list,mask_flag = [],[],[]
def add_var(var_name,lev=-1,mask=None): 
   var.append(var_name); lev_list.append(lev),mask_flag.append(mask)
#---------------------------------------------------------------------------------------------------

### AMIP test on perlmutter
# add_case('E3SM.AMIP.GNUGPU.ne30pg2_r05_oECv3.F20TR-MMFXX-CMIP6.MOMFB.BVT.00',n='E3SM-MMF AMIP',p='/pscratch/sd/w/whannah/e3sm_scratch/perlmutter',s='run',g='r05')

### runoff denial test
# name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
# pscratch = '/pscratch/sd/w/whannah/e3sm_scratch/perlmutter'
# add_case('E3SM.RUNOFF-TEST.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.BVT.00',                            n='MMF',          p=pscratch,s='run')
# add_case('E3SM.RUNOFF-TEST.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.BVT.NO-RUNOFF.00',                  n='MMF NO-RUNOFF',p=pscratch,s='run')
# add_case('E3SM.RUNOFF-TEST.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.BVT.NO-RUNOFF.MAX-INFIL.NO-POND.00',n='MMF NO-POND',  p=pscratch,s='run')

### INCITE2021-HR
# add_case('E3SM.INCITE2021-HR.GNUGPU.ne120pg2_r0125_oRRS18to6v3.F-MMFXX.BVT.00',     n='MMF ne120pg2')
# add_case('E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x1.BVT.01',   n='MMF ne45pg2')

### SCREAM LR Cess tests
# add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010-SCREAM-LR.SSTP_0K.NN_64',n='SCREAMv0 +0K')
# add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010-SCREAM-LR.SSTP_4K.NN_64',n='SCREAMv0 +4K')
# add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010.SSTP_0K.NN_64',          n='E3SMv2 +0K')
# add_case('E3SM.SCREAM-CESS-LR.ne30pg2_oECv3.F2010.SSTP_4K.NN_64',          n='E3SMv2 +4K')


add_case('ELM_spinup.ICRUELM.ne0np4-2024-nimbus-iraq-32x3-pg2.20-yr.2015-10-01' ,n='iraq-32x3', p='/p/lustre2/hannah6/e3sm_scratch',s='run')
add_case('ELM_spinup.ICRUELM.ne0np4-2024-nimbus-iraq-64x3-pg2.20-yr.2015-10-01' ,n='iraq-64x3', p='/p/lustre2/hannah6/e3sm_scratch',s='run')
add_case('ELM_spinup.ICRUELM.ne0np4-2024-nimbus-iraq-128x3-pg2.20-yr.2015-10-01',n='iraq-128x3',p='/p/lustre2/hannah6/e3sm_scratch',s='run')


add_var('H2OSOI')
# add_var('TSOI')

fig_type = "png"
fig_file = os.getenv("HOME")+"/Research/E3SM/figs_land/land.soil_moisture.v1"


# lat1,lat2 = -30,30
# lon1,lon2 = -90,-60
# lon1,lon2 = 0,90
# clat,clon, dx = 36, -97, 2.5 # ARM SGP
# lat1,lat2,lon1,lon2 = clat-dx,clat+dx,clon-dx,clon+dx

# lat1,lat2,lon1,lon2 = -45,-45+60,360-90,360-30  # S. America

htype,num_files = 'h0',12*20

print_stats = False

convert_to_annual_mean = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

wks_res = ngl.Resources()
# wks_res.wkColorMap = "MPL_cool"

wks = ngl.open_wks(fig_type,fig_file,wks_res)
plot = []
res = hs.res_xy()
res.vpHeightF                    = 0.4
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.xyLineThicknessF             = 12.

color_map = ngl.get_MDfloat_array(wks,"wkColorMap")
num_color = len(color_map[:,0])


def get_comp(case):
   comp = 'elm'
   # comp = 'clm2'
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   time_list,data_list = [],[]
   for c in range(num_case):
      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      # if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      # if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case(  name=case[c], 
                           data_dir=case_dir[c], 
                           data_sub=case_sub[c], 
                           populate_files=False )

      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      area_name = 'area'
      # if 'DYN_' in tvar : 
      #    if 'pg2' in case[c] : case_obj.grid = case_obj.grid.replace('pg2','np4')
      #    if 'pg3' in case[c] : case_obj.grid = case_obj.grid.replace('pg3','np4')
      #    if 'pg2' in case[c] or 'pg3' in case[c] : 
      #       case_obj.ncol_name = 'ncol_d'
      #       area_name = 'area_d'

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      ### uncommentt this to subset the data
      if 'lat1' in vars() : case_obj.lat1 = lat1
      if 'lat2' in vars() : case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1
      if 'lon2' in vars() : case_obj.lon2 = lon2

      data = case_obj.load_data(tvar,     component='elm',htype=htype,num_files=num_files,lev=lev)
      area = case_obj.load_data(area_name,component='elm',htype=htype,num_files=num_files).astype(np.double)

      if 'lat' in data.coords and 'lat1' in vars():
         data = data.sel(lat=slice(lat1,lat2))
         area = area.sel(lat=slice(lat1,lat2))
      if 'lon' in data.coords and 'lon1' in vars():
         data = data.sel(lon=slice(lon1,lon2))
         area = area.sel(lon=slice(lon1,lon2))

      # if 'levgrnd' in data.dims : data = data.sum(dim='levgrnd')
      # if 'levgrnd' in data.dims : data = data.isel(levgrnd=9)     # deepest layer
      
      if 'lndgrid' in data.dims:
            data = data.rename({'lndgrid':'ncol'})
            area = area.rename({'lndgrid':'ncol'})

      data = data.rename({'levgrnd':'lev'})

      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      # if 'time' in data.dims: hc.print_time_length(data['time'],indent=' '*6)

      ### print stats before averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      if 'ncol' in data.dims:
            gbl_data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      elif 'lat' in data.dims:
            gbl_data = ( (data*area).sum(dim=('lat','lon')) / area.sum(dim=('lat','lon')) )

      if convert_to_annual_mean:
         # print(); print(gbl_data)
         gbl_data = gbl_data.resample(time='YE').mean(dim='time')
         # print(); print(gbl_data)
         # exit()

      if c==0:
         max_lev = len(gbl_data.lev)
         ### for soil moisture => omit lowest level because it is essentially zero (not sure why)
         if var[v] in ['H2OSOI']:
            max_lev = 0
            for k in range(1,len(gbl_data.lev)) :
                  if np.all( gbl_data[:,k].values < 0.001 ) : continue
                  max_lev = k
                  # print(f'  {k:2d}   {gbl_data[:,k].values}')
         # exit()

      # if 'time' in gbl_data.dims : 
      # avg_X = gbl_data.mean(dim='time')
      # print('\n      Area Weighted Global Mean : '+'%f'%avg_X+'\n')

      time = ( gbl_data['time'] - gbl_data['time'][0] ).astype('float') / 86400e9 / 365

      data_list.append( gbl_data[:,:max_lev].values )
      time_list.append( time.values )

      #-------------------------------------------------------------------------
      # Load spin up data
      #-------------------------------------------------------------------------
      # idir = '~/Research/E3SM/'
      # ifile = idir+'E3SM_PG-LAND-SPINUP_ne120pg2_FC5AV1C-H01A_00.clm2.h0.'+var[v]+'.nc'

      # ds = xr.open_dataset(ifile)

      # # area = ds['area'].rename({'lndgrid':'ncol'})
      # data = ds[var[v]].rename({'lndgrid':'ncol','levgrnd':'lev'})

      # gbl_baseline = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )

      # time_baseline = ( gbl_baseline['time'] - gbl_baseline['time'][0] ).astype('float') / 86400e9 / 365

      # print()
      # print(gbl_baseline)
      # print()
      # exit()
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   # tres.tiXAxisString = 'Time [days]'
   tres.tiXAxisString = 'Time [years]'
      

   # tres.trYMinF = np.min(gbl_data[:,:max_lev].values)
   # tres.trYMaxF = np.max(gbl_data[:,:max_lev].values)

   tres.trYMinF = np.min([np.nanmin(d) for d in data_list])
   tres.trYMaxF = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
   tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])

   # if var[v]=='H2OSOI': tres.trYMinF,tres.trYMaxF = 0.15,0.31
   # if var[v]=='H2OSOI': tres.trYMinF,tres.trYMaxF = 0.1,0.38

   clr = np.linspace(30,num_color-1,max_lev).astype(int)

   for c in range(num_case):

      tres.xyLineColor = clr[0]
      plot.append( ngl.xy(wks, time_list[c], data_list[c][:,0], tres) )

      for k in range(1,max_lev) :
            tres.xyLineColor = clr[k]
            ngl.overlay( plot[len(plot)-1], ngl.xy(wks, time_list[c], data_list[c][:,k], tres) )
      #-------------------------------------------------------------------------
      # Combine with baseline
      #-------------------------------------------------------------------------
      # # Shift the time to line up with baseline
      # # time = time+365*3
      # time = time+3

      # tres.trXMinF = np.min( [np.min(time.values),             np.min(time_baseline.values)] )
      # tres.trXMaxF = np.max( [np.max(time.values),             np.max(time_baseline.values)] )
      # tres.trYMinF = np.min( [np.min(gbl_data[:,:max_lev].values),np.min(gbl_baseline[:,:max_lev].values)] )
      # tres.trYMaxF = np.max( [np.max(gbl_data[:,:max_lev].values),np.max(gbl_baseline[:,:max_lev].values)] )

      # tres.xyDashPattern = 1
      # tres.xyLineColor = clr[0]
      # plot.append( ngl.xy(wks, time_baseline.values, gbl_baseline[:,0].values, tres) )
      # for k in range(1,max_lev) :
      #       tres.xyLineColor = clr[k]
      #       ngl.overlay( plot[len(plot)-1], ngl.xy(wks, time_baseline.values, gbl_baseline[:,k].values, tres) )

      # tres.xyDashPattern = 0
      # for k in range(0,max_lev) :
      #       tres.xyLineColor = clr[k]
      #       ngl.overlay( plot[len(plot)-1], ngl.xy(wks, time.values, gbl_data[:,k].values, tres) )

      #-------------------------------------------------------------------------
      # Set strings at top of plot
      #-------------------------------------------------------------------------
      var_str = var[v]


      ctr_str = ''
      hs.set_subtitles(wks, plot[len(plot)-1], case_name[c], ctr_str, var_str, font_height=0.015)
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [len(plot),1]
layout = [num_var,num_case]
# layout = [num_case,num_var]

# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

if num_var==1 and num_case==4 : layout = [2,2]

ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------