; plots a series of maps that can be animated
; v1 - create a animated map using high freq output - use specific regridded file
; v2 - streamlined version for standard output
; v3 - similar to v2 - but does not regrid  
;    - plot on native grid for model data
;    - obs data is handled in a clunky way due to different grids - might be able to streamline
; v4 - similar to v3 (use on native grid)
;    - configured for longer animations (3-hourly instead of 30 min)
;    - TRMM is interpolated to cube sphere grid
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/NCL/custom_functions_E3SM.ncl"
begin

    ;;; use scratch data
    E3SM_data_sub = "/run"
    ; E3SM_data_dir = "/lustre/atlas1/cli115/scratch/hannah6/"
    ; E3SM_data_dir = "/global/cscratch1/sd/whannah/acme_scratch/edison/"
    E3SM_data_dir = "/global/cscratch1/sd/whannah/acme_scratch/cori-knl/"

    E3SM_ignore_h2 = True

    
    ; case = "TRMM"
    ; case = (/"TRMM","E3SM_SP1_CTL_ne30_32x1_1km_00"/)
    ; case = (/"E3SM_SP1_CTL_ne30_32x1_1km_00"/)

    ; case = (/"E3SM_SP1_TEST_ne30_128x1_2km_00","E3SM_SP1_CTL_ne30_128x1_2km_80"/)
    ; case = "E3SM_SP1_GPST_ne30_32x1_1km_20"
    ; case = "E3SM_ZM_TEST_ne30_00"
    ; case = (/"GPM","E3SM_SP1_TEST_ne30_32x1_1km_00","E3SM_ZM_TEST_ne30_00"/)
    ; case = (/"GPM","E3SM_SP1_TEST_ne30_32x1_1km_00"/)

    ; grid = "ne120pg2";+(/"",""/)
    ; grid = (/"ne30","ne30pg2","ne120pg2"/)
    ; grid = (/"ne30","ne30pg2"/)
    ; case = "E3SM_CTEST-H_"+grid+"_F-EAMv1-AQP1"
    ; casename = grid

    ; case = (/"E3SM_CTEST-H_ne30_F-EAMv1-AQP1"       \
    ;          ; ,"E3SM_TEST_ne30pg2_F-EAMv1-AQP1"      \
    ;          ,"E3SM_CTEST-H_ne30pg2_F-EAMv1-AQP1"   \
    ;          ,"E3SM_CTEST-H_ne30pg2_F-EAMv1-AQP1_01"   \
    ;          /)

    ; case = "E3SM_TEST_ne30pg2_FC5AV1C-L"
    ; case = "E3SM_SP1_TEST_ne30pg2_64x1_1.0km_s2_00" 

    ; case = "E3SM_LOPG-1WT_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00"
    ; case = "E3SM_HOPG-1WT_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_00"

    case = "E3SM_RCE-TEST_ne30_F-EAMv1-RCEMIP_00"

    ; var = (/"PRECT","PRECT","PRECT"/)
    ; var = (/"PRECT","LHFLX","SHFLX"/)
    ; var = "PRECT"
    var = "CWV"
    ; var = "DYN_OMEGA"

    use_nday = True
    nday = 5


    panel_flip  = True      ; True = case x var

    make_png = True         ; set to False if you only want to remake the gif
    make_gif = True


    num_r = 1               ; for 1 variable and multiple regions

    obs_case = "TRMM"       ; Set this to whatever obs dataset is used above
                            ; don't use multiple obs datasets here


    fig_type = "png"
    fig_file = "~/Research/E3SM/figs_animation/animation.map.v4."+var ;+".small"

;===================================================================================
;===================================================================================
    
    num_c = dimsizes(case)
    num_v = dimsizes(var)

    if num_r.gt.1 then
        num_v = 1
    end if

    if num_c.eq.1 then
        gif_file = fig_file+"."+case+".gif"
    else
        gif_file = fig_file+".gif"
    end if

    opt  = True
    ; opt@debug    = True
    ; opt@monthly  = True
    ; opt@use3h = True
    ; opt@GPMyr1 = 2017
    ; opt@GPMyr2 = 2017
    opt@TRMMyr1 = 2000
    opt@TRMMyr2 = 2000
    opt@use_grid_native = True
    ; nyears = 1
    ; opt@year = ispan(1,nyears-1,1)
    opt@year = 1

    ; opt@lat1 = -60.
    ; opt@lat2 =  60.

    ; opt@lev = 500
    opt@lev = -2

    if use_nday then
        opt@monthly  = False
        ; opt@daily_avg = True
        opt@num_day = nday
        ; opt@day0 = 365
        ; opt@f0 = 365
    end if

    if .not.isvar("casename") then casename = E3SM_get_casename(case) end if
    casename = where(case.eq."E3SM_ZM_CTL_ne30_00"              ,"Standard E3SM" ,casename)
    casename = where(case.eq."E3SM_SP1_CTL_ne30_32x1_1km_00"    ,"SP-E3SM" ,casename)

    casename = where(case.eq."E3SM_SP1_TEST_ne30_128x1_2km_00"   ,"SP-E3SM 128x2km 72L RRTMG" ,casename)
    casename = where(case.eq."E3SM_SP1_CTL_ne30_128x1_2km_80"    ,"SP-E3SM 128x2km 30L RRTMG" ,casename)

    
    res = setres_contour_fill_smalltxt()
    ;res = setres_contour()
    ; res@lbLabelBarOn    = False
    res@lbTitleString = ""
    ; res@cnLineLabelsOn  = False
    ; res@cnInfoLabelOn   = False
    ; res@gsnAddCyclic    = False

    ; res@tmXBLabelFontHeightF        = 0.02
    ; res@tmYLLabelFontHeightF        = 0.02
    ; res@gsnLeftStringFontHeightF    = 0.025
    ; res@gsnCenterStringFontHeightF  = 0.025
    ; res@gsnRightStringFontHeightF   = 0.025
    res@mpCenterLonF    = 180.
    ; if isatt(opt,"lat1") then res@mpMinLatF = opt@lat1 end if
    ; if isatt(opt,"lat2") then res@mpMaxLatF = opt@lat2 end if

    ; res@cnFillPalette   = "MPL_hsv"

    ; res@mpProjection    = "Satellite"
    ; res@mpProjection    = "Mollweide"

    ; res@cnFillMode          = "RasterFill"
    res@cnFillMode          = "CellFill"

    ;;; default global view without polar regions
    res@mpMinLatF = -70
    res@mpMaxLatF =  70

; res@mpMinLatF =  0
; res@mpMaxLatF = 70
; res@mpMinLonF = 90
; res@mpMaxLonF = 180

    ;;; Tropical West Pac
    ; res@mpMinLonF = 90
    ; res@mpMaxLonF = 180
    ; res@mpMinLatF = -30
    ; res@mpMaxLatF = 40

    ;; Tropical East Pac
    ; res@mpMinLonF = 360-150
    ; res@mpMaxLonF = 360-30
    ; res@mpMinLatF = -20
    ; res@mpMaxLatF = 40

    ;;; South America
    ; res@mpMinLonF = 360-100
    ; res@mpMaxLonF = 360-10
    ; res@mpMinLatF = -40
    ; res@mpMaxLatF = 30

    ;;; Africa
    ; res@mpMinLonF = -30
    ; res@mpMaxLonF = 60
    ; res@mpMinLatF = -30
    ; res@mpMaxLatF = 30

    ; res@mpCenterLonF = avg( (/ res@mpMinLonF, res@mpMaxLonF /) )

    ; dlb = 0.3
    ; res@lbTopMarginF    = 0.05 + dlb
    ; res@lbBottomMarginF = 0.05 - dlb ;+ 0.1
    
    lres = setres_default()
    lres@xyLineThicknessF   = 1.
    lres@xyLineColor        = "black"
    lres@xyDashPattern      = 0

;===================================================================================
;===================================================================================
tfiles = ""
v = 0
c = 0
    
    ;------------------------------------------------------------
    ; Setup variables for data
    ;------------------------------------------------------------

    ; if any(ndtooned(case(:).eq.obs_case)) then

    ;     opt@num_file = 30
    ;     opt@f0       = 0 

    ;     ocase = ind(case.eq.obs_case)
    ;     ; if ismissing(ocase) then ocase = ind(case.eq."TRMM") end if
    ;     tX := LoadE3SM(case(c),var(v),opt)
    ;     otime = tX&time
    ;     num_o = dimsizes(otime)
    ;     dims := dimsizes(tX)
    ;     ; oX := new((/num_c,num_v,dims(0),dims(1),dims(2)/),float)
    ;     oX := new((/num_v,dims(0),dims(1),dims(2)/),float)

    ; end if

    ; if any(ndtooned(case(:).ne.obs_case)) then
    ;     opt@num_file = 30/5
    ;     opt@f0       = 0

    ;     mcase = ind(case.ne.obs_case)
    ;     tX := LoadE3SM(case(mcase(0)),var(v),opt)

    ;     time = tX&time
    ;     num_t = dimsizes(time)

    ;     dims := dimsizes(tX)
    ;     X := new((/num_c,num_v,dims(0),dims(1)/),float)
    ; end if

    ; if all(ndtooned(case(:).ne.obs_case)) then
    ;     num_o = num_t
    ; else
    ;     num_t = num_o
    ; end if

    X = True

    units := new((/num_v/),string)

    num_t := new(num_c,integer)
    
    lat_ptr = True
    lon_ptr = True
    cell_node_lat_ptr = True
    cell_node_lon_ptr = True

    ;------------------------------------------------------------
    ; load data
    ;------------------------------------------------------------

    if make_png then
        do v = 0,num_v-1
            do c = 0,num_c-1
                print("  "+case(c)+"")

                ;;; Limit number of files to speed up loading
                if case(c).eq."TRMM" then
                    opt@num_file = 30
                    opt@f0       = 0 
                end if
                if case(c).ne."GPM" .and. case(c).ne."TRMM"  then
                    opt@num_file = 30/5
                    opt@f0       = 0
                end if


                ;;; Load data
                iX = LoadE3SM(case(c),var(v),opt)

                if dimsizes(dimsizes(iX)).eq.3 then
                    klev = 58
                    print("WARNING: picking vertical level "+klev+" ")
                    tmp := iX(:,klev,:)
                    iX  := tmp
                end if


                ;;; Adjust units
                if c.eq.0 then units(v) = iX@units end if
                if var(v).eq."PRECT" .and. case(c).ne.obs_case then 
                    iX = iX*86400.*1e3 
                    units(v) = "mm/day"
                end if

                ;;; Load grid info
                if v.eq.0 then 
                    if any(case(c).eq.obs_case) then
                        wopt = True
                        wopt@src_lat = iX&lat
                        wopt@src_lon = iX&lon
                        wgts_file = wgts_root_dir+"wgts."+obs_case+"_to_ne30.nc"

                        if .not.fileexists(wgts_file) then 
                            print("  Generating new grid and wgts file : "+wgts_file)

                            if obs_case.eq."TRMM" then 
                                ; obs_dx = 0.25
                                src_grid = "0.25deg" 
                            end if

                            srcGridFile = wgts_root_dir+"grid."+obs_case+".nc"
                            dstGridFile = wgts_root_dir+"grid.ne30.nc"

                            sopt = True
                            ; sopt@LLCorner = (/-90,-180 + obs_dx/)
                            ; sopt@URCorner = (/ 90, 180 /)
                            sopt@ForceOverwrite = True
                            sopt@LLCorner = (/min(iX&lat),min(iX&lon)/)
                            sopt@URCorner = (/max(iX&lat),max(iX&lon)/)
                            latlon_to_SCRIP     (srcGridFile, src_grid, sopt)
                            

                            ropt                = True
                            ropt@InterpMethod   = "bilinear"   ; bilinear / patch / conserve
                            ropt@ForceOverwrite = True 
                            ropt@SrcRegional    = True
                            ropt@NoPETLog       = True
                            ropt@SrcESMF        = False
                            ropt@DstESMF        = True
                            ESMF_regrid_gen_weights( srcGridFile, dstGridFile, wgts_file, ropt)

                            ; src_ny = 180 / obs_dx + 1
                            ; src_nx = 360 / obs_dx
                            ; src_lat = fspan( sopt@LLCorner(0), sopt@URCorner(0), dst_ny)
                            ; src_lon = fspan( sopt@LLCorner(1), sopt@URCorner(1), dst_nx)

                            ; src_lat!0 = "lat"
                            ; src_lat&lat = src_lat

                            ; src_lon!0 = "lon"
                            ; src_lon&lon = src_lon

                            ; outfile = addfile(wgts_file,"w") 
                            ; outfile->src_lat = src_lat
                            ; outfile->src_lon = src_lon
                        end if

                        

                        ropt                = True
                        ropt@InterpMethod   = "bilinear"   ; patch / conserve / bilinear
                        ropt@NoPETLog       = True
                        ; ropt@DstGridType    = "rectilinear"
                        ropt@DstGridType    = "unstructured"
                        ropt@SrcRegional    = True
                        ; ropt@DstRegional    = True

                        tmp = iX
                        iX := ESMF_regrid_with_weights( tmp, wgts_file, ropt)


                        lat_ptr@$case(c)$ = iX@lat1d
                        lon_ptr@$case(c)$ = iX@lon1d
                        ifile = "~/E3SM/data_grid/ne30np4_scrip.nc"
                        infile = addfile(ifile,"r")
                        cell_node_lon_ptr@$case(c)$ = infile->grid_corner_lon
                        cell_node_lat_ptr@$case(c)$ = infile->grid_corner_lat

                    else
                        ; lat_ptr@$case(c)$ = iX@lat
                        ; lon_ptr@$case(c)$ = iX@lon
                        if isStrSubset(case(c),"ne120")     then grid = "ne120np4" end if
                        if isStrSubset(case(c),"ne120pg2")  then grid = "ne120pg2" end if
                        if isStrSubset(case(c),"ne30")      then grid = "ne30np4" end if
                        if isStrSubset(case(c),"ne30pg2")   then grid = "ne30pg2" end if

                        if isStrSubset(var(v),"DYN_") then grid = str_sub_str(grid,"pg2","np4") end if

                        ifile = "~/E3SM/data_grid/"+grid+"_scrip.nc"

                        ; if isStrSubset(case(c),"ne16")    then ifile = "~/Research/E3SM/data_grid/SCRIPgrid_ne16np4_nomask_c110512.nc" end if
                        ; if isStrSubset(case(c),"ne16np8") then ifile = "~/Research/E3SM/data_grid/ne16np8_scrip.nc" end if
                        ; if isStrSubset(case(c),"ne4")     then ifile = "~/Research/E3SM/data_grid/ne4np4-pentagons_c100308.nc" end if
                        ; if isStrSubset(case(c),"ne4pg1")  then ifile = "~/Research/E3SM/data_grid/ne4np1_scrip.nc" end if
                        ; if isStrSubset(case(c),"ne4pg2")  then ifile = "~/E3SM/data_grid/ne4pg2_scrip.nc" end if
                        ; if isStrSubset(case(c),"ne16pg2") then ifile = "~/E3SM/data_grid/ne16pg2_scrip.nc" end if
                        ; if isStrSubset(case(c),"ne30pg1") then ifile = "~/Research/E3SM/data_grid/ne30np1_scrip.nc" end if
                        

                        ; if isStrSubset(case(c),"ne0np4-aqua")     then ifile = "~/E3SM/data_grid/ne4x2np4b_scrip.nc" end if
                        ; if isStrSubset(case(c),"ne0np4-aqua.pg2") then ifile = "~/E3SM/data_grid/ne4x2pg2_scrip.nc" end if

                        infile = addfile(ifile,"r")
                        cell_node_lon_ptr@$case(c)$ = infile->grid_corner_lon
                        cell_node_lat_ptr@$case(c)$ = infile->grid_corner_lat

                        lat_ptr@$case(c)$ = infile->grid_center_lat
                        lon_ptr@$case(c)$ = infile->grid_center_lon

                    end if
                end if


                num_t(c) = dimsizes(iX&time)

                ;;; Assign data to pointer
                X@$(var(v)+"_"+case(c))$ = iX

                delete(iX)

            end do ; case
        end do ; var
    end if

    ;------------------------------------------------------------
    ; plot animation frames
    ;------------------------------------------------------------

    num_a = min( num_t )

    do t = 1,num_a-1
    ; do t = t1,num_a-1
    ; do t = nt/2,nt-1

        if num_c.eq.1 then
            tfig_file = fig_file+"."+case+"."+sprinti("%4.4i",t)
        else
            tfig_file = fig_file+"."+sprinti("%4.4i",t)
        end if

        tfiles = tfiles+"  "+tfig_file+"."+fig_type

        if .not.make_png then continue end if

        wks := gsn_open_wks(fig_type,tfig_file)
        if num_r.gt.1 then
            plot := new(num_c*num_r,graphic)
        else
            plot := new(num_c*num_v,graphic)
        end if

        do c = 0,num_c-1
        do v = 0,num_v-1
        do r = 0,num_r-1
            ; printline()
            ; print("var: "+var(v)+"")
            ;---------------------------------------
            ; Setup resources
            ;---------------------------------------
            if num_r.gt.1 then
                if panel_flip then ip = c*num_r+r else ip = r*num_c+c end if
            else
                if panel_flip then ip = c*num_v+v else ip = v*num_c+c end if
            end if
            
            tres := res
            tres@tiXAxisString   = "" ;units(v)
            tres@gsnLeftString   = casename(c)
            tres@gsnRightString  = var(v)

            ; if all(case(c).ne.obs_case) then
                tres@sfXArray        = lon_ptr@$case(c)$
                tres@sfYArray        = lat_ptr@$case(c)$

                tres@sfXCellBounds   = cell_node_lon_ptr@$case(c)$
                tres@sfYCellBounds   = cell_node_lat_ptr@$case(c)$
            ; end if

            if var(v).eq."PRECT" then tres@gsnRightString = "Precipitation"         end if
            if var(v).eq."TMQ"   then tres@gsnRightString = "Column Water Vapor"    end if

            if c.eq.num_c-1 then
                tres@lbLabelBarOn    = True
            else
                tres@lbLabelBarOn    = False
            end if

            res@cnFillPalette = "ncl_default"
            if any(var(v).eq.(/"CWV","TMQ","PRECT","CLDLIQ"/)) then 
                tres@cnFillPalette = "WhiteBlueGreenYellowRed"

                ; tres@cnFillPalette = "GMT_ocean" 
                ; tres@cnFillPalette = "MPL_cool" 
                ; tres@cnFillPalette = "cmocean_ice"    ; not available?
                ; tres@cnFillPalette = "cmocean_gray"   ; not available? 
                ; tres@cnFillPalette = "WhiteBlue" 
                ; tres@cnFillPalette = "MPL_gist_gray" 
                ; tres@cnFillPalette = "MPL_bone" 
                ; cmap = read_colormap_file("MPL_gist_gray")

                ; num_clev = 250
                ; cmap = new((/num_clev,4/),float)
                ; cmap(:,0) = fspan(0.,1.,num_clev)
                ; cmap(:,1) = fspan(0.,1.,num_clev)
                ; cmap(:,2) = fspan(0.,1.,num_clev)
                ; cmap(:,3) = 1.
                ; tres@cnFillPalette := cmap

                ; tres@cnLevels := fspan(1,60,num_clev)

            end if

            ; if var(v).eq."PS"    then tres@cnFillPalette = "NCV_bright" end if
            if var(v).eq."PS"    then tres@cnFillPalette = "NCV_banded" end if
            

            tres@lbBoxLinesOn = False

            tres@cnLevelSelectionMode = "ExplicitLevels"
            
            if var(v).eq."PRECT" then tres@cnLevels = ispan(2,300,2)/1e1         end if
            ; if var(v).eq."PRECT" then tres@cnLevels = ispan(1,10,1)/1e1         end if
            if var(v).eq."TMQ"   then tres@cnLevels = ispan(4,60,2)         end if

            if var(v).eq."PS"    then tres@cnLevels = ispan(600,1040,2)*1e2 end if

            if var(v).eq."CLOUD" then tres@cnLevels = ispan(1,9,1)/1e1      end if
            if var(v).eq."LHFLX" then tres@cnLevels = ispan(-200,400,50)    end if
            if var(v).eq."SHFLX" then tres@cnLevels = ispan(-100,200,20)    end if

            if var(v).eq."DYN_OMEGA" then tres@cnLevels = ispan(-10,10,1)/1e1/2. end if

            ;---------------------------------------
            ; Specify Region
            ;---------------------------------------
            if num_r.gt.1 then 
                
                if r.eq.0 then
                    ;;; Africa
                    tres@mpMinLonF = -20
                    tres@mpMaxLonF = 70
                    tres@mpMinLatF = -40
                    tres@mpMaxLatF = 30
                end if

                if r.eq.1 then
                    ;;; Tropical East Pac
                    tres@mpMinLonF = -180 +360
                    tres@mpMaxLonF = -90  +360
                    tres@mpMinLatF = -30
                    tres@mpMaxLatF = 40
                end if

                if r.eq.2 then
                    ;;; Tropical West Pac
                    tres@mpMinLonF = 90
                    tres@mpMaxLonF = 180
                    tres@mpMinLatF = -30
                    tres@mpMaxLatF = 40
                end if

                res@mpCenterLonF = avg( (/ res@mpMinLonF, res@mpMaxLonF /) )

            end if
            ;---------------------------------------
            ; Create plot
            ;---------------------------------------

            ; if any(case(c).eq.obs_case) then
            ;     pX := X@$(var(v)+"_"+case(c))$(t,:)
            ; else
            ;     pX := X@$(var(v)+"_"+case(c))$(t,:)
            ; end if


            pX := X@$(var(v)+"_"+case(c))$

            plot(ip) = gsn_csm_contour_map(wks,pX(t,:),tres)

            

            ; plot(ip) = gsn_csm_map(wks,res) 
            
            ;------------------------------------------------
            ; add zero line
            ;------------------------------------------------
            ; xx = (/-1,1/)*1e10
            ; overlay( plot(ip), gsn_csm_xy(wks,xx,xx*0,lres) )
            ; overlay( plot(ip), gsn_csm_xy(wks,xx*0,xx,lres) )
            ;---------------------------------------
            ;---------------------------------------
        end do
        end do
        end do

        pres = setres_panel()
        ; pres = setres_panel_labeled()
        pres@gsnPanelYWhiteSpacePercent = 5
        pres@gsnPanelBottom = 0.1
        ; pres@txString = season

        if num_r.gt.1 then
            layout = (/num_r,num_c/)
            if panel_flip then layout = (/num_c,num_r/) end if
        else
            layout = (/num_v,num_c/)
            if panel_flip then layout = (/num_c,num_v/) end if
        end if


        gsn_panel(wks,plot,layout,pres)
        
        tfig_file@add_space = False  ; disable printing extra line for cleaner terminal output
        
        trimPNG(tfig_file)

        ;------------------------------------------------------------
        ;------------------------------------------------------------

    end do

; end do
printline()
;===================================================================================
;===================================================================================

if make_gif .and. fig_type.eq."png" then

    ; 2028×733
    offset = 2028


    ; cmd = "convert -delay 10 -loop 0 -density 1000 "+tfiles+" "+gif_file
    ; cmd = "convert -delay 10 -loop 0 -size 2028×733 "+tfiles+" "+gif_file
    cmd = "convert -repage 0x0 -delay 10 -loop 0  "+tfiles+" "+gif_file
    ; cmd = "convert -delay 10 -loop 0 -origin  "+tfiles+" "+gif_file

    printline()
    print("")
    print(cmd)
    system(cmd)
    print("  "+gif_file)
    print("")

end if
;===================================================================================
;===================================================================================

end
