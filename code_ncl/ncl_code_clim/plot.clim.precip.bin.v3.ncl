; make a line plot of Yvar binned by Xvar
; v1 - only plot precip rate frequency distribution with logarithmic bins
; v2 - bin some quantity against logarithmic precip bins
; v3 - plot component of Matt Igel's Precip breakdown - based on code_clim/plot.clim.bin.v1.ncl
; v4 - same as v1 but make seperate distributions for land and ocean
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/NCL/custom_functions_E3SM.ncl"
begin

    ;;; CRM domain sensitivity tests
    ; case = "E3SM_SP1_CTL_ne30_"+(/"32x1_0.5","32x1_1","32x1_2"/)+"km_00"
    ; case = "E3SM_SP1_CTL_ne30_"+(/"16x1_2","32x1_1","64x1_0.5"/)+"km_00"
    ; case = "E3SM_SP1_CTL_ne30_"+(/"16x1_1","32x1_1","64x1_1"/)+"km_00"

    ; case = (/"E3SM_SP1_CTL_ne30_32x1_1km_00","E3SM_SP1_CTL_ne30_128x1_1km_00"/)
    case = (/"OBS","E3SM_ZM_CTL_ne30_00","E3SM_SP1_CTL_ne30_32x1_1km_00","E3SM_SP1_CTL_ne30_128x1_1km_00"/)
    clr = (/"black","gray","red","green"/)

    
    obs_case_X = "ERAi"
    obs_case_Y = "GPCP"


    Xvar = "CWV"
    ; Xvar = "PRECT"

    Yvar = "PRECT"
    ; Yvar = "COLQR"
    
    nyears = 1
    ;;; Comment out the months variable for all seasons
    months = ispan(1,1,1)
    
    panel_flip  = False       ; True = case x var

    fig_type = "png"
    fig_file = "~/Research/E3SM/figs_clim/clim.precip.bin.v3"
;===================================================================================
;===================================================================================
    case = E3SM_get_case(case)
    casename = E3SM_get_casename(case)

    casename = where(case.eq."E3SM_SP1_CTL_ne30_64x1_1km_90","CTRL - 64 rad columns" ,casename)
    casename = where(case.eq."E3SM_SP1_CTL_ne30_64x1_1km_91","1 rad column" ,casename)
    casename = where(case.eq."E3SM_SP1_CTL_ne30_64x1_1km_98","8 rad columns" ,casename)

    num_c = dimsizes(case)
    num_v = dimsizes(Yvar)

    opt  = True
    ; opt@debug    = True
    opt@daily_avg = True
    ; opt@use6h = True
    ; opt@year = ispan(1990,1990+nyears,1) - 1974
    ; opt@year = 1998 - 1997
    opt@year = ispan(0,nyears-1,1)
    ; opt@year = (/1,2/)

    opt@ERAIyr1 = 2000
    opt@GPCPyr1 = 2000
    opt@TRMMyr1 = 2000
    opt@GPMyr1  = 2017
    opt@ERAIyr2 = opt@ERAIyr1+nyears-1
    opt@TRMMyr2 = opt@TRMMyr1+nyears-1
    opt@GPCPyr2 = opt@GPCPyr1+nyears-1
    opt@GPMyr2  = opt@GPMyr1 +nyears-1

    ; opt@num_file = 6
    ; opt@f0       = (31+28+31)/5

    opt@lat1  = -30
    opt@lat2  =  30
    ; opt@lon1  =  0
    ; opt@lon2  = 360

;===================================================================================
; Define plot properties
;===================================================================================

    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(4,graphic)
        res = setres_xy()
        ; res@tmXBLabelFontHeightF        = 0.015
        ; res@tmYLLabelFontHeightF        = 0.015
        ; res@gsnLeftStringFontHeightF    = 0.015
        ; res@gsnCenterStringFontHeightF  = 0.015
        ; res@gsnRightStringFontHeightF   = 0.015
        
        lres = setres_default()
        lres@xyLineThicknessF   = 1.
        lres@xyLineColor        = "black"
        lres@xyDashPattern      = 0
;===================================================================================
;===================================================================================
orig_E3SM_data_dir = E3SM_data_dir
do v = 0,num_v-1

    do c = 0,num_c-1
        printline()
        print(case(c)+":")
        ;---------------------------------------
        ;---------------------------------------
        tcase_x = case(c)
        tcase_y = case(c)

        if case(c).eq."OBS" then
            tcase_x = obs_case_X
            tcase_y = obs_case_Y
        end if

        derived_X = False
        derived_Y = False
        
        ; if Yvar(v).eq."LWCRE" then
        ;     iY := LoadE3SM(tcase_y, "FLNT", opt)
        ;     derived_Y = True
        ; end if

        if .not.derived_X then iX := LoadE3SM(tcase_x, Xvar(0), opt) end if
        if .not.derived_Y then iY := LoadE3SM(tcase_y, Yvar(v), opt) end if

        dims := dimsizes(iX)
        ndim := dimsizes(dims)

        ;---------------------------------------
        ; Adjust units
        ;---------------------------------------
        if case(c).ne."OBS".and..not.opt@monthly  then 
            if Xvar(0).eq."PRECT" then 
                iX = (/ iX * 86400. * 1e3 /) 
                iX@units = "mm/day"
            end if
            if Yvar(v).eq."PRECT" then 
                iY = (/ iY * 86400. * 1e3 /) 
                iY@units = "mm/day"
            end if
        end if

        if any(Xvar(v).eq.(/"TMQ","CWV"/)) then 
            ; iY = (/ iY * 86400. * 1e3 /) 
            iX@units = "mm"
        end if

        if any(Xvar(v).eq.(/"COLQR"/)) then 
            iX@units = "K/day"
        end if

        Xunits = iX@units
        Yunits = iY@units

printMAM(iX)
printMAM(iY)

        ;---------------------------------------
        ; interpolate obs data to model grid
        ;---------------------------------------
        if case(c).eq."OBS" then
            tcase = ""
            do cc = 0,num_c-1
                if case(cc).ne."OBS" then tcase = case(cc) end if
            end do
            if tcase.ne."" then  

                mlat = LoadE3SMlat_remap(tcase,opt)
                mlon = LoadE3SMlon_remap(tcase,opt)

                ;;; need to flip for interpolation so we don't get white line at 180
                mlon := flip_lon(mlon)
                mlon = (/mlon&lon/)

                tmp = linint2_Wrap(iX&lon,iX&lat,iX,True,mlon,mlat,0)
                iX := tmp
                delete(tmp)

                tmp = linint2_Wrap(iY&lon,iY&lat,iY,True,mlon,mlat,0)
                iY := tmp
                delete(tmp)

            else ; make sure obs data grid matches

                olat = iY&lat
                olon = iY&lon

                tmp = linint2_Wrap(iX&lon,iX&lat,iX,True,olon,olat,0)
                iX := tmp
                delete(tmp)

            end if
        end if
        ;---------------------------------------
        ; Isolate season
        ;---------------------------------------
        if isvar("months") then
            mn := iX@mn
            condition := mn.eq.0
            do m = 0,dimsizes(months)-1 condition = condition.or.(mn.eq.months(m)) end do
            vals := where(condition,True,False)
            
            tX := iX(ind(vals),:,:)
            tY := iY(ind(vals),:,:)

            smn = (/"J","F","M","A","M","J","J","A","S","O","N","D"/)
            str = ""
            do m = 0,dimsizes(months)-1 str = str+smn(months(m)-1) end do
            season = str
        else
            tY := iY
            tX := iX
            season = ""
        end if

        ;---------------------------------------
        ; Bin the data
        ;---------------------------------------
        bins = True
        bins@verbose = True
        
        if Xvar(v).eq."CWV"
            bins@bin_min = 2
            bins@bin_max = 70
            bins@bin_spc =  2
        end if

        tmp = bin_YbyX( tY, tX, bins )

        if c.eq.0 then
            xbin = tmp&bin
            num_bin = dimsizes(xbin)
            binval  := new((/num_c,num_bin/),float)
            binpct  := new((/num_c,num_bin/),float)
            binval!0 = "case"
            binval!1 = "bin"
            binval&bin = xbin
            copy_VarCoords(binval,binpct)
        end if

        binval(c,:) = (/ tmp     /)
        binpct(c,:) = (/ tmp@pct /)

        delete([/tmp/])

        ; binval@lon_name = "Binned Average"
        ; printMAM(binval)
        ;---------------------------------------
        ;---------------------------------------
    end do

    ;---------------------------------------
    ; print stats to check for unit issues
    ;---------------------------------------
    ; printMAM(aX)
    ; print("")
    ; print("max of all data : "+max(tX))
    ; delete([/tX/])
    
    ;---------------------------------------
    ; Set strings for plot
    ;---------------------------------------
    tres = res

    ; if .not.isatt(aX,"long_name") then aX@long_name = var(v) end if
    ; if ndim.eq.4.and.isatt(opt,"lev") then
    ;     if opt@lev.eq.-2 then 
    ;         aX@long_name = "(k="+k_lev+") "+aX@long_name 
    ;     else
    ;         aX@long_name = opt@lev+"mb "+aX@long_name 
    ;     end if
    ; end if

    Xname = Xvar(0)
    Yname = Yvar(v)
    
    if Yvar(v).eq."LWCRE" then Yname = "LW Cloud Radiative Effect" end if
    if Yvar(v).eq."FLNT"  then Yname = "TOA Net LW Flux" end if
    if Yvar(v).eq."FSNT"  then Yname = "TOA Net SW Flux" end if

    tres@gsnCenterString = Yname+" vs. "+Xname

    ;---------------------------------------
    ; Create plot
    ;---------------------------------------
    
    if isvar("clr") then
        tres@xyDashPattern = 0
        tres@xyLineColors  = clr
    end if


    ;;; Precip vs CWV
    tres@tiXAxisString = "["+ Xunits +"]"
    tres@tiYAxisString = "["+ Yunits +"]"

    plot(0) = gsn_csm_xy(wks,xbin,binval,tres)

    
    ;;; CWV distribution
    tres@gsnCenterString = Xvar+" Distribution"
    tres@tiYAxisString = "Frequency [%]"

    plot(1) = gsn_csm_xy(wks,xbin,binpct,tres) 


    ;;; Chance of precip
    

    ;---------------------------------------
    ;---------------------------------------
end do
printline()
;===================================================================================
;===================================================================================
        pres = setres_panel()
        ; pres = setres_panel_labeled()
        pres@gsnPanelYWhiteSpacePercent = 5
        pres@gsnPanelBottom = 0.1
        ;pres@txString = season

    ; if (num_v*num_c).eq.4 then layout = (/2,2/) end if
    ;if (num_v*num_c).eq.9 then layout = (/3,3/) end if

    num_p = dimsizes(plot)
    layout = (/num_p,1/)
    if panel_flip then layout = (/1,num_p/) end if

    gsn_panel(wks,pplot,layout,pres)

    trimPNG(fig_file)

;===================================================================================
;===================================================================================

end

