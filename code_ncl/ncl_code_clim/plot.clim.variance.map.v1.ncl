; plot a map of a global mean quantity
; v1 - 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/NCL/custom_functions_E3SM.ncl"
begin

    ; case = (/"E3SM_SP1_CTL_ne30_64x1_1km_s1_46"/)
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_s1_"+(/41,42/)

    ; case = "E3SM_SP1_CTL_ne30_64x1_1km_"+(/91,98,90/)

    ; case = (/"E3SM_SP1_CTL_ne30_32x1_1km_00","E3SM_ZM_CTL_ne30_00","TRMM"/)
    ; case = (/"E3SM_SP1_CTL_ne30_32x1_1km_00","E3SM_ZM_CTL_ne30_00"/)
    ; case = "E3SM_SP1_CTL_ne30_"+(/16,32/)+"x1_1km_00"
    ; case = "E3SM_SP1_CTL_ne30_32x1_"+(/"0.5","1","2"/)+"km_00"
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_"+(/"00","s1_00"/)

    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_00"
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_01"
    case = "E3SM_SP1_CTL_ne30_32x1_1km_80"

    
    ;;; CRM domain sensitivity tests
    ; case = "E3SM_SP1_CTL_ne30_"+(/"32x1_0.5","32x1_1","32x1_2"/)+"km_00"
    ; case = "E3SM_SP1_CTL_ne30_"+(/"16x1_2","32x1_1","64x1_0.5"/)+"km_00"
    ; case = "E3SM_SP1_CTL_ne30_"+(/"16x1_1","32x1_1","64x1_1"/)+"km_00"

    ;;; SP_CRM_REINIT test
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_"+(/"00","s1_00"/)

    ;;; ESMT test
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_"+(/"00","s2_65"/)

    ;;; SP_FLUX_BYPASS test
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_"+"01" ;(/"00","01"/)

    ;;; GCM time step sensitivity
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_"+(/"00","80"/)



    ; case = (/"E3SM_ZM_AMIP_ne16_00","E3SM_ZM_AMIP_ne16_13"/)
    ; case = (/"E3SM_ZM_AMIP_ne30_00","E3SM_ZM_AMIP_ne30_13"/)
    ; case =        (/"E3SM_ZM_AMIP_ne16_00","E3SM_ZM_AMIP_ne16_13","E3SM_ZM_AMIP_ne30_00","E3SM_ZM_AMIP_ne30_13"/)
    ; case = (/"ERAi","E3SM_ZM_AMIP_ne16_00","E3SM_ZM_AMIP_ne16_13","E3SM_ZM_AMIP_ne30_00","E3SM_ZM_AMIP_ne30_13"/)


    fig_type = "png"
    fig_file = "~/Research/E3SM/figs_clim/clim.variance.map.v1"

    ; var = "U"
    var = "PRECT"
    ; var = "LHFLX"
    ; var = "TMQ"
    ; var = (/"LHFLX","SHFLX"/)
    ; var = (/"TAUX","TAUY"/)
    ; var = (/"U","T"/)
    ; var = (/"TMQ"/)
    ; var = (/"CLOUD"/)
    
    ; var = (/"CLDLOW","CLDMED","CLDHGH"/)
    ; var = (/"CLDTOT","CLDLOW","CLDHGH"/)
    ; var = (/"CLDHGH","CLDLOW"/)
    ; var = "CLDTOT"
    ;var = (/"TAUX","TAUY"/)
    ; var = (/"TAUX"/)
    ; var = (/"SWCF","LWCF"/)
    ; var = (/"FLNT","LWCF","SWCF"/)
    ; var = "NET_TOA_RAD"

    

    nyears = 1

    ;;; Comment out the months variable for all seasons
    ; months = (/7,8,9,10,11,12/)
    ; months = (/7,8,9,10,11,12/)
    ; months = (/1,2,3,4,5/)
    ; months = (/1,2,3/)
    ; months = (/3/)
    ; months = (/4,5,6/)
    ; months = (/7,8,9/)
    ; months = (/10,11,12/)
    
    
    add_vec     = False
    add_box     = False
    wind_lev    = 975

    plot_diff   = False

    panel_flip  = False       ; True = case x var
;===================================================================================
;===================================================================================
    case = E3SM_get_case(case)

    num_c = dimsizes(case)
    num_v = dimsizes(var)


    E3SM_trunclen = nyears

    opt  = True
    ; opt@debug    = True
    ; opt@monthly  = True
    opt@daily_avg = True
    ; opt@use6h = True
    ; opt@truncate = True
    ; opt@useremap = True
    ; opt@ERAIyr1  = 2000
    ; opt@ERAIyr2 = opt@ERAIyr1 + E3SM_trunclen
    opt@ERAIyr1  = 1997
    opt@ERAIyr2  = 1997+nyears
    opt@TRMMyr1  = 1999
    opt@TRMMyr2  = 1999+nyears
    ; opt@year = ispan(1990,1990+nyears,1) - 1974
    ; opt@year = 1998 - 1997
    opt@year = ispan(0,nyears-1,1)
    ; opt@year = (/1,2/)

; opt@debug    = True
; opt@monthly  = False
    
    ; opt@lev = 200;600;850
    ; opt@lev = 850
    ; opt@lev = -2
    
    opt@lat1  = -40
    opt@lat2  =  40
    ; opt@lon1  =  0
    ; opt@lon2  = 360

    ; Zoom into Africa
    ; opt@lat1  = -10;-30
    ; opt@lat2  =  40
    ; opt@lon1  = -50;-80
    ; opt@lon2  =  40; 60
    

;===================================================================================
; Define plot properties
;===================================================================================

    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(num_c*num_v,graphic)
        res = setres_contour_fill_smalltxt()
        ;res = setres_contour()
        ; res@lbLabelBarOn    = False
        ; res@cnLineLabelsOn  = False
        ; res@cnInfoLabelOn   = False
        ; res@gsnAddCyclic    = False
        res@mpCenterLonF    = 0.
        res@tmXBLabelFontHeightF        = 0.015
        res@tmYLLabelFontHeightF        = 0.015
        res@gsnLeftStringFontHeightF    = 0.015
        res@gsnCenterStringFontHeightF  = 0.015
        res@gsnRightStringFontHeightF   = 0.015
        res@mpLimitMode     = "LatLon" 
        ;res@mpProjection    = "Aitoff"
        if isatt(opt,"lat1") then res@mpMinLatF = opt@lat1 end if
        if isatt(opt,"lat2") then res@mpMaxLatF = opt@lat2 end if
        ; if isatt(opt,"lon1") then res@mpMinLonF = opt@lon1 end if
        ; if isatt(opt,"lon2") then res@mpMaxLonF = opt@lon2 end if
        res@mpCenterLonF    = 180
        ; res@mpMinLonF     = 0
        ; res@mpMaxLonF     = 360

        dlb = 0.3
        res@lbTopMarginF    = 0.05 + dlb
        res@lbBottomMarginF = 0.05 - dlb ;+ 0.1
        

        vres = setres_vector()
        vres@vcRefAnnoOn = False

        lres = setres_default()
        lres@xyLineThicknessF   = 1.
        lres@xyLineColor        = "black"
        lres@xyDashPattern      = 0
;===================================================================================
;===================================================================================
orig_E3SM_data_dir = E3SM_data_dir
do v = 0,num_v-1
do c = 0,num_c-1

    printline()
    print(case(c)+":")

    E3SM_data_dir = orig_E3SM_data_dir

    ; E3SM_data_sub = "/run"
    if case(c).eq."20170731.F20TR_tauk5x_2400.ne30_ne30.edison" then    E3SM_data_dir = "/global/cscratch1/sd/whannah/E3SM_simulations"  end if
    if case(c).eq."20170731.F20TR_ck10.ne30_ne30.edison"        then    E3SM_data_dir = "/global/cscratch1/sd/golaz/E3SM_simulations"   end if
    if case(c).eq."20170720.F20TR_ke5.ne30_ne30.cori-haswell"   then    E3SM_data_dir = "/global/cscratch1/sd/golaz/E3SM_simulations"   end if
    if case(c).eq."20170731.F20TR_ck10_ke.ne30_ne30.edison"     then    E3SM_data_dir = "/global/cscratch1/sd/golaz/E3SM_simulations"   end if
    if case(c).eq."20170707.POPv1atm_gust.A_WCYCL1850.ne30_g16.edison"              then E3SM_data_dir = "/global/cscratch1/sd/golaz/E3SM_simulations/"  end if
    if case(c).eq."20170612.r5x3.ne30_ne30.edison"                                  then E3SM_data_dir = "/global/cscratch1/sd/petercal/E3SM_simulations/X_r5x3"  end if
    if case(c).eq."20170612.r5x3-c0ocn0.01_c0lnd0.0025_c14-1.75.ne30_ne30.edison"   then E3SM_data_dir = "/global/cscratch1/sd/petercal/E3SM_simulations/X_r5x3"  end if
    if case(c).eq."20161118.beta0.F20TRCOSP.ne30_ne30.edison" then E3SM_data_dir = "/global/cscratch1/sd/tang30/E3SM_simulations" end if

    if case(c).eq."E3SM_ZM_CTL_ne30_00" then E3SM_data_dir = "/lustre/atlas/proj-shared/cli115/hannah6/E3SM/" end if
        
    ;---------------------------------------
    ;---------------------------------------
    derived = False
    if var(v).eq."NET_TOA_RAD" then
        iLW := LoadE3SM(case(c),"FLNT",opt)
        iSW := LoadE3SM(case(c),"FSNT",opt)
        iX := (/iSW - iLW/)
        copy_VarCoords(iSW,iX)
        copy_VarAtts(iSW,iX)
        iX@long_name = "Net Radiative Flux at TOM"
        delete([/iLW,iSW/])
        derived = True
    end if

    if .not.derived then iX := LoadE3SM(case(c),var(v),opt) end if

    if any(iX&lon.lt.0.) then iX := flip_lon(iX) end if

    dims := dimsizes(iX)
    ndim := dimsizes(dims)

    ;---------------------------------------
    ; Adjust units
    ;---------------------------------------
    if any(var(v).eq.(/"PRECT"/)).and..not.opt@monthly then 
       if all(case(c).ne.(/"ERAi","TRMM"/)) then  iX = (/ iX * 86400. * 1e3 /) end if
       iX@units = "mm/day"
    end if
    if case(c).eq."ERAi" .and. any(var(v).eq.(/"TAUX","TAUY"/)) then 
        iX = (/iX*-1./)
    end if
    ;---------------------------------------
    ;---------------------------------------
    if ndim.eq.4 then 
        if opt@lev.eq.-2 then
            k_lev = 0
            tX = iX(:,dims(1)-1-k_lev,:,:)
            iX := tX
            delete(tX)
        else
            lev := iX&lev*1e2
            lev!0 = "lev"
            lev&lev = lev
            lev@units = "Pa"
            dp = calc_dP(lev,LoadE3SM(case(c),"PS",opt))
            tX = dim_sum_n(iX*dp/g,1)
            copy_VarCoords(iX(:,0,:,:),tX)
            copy_VarAtts  (iX(:,0,:,:),tX)
            delete(iX)
            iX = tX
            delete([/tX,dp/])
        end if
    end if 

    if add_vec then 
        topt = opt
        topt@lev = wind_lev
        iU := LoadE3SM(case(c),"U",topt)
        iV := LoadE3SM(case(c),"V",topt)
    end if

    ;---------------------------------------
    ; interpolate TRMM to model grid
    ;---------------------------------------
    if case(c).eq."TRMM" then
        do cc = 0,num_c-1
            if case(cc).ne."TRMM" then tcase = case(cc) end if
        end do
        mlat = LoadE3SMlat_remap(tcase,opt)
        mlon = LoadE3SMlon_remap(tcase,opt)

        ; need to flip for interpolation so we don't get white line at 180
        ; iX := flip_lon(iX)
        mlon := flip_lon(mlon)
        mlon = (/mlon&lon/)

        tmp = linint2_Wrap(iX&lon,iX&lat,iX,True,mlon,mlat,0)
        iX := tmp
        delete(tmp)
        ; iX := flip_lon(iX)
    end if
    ;---------------------------------------
    ; Isolate season
    ;---------------------------------------
    if isvar("months") then

        mn := iX@mn
        condition := mn.eq.0
        do m = 0,dimsizes(months)-1 condition = condition.or.(mn.eq.months(m)) end do
        vals := where(condition,True,False)
        tX := iX(ind(vals),:,:)
        if add_vec then
            tU := iU(ind(vals),:,:)
            tV := iV(ind(vals),:,:)
        end if
        ;if ndim.eq.3 then tX := iX(ind(vals),:,:) end if
        ;if ndim.eq.4 then tX := iX(ind(vals),:,:) end if

        smn = (/"J","F","M","A","M","J","J","A","S","O","N","D"/)
        str = ""
        do m = 0,dimsizes(months)-1 str = str+smn(months(m)-1) end do
        season = str
    else
        tX := iX
        ; season = "All Seasons"
        season = ""
        if add_vec then
            tU := iU
            tV := iV
        end if
    end if

    ;---------------------------------------
    ; Average in time
    ;---------------------------------------
    tdims := dimsizes(tX)
    tndim := dimsizes(tdims)
    if tndim.gt.2 then
        aX := dim_variance_n_Wrap(tX,0)
    else
        aX := tX
    end if
    aX&lat@units = "degrees north"
    aX&lon@units = "degrees east"

    if any(var(v).eq.(/"ZMMTU","ZMMTV","U_residual","V_residual"/)) then aX = (/aX*86400./) end if

    delete([/iX/])
    
    if add_vec then
        aU := dim_avg_n_Wrap(tU,0)
        aV := dim_avg_n_Wrap(tV,0)
        delete([/iU,iV,tU,tV/])
    end if
    ;---------------------------------------
    ; print stats to check for unit issues
    ;---------------------------------------
    printMAM(aX)
    print("")
    print("max of all data : "+max(tX))

    delete([/tX/])
    ;---------------------------------------
    ; print global mean
    ;---------------------------------------
    if True then
        lat := aX&lat
        lon := aX&lon
        nlat = dimsizes(lat)
        nlon = dimsizes(lon)

        re   = 6.37122e06
        rad  = 4.0 * atan(1.0) / 180.
        con  := re * rad                 
        clat := cos(lat * rad)           ; cosine of latitude
        dlon := (lon(2) - lon(1))        ; assume dlon is constant
        dlat := (lat(2) - lat(1))        ; assume dlat is constant
        dx   := con * dlon * clat        ; dx at each latitude
        dy   := con * dlat               ; dy is constant
        dydx := dy * dx  
        wgt  := new((/nlat,nlon/), float)
        wgt  = conform(wgt,tofloat(dydx), 0)

        avgopt := 0
        gX := wgt_areaave2(aX,wgt,avgopt)

        print("")
        print("Global mean     : "+gX)

    end if
    print("")
    ;---------------------------------------
    ; Assign contour levels
    ;---------------------------------------
    if isatt(res,"cnLevelSelectionMode") then delete(res@cnLevelSelectionMode) end if
    if isatt(res,"cnLevels") then delete(res@cnLevels) end if
    ;res@cnLevelSelectionMode = "ExplicitLevels"
    ;if var(v).eq."OMEGA" then cnLevels = ispan(-25,25,5)/1e2 end if

    if any(var(v).eq.(/"ZMMTU","ZMMTV","U_residual","V_residual"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        res@cnLevels = ispan(-100,100,5)/1e1
    end if

    if any(var(v).eq.(/"PRECT"/)) then 
        ; res@cnLevelSelectionMode = "ExplicitLevels"
        ; res@cnLevels = ispan(2,18,2)
        if opt@monthly then
            ; res@cnLevels = ispan(1,18,1)
        else
            res@cnLevelSelectionMode = "ExplicitLevels"
            res@cnLevels = ispan(100,1000,100)*2
        end if
    end if

    if any(var(v).eq.(/"TS"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels = ispan(280,312,2)
        res@cnLevels = ispan(273,314,2)
    end if

    if any(var(v).eq.(/"TMQ"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels = ispan(280,312,2)
        res@cnLevels = ispan(4,60,4)
    end if

    if any(var(v).eq.(/"PSL"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels = ispan(280,312,2)
        res@cnLevels = ispan(1009,1024,1)*1e2
    end if

    if any(var(v).eq.(/"LHFLX"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        res@cnLevels = ispan(5,205,10)
    end if

    if any(var(v).eq.(/"SWCF"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ; res@cnLevels = ispan(-160,40,10)
        res@cnLevels = ispan(-160,40,20)
    end if

    if any(var(v).eq.(/"LWCF"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ; res@cnLevels = ispan(-40,110,10)
        res@cnLevels = ispan(-40,110,20)
    end if

    if any(var(v).eq.(/"TAUX","TAUY"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        res@cnLevels = ispan(-14,14,2)/1e2
    end if

    if any(var(v).eq.(/"UdQdxvi","VdQdyvi"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        res@cnLevels = ispan(-300,300,30)
    end if

    if .not.isatt(res,"cnLevelSelectionMode") then 
    if c.eq.0  then 
        mnmxint = nice_mnmxintvl( min(aX), max(aX), 21, False)
        res@cnLevelSelectionMode = "ManualLevels"
        res@cnMinLevelValF       = mnmxint(0)
        res@cnMaxLevelValF       = mnmxint(1)
        res@cnLevelSpacingF      = mnmxint(2)
        if any(var(v).eq.(/"U","V","OMEGA","TOARAD"/)) then symMinMaxPlt(aX,31,False,res) end if
    end if
    end if
    ;---------------------------------------
    ; assign color map
    ;---------------------------------------
    res@cnFillPalette = "ncl_default"
    if any(var(v).eq.(/"CWV","PRECT","CLDLIQ"/)) then 
        res@cnFillPalette = "WhiteBlueGreenYellowRed"
    end if
    ;---------------------------------------
    ; Take difference from first case
    ;---------------------------------------
    if plot_diff then
        abs_diff_vars = (/"TAUX","TAUY"/)
        if c.eq.0 then 
            Xo := aX 
            if add_vec then
                Uo := aU
                Vo := aV
            end if
        else
            tdX := linint2_Wrap(Xo&lon,Xo&lat,Xo,False,aX&lon,aX&lat,0)
            if any(var(v).eq.abs_diff_vars) then
                aX = (/ abs(aX) - abs(tdX) /)
            else
                aX = (/ aX - tdX /)
            end if
            if add_vec then
                aU = (/ aU - linint2_Wrap(Uo&lon,Uo&lat,Uo,False,aU&lon,aU&lat,0) /)
                aV = (/ aV - linint2_Wrap(Vo&lon,Vo&lat,Vo,False,aV&lon,aV&lat,0) /)
            end if
        end if
        if c.gt.0 then 
            res@cnFillPalette = "BlWhRe" 
            if c.eq.1 then 
                dres = res
                symMinMaxPlt(aX,41,False,dres) 
            end if
            ; if any(var(v).eq.abs_diff_vars) then
            ;     aX = abs(aX)
            ; end if
        end if
    end if
    ;---------------------------------------
    ; Set strings at top of plots
    ;---------------------------------------
    if .not.isatt(aX,"long_name") then aX@long_name = var(v) end if
    if ndim.eq.4.and.isatt(opt,"lev") then
        if opt@lev.eq.-2 then 
            aX@long_name = "(k="+k_lev+") "+aX@long_name 
        else
            aX@long_name = opt@lev+"mb "+aX@long_name 
        end if
    end if
    if isatt(aX,"units") then sunits = aX@units else sunits = "" end if
    res@gsnLeftString   = casename(c)
    res@gsnRightString  = aX@long_name 
    res@gsnCenterString = season
    ;res@gsnCenterString = "Avg: "+global_wgt_avg(aX) +" "+sunits
    if var(v).eq."PRECT" then res@gsnRightString = "Precipitation" end if
    if var(v).eq."TMQ"   then res@gsnRightString = "CWV" end if
    if var(v).eq."PRECT".and.case(c).eq."ERAi" then res@gsnLeftString = "TRMM" end if
    if plot_diff.and.c.gt.0 then 
        if any(var(v).eq.abs_diff_vars) then
            dres@gsnLeftString = res@gsnLeftString+" (absolute difference)"
        else
            dres@gsnLeftString = res@gsnLeftString+" (difference)" 
        end if
    end if
    ;---------------------------------------
    ; Create plot
    ;---------------------------------------
    i = v*num_c+c
    ; res@lbLabelBarOn = False
    if panel_flip then 
        i = c*num_v+v 
        ; if c.eq.(num_c-1) then res@lbLabelBarOn = True end if
    else
        ; if v.eq.(num_v-1) then res@lbLabelBarOn = True end if
    end if

    if plot_diff then
        res@lbLabelBarOn = True
    end if
    
    tres = res
    ; if case(c).eq."TRMM" then tres@gsnAddCyclic = True end if
    if plot_diff.and.c.gt.0 then tres = dres end if

    plot(i) = gsn_csm_contour_map(wks,aX,tres)

    ; Add line at equator
    overlay( plot(i) , gsn_csm_xy(wks,aX&lon,0.*aX&lon,lres) ) 
    

    if add_vec then
        overlay( plot(i) , gsn_csm_vector(wks,aU,aV,vres) ) 
    end if

    if isStrSubset(case(c),"NUNA") then 
        topt = opt
        topt@monthly = True
        frac := LoadE3SM(case(c),"LANDFRAC",opt)
            cres = setres_contour()
            cres@cnInfoLabelOn      = False
            cres@cnLineLabelsOn     = False
            cres@cnLevelSelectionMode = "ExplicitLevels"
            cres@cnLevels = (/0.5/)
            cres@gsnLeftString  = ""
            cres@gsnRightString = ""
        overlay( plot(i) , gsn_csm_contour(wks,frac(0,:,:),cres) )
        delete(frac)
    end if
    ;---------------------------------------
    ;---------------------------------------
    mopt = True
    mopt@blat1  =   0
    mopt@blat2  =  30
    mopt@blon1  = -20
    mopt@blon2  =  10
    mopt@lineclr = "red"
    mopt@linethk = 4.
    if add_box then
        plot_add_box(wks,plot(i),mopt)
    end if
    ;---------------------------------------
    ;---------------------------------------
end do
end do
printline()
;===================================================================================
;===================================================================================
        pres = setres_panel()
        ; pres = setres_panel_labeled()
        pres@gsnPanelYWhiteSpacePercent = 5
        pres@gsnPanelBottom = 0.1
        ;pres@txString = season

    layout = (/num_v,num_c/)
    if panel_flip then layout = (/num_c,num_v/) end if

    ; if (num_v*num_c).eq.4 then layout = (/2,2/) end if
    ;if (num_v*num_c).eq.9 then layout = (/3,3/) end if

    gsn_panel(wks,plot,layout,pres)

    trimPNG(fig_file)

;===================================================================================
;===================================================================================

end

