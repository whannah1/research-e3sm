; calculate column integrated radiative heating (COLQR) and write to file
; separate file for each year (see also code_data/mk.var.fv.daily.v4.ncl)
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/NCL/custom_functions_E3SM.ncl"
begin
    
    ;;; Radiative column test
    case = "E3SM_SP1_CTL_ne30_64x1_1km_"+(/91,98,90/)
    
    var = "COLQR"
    
    debug     = False
    overwrite = True

    ; nyears = 1

    yr1 = 0
    yr2 = 0

;==================================================================================
;==================================================================================
    odir = E3SM_data_dir+case+"/data/"

    num_c = dimsizes(case)
    num_v = dimsizes(var)

    opt  = True
    opt@debug  = debug
    opt@useraw = True
    opt@lat1   = -30
    opt@lat2   =  30
    ;opt@lon1   = -60
    ;opt@lon2   =  60
    opt@lev    = -1
    ; opt@year = ispan(0,nyears-1,1)

    ;;; Filter Setup
    ; fca = 1./(10.*4)   ; longer period
    ; fcb = 1./( 2.*4)   ; shorter period
    ; opt = False       

    ; nwgt  = 15*4+1
    ; sigma = 1.0
    ; wgt   = filwgts_lanczos (nwgt,2,fca,fcb,sigma)
;==================================================================================
;==================================================================================
do c = 0,num_c-1
    printline()
    print("case: "+case(c))
    if .not.fileexists(odir(c)) then system("mkdir "+odir(c)) end if
    year = E3SM_get_year(case(c))
    ;------------------------------------------------------------------
    ;------------------------------------------------------------------
    ;do y = year(0),year(1)
    do y = yr1,yr2
        opt@year = y
        syear = sprinti( "%0.4i" , toint( year(0)+y ) )
        do v = 0,num_v-1 
            ;--------------------------------------
            ;--------------------------------------
            ;ofile = odir(c) + case(c) +"."+ var(v) +"."+y+".nc"
            ofile = odir(c) + case(c) +"."+ var(v) +"."+syear+".nc"
            if fileexists(ofile) then
                if .not.overwrite then continue end if
                if      overwrite then system("rm "+ofile) end if
            end if
            print("    var: "+strpad(var(v),10)+"      "+ofile)
            ;--------------------------------------
            ; Do Calculation
            ;--------------------------------------
            QR := LoadE3SM(case(c),"QR",opt)
            Ps := LoadE3SM(case(c),"PS",opt)

            lev = QR&lev

            P = lev*100.
            P!0 = "lev"
            P&lev = lev
            P@units = "Pa"
            dP = calc_dP(P,Ps)

            COLQR = dim_sum_n(QR*dP/g*cpd,1)

            COLQR@long_name = "Column Integrated Radiative Heating"

            COLQR!0 = "time"
            COLQR!1 = "lat"
            COLQR!2 = "lon"            
            COLQR&time = QR&time
            COLQR&lat = QR&lat
            COLQR&lon = QR&lon

            COLQR@units = "W m-2"

            delete([/Ps,P,dP/])
            ;--------------------------------------
            ; Write to output
            ;--------------------------------------
            outfile = addfile(ofile,"c")
            outfile->$var(v)$ = COLQR
            outfile@info = "Column Integrated Total Radiative Heating (LW+SW)"

            delete([/COLQR/])
            ;--------------------------------------
            ;--------------------------------------
        end do
    end do
    ;------------------------------------------------------------------
    ;------------------------------------------------------------------
end do
;==================================================================================
;==================================================================================
printline()
end
