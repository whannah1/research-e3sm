load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/NCL/custom_functions_E3SM.ncl"
begin

    ;;; use scratch data
    E3SM_data_sub = "/run"
    E3SM_data_dir = "/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/"
    ; E3SM_data_dir = "/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/"

    E3SM_ignore_h2 = True

    ; case = "TRMM"
    ; case = (/"E3SM_ZM_CTL_ne30_00","E3SM_SP1_CTL_ne30_32x1_1km_00"/)
    ; case = "E3SM_ZM_CTL_ne30_00"
    ; case = "E3SM_SP1_CTL_ne30_32x1_1km_00"

    ; case = (/ \
    ;         "E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-cbe53b" \
    ;         ,"E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b" \
    ;         ; ,"E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-cbe53b" \
    ;         ; ,"E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-cbe53b" \
    ;         ; ,"E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-cbe53b" \
    ;         ; ,"E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b" \
    ; /)

    ; case = (/ \
    ;         "E3SM.CVT-TEST.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_4.00" \
    ;        ; ,"E3SM.CVT-TEST.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_4.SCVT.NTRC_1.00" \
    ;        ,"E3SM.CVT-TEST.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_4.BCVT.00" \
    ;        ; ,"E3SM.CVT-TEST.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.CRMDT_5.RADNX_4.FCVT_08.00" \
    ; /)

    case = (/ \
            ; "E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.00"      \
            ; ,"E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_0.00" \
            ; ,"E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.VT_1.00" \
            "E3SM.VTVAL.GNUGPU.ne30pg2_r05_oECv3.F-MMFXX.MOMFB.MOMVT.VT_0.00" \
    /)

   

    debug = False

    ; var = (/"PRECT"/)
    ; var = (/"TGCLDLWP","TGCLDIWP"/)
    var = (/"TGCLDIWP"/)

    month = (/6,7,8/)
    ; month = (/6,7,8/)
    ; month = (/1,2/)
    ; month = ispan(1,1,1)

    E3SM_use_lowres = False
    
;==========================================================================
;==========================================================================
    ; nt = 24
    ; nt = nh2

    if .not.isvar("month") then month = ispan(1,12,1) end if

    ; case = where(case.ne."TRMM",case+"-CPL_0.9x1.25_gx1v6_01",case)
    casename = E3SM_get_casename(case)

    num_c = dimsizes(case)
    num_v = dimsizes(var)

    nmonth = dimsizes(month)

    ; if debug then CESM_trunclen = 1 else CESM_trunclen = 8 end if

    ; nyears = 10
    nyears = 5


    opt  = True
    opt@TRMMyr1     = 2000
    opt@TRMMyr2     = opt@TRMMyr1 + nyears-1
    opt@debug       = debug
    opt@year        = ispan(0,nyears-1,1)
    ; opt@truncate   = True
    ;opt@useraw     = True
    ; opt@use3h      = False
    ; opt@lat1       = -30
    ; opt@lat2       =  60

    TRMM_timeopt_default = "monthly_3hour"

;==========================================================================
;==========================================================================
print("Creating diurnal composites...")
printline()
do c = 0,num_c-1
do v = 0,num_v-1
    print(case(c)+"     "+var(v))
    print("")
    
    ; ofile = E3SM_data_dir+"/"+case(c)+"/diurnal.comp.v1."+var(v)+".nc"
    temp_dir = "~/Research/E3SM/data_temp/"
    ofile = temp_dir+"/diurnal.comp.v1."+case(c)+"."+var(v)+".nc"

    ;--------------------------------------------------------
    ; load data
    ;--------------------------------------------------------
    V := LoadE3SM(case(c),var(v),opt)

    dims := dimsizes(V)
    ndim := dimsizes(dims)

; print("Time dimension : "+dims(0))

    ; nt = nh2
    nt = nt_per_day_h1

    if case(c).eq."TRMM" then nt = 8 end if

    if nt.lt.0 then printExit("ERROR: nt not defined! ") end if

    lat := V&lat
    lon := V&lon
    ;--------------------------------------------------------
    ; Define output variable
    ;--------------------------------------------------------
    ; xdim = new(ndim+1,integer)
    xdim = new(ndim+1,long)
    xdim(0) = nmonth
    xdim(1) = nt
    xdim(2:) = dims(1:)
    dcomp = new(xdim,float)
    dcomp!0 = "month"
    dcomp!1 = "hour"
    if ndim.eq.3 then 
        dcomp!2 = "lat"
        dcomp!3 = "lon"
    end if
    if ndim.eq.4 then 
        dcomp!2 = "lev"
        dcomp!3 = "lat"
        dcomp!4 = "lon"
        dcomp&lev = V&lev
    end if
    dcomp&lat = lat
    dcomp&lon = lon

    dcomp&month = month

    dh = tofloat( 24/nt )

    hour = ispan(0,nt-1,1) * dh
    hour@units = "hour"
    dcomp&hour = hour

    if isatt(V,"units")     then dcomp@units     = V@units end if
    if isatt(V,"long_name") then dcomp@long_name = V@long_name end if
    ;--------------------------------------------------------
    ; Calculate diurnal composite
    ;--------------------------------------------------------
    do m = 0,nmonth-1
        vals := ind( V@mn .eq. month(m) ) 
        
        ; time := V&time
        ; time@calendar = "noleap"
        ; date := cd_calendar(time,0)
        ; mn := date(:,1)
        ; vals := ind( mn .eq. month(m) ) 

        print("    # times / days :  " + dimsizes(vals) + "    /    " + (dimsizes(vals)/nt) )

        if all(.not.ismissing(vals)) then
            do h = 0,nt-1
                if ndim.eq.3 then dcomp(m,h,  :,:) = (/ dim_avg_n( V(vals(h::nt),  :,:) ,0) /) end if
                if ndim.eq.4 then dcomp(m,h,:,:,:) = (/ dim_avg_n( V(vals(h::nt),:,:,:) ,0) /) end if
            end do
        end if
    end do
    ;--------------------------------------------------------
    ; Write to file
    ;--------------------------------------------------------
    if debug then 
        print("WARNING: Debug mode is on, no file was written, exiting.")
        exit 
    end if
    if fileexists(ofile) then system("rm "+ofile) end if
    outfile = addfile(ofile,"c")
    outfile->dcomp = dcomp

    outfile->nyear = nyears
    ;--------------------------------------------------------
    ;--------------------------------------------------------
    print("")
    print("    "+ofile)
    printline()
    delete([/V/])
end do
end do
;==========================================================================
;==========================================================================
end
