; Use data on both physics and dynamics grids to check for drift
; v1 - 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/NCL/custom_functions_E3SM.ncl"
begin

    ;;; use scratch data
    E3SM_data_sub = "/run"
    E3SM_data_dir = "/global/cscratch1/sd/whannah/acme_scratch/cori-knl/"
    E3SM_ignore_h2 = True

    fig_type = "png"
    fig_file = "~/Research/E3SM/figs_grid/ncl.grid_drift.pg.v1"


    case  = "E3SM_SP1_64x1_1000m_ne30pg2_F-EAMv1-AQP1_01"    


    ; var = "T"
    var = (/"T","Q"/)
    
    use_nday = True
    nday = 20

;==================================================================================================
;==================================================================================================
    if .not.isvar("casename") then casename = E3SM_get_casename(case) end if

    num_c = dimsizes(case)
    num_v = dimsizes(var)
    
    native_grid_flag = True

    opt = True
    opt@use_grid_native = native_grid_flag
    opt@monthly  = False
    
    
    ; opt@lev = 200
    ; opt@lev = (/30,50,75,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000/)
    opt@lev = -2
    native_lev_top = 50
    ; if isvar("lev") then opt@lev := lev end if

    ; opt@lat1  = -30
    ; opt@lat2  =  30
    ; opt@lon1  = -30
    ; opt@lon2  =  30


    if use_nday then
        opt@monthly = False
        opt@num_day = nday
        ; opt@daily_avg = True
        ; opt@day0 = 60
    end if

    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(num_v,graphic)
        res = setres_xy()
        res@vpHeightF = 0.2

        cres = setres_contour_fill_smalltxt()
        cres@trYReverse = True

        lres = setres_default()
        lres@xyLineThicknessF   = 2.
        lres@xyLineColor        = "black"
        lres@xyDashPattern      = 0

;==================================================================================================
;==================================================================================================
do v = 0,num_v-1
    printline()
    print("  "+var(v)+":")
    do c = 0,num_c-1
        print("    "+case(c)+":")
        ;------------------------------------------------------
        ;------------------------------------------------------
        opt@area_name = "area_p"
        iX1 := LoadE3SM(case(c),       var(v),opt)

        opt@area_name = "area_d"
        iX2 := LoadE3SM(case(c),"DYN_"+var(v),opt)

        dims1 := dimsizes(iX1)
        dims2 := dimsizes(iX2)
        ndim := dimsizes(dims1)

        iX1@long_name = iX1@long_name + " (phys grid)"
        iX2@long_name = iX2@long_name + " (dyn grid)"
        
        ; printline()
        ; printVarSummary(iX1)
        ; printline()
        ; printVarSummary(iX2)
        
        printline()
        printMAM(iX1)
        printMAM(iX2)
        ; exit


        time := iX1&time

        ;------------------------------------------------------
        ;------------------------------------------------------
        ;;; This isn't working for some reason...
        ; area1 = iX1@area
        ; area2 = iX2@area

        opt@area_name = "area_p"
        opt@dyn_grid = False
        area1 := LoadE3SMarea(case(c),opt)

        opt@area_name = "area_d"
        opt@dyn_grid = True
        area2 := LoadE3SMarea(case(c),opt)

        ; ifile = "~/E3SM/init_files/RCEMIP_domain.ocn.ne4np1.nc"
        ; infile = addfile(ifile,"r")
        ; area1 = infile->area

        ; ifile = "~/E3SM/init_files/RCEMIP_domain.ocn.ne4np4_oQU240.160614.nc"
        ; infile = addfile(ifile,"r")
        ; area2 = infile->area

        ; kopt = True
        ; kopt@iter = 200
        ; kopt@iseed = 2
        ; kmeans = kmeans_as136( conform_dims((/1,866/),area2,1) ,3,kopt)
        ; print(kmeans)

        ;;; isolate middle nodes
        ; area2 = where(area2.ge.0.02,area2,0.)


printDim(area1)
printDim(area2)

; printMAM(area1)
; printMAM(area2)

; area2@grid = "ne4np4"
; qplot_map_se(area2)

; area1@grid = "ne4np1"
; qplot_map_se(area1)

; exit


        wgt1 = area1 / sum(area1)
        wgt2 = area2 / sum(area2)

        adim_size1 = dimsizes(area1)
        adim_size2 = dimsizes(area2)
        adim_index1 = -1
        adim_index2 = -1
        do d = 0,ndim-1
            if dims1(d).eq.adim_size1 then adim_index1 = d end if
            if dims2(d).eq.adim_size2 then adim_index2 = d end if
        end do
        if (adim_index1.eq.-1).or.(adim_index2.eq.-1) then 
            print("ERROR: could not find dimension matching the area array")
            exit
        end if

        aX1 := dim_sum_n_Wrap( iX1 * conform(iX1,wgt1,adim_index1) ,adim_index1)
        aX2 := dim_sum_n_Wrap( iX2 * conform(iX2,wgt2,adim_index2) ,adim_index2)

        aX1!0 = "time"
        aX1&time = iX1&time
        if ndim.eq.3 then
            aX1!1 = "lev"
            aX1&lev = iX1&lev
        end if
        copy_VarCoords(aX1,aX2)
        
        delete([/iX1,iX2/])
        ;------------------------------------------------------
        ;------------------------------------------------------
            if ndim.eq.3 then
                tres := cres
            else
                tres = res
                ; tres@xyLineColor   = clr(c)
                tres@xyLineColors   = (/"red","blue"/)
                tres@xyDashPatterns = (/0,1/)
            end if
            tres@tiXAxisString = "Time ["+time@units+"]"

            tres@gsnLeftString  = casename(c)
            tres@gsnRightString = var(v)

        if ndim.eq.3 then
            tres@gsnRightString = tres@gsnRightString+" Difference"
            if all(opt@lev.eq.-2) then
                if native_lev_top.lt.min(aX1&lev) then
                    tres@trYMinF = min(aX1&lev)
                else
                    tres@trYMinF = native_lev_top
                end if
            end if
            dX = (/ aX1 - aX2 /)
            ; dX = (/ dX - conform(dX,dX(0,:),1) /)
            copy_VarCoords(aX1,dX)

            symMinMaxPlt(dX,21,False,tres)

            tplot = gsn_csm_contour(wks,dX(lev|:,time|:),tres)
        else
            tres@trYMinF = min((/aX1,aX2/)) - stddev((/aX1,aX2/))*0.1
            tres@trYMaxF = max((/aX1,aX2/)) + stddev((/aX1,aX2/))*0.1
            tplot = gsn_csm_xy(wks,time,(/aX1,aX2/),tres)
        end if

        plot(v) = tplot

        ; if c.eq.0 then 
        ;     plot(ip) = tplot
        ; else

        ;------------------------------------------------------
        ;------------------------------------------------------

    end do ; c
end do ; v
;==================================================================================================
;==================================================================================================
    pres = setres_panel()
    pres@gsnPanelYWhiteSpacePercent = 5 
    layout = (/1,dimsizes(plot)/)
    gsn_panel(wks,plot,layout,pres)
    trimPNG(fig_file)

;==================================================================================================
;==================================================================================================
end