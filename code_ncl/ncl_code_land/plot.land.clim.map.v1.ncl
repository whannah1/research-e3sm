; plot a map of a global mean quantity
; v1 - 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/NCL/custom_functions_E3SM.ncl"
begin

    E3SM_data_dir = "/lustre/atlas1/cli115/scratch/hannah6"
    E3SM_data_sub = "/run"
    E3SM_hist_comp = "clm2"
    ; E3SM_use_remap = True

E3SM_ignore_h2 = True

    case = "E3SM_SP1_CTL_ne30_64x1_1km_80a";+(/"a","b"/)
    ; casename = (/"Branch @ day 5","Branch @ day 10"/)

    ; case = (/"E3SM_SP1_CTL_ne30_64x1_1km_00","E3SM_SP1_CTL_ne30_64x1_1km_00c"/)
    ; casename = (/"old SP-E3SM","new SP-E3SM"/)

    ; case = (/"E3SM_SP1_CTL_ne30_64x1_1km_00"/)
    ; casename = (/"SP-E3SM"/)

; case = (/"E3SM_ZM_TEST_ne30_00","E3SM_SP1_TEST_ne30_64x1_4km_00","E3SM_SP1_TEST_ne30_64x1_4km_00e"/)
; casename = (/"E3SM","SP-E3SM","SP-E3SM w/ bug fix"/)
; case = (/"E3SM_ZM_TEST_ne30_00","E3SM_SP1_TEST_ne30_64x1_4km_00e"/)
; casename = (/"E3SM","SP-E3SM"/)
; nh1 = 24
; E3SM_ignore_h2 = True


    fig_type = "png"
    fig_file = "~/Research/E3SM/figs_land/land.clim.map.v1"

    
    ; var = "H2OSOI"
    ; var = "QH2OSFC"
    ; var = "ZWT"
    ; var = (/"QINFL"/)
    ; var = "RAIN"
    ; var = "PRECT"
    ; var = "TWS"
    var = "TLAI"
    ; var = (/"FSA","FSDS"/)
    ; var = (/"FSDSND","FSDSNI"/)
    ; var = (/"FSDS","FSDSVI","FSDSVD"/)


    use_nday = True
    nday = 5

    nyears = 1

    ;;; Comment out the months variable for all seasons
    ; months = (/2/)
    months = ispan(1,1,1)
    ; months = ispan(5,8,1)
    
    
    add_vec     = False
    add_box     = False
    wind_lev    = 975

    plot_diff   = True

    panel_flip  = True       ; True = case x var
;===================================================================================
;===================================================================================
    case = E3SM_get_case(case)

    if .not.isvar("casename") then casename = E3SM_get_casename(case) end if
    casename = where(case.eq."E3SM_ZM_CTL_ne30_00","Standard E3SM" ,casename)
    
    num_c = dimsizes(case)
    num_v = dimsizes(var)

    E3SM_trunclen = nyears

    opt  = True
    ; opt@debug    = True
    opt@monthly  = True
    ; opt@truncate = True
    ; opt@useremap = True
    ; opt@ERAIyr1  = 2000
    ; opt@ERAIyr2 = opt@ERAIyr1 + E3SM_trunclen
    opt@ERAIyr1  = 2000
    opt@ERAIyr2  = opt@ERAIyr1+nyears-1
    opt@TRMMyr1  = 2000
    opt@TRMMyr2  = opt@TRMMyr1+nyears-1
    opt@GPCPyr1  = 2000
    opt@GPCPyr2  = opt@GPCPyr1+nyears-1
    ; opt@year = ispan(1990,1990+nyears,1) - 1974
    ; opt@year = 1998 - 1997
    opt@year = ispan(0,nyears-1,1)
    ; opt@year = (/1/)

    if use_nday then
        opt@monthly  = False
        opt@num_day = nday
    end if

    opt@lev = -2

    opt@lat1  = -40
    opt@lat2  =  40


    ;;; South America
    opt@lat1 = -40
    opt@lat2 =  20
    opt@lon1 = -100
    opt@lon2 = -20 

;===================================================================================
; Define plot properties
;===================================================================================

    wks = gsn_open_wks(fig_type,fig_file)
    plot = new(num_c*num_v,graphic)
        res = setres_contour_fill_smalltxt()
        ;res = setres_contour()
        ; res@lbLabelBarOn    = False
        ; res@cnLineLabelsOn  = False
        ; res@cnInfoLabelOn   = False
        if isatt(opt,"lon1") then res@gsnAddCyclic    = False end if
        ; res@mpCenterLonF    = 0.
        res@tmXBLabelFontHeightF        = 0.015
        res@tmYLLabelFontHeightF        = 0.015
        res@gsnLeftStringFontHeightF    = 0.02;15
        ; res@gsnCenterStringFontHeightF  = 0.02;15
        res@gsnRightStringFontHeightF   = 0.02;15
        ; if isatt(opt,"lon1") then
        ;     res@gsnLeftStringFontHeightF    = 0.015
        ;     res@gsnCenterStringFontHeightF  = 0.015
        ;     res@gsnRightStringFontHeightF   = 0.015
        ; end if
        res@mpLimitMode     = "LatLon" 
        ;res@mpProjection    = "Aitoff"
        if isatt(opt,"lat1") then res@mpMinLatF = opt@lat1 end if
        if isatt(opt,"lat2") then res@mpMaxLatF = opt@lat2 end if
        if isatt(opt,"lon1") then res@mpMinLonF = opt@lon1 end if
        if isatt(opt,"lon2") then res@mpMaxLonF = opt@lon2 end if
        res@mpCenterLonF    = 180
        ; res@mpMinLonF     = 0
        ; res@mpMaxLonF     = 360

        dlb = 0.3
        res@lbTopMarginF    = 0.05 + dlb
        res@lbBottomMarginF = 0.05 - dlb ;+ 0.1
        

        vres = setres_vector()
        vres@vcRefAnnoOn = False

        lres = setres_default()
        lres@xyLineThicknessF   = 1.
        lres@xyLineColor        = "black"
        lres@xyDashPattern      = 0
;===================================================================================
;===================================================================================
orig_E3SM_data_dir = E3SM_data_dir
if isatt(opt,"monthly") then orig_opt_monthly = opt@monthly else orig_opt_monthly = False end if
do v = 0,num_v-1
do c = 0,num_c-1

    printline()
    print(case(c)+":")

    E3SM_data_dir = orig_E3SM_data_dir

    
    ; if var(v).eq."PRECT".and.(case(c).ne."GPCP".and.case(c).ne."TRMM") then
    ;     opt@monthly = False
    ; else
    ;     opt@monthly = orig_opt_monthly
    ; end if
    ;---------------------------------------
    ;---------------------------------------
    derived = False

    if var(v).eq."PRECT".and.(case(c).ne."GPCP".and.case(c).ne."TRMM") then
        iX := LoadE3SM(case(c),"PRECC",opt)    
        iX = (/ iX + LoadE3SM(case(c),"PRECL",opt) /)
        derived = True
    end if
    
    if .not.derived then iX := LoadE3SM(case(c),var(v),opt) end if

    ; if isatt(opt,"lon1") then 
    ;     ; iX(:,:,:) = iX(:,:,::-1) 
    ; else
    ;     if any(iX&lon.lt.0.) then iX := flip_lon(iX) end if
    ; end if


    if any(case.eq."GPCP").and.case(c).ne."GPCP" then
        if all(iX&lon.lt.0.) then 
            iX&lon = (/ iX&lon + 360. /)
            ; iX := flip_lon(iX)
        end if
    end if

    dims := dimsizes(iX)
    ndim := dimsizes(dims)


    ; print(dim_collapse(iX,1))
    ; exit

    print("Time dimension length : "+dims(0))

    ; if ndim.le.3.and.isatt(opt,"lev") then delete(opt@lev) end if
    ;---------------------------------------
    ; isolate near-surface layer
    ;---------------------------------------
    if ndim.eq.4 .and.opt@lev.eq.-2 then
        ; tmp = iX(:,0,:,:)
        ; tmp = iX(:,dims(1)/2,:,:)
        tmp = iX(:,9,:,:)
        iX := tmp
        delete(tmp)
    end if
    ;---------------------------------------
    ; vertically integrate
    ;---------------------------------------
    ; if ndim.eq.4 .and.opt@lev.eq.-2 then
    ;     tmp = dim_sum_n_Wrap( iX(:,:,:,:) ,1)
    ;     iX := tmp
    ;     delete(tmp)
    ;     dims := dimsizes(iX)
    ;     ndim := dimsizes(dims)
    ; end if

    ; if ndim.eq.4 then 
    ;     if opt@lev.eq.-2 then
    ;         k_lev = 0
    ;         tX = iX(:,dims(1)-1-k_lev,:,:)
    ;         iX := tX
    ;         delete(tX)
    ;     else
    ;         lev := iX&lev*1e2
    ;         lev!0 = "lev"
    ;         lev&lev = lev
    ;         lev@units = "Pa"
    ;         dp = calc_dP(lev,LoadE3SM(case(c),"PS",opt))
    ;         tX = dim_sum_n(iX*dp/g,1)
    ;         copy_VarCoords(iX(:,0,:,:),tX)
    ;         copy_VarAtts  (iX(:,0,:,:),tX)
    ;         delete(iX)
    ;         iX = tX
    ;         delete([/tX,dp/])
    ;     end if
    ; end if 
    ;---------------------------------------
    ; Adjust units
    ;---------------------------------------
    ; if any(var(v).eq.(/"PRECT"/)).and..not.opt@monthly then 
    if any(var(v).eq.(/"PRECT"/)) then 
       if all(case(c).ne.(/"ERAi","TRMM"/)) then  iX = (/ iX * 86400. * 1e3 /) end if
       iX@units = "mm/day"
    end if
    if case(c).eq."ERAi" .and. any(var(v).eq.(/"TAUX","TAUY"/)) then 
        iX = (/iX*-1./)
    end if

    if any(var(v).eq.(/"QINFL","RAIN"/)) then
        iX = (/iX*86400./)
        iX@units = "mm/day"
    end if

    if any(var(v).eq.(/"PRECC"/)) then 
        iX = (/ iX * 86400. * 1e3 /)
    end if
    ;---------------------------------------
    ;---------------------------------------
    if add_vec then 
        topt = opt
        topt@lev = wind_lev
        iU := LoadE3SM(case(c),"U",topt)
        iV := LoadE3SM(case(c),"V",topt)
    end if

    ;---------------------------------------
    ; interpolate TRMM to model grid
    ;---------------------------------------
    if any(case(c).eq.(/"TRMM","GPCP"/)) then
        do cc = 0,num_c-1
            if all(case(cc).ne.(/"TRMM","GPCP"/)) then tcase = case(cc) end if
        end do
        if isvar("tcase") then
            mlat = LoadE3SMlat_remap(tcase,opt)
            mlon = LoadE3SMlon_remap(tcase,opt)

            ; need to flip for interpolation so we don't get white line at 180
            ;;; iX := flip_lon(iX)
            mlon := flip_lon(mlon)
            mlon = (/mlon&lon/)


            ; if all(iX&lon.lt.0.) then 
            ; if all(mlon.lt.0.) then 
            ; if case(c).eq."GPCP" then
            ;     iX := flip_lon(iX) 
            ;     mlon = (/mlon&lon/)
            ; end if

; printMM(iX&lon)
; printMM(mlon)
; exit

            tmp = linint2_Wrap(iX&lon,iX&lat,iX,True,mlon,mlat,0)
            iX := tmp
            delete(tmp)
            ; iX := flip_lon(iX)

            delete(tcase)
        end if
    end if
    ;---------------------------------------
    ; Isolate season
    ;---------------------------------------
    if isvar("months") then

        mn := iX@mn
        condition := mn.eq.0
        do m = 0,dimsizes(months)-1 condition = condition.or.(mn.eq.months(m)) end do
        vals := where(condition,True,False)
        if ndim.eq.3 then tX := iX(ind(vals),:,:) end if
        if ndim.eq.2 then tX := iX(ind(vals),:) end if
        if add_vec then
            tU := iU(ind(vals),:,:)
            tV := iV(ind(vals),:,:)
        end if
        ;if ndim.eq.3 then tX := iX(ind(vals),:,:) end if
        ;if ndim.eq.4 then tX := iX(ind(vals),:,:) end if

        smn = (/"J","F","M","A","M","J","J","A","S","O","N","D"/)
        str = ""
        do m = 0,dimsizes(months)-1 str = str+smn(months(m)-1) end do
        season = str
    else
        tX := iX
        ; season = "All Seasons"
        season = ""
        if add_vec then
            tU := iU
            tV := iV
        end if
    end if
    ;---------------------------------------
    ; Average in time
    ;---------------------------------------
    tdims := dimsizes(tX)
    tndim := dimsizes(tdims)
    if tndim.gt.2 then
        aX := dim_avg_n_Wrap(tX,0)
    else
        aX := tX
    end if
    aX&lat@units = "degrees north"
    aX&lon@units = "degrees east"

    ; if any(var(v).eq.(/"ZMMTU","ZMMTV","U_ESMT","V_ESMT","U_residual","V_residual"/)) then aX = (/aX*86400./) end if

    delete([/iX/])
    
    if add_vec then
        aU := dim_avg_n_Wrap(tU,0)
        aV := dim_avg_n_Wrap(tV,0)
        delete([/iU,iV,tU,tV/])
    end if
    ;---------------------------------------
    ; print stats to check for unit issues
    ;---------------------------------------
    printMAM(aX)
    print("")
    print("max of all data : "+max(tX))

    delete([/tX/])
    ;---------------------------------------
    ; print global mean
    ;---------------------------------------
    if True then
        lat := aX&lat
        lon := aX&lon
        nlat = dimsizes(lat)
        nlon = dimsizes(lon)

        re   = 6.37122e06
        rad  = 4.0 * atan(1.0) / 180.
        con  := re * rad                 
        clat := cos(lat * rad)           ; cosine of latitude
        dlon := (lon(2) - lon(1))        ; assume dlon is constant
        dlat := (lat(2) - lat(1))        ; assume dlat is constant
        dx   := con * dlon * clat        ; dx at each latitude
        dy   := con * dlat               ; dy is constant
        dydx := dy * dx  
        wgt  := new((/nlat,nlon/), float)
        wgt  = conform(wgt,tofloat(dydx), 0)

        avgopt := 0
        gX := wgt_areaave2(aX,wgt,avgopt)

        print("")
        print("Global mean     : "+gX)

    end if
    print("")
    ;---------------------------------------
    ; Assign contour levels
    ;---------------------------------------
    if isatt(res,"cnLevelSelectionMode") then delete(res@cnLevelSelectionMode) end if
    if isatt(res,"cnLevels") then delete(res@cnLevels) end if
    ;res@cnLevelSelectionMode = "ExplicitLevels"
    ;if var(v).eq."OMEGA" then cnLevels = ispan(-25,25,5)/1e2 end if

    if any(var(v).eq.(/"ZMMTU","ZMMTV","U_ESMT","V_ESMT","U_CMT_TOTAL","V_CMT_TOTAL"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        if opt@lev.gt.800 then
            res@cnLevels = ispan(-10,10,1)/1e1 * 5
        else
            res@cnLevels = ispan(-10,10,1)/1e1 * 1
        end if
    end if

    if any(var(v).eq.(/"PRECT","PRECC"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ; res@cnLevels = ispan(2,18,2)
        ; if opt@monthly then
            res@cnLevels = ispan(1,18,1)
        ; else
        ;     res@cnLevels = ispan(2,30,2)
        ; end if
    end if

    if any(var(v).eq.(/"TS"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels = ispan(280,312,2)
        res@cnLevels = ispan(273,314,2)
    end if

    if any(var(v).eq.(/"TMQ"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels = ispan(280,312,2)
        res@cnLevels = ispan(4,60,4)
    end if

    if any(var(v).eq.(/"PSL"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ;res@cnLevels = ispan(280,312,2)
        res@cnLevels = ispan(1009,1024,1)*1e2
    end if

    if any(var(v).eq.(/"LHFLX"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        res@cnLevels = ispan(5,205,10)
    end if

    if any(var(v).eq.(/"SWCF"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ; res@cnLevels = ispan(-160,40,10)
        res@cnLevels = ispan(-160,40,20)
    end if

    if any(var(v).eq.(/"LWCF"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ; res@cnLevels = ispan(-40,110,10)
        res@cnLevels = ispan(-40,110,20)
    end if

    if any(var(v).eq.(/"TAUX","TAUY"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        ; res@cnLevels = ispan(-14,14,2)/1e2
        res@cnLevels = ispan(-50,50,5)/1e2
    end if

    if any(var(v).eq.(/"UdQdxvi","VdQdyvi"/)) then 
        res@cnLevelSelectionMode = "ExplicitLevels"
        res@cnLevels = ispan(-300,300,30)
    end if

    if .not.isatt(res,"cnLevelSelectionMode") then 
    if c.eq.0  then 
        mnmxint = nice_mnmxintvl( min(aX), max(aX), 21, False)
        res@cnLevelSelectionMode = "ManualLevels"
        res@cnMinLevelValF       = mnmxint(0)
        res@cnMaxLevelValF       = mnmxint(1)
        res@cnLevelSpacingF      = mnmxint(2)
        if any(var(v).eq.(/"U","V","OMEGA","TOARAD"/)) then symMinMaxPlt(aX,31,False,res) end if
    end if
    end if
    ;---------------------------------------
    ; assign color map
    ;---------------------------------------
    res@cnFillPalette = "ncl_default"
    if any(var(v).eq.(/"CWV","TMQ","PRECT","PRECC","CLDLIQ"/)) then 
        res@cnFillPalette = "WhiteBlueGreenYellowRed"
    end if

    if any(var(v).eq.(/"QEXCESS"/)) then 
        res@cnFillPalette = "prcp_1"
        gsn_reverse_colormap(wks)  
    end if

    if any(var(v).eq.(/"CLDLOW","CLDHGH"/)) then 
        res@cnFillPalette = "CBR_wet"
    end if

    if any(var(v).eq.(/"TAUX","TAUY"/)) then 
        res@cnFillPalette = "cmp_flux"
    end if
    
    ;---------------------------------------
    ; Take difference from first case
    ;---------------------------------------
    if plot_diff then
        abs_diff_vars = (/"TAUX","TAUY"/)
        if c.eq.0 then 
            Xo := aX 
            if add_vec then
                Uo := aU
                Vo := aV
            end if
        else
            tdX := linint2_Wrap(Xo&lon,Xo&lat,Xo,False,aX&lon,aX&lat,0)
            if any(var(v).eq.abs_diff_vars) then
                aX = (/ abs(aX) - abs(tdX) /)
            else
                aX = (/ aX - tdX /)
            end if
            if add_vec then
                aU = (/ aU - linint2_Wrap(Uo&lon,Uo&lat,Uo,False,aU&lon,aU&lat,0) /)
                aV = (/ aV - linint2_Wrap(Vo&lon,Vo&lat,Vo,False,aV&lon,aV&lat,0) /)
            end if
        end if
        if c.gt.0 then 
            res@cnFillPalette = "BlWhRe" 
            if c.eq.1 then 
                dres = res
                symMinMaxPlt(aX,21,False,dres) 

                if any(var(v).eq.(/"PRECT"/)) then 
                    dres@cnLevelSelectionMode = "ExplicitLevels"
                    dres@cnLevels := ispan(-12,12,2)
                end if

            end if
            ; if any(var(v).eq.abs_diff_vars) then
            ;     aX = abs(aX)
            ; end if
        end if
    end if
    ;---------------------------------------
    ; Set strings at top of plots
    ;---------------------------------------
    if .not.isatt(aX,"long_name") then aX@long_name = var(v) end if

    if var(v).eq."PRECT"  then aX@long_name = "Precipitation" end if
    if var(v).eq."TMQ"    then aX@long_name = "CWV" end if
    if var(v).eq."CLDLOW" then aX@long_name = "Vert. Integrated Low Cloud" end if
    if var(v).eq."CLDHGH" then aX@long_name = "Vert. Integrated High Cloud" end if
    if var(v).eq."ZMMTU"  then aX@long_name = "Zonal CMT (ZM)" end if
    if var(v).eq."ZMMTV"  then aX@long_name = "Meridional CMT (ZM)" end if

    ; Note - these names got switched for a few early cases
    if var(v).eq."U_ESMT" then aX@long_name = "Meridional CMT (ESMT)" end if
    if var(v).eq."V_ESMT" then aX@long_name = "Zonal CMT (ESMT)" end if

    if var(v).eq."TGCLDLWP" then aX@long_name = "liq water path" end if
    if var(v).eq."SWCF"     then aX@long_name = "SW cloud forcing" end if
    if var(v).eq."LWCF"     then aX@long_name = "LW cloud forcing" end if

    if var(v).eq."QEXCESS"  then aX@long_name = "QNEG4 Adjustment" end if
    if var(v).eq."TS"       then aX@long_name = "T sfc" end if

    ; if ndim.eq.4.and.isatt(opt,"lev") then

    ; if isatt(opt,"lev") then
    ;     if opt@lev.eq.-2 then 
    ;         aX@long_name = "(k="+k_lev+") "+aX@long_name 
    ;     else
    ;         aX@long_name = opt@lev+"mb "+aX@long_name 
    ;     end if
    ; end if
    if isatt(aX,"units") then sunits = aX@units else sunits = "" end if
    res@gsnLeftString   = casename(c)
    res@gsnRightString  = aX@long_name 
    ; res@gsnCenterString = season
    ;res@gsnCenterString = "Avg: "+global_wgt_avg(aX) +" "+sunits
    
    if var(v).eq."PRECT".and.case(c).eq."ERAi" then res@gsnLeftString = "TRMM" end if
    if plot_diff.and.c.gt.0 then 
        if any(var(v).eq.abs_diff_vars) then
            dres@gsnLeftString = res@gsnLeftString+" (absolute difference)"
        else
            dres@gsnLeftString = res@gsnLeftString+" (difference)" 
            ; dres@gsnLeftString = res@gsnLeftString
            ; dres@gsnRightString = res@gsnRightString+" Bias" 
        end if
    end if
    ;---------------------------------------
    ; Create plot
    ;---------------------------------------
    i = v*num_c+c
    ; res@lbLabelBarOn = False
    if panel_flip then 
        i = c*num_v+v 
        ; if c.eq.(num_c-1) then res@lbLabelBarOn = True end if
    else
        ; if v.eq.(num_v-1) then res@lbLabelBarOn = True end if
    end if

    if plot_diff then
        res@lbLabelBarOn = True
    end if
    
    tres = res
    ; if case(c).eq."TRMM" then tres@gsnAddCyclic = True end if
    if plot_diff.and.c.gt.0 then tres = dres end if

    if isStrSubset(case(c),"AQUA") then 
        tres@mpOutlineBoundarySets = "NoBoundaries"
    end if

    plot(i) = gsn_csm_contour_map(wks,aX,tres)

    ; Add line at equator
    xx = (/-1,1/)*1e3
    overlay( plot(i) , gsn_csm_xy(wks,xx,0.*xx,lres) ) 
    ; overlay( plot(i) , gsn_csm_xy(wks,aX&lon,0.*aX&lon,lres) ) 

    if add_vec then
        overlay( plot(i) , gsn_csm_vector(wks,aU,aV,vres) ) 
    end if

    if isStrSubset(case(c),"NUNA") then 
        topt = opt
        topt@monthly = True
        frac := LoadE3SM(case(c),"LANDFRAC",opt)
            cres = setres_contour()
            cres@cnInfoLabelOn      = False
            cres@cnLineLabelsOn     = False
            cres@cnLevelSelectionMode = "ExplicitLevels"
            cres@cnLevels = (/0.5/)
            cres@gsnLeftString  = ""
            cres@gsnRightString = ""
        overlay( plot(i) , gsn_csm_contour(wks,frac(0,:,:),cres) )
        delete(frac)
    end if
    ;---------------------------------------
    ;---------------------------------------
    mopt = True
    mopt@blat1  =   0
    mopt@blat2  =  30
    mopt@blon1  = -20
    mopt@blon2  =  10
    mopt@lineclr = "red"
    mopt@linethk = 4.
    if add_box then
        plot_add_box(wks,plot(i),mopt)
    end if
    ;---------------------------------------
    ;---------------------------------------
end do
end do
printline()
;===================================================================================
;===================================================================================
        pres = setres_panel()
        ; pres = setres_panel_labeled()
        pres@gsnPanelYWhiteSpacePercent = 5
        pres@gsnPanelBottom = 0.1
        ;pres@txString = season

    layout = (/num_v,num_c/)
    if panel_flip then layout = (/num_c,num_v/) end if

    ; if (num_v*num_c).eq.4 then layout = (/2,2/) end if
    ;if (num_v*num_c).eq.9 then layout = (/3,3/) end if

    gsn_panel(wks,plot,layout,pres)

    trimPNG(fig_file)

;===================================================================================
;===================================================================================

end

