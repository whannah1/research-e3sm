
period_min =  20
period_max = 100

nh = 1

fcb = 1./(tofloat(nh)*period_min)
fca = 1./(tofloat(nh)*period_max)
opt = False
nwgt  = 120*nh+1
sigma = 1.0

wgt   = filwgts_lanczos (nwgt,2,fca,fcb,sigma)

num_wgt = dimsizes(wgt)
str = ""
do w = 0,num_wgt-1
    str = str + sprintf("%10.6f",wgt(w)) +","
end do

cr = tochar(10)
str = cr+"["+str+"]"

print(str)