
#---------------------------------------------------------------------------------------------------
# import cdsapi
# c = cdsapi.Client()
# c.retrieve(
#     'satellite-aerosol-properties',
#     {
#         'format': 'tgz',
#         'variable': [
#             'aerosol_optical_depth', 'fine_mode_aerosol_optical_depth',
#         ],
#         'year': '2022',
#         'month': '05',
#         'day': [
#             '01', '02', '03',
#             '04', '05', '06',
#             '07', '08', '09',
#             '10', '11', '12',
#             '13', '14', '15',
#             '16', '17', '18',
#             '19', '20', '21',
#             '22', '23', '24',
#             '25', '26', '27',
#             '28', '29', '30',
#             '31',
#         ],
#         'time_aggregation': 'daily_average',
#         'algorithm': 'swansea',
#         'sensor_on_satellite': 'slstr_on_sentinel_3b',
#         'version': 'v1.12',
#     },
#     'download.tar.gz')

#---------------------------------------------------------------------------------------------------
import cdsapi

dataset = "satellite-aerosol-properties"
request = {
    'time_aggregation': 'daily_average',
    'variable': ['aerosol_optical_depth'],
    'sensor_on_satellite': 'slstr_on_sentinel_3b',
    'algorithm': 'swansea',
    'year': ['2022'],
    'month': ['05'],
    'day': ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'],
    'version': ['v1_12']
}

client = cdsapi.Client()
client.retrieve(dataset, request).download()
#---------------------------------------------------------------------------------------------------