# v2 is a cleaned up version of v1 - functionality is mostly the same
import os, glob, ngl, subprocess as sp, datetime, copy, string, cmocean
import numpy as np, xarray as xr, dask, pandas as pd
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import warnings
#-------------------------------------------------------------------------------
name,case,case_root,case_sub = [],[],[],[]
scrip_file_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,c=None,scrip_file=None):
   global name,case,case_root,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); 
   case_root.append(p); case_sub.append(s);
   scrip_file_list.append(scrip_file)
#-------------------------------------------------------------------------------
var,lev_list,var_str = [],[],[]
def add_var(var_name,lev=0,s=None): 
   var.append(var_name); lev_list.append(lev); 
   var_str.append(var_name if s is None else s)
#-------------------------------------------------------------------------------
# gif_file = os.getenv('HOME')+'/Research/E3SM/figs_nimbus/nimbus.animation.map.v1'
gif_file = 'figs_nimbus/nimbus.animation.map.v1'

time1,time2,dtime = 0,24*3,1
# time1,time2,dtime = 0,24*10,1

plot_start_date = '2022-05-15'
# plot_start_date = '2022-05-22'

time_list = list(np.arange(time1,time2+dtime,dtime))

create_png,create_gif = True,True
# create_png,create_gif = True,False
# create_png,create_gif = False,True

#-------------------------------------------------------------------------------

troot,tsub = '/p/lustre1/hannah6/e3sm_scratch','run'

tscrip_file_ne128 = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-128x3_pg2_scrip.nc'
tscrip_file_ne64  = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-64x3_pg2_scrip.nc'
tscrip_file_ne32  = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-14.NN_512.DTP_60.DTD_6.NDG_OFF.PGW_OFF',   n='128x3 NDG_OFF',p=troot,s=tsub,scrip_file=tscrip_file1)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-14.NN_512.DTP_60.DTD_6.NDG_ON.PGW_OFF',    n='128x3 NDG_ON', p=troot,s=tsub,scrip_file=tscrip_file1)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-16.NN_512.DTP_60.DTD_6.NDG_OFF.PGW_OFF',   n='128x3 NDG_OFF',p=troot,s=tsub,scrip_file=tscrip_file1)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-16.NN_512.DTP_60.DTD_6.NDG_ON.PGW_OFF',    n='128x3 NDG_ON', p=troot,s=tsub,scrip_file=tscrip_file1)
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   n='32x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file2)
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    n='32x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file2)
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   n='32x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file2)
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    n='32x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file2)
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-14.NN_128.DTP_120.DTD_15.NDG_OFF.PGW_OFF',  n='64x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file3)
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-14.NN_128.DTP_120.DTD_15.NDG_ON.PGW_OFF',   n='64x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file3)
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-16.NN_128.DTP_120.DTD_15.NDG_OFF.PGW_OFF',  n='64x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file3)
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-16.NN_128.DTP_120.DTD_15.NDG_ON.PGW_OFF',   n='64x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file3)

add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF', n='E3SMv3 ne32x3',  p=troot,s=tsub,scrip_file=tscrip_file_ne32)

# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF', n='NDG_TAU=6hr',  p=troot,s=tsub,scrip_file=tscrip_file_ne32)
# add_case('E3SM.2024-NIMBUS-IRAQ-03.32x3-pg2.F2010.2022-05-14.NDG_TAU_12.PGW_OFF',                  n='32x3 NDG_TAU=12', p=troot,s=tsub,scrip_file=tscrip_file_ne32)
# add_case('E3SM.2024-NIMBUS-IRAQ-03.32x3-pg2.F2010.2022-05-14.NDG_TAU_24.PGW_OFF',                  n='32x3 NDG_TAU=24', p=troot,s=tsub,scrip_file=tscrip_file_ne32)
# add_case('E3SM.2024-NIMBUS-IRAQ-03.32x3-pg2.F2010.2022-05-14.NDG_TAU_48.PGW_OFF',                  n='NDG_TAU=48hr', p=troot,s=tsub,scrip_file=tscrip_file_ne32)

#-------------------------------------------------------------------------------

# add_var('V850')

# add_var('BURDENDUST',s='Total Column Dust Burden [kg/m2]')
# add_var('ABURDENDUST')

add_var('PRECT')
# add_var('TMQ')
# add_var('FLNT')

# add_var('BURDENDUST',s='Total Column Dust Burden [kg/m2]')
# add_var('QTRANS',    s='Vapor Transport Magnitude [kg/m/s]')


# add_var('TGCLDLWP',s='Liq Water Path')
# add_var('TGCLDIWP',s='Ice Water Path')


subtitle_font_height = 0.015

#-------------------------------------------------------------------------------
# lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = 0,60,20,80
# lat1,lat2,lon1,lon2 = 10,40,30,60
lat1,lat2,lon1,lon2 = 12,50,20,70
#-------------------------------------------------------------------------------

htype    = 'h1'

print_stats          = True
var_x_case           = False

num_plot_col         = 1 # len(case)

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if case==[]: raise ValueError('ERROR - case list is empty!')
num_var,num_case = len(var),len(case)

if 'subtitle_font_height' not in locals(): subtitle_font_height = 0.006

res = hs.res_contour_fill_map()
if 'lat1' in locals() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in locals() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.lbLabelFontHeightF           = 0.015
res.tmXBOn                       = False
res.tmYLOn                       = False
res.mpGeophysicalLineColor       = 'white'
res.mpGeophysicalLineThicknessF  = 4.
res.mpDataBaseVersion = 'MediumRes'
# res.mpProjection = 'Mollweide'
res.lbLabelBarOn = False
# res.lbLabelBarOn = False if use_common_label_bar else True

#---------------------------------------------------------------------------------------------------
def set_color_palette(var,res):
   res.cnFillPalette = "MPL_viridis"
   # res.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
   # res.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
   if var in ['P-E']                      : res.cnFillPalette = "BlueWhiteOrangeRed"
   if var in ['CLDLOW','CLDMED','CLDHGH'] : res.cnFillPalette = "CBR_wet"
   if var in ['TGCLDLWP','TGCLDIWP']      : res.cnFillPalette = "MPL_viridis"
   if var in ['DYN_QLIQ']                 : res.cnFillPalette = "MPL_viridis"
   # if var in ['TS','PS']                  : res.cnFillPalette = 'BlueWhiteOrangeRed'
   # if var in ['TS','PS']                  : res.cnFillPalette = 'WhiteBlueGreenYellowRed'
   # if var in ['TS','PS']                  : res.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
   if var[v]=='QTRANS'                    : res.cnFillPalette = "MPL_viridis"
   if var in ['U','V'] or var[0] in ['U','V']:
      res.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
   return
#---------------------------------------------------------------------------------------------------
def set_color_levels(var,res):
   if var=='ABURDENDUST'              : res.cnLevels = np.linspace(1e-4,60e-4,20)
   # if var=='BURDENDUST'               : res.cnLevels = np.linspace(1e-4,60e-4,20)
   if var=='BURDENDUST'               : res.cnLevels = np.linspace(5e-5,405e-5,20)
   # if var=='BURDENDUST'               : res.cnLevels = np.linspace(1e-4,20e-4,20)
   if var=='BURDENDUST'               : res.cnLevels = np.logspace( np.log10(10e-5), np.log10(600e-5), num=40).round(decimals=4)
   if var=='AODDUST'                  : res.cnLevels = np.linspace(0.01,0.81,8*2)
   # if var=='AODDUST'                  : res.cnLevels = np.logspace( np.log10(0.01), np.log10(1.), num=20).round(decimals=4)
   if var in ['PRECT','PRECC','PRECL']: res.cnLevels = np.arange(4,80+4,4)
   if var=='LHFLX'                    : res.cnLevels = np.arange(5,205+5,5)
   if var=='P-E'                      : res.cnLevels = np.linspace(-10,10,21)
   if var=='RH'                       : res.cnLevels = np.arange(10,100+1,1)
   if var=='TS'                       : res.cnLevels = np.arange(-55,35+2,2)
   if var=='TS'                       : res.cnLevels = np.arange(22,32+0.2,0.2)
   if var=='PS'                       : res.cnLevels = np.arange(940,1040+1,1)*1e2
   if var in ['TGCLDIWP','TGPRCIWP']  : res.cnLevels = np.arange(1,30+1,1)*1e-2
   if var=='QTRANS'                   : res.cnLevels = np.arange(20,400+20,20)
   if hasattr(res,'cnLevels'): res.cnLevelSelectionMode = 'ExplicitLevels'
   # # set non-explicit contour levels  
   # if not hasattr(res,'cnLevels'):
   #    nlev = 21
   #    aboutZero = False
   #    if var in ['U','V'] or var[0] in ['U','V']: aboutZero = True
   #    clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, returnLevels=False, aboutZero=aboutZero )
   #    if clev_tup==None: res.cnLevelSelectionMode = 'AutomaticLevels'
   #    if clev_tup!=None: 
   #       cmin,cmax,cint = clev_tup
   #       res.cnLevels = np.linspace(cmin,cmax,num=nlev)
   #       res.cnLevelSelectionMode = 'ExplicitLevels'
#---------------------------------------------------------------------------------------------------
png_file_list = ''
fig_type = 'png'
for t in time_list:
   fig_file = gif_file+'.'+str(t).zfill(3)
   tfile_name = f'{fig_file}.{fig_type}'
   tfile_name = tfile_name.replace((os.getenv('PWD')+'/'),'')
   png_file_list += f'  {tfile_name}'
   print(f'  t: {t:3d}     {tfile_name}')
   wkres = ngl.Resources()
   npix = 1024; wkres.wkWidth,wkres.wkHeight=npix,npix
   # npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
   wks = ngl.open_wks(fig_type,fig_file,wkres)
   plot = [None]*(num_var*num_case)
   if create_png :
      for v in range(num_var):
         print(' '*2+f'var: {hc.tclr.MAGENTA}{var[v]}{hc.tclr.END}')
         data_list = []
         time_list = []
         for c in range(num_case):
            print(' '*4+f'case: {hc.tclr.GREEN}{case[c]}{hc.tclr.END}')
            #-------------------------------------------------------------------
            file_path = f'{case_root[c]}/{case[c]}/{case_sub[c]}/*.eam.{htype}.*'
            file_list = sorted(glob.glob(file_path))
            data_ds = xr.open_mfdataset( file_list )
            # data = data_ds[var[v]]
            #-------------------------------------------------------------------
            tvar = var[v]
            if var[v]=='QTRANS': tvar = 'TUQ'
            data = data_ds[tvar]
            if var[v]=='QTRANS':  data = np.sqrt( np.square(data) + np.square(data_ds['TVQ']) )
            #-------------------------------------------------------------------
            with warnings.catch_warnings():
               warnings.simplefilter("ignore", category=RuntimeWarning)
               data['time'] = data.indexes['time'].to_datetimeindex()
            data = data.sel(time=slice(plot_start_date,None))
            data = data.isel(time=t)
            data_list.append( np.ma.masked_invalid( data.values ) )
            #-------------------------------------------------------------------
            time_list.append( pd.to_datetime(data['time'].values).strftime('%Y-%m-%d %HZ') )
         #----------------------------------------------------------------------
         tres = copy.deepcopy(res)
         set_color_palette(var[v],tres)
         set_color_levels(var[v],tres)
         for c in range(num_case):
            grid_ds = xr.open_dataset( scrip_file_list[c] ).rename({'grid_size':'ncol'})
            tres.cnFillMode    = 'CellFill'
            tres.sfXArray      = grid_ds['grid_center_lon'].values
            tres.sfYArray      = grid_ds['grid_center_lat'].values
            tres.sfXCellBounds = grid_ds['grid_corner_lon'].values
            tres.sfYCellBounds = grid_ds['grid_corner_lat'].values
            #-------------------------------------------------------------------
            ip = v*num_case+c if var_x_case else c*num_var+v
            plot[ip] = ngl.contour_map( wks, data_list[c], tres ) 
            hs.set_subtitles(wks, plot[ip], name[c], '', var_str[v], font_height=subtitle_font_height)
            #-------------------------------------------------------------------
            strx = ngl.get_float(plot[ip],"trXMinF")
            stry = ngl.get_float(plot[ip],"trYMinF")
            ttres = ngl.Resources() ; ttres.nglDraw = False ; ttres.txFontHeightF  = 0.01
            ttres.txFontColor = 'white' ; ttres.txFont = 'helvetica'
            tx_id = ngl.text(wks, plot[ip], time_list[c], strx, stry, ttres)
            amres = ngl.Resources() ; amres.amJust = 'BottomCenter'
            amres.amParallelPosF = -0.1 ; amres.amOrthogonalPosF = -0.44
            anno_id_c2 = ngl.add_annotation(plot[ip], tx_id, amres)
      #---------------------------------------------------------------------------------------------
      layout = [num_var,num_case] if var_x_case else [num_case,num_var]
      if num_case==1 or num_var==1: layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      pnl_res = hs.setres_panel()
      pnl_res.nglPanelYWhiteSpacePercent = 5
      if use_common_label_bar: pnl_res.nglPanelLabelBar = True
      ngl.panel(wks,plot,layout,pnl_res)
      ngl.destroy(wks)
      hc.trim_png(fig_file)

if create_png : ngl.end()
#---------------------------------------------------------------------------------------------------
if create_gif :
   cmd = f'convert -repage 0x0 -delay 10 -loop 0  {png_file_list} {gif_file}.gif'
   print('\n'+cmd+'\n')
   os.system(cmd)
   print('\n  '+gif_file+'.gif'+'\n')

#---------------------------------------------------------------------------------------------------
