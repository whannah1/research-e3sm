# v2 is a cleaned up version of v1 - functionality is mostly the same
import os, glob, ngl, subprocess as sp, numpy as np, xarray as xr, dask, copy, string, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import warnings
#-------------------------------------------------------------------------------
var,lev_list,var_str = [],[],[]
def add_var(var_name,lev=0,s=None): 
   var.append(var_name); lev_list.append(lev); 
   var_str.append(var_name if s is None else s)
#-------------------------------------------------------------------------------
fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_nimbus/nimbus.initial_condition.map.v1','png'
#-------------------------------------------------------------------------------

file_list = []
year_list = []

# scrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'

init_scratch = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_init'

# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2025.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2030.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2035.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2040.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2045.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2050.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2055.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2060.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2065.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2070.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2075.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2080.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2085.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2090.nc')
# file_list.append(f'{init_scratch}/HICCUP.atm_era5.2022-05-14.2024-nimbus-iraq-32x3.L80.PGW.2100.nc')


# file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.alt.nc');        year_list.append(2022)

file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.nc');            year_list.append(2022)
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2025.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2030.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2035.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2040.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2045.nc')
file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2050.nc');   year_list.append(2050)
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2055.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2060.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2065.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2070.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2075.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2080.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2085.nc')
# # file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2090.nc')
file_list.append(f'{init_scratch}/HICCUP.sst_noaa.2022-05-14.PGW.2100.nc');   year_list.append(2100)

#-------------------------------------------------------------------------------

add_var('SST_cpl',s='SST')
# add_var('TMQ')
# add_var('TGCLDLWP',s='Liq Water Path')
# add_var('TGCLDIWP',s='Ice Water Path')

#-------------------------------------------------------------------------------
# lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = 0,60,20,80
# lat1,lat2,lon1,lon2 = 12,38,35,58
# lat1,lat2,lon1,lon2 = 10,40,30,60
# lat1,lat2,lon1,lon2 = 12,50,20,70
#-------------------------------------------------------------------------------

plot_diff,add_diff = True,False

print_stats          = True
var_x_case           = False

num_plot_col         = 1#len(var)

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_file = len(var),len(file_list)


diff_base = 0

if 'scrip_file_path' not in locals(): scrip_file_path = None

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*num_file)
if plot_diff and add_diff: plot = [None]*(num_var*(num_file*2-1))

res = hs.res_contour_fill_map()
if 'lat1' in locals() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in locals() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.lbLabelFontHeightF           = 0.015
res.tmXBOn                       = False
res.tmYLOn                       = False
res.mpLimitMode                  = 'LatLon'
# res.mpGeophysicalLineColor       = 'white'
res.mpGeophysicalLineColor       = 'black'
# res.mpProjection = 'Mollweide'

res.mpProjection              = 'Robinson'
res.mpOutlineBoundarySets     = 'NoBoundaries'
res.mpGridAndLimbOn           = False
res.mpPerimOn                 = False
# 
# res.mpMinLatF,res.mpMaxLatF   =  0, 60
# res.mpMinLonF,res.mpMaxLonF   =  0, 90
res.mpCenterLonF          = 0


res.cnMissingValFillColor = 'gray'

# res.cnFillPalette = 'BlueWhiteOrangeRed'
# res.cnFillPalette = "MPL_viridis"
# res.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
# res.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
res.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
# res.cnFillPalette = "CBR_wet"
# res.cnFillPalette = 'BlueWhiteOrangeRed'
# res.cnFillPalette = 'WhiteBlueGreenYellowRed'

# cmap = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
# gray = [0.1,0.1,0.1,1.]
# cmap[0] = gray
# cmap[1] = gray
# cmap[-1] = gray
# # print()
# # print(cmap)
# # exit()
# res.cnFillPalette = cmap

#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print(' '*2+f'var: {hc.tclr.MAGENTA}{var[v]}{hc.tclr.END}')
   data_list = []
   for f in range(num_file):
      print(' '*4+f'file: {hc.tclr.GREEN}{file_list[f]}{hc.tclr.END}')
      #-------------------------------------------------------------------------
      # read the data 
      data_ds = xr.open_dataset( file_list[f] )
      data = data_ds[var[v]].isel(time=0)

      #-------------------------------------------------------------------------
      if var[v] in 'SST_cpl':
         # alt_ds = xr.open_dataset( '/p/lustre1/hannah6/2024-nimbus-iraq-data/init_data_raw/sst.day.mean.2022.nc' )
         if v==0 and f==0:
            alt_ds = xr.open_dataset( '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_init/HICCUP.sst_noaa.2022-05-14.alt.nc' )
            sst_raw = alt_ds['SST_cpl'].isel(time=0)
         # hc.print_stat(data,name=var[v],stat='naxsh',indent='   '*4,compact=True)
         # data = xr.where(np.isfinite(sst_raw),data,np.nan)
         data = xr.where( np.fabs(sst_raw.values) < 999, data, np.nan)
         # hc.print_stat(data,name=var[v],stat='naxsh',indent='   '*4,compact=True)
         # exit()
      #-------------------------------------------------------------------------
      # print()
      # print(data)
      # print()
      # exit()
      #-------------------------------------------------------------------------
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='   '*4,compact=True)
      #-------------------------------------------------------------------------
      # setup plot stuff for this variable
      tres = copy.deepcopy(res)
      #-------------------------------------------------------------------------
      
      #-------------------------------------------------------------------------
      tres.cnFillMode    = 'RasterFill'
      tres.sfXArray      = data['lon'].values
      tres.sfYArray      = data['lat'].values
      #-------------------------------------------------------------------------
      num_file_alt = num_file*2-1 if (plot_diff and add_diff) else num_file
      ip = v*num_file_alt+f if var_x_case else f*num_var+v
      #-------------------------------------------------------------------------
      tres.cnLevelSelectionMode = 'ExplicitLevels'
      if f==0:
         baseline = data.values
         if var[v]=='SST_cpl': tres.cnLevels = np.arange(0,36+2,2)
      else:
         data = data - baseline
         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         if var[v]=='SST_cpl': mag,nlev=5,10; tres.cnLevels = np.arange(-1*mag,mag+mag/nlev,mag/nlev)
      #-------------------------------------------------------------------------
      plot[ip] = ngl.contour_map( wks, np.ma.masked_invalid( data.values ), tres ) 

      if f==0:
         var_str_alt = var_str[v]
      else:
         var_str_alt = var_str[v] + ' diff'

      hs.set_subtitles(wks, plot[ip], var_str_alt, '', f'year: {year_list[f]}', font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot

if num_file==1 or num_var==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [num_var,num_file] if var_x_case else [num_file,num_var]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
