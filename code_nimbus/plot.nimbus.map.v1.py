# v2 is a cleaned up version of v1 - functionality is mostly the same
import os, glob, ngl, subprocess as sp, numpy as np, xarray as xr, dask, copy, string, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import warnings
#-------------------------------------------------------------------------------
name,case,case_root,case_sub = [],[],[],[]
scrip_file_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,c='black',d=0,scrip_file=None):
   global name,case,case_root,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); 
   case_root.append(p); case_sub.append(s);
   scrip_file_list.append(scrip_file)
#-------------------------------------------------------------------------------
var,lev_list,var_str = [],[],[]
def add_var(var_name,lev=0,s=None): 
   var.append(var_name); lev_list.append(lev); 
   var_str.append(var_name if s is None else s)
#-------------------------------------------------------------------------------
fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_nimbus/nimbus.map.v1','png'
#-------------------------------------------------------------------------------

troot,tsub = '/p/lustre1/hannah6/e3sm_scratch','run'

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-128x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-14.NN_512.DTP_60.DTD_6.NDG_OFF.PGW_OFF',   n='128x3 NDG_OFF',p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-14.NN_512.DTP_60.DTD_6.NDG_ON.PGW_OFF',    n='128x3 NDG_ON', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-16.NN_512.DTP_60.DTD_6.NDG_OFF.PGW_OFF',   n='128x3 NDG_OFF',p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-16.NN_512.DTP_60.DTD_6.NDG_ON.PGW_OFF',    n='128x3 NDG_ON', p=troot,s=tsub,scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   n='32x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    n='32x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   n='32x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    n='32x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-64x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-14.NN_128.DTP_120.DTD_15.NDG_OFF.PGW_OFF',  n='64x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-14.NN_128.DTP_120.DTD_15.NDG_ON.PGW_OFF',   n='64x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-16.NN_128.DTP_120.DTD_15.NDG_OFF.PGW_OFF',  n='64x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.64x3-pg2.F2010.2022-05-16.NN_128.DTP_120.DTD_15.NDG_ON.PGW_OFF',   n='64x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   d=0, c='red',  n='32x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='red',  n='32x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   d=0, c='blue', n='32x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='blue', n='32x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)

tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=0, c='red',  n='32x3 CTRL', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2050',   d=0, c='blue', n='32x3 2050', p=troot,s=tsub,scrip_file=tscrip_file)

add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='black',  n='32x3 CTRL', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2035',   d=0, c='blue',   n='32x3 2035', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2075',   d=0, c='green',  n='32x3 2075', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2100',   d=0, c='red',    n='32x3 2100', p=troot,s=tsub,scrip_file=tscrip_file)

# add_case('E3SM.2024-NIMBUS-IRAQ-03.32x3-pg2.F2010.2022-05-14.NDG_TAU_12.PGW_OFF',    d=1, c='black',  n='32x3 NDG_TAU=12', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-03.32x3-pg2.F2010.2022-05-14.NDG_TAU_24.PGW_OFF',    d=1, c='black',  n='32x3 NDG_TAU=24', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-03.32x3-pg2.F2010.2022-05-14.NDG_TAU_48.PGW_OFF',    d=1, c='black',  n='32x3 NDG_TAU=48', p=troot,s=tsub,scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('CAMS',n='CAMS',p='/p/lustre1/hannah6/2024-nimbus-iraq-data/files_analysis_32x3',s='',scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/scrip_CAMS_241x480.nc'
# add_case('CAMS',n='CAMS',p='/p/lustre1/hannah6/2024-nimbus-iraq-data/files_analysis',s='',scrip_file=tscrip_file)

#-------------------------------------------------------------------------------

# add_var('PRECT',s='Precipitation')
# add_var('TMQ')
# add_var('TGCLDLWP',s='Liq Water Path')
# add_var('TGCLDIWP',s='Ice Water Path')

# add_var('FSNT')
# add_var('FLNT')

# add_var('TUQ')
# add_var('TVQ')
# add_var('QTRANS')

# add_var('TS')
# add_var('SHFLX')

# add_var('WSPDBOT')

# add_var('T850')

# add_var('AODVIS')
# add_var('AODDUST')
# add_var('AODDUST1')
# add_var('AODDUST2')
# add_var('AODDUST3')
# add_var('AODDUST4')
# add_var('BURDENDUST')
# add_var('ABURDENDUST')

# add_var('dst_a3',lev=850)


#-------------------------------------------------------------------------------
# lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = 0,60,20,80
# lat1,lat2,lon1,lon2 = 12,38,35,58
# lat1,lat2,lon1,lon2 = 10,40,30,60
lat1,lat2,lon1,lon2 = 12,50,20,70
#-------------------------------------------------------------------------------

slice_val = 0
time_opt = 'slice' # mean / slice
# slice_val = 12#0
#0

# htype,plot_date = 'h2','2022-05-17'; slice_val = 0
htype,plot_date = 'h1','2022-05-16'; slice_val = 0
# htype,plot_date = 'h1','2022-05-24' ; slice_val = 12
# htype,plot_date = 'h1','2022-05-25'

# htype,first_file,num_files = 'h1',0,1


use_remap,remap_grid = False,'90x180'

plot_diff,add_diff = False,False

print_stats          = True
var_x_case           = True

num_plot_col         = 2#len(var)

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if case==[]: raise ValueError('ERROR - case list is empty!')
num_var,num_case = len(var),len(case)

if 'subtitle_font_height' not in locals(): subtitle_font_height = 0.008

diff_base = 0

if 'scrip_file_path' not in locals(): scrip_file_path = None

wkres = ngl.Resources()
npix = 2048*2; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*num_case)
if plot_diff and add_diff: plot = [None]*(num_var*(num_case*2-1))

res = hs.res_contour_fill_map()
if 'lat1' in locals() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in locals() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.lbLabelFontHeightF           = 0.015
res.tmXBOn                       = False
res.tmYLOn                       = False
res.mpLimitMode                  = 'LatLon'
res.mpGeophysicalLineColor       = 'white'
# res.mpProjection = 'Mollweide'

# res.mpProjection              = 'Orthographic'
# # res.mpOutlineBoundarySets     = 'NoBoundaries'
# res.mpGridAndLimbOn           = False
# res.mpPerimOn                 = False
# 
# res.mpMinLatF,res.mpMaxLatF   =  0, 60
# res.mpMinLonF,res.mpMaxLonF   =  0, 90
# res.mpCenterLonF          = 0

#---------------------------------------------------------------------------------------------------
def set_color_palette(var,res):
   res.cnFillPalette = "MPL_viridis"
   # res.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
   # res.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
   if var in ['P-E']                      : res.cnFillPalette = "BlueWhiteOrangeRed"
   if var in ['CLDLOW','CLDMED','CLDHGH'] : res.cnFillPalette = "CBR_wet"
   if var in ['TGCLDLWP','TGCLDIWP']      : res.cnFillPalette = "MPL_viridis"
   if var in ['DYN_QLIQ']                 : res.cnFillPalette = "MPL_viridis"
   # if var in ['TS','PS']                  : res.cnFillPalette = 'BlueWhiteOrangeRed'
   # if var in ['TS','PS']                  : res.cnFillPalette = 'WhiteBlueGreenYellowRed'
   # if var in ['TS','PS']                  : res.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
   if var in ['U','V'] or var[0] in ['U','V']:
      res.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
   return
#---------------------------------------------------------------------------------------------------
def set_color_levels(var,res):
   if var=='BURDENDUST'               : res.cnLevels = np.linspace(1e-4,20e-4,20)
   if var=='AODDUST'                  : res.cnLevels = np.linspace(0.01,0.81,8*2)
   # if var=='AODDUST'                  : res.cnLevels = np.logspace( np.log10(0.01), np.log10(1.), num=20).round(decimals=4)
   if var in ['PRECT','PRECC','PRECL']: res.cnLevels = np.arange(4,80+4,4)
   if var=='LHFLX'                    : res.cnLevels = np.arange(5,205+5,5)
   if var=='P-E'                      : res.cnLevels = np.linspace(-10,10,21)
   if var=='RH'                       : res.cnLevels = np.arange(10,100+1,1)
   # if var=='TS'                       : res.cnLevels = np.arange(-55,35+2,2)
   # if var=='TS'                       : res.cnLevels = np.arange(280,310+2,2)
   # if var=='TS'                       : res.cnLevels = np.arange(22,32+0.2,0.2)
   if var=='PS'                       : res.cnLevels = np.arange(940,1040+1,1)*1e2
   if var in ['TGCLDIWP','TGPRCIWP']  : res.cnLevels = np.arange(1,30+1,1)*1e-2
   if var=='QTRANS'                   : res.cnLevels = np.arange(10,400+10,10)
   # set non-explicit contour levels  
   if hasattr(res,'cnLevels'): res.cnLevelSelectionMode = 'ExplicitLevels'
   if not hasattr(res,'cnLevels'):
      nlev = 21
      aboutZero = False
      if var in ['U','V'] or var[0] in ['U','V']: aboutZero = True
      clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, returnLevels=False, aboutZero=aboutZero )
      if clev_tup==None: res.cnLevelSelectionMode = 'AutomaticLevels'
      if clev_tup!=None: 
         cmin,cmax,cint = clev_tup
         res.cnLevels = np.linspace(cmin,cmax,num=nlev)
         res.cnLevelSelectionMode = 'ExplicitLevels'
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print(' '*2+f'var: {hc.tclr.MAGENTA}{var[v]}{hc.tclr.END}')
   data_list = []
   for c in range(num_case):
      print(' '*4+f'case: {hc.tclr.GREEN}{case[c]}{hc.tclr.END}')
      #-------------------------------------------------------------------------
      tvar = var[v]
      if case[c]=='CAMS':
         if var[v]=='BURDENDUST': tvar = 'aermr04'
         if var[v]=='dst_a3'    : tvar = 'aermr06'
      else:
         if var[v]=='QTRANS'    : tvar = 'TUQ'
         if var[v]=='WSPDBOT'   : tvar = 'UBOT'
      #-------------------------------------------------------------------------
      # read the data 
      if case[c]=='CAMS':
         file_path = f'{case_root[c]}/CAMS.atm.*.nc'
      else:
         file_path = f'{case_root[c]}/{case[c]}/{case_sub[c]}/*.eam.{htype}.*'
      file_list = sorted(glob.glob(file_path))
      #-------------------------------------------------------------------------
      data_ds = xr.open_mfdataset( file_list )
      data = data_ds[tvar]
      
      if case[c]=='CAMS': 
         data = data.stack(ncol=('latitude','longitude'))
         data = data.rename({'level':'lev'})
      else:
         if var[v]=='QTRANS' : data = np.sqrt( np.square(data) + np.square(data_ds['TVQ']) )
         if var[v]=='WSPDBOT': data = np.sqrt( np.square(data) + np.square(data_ds['VBOT']) )
      #-------------------------------------------------------------------------
      if lev_list[v] is not None and 'lev' in data.dims:
         data = data.sel(lev=lev_list[v],method='nearest')
      #-------------------------------------------------------------------------
      # print()
      # print(data)
      # print()
      # exit()
      #-------------------------------------------------------------------------
      if case[c]=='CAMS' and var[v]=='BURDENDUST': data += data_ds['aermr05'] + data_ds['aermr06']

      if var[v]=='dst_a3': data = data*1e3
      #-------------------------------------------------------------------------
      if case[c]!='CAMS':
         with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            data['time'] = data.indexes['time'].to_datetimeindex()

      data = data.sel(time=plot_date)
      if time_opt=='slice': 
         if case[c]=='CAMS': data = data.isel(time=int(slice_val/3-1))
         if case[c]!='CAMS': data = data.isel(time=slice_val)
      if time_opt=='mean' : data = data.mean(dim='time')

      data_list.append( np.ma.masked_invalid( data.values ) )

      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='   '*4,compact=True)
   #----------------------------------------------------------------------------
   # calculate common limits for consistent contour levels
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   #----------------------------------------------------------------------------
   # setup plot stuff for this variable
   tres = copy.deepcopy(res)
   set_color_palette(var[v],tres)
   set_color_levels(var[v],tres)
   #----------------------------------------------------------------------------
   # Create plot panels for this variable
   for c in range(num_case):
      # tres.lbLabelBarOn = False if use_common_label_bar else True
      #-------------------------------------------------------------------------
      grid_ds = xr.open_dataset( scrip_file_list[c] ).rename({'grid_size':'ncol'})
      tres.cnFillMode    = 'CellFill'
      tres.sfXArray      = grid_ds['grid_center_lon'].values
      tres.sfYArray      = grid_ds['grid_center_lat'].values
      tres.sfXCellBounds = grid_ds['grid_corner_lon'].values
      tres.sfYCellBounds = grid_ds['grid_corner_lat'].values
      #-------------------------------------------------------------------------
      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v
      #-------------------------------------------------------------------------
      plot[ip] = ngl.contour_map( wks, data_list[c], tres ) 

      hs.set_subtitles(wks, plot[ip], name[c], var_str[v], plot_date, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot

if num_case==1 or num_var==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [num_var,num_case] if var_x_case else [num_case,num_var]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
