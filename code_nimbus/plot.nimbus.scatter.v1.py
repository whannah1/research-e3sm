# v2 is a cleaned up version of v1 - functionality is mostly the same
import os, glob, datetime, ngl, subprocess as sp, numpy as np, xarray as xr, dask, copy, string, cmocean,numba
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import warnings
#-------------------------------------------------------------------------------
case,case_name,case_root,case_sub = [],[],[],[]
clr,dsh = [],[]
scrip_file_list = []
mrk = []
def add_case(case_in,n=None,p=None,s=None,c='black',d=0,m=16,scrip_file=None):
   global name,case,case_root,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); case_name.append(tmp_name); 
   case_root.append(p); case_sub.append(s);
   clr.append(c); dsh.append(d)
   scrip_file_list.append(scrip_file)
   mrk.append(m)
#-------------------------------------------------------------------------------
xvar_list     = []
yvar_list     = []
xvar_str_list = []
yvar_str_list = []
def add_var(xvar, yvar, xvar_str, yvar_str): 
   xvar_list.append(xvar)
   yvar_list.append(yvar)
   xvar_str_list.append(xvar_str)
   yvar_str_list.append(yvar_str)
#-------------------------------------------------------------------------------
fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_nimbus/nimbus.scatter.v1','png'
#-------------------------------------------------------------------------------

troot,tsub = '/p/lustre1/hannah6/e3sm_scratch','run'

tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=0, c='blue',n='32x3 CTRL', p=troot,s=tsub,scrip_file=tscrip_file)
add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2050',   d=0, c='red', n='32x3 2050', p=troot,s=tsub,scrip_file=tscrip_file)
#-------------------------------------------------------------------------------

add_var(xvar='BURDENDUST',yvar='FLDS',xvar_str='BURDENDUST',yvar_str='FLDS')

#-------------------------------------------------------------------------------
# lat1,lat2,lon1,lon2 = 0,60,20,80
lat1,lat2,lon1,lon2 = 10,40,30,60
# lat1,lat2,lon1,lon2 = 0,50,20,70
#-------------------------------------------------------------------------------

htype,first_file,num_files = 'h1',0,1

# integrate_vert = True
daily_mean     = False
# recalculate    = True

# temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)
num_var = len(xvar_list)

# if integrate_vert:
#    # lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
#    # lev = np.array([100,150,200,300,400,500,600,700,800,850,900,950,1000])
#    lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_var

res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 12
res.xyMarkLineMode = 'Markers'
# res.xyMarkLineMode = 'MarkLines'
res.xyMarker = 1
res.xyDashPattern = 2
# res.xyMarkerSizeF = 0.015
res.xyMarkerSizeF = 0.001
# res.xyXStyle = "Log"
# res.xyYStyle = "Log"
# res.xyLineColors   = clr

lres = hs.res_xy()
lres.xyDashPattern    = 0
lres.xyLineThicknessF = 1
lres.xyLineColor      = "black"

#---------------------------------------------------------------------------------------------------
# routine for calculating time derivative
#---------------------------------------------------------------------------------------------------
@numba.njit()
def ddt(data_in,ntime,ncol,dt,data_out,method=1) :
   for t in range(1,ntime):
      # data_out[t,:] = ( data_in[t+1,:] - data_in[t-1,:] ) / dt
      if method==0: data_out[t] = ( data_in[t+1] - data_in[t-1] ) / (dt*2)
      if method==1: data_out[t] = ( data_in[t] - data_in[t-1] ) / (dt)

@numba.njit()
def hybrid_dp3d( PS, P0, hyai, hybi, dp3d ):
   nlev = len(hyai)-1
   (ntime,ncol) = PS.shape
   # dp3d = np.zeros([ntime,nlev,ncol])
   for t in range(ntime):
      for i in range(ncol):
         for k in range(nlev):
            p1 = hyai[k  ]*P0 + hybi[k  ]*PS[t,i]
            p2 = hyai[k+1]*P0 + hybi[k+1]*PS[t,i]
            dp3d[t,k,i] = p2 - p1
   # return dp3d

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
x_list = []
y_list = []

for v in range(num_var):

   for c in range(num_case):

      print(' '*4+'case: '+case[c])

      # tmp_file = temp_dir+"/clim.scatter.v1."+case[c]+"."+var+".lat1_"+str(lat1)+".lat2_"+str(lat2)+".nc"
      # print('    tmp_file: '+tmp_file )

      # if recalculate :
      if True:
         #-------------------------------------------------------------------------
         # read the data
         #-------------------------------------------------------------------------
         # case_obj.lat1, case_obj.lat1 = lat1, lat1
         # case_obj.lon2, case_obj.lon2 = lon2, lon2
         # area = case_obj.load_data('area',htype=htype,num_files=1).astype(np.float)
         # X = case_obj.load_data(xvar_list[v],htype=htype,first_file=first_file,num_files=num_files)
         # Y = case_obj.load_data(yvar_list[v],htype=htype,first_file=first_file,num_files=num_files)

         #-------------------------------------------------------------------------
         file_path = f'{case_root[c]}/{case[c]}/{case_sub[c]}/*.eam.{htype}.*'
         file_list = sorted(glob.glob(file_path))
         # for f in file_list: print(f)
         if 'first_file' in locals(): file_list = file_list[first_file:]
         if 'num_files'  in locals(): file_list = file_list[:num_files]
         #-------------------------------------------------------------------------
         data_ds = xr.open_mfdataset( file_list )
         # area = data_ds['area']
         x_da = data_ds[xvar_list[v]]
         y_da = data_ds[yvar_list[v]]
         #-------------------------------------------------------------------------
         grid_ds = xr.open_dataset( scrip_file_list[c] ).rename({'grid_size':'ncol'})
         scrip_lat_name,scrip_lon_name = 'grid_center_lat','grid_center_lon'
         mask = xr.DataArray( np.ones(len(grid_ds[scrip_lat_name]),dtype=bool), coords=grid_ds[scrip_lat_name].coords )
         if 'lat1' in locals(): mask = mask & (grid_ds[scrip_lat_name]>=lat1) & (grid_ds[scrip_lat_name]<=lat2)
         if 'lon1' in locals(): mask = mask & (grid_ds[scrip_lon_name]>=lon1) & (grid_ds[scrip_lon_name]<=lon2)
         
         # area = area.where(mask,drop=True)
         x_da = x_da.where(mask,drop=True)
         y_da = y_da.where(mask,drop=True)


         #-------------------------------------------------------------------------
         # if integrate_vert:
         #    ps = case_obj.load_data('PS',htype=htype,first_file=first_file,num_files=num_files)
         #    # dp3d = hc.calc_dp3d(ps,Y['lev'])
            
         #    hyai = case_obj.load_data('hyai',htype=htype,num_files=1)
         #    hybi = case_obj.load_data('hybi',htype=htype,num_files=1)
         #    # , len(Y['time']), len(Y['ncol']), len(Y['lev'])+1 
         #    # dp3d = hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values )
         #    dp3d = np.zeros( Y.shape )
         #    hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values, dp3d )
         #    dp3d = xr.DataArray(dp3d,dims=Y.dims)
            
         #    X = ( X * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')
         #    Y = ( Y * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')
         #-------------------------------------------------------------------------
         # # area average
         # X = ( (X*area).sum(dim='ncol') / area.sum(dim='ncol') )
         # Y = ( (Y*area).sum(dim='ncol') / area.sum(dim='ncol') )
         #-------------------------------------------------------------------------
         # Convert to daily mean
         if htype=='h1' and daily_mean: 
            x_da = x_da.resample(time='D').mean(dim='time')
            y_da = y_da.resample(time='D').mean(dim='time')

         # combine columns into single dimension
         if 'ncol' in x_da.dims: x_da.stack( allcols=('ncol','time') )
         if 'ncol' in y_da.dims: y_da.stack( allcols=('ncol','time') )      

         hc.print_stat(x_da,name=xvar_list[v],stat='naxs',compact=True)
         hc.print_stat(y_da,name=yvar_list[v],stat='naxs',compact=True)
         print()

         #-------------------------------------------------------------------------
         # Calculate the mean and variance at each point
         #-------------------------------------------------------------------------
         # dims   = ('ncol')
         # stat_ds = xr.Dataset()
         # stat_ds['avg'] = (dims, X.mean(dim='time') )
         # stat_ds['std'] = (dims, X.std(dim='time') )
         # stat_ds['var'] = (dims, X.var(dim='time') )
         # # stat_ds['skw'] = (dims, skew(X,axis=0,nan_policy='omit') )
         # stat_ds.coords['ncol'] = ('ncol',X.coords['ncol'])

         #-------------------------------------------------------------------------
         # Write to file 
         #-------------------------------------------------------------------------
      #    stat_ds.to_netcdf(path=tmp_file,mode='w')
      # else:
      #    stat_ds = xr.open_mfdataset( tmp_file )

      x_list.append(x_da)
      y_list.append(y_da)

   #-------------------------------------------------------------------------------
   #-------------------------------------------------------------------------------

   x_min = np.min([np.nanmin(d) for d in x_list])
   x_max = np.max([np.nanmax(d) for d in x_list])

   y_min = np.min([np.nanmin(d) for d in y_list])
   y_max = np.max([np.nanmax(d) for d in y_list])

   x_mag = np.abs(x_max-x_min)
   y_mag = np.abs(x_max-x_min)
   res.trXMinF = x_min - x_mag*0.02
   res.trXMaxF = x_max + x_mag*0.02
   res.trYMinF = y_min - y_mag*0.02
   res.trYMaxF = y_max + y_mag*0.02

   for c in range(num_case):

      # res.xyExplicitLegendLabels = case_name
      # res.pmLegendDisplayMode    = "Always"
      # res.pmLegendOrthogonalPosF = -1.13
      # res.pmLegendParallelPosF   =  0.8+0.04
      # res.pmLegendWidthF         =  0.16
      # res.pmLegendHeightF        =  0.12   
      # res.lgBoxMinorExtentF      =  0.16   

      res.tiXAxisString = xvar_str_list[v]
      res.tiYAxisString = yvar_str_list[v]
      # res.xyLineColor   = clr[c]
      res.xyMarkerColor = clr[c]
      res.xyMarker      = mrk[c]

      px = np.ma.masked_invalid( x_list[c].stack().values )
      py = np.ma.masked_invalid( y_list[c].stack().values )
      
      tplot = ngl.xy(wks, px, py, res)
      
      if c==0:
         plot[v] = tplot
      else:
         ngl.overlay( plot[v], tplot )
      
      #----------------------------------------
      # add linear fit

      # # simple and fast method for regression coeff
      # a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )

      # # intercept value
      # b = np.nanmean(py) - a*np.nanmean(px)

      # print()
      # print(f'    case: {case[c]}')
      # print(f'    linear regression a: {a}    b: {b}\n')

      # tres = copy.deepcopy(lres)
      # tres.xyLineColor      = clr[c]
      # tres.xyDashPattern    = dsh[c]
      # tres.xyLineThicknessF = 6

      # px_range = np.abs( np.max(px) - np.min(px) )
      # lx = np.array([-1e2*px_range,1e2*px_range])

      # tplot = ngl.xy(wks, lx, lx*a+b , tres)
      # ngl.overlay( plot[len(plot)-1], tplot )

      #----------------------------------------
      # Add horizontal lines
      ngl.overlay( plot[len(plot)-1], ngl.xy(wks,[0,0],[-1e8,1e8],lres) )
      ngl.overlay( plot[len(plot)-1], ngl.xy(wks,[-1e8,1e8],[0,0],lres) )

   # hs.set_subtitles(wks, plot[v], '', '', '', font_height=0.015)


#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

# res.xyExplicitLegendLabels = case_name
# res.pmLegendDisplayMode    = "Always"
# res.pmLegendOrthogonalPosF = -1.13
# res.pmLegendParallelPosF   =  0.8+0.04
# res.pmLegendWidthF         =  0.16
# res.pmLegendHeightF        =  0.12   
# res.lgBoxMinorExtentF      =  0.16   

# res.trXMinF = np.min(x_list)
# res.trXMaxF = np.max(x_list)

### plot stddev vs mean
# plot.append( ngl.xy(wks, np.stack(x_list) , np.stack(y_list) ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

# ### plot accumulation
# res.pmLegendDisplayMode = "Never"
# res.tiYAxisString = 'Rain Amount [mm/day]'
# plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(acc_list) ,res) )
# # hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------