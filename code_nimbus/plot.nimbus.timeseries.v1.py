# v2 is a cleaned up version of v1 - functionality is mostly the same
import os, glob, datetime, ngl, subprocess as sp, numpy as np, xarray as xr, dask, copy, string, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import warnings
#-------------------------------------------------------------------------------
case,case_name,case_root,case_sub = [],[],[],[]
clr,dsh = [],[]
scrip_file_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,c='black',d=0,scrip_file=None):
   global name,case,case_root,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); case_name.append(tmp_name); 
   case_root.append(p); case_sub.append(s);
   clr.append(c); dsh.append(d)
   scrip_file_list.append(scrip_file)
#-------------------------------------------------------------------------------
var,lev_list,var_str = [],[],[]
def add_var(var_name,lev=0,s=None): 
   var.append(var_name); lev_list.append(lev); 
   var_str.append(var_name if s is None else s)
#-------------------------------------------------------------------------------
fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_nimbus/nimbus.timeseries.v1','png'
#-------------------------------------------------------------------------------

troot,tsub = '/p/lustre1/hannah6/e3sm_scratch','run'

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-128x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-14.NN_512.DTP_60.DTD_6.NDG_OFF.PGW_OFF',   n='128x3 NDG_OFF',p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-14.NN_512.DTP_60.DTD_6.NDG_ON.PGW_OFF',    n='128x3 NDG_ON', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-16.NN_512.DTP_60.DTD_6.NDG_OFF.PGW_OFF',   n='128x3 NDG_OFF',p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ.128x3-pg2.F2010.2022-05-16.NN_512.DTP_60.DTD_6.NDG_ON.PGW_OFF',    n='128x3 NDG_ON', p=troot,s=tsub,scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ-00.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   d=0, c='red',  n='32x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-00.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='red',  n='32x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-00.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   d=0, c='blue', n='32x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-00.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='blue', n='32x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-64x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ-00.64x3-pg2.F2010.2022-05-14.NN_128.DTP_120.DTD_15.NDG_OFF.PGW_OFF',  d=0, c='green',   n='64x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-00.64x3-pg2.F2010.2022-05-14.NN_128.DTP_120.DTD_15.NDG_ON.PGW_OFF',   d=1, c='green',   n='64x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-00.64x3-pg2.F2010.2022-05-16.NN_128.DTP_120.DTD_15.NDG_OFF.PGW_OFF',  d=0, c='magenta', n='64x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-00.64x3-pg2.F2010.2022-05-16.NN_128.DTP_120.DTD_15.NDG_ON.PGW_OFF',   d=1, c='magenta', n='64x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)

# tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   d=0, c='red',  n='32x3 init:5-14 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='red',  n='32x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_OFF.PGW_OFF',   d=0, c='blue', n='32x3 init:5-16 NDG_OFF', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='blue', n='32x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)

# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=0, c='red',  n='32x3 init:5-14 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-01.32x3-pg2.F2010.2022-05-16.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=0, c='blue', n='32x3 init:5-16 NDG_ON',  p=troot,s=tsub,scrip_file=tscrip_file)

tscrip_file = '/p/lustre1/hannah6/2024-nimbus-iraq-data/files_grid/2024-nimbus-iraq-32x3_pg2_scrip.nc'
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=0, c='blue',  n='32x3 CTRL', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2050',   d=0, c='red', n='32x3 2050', p=troot,s=tsub,scrip_file=tscrip_file)

# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2025',   d=0, c='red',     n='32x3 2025', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2030',   d=0, c='orange',  n='32x3 2030', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2035',   d=0, c='green',   n='32x3 2035', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2040',   d=0, c='blue',    n='32x3 2040', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2045',   d=0, c='purple',  n='32x3 2045', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2050',   d=0, c='red',     n='32x3 2050', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2075',   d=0, c='magenta', n='32x3 2075', p=troot,s=tsub,scrip_file=tscrip_file)
# add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2100',   d=0, c='pink',    n='32x3 2100', p=troot,s=tsub,scrip_file=tscrip_file)

add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_OFF',    d=1, c='black',  n='32x3 CTRL', p=troot,s=tsub,scrip_file=tscrip_file)
add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2035',   d=0, c='blue',   n='32x3 2035', p=troot,s=tsub,scrip_file=tscrip_file)
add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2075',   d=0, c='green',  n='32x3 2075', p=troot,s=tsub,scrip_file=tscrip_file)
add_case('E3SM.2024-NIMBUS-IRAQ-02.32x3-pg2.F2010.2022-05-14.NN_64.DTP_300.DTD_30.NDG_ON.PGW_2100',   d=0, c='red',    n='32x3 2100', p=troot,s=tsub,scrip_file=tscrip_file)

#-------------------------------------------------------------------------------

# add_var('PRECT',s='Precipitation')
# add_var('TMQ')
# add_var('TGCLDLWP',s='Liq Water Path')
# add_var('TGCLDIWP',s='Ice Water Path')

# add_var('TS')
# add_var('T850')
# add_var('V850')
# add_var('T850')

# add_var('AODVIS')
# add_var('AODDUST')
# add_var('AODDUST1')
# add_var('AODDUST2')
# add_var('AODDUST3')
# add_var('AODDUST4')
# add_var('ABURDENDUST')
add_var('BURDENDUST',s='Total Column Dust Burden [kg/m 2]')

# add_var('FSNS')
# add_var('FLDS',s='Longwave Radiation Down [W/m2]')
add_var('TS',s='Surface Temperature')


#-------------------------------------------------------------------------------
reg_str = ''
# lat1,lat2 = -60,60; reg_str = f'{lat1}:{lat2}N'
# lat1,lat2,lon1,lon2 = 0,60,20,80; reg_str = f'{lat1}:{lat2}N {lon1};{lon2}E'
# lat1,lat2,lon1,lon2 = 15,30,40,55; reg_str = f'{lat1}:{lat2}N {lon1};{lon2}E'

# xlat,xlon,dx,dy,reg_str = 23, 46, 1, 1, None

# xlat,xlon,dx,dy,reg_str = 23.96, 52.21, 1, 1, 'Barakah'
# xlat,xlon,dx,dy,reg_str = 24.44, 54.62, 1, 1, 'Abu Dhabi'
# xlat,xlon,dx,dy,reg_str = 30.37, 48.25, 1, 1, 'Abadan Ayatollah Jami'
# xlat,xlon,dx,dy,reg_str = 35.52, 45.45, 1, 1, 'Sulaimaniyah'

if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

if reg_str is None: reg_str = f'{lat1}:{lat2}N {lon1};{lon2}E'
#-------------------------------------------------------------------------------

htype = 'h1'
# htype,first_file,num_files = 'h1',0,1


use_remap,remap_grid = False,'90x180'

plot_diff,add_diff = False,True

convert_to_daily_mean = True

print_stats          = True
var_x_case           = True

num_plot_col         = 1 # len(case)

#-------------------------------------------------------------------------------
# plot legend in separate file
#-------------------------------------------------------------------------------
if len(case)>1:
   # create_legend = True
   legend_file = fig_file+'.legend'
   wkres = ngl.Resources() #; npix = 1024 ; wkres.wkWidth,wkres.wkHeight=npix,npix
   lgd_wks = ngl.open_wks('png',legend_file,wkres)
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.03*len(case)
   lgres.lgLabelFontHeightF = 0.008
   lgres.lgLabelFont        = "courier"
   lgres.lgMonoDashIndex    = False
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 16
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   labels = case_name
   for i in range(len(labels)): labels[i] = ' '*4+labels[i] 

   pid = ngl.legend_ndc(lgd_wks, len(labels), labels, 0.5, 0.65, lgres)

   ngl.frame(lgd_wks)
   hc.trim_png(legend_file)
   # exit()
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if case==[]: raise ValueError('ERROR - case list is empty!')
num_var,num_case = len(var),len(case)

# subtitle_font_height = 0.015
subtitle_font_height = 0.01

diff_base = 0

wkres = ngl.Resources()
# npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*num_var
if plot_diff and add_diff: plot = [None]*(num_var*(num_case*2-1))

res = hs.res_xy()
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.xyLineThicknessF       = 12

#---------------------------------------------------------------------------------------------------
def calculate_obs_area(lon,lat,lon_bnds,lat_bnds):
   re = 6.37122e06  # radius of earth
   nlat,nlon = len(lat),len(lon)
   area = np.empty((nlat,nlon),np.float64)
   for j in range(nlat):
      for i in range(nlon):
         dlon = np.absolute( lon_bnds[j,1] - lon_bnds[j,0] )
         dlat = np.absolute( lat_bnds[j,1] - lat_bnds[j,0] )
         dx = re*dlon*np.pi/180.
         dy = re*dlat*np.pi/180.
         area[j,i] = dx*dy
   return area
#---------------------------------------------------------------------------------------------------
def get_time_coord(time):
   time_coord = time.dt.dayofyear.values + time.dt.hour.values / 24.
   return np.array(time_coord)
#---------------------------------------------------------------------------------------------------
def convert_time_to_date(time):
   ntime  = len(time)
   # years  = time.dt.year.values ; months = time.dt.month.values ; days   = time.dt.day.values
   # date_labels = [datetime.date(years[i],months[i],days[i]) for i in range(0,ntime)]
   date_labels = [ t.strftime("%Y-%m-%d") for t in time.values ]
   # date_labels = np.array(date_labels,dtype='str')
   return(date_labels)
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print(' '*2+f'var: {hc.tclr.MAGENTA}{var[v]}{hc.tclr.END}')
   data_list = []
   time_list = []
   date_list = []
   for c in range(num_case):
      print(' '*4+f'case: {hc.tclr.GREEN}{case[c]}{hc.tclr.END}')
      #-------------------------------------------------------------------------
      file_path = f'{case_root[c]}/{case[c]}/{case_sub[c]}/*.eam.{htype}.*'
      file_list = sorted(glob.glob(file_path))
      # for f in file_list: print(f)
      #-------------------------------------------------------------------------
      data_ds = xr.open_mfdataset( file_list )
      # print() ; print(data_ds) ; print() ; exit()
      area = data_ds['area']
      data = data_ds[var[v]]

      # print() ; print(data) ; print() ; exit()
      #-------------------------------------------------------------------------
      grid_ds = xr.open_dataset( scrip_file_list[c] ).rename({'grid_size':'ncol'})
      scrip_lat_name,scrip_lon_name = 'grid_center_lat','grid_center_lon'
      mask = xr.DataArray( np.ones(len(grid_ds[scrip_lat_name]),dtype=bool), coords=grid_ds[scrip_lat_name].coords )
      if 'lat1' in locals(): mask = mask & (grid_ds[scrip_lat_name]>=lat1) & (grid_ds[scrip_lat_name]<=lat2)
      if 'lon1' in locals(): mask = mask & (grid_ds[scrip_lon_name]>=lon1) & (grid_ds[scrip_lon_name]<=lon2)
      
      area = area.where(mask,drop=True)
      data = data.where(mask,drop=True)

      # hc.print_stat(data,compact=True,indent=' '*6)

      xy_dims = ('ncol')
      data = (data*area).sum(dim=xy_dims) / area.sum(dim=xy_dims)

      #-------------------------------------------------------------------------
      if convert_to_daily_mean: 
         data = data.resample(time='D').mean(dim='time')
         data = data.isel(time=slice(0,-1))
      #-------------------------------------------------------------------------
      
      # hc.print_stat(data,compact=True,indent=' '*6)
      # exit()
      #-------------------------------------------------------------------------
      if var[v]=='BURDENDUST' : data = data * 1e3 # kg/m2 => g/m2
      #-------------------------------------------------------------------------
      # with warnings.catch_warnings():
      #    warnings.simplefilter("ignore", category=RuntimeWarning)
      #    data['time'] = data.indexes['time'].to_datetimeindex()
      # data = data.sel(time=plot_date)

      # time_coord = np.arange(0,len(data.time),1)

      # time_coord = data.time.dt.dayofyear #+ data.time.dt.year*365
      #-------------------------------------------------------------------------
      # data_list.append( np.ma.masked_invalid( data.values ) )
      data_list.append( data.values )
      time_list.append( data['time'] )
      # date_list.append( convert_time_to_date(data['time']) )

   # if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='   '*4,compact=True)
   #----------------------------------------------------------------------------
   # adjust time coordinates
   time_union = time_list[0]
   for c in range(1,num_case): time_union = np.union1d( time_union, time_list[c] )
   time_union = xr.DataArray(time_union)
   time_coord_union = get_time_coord(time_union)
   date_label_union = convert_time_to_date( time_union )
   # print(); print(time_coord_union)
   # print(); print(date_label_union)
   # print(date_label_union.dtype)
   # exit()
   #----------------------------------------------------------------------------
   res.trYMinF = np.min([np.nanmin(d) for d in data_list])
   res.trYMaxF = np.max([np.nanmax(d) for d in data_list])
   res.trXMinF = np.min(time_coord_union)
   res.trXMaxF = np.max(time_coord_union)
   #----------------------------------------------------------------------------
   # setup plot stuff for this variable
   tres = copy.deepcopy(res)
   tres.tmXBMode = 'Explicit'
   if convert_to_daily_mean:
      tres.tmXBValues = time_coord_union
      tres.tmXBLabels = date_label_union
   else:
      tres.tmXBValues = time_coord_union[::24]
      tres.tmXBLabels = date_label_union[::24]
   # tres.tmXBLabelAngleF  = -45.
   tres.tmXBLabelAngleF  = -90.
   #----------------------------------------------------------------------------
   # Create plot panels for this variable
   for c in range(num_case):
      ip = v

      tres.xyLineColor   = clr[c]
      tres.xyDashPattern = dsh[c]

      xx = get_time_coord(time_list[c])
      yy = data_list[c]

      # also mask zero values for dust analysis
      yy = np.ma.masked_where( yy==0, yy )

      tplot = ngl.xy( wks, xx, yy, tres ) 

      if c==0: plot[ip] = tplot
      if c!=0: ngl.overlay(plot[ip],tplot)

      hs.set_subtitles(wks, plot[ip], reg_str, '', var_str[v], font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Add legend
# lgres = ngl.Resources()
# lgres.lgLabelFontHeightF = 0.01
# lgres.lgLineThicknessF   = 20
# lgres.lgMonoLineColor,lgres.lgLineColors  = False, clr
# lgres.lgMonoDashIndex,lgres.lgDashIndexes = True, 0
# lgres.lgLabelJust    = 'CenterLeft'

# if num_var==1:
#    lgres.vpWidthF, lgres.vpHeightF  = 0.09, 0.13
#    lname = [f' {n}' for n in name]
#    xpos = 0.3 if add_diff_plot else 0.5
#    ypos = 0.65 + (num_var-1)*0.1
#    pid = ngl.legend_ndc(wks, len(name), lname, xpos, ypos, lgres)

# if num_var==2:
#    lgres.vpWidthF, lgres.vpHeightF  = 0.09, 0.11

#    lname = [f' {n}' for n in name]
#    for (n,l) in enumerate(lname):
#       if 'OBS' in lname[n]: 
#          lname[n] = lname[n].replace('OBS','MAC')
#    xpos = 0.3 if add_diff_plot else 0.5
#    ypos = 0.7 + (num_var-1)*0.1
#    pid = ngl.legend_ndc(wks, len(name), lname, xpos, ypos, lgres)

#    lname = [f' {n}' for n in name]
#    for (n,l) in enumerate(lname):
#       if 'OBS' in lname[n]: 
#          lname[n] = lname[n].replace('OBS','IMERG')
#    xpos = 0.3 if add_diff_plot else 0.5
#    ypos = 0.35 + (num_var-1)*0.1
#    pid = ngl.legend_ndc(wks, len(name), lname, xpos, ypos, lgres)

#---------------------------------------------------------------------------------------------------
# Add inset map
add_inset_map = True
if add_inset_map:

   ires = ngl.Resources()
   ires.nglDraw         = False
   ires.nglFrame        = False
   ires.nglMaximize     = False
   ires.tmXBOn          = False
   ires.tmYLOn          = False
   ires.vpHeightF       = 0.1
   ires.vpWidthF        = 0.1
   ires.mpGridAndLimbOn = False
   ires.mpCenterLonF    = 180.
   ires.mpLimitMode = "LatLon" 
   # ires.mpMinLatF = lat1-7; ires.mpMaxLatF = lat2+7
   # ires.mpMinLonF = lon1-7; ires.mpMaxLonF = lon2+7
   idx = 7
   ilat1,ilat2,ilon1,ilon2 = 15-idx,30+idx,40-idx,55+idx
   ires.mpMinLatF = ilat1; ires.mpMaxLatF = ilat2
   ires.mpMinLonF = ilon1; ires.mpMaxLonF = ilon2
   
   iplot = ngl.map(wks, ires) 

   ### draw box around map region used for inset
   bx = np.array([lon1,lon2,lon2,lon1,lon1])
   by = np.array([lat1,lat1,lat2,lat2,lat1])

   pgres = ngl.Resources()
   pgres.nglDraw,pgres.nglFrame = False,False
   pgres.gsLineColor = 'red'
   pgres.gsLineThicknessF = 6

   pdum = ngl.add_polyline(wks,iplot,bx,by,pgres)

   ### attach plot via annotation
   ares = ngl.Resources()
   ares.amZone = 1
   # ares.amOrthogonalPosF = 0.78
   # ares.amParallelPosF   = 0.5
   ares.amOrthogonalPosF = 0.8
   ares.amParallelPosF   = 0.8
   anno = ngl.add_annotation(plot[0], iplot ,ares)
#---------------------------------------------------------------------------------------------------
# Finalize plot

# if num_case==1 or num_var==1:
#    layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
# else:
#    layout = [num_var,num_case] if var_x_case else [num_case,num_var]

# layout = [num_var,1]

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
