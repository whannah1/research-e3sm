import os, glob, subprocess as sp
import xarray as xr
import rioxarray as rxr
class clr:END,RED,GREEN,YELLOW,MAGENTA,CYAN,BOLD = '\033[0m','\033[31m','\033[32m','\033[33m','\033[35m','\033[36m','\033[1m'
#---------------------------------------------------------------------------------------------------
src_root = '/p/lustre2/hannah6/obs_data/MODIS/raw_hdf'
dst_root = '/p/lustre2/hannah6/obs_data/MODIS'

# src_file_prefix = 'MYD08_M3'

# src_file_prefix = 'MOD09CMA'
src_file_prefix = 'MYD09CMA'

var_list = []
# var_list.append('AOD_550_Dark_Target_Deep_Blue_Combined_Mean_Mean')
var_list.append('')

#---------------------------------------------------------------------------------------------------

src_file_path = f'{src_root}/{src_file_prefix}*.hdf'
src_file_list = sorted(glob.glob(src_file_path))

print()
for src_file in src_file_list:

   dst_file = src_file.replace(src_root,dst_root).replace('.hdf','.nc')

   print(f'  {clr.CYAN}{src_file}{clr.END}  =>  {clr.MAGENTA}{dst_file}{clr.END}')

   modis_pre = rxr.open_rasterio(src_file,mask_and_scale=True)

   # print(); print(modis_pre)
   # print(); print(modis_pre.variables)
   # print(); print(modis_pre['AOD_550_Dark_Target_Deep_Blue_Combined_Mean_Min'])
   # exit()

   if src_file==src_file_list[0]:
      print()
      for k in modis_pre.keys(): print(f'    key: {k}')
      print()

   # ds = xr.Dataset()
   # for var in var_list: ds[var] = modis_pre[var] #* modis_pre[var].attrs['scale_factor']
   # ds.to_netcdf(path=dst_file,mode='w')
   # ds.close()

   ds = xr.Dataset()
   for key in modis_pre.keys():
      var_name = key.replace(' ','_')
      ds[var_name] = modis_pre[key]
   ds.to_netcdf(path=dst_file,mode='w')
   ds.close()

   cmd = f'  ncpdq -U --ovr {dst_file} {dst_file}'
   print(clr.GREEN + cmd + clr.END)
   sp.check_call(cmd,shell=True)

   # exit(f'Stopping to check the output file: {dst_file}')


#---------------------------------------------------------------------------------------------------
print()
