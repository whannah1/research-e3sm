#!/usr/bin/env python
import sys, os, fileinput, re, subprocess as sp, glob, copy
import hapy_common as hc, hapy_setres as hs
import ngl, numpy as np, xarray as xr
# Set up terminal colors
class bcolor:
   ENDC    = '\033[0m';  BLACK  = '\033[30m'; RED   = '\033[31m'  
   GREEN   = '\033[32m'; YELLOW = '\033[33m'; BLUE  = '\033[34m'
   MAGENTA = '\033[35m'; CYAN   = '\033[36m'; WHITE = '\033[37m'
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
name,nodes,groups,clr,dsh  = [],[],[],[],[]
sbase,sgroup = [],[]
plot_speedup = False
#-------------------------------------------------------------------------------
# updated Summit timing
#-------------------------------------------------------------------------------
# case_config = 'E3SM.TIMING-2021.GNUCPU.ne30pg2_ne30pg2.F-MMFXX-AQP1'
# group = []
# group.append(f'{case_config}.NODES_16.00')
# group.append(f'{case_config}.NODES_32.00')
# group.append(f'{case_config}.NODES_64.00')
# group.append(f'{case_config}.NODES_128.00')
# group.append(f'{case_config}.NODES_256.00')
# groups.append(group); clr.append('red'); dsh.append(0)

# case_config = 'E3SM.TIMING-2021.GNUGPU.ne30pg2_ne30pg2.F-MMFXX-AQP1'
# group = []
# group.append(f'{case_config}.NODES_16.00')
# group.append(f'{case_config}.NODES_32.00')
# group.append(f'{case_config}.NODES_64.00')
# group.append(f'{case_config}.NODES_128.00')
# group.append(f'{case_config}.NODES_256.00')
# groups.append(group); clr.append('blue'); dsh.append(0)

# node_cnt_str = 'NODES'

# group_list = []
# group_list.append({'arch':'CPU','ndim':'2D','clr':'red',  'dsh':0,'sbase':True})
# group_list.append({'arch':'GPU','ndim':'2D','clr':'blue', 'dsh':0,'sbase':False})
# group_list.append({'arch':'CPU','ndim':'3D','clr':'red',  'dsh':1,'sbase':True})
# group_list.append({'arch':'GPU','ndim':'3D','clr':'blue', 'dsh':1,'sbase':False})

# for g in group_list:
#    group = []
#    for n in [32,64,128,256,512,1024]:
#       a,nd,c,d = g['arch'],g['ndim'],g['clr'],g['dsh']
#       sb = g['sbase']
#       if nd=='2D': group.append(f'E3SM.TIMING-2022.GNU{a}.ne45pg2_r05_oECv3.F-MMFXX.NODES_{n}.00')
#       if nd=='3D': group.append(f'E3SM.TIMING-2022.GNU{a}.ne45pg2_r05_oECv3.F-MMFXX.NODES_{n}.NXY_32x32.00')
#    groups.append(group); clr.append(c); dsh.append(d)
#    sbase.append(sb)

#-------------------------------------------------------------------------------
# Frontier pre-GB results
#-------------------------------------------------------------------------------

node_cnt_str = 'NN'

group_list = []
group_list.append({'arch':'GPU','dnp':'fixed','clr':'blue','dsh':0,'sbase':True})
group_list.append({'arch':'GPU','dnp':'unset','clr':'red', 'dsh':0,'sbase':False})

for g in group_list:
   group = []
   for n in [1,2,4,8,16,32]:
      a,dnp,c,d = g['arch'],g['dnp'],g['clr'],g['dsh']
      sb = g['sbase']
      if dnp=='fixed': group.append(f'E3SM.GB2023-TIMING-01.CRAY{a}.ne4pg2.F2010-MMF1.dyn_npes_fixed.NN_{n}.L60_50.NXY_32x32.DXT_2000x10.DTM_20')
      if dnp=='unset': group.append(f'E3SM.GB2023-TIMING-01.CRAY{a}.ne4pg2.F2010-MMF1.dyn_npes_unset.NN_{n}.L60_50.NXY_32x32.DXT_2000x10.DTM_20')

   groups.append(group); clr.append(c); dsh.append(d)
   sbase.append(sb)

#-------------------------------------------------------------------------------
# Perlmutter timing
#-------------------------------------------------------------------------------

# case_config = 'E3SM.PM_TEST.GNUGPU.ne30pg2_ne30pg2.F-MMFXX'
# group = []
# group.append(f'{case_config}.NNODES_8.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_16.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_32.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_64.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_128.NTHRDS_8.00')
# groups.append(group); clr.append('blue'); dsh.append(0)

# case_config = 'E3SM.PM_TEST.GNUCPU.ne30pg2_ne30pg2.F-MMFXX'
# group = []
# group.append(f'{case_config}.NNODES_8.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_16.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_32.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_64.NTHRDS_8.00')
# group.append(f'{case_config}.NNODES_128.NTHRDS_8.00')
# groups.append(group); clr.append('red'); dsh.append(0)

# node_cnt_str = 'NNODES'

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

node_list_all = []
for case_list in groups:
   for case in case_list:
      params = [p.split('_') for p in case.split('.')]
      for p in params:
         if p[0]==node_cnt_str: node_list_all.append(float(p[1]))


param_list = []
param_list.append('Throughput')
# param_list.append('physics_fraction')
# param_list.append('Run Time    :')
# param_list.append('ATM Run Time')
# param_list.append('\"a:crm\"')
# param_list.append('a:CAM_run3')
# param_list.append('a:CAM_run1')


#-------------------------------------------------------------------------------
# setup plot stuff
#-------------------------------------------------------------------------------
# num_param = len(param)

if 'dsh' not in vars() or dsh==[]: dsh = np.zeros(num_case)

fig_file = os.getenv("HOME")+'/Research/E3SM/figs_performance/scaling.v1'
wks = ngl.open_wks('png',fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.tmXBMinorOn  = True
res.tmYLMinorOn  = True
res.xyMarkLineMode = 'MarkLines'
res.xyMarker = 16
res.xyMarkerSizeF = 0.008
res.xyLineThicknessF = 8

res.tmXMajorGrid = True
res.tmYMajorGrid = True
res.tmXMinorGrid = True
res.tmYMinorGrid = True

res.tmXMajorGridLineColor = 'gray'
res.tmYMajorGridLineColor = 'gray'
res.tmXMinorGridLineColor = 'gray'
res.tmYMinorGridLineColor = 'gray'

res.tmXMajorGridThicknessF = 4
res.tmYMajorGridThicknessF = 4
res.tmXMinorGridThicknessF = 1
res.tmYMinorGridThicknessF = 1

# res.tmXMajorGridLineDashPattern = 0

res.xyXStyle = 'Log'
res.xyYStyle = 'Log'


#-------------------------------------------------------------------------------
# make sure all logs are unzipped
#-------------------------------------------------------------------------------
max_case_len = 0
for case_list in groups:
   for case in case_list:
      timing_dir = os.getenv('HOME')+f'/E3SM/Cases/{case}/timing'
      timing_stat_gz_path = f'{timing_dir}/*.gz'
      if len(glob.glob(timing_stat_gz_path))>0: os.system(f'gunzip {timing_stat_gz_path} ')
      # find max char len for case name
      if len(case)>max_case_len: max_case_len = len(case)

      ### copy files to folder for sharing
#       timing_stat_path = f'{timing_dir}/e3sm_timing*'
#       if len(glob.glob(timing_stat_path))>0: 
#          # os.system(f'ls -1 {timing_stat_path} ')
#          tfiles = glob.glob(timing_stat_path)
#          for tfile in tfiles:
#             dst_file_name = tfile
#             if 'e3sm_timing_stats' in dst_file_name: 
#                dst_file_name = dst_file_name.replace('e3sm_timing_stats',f'e3sm_timing_stats.{case}')
#             dst_file_name = dst_file_name.replace(f'/ccs/home/hannah6/E3SM/Cases/{case}/timing/','')
#             cmd = f'cp {tfile} ~/Research/E3SM/timing_data_for_mark/{dst_file_name}'
#             print(f'\n{bcolor.CYAN}{cmd}{bcolor.ENDC}')
#             os.system(cmd)
# exit()


#-------------------------------------------------------------------------------
# Loop through parameters and cases
#-------------------------------------------------------------------------------
for param in param_list :
   print()
   group_data_list = []
   file_list = []
   for case_list in groups:
      print()
      data_list = []
      for c,case in enumerate(case_list):
         # print()
         timing_dir = os.getenv('HOME')+f'/E3SM/Cases/{case}/timing'
         
         case_name_fmt = bcolor.CYAN+f'{case:{max_case_len}}'+bcolor.ENDC

         # check that the timing files exist
         timing_file_path = f'{timing_dir}/*'

         # check if files exist
         if len(glob.glob(timing_file_path))==0: 
            if os.path.isdir(os.getenv('HOME')+f'/E3SM/Cases/{case}'):
               print(f'    {case_name_fmt}   '+bcolor.YELLOW+'(no files)'+bcolor.ENDC)
            else:
               print(f'    {case_name_fmt}   '+bcolor.RED+'case doesn\'t exist!'+bcolor.ENDC)
            data_list.append(np.nan)
            continue


         tparam = param
         if param=='physics_fraction': tparam = 'a:CAM_run'
         #-------------------------------------------------------------------------
         # grep for the appendropriate line in the stat files
         cmd = 'grep  \''+tparam+f'\'  {timing_dir}/*'
         proc = sp.Popen(cmd, stdout=sp.PIPE, shell=True, universal_newlines=True)
         (msg, err) = proc.communicate()
         msg = msg.split('\n')

         stat_file = msg[0].split(':')[0]

         #-------------------------------------------------------------------------
         # Clean up message but don't print yet
         for m in range(len(msg)): 
            line = msg[m]
            line = line.replace(timing_dir+'/','')
            line = line.replace(f'e3sm_timing.{case}.','e3sm_timing        ')
            line = line.replace('e3sm_timing_stats.'  ,'e3sm_timing_stats  ')
            # Add case name
            if line!='': line = case_name_fmt+' '+line
            line = line.replace(f'e3sm_timing      ','')
            msg[m] = line
         #-------------------------------------------------------------------------
         # print stat file header with indentation
         if 'a:' in tparam : 
            head = sp.check_output(['head',stat_file],universal_newlines=True).split('\n')
            for line in head: 
               if 'walltotal' in line:
                  indent = len(msg[0].split(':')[0])+1
                  line = ' '*indent+line
                  hline = line
                  # Get rid of some dead space
                  line = line.replace('name        ','name')
                  print(hline)
                  break
         #-------------------------------------------------------------------------
         # set up character indices for color
         if 'a:' in tparam:
            n1 = hline.find('walltotal')    +len('walltotal')
            n2 = hline.find('wallmax')      +len('wallmax')
            n3 = hline.find('wallmax')      +len('wallmax (proc   thrd  )')
            n4 = hline.find('wallmin')      +len('wallmin')
         elif 'ATM Run Time' in tparam:
            line = msg[0]
            n1 = line.replace(':','', 1).find(':')+2
            num_in_list = re.findall(r'\d+\.\d+', line[n1:])
            n1 = line.find(num_in_list[2])
            n2 = line.find(num_in_list[2])+len(num_in_list[2])
         else:
            line = msg[0]
            n1 = line.replace(':','', 1).find(':')+2
            num_in_list = re.findall(r'\d+\.\d+', line[n1:])
            n2 = line.find(num_in_list[0])+len(num_in_list[0])

         #-------------------------------------------------------------------------
         # print the timing data
         num_file = -len(msg)
         data,dpos = [],[]
         frac_cnt,phys_cost,tot_cost = 0,0,0
         for line in msg[num_file:] : 
            if line=='': continue
            if param=='physics_fraction': 
               if frac_cnt==0: print()
               if any([s in line for s in ['a:CAM_run1','a:CAM_run2','a:CAM_run3']]):
                  tot_cost += float(line[n1:n2])
               if any([s in line for s in ['a:CAM_run1','a:CAM_run2']]):
                  phys_cost += float(line[n1:n2])
               frac_cnt += 1
               if frac_cnt==4:
                  data.append( phys_cost / tot_cost )
                  dpos.append(float(c))
                  frac_cnt,phys_cost,tot_cost = 0,0,0
            else:
               data.append(float(line[n1:n2]))
               # dpos.append(float(c))
            if 'a:' in tparam : 
               line = line[:n1] \
                    +bcolor.CYAN  +line[n1:n2]+bcolor.CYAN +line[n2:n3] \
                    +bcolor.GREEN +line[n3:n4]+bcolor.ENDC +line[n4:]
               # Get rid of some dead space
               line = line.replace('        ','')
            else:
               line = line[:n1] \
                    +bcolor.GREEN +line[n1:n2]+bcolor.ENDC \
                    +line[n2:]
               # Print conversion to min aand hours for specific params
               offset = len(bcolor.GREEN)
               if tparam=='Run Time    :' and line[n1+offset:n2+offset]!='' :
                  sec = float( line[n1+offset:n2+offset] )
                  line = line+'  ('+bcolor.GREEN+f'{sec/60:.2f}'     +bcolor.ENDC+' min)'
                  line = line+'  ('+bcolor.GREEN+f'{sec/60/60:.2f}'  +bcolor.ENDC+' hour)'
            # print the line
            print('    '+line)

         data = np.array(data)
         avg = np.average(data)

         data_list.append(avg)

         # print(f'average: {bcolor.GREEN}{avg:6.3}{bcolor.ENDC}')

      group_data_list.append(data_list)

      # print()
      # print(group_data_list)
      # print()


   #----------------------------------------------------------------------------
   # calculate plot limits

   # res.tiXAxisString = xvar
   # res.tiYAxisString = yvar

   group_data_list_flat = [item for sublist in group_data_list for item in sublist]

   # px = xr.DataArray(np.array(dpos_list)).stack().values
   py = xr.DataArray(np.array(group_data_list_flat)).stack().values

   mag = np.max(node_list_all) - np.min(node_list_all)
   res.trXMinF = np.min(node_list_all) - 0.01*mag
   res.trXMaxF = np.max(node_list_all) + 0.1*mag
   
   tpy = np.ma.masked_invalid(py)
   mag = np.max(tpy) - np.min(tpy)
   res.trYMinF = np.min(tpy)   - 0.1*np.min(tpy)
   res.trYMaxF = np.max(tpy)   + 0.1*np.max(tpy)

   # calculate bounds for speedup plot
   smin_grp = np.full(len(group_data_list),np.nan)
   smax_grp = np.full(len(group_data_list),np.nan)
   for g,data_list in enumerate(group_data_list):
      py = np.array(data_list)
      if sbase[g]:
         baseline = py
      else:
         smin_grp[g] = np.nanmin(py/baseline)
         smax_grp[g] = np.nanmax(py/baseline)
         # print(f'   {(py/baseline)}')
   smin_grp = np.ma.masked_invalid(np.array(smin_grp))
   smax_grp = np.ma.masked_invalid(np.array(smax_grp))
   smin = np.nanmin(smin_grp) - 0.1*np.nanmin(smin_grp)
   smax = np.nanmax(smax_grp) + 0.1*np.nanmax(smax_grp)

   # print()
   # print(f'smin_grp: {smin_grp}')
   # print(f'smax_grp: {smax_grp}')
   # print()
   # print(f'smin: {smin}')
   # print(f'smax: {smax}')     
   # exit()

   #----------------------------------------------------------------------------
   # plot timing data

   for g,data_list in enumerate(group_data_list):

      case_list = groups[g]

      # figure out node count for each point
      node_list = []
      for case in case_list:
         params = [p.split('_') for p in case.split('.')]
         for p in params:
            if p[0]==node_cnt_str: node_list.append(float(p[1]))

      px = xr.DataArray(np.array(node_list)).stack().values
      py = xr.DataArray(np.array(data_list)).stack().values

      #-------------------------------------------------
      #-------------------------------------------------
      # print data that will go on the plot
      print()
      for c,case in enumerate(case_list):
         if np.isnan(py[c]) : continue
         if 'GPU' in case: arch = 'GPU'
         if 'CPU' in case: arch = 'CPU'
         msg = f'    {arch}'
         msg+= f'    # nodes: {bcolor.GREEN}{int(px[c]):6}{bcolor.ENDC}'
         msg+= f'    Throughput: {bcolor.GREEN}{    py[c] :6.2f}{bcolor.ENDC}'
         print(msg)
      #-------------------------------------------------
      #-------------------------------------------------

      px = np.where(np.isnan(py), np.nan, px)
      px = np.ma.masked_invalid(np.stack(px))
      py = np.ma.masked_invalid(np.stack(py))

      res.tiXAxisString = '# Nodes'
      # res.tiYAxisString = param

      res.xyMarkerColor = clr[g]
      res.xyLineColor   = clr[g]
      res.xyDashPattern = dsh[g]
      
      tplot = ngl.xy(wks, px, py, res)

      if g==0:
         plot.append( tplot  )
      else:
         # ngl.overlay( plot[len(plot)-1], tplot )
         ngl.overlay( plot[0], tplot )

      #-------------------------------------------------
      # plot speedup
      #-------------------------------------------------
      if plot_speedup:
         if sbase[g]:
            baseline = py
         else:
            sx,sy = px,py/baseline
            sres = copy.deepcopy(res)         
            sres.trYMinF = smin 
            sres.trYMaxF = smax 
            sres.xyYStyle = 'Linear'
            splot = ngl.xy(wks, sx, sy, sres)

         if 'splot' in locals():
            if len(plot)==1:
               plot.append( splot  )
            else:
               ngl.overlay( plot[1], splot )
            del splot
      
      #-------------------------------------------------
      #-------------------------------------------------

   # # Add horizontal lines
   # ngl.overlay( plot[len(plot)-1], ngl.xy(wks,[0,0],[-1e8,1e8],lres) )
   # ngl.overlay( plot[len(plot)-1], ngl.xy(wks,[-1e8,1e8],[0,0],lres) )

   param_name = ''
   if param=='Throughput':       param_name = 'Overall Model Throughput [sypd]'
   if param=='ATM Run Time':     param_name = 'Atmosphere Model Throughput [sypd]'
   if param=='physics_fraction': param_name = 'Proportional Cost of Physics (excluding I/O)'
   
   subtitle_font_height = 0.015
   if len(param_list)==2: subtitle_font_height = 0.015
   hs.set_subtitles(wks, plot[0], '', param_name, '', font_height=subtitle_font_height)

   if len(plot)==2:
      hs.set_subtitles(wks, plot[1], '', 'GPU speedup', '', font_height=subtitle_font_height)
      
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
pres = hs.setres_panel()
if len(param_list)>1:
   # pres.nglPanelYWhiteSpacePercent = 5
   # pres.nglPanelXWhiteSpacePercent = 5
   pres.nglPanelFigureStrings            = ['a','b','c','d','e','f','g','h']
   pres.nglPanelFigureStringsJust        = "TopLeft"
   pres.nglPanelFigureStringsFontHeightF = 0.015

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

hc.trim_png(fig_file)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
