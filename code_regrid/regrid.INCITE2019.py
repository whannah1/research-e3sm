#!/usr/bin/env python
import os
import glob
import subprocess
home = os.getenv('HOME')
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
case = 'INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'

data_dir  = f'/gpfs/alpine/scratch/hannah6/cli115/e3sm_scratch/{case}/run'
out_dir = data_dir.replace('run','regridded')

src_grid = 'ne120pg2'

hist_type = 'h1'

# destination grid lat and lon
nlat,nlon = 180,360

map_file = f'~/E3SM/data_mapping/map_{src_grid}_to_{nlat}x{nlon}.nc'

execute = True

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def generate_mapping_file(clean=False):

    src_grid_file = f'{home}/E3SM/data_grid/{src_grid}_scrip.nc'
    dst_grid_file = f'{home}/E3SM/data_grid/{nlat}x{nlon}_scrip.nc'

    # remove old destination grid and mapping file
    if clean :
        cmd = f'rm {dst_grid_file} {map_file}'
        print('\n'+cmd+'\n')
        if execute:  subprocess.call(cmd, shell=True)

    # Generate target grid file:
    cmd  = f'ncremap -G ttl=\'Equi-Angular grid {nlat}x{nlon}\'#latlon={nlat},{nlon}#lat_typ=uni#lon_typ=grn_ctr -g {dst_grid_file} '
    # cmd  = f'ncremap -G ttl=\'Equi-Angular grid {nlat}x{nlon}\'#latlon={nlat},{nlon}#lat_typ=uni#lon_typ=grn_wst -g {dst_grid_file} '
    print('\n'+cmd)
    if execute:  subprocess.call(cmd, shell=True)

    # Need to make sure the 'grid_imask' variable is an integer for TempestRemap
    cmd = f'ncap2 -s \'grid_imask=int(grid_imask)\' {src_grid_file} {src_grid_file} --ovr'
    print('\n'+cmd+'\n')
    if execute:  subprocess.call(cmd, shell=True)

    # Generate mapping file:
    cmd  = f'ncremap -a fv2fv --src_grd={src_grid_file} --dst_grd={dst_grid_file} -m {map_file} '
    print('\n'+cmd+'\n')
    if execute:  subprocess.call(cmd, shell=True)

    return
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
def regrid_data():
    
    files = sorted( glob.glob(data_dir+f'/*cam.{hist_type}.*') )

    for ifile in files:
        # ofile = ifile.replace(data_dir,'').replace('.nc','.remap.nc')
        cmd = f'ncremap -m {map_file} -i {ifile} -O {out_dir}  '
        print('\n'+cmd+'\n')
        if execute: subprocess.call(cmd, shell=True)
        
        # break

    return
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
if __name__ == '__main__':

    print()
    print(f'  output directory: {out_dir}')
    print()
    # generate_mapping_file()
    regrid_data()
