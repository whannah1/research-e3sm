import os, copy, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
var = []
def add_var(var_name): var.append(var_name)
#---------------------------------------------------------------------------------------------------

### PAM dev tests
# add_case('E3SM.PAM-DEV-2023-00.GNUGPU.ne4_ne4.FSCM-ARM97-MMF1',n='MMF-SAM',c='gray')
# add_case('E3SM.PAM-DEV-2023-00.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='red')
# add_case('E3SM.PAM-DEV-2023-02.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='cyan')
# add_case('E3SM.PAM-DEV-2023-03.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='aquamarine')
# add_case('E3SM.PAM-DEV-2023-09.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='pink')
# add_case('E3SM.PAM-DEV-2023-04.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='plum')            #!
# add_case('E3SM.PAM-DEV-2023-08.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='plum1')           #!
# add_case('E3SM.PAM-DEV-2023-10.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='plum2')           #!
# add_case('E3SM.PAM-DEV-2023-11.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='plum3')           #!
# add_case('E3SM.PAM-DEV-2023-12.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='plum4')           #!
# add_case('E3SM.PAM-DEV-2023-13.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='magenta')
# add_case('E3SM.PAM-DEV-2023-14.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='palevioletred')
# add_case('E3SM.PAM-DEV-2023-16.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='paleturquoise')
# add_case('E3SM.PAM-DEV-2023-17.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='peachpuff')       #no-dycor
# add_case('E3SM.PAM-DEV-2023-15.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='yellowgreen')
# add_case('E3SM.PAM-DEV-2023-18.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='green')
# add_case('E3SM.PAM-DEV-2023-19.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='seagreen')
# add_case('E3SM.PAM-DEV-2023-20.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='yellow')
# add_case('E3SM.PAM-DEV-2023-21.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='purple')
# add_case('E3SM.PAM-DEV-2023-22.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='plum',d=1)
# add_case('E3SM.PAM-DEV-2023-23.GNUGPU.ne4_ne4.FSCM-ARM97-MMF2',n='MMF-PAM',c='magenta',d=2)

# add_case('E3SM.2023-PAM-SCM-00.ne4_ne4.FSCM-ARM97-MMF1',                  n='MMF-SAM',c='red',    d=0)
# add_case('E3SM.2023-PAM-SCM-01.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='green',  d=0)
# add_case('E3SM.2023-PAM-SCM-02.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='cyan',   d=0)
# add_case('E3SM.2023-PAM-SCM-03.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='blue',   d=0)
# add_case('E3SM.2023-PAM-SCM-04.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='purple',   d=0)
# add_case('E3SM.2023-PAM-SCM-05.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='pink',   d=0)
# add_case('E3SM.2023-PAM-SCM-06.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='magenta',   d=0)
# add_case('E3SM.2023-PAM-SCM-07.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='purple',   d=0)
# add_case('E3SM.2023-PAM-SCM-08.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='purple',   d=0)
# add_case('E3SM.2023-PAM-SCM-09.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='pink',   d=0)
# add_case('E3SM.2023-PAM-SCM-10.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='green',   d=0)

# add_case('E3SM.2023-PAM-SCM-00.ne4_ne4.FSCM-ARM97-MMF1',                  n='MMF-SAM',c='red',    d=0)
# add_case('E3SM.2023-PAM-SCM-03.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='blue',   d=0)
# add_case('E3SM.2023-PAM-SCM-02.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='blue',   d=0)
# add_case('E3SM.2023-PAM-SCM-06.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='magenta',   d=0)
# add_case('E3SM.2023-PAM-SCM-10.ne4_ne4.FSCM-ARM97-MMF2',                  n='MMF-PAM',c='green',   d=0)



add_var('T')
add_var('Q')
add_var('CLDLIQ')
add_var('CLDICE')
# add_var('NUMLIQ')
# add_var('NUMICE')
# add_var('MMF_QC')
# add_var('MMF_QI')
# add_var('MMF_QR')

# add_var('CRM_T')
# add_var('CRM_QV')
# add_var('CRM_QC')
# add_var('CRM_QI')

# add_var('CLDLIQ')
# add_var('MMF_QC')
# add_var('MMF_QI')
# add_var('MMF_QR')
# add_var('MMF_NC')
# add_var('MMF_NI')
# add_var('MMF_NR')

# add_var('MMF_LIQ_ICE')
# add_var('MMF_VAP_LIQ')
# add_var('MMF_VAP_ICE')

# add_var('CLDLIQ')
# add_var('MMF_DQC')
# add_var('MMF_DQC_MICRO')
# add_var('MMF_DQC_SGS')
# add_var('MMF_DQC_PHYS')
# add_var('MMF_DQC_DYCOR')

# add_var('MMF_DQI_MICRO')
# add_var('MMF_DQI_SGS')
# add_var('MMF_DQI_PHYS')
# add_var('MMF_DQI_DYCOR')

# add_var('MMF_DT')
# add_var('MMF_DT_DYCOR')
# add_var('MMF_DT_MICRO')
# add_var('MMF_DT_SGS')

# add_var('MMF_DT_SGS')
# add_var('MMF_DQV_SGS')
# add_var('MMF_DQC_SGS')
# add_var('MMF_DQI_SGS')
# add_var('MMF_DQR_SGS')

# add_var('T')
# add_var('MMF_TLS')
# add_var('MMF_RHODLS')
# add_var('MMF_RHOVLS')
# add_var('MMF_RHOLLS')
# add_var('MMF_RHOILS')

# add_var('MMF_DQ')
# add_var('MMF_DQV_MICRO')
# add_var('MMF_DQV_SGS')
# add_var('MMF_DQV_DYCOR')
# add_var('MMF_DQC_MICRO')
# add_var('MMF_DQC_SGS')
# add_var('MMF_DQI_MICRO')
# add_var('MMF_DQI_SGS')



htype,years,months,first_file,num_files = 'h2',[],[],0,10


num_plot_col = len(var)
# num_plot_col = 2


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scm/scm.profile.v1'


# write_file    = False
daily_mean    = False
print_stats   = True

plot_diff = False

use_height_coord = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'clr' not in vars() or clr==[]: clr = ['black']*num_case
if 'dsh' not in vars() or dsh==[]: dsh = np.zeros(num_case)

if 'lev' not in vars(): lev = np.array([0])

plot = [None]*(num_var)

wkres = ngl.Resources()
npix=4096; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 16
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

if use_height_coord: 
   res.tiYAxisString = 'Height [km]'
   res.trYMaxF = 40
else:
   res.tiYAxisString = 'Pressure [hPa]'
   res.trYReverse = True
   # res.xyYStyle = 'Log'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def CESM_FV(case):
   CESM_FV = False
   if 'CESM' in case and any([g in case for g in ['f09','f19']]): CESM_FV = True
   return CESM_FV

def get_comp(case):
   comp = 'eam'
   # if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)

   lev_list,data_list = [],[]
   use_crm_lev = False
   for c in range(num_case):
      # pam_exclusive_var_list = ['MMF_RHO','MMF_DQ_']
      # if 'MMF1' in case[c] and any(tv in var[v] for tv in pam_exclusive_var_list): 
      #   data = None
      # else:
      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )

      tvar = var[v]
      if var[v]=='MMF_DQC_PHYS': tvar = 'MMF_DQC_MICRO'
      if var[v]=='MMF_DQI_PHYS': tvar = 'MMF_DQI_MICRO'
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
   
      data = case_obj.load_data(tvar,htype=htype,lev=lev,first_file=first_file,num_files=num_files)

      #-------------------------------------------------------------------------
      # derived quantities
      if var[v]=='MMF_DQC_PHYS': data = data + case_obj.load_data('MMF_DQC_SGS',htype=htype,lev=lev,first_file=first_file,num_files=num_files)
      if var[v]=='MMF_DQI_PHYS': data = data + case_obj.load_data('MMF_DQI_SGS',htype=htype,lev=lev,first_file=first_file,num_files=num_files)
      #-------------------------------------------------------------------------

      # get rid of CRM dimensions
      if 'crm_nx' in data.dims : 
         use_crm_lev = True
         data = data.mean(dim=['crm_nx','crm_ny']).rename({'crm_nz':'lev'})

      if use_height_coord: 
         Z = case_obj.load_data('Z3',component=get_comp(case[c]),htype=htype,
                                 years=years,months=months,lev=lev,
                                 first_file=first_file,num_files=num_files)

      
      # print(hc.tcolor.RED+'WARNING: limiting time length to 5 days'+hc.tcolor.ENDC)
      # data = data.isel(time=slice(0,72*5))
      # if use_height_coord: Z = Z.isel(time=slice(0,72*5))

      # Convert to daily mean
      # if htype in ['h1','h2'] and daily_mean: data = data.resample(time='D').mean(dim='time')

      # if np.all( lev < 0 ) and 'lev' in data.coords : print(f'    lev value: {data.lev.values}')

      # Get rid of ncol dimensions dimension
      if 'ncol' in data.dims : data = data.isel(ncol=0)

      


      if print_stats: 
         hc.print_time_length(data.time,indent=' '*6)
         hc.print_stat(data,name=var[v],stat='naxs',indent=' '*6,compact=True)
         # time_mean = data.mean(dim='time').values
         # print('      Area Weighted Time Mean : '+hc.tcolor.GREEN+f'{time_mean:10.6f}'+hc.tcolor.ENDC)

      # average over time
      data = data.mean(dim='time')
      if use_height_coord: 
         Z = Z.mean(dim='time')
         if 'ncol' in Z.dims : Z = Z.isel(ncol=0)

      #-------------------------------------------------------------------------
      # unit conversions
      # if 'MMF_DQ' in var[v]: data = data*1e3*86400. # convert kg/kg/s => g/kg/day
      #-------------------------------------------------------------------------
      if use_height_coord:
         lev_coord_tmp = Z.values/1e3 # convert to km
      else:
         lev_coord_tmp = data['lev'].values

      if use_crm_lev:
         data = data[::-1]
         if use_height_coord:
            nlev_grm,nlev_crm = len(lev_coord_tmp),len(data['lev'].values)
            lev_coord_tmp = lev_coord_tmp[nlev_grm-nlev_crm-1:nlev_grm-1]

      data_list.append( data.transpose().values )
      lev_list.append( copy.deepcopy(lev_coord_tmp) )

      del lev_coord_tmp

      #-------------------------------------------------------------------------
      # write to file
      #-------------------------------------------------------------------------
      # if write_file : 
      #    tfile = f'/global/homes/w/whannah/E3SM/scratch/{case[0]}/run/{case[0]}.time_series.{var[v]}.nc'
      #    print('writing to file: '+tfile)
      #    avg_X.name = var[v]
      #    avg_X.to_netcdf(path=tfile,mode='w')
      #    exit()

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   print()
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = data_min
   tres.trXMaxF = data_max

   # if use_crm_lev: tres.trYReverse = False

   ip = v

   for c in range(num_case):   
      tres.xyLineColor   = clr[c]
      tres.xyMarkerColor = clr[c]
      tres.xyDashPattern = dsh[c]

      tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)  

      if (c==1 and plot_diff) or (c==0 and not plot_diff) :
         plot[ip] = tplot
      elif (plot_diff and c>0) or not plot_diff:
         ngl.overlay(plot[ip],tplot)
   
   ### add vertical line
   lres = hs.res_xy()
   lres.xyLineThicknessF = 1
   lres.xyDashPattern = 0
   lres.xyLineColor = 'black'
   ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))
      
   # Set strings at top of plot
   Lstr,Cstr,Rstr = '','',var[v]
   hs.set_subtitles(wks, plot[ip], Lstr, Cstr, Rstr, font_height=0.01)

   #----------------------------------------------------------------------------
   # Ad legend
   #----------------------------------------------------------------------------
   lgres = ngl.Resources()
   lgres.vpWidthF, lgres.vpHeightF  = 0.08, 0.08  
   lgres.lgLabelFontHeightF = 0.01
   lgres.lgLineThicknessF   = 4
   lgres.lgMonoLineColor    = False
   # lgres.lgMonoDashIndex    = True
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh
   lgres.lgLabelJust    = 'CenterLeft'
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.5, 0.4, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.5, 0.1, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.3, 0.5, lgres)  # 1x2

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
hc.printline()

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

