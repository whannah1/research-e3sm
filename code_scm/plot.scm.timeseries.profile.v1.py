import os, copy, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)

var = []
def add_var(var_name): var.append(var_name)
#---------------------------------------------------------------------------------------------------


# add_case('E3SM.2023-PAM-SCM-00.ne4_ne4.FSCM-ARM97-MMF1',                  n='MMF-SAM',c='red',    d=0)


# add_var('T')
# add_var('Q')
# add_var('CLDLIQ')
# add_var('CLDICE')
# add_var('CLOUD')
# add_var('OMEGA')
# add_var('QRL')
# add_var('QRS')
# add_var('NUMLIQ')
# add_var('NUMICE')
# add_var('MMF_QC')
# add_var('MMF_QI')
# add_var('MMF_QR')

# add_var('T'); add_var('MMF_DT')
# add_var('Q'); add_var('MMF_DQ')

# add_var('MMF_RHOVLS')
# add_var('MMF_QTLS')

# add_var('Q')
# add_var('MMF_QI')
# add_var('MMF_QC')
# add_var('MMF_QR')

# add_var('MMF_LIQ_ICE')
add_var('MMF_VAP_LIQ')
# add_var('MMF_VAP_ICE')

# add_var('CRM_W')

# add_var('MMF_DT')
# add_var('MMF_DT_DYCOR')
# add_var('MMF_DT_MICRO')
# add_var('MMF_DT_SGS')

# add_var('MMF_DQ')
# add_var('MMF_DQV_DYCOR')
# add_var('MMF_DQV_MICRO')
# add_var('MMF_DQV_SGS')

# add_var('MMF_DQV_MICRO')
# add_var('MMF_DQC_MICRO')
# add_var('MMF_DQI_MICRO')

# add_var('MMF_DQ'); add_var('MMF_DQC'); add_var('MMF_DQI')

# add_var('Q')
# add_var('MMF_DQ')
# add_var('MMF_DQV_SGS')
# add_var('MMF_DQV_DYCOR')

# add_var('MMF_DQC')
# add_var('MMF_DQC_DYCOR')
# add_var('MMF_DQC_MICRO')
# add_var('MMF_DQC_SGS')

# add_var('MMF_QI')
# add_var('MMF_DQI')
# add_var('MMF_DQI_MICRO')
# add_var('MMF_DQI_SGS')
# add_var('MMF_DQI_DYCOR')

# num_plot_col  = 3
# add_var('Q')            ;add_var('MMF_QC')       ;add_var('MMF_QI')
# add_var('MMF_DQ')       ;add_var('MMF_DQC')      ;add_var('MMF_DQI')
# add_var('MMF_DQV_DYCOR');add_var('MMF_DQC_DYCOR');add_var('MMF_DQI_DYCOR')
# add_var('MMF_DQV_MICRO');add_var('MMF_DQC_MICRO');add_var('MMF_DQI_MICRO')
# add_var('MMF_DQV_SGS')  ;add_var('MMF_DQC_SGS')  ;add_var('MMF_DQI_SGS')

# num_plot_col  = 2
# add_var('MMF_TLS')   ; add_var('MMF_DT')
# add_var('MMF_RHOVLS'); add_var('MMF_DQ')

# add_var('MMF_TLS')
# add_var('MMF_QTLS')
# add_var('MMF_RHOVLS')
# add_var('MMF_RHODLS')
# add_var('MMF_RHOLLS')
# add_var('MMF_RHOILS')


htype,years,months,first_file,num_files = 'h2',[],[],5,8
# htype,years,months,first_file,num_files = 'h2',[],[],4,3
# htype,years,months,first_file,num_files = 'h2',[],[],2,1


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scm/scm.timeseries.profile.v1'

# write_file    = False
daily_mean    = False
print_stats   = True

# add_obs = True
# obs_file = os.getenv('HOME')+'/E3SM/inputdata/atm/cam/scam/iop/ARM97_iopfile_4scam.nc'

if 'num_plot_col' not in locals(): num_plot_col  = 1

var_x_case = True
use_height_coord = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'clr' not in vars() or clr==[]: clr = ['black']*num_case
if 'dsh' not in vars() or dsh==[]: dsh = np.zeros(num_case)

if 'lev' not in vars(): lev = np.array([0])

plot = [None]*(num_case*num_var)

wkres = ngl.Resources()
npix=4096; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

res = hs.res_contour_fill()
res.vpHeightF = 0.2
res.lbOrientation                = 'Vertical'

# res.trXMinF = 12
# res.trXMaxF = 10

res.tmYLLabelFontHeightF         = 0.01
res.tmXBLabelFontHeightF         = 0.01
res.tiXAxisFontHeightF           = 0.01
res.tiYAxisFontHeightF           = 0.01

# res.cnFillPalette = "MPL_viridis"
# res.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
# res.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
# res.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
# res.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

if use_height_coord: 
   # res.trYMaxF = 5
   res.nglYAxisType = "LinearAxis"
else:
   # res.trYMinF = 700
   res.trYReverse = True

lres = hs.res_xy()
lres.xyDashPattern    = 1
lres.xyLineThicknessF = 1
lres.xyLineColor      = "white"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def CESM_FV(case):
   CESM_FV = False
   if 'CESM' in case and any([g in case for g in ['f09','f19']]): CESM_FV = True
   return CESM_FV

def get_comp(case):
   comp = 'eam'
   # if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)

   time_list,lev_list,data_list = [],[],[]
   for c in range(num_case):

      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], 
                          atm_comp='eam', 
                          data_dir=data_dir_tmp, 
                          data_sub=data_sub_tmp,
                          # populate_files=False,
                        )

      tvar = var[v]
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      # lat = case_obj.load_data('lat',  component=get_comp(case[c]),htype=htype)
      # lon = case_obj.load_data('lon',  component=get_comp(case[c]),htype=htype)
      # area = case_obj.load_data('area',component=get_comp(case[c]),htype=htype,num_files=1).astype(np.double)
      data = case_obj.load_data(tvar,  component=get_comp(case[c]),htype=htype,
                                 years=years,months=months,lev=lev,
                                 first_file=first_file,num_files=num_files)

      if use_height_coord: 
         Z = case_obj.load_data('Z3',component=get_comp(case[c]),htype=htype,
                                 years=years,months=months,lev=lev,
                                 first_file=first_file,num_files=num_files)

      # Convert to daily mean
      if htype in ['h1','h2'] and daily_mean: data = data.resample(time='D').mean(dim='time')

      # if np.all( lev < 0 ) and 'lev' in data.coords : print(f'    lev value: {data.lev.values}')

      # Get rid of ncol dimensions dimension
      if 'ncol' in data.dims : data = data.isel(ncol=0)

      if use_height_coord: 
         Z = Z.mean(dim='time')
         if 'ncol' in Z.dims : Z = Z.isel(ncol=0)

      #reset time index to start at zero and convert to days
      dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')

      # Make time start at zero
      data['time'] = ( data['time'] - data['time'][0] ).astype('float') / 86400e9

      #-------------------------------------------------------------------------
      # unit conversions
      # if 'MMF_DQ' in var[v]: data = data*1e3*86400. # convert kg/kg/s => g/kg/day

      # if 'MMF_D' in var[v]: res.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
      #-------------------------------------------------------------------------

      if print_stats: 
         hc.print_time_length(data.time,indent=' '*6)
         hc.print_stat(data,name=var[v],stat='naxs',indent=' '*6,compact=True,fmt='e')
         # time_mean = data.mean(dim='time').values
         # print('      Area Weighted Time Mean : '+hc.tcolor.GREEN+f'{time_mean:10.6f}'+hc.tcolor.ENDC)
      
      

      # data_list.append( data.values )
      time_list.append( data['time'].values )
      data_list.append( data.transpose().values )
      if use_height_coord:
         lev_list.append( Z.values/1e3 ) # convert to km
      else:
         lev_list.append( data['lev'].values )

      #-------------------------------------------------------------------------
      # write to file
      #-------------------------------------------------------------------------
      # if write_file : 
      #    tfile = f'/global/homes/w/whannah/E3SM/scratch/{case[0]}/run/{case[0]}.time_series.{var[v]}.nc'
      #    print('writing to file: '+tfile)
      #    avg_X.name = var[v]
      #    avg_X.to_netcdf(path=tfile,mode='w')
      #    exit()

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.tiXAxisString = 'Time [days]'
   #----------------------------------------------------------------------------
   if 'MMF_DQ' in var[v]: tres.cnLevels = np.linspace(-1,1,num=21)*30
   # if 'DQC'    in var[v]: tres.cnLevels = np.linspace(-1,1,num=11)

   if var[v]=='MMF_DT_SGS': tres.cnLevels = np.linspace(-10,10,num=21)

   if var[v]=='MMF_TLS'   : tres.cnLevels = np.linspace(-20,20,num=21)
   if var[v]=='MMF_QTLS'  : tres.cnLevels = np.linspace(-30,30,num=21)
   # if var[v]=='MMF_RHOVLS': tres.cnLevels = np.linspace(-20,20,num=21)
   # if var[v]=='MMF_RHOLLS': tres.cnLevels = np.linspace(-20,20,num=21)
   # if var[v]=='MMF_RHOILS': tres.cnLevels = np.linspace(-20,20,num=21)
   # if var[v]=='MMF_RHODLS': tres.cnLevels = np.linspace(-100,100,num=21)

   if hasattr(tres,'cnLevels') : 
      tres.cnLevelSelectionMode = 'ExplicitLevels'
   else:
      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])
      aboutZero = False
      if var[v]=='MMF_LIQ_ICE': aboutZero = True
      if var[v]=='MMF_VAP_LIQ': aboutZero = True
      if var[v]=='MMF_VAP_ICE': aboutZero = True
      cmin,cmax,cint,clev = ngl.nice_cntr_levels(data_min,data_max,cint=None,max_steps=21 \
                                                ,returnLevels=True,aboutZero=aboutZero )
      tres.cnLevels = np.linspace(cmin,cmax,num=21)
      tres.cnLevelSelectionMode = 'ExplicitLevels'
   #----------------------------------------------------------------------------
   print()
   for c in range(num_case):

      tres.sfYArray = lev_list[c]
      tres.sfXArray = time_list[c]

      ip = v*num_case+c if var_x_case else c*num_var+v

      plot[ip] = ngl.contour(wks, data_list[c], tres)

      
      line_plev = 500
      ngl.overlay(plot[ip], ngl.xy(wks,[-1e8,1e8],[line_plev,line_plev],lres) )
      
      
      # Set strings at top of plot
      Lstr,Cstr,Rstr = case_name[c],'',var[v]
      hs.set_subtitles(wks, plot[ip], Lstr, Cstr, Rstr, font_height=0.01)

   #----------------------------------------------------------------------------
   # Ad legend
   #----------------------------------------------------------------------------
   lgres = ngl.Resources()
   lgres.vpWidthF, lgres.vpHeightF  = 0.08, 0.08  
   lgres.lgLabelFontHeightF = 0.01
   lgres.lgLineThicknessF   = 4
   lgres.lgMonoLineColor    = False
   # lgres.lgMonoDashIndex    = True
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh
   lgres.lgLabelJust    = 'CenterLeft'
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.5, 0.4, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.5, 0.1, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.3, 0.5, lgres)  # 1x2

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
hc.printline()

layout = [num_var,num_case] if var_x_case else [num_case,num_var]
if 'num_plot_col' in locals():
      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------