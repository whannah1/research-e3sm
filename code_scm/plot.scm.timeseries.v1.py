import os, copy, ngl, xarray as xr, numpy as np, numba
from scipy.stats import skew
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
from datetime import datetime
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)

var,lev_list,htype_list = [],[],[]
def add_var(var_name,lev=None,htype='h1'): 
   var.append(var_name)
   lev_list.append(lev)
   htype_list.append(htype)
#---------------------------------------------------------------------------------------------------

# add_case('obs',n='obs',c='black')

### new SCM-RCE compsets
# add_case('E3SM.SCM-TEST-01.GNUCPU.FSCM-RCE',     n='',     c='red', p='/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu')
# add_case('E3SM.SCM-TEST-01.GNUGPU.FSCM-RCE-MMF1',n='',     c='blue',p='/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu')

add_case('E3SM.2023-PAM-SCM-01.ne4_ne4.FSCM-ARM97-MMF1',n='MMF-SAM',c='black',d=0)

### PAM ensemble to revisit time step sensitivity
tmp_dir,tmp_sub = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch','run'
add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_02.SSC_01.HDT_0010',n='',c='red',    d=0,p=tmp_dir,s=tmp_sub)
# add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_02.SSC_02.HDT_0010',n='',c='orange', d=0,p=tmp_dir,s=tmp_sub)
add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_04.SSC_01.HDT_0010',n='',c='green',  d=0,p=tmp_dir,s=tmp_sub)
# add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_04.SSC_02.HDT_0010',n='',c='blue',   d=0,p=tmp_dir,s=tmp_sub)
add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_06.SSC_01.HDT_0010',n='',c='blue',   d=0,p=tmp_dir,s=tmp_sub)
# add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_06.SSC_02.HDT_0010',n='',c='purple', d=0,p=tmp_dir,s=tmp_sub)

add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_04.SSC_01.HDT_0060',n='',c='green',  d=1,p=tmp_dir,s=tmp_sub)
# add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_04.SSC_02.HDT_0060',n='',c='blue',   d=1,p=tmp_dir,s=tmp_sub)
add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_06.SSC_01.HDT_0060',n='',c='blue',   d=1,p=tmp_dir,s=tmp_sub)
# add_case('E3SM.2023-PAM-ENS-00.ne4_ne4.FSCM-ARM97-MMF2.CDT_10.DPP_06.SSC_02.HDT_0060',n='',c='purple', d=1,p=tmp_dir,s=tmp_sub)


htype,first_file,num_files = 'h1',3,7
# htype,first_file,num_files = 'h1',0,10
# htype,first_file,num_files = 'h1',0,2

add_var('TMQ')
add_var('PRECT')
# add_var('PRECSC')
add_var('TGCLDLWP')
add_var('TGCLDIWP')
# add_var('T'     ,htype='h2')
# add_var('MMF_TLS',htype='h2')
# add_var('MMF_DT',htype='h2')
# add_var('MMF_QTLS'    ,htype='h2')
# add_var('MMF_DQT'      ,htype='h2')


# add_var('MMF_DT'      ,htype='h2')
# add_var('MMF_DT_DYCOR',htype='h2')
# add_var('MMF_DT_MICRO',htype='h2')
# add_var('MMF_DT_SGS'  ,htype='h2')

# add_var('MMF_DT_DYCOR' ,htype='h2')
# add_var('MMF_DQV_DYCOR',htype='h2')
# add_var('MMF_DQC_DYCOR',htype='h2')
# add_var('MMF_DQI_DYCOR',htype='h2')
# add_var('MMF_DQR_DYCOR',htype='h2')

# add_var('MMF_DQT'      ,htype='h2')
# add_var('MMF_DQT_DYCOR',htype='h2')
# add_var('MMF_DQT_MICRO',htype='h2')
# add_var('MMF_DQT_SGS'  ,htype='h2')

# add_var('MMF_RHODLS'  ,htype='h2')
# add_var('MMF_RHOVLS'  ,htype='h2')

# add_var('MMF_DQT'      ,htype='h2')
# add_var('PRECT')
# add_var('MMF_RES_DQT'  ,htype='h2')

# add_var('TBOT')
# add_var('MMF_QR',htype='h2')


# add_var('MMF_RHOVLS'  ,htype='h2')
# add_var('MMF_RHOD'    ,htype='h2')
# add_var('MMF_RHOV'    ,htype='h2')
# add_var('MMF_QTLS'    ,htype='h2')
# add_var('MMF_DQ_SGS'  ,htype='h2')
# add_var('MMF_DQ_MICRO',htype='h2')
# add_var('MMF_DQV_MICRO',htype='h2')
# add_var('MMF_DQ_DYCOR',htype='h2')
# add_var('MMF_DQV_DYCOR',htype='h2')
# add_var('MMF_DQ_RES'  ,htype='h2')


# add_var('MMF_DQV_MICRO',htype='h2')
# add_var('MMF_DQC_MICRO',htype='h2')
# add_var('MMF_DQI_MICRO',htype='h2')

# add_var('MMF_QR',htype='h2')
# add_var('CRM_QV',htype='h2')

# add_var('MMF_DQ',htype='h2')
# add_var('MMF_DQC',htype='h2')
# add_var('MMF_DQI',htype='h2')

# add_var('MMF_NC',htype='h2')
# add_var('MMF_NR',htype='h2')
# add_var('MMF_NI',htype='h2')

# add_var('RESTOM')
# add_var('FSNTOA')
# add_var('NET_TOM_RAD')
# add_var('SHFLX')
# add_var('LHFLX')
# add_var('U',lev=850)
# add_var('U',lev=200)
# add_var('CLDICE',lev=?)
# add_var('MMF_QI',lev=?)





fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scm/scm.timeseries.v1'

obs_file = os.getenv('HOME')+'/E3SM/inputdata/atm/cam/scam/iop/ARM97_iopfile_4scam.nc'

# write_file    = False
daily_mean    = False
print_stats   = True
overlay_cases = True

add_trend = False

num_plot_col  = 1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

# overlay_cases with single case causes segfault
if num_case==1 : overlay_cases = False

if 'clr' not in vars() or clr==[]: clr = ['black']*num_case
if 'dsh' not in vars() or dsh==[]: dsh = np.zeros(num_case)

if 'lev' not in vars(): lev = np.array([0])

if overlay_cases:
   plot = [None]*(num_var)
else:
   plot = [None]*(num_case*num_var)

wkres = ngl.Resources()
npix=1024*2; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

res = hs.res_xy()
# res.vpHeightF = 0.5
res.vpHeightF = 0.2
res.tmYLLabelFontHeightF         = 0.005
res.tmXBLabelFontHeightF         = 0.005
res.tiXAxisFontHeightF           = 0.005
res.tiYAxisFontHeightF           = 0.005
res.xyLineThicknessF = 10

lres = hs.res_xy()
lres.xyDashPattern    = 1
lres.xyLineThicknessF = 1
lres.xyLineColor      = "black"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def CESM_FV(case):
   CESM_FV = False
   if 'CESM' in case and any([g in case for g in ['f09','f19']]): CESM_FV = True
   return CESM_FV

def get_comp(case):
   comp = 'eam'
   # if 'E3SM.PI-CPL.v1.' in case: comp = 'cam'
   return comp

# @numba.njit()
def ddt(data_in,ntime,ncol,dt,data_out,method=1) :
   for t in range(1,ntime):
      # data_out[t,:] = ( data_in[t+1,:] - data_in[t-1,:] ) / dt
      if method==0: data_out[t] = ( data_in[t+1] - data_in[t-1] ) / (dt*2)
      if method==1: data_out[t] = ( data_in[t] - data_in[t-1] ) / (dt)

# @numba.njit()
def hybrid_dp3d( PS, P0, hyai, hybi, dp3d ):
   nlev = len(hyai)-1
   (ntime,ncol) = PS.shape
   # dp3d = np.zeros([ntime,nlev,ncol])
   for t in range(ntime):
      for i in range(ncol):
         for k in range(nlev):
            p1 = hyai[k  ]*P0 + hybi[k  ]*PS[t,i]
            p2 = hyai[k+1]*P0 + hybi[k+1]*PS[t,i]
            # dp3d[t,k,i] = p2 - p1
            dp3d[t,k] = p2 - p1
   # return dp3d
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)

   if 'lev_list' in locals(): lev = lev_list[v]

   time_list,data_list = [],[]
   for c in range(num_case):

      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)

      if case[c]!='obs':
         data_dir_tmp,data_sub_tmp = None, None
         if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         if case_sub[c] is not None: data_sub_tmp = case_sub[c]
         case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )

      tvar = var[v]
      if var[v]=='MMF_DQT_SGS'  : tvar = 'MMF_DQV_SGS'
      if var[v]=='MMF_DQT_MICRO': tvar = 'MMF_DQV_MICRO'
      if var[v]=='MMF_DQT_DYCOR': tvar = 'MMF_DQV_DYCOR'
      if var[v]=='MMF_RES_DQT'  : tvar = 'MMF_DQ'
      if var[v]=='MMF_DQT'      : tvar = 'MMF_DQ'
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if case[c]=='obs':
         obs_var = None
         if tvar=='PRECT'    : obs_var = 'Prec'
         if tvar=='TBOT'     : obs_var = 'T'
         if tvar=='QBOT'     : obs_var = 'q'
         if tvar=='TGCLDLWP' : obs_var = 'cldliq'
         if tvar=='TMQ'      : obs_var = 'prew'
         # if tvar=='FSNT'     : obs_var = ''
         if tvar=='FLNT'     : obs_var = 'TOA_LWup'

         if obs_var is not None:
            # ds = xr.open_dataset(obs_file,use_cftime=True,decode_times=True,decode_cf=True)
            ds = xr.open_dataset(obs_file,decode_times=True,decode_cf=True)
            data = ds[obs_var].isel(lat=0,lon=0)
            if tvar in ['TBOT','QBOT']: 
               data = data.isel(lev=-1)

            yr = ds['year'].values
            mn = ds['month'].values
            dy = ds['day'].values
            hr = ds['hour'].values
            nn = ds['minute'].values
            # sc = ds['tsec'].values

            if 'ARM97' in obs_file:
               model_start_date = np.datetime64('1997-06-19T23:29:45.000000000')
               for t in range(len(data.time.values)):
                  dt = datetime(yr[t], mn[t], dy[t], hr[t], nn[t])
                  obs_start_date = np.datetime64(dt)
                  date_diff = model_start_date - obs_start_date
                  # print(f' {obs_start_date}   {date_diff}')
                  if date_diff<0: break
               data = data[t:]

            if tvar=='PRECT'    : data = data*86400.
            if tvar=='TGCLDLWP' : data = data*1e3
            if tvar=='TMQ'      : data = data*1e1

            if daily_mean: data = data.resample(time='D').mean(dim='time')

            dtime = data['time'][1] - data['time'][0]
            tmp_time = data['time'].values
            for t,tt in enumerate(data['time']): tmp_time[t] = data['time'][0] + dtime*t
            data['time'] = tmp_time

            if num_files>0:
               for t,tt in enumerate(data['time']):
                  curr_day = data['time'][t].values - data['time'][0].values
                  if curr_day > num_files: break
               data = data.isel(time=slice(0,t))

         # else:
         #    ds = xr.open_dataset(obs_file,decode_times=True,decode_cf=True)
         #    np.ma.masked_invalid(data.values)
         #    data = ds['Prec'].isel(lat=0,lon=0)
         #    data = np.ma.masked_where( frequency<0.1, frequency )

      else:
         if 'data' in locals(): del data
         pam_exclusive_var_list = ['MMF_RHO','MMF_DQ_','MMF_DT_','MMF_VAP_LIQ','MMF_LIQ_ICE']
         if 'MMF1' in case[c] and any(tv in var[v] for tv in pam_exclusive_var_list): 
            data = None
         else:

            htype = htype_list[v]
            # lat = case_obj.load_data('lat',  component=get_comp(case[c]),htype=htype)
            # lon = case_obj.load_data('lon',  component=get_comp(case[c]),htype=htype)
            # area = case_obj.load_data('area',component=get_comp(case[c]),htype=htype,num_files=1).astype(np.double)
            data = case_obj.load_data(tvar, component=get_comp(case[c]),
                                      htype=htype, lev=lev,
                                      first_file=first_file,num_files=num_files)
            
            # hc.print_stat(data,name=var[v],stat='naxs',indent=' '*6,compact=True)
            # print(); print(data['time'][0].values.astype('datetime64[ns]'))

            if var[v]=='MMF_DQT_SGS':
               data = data + case_obj.load_data('MMF_DQC_SGS', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQI_SGS', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQR_SGS', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data/(86400.*1e3) # undo automatic unit conversion g/kg/day => kg/kg/s
            if var[v]=='MMF_DQT_MICRO': 
               data = data + case_obj.load_data('MMF_DQC_MICRO', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQI_MICRO', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQR_MICRO', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data/(86400.*1e3) # undo automatic unit conversion g/kg/day => kg/kg/s
            if var[v]=='MMF_DQT_DYCOR': 
               data = data + case_obj.load_data('MMF_DQC_DYCOR', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQI_DYCOR', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQR_DYCOR', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data/(86400.*1e3) # undo automatic unit conversion g/kg/day => kg/kg/s
            if var[v]=='MMF_RES_DQT'  :
               data = data + case_obj.load_data('MMF_DQC', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQI', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQR', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data/(86400.*1e3) # undo automatic unit conversion g/kg/day => kg/kg/s
               # data = data + case_obj.load_data('MMF_QTLS', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
            if var[v]=='MMF_DQT':
               data = data + case_obj.load_data('MMF_DQC', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQI', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data + case_obj.load_data('MMF_DQR', component=get_comp(case[c]),htype=htype,lev=lev,first_file=first_file,num_files=num_files)
               data = data/(86400.*1e3) # undo automatic unit conversion g/kg/day => kg/kg/s

            # Convert to daily mean
            if htype in ['h1','h2'] and daily_mean: data = data.resample(time='D').mean(dim='time')

            # Get rid of lev and ncol dimensions dimension
            if 'ncol' in data.dims : data = data.isel(ncol=0)
            # if 'lev'  in data.dims : data = data.isel(lev=-1)

            # vertically integrate
            if 'lev' in data.dims :
               ps = case_obj.load_data('PS',component=get_comp(case[c]),htype=htype,first_file=first_file,num_files=num_files)
               if daily_mean: ps = ps.resample(time='D').mean(dim='time')
               hyai = case_obj.load_data('hyai',component=get_comp(case[c]),htype=htype,num_files=1)
               hybi = case_obj.load_data('hybi',component=get_comp(case[c]),htype=htype,num_files=1)
               dp3d = np.zeros( data.shape )
               hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values, dp3d )
               # hc.print_stat(dp3d,name='dp3d',stat='naxs',indent=' '*6,compact=True)
               # hc.print_stat(ps  ,name='ps'  ,stat='naxs',indent=' '*6,compact=True)
               dp3d = xr.DataArray(dp3d,dims=data.dims)
               data = ( data * dp3d ).sum(dim='lev') / dp3d.sum(dim='lev')

            if 'crm_nz' in data.dims :
               ps = case_obj.load_data('PS',component=get_comp(case[c]),htype=htype,first_file=first_file,num_files=num_files)
               if daily_mean: ps = ps.resample(time='D').mean(dim='time')
               hyai = case_obj.load_data('hyai',component=get_comp(case[c]),htype=htype,num_files=1)
               hybi = case_obj.load_data('hybi',component=get_comp(case[c]),htype=htype,num_files=1)
               dp3d = np.zeros( data.shape )
               hyai = hyai.isel(ilev=slice(61-1,10-1,-1))
               hybi = hybi.isel(ilev=slice(61-1,10-1,-1))
               hybrid_dp3d( ps.values, 1e5, hyai.values, hybi.values, dp3d )
               dp3d = xr.DataArray(dp3d*-1,dims=data.dims)
               data = ( data * dp3d ).sum(dim='crm_nz') / dp3d.sum(dim='crm_nz')
               data = data.mean(dim='crm_nx').isel(crm_ny=0)
               
            if var[v]=='MMF_RES_DQT'  :
               data = data*dp3d.sum(dim='lev')  # convert column average back to integral
               data = data/(9.81*1e3)           # normalize by rho_w*g to get m/s
               tmp_prec = case_obj.load_data('PRECT',htype='h1',lev=lev,first_file=first_file,num_files=num_files)
               tmp_prec = tmp_prec/(86400.*1e3) # undo automatic unit conversion mm/day => m/s
               # hc.print_stat(data_old*(86400.*1e3),name=var[v],stat='naxs',indent=' '*6,compact=True)
               data = data + tmp_prec
               # hc.print_stat(tmp_prec*(86400.*1e3),name='PRECT',stat='naxs',indent=' '*6,compact=True)
               # hc.print_stat(data    *(86400.*1e3),name=var[v],stat='naxs',indent=' '*6,compact=True)
               data = data * 86400.*1e3 # convert m/s to mm/day
               if 'ncol' in data.dims : data = data.isel(ncol=0)
               # exit()
            if 'MMF_DQT' in var[v]:
               data = data*dp3d.sum(dim='lev')  # convert column average back to integral
               data = data/(9.81*1e3)           # normalize by rho_w*g to get m/s
               data = data * 86400.*1e3         # convert m/s to mm/day
      #-------------------------------------------------------------------------
      if data is not None:

         # reset time index to start at zero and convert to days
         # dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')

         # Make time start at zero
         data['time'] = ( data['time'] - data['time'][0] ).astype('float') #/ 86400e9

         if case[c]!='obs': data['time'] = data['time'] / 86400e9

         # print(); print(data)
         # print(); print(data['time'])

         # for t in data['time'].values: print(t)
         # exit()

      #-------------------------------------------------------------------------
      # unit conversions
      # if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3
      # if var[v] in ['PRECT','PRECC','PRECL'] : data = data*1e6

      # if var[v] in ['PRECT','PRECC','PRECL'] and 'PAM-C' in case[c] : data = data*86400
      # if var[v] in ['PRECT','PRECC','PRECL'] and 'PAM-C' in case[c] : print(data); exit()
      #-------------------------------------------------------------------------
      if print_stats and data is not None: 
         # print(' '*6+f'Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')
         hc.print_time_length(data.time,indent=' '*6)
         hc.print_stat(data,name=var[v],stat='naxs',indent=' '*6,compact=True)
         # time_mean = data.mean(dim='time').values
         # print('      Area Weighted Time Mean : '+hc.tcolor.GREEN+f'{time_mean:10.6f}'+hc.tcolor.ENDC)
      #-------------------------------------------------------------------------
      if data is not None:
         data_list.append( data.values )
         time_list.append( data['time'].values )
      else:
         data_list.append( None )
         time_list.append( None )

      #-------------------------------------------------------------------------
      # write to file
      #-------------------------------------------------------------------------
      # if write_file : 
      #    tfile = f'/global/homes/w/whannah/E3SM/scratch/{case[0]}/run/{case[0]}.time_series.{var[v]}.nc'
      #    print('writing to file: '+tfile)
      #    avg_X.name = var[v]
      #    avg_X.to_netcdf(path=tfile,mode='w')
      #    exit()

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.tiXAxisString = 'Time [days]'

   ### Make sure plot bounds are consistent
   # tres.trYMinF = np.min([np.nanmin(d) for d in data_list])
   # tres.trYMaxF = np.max([np.nanmax(d) for d in data_list])
   # tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
   # tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])

   data_min_list = []
   data_max_list = []
   time_min_list = []
   time_max_list = []
   for d in data_list:
      if d is not None:
         data_min_list.append(np.nanmin(d))
         data_max_list.append(np.nanmax(d))
   for d in time_list:
      if d is not None:
         time_min_list.append(np.nanmin(d))
         time_max_list.append(np.nanmax(d))
   tres.trYMinF = np.min(data_min_list)
   tres.trYMaxF = np.max(data_max_list)
   tres.trXMinF = np.min(time_min_list)
   tres.trXMaxF = np.max(time_max_list)


   # if var[v]=='NET_TOA_RAD':
   #    tres.trYMinF = -20
   #    tres.trYMaxF =  20
   # if 'MMF_DT' in var[v]:
   #    tres.trYMinF = -5
   #    tres.trYMaxF =  5

   print()
   for c in range(num_case):
      if data_list[c] is not None:
         ip = c*num_var + v
         if overlay_cases: ip = v
         
         tres.xyLineColor   = clr[c]
         tres.xyDashPattern = dsh[c]

         tplot = ngl.xy(wks, time_list[c], data_list[c], tres)
      
         if overlay_cases: 
            # if c==0: 
            if plot[ip] is None:
               plot[ip] = tplot
            else:
               ngl.overlay(plot[ip],tplot)
         else:
            plot[ip] = tplot

      #------------------------------------------------
      # add linear trend
      #------------------------------------------------
      if add_trend:
         px = time_list[c]
         py = data_list[c]
         # simple and fast method for regression coeff and intercept
         a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )
         b = np.mean(py) - a*np.mean(px)

         print(f'    linear regression a: {a}    b: {b}')

         px_range = np.abs( np.max(px) - np.min(px) )
         lx = np.array([-1e2*px_range,1e2*px_range])

         lres.xyLineColor = clr[c]
         ngl.overlay( plot[ip], ngl.xy(wks, lx, lx*a+b , lres) )
      #------------------------------------------------
      #------------------------------------------------
      ### add horizontal line at zero
      zres = hs.res_xy()
      zres.xyLineThicknessF = 1
      zres.xyDashPattern = 0
      zres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([-1e15,1e15]), np.array([0,0]), zres))


   # if overlay_cases:
   #    ip = v

   #    ### use this for overlaying variables on same plot
   #    for c in range(num_case):
   #       tres.xyLineColor   = clr[c]
   #       tres.xyDashPattern = dsh[c]
   #       tplot = ngl.xy(wks, time_list[c], data_list[c], tres)
   #       if c==0: 
   #          plot[ip] = tplot
   #       else:
   #          ngl.overlay(plot[ip],tplot)

   #       #------------------------------------------------
   #       # add linear trend
   #       #------------------------------------------------
   #       if add_trend:
   #          px = time_list[c]
   #          py = data_list[c]
   #          # simple and fast method for regression coeff and intercept
   #          a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )
   #          b = np.mean(py) - a*np.mean(px)

   #          print(f'\n    linear regression a: {a}    b: {b}\n')

   #          px_range = np.abs( np.max(px) - np.min(px) )
   #          lx = np.array([-1e2*px_range,1e2*px_range])

   #          lres.xyLineColor = clr[c]
   #          ngl.overlay( plot[ip], ngl.xy(wks, lx, lx*a+b , lres) )
   #       #------------------------------------------------
   #       #------------------------------------------------
   # else:
   #    for c in range(num_case):
   #       ip = c*num_var + v
   #       # ip = v*num_case + c
   #       tres.xyLineColor   = clr[c]
   #       tres.xyDashPattern = dsh[c]
   #       plot[ip] = ngl.xy(wks, time_list[c], data_list[c], tres)


   #----------------------------------------------------------------------------
   # Set strings at top of plot
   #----------------------------------------------------------------------------
   var_str = var[v]
   # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
   if var[v]=="TMQ"   : var_str = "Column Water Vapor [mm]"

   lft_str = ''
   ctr_str = ''
   # if var[v] in ['PRECT','PRECC','PRECL'] : ctr_str = 'Mean: '+'%.2f'%avg_X+' [mm/day]'

   if overlay_cases:
      hs.set_subtitles(wks, plot[ip], var_str, ctr_str, '', font_height=0.012)
   else:
      hs.set_subtitles(wks, plot[ip], case_name[c], ctr_str, var_str, font_height=0.015)

   #----------------------------------------------------------------------------
   # Ad legend
   #----------------------------------------------------------------------------
   lgres = ngl.Resources()
   lgres.vpWidthF, lgres.vpHeightF  = 0.08, 0.08  
   lgres.lgLabelFontHeightF = 0.01
   lgres.lgLineThicknessF   = 4
   lgres.lgMonoLineColor    = False
   # lgres.lgMonoDashIndex    = True
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh
   lgres.lgLabelJust    = 'CenterLeft'
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.5, 0.4, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.5, 0.1, lgres)  # 3x2
   # pid = ngl.legend_ndc(wks, len(case_name), case_name, 0.3, 0.5, lgres)  # 1x2

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
hc.printline()

if 'num_plot_col' in locals():
   if overlay_cases :
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   else:
      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      else:
         layout = [num_var,num_case]
         # layout = [num_case,num_var]
else:
   layout = [num_var,num_case]


ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
