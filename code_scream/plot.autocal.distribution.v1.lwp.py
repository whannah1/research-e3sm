# v1 - original
# v2 - calculate dist for each point first as a way to handle area weighting issues
import os, ngl, xarray as xr, numpy as np, pandas as pd, glob
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
home = os.getenv("HOME")
print()
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------

tmp_scratch = '/global/cfs/cdirs/e3smdata/simulations/ecp-autotune/ne256-nudged-feb21-knob1/ndy1ne256'
# tmp_sub = 'SCREAM.2024-autocal-00.ne1024pg2/run'
tmp_sub = 'SCREAM.2024-autocal-00.ne256pg2/run'

file_name = 'output.scream.AutoCal.daily_avg_ne30pg2.AVERAGE.nhours_x24.2016-08-07-00000.nc'

debug = True

if debug:
   add_case(f'm0001', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0003', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0006', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0009', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0017', p=tmp_scratch,s=tmp_sub)
else:
   case_path_list = sorted(glob.glob(f'{tmp_scratch}/m*'))
   for c in range(len(case_path_list)):
      add_case( case_path_list[c].replace(f'{tmp_scratch}/',''), p=tmp_scratch,s=tmp_sub)

# var = 'LW_flux_up_at_model_top'; var_str = 'LW up @ TOM'
var = 'tlwp'; var_str = 'Liq + Rain Water Path'

fig_file,fig_type = home+'/Research/E3SM/figs_scream/autocal.distribution.v1.lwp', 'png'

obs_root = '/global/cfs/projectdirs/e3smdata/simulations/SCREAM.2024-autocal-00.ne1024pg2/obs'
obs_file = f'{obs_root}/mac.clwp-tlwp-wvp.20200126.ne30pg2.nc'
obs_var = 'tlwp'


# obs_root = '/global/cfs/cdirs/e3sm/terai/Obs_datasets/'

# obs_root ='/global/cfs/cdirs/m3312/jlee1046/SCREAM_AutoTuning/plotting_scripts/Autotuning_ne256_DYAMOND1/post_processed_data'
# obs_file = f'{obs_root}/DailyAvg_3B-HHR.MS.MRG.3IMERG.20160807.V06B.HDF5.regrid.ne30pg2.nc'
# obs_var  = 'precipitationCal'


print_stats    = False
print_glb_mean = False

if debug: print_stats = True

# if var=='tlwp': bin_min, bin_max, bin_spc = 0, 100, 4
# if var=='tlwp': bin_min_ctr, bin_min_wid, bin_spc_pct, nbin = 1, 2, 10., 38
# if var=='tlwp': bin_min_ctr, bin_min_wid, bin_spc_pct, nbin = .1, .2, 10., 70
if var=='tlwp': bin_min_ctr, bin_min_wid, bin_spc_pct, nbin = .01, .02, 10., 80
#---------------------------------------------------------------------------------------------------
# use this code for checking bin sizes and range
#---------------------------------------------------------------------------------------------------
# bin_min, bin_spc, bin_spc_log, nbin_log = 0.04, 0.02, 25., 40
bin_min, bin_spc, bin_spc_log, nbin_log = 0.1, 0.02, 18., 60
# bin_min, bin_spc, bin_spc_log, nbin_log = 0.105, 0.01, 10., 100  # Gabe's 2016 paper
bin_log_wid = np.zeros(nbin_log)
bin_log_ctr = np.zeros(nbin_log)
bin_log_ctr[0] = bin_min
bin_log_wid[0] = bin_spc
for b in range(1,nbin_log):
   bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
   bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
nbin = nbin_log
bin_coord = xr.DataArray( bin_log_ctr )
ratio = bin_log_wid / bin_log_ctr
for b in range(nbin_log): print('  center: {:12.6f}      width: {:12.6f}     ratio: {:6.4f}'.format(bin_log_ctr[b],bin_log_wid[b],ratio[b]))
exit()
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*1
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 2
# res.tmYLLabelFontHeightF      = 0.008
# res.tmXBLabelFontHeightF      = 0.008
res.xyXStyle                  = 'Log'
res.xyYStyle                  = 'Log'
res.tiXAxisString = var_str

ngl.define_colormap(wks,'BlAqGrYeOrReVi200')
# clr[1:] = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case-1,dtype=int)
clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)

# res.xyLineColors              = clr
# res.xyDashPatterns            = np.zeros(num_case)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# nbin    = np.round( ( bin_max - bin_min + bin_spc )/bin_spc ).astype(int)
def calculate_distribution(data):
   #----------------------------------------------------------------------------
   bin_min = bin_min_ctr ; bin_spc = bin_min_wid ; bin_spc_log = bin_spc_pct
   bin_log_wid = np.zeros(nbin)
   bin_log_ctr = np.zeros(nbin)
   bin_log_ctr[0] = bin_min
   bin_log_wid[0] = bin_spc
   for b in range(1,nbin):
      bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
      bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
   bin_coord = xr.DataArray( bin_log_ctr )
   #----------------------------------------------------------------------------
   # bins    = np.linspace(bin_min,bin_max,nbin)
   # bin_coord = xr.DataArray( bins )
   #----------------------------------------------------------------------------
   # for b in range(nbin): 
   #    ratio = bin_log_wid[b] / bin_log_ctr[b]
   #    print('  center: {:12.6f}      width: {:12.6f}     ratio: {:6.4f}'.format(bin_log_ctr[b],bin_log_wid[b],ratio))
   # exit()
   #----------------------------------------------------------------------------
   # create output data arrays
   ntime = len(data['time']) if 'time' in data.dims else 1   
   
   shape,dims,coord = (nbin,),'bins',[('bins', bin_coord.values)]

   bin_cnt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
   bin_amt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
   bin_frq = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
   bin_std = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )

   condition = xr.DataArray( np.full(data.shape,False,dtype=bool), coords=data.coords )

   wgt, *__ = xr.broadcast(area,data)
   wgt = wgt.transpose()

   data_area_wgt = (data*wgt) / wgt.sum()

   ones_area_wgt = xr.DataArray( np.ones(data.shape), coords=data.coords )
   ones_area_wgt = ( ones_area_wgt*wgt ) / wgt.sum()
   #----------------------------------------------------------------------------
   # Loop through bins
   for b in range(nbin):
      bin_bot = bin_min - bin_spc/2. + bin_spc*(b  )
      bin_top = bin_min - bin_spc/2. + bin_spc*(b+1)
      condition.values = ( data.values >=bin_bot )  &  ( data.values < bin_top )
      bin_cnt[b] = condition.sum()
      if bin_cnt[b]>0 :
         # bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2
         bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2 #/ (bin_spc_pct/1e2)
         # bin_amt[b] = data_area_wgt.where(condition,drop=True).sum() 
         # bin_std[b] = data_area_wgt.where(condition,drop=True), np.std( Vy_tmp ) )
         # bin_std[b] = data_area_wgt.where(condition,drop=True).std()
   #----------------------------------------------------------------------------
   frequency = np.ma.masked_invalid( bin_frq.values )
   amount    = np.ma.masked_invalid( bin_amt.values )
   stddev    = np.ma.masked_invalid( bin_std.values )
   return ( frequency, amount, stddev, bin_coord.values )
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# handle Obs data
if True:
   print(" "*4+f'case: Observations')
   #-------------------------------------------------------------------------
   # read the data
   ds = xr.open_dataset( obs_file )
   area = ds['area']
   data = ds[obs_var].isel(time=0)
   #-------------------------------------------------------------------------
   # convert units
   # data = data*? # g/m2 => kg/m2
   #-------------------------------------------------------------------------
   if print_stats: hc.print_stat(data,name=f'\n{var} (obs)',indent=' '*4,compact=True)
   #-------------------------------------------------------------------------
   # # calculate area weighted global mean for sanity check
   # wgt, *__ = xr.broadcast(area, data) 
   # gbl_mean = ( (data*wgt).sum() / wgt.sum() ).values 
   #-------------------------------------------------------------------------
   ( obs_frequency, obs_amount, obs_stddev, obs_bin_coord ) = calculate_distribution(data)

   # if print_stats: hc.print_stat(obs_amount,name=f'\n{var} (obs)',indent=' '*4,compact=True)

#---------------------------------------------------------------------------------------------------
frq_list = []
amt_list = []
bin_list = []
std_list = []

for c in range(num_case):
   print(" "*4+f'case: {case[c]}')
   #-------------------------------------------------------------------------
   # read the data
   # print(f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_name}')
   # exit()
   ds = xr.open_dataset( f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_name}' )
   area = ds['area']
   data = ds['LiqWaterPath'] + ds['RainWaterPath']
   data = data.isel(time=0)
   #-------------------------------------------------------------------------
   # convert units
   data = data*1e3 # kg/m2 => g/m2
   #-------------------------------------------------------------------------
   if print_stats: hc.print_stat(data,name=f'\n{var}',indent=' '*4,compact=True)
   #-------------------------------------------------------------------------
   # calculate area weighted global mean for sanity check
   wgt, *__ = xr.broadcast(area, data) 
   gbl_mean = ( (data*wgt).sum() / wgt.sum() ).values 
   #-------------------------------------------------------------------------
   ( frequency, amount, stddev, bin_coord ) = calculate_distribution(data)

   frq_list.append( frequency )
   amt_list.append( amount )
   std_list.append( stddev )
   bin_list.append( bin_coord )
   #----------------------------------------------------------------------------
   # sanity check
   # if 'gbl_mean' in locals() and print_glb_mean:
      # print('\n'+f'    Area weighted global mean:  {gbl_mean} ')
      # print(     f'    sum of rain amount dist:    {np.sum(amount*(bin_spc_pct/1e2))} \n')
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

# res.xyExplicitLegendLabels = case_name
# res.pmLegendDisplayMode    = "Always"
# res.pmLegendOrthogonalPosF = -1.13
# res.pmLegendParallelPosF   =  0.8+0.04
# res.pmLegendWidthF         =  0.16
# res.pmLegendHeightF        =  0.12   
# res.lgBoxMinorExtentF      =  0.16   

res.trXMinF = np.min(bin_list)
res.trXMaxF = np.max(bin_list)

res.trYMinF = np.min( [ np.min(frq_list), np.min(obs_frequency) ] )
res.trYMaxF = np.max( [ np.max(frq_list), np.max(obs_frequency) ] )

for c in range(num_case):
   res.xyLineColor = clr[c]
   if c==0: res.tiYAxisString = 'Frequency [%]'
   tplot1 = ngl.xy(wks, bin_list[c] , frq_list[c] ,res)
   # if c==0: res.tiYAxisString = 'Rain Amount [mm/day]'
   # tplot2 = ngl.xy(wks, bin_list[c] , amt_list[c] ,res)
   if c==0:
      plot[0] = tplot1 
      # plot[1] = tplot2 
   else:
      ngl.overlay( plot[0], tplot1 )
      # ngl.overlay( plot[1], tplot2 )

# Overlay obs data
obs_frequency, obs_amount, obs_bin_coord
res.xyLineColor = 'black'
res.xyLineThicknessF = 10
ngl.overlay( plot[0], ngl.xy(wks, obs_bin_coord , obs_frequency , res) )
# ngl.overlay( plot[1], ngl.xy(wks, obs_bin_coord , obs_amount    , res) )

# Overlay mean across ensemble data
obs_frequency, obs_amount, obs_bin_coord
res.xyLineColor = 'black'
res.xyDashPattern = 2
# res.xyLineThicknessF = res.xyLineThicknessF * 2
amt_list_avg,frq_list_avg = amt_list[0]*0,frq_list[0]*0
for c in range(num_case):
   for b in range(nbin):
      amt_list_avg[b] += amt_list[c][b]/num_case
      frq_list_avg[b] += frq_list[c][b]/num_case
ngl.overlay( plot[0], ngl.xy(wks, bin_list[0] , frq_list_avg , res) )
# ngl.overlay( plot[1], ngl.xy(wks, bin_list[0] , amt_list_avg    , res) )


#---------------------------------------------------------------------------------------------------
# # append standard deviation panels
# amt_list_std,frq_list_std = np.zeros(nbin), np.zeros(nbin)
# for b in range(nbin):
#    amt_list_tmp,frq_list_tmp = np.zeros(num_case), np.zeros(num_case)
#    for c in range(num_case):
#       amt_list_tmp[c] = amt_list[c][b]
#       frq_list_tmp[c] = frq_list[c][b]
#    amt_list_std[b] = np.std( amt_list_tmp )
#    frq_list_std[b] = np.std( frq_list_tmp )
#    amt_list_avg[b] = np.mean( amt_list_tmp )
#    frq_list_avg[b] = np.mean( frq_list_tmp )
# plot.append( None )
# plot.append( None )
# plot[2] = ngl.xy(wks, bin_list[0] , frq_list_std , res)
# plot[3] = ngl.xy(wks, bin_list[0] , amt_list_std , res)

#
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
num_plot_col = 2
layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
