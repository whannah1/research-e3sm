# v1 - original
# v2 - calculate dist for each point first as a way to handle area weighting issues
import os, ngl, xarray as xr, numpy as np, pandas as pd, glob, subprocess as sp
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
home = os.getenv("HOME")
print()
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------

tmp_scratch = '/global/cfs/cdirs/e3smdata/simulations/ecp-autotune/ne256-nudged-feb21-knob1/ndy1ne256'
# tmp_sub = 'SCREAM.2024-autocal-00.ne1024pg2/run'
tmp_sub = 'SCREAM.2024-autocal-00.ne256pg2/run'

file_name = 'output.scream.AutoCal.daily_avg_ne30pg2.AVERAGE.nhours_x24.2016-08-07-00000.nc'

debug = True

if debug:
   add_case(f'm0001', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0003', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0006', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0009', p=tmp_scratch,s=tmp_sub)
   add_case(f'm0017', p=tmp_scratch,s=tmp_sub)
else:
   case_path_list = sorted(glob.glob(f'{tmp_scratch}/m*'))
   for c in range(len(case_path_list)):
      case_tmp = case_path_list[c].replace(f'{tmp_scratch}/','')
      if len(case_tmp)==5:
         add_case( case_tmp, p=tmp_scratch,s=tmp_sub)

var = 'LW_flux_up_at_model_top'; var_str = 'LW up @ TOM'
# var = 'SW_flux_up_at_model_top'; var_str = 'SW up @ TOM'

fig_file,fig_type = home+'/Research/E3SM/figs_scream/autocal.zonal-mean.v1.rad', 'png'

obs_root = '/lustre/orion/cli115/proj-shared/hannah6/SCREAM.2024-autocal-00.ne1024pg2/obs/'
# obs_root = '/global/cfs/projectdirs/e3smdata/simulations/SCREAM.2024-autocal-00.ne1024pg2/obs'

if var=='LW_flux_up_at_model_top': obs_file = f'{obs_root}/CERES.LW_flux_up_at_model_top.AVERAGE.ne30pg2.20200126.nc'
if var=='SW_flux_up_at_model_top': obs_file = f'{obs_root}/CERES.SW_flux_up_at_model_top.AVERAGE.ne30pg2.20200126.nc'
obs_var = var

lat1,lat2,bin_dlat = -88,88,2


print_stats    = False
print_glb_mean = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*1
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 2
# res.tmYLLabelFontHeightF      = 0.008
# res.tmXBLabelFontHeightF      = 0.008
# res.xyXStyle                  = 'Log'

res.tiXAxisString = 'Latitude'
# res.tiXAxisString = var_str

ngl.define_colormap(wks,'BlAqGrYeOrReVi200')
# clr[1:] = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case-1,dtype=int)
clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)

# res.xyLineColors              = clr
# res.xyDashPatterns            = np.zeros(num_case)


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# handle Obs data
if True:
   print(" "*4+f'case: Observations')
   #-------------------------------------------------------------------------
   # read the data
   ds = xr.open_dataset( obs_file )
   area = ds['area']
   lat = ds['lat']
   data = ds[obs_var].isel(time=0)
   #-------------------------------------------------------------------------
   # convert units
   if obs_var == 'precip_total_surf_mass_flux': data = data*86400*1e3
   if obs_var == 'precipitationCal': data = data*24. # mm/hr to mm/day
   #-------------------------------------------------------------------------
   if print_stats: hc.print_stat(data,name=f'\n{var} (obs)',indent=' '*4,compact=True)
   #-------------------------------------------------------------------------
   # # calculate area weighted global mean for sanity check
   # wgt, *__ = xr.broadcast(area, data) 
   # gbl_mean = ( (data*wgt).sum() / wgt.sum() ).values 
   #-------------------------------------------------------------------------
   bin_ds = hc.bin_YbyX( data, lat, bin_min=lat1, bin_max=lat2, bin_spc=bin_dlat, wgt=area )

   obs_data = bin_ds['bin_val'].values

   # if print_stats: hc.print_stat(obs_amount,name=f'\n{var} (obs)',indent=' '*4,compact=True)

#---------------------------------------------------------------------------------------------------
data_list,std_list,cnt_list = [],[],[]
bin_list = []


case_cnt = 0
for c in range(num_case):
# for c in range(10):
   msg = " "*4+f'case: {case[c]}'
   #-------------------------------------------------------------------------
   os.chdir(f'{case_dir[c]}/{case[c]}/{case_sub[c].replace("/run","/case_scripts")}')
   p3_ice_sed_knob = sp.check_output(['./atmquery','--value','p3_ice_sed_knob'],universal_newlines=True)
   p3_ice_sed_knob = float(p3_ice_sed_knob)
   msg += f'   p3_ice_sed_knob: {p3_ice_sed_knob}'
   if p3_ice_sed_knob < 1.0 : print(msg+'  (skipping)'); continue
   #-------------------------------------------------------------------------
   print(msg)
   #-------------------------------------------------------------------------
   # read the data
   # print(f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_name}')
   # exit()
   file_tmp = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_name}'
   if not os.path.exists( file_tmp ): continue
   ds = xr.open_dataset( file_tmp )
   area = ds['area']
   lat = ds['lat']
   data = ds[var].isel(time=0)
   #-------------------------------------------------------------------------
   # convert units
   if var == 'precip_total_surf_mass_flux': data = data*86400*1e3
   #-------------------------------------------------------------------------
   if print_stats: hc.print_stat(data,name=f'\n{var}',indent=' '*4,compact=True)
   #-------------------------------------------------------------------------
   # calculate area weighted global mean for sanity check
   wgt, *__ = xr.broadcast(area, data) 
   gbl_mean = ( (data*wgt).sum() / wgt.sum() ).values 
   #-------------------------------------------------------------------------
   bin_ds = hc.bin_YbyX( data, lat, bin_min=lat1, bin_max=lat2, bin_spc=bin_dlat, wgt=area )

   data_list.append( bin_ds['bin_val'].values )

   lat_bins = bin_ds['bins'].values

   sin_lat_bins = np.sin(lat_bins*np.pi/180.)

   bin_list.append( lat_bins )


   case_cnt += 1

num_case = case_cnt

#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

res.trXMinF = np.min(bin_list)
res.trXMaxF = np.max(bin_list)

# lat_tick = np.array([-90,-60,-30,0,30,60,90])
# res.tmXBMode = "Explicit"
# res.tmXBValues = np.sin( lat_tick*3.14159/180. )
# res.tmXBLabels = lat_tick

for c in range(num_case):
   res.xyLineColor = clr[c]
   if c==0: res.tiYAxisString = var_str
   tplot = ngl.xy(wks, bin_list[c], np.ma.masked_invalid( data_list[c] ), res)
   if c==0:
      plot[0] = tplot
   else:
      ngl.overlay( plot[0], tplot )

# Overlay obs data
res.xyLineColor = 'black'
res.xyLineThicknessF = 10
ngl.overlay( plot[0], ngl.xy(wks, bin_list[0] , obs_data , res) )

# # Overlay mean across ensemble data
# res.xyLineColor = 'black'
# res.xyDashPattern = 2
# res.xyLineThicknessF = 10
# data_list_avg = data_list[0]*0
# for c in range(num_case):
#    for b in range(len(bin_list[0])):
#       data_list_avg[b] += data_list[c][b]/num_case
# ngl.overlay( plot[0], ngl.xy(wks, bin_list[0] , data_list_avg , res) )


#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

num_plot_col = 1
layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

os.chdir(home+'/Research/E3SM/')

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
