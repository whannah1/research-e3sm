import os, glob, ngl, copy, xarray as xr, numpy as np, cmocean
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
lx_list,ne_list = [],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,ne=None,lx=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = '' if n is None else n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   lx_list.append(lx); ne_list.append(ne)

# case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
# lx_list,ne_list = [],[]
# def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,lx=None,ne=None):
#    global name,case,case_dir,case_sub,clr,dsh,mrk
#    tmp_name = '' if n is None else n
#    case.append(case_in); case_name.append(tmp_name)
#    case_dir.append(p); case_sub.append(s); case_grid.append(g)
#    dsh.append(d) ; clr.append(c) ; mrk.append(m)
#    lx_list.append(lx); ne_list.append(ne)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
lev_list = []
def add_var(var_name,file_type,n='',lev=None):
   var.append(var_name)
   file_type_list.append(file_type)
   var_str.append(n)
   lev_list.append(lev)
#-------------------------------------------------------------------------------
host = None
if os.path.exists('/global/cfs/cdirs'): host = 'nersc'
if os.path.exists('/lustre/orion'):     host = 'olcf'
#-------------------------------------------------------------------------------
if host=='nersc':
   scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
   scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'


   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',      n='DYNAMO 400km',      c='red',  p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acn_1',n='DYNAMO ne44 400km acn=1',c='green',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acc_1',n='DYNAMO ne44 400km acc=1',c='blue', p=scratch_gpu,s='run')

   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L276v1.1',n='DYNAMO 400km L276',c='cyan', p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L256v1.1',n='DYNAMO 400km L256',c='blue', p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.2',n='DYNAMO ne44 400km L128v2.2',c='cyan',    p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.1',n='DYNAMO ne44 400km L128v2.1',c='magenta', p=scratch_gpu,s='run')

   # first_file,num_files = 3,1

   #add_case('DPSCREAM.2024-RCE-00.ne44.len_400km.DT_60',ne=44,lx=400,c='red',n='RCE old',    p=scratch_gpu,s='run')
   #add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60',ne=44,lx=400,c='red',n='RCE new',    p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-RCE-00.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',    n='RCE 400km L128',    p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-00.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='RCE 400km L128v2.1',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-00.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='RCE 400km L128v2.2',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-00.ne44.len_400km.DT_60.vgrid_L256v1.1',ne=44,lx=400,c='cyan',   n='RCE 400km L256v1.1',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-00.ne44.len_400km.DT_60.vgrid_L276v1.1',ne=44,lx=400,c='magenta',n='RCE 400km L276v1.1',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60',            ne=44,lx=400,n='GATE',       p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFR',       ne=44,lx=400,n='GATE FCFR',  p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFR_FCFI',  ne=44,lx=400,n='GATE FCFR+FCFI',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_40',     ne=44,lx=400,n='GATE acc_40',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_40.FCFR',ne=44,lx=400,n='GATE acc_40',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60',      ne=44,lx=400,n='GATE 400km',          p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFR', ne=44,lx=400,n='GATE 400km+frac_r=1', p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60',      ne=88,lx=800,n='GATE 800km',          p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.FCFR', ne=88,lx=800,n='GATE 800km+frac_r=1', p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.FCFR',          ne=88,lx=800, c='black',     n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.eri_0.25.FCFR', ne=88,lx=800, c='royalblue', n='GATE-IDEAL 800km eri_0.25',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.eci_0.1.FCFR',  ne=88,lx=800, c='chocolate', n='GATE-IDEAL 800km eci_0.1',p=scratch_gpu,s='run')
   
   minimal_suffix = 'no_ice.acp_0.0005555555.acr_0.0001.acq_1.0.acn_0.0.acc_0.0.rsc_0.0.FCFR'
   add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.FCFR',                           ne=88,lx=800, c='black', n='ctrl',p=scratch_gpu,s='run')
   add_case(f'DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.{minimal_suffix}',              ne=88,lx=800, c='black', n='minimal',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.FCFR',                    ne=88,lx=800, c='black', n='noice',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.rsc_0.0.FCFR',            ne=88,lx=800, c='black', n='noice + rsc=0.0',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.acc_0.0.FCFR',            ne=88,lx=800, c='black', n='noice + acc=0.0',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.rsc_0.0.FCFR',                  ne=88,lx=800, c='black', n='rsc=0.0',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acc_0.0.FCFR',                  ne=88,lx=800, c='black', n='acc=0.0',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acn_0.0.FCFR',                  ne=88,lx=800, c='black', n='acn=0.0',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acp_0.0005555555.FCFR',         ne=88,lx=800, c='black', n='acp=0.0005555555',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acq_1.0.FCFR',                  ne=88,lx=800, c='black', n='',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acr_0.0001.acq_1.0.FCFR',       ne=88,lx=800, c='black', n='acr=0.0001 + acq=1.0',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acr_0.0001.FCFR',               ne=88,lx=800, c='black', n='',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.acr_0.0001.acq_1.0.FCFR',ne=88,lx=800, c='black', n='noice + acr=0.0001 + acq=1.0',p=scratch_gpu,s='run')
   
   add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_1.0.eci_0.1',                           ne=88,lx=800, c='black', n='ctrl + rsc_1.0 + eci_0.1',p=scratch_gpu,s='run')
   add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_2.0.eci_0.2',                           ne=88,lx=800, c='black', n='ctrl + rsc_2.0 + eci_0.2',p=scratch_gpu,s='run')
   add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_3.0.eci_0.3',                           ne=88,lx=800, c='black', n='ctrl + rsc_3.0 + eci_0.3',p=scratch_gpu,s='run')
   add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_4.0.eci_0.4',                           ne=88,lx=800, c='black', n='ctrl + rsc_4.0 + eci_0.4',p=scratch_gpu,s='run')

   first_file,num_files = 25,1
   # first_file,num_files = 1,1

   
   # add_case('scream_dpxx_RCE_300K.stand.001b',ne=44,lx=400,c='magenta',n='scream_dpxx_RCE_300K.stand.001b',p='/pscratch/sd/b/bogensch/dp_screamxx',s='run')
   # first_file,num_files = 0,1

# acp => autoconversion_prefactor         0.0      1350.0
# acr => autoconversion_radius            100e-6   25.0e-6
# acq => autoconversion_qc_exponent       1.0      2.47
# acn => autoconversion_nc_exponent       0.0      1.79
# acc => accretion_prefactor              0.0      67.0
# rsc => rain_selfcollection_prefactor    0.0      5.78
# eci => cldliq_to_ice_collection_factor           0.5
# eri => rain_to_ice_collection_factor             1.0

#-------------------------------------------------------------------------------
if host=='olcf':
   scratch_frontier = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',       n='DYNAMO ne44 400km',p=scratch_frontier,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01-P3FRAC.ne44.len_400km.DT_60',n='DYNAMO ne44 400km',p=scratch_frontier,s='run')

   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60',      n='RCE ne11 100km',      c='red',   p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60',      n='RCE ne22 200km',      c='green', p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60',      n='RCE ne44 400km',      c='blue',  p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60.GFLUX',n='RCE ne11 100km GFLUX',c='blue',p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60.GFLUX',n='RCE ne22 200km GFLUX',c='green', p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60.GFLUX',n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
   add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-01.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')

   first_file,num_files = 3,2

#-------------------------------------------------------------------------------
tmp_file_type = 'output.scream.2D.1hr.AVERAGE.nhours_x1'
# tmp_file_type = 'output.scream.2D.30min.AVERAGE'
# tmp_file_type = 'scream.hourly.2d.AVERAGE.nhours_x1'

add_var('precip_total_surf_mass_flux', tmp_file_type, n='precip')
# add_var('VapWaterPath',                tmp_file_type, n='VapWP')
add_var('LiqWaterPath',                tmp_file_type, n='LiqWP')
# add_var('IceWaterPath',                tmp_file_type, n='IceWP')
add_var('surf_evap',                   tmp_file_type, n='surf_evap')
#add_var('wind_speed_10m',               tmp_file_type, n='wind_speed_10m')

tmp_file_type = 'output.scream.3D.1hr.AVERAGE.nhours_x1'
# tmp_file_type = 'output.scream.3D.5min'
#add_var('T_mid',               tmp_file_type, n='T_mid',lev=-1)
add_var('omega',               tmp_file_type, n='omega',lev=-50)

# num_plot_col = len(var)
num_plot_col = len(case)

#-------------------------------------------------------------------------------


### output figure type and name
fig_type = 'png'
# fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/dp-scream.xy.v1.'+str(time_slice).zfill(3)
# fig_file = 'figs_scream/dp-scream.xy.v1.'+str(time_slice).zfill(3)
fig_file = 'figs_scream/dp-scream.xy.v1'

# animation start, end, and stride
# time1,time2,dtime = 0,100,2

# Set final animation file (don't include file suffix)
gif_file = f'figs_scream/dp-scream.animation.xy.v1'

single_frame = True

create_png,create_gif = True,True
# create_png,create_gif = True,False
# create_png,create_gif = False,True

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# if 'input_file_name' not in locals():
#    files = glob.glob(os.getenv('HOME')+f'/SCREAM/scratch/{case_name}/run/{case_name}.eam.h1.*.nc')
#    input_file_name = sorted(files)[-2]
# print()
# print(f'  input_file_name: {input_file_name}')

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

res = hs.res_contour_fill()

# res.lbLabelBarOn = False

#---------------------------------------------------------------------------------------------------
def get_file_list(case,case_dir,case_sub,file_type):
   global first_file,num_files
   file_path = f'{case_dir}/{case}/{case_sub}/*{file_type}*'
   file_list = sorted(glob.glob(file_path))
   if 'first_file' in globals(): file_list = file_list[first_file:]
   if 'num_files' in globals(): file_list = file_list[:num_files]
   if file_list==[]:
      exit(f'\nERROR: no files found for file_path:\n{file_path}\n')
   return file_list
#---------------------------------------------------------------------------------------------------
# set animation length
#---------------------------------------------------------------------------------------------------
# v,c = 0,0
# data_dir_tmp,data_sub_tmp = None, None
# if case_dir[c] is not None: data_dir_tmp = case_dir[c]
# if case_sub[c] is not None: data_sub_tmp = case_sub[c]
# case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
# data = case_obj.load_data(var[v],  htype=htype,first_file=first_file,num_files=num_files,lev=lev_list[v])
# num_t = len(data.time)


v,c = 0,0
# file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_type_list[v]}*'
# file_list = sorted(glob.glob(file_path))
file_list = get_file_list(case[c],case_dir[c],case_sub[c],file_type_list[v])
ds = xr.open_mfdataset( file_list )
data = ds[var[v]]
num_t = len(data.time)


# print(); print(data.time)
# print(); print(f'num_t: {num_t}')
# exit()
#---------------------------------------------------------------------------------------------------
def get_coords(ncol,lx,ne):
  # lx = 200
  nx = int(np.sqrt(ncol))
  dx = lx/nx
  # ne = int(nx/2)

  uxi = np.linspace(0,lx,nx+1)
  uxc = uxi[:-1] + np.diff(uxi)/2

  xc = np.full(ncol,np.nan)
  yc = np.full(ncol,np.nan)

  for ny in range(ne):
    for nx in range(ne):
      for nyj in range(2):
        for nxi in range(2):
          g = ny*4*ne + nx*4 + nyj*2 + nxi
          i = nx*2+nxi
          j = ny*2+nyj
          xc[g] = uxc[i]
          yc[g] = uxc[j]

  return xc,yc
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
fig_file_list = ''
for t in range(num_t):
   fig_file = f'{gif_file}.{str(t).zfill(3)}'
   fig_file_list = fig_file_list+f' {fig_file}.{fig_type}'
   print(f'  t: {t:3d}     {fig_file}.png')
   if create_png :
      # wkres = ngl.Resources() ; npix = 1024 ; wkres.wkWidth,wkres.wkHeight=npix,npix
      wkres = ngl.Resources() ; npix = 2048 ; wkres.wkWidth,wkres.wkHeight=npix,npix
      # wkres = ngl.Resources() ; npix = 4096 ; wkres.wkWidth,wkres.wkHeight=npix,npix
      wks = ngl.open_wks(fig_type,fig_file,wkres)
      # wks = ngl.open_wks(fig_type,fig_file)
      plot = [None]*(num_case*num_var)
      for c in range(num_case):
         print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
         # data_dir_tmp,data_sub_tmp = None, None
         # if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         # if case_sub[c] is not None: data_sub_tmp = case_sub[c]
         # case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
         # xcenter = case_obj.load_data('crm_grid_x',htype=htype).values
         # ycenter = case_obj.load_data('crm_grid_y',htype=htype).values
         for v in range(num_var):
            
            print('      var: '+var[v])
            # if 'lev_list' in locals(): lev = lev_list[v]
            #-------------------------------------------------------------------
            # data = case_obj.load_data(var[v],  htype=htype,lev=lev_list[v],
            #                           first_file=first_file,num_files=num_files)
            # if 'time' in data.dims: data = data.isel(time=t)
            #-------------------------------------------------------------------
            # file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_type_list[v]}*'
            # file_list = sorted(glob.glob(file_path))
            # if 'first_file' in locals(): file_list = file_list[first_file:]
            # if 'num_files' in locals(): file_list = file_list[:num_files]
            file_list = get_file_list(case[c],case_dir[c],case_sub[c],file_type_list[v])
            #-------------------------------------------------------------------
            ds = xr.open_mfdataset( file_list )
            ds = ds.isel(time=t)
            data = ds[var[v]].load()
            #-------------------------------------------------------------------
            if 'lev' in data.dims and lev_list[v] is not None:
               if lev_list[v]<0: data = data.isel(lev=lev_list[v])
               if lev_list[v]>0: data = data.sel(lev=lev_list[v])
            #-------------------------------------------------------------------
            dy = ds['time.day'].values
            hr = ds['time.hour'].values
            mn = ds['time.minute'].values
            # timestamp = f'day h:m = {dy:02d} {hr:02d}:{mn:02d}'
            timestamp = f'{dy:02d} {hr:02d}:{mn:02d}'
            #-------------------------------------------------------------------
            # if v==0: xc,yc = get_coords( len(data) )
            xc,yc = get_coords( len(data), lx_list[c], ne_list[c] )
            #-------------------------------------------------------------------
            # unit conversions    
            if 'precip' in var[v]: data = data*86400.*1e3
            # if var[v]=='LiqWaterPath': data = data*1e3
            # if var[v]=='IceWaterPath': data = data*1e3
            
            # print(); print(data); print()
            # hc.print_stat(data)
            # exit()
            #-------------------------------------------------------------------
            tres = copy.deepcopy(res)
            tres.cnFillMode    = 'RasterFill'
            tres.sfXArray      = xc
            tres.sfYArray      = yc
            tres.cnFillPalette = "MPL_viridis"
            tres.cnLevelSelectionMode = 'ExplicitLevels'
            if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(1,60+1,1)
            # if var[v]=="TGCLDLWP"                  : tres.cnLevels = np.linspace( 0.1,10, num=60)
            # if var[v]=="TGCLDIWP"                  : tres.cnLevels = np.linspace( 0.1,10, num=60)
            if var[v]=='TGCLDLWP'                  : tres.cnLevels = np.logspace(-2,1.5,num=40).round(decimals=2)
            if var[v]=='TGCLDIWP'                  : tres.cnLevels = np.logspace(-2,1.5,num=40).round(decimals=2)
            # if 'precip' in var[v]         : tres.cnLevels = np.arange(1,40+1,2)
            if 'precip' in var[v]         : tres.cnLevels = np.logspace(-1,2.5,num=30)
            if var[v]=='VapWaterPath'     : tres.cnLevels = np.arange(4,64+4,4)
            # if var[v]=='LiqWaterPath'     : tres.cnLevels = np.linspace( 0.1,2.1, num=20)
            # if var[v]=='IceWaterPath'     : tres.cnLevels = np.linspace( 0.1,2.1, num=20)
            if var[v]=='LiqWaterPath'     : tres.cnLevels = np.logspace(-3,1.2,num=40)#.round(decimals=2)
            if var[v]=='IceWaterPath'     : tres.cnLevels = np.logspace(-3,1.2,num=40)#.round(decimals=2)
            if var[v]=='surf_evap'        : tres.cnLevels = np.arange(2e-5,12e-5,1e-5)

            if var[v]=='wind_speed_10m'     : tres.cnLevels = np.arange(0.5,5+0.5,0.5)
            if var[v]=='T_mid'              : tres.cnLevels = np.arange(295,300+0.5,0.5)

            if var[v]=='omega'              : tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
            if var[v]=='omega'              : tres.cnLevels = np.arange(-20,20+1,1)
            #-------------------------------------------------------------------
            # ip = c*num_var + v
            ip = v*num_case + c
            plot[ip] = ngl.contour(wks,data.values,tres)

            hs.set_subtitles(wks, plot[ip], 
                             left_string=case_name[c], 
                             center_string=timestamp, 
                             right_string=var_str[v], 
                             font_height=0.005)
      #-------------------------------------------------------------------------
      pres = ngl.Resources()
      pres.nglPanelYWhiteSpacePercent = 5
      pres.nglPanelXWhiteSpacePercent = 5
      # layout = [num_case,num_var]
      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      else:
         layout = [num_var,num_case]
         # layout = [num_case,num_var]
      ngl.panel(wks,plot,layout,pres)
      ngl.destroy(wks)
      hc.trim_png(fig_file,verbose=False)

      if single_frame: exit()

if create_png : ngl.end()
#-------------------------------------------------------------------------------
# Create animated gif
#-------------------------------------------------------------------------------
if create_gif : 
   # cmd = f'convert -repage 0x0 -delay 10 -loop 0  {fig_file_list} {gif_file}.gif'
   cmd = f'convert -repage 0x0 -delay 20 -loop 0  {fig_file_list} {gif_file}.gif'
   print(f'\n{cmd}\n')
   os.system(cmd)
   print(f'\n  {gif_file}.gif\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
