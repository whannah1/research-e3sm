import os, glob, ngl, copy, xarray as xr, numpy as np, numba
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
lx_list,ne_list = [],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,lx=None,ne=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = '' if n is None else n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   lx_list.append(lx); ne_list.append(ne)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
def add_var(var_name,file_type,n=None):
   var.append(var_name)
   file_type_list.append(file_type)
   if n is None:
      var_str.append(var_name)
   else:
      var_str.append(n)
#-------------------------------------------------------------------------------

# scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
# scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'
# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',n='DYNAMO ne44 400km',p=scratch_gpu,s='run')


scratch_frontier = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'

# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',       n='DYNAMO ne44 400km',p=scratch_frontier,s='run')
# add_case('DPSCREAM.DYNAMO-TEST-01-P3FRAC.ne44.len_400km.DT_60',n='DYNAMO ne44 400km',p=scratch_frontier,s='run')


# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60',      lx=100,n='RCE ne11 100km',      c='red',   p=scratch_frontier,s='run')
# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60',      lx=200,n='RCE ne22 200km',      c='green', p=scratch_frontier,s='run')
# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60',      lx=400,n='RCE ne44 400km',      c='blue',  p=scratch_frontier,s='run')
# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60.GFLUX',lx=100,n='RCE ne11 100km GFLUX',c='blue',p=scratch_frontier,s='run')
# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60.GFLUX',lx=200,n='RCE ne22 200km GFLUX',c='green', p=scratch_frontier,s='run')
# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60.GFLUX',lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-01.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
# add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-02.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')

add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-04.ne22.len_200km.DT_60.NN_2.GFLUX',ne=22,lx=200,n='DYNAMO ne22 200km',c='blue',p=scratch_frontier,s='run')

# first_file,num_files = 2,1
first_file,num_files = 2*4,1*4


#-------------------------------------------------------------------------------
# tmp_file_type = 'output.scream.3D.3hr.AVERAGE'
# tmp_file_type = 'output.scream.3D.30min.AVERAGE'
tmp_file_type = 'output.scream.3D.5min.AVERAGE'


# add_var('T_mid',              tmp_file_type, n=None)
# add_var('qv',                 tmp_file_type, n=None)
# add_var('qcri',                 tmp_file_type, n=None)
add_var('qci',                  tmp_file_type, n=None)
# add_var('qc',                   tmp_file_type, n=None)
# add_var('qi',                   tmp_file_type, n=None)
# add_var('omega',                tmp_file_type, n=None)
# add_var('horiz_winds_u',        tmp_file_type, n=None)
# add_var('z_mid',              tmp_file_type, n=None)
# add_var('rad_heating_pdel',   tmp_file_type, n=None)

# add_var('wind_speed_10m',               tmp_file_type, n='wind_speed_10m')

# tmp_file_type = 'output.scream.3D.3hr.AVERAGE.nhours_x3'
# add_var('T_mid',               tmp_file_type, n='T_mid',lev=None)

num_plot_col = 2#len(var)

#-------------------------------------------------------------------------------


### output figure type and name
fig_type = 'png'
fig_file = 'figs_scream/dp-scream.xz.v1'

# animation start, end, and stride
# time1,time2,dtime = 0,100,2

# Set final animation file (don't include file suffix)
gif_file = f'figs_scream/dp-scream.animation.xz.v1'

create_png,create_gif = True,True
# create_png,create_gif = True,False
# create_png,create_gif = False,True

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# if 'input_file_name' not in locals():
#    files = glob.glob(os.getenv('HOME')+f'/SCREAM/scratch/{case_name}/run/{case_name}.eam.h1.*.nc')
#    input_file_name = sorted(files)[-2]
# print()
# print(f'  input_file_name: {input_file_name}')

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

res = hs.res_contour_fill()
res.vpHeightF = 0.4
# res.trYReverse = True

#---------------------------------------------------------------------------------------------------
def get_file_list(case,case_dir,case_sub,file_type):
   global first_file,num_files
   file_path = f'{case_dir}/{case}/{case_sub}/{file_type}*'
   file_list = sorted(glob.glob(file_path))
   if 'first_file' in globals(): file_list = file_list[first_file:]
   if 'num_files' in globals(): file_list = file_list[:num_files]
   return file_list
#---------------------------------------------------------------------------------------------------
def get_data(ds,var):
   tvar = var
   #----------------------------------------------------------------------------
   if var=='horiz_winds_u': tvar = 'horiz_winds'
   if var=='horiz_winds_v': tvar = 'horiz_winds'
   if var=='qcri'         : tvar = 'qc'
   if var=='qci'          : tvar = 'qc'
   #----------------------------------------------------------------------------
   data = ds[tvar]#.load()
   #----------------------------------------------------------------------------
   if var=='horiz_winds_u': data = data.isel(dim2=0)
   if var=='horiz_winds_v': data = data.isel(dim2=1)
   if var=='qcri'         : data = data + ds['qr'] + ds['qi']
   if var=='qci'          : data = data + ds['qi']
   #----------------------------------------------------------------------------
   # unit conversions    
   if 'precip' in var: data = data*86400.*1e3
   if var in ['qc','qr','qi','qcri','qci']: data = data*1e3
   #----------------------------------------------------------------------------
   return data
#---------------------------------------------------------------------------------------------------
# set animation length
#---------------------------------------------------------------------------------------------------
# v,c = 0,0
# data_dir_tmp,data_sub_tmp = None, None
# if case_dir[c] is not None: data_dir_tmp = case_dir[c]
# if case_sub[c] is not None: data_sub_tmp = case_sub[c]
# case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
# data = case_obj.load_data(var[v],  htype=htype,first_file=first_file,num_files=num_files,lev=lev_list[v])
# num_t = len(data.time)


v,c = 0,0
# file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_type_list[v]}*'
# file_list = sorted(glob.glob(file_path))
file_list = get_file_list(case[c],case_dir[c],case_sub[c],file_type_list[v])
ds = xr.open_mfdataset( file_list )
num_t = len(get_data(ds,var[v]).time)


# print(); print(data.time)
# print(); print(f'num_t: {num_t}')
# exit()
#---------------------------------------------------------------------------------------------------
@numba.njit()
def get_coords(ncol,lx=None):
   nx = int(np.sqrt(ncol))
   ne = int(nx/2)
   uxi = np.linspace(0,lx,nx+1)
   uxc = uxi[:-1] + np.diff(uxi)/2
   xc = np.full(ncol,np.nan)
   yc = np.full(ncol,np.nan)
   for ny in range(ne):
      for nx in range(ne):
         for nyj in range(2):
            for nxi in range(2):
               g = ny*4*ne + nx*4 + nyj*2 + nxi
               i = nx*2+nxi
               j = ny*2+nyj
               xc[g] = uxc[i]
               yc[g] = uxc[j]
   return xc,yc
#---------------------------------------------------------------------------------------------------
@numba.njit()
def reorg_data(data,xc,yc,ncol,nlev):
   nx = int(np.sqrt(ncol))
   ne = int(nx/2)
   xx = np.zeros((nx,nx))
   yy = np.zeros((nx,nx))
   data_out = np.zeros((nx,nx,nlev))
   for ny in range(ne):
      for nx in range(ne):
         for nyj in range(2):
            for nxi in range(2):
               g = ny*4*ne + nx*4 + nyj*2 + nxi
               i = nx*2+nxi
               j = ny*2+nyj
               xx[i,j] = xc[g]
               yy[i,j] = yc[g]
               data_out[i,j,:] = data[g,:]
   return data_out,xx,yy
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
fig_file_list = ''
for t in range(num_t):
   fig_file = f'{gif_file}.{str(t).zfill(3)}'
   fig_file_list = fig_file_list+f' {fig_file}.{fig_type}'
   print(f'  t: {t:3d}     {fig_file}.png')
   if create_png :
      wks = ngl.open_wks(fig_type,fig_file)
      plot = [None]*(num_case*num_var)
      for c in range(num_case):
         print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
         # data_dir_tmp,data_sub_tmp = None, None
         # if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         # if case_sub[c] is not None: data_sub_tmp = case_sub[c]
         # case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
         # xcenter = case_obj.load_data('crm_grid_x',htype=htype).values
         # ycenter = case_obj.load_data('crm_grid_y',htype=htype).values
         for v in range(num_var):
            
            print('      var: '+var[v])
            if 'lev_list' in locals(): lev = lev_list[v]
            #-------------------------------------------------------------------
            # data = case_obj.load_data(var[v],  htype=htype,lev=lev_list[v],
            #                           first_file=first_file,num_files=num_files)
            # if 'time' in data.dims: data = data.isel(time=t)
            #-------------------------------------------------------------------
            # file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_type_list[v]}*'
            # file_list = sorted(glob.glob(file_path))
            # if 'first_file' in locals(): file_list = file_list[first_file:]
            # if 'num_files' in locals(): file_list = file_list[:num_files]
            file_list = get_file_list(case[c],case_dir[c],case_sub[c],file_type_list[v])
            #-------------------------------------------------------------------
            ds = xr.open_mfdataset( file_list ).isel(time=t)
            data = get_data(ds,var[v])

            data['lev'] = get_data(ds,'z_mid').isel(ncol=0)/1e3
            dz = 0.1
            data = data.interp(lev=np.arange(dz,20+dz,dz))
            #-------------------------------------------------------------------
            # if v==0: 
            xc,yc = get_coords( len(data), lx=lx_list[c] )
            xc = xr.DataArray(xc,dims=['ncol'])
            yc = xr.DataArray(yc,dims=['ncol'])
            #-------------------------------------------------------------------
            lev_tmp = data['lev']
            data,xc,yc = reorg_data(data.values,xc.values,yc.values,len(data['ncol']),len(data['lev']))

            data = xr.DataArray(data,coords={'x':xc[:,0],'y':yc[0,:],'lev':lev_tmp})

            ny = len(data['y'])
            data = data.isel(y=slice(int(ny/2-2),int(ny/2+2)+1)).mean(dim='y').transpose('lev','x')
            xc = data['x']

            #-------------------------------------------------------------------
            # dy = yc-(lx_list[c]/2)
            # condition = np.logical_and( dy>0, dy<4 )
            # data = data.where(condition,drop=True)
            # xc   = xc  .where(condition,drop=True)
            #-------------------------------------------------------------------
            # print(); print(data)
            # for x in xc.values: print(x)
            # exit()
            #-------------------------------------------------------------------
            # print(); print(data); print()
            # hc.print_stat(data)
            # exit()
            #-------------------------------------------------------------------
            tres = copy.deepcopy(res)
            tres.cnFillMode    = 'RasterFill'
            # tres.sfXArray      = xc.values
            tres.sfXArray      = data['x'].values
            tres.sfYArray      = data['lev'].values
            tres.cnFillPalette = 'MPL_viridis'
            tres.cnLevelSelectionMode = 'ExplicitLevels'
            # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(1,60+1,1)
            # if var[v]=="TGCLDLWP"                  : tres.cnLevels = np.linspace( 0.1,10, num=60)
            # if var[v]=="TGCLDIWP"                  : tres.cnLevels = np.linspace( 0.1,10, num=60)
            # if var[v]=='TGCLDLWP'                  : tres.cnLevels = np.logspace(-2,1,num=40).round(decimals=2)
            # if var[v]=='TGCLDIWP'                  : tres.cnLevels = np.logspace(-2,1,num=40).round(decimals=2)
            # if 'precip' in var[v]         : tres.cnLevels = np.arange(1,40+1,2)
            # if var[v]=='VapWaterPath'     : tres.cnLevels = np.arange(4,64+4,4)
            # if var[v]=='LiqWaterPath'     : tres.cnLevels = np.linspace( 0.1,2.1, num=20)
            # if var[v]=='IceWaterPath'     : tres.cnLevels = np.linspace( 0.1,2.1, num=20)
            # if var[v]=='LiqWaterPath'     : tres.cnLevels = np.logspace(-3,0,num=30)#.round(decimals=2)
            # if var[v]=='IceWaterPath'     : tres.cnLevels = np.logspace(-3,0,num=30)#.round(decimals=2)
            # if var[v]=='wind_speed_10m'     : tres.cnLevels = np.arange(0.5,5+0.5,0.5)
            if var[v]=='qc'                : tres.cnLevels = np.arange(0.02,1+0.02,0.02)
            if var[v]=='qi'                : tres.cnLevels = np.arange(0.02,1+0.02,0.02)
            # if var[v]=='qci'               : tres.cnLevels = np.arange(0.001,2.001+0.01,0.01)
            # if var[v]=='qcri'              : tres.cnLevels = np.arange(0.001,2.001+0.01,0.01)
            if var[v]=='qci'               : tres.cnLevels = np.logspace(-3,1,num=30)
            if var[v]=='qcri'              : tres.cnLevels = np.logspace(-3,1,num=30)
            if var[v]=='omega'             : tres.cnLevels = np.linspace(-5,5,num=21)
            if var[v]=='horiz_winds_u'     : tres.cnLevels = np.linspace(-5,5,num=21)
            #-------------------------------------------------------------------
            ip = c*num_var + v
            # plot[ip] = ngl.contour(wks,data.transpose('lev','ncol').values,tres)
            plot[ip] = ngl.contour(wks,data.values,tres)
            hs.set_subtitles(wks, plot[ip], 
                             left_string=case_name[c], 
                             center_string='', 
                             right_string=var[v], 
                             font_height=0.008)
      #-------------------------------------------------------------------------
      pres = ngl.Resources()
      pres.nglPanelYWhiteSpacePercent = 5
      pres.nglPanelXWhiteSpacePercent = 5
      # layout = [num_case,num_var]
      if num_case==1 or num_var==1:
         layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      else:
         # layout = [num_var,num_case]
         layout = [num_case,num_var]
      ngl.panel(wks,plot,layout,pres)
      ngl.destroy(wks)
      hc.trim_png(fig_file,verbose=False)

      # exit()

if create_png : ngl.end()
#-------------------------------------------------------------------------------
# Create animated gif
#-------------------------------------------------------------------------------
if create_gif : 
   cmd = f'convert -repage 0x0 -delay 10 -loop 0  {fig_file_list} {gif_file}.gif'
   print(f'\n{cmd}\n')
   os.system(cmd)
   print(f'\n  {gif_file}.gif\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
