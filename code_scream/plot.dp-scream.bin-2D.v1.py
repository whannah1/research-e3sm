import os, glob, ngl, subprocess as sp, copy, string, warnings, cmocean
import numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#-------------------------------------------------------------------------------
case,case_name,case_dir,case_sub = [],[],[],[]
clr,dsh=[],[]
obs_flag = []
def add_case(case_in,n=None,p=None,s=None,c='black',d=0,obs=False):
  global name,case,case_dir,case_sub
  tmp_name = case_in if n is None else n
  case.append(case_in); case_name.append(tmp_name)
  case_dir.append(p); case_sub.append(s)
  clr.append(c),dsh.append(d)
  obs_flag.append(obs)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
def add_var(var_name,file_type,n=''):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
#-------------------------------------------------------------------------------

scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'

add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',      n='DYNAMO ne44 400km',      c='red',  p=scratch_gpu,s='run')
add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acn_1',n='DYNAMO ne44 400km acn=1',c='green',p=scratch_gpu,s='run')
add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acc_1',n='DYNAMO ne44 400km acc=1',c='blue', p=scratch_gpu,s='run')

# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.1',n='DYNAMO ne44 400km L128v2.1',c='magenta', p=scratch_gpu,s='run')
# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.2',n='DYNAMO ne44 400km L128v2.2',c='cyan',    p=scratch_gpu,s='run')
# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L256v1.1',n='DYNAMO ne44 400km L256v1.1',c='blue', p=scratch_gpu,s='run')
# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L276v1.1',n='DYNAMO ne44 400km L276v1.1',c='blue', p=scratch_gpu,s='run')



'''
red   control
green p3_autoconversion_prefactor = 1
blue  p3_k_accretion = 1
'''

first_file,num_files = 0,5
#-------------------------------------------------------------------------------

tmp_file_type = 'output.scream.2D.1hr.AVERAGE.nhours_x1'

add_var('precip_total_surf_mass_flux', tmp_file_type, n='precip')
# add_var('VapWaterPath',                tmp_file_type, n='VapWP')
# add_var('LiqWaterPath',                tmp_file_type, n='LiqWP')
# add_var('IceWaterPath',                tmp_file_type, n='IceWP')

#-------------------------------------------------------------------------------

fig_file,fig_type = f'figs_scream/dp-scream.bin-2D.v1','png'

tmp_data_path = os.getenv('HOME')+'/Research/E3SM/data_tmp'
tmp_file_prefix = 'scream.bin-2D.DP'

recalculate = True

var_x_case = True

#---------------------------------------------------------------------------------------------------
bin_min_x, bin_max_x, bin_spc_x = 75, 320, 10
bin_min_y, bin_max_y, bin_spc_y = 0.05, 0.9, 0.05
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*num_case)

cres_common = hs.res_contour()


sres_common = hs.res_contour_fill()
sres_common.tmYLLabelFontHeightF         = 0.008
sres_common.tmXBLabelFontHeightF         = 0.008
sres_common.lbLabelFontHeightF           = 0.01
sres_common.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
# sres_common.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
# sres_common.cnFillPalette = "MPL_viridis"
# sres_common.cnFillMode    = 'RasterFill'
# sres_common.tiXAxisString = 'Log10(Obj Size) [km]'
# sres_common.tiYAxisString = 'Count Weighted Histogram'

#---------------------------------------------------------------------------------------------------
def get_tmp_file(var,case):
  return f'{tmp_data_path}/{tmp_file_prefix}.tmp.{var}.{case}.nc'
#---------------------------------------------------------------------------------------------------
def get_coords(ncol,lx):
  nx = int(np.sqrt(ncol)) ; dx = lx/nx ; ne = int(nx/2)
  uxi = np.linspace(0,lx,nx+1)
  uxc = uxi[:-1] + np.diff(uxi)/2
  xc,yc = np.full(ncol,np.nan), np.full(ncol,np.nan)
  for ny in range(ne):
    for nx in range(ne):
      for nyj in range(2):
        for nxi in range(2):
          g = ny*4*ne + nx*4 + nyj*2 + nxi
          i = nx*2+nxi
          j = ny*2+nyj
          xc[g] = uxc[i]
          yc[g] = uxc[j]
  return xc,yc
#---------------------------------------------------------------------------------------------------
def bin_ZbyXY(data_z,data_y,data_x,bin_min_x,bin_max_x,bin_spc_x,bin_min_y,bin_max_y,bin_spc_y,):
  #-----------------------------------------------------------------------------  
  nbin_x = np.round( ( bin_max_x - bin_min_x + bin_spc_x )/bin_spc_x ).astype(int)
  bins_x = np.linspace(bin_min_x,bin_max_x,nbin_x)
  bin_x_coord = xr.DataArray( bins_x )
  #-----------------------------------------------------------------------------
  nbin_y = np.round( ( bin_max_y - bin_min_y + bin_spc_y )/bin_spc_y ).astype(int)
  bins_y = np.linspace(bin_min_y,bin_max_y,nbin_y)
  bin_y_coord = xr.DataArray( bins_y )
  #-----------------------------------------------------------------------------
  shape = (nbin_y,nbin_x)
  dims  = ('bin_y','bin_x')
  # coords = {'bin_x':bin_x_coord,'bin_y':bin_y_coord}
  coords = [bin_y_coord,bin_x_coord]
  bin_val = xr.DataArray( np.full( shape,np.nan,dtype=data_z.dtype), dims=dims, coords=coords )
  bin_cnt = xr.DataArray( np.zeros(shape,       dtype=data_z.dtype), dims=dims, coords=coords )
  #-----------------------------------------------------------------------------
  # Loop through bins
  for bx in range(nbin_x):
    for by in range(nbin_y):
      bin_bot_x = bin_min_x - bin_spc_x/2. + bin_spc_x*(bx  )
      bin_top_x = bin_min_x - bin_spc_x/2. + bin_spc_x*(bx+1)
      bin_bot_y = bin_min_y - bin_spc_y/2. + bin_spc_y*(by  )
      bin_top_y = bin_min_y - bin_spc_y/2. + bin_spc_y*(by+1)
      condition_x = ( data_x.values >=bin_bot_x ) & ( data_x.values  <bin_top_x )
      condition_y = ( data_y.values >=bin_bot_y ) & ( data_y.values  <bin_top_y )
      condition_xy = condition_x & condition_y
      if np.sum(condition_xy)>0:
        with warnings.catch_warnings():
          warnings.simplefilter("ignore", category=RuntimeWarning)
          bin_val[by,bx] = data_z.where(condition_xy).mean(skipna=True)
          bin_cnt[by,bx] = np.sum( condition_xy )
  #-----------------------------------------------------------------------------
  bin_ds = xr.Dataset()
  bin_ds['bin_val'] = bin_val
  bin_ds['bin_cnt'] = bin_cnt
  bin_ds['bin_pct'] = bin_cnt/bin_cnt.sum()*1e2
  #-----------------------------------------------------------------------------
  return bin_ds
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
  print(' '*2+'var: '+hc.tcolor.GREEN+var[v]+hc.tcolor.ENDC)
  #-----------------------------------------------------------------------------
  cnt_list = []
  data_list = []
  binx_list = []
  biny_list = []
  #-----------------------------------------------------------------------------
  for c in range(num_case):
    print(' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
    
    tmp_file = get_tmp_file(var[v],case[c])

    if recalculate :
      #-------------------------------------------------------------------------
      file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_type_list[v]}*'
      file_list = sorted(glob.glob(file_path))
      if 'first_file' in globals(): file_list = file_list[first_file:]
      if 'num_files'  in globals(): file_list = file_list[:num_files]
      #-------------------------------------------------------------------------
      ds = xr.open_mfdataset( file_list )
      data_z = ds[var[v]].load()
      data_x = ds['LW_flux_up_at_model_top'].load()
      data_y = ds['SW_flux_up_at_model_top'].load() / ds['SW_flux_dn_at_model_top'].load()
      #-------------------------------------------------------------------------
      # unit conversions
      if 'precip' in var[v]: data_z = data_z*86400.*1e3
      #-------------------------------------------------------------------------
      # print()
      # hc.print_stat(data_x,name='data_x',compact=True,indent=' '*6)
      # hc.print_stat(data_y,name='data_y',compact=True,indent=' '*6)
      hc.print_stat(data_z,name='data_z',compact=True,indent=' '*6)
      # print()
      # exit()
      #-------------------------------------------------------------------------
      area = ds['area'].isel(time=0)
      area_coeff = 1/1e6
      #-------------------------------------------------------------------------
      # bin the data
      bin_ds = bin_ZbyXY( data_z, data_y, data_x, \
                          bin_min_x, bin_max_x, bin_spc_x, \
                          bin_min_y, bin_max_y, bin_spc_y, )
      data_list.append(bin_ds['bin_val'].values)
      with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=RuntimeWarning)
        log_cnt = np.log10(bin_ds['bin_pct'].values)
      log_cnt = np.where(np.isinf(log_cnt), np.nan, log_cnt)
      cnt_list.append(log_cnt)
      binx_list.append(bin_ds['bin_x'].values)
      biny_list.append(bin_ds['bin_y'].values)
      #-------------------------------------------------------------------------

    #    # Write to file
    #    print(' '*6+f'Writing data to file: {tmp_file}')
    #    gbl_mean.name = tvar
    #    tmp_ds = xr.Dataset()
    #    # tmp_ds['time'] = time
    #    tmp_ds[tvar]   = gbl_mean
    #    tmp_ds.to_netcdf(path=tmp_file,mode='w')
    # else:
    #    print(' '*6+f'Reading pre-calculated data from file: {hc.tcolor.MAGENTA}{tmp_file}{hc.tcolor.ENDC}')
    #    tmp_ds = xr.open_dataset( tmp_file )
    #    # time_tmp = tmp_ds['time'].values
    #    gbl_mean = tmp_ds[tvar]
  #-----------------------------------------------------------------------------
  cres = copy.deepcopy(cres_common)
  sres = copy.deepcopy(sres_common)

  cres.sfXArray, cres.sfYArray = binx_list[0], biny_list[0]
  sres.sfXArray, sres.sfYArray = binx_list[0], biny_list[0]

  cnt_min  = np.min([np.nanmin(x) for x in cnt_list])
  cnt_max  = np.max([np.nanmax(x) for x in cnt_list])
  data_min = np.min([np.nanmin(x) for x in data_list])
  data_max = np.max([np.nanmax(x) for x in data_list])

  if var[v]=='VapWaterPath': sres.cnLevels = np.arange(36,50+2,2)
  if var[v]=='LiqWaterPath': sres.cnLevels = np.arange(0.1,1+0.1,0.1)*1
  if var[v]=='IceWaterPath': sres.cnLevels = np.arange(0.1,1+0.1,0.1)*1
  # if var[v]=='precip_total_surf_mass_flux': sres.cnLevels = np.arange(10,60+10,10)
  if var[v]=='precip_total_surf_mass_flux': sres.cnLevels = np.logspace( -1, 2, num=20).round(decimals=2)

  nlev = 11
  (cmin,cmax,cint) = ngl.nice_cntr_levels(cnt_min, cnt_max, cint=None, max_steps=nlev, returnLevels=False, aboutZero=False )
  cres.cnLevels = np.linspace(cmin,cmax,num=nlev)
  cres.cnLevelSelectionMode = 'ExplicitLevels'
  # res.cnLevels = np.arange(1.4,3.4+0.4,0.4)

  sres.cnLevelSelectionMode = 'ExplicitLevels'
  if not hasattr(sres,'cnLevels') : 
    nlev = 11
    (cmin,cmax,cint) = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, returnLevels=False, aboutZero=False )
    sres.cnLevels = np.linspace(cmin,cmax,num=nlev)
    
    
  #-----------------------------------------------------------------------------
  for c in range(num_case):
    ip = v*num_case+c if var_x_case else c*num_var+v

    cplot = ngl.contour(wks, np.ma.masked_invalid(cnt_list[c]), cres)
    splot = ngl.contour(wks, np.ma.masked_invalid(data_list[c]), sres)

    plot[ip] = splot
    ngl.overlay( plot[ip], cplot )
    hs.set_subtitles(wks, plot[ip], case_name[c], '', var_str[v], font_height=0.008)
#-------------------------------------------------------------------------------
layout = [num_var,num_case] if var_x_case else [num_case,num_var]
ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#-------------------------------------------------------------------------------
