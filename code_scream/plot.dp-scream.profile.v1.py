import os, glob, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
clr,dsh=[],[]
obs_flag = []
def add_case(case_in,n=None,p=None,s=None,c='black',d=0,obs=False):
  global name,case,case_dir,case_sub
  tmp_name = case_in if n is None else n
  case.append(case_in); name.append(tmp_name)
  case_dir.append(p); case_sub.append(s)
  clr.append(c),dsh.append(d)
  obs_flag.append(obs)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
def add_var(var_name,file_type,n=None):
   var.append(var_name)
   file_type_list.append(file_type)
   if n is None:
      var_str.append(var_name)
   else:
      var_str.append(n)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
host = None
if os.path.exists('/global/cfs/cdirs'): host = 'nersc'
if os.path.exists('/lustre/orion'):     host = 'olcf'
#-------------------------------------------------------------------------------
if host=='nersc':
   scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
   scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'


   add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',      n='DYNAMO 400km',      c='red',  p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acn_1',n='DYNAMO ne44 400km acn=1',c='green',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acc_1',n='DYNAMO ne44 400km acc=1',c='blue', p=scratch_gpu,s='run')

   add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L276v1.1',n='DYNAMO 400km L276',c='cyan', p=scratch_gpu,s='run')
   add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L256v1.1',n='DYNAMO 400km L256',c='blue', p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.2',n='DYNAMO ne44 400km L128v2.2',c='cyan',    p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.1',n='DYNAMO ne44 400km L128v2.1',c='magenta', p=scratch_gpu,s='run')

   first_file,num_files = 0,5

#-------------------------------------------------------------------------------
if host=='olcf':
   scratch_frontier = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',       n='DYNAMO ne44 400km',p=scratch_frontier,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01-P3FRAC.ne44.len_400km.DT_60',n='DYNAMO ne44 400km',p=scratch_frontier,s='run')

   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60',      n='RCE ne11 100km',      c='red',   p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60',      n='RCE ne22 200km',      c='green', p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60',      n='RCE ne44 400km',      c='blue',  p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60.GFLUX',n='RCE ne11 100km GFLUX',c='blue',p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60.GFLUX',n='RCE ne22 200km GFLUX',c='green', p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60.GFLUX',n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
   add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-01.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')

   first_file,num_files = 3,2

#-------------------------------------------------------------------------------

tmp_file_type = 'output.scream.3D.3hr.AVERAGE.nhours_x3'

add_var('T_mid',              tmp_file_type, n=None)
# add_var('qv',                 tmp_file_type, n=None)
# add_var('qc',                 tmp_file_type, n=None)
# add_var('qi',                 tmp_file_type, n=None)
# add_var('omega',              tmp_file_type, n=None)
# add_var('horiz_winds',        tmp_file_type, n=None)
# add_var('z_mid',              tmp_file_type, n=None)
# add_var('rad_heating_pdel',   tmp_file_type, n=None)
#-------------------------------------------------------------------------------

num_plot_col = len(var)
# num_plot_col = 2


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/dp-scream.profile.v1'

plot_diff = False

overlay_vars = False

# use_snapshot,ss_t  = False,1
use_height_coord   = True

# omit_bot,bot_k     = False,-2
# omit_top,top_k     = False,30

# print_stats        = True
# print_profile      = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'diff_mode' not in locals(): diff_mode = 0

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
# plot = [None]*(num_var*num_case)
if overlay_vars: 
   plot = [None]*(num_case)
else:
   plot = [None]*(num_var)
res = hs.res_xy()
# res.vpWidthF = 0.4
res.xyMarkLineMode = "Lines"
# res.xyMarkLineMode = "MarkLines"
# res.xyMarkerSizeF = 0.008
# res.xyMarker = 16
res.xyLineThicknessF = 16
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2


if use_height_coord:
   res.tiYAxisString = 'Height [km]'
   res.trYMaxF = 20
   res.trYMinF = 0
else:
   res.tiYAxisString = 'Pressure [hPa]'
   res.trYReverse = True
   # res.xyYStyle = 'Log'
   # res.trYMaxF = 100
   res.trYMinF = 100


# if use_snapshot:
#    print(hc.tcolor.RED+'WARNING - plotting snapshot!!!'+hc.tcolor.ENDC)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(' '*2+f'var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
   #----------------------------------------------------------------------------
   data_list,lev_list = [],[]
   #----------------------------------------------------------------------------
   for c in range(num_case):
      print(' '*4+f'case: {hc.tclr.CYAN}{case[c]}{hc.tclr.END}')
      #-------------------------------------------------------------------------
      file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_type_list[v]}*'
      file_list = sorted(glob.glob(file_path))
      if 'first_file' in globals(): file_list = file_list[first_file:]
      if 'num_files'  in globals(): file_list = file_list[:num_files]
      #-------------------------------------------------------------------------
      ds = xr.open_mfdataset( file_list )
      area = ds['area']
      data = ds[var[v]]
      data = data.mean(dim='time')
      data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      #-------------------------------------------------------------------------
      if var[v]=='qv': data = data*1e3
      #-------------------------------------------------------------------------
      data_list.append( data.values )
      #-------------------------------------------------------------------------
      if use_height_coord:
         hght = ds['z_mid']/1e3
         hght = hght.mean(dim='time')
         hght = ( (hght*area).sum(dim='ncol') / area.sum(dim='ncol') )
         lev_list.append( hght.values )
      else:
         lev_list.append( data['lev'].values )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      # if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      # tnum_files = num_files

      # if use_height_coord: 
      #    Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c])

      # area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c]).astype(np.double)
      # X    = case_obj.load_data(tvar,     htype=htype,years=years,months=months,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c],lev=lev)


      # hc.print_time_length(X.time,indent=' '*6)
      # if print_stats: hc.print_stat(X,name=f'{var[v]} before averaging',indent=' '*4,compact=True)


      # if len(X.time)>1:
      #   if htype=='h0':
      #      dtime = ( X['time'][-1] - X['time'][0] + (X['time'][1]-X['time'][0]) ).values.astype('timedelta64[M]') + 1
      #      print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[D]'))+')')
      #   else:
      #      dtime = ( X['time'][-1] - X['time'][0] ).values.astype('timedelta64[D]')+1
      #      print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

      # if use_snapshot:
      #    X = X.isel(time=ss_t)
      #    if use_height_coord: Z = Z.isel(time=ss_t)
      # else:
      #    X = X.mean(dim='time')
      #    if use_height_coord: Z = Z.mean(dim='time')

      # if case[c]=='ERA5' or 'ncol' not in X.dims:
      #    X = ( (X*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )
      # else:
      #    if use_height_coord: Z = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') )
      #    X = ( (X*area).sum(dim='ncol') / area.sum(dim='ncol') )

      # top_lev = 60
      # X = X[top_lev:]

      # if use_height_coord: Z = Z[top_lev:]
      # Z = Z[top_lev:]

      # if omit_bot:
      #    X = X[:bot_k]
      #    if use_height_coord: Z = Z[:bot_k]

      # if omit_top:
      #    X = X[top_k:]
      #    if use_height_coord: Z = Z[top_k:]

      # if print_stats: hc.print_stat(X,name=f'{var[v]} after averaging',indent=' '*4,compact=True)

      # if print_profile:
      #    print()
      #    for xx in X.values: print(f'    {xx}')
      #    print()

      # gbl_mean = ( (X*area).sum() / area.sum() ).values 
      # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

      # data_list.append( X.values )
      # if use_height_coord:
      #    lev_list.append( Z.values )
      # else:
      #    lev_list.append( X['lev'].values )

   data_list_list.append(data_list)
   lev_list_list.append(lev_list)

#-------------------------------------------------------------------------------
# Create plot 1 - overlay all vars for each case
#-------------------------------------------------------------------------------
if overlay_vars:
   for c in range(num_case):
      ip = c
      tres = copy.deepcopy(res)
      data_min = np.min( data_list_list[0][c] )
      data_max = np.max( data_list_list[0][c] )
      for v in range(num_var):
         data_min = np.min([ data_min, np.nanmin(data_list_list[v][c]) ])
         data_max = np.max([ data_max, np.nanmax(data_list_list[v][c]) ])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      
      for v in range(num_var):
         tres.xyLineColor   = vclr[v]
         # tres.xyMarkerColor = vclr[v]
         tres.xyDashPattern = vdsh[v]
         tplot = ngl.xy(wks, data_list_list[v][c], lev_list_list[v][c], tres)

         if v==0 :
            plot[ip] = tplot
         else:
            ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''

      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      hs.set_subtitles(wks, plot[ip], case_name[c], ctr_str, '', font_height=0.01)
     
#-------------------------------------------------------------------------------
# Create plot 2 - overlay all cases for each var
#-------------------------------------------------------------------------------
if not overlay_vars:
   for v in range(num_var):
      data_list = data_list_list[v]
      lev_list = lev_list_list[v]
      
      tres = copy.deepcopy(res)

      # ip = v*num_case+c
      ip = c*num_var+v
      
      baseline = data_list[0]
      if diff_mode==1 :
         baseline1 = data_list[0]
         baseline2 = data_list[1]
      if diff_mode==2 :
         baseline1 = data_list[0]
         baseline2 = data_list[2]
      if plot_diff:
         for c in range(num_case): 
            if diff_mode==1 :
               if c==0 or c==2 : baseline = baseline1
               if c==1 or c==3 : baseline = baseline2
            if diff_mode==2 :
               if c==0 or c==1 : baseline = baseline1
               if c==2 or c==3 : baseline = baseline2
            data_list[c] = data_list[c] - baseline

      
      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      ip = v

      #-------------------------------------------------------------------------
      for c in range(num_case):
         tres.xyMarkLineMode = "Lines"
         tres.xyLineColor   = clr[c]
         # tres.xyMarkerColor = clr[c]
         # tres.xyDashPattern = dsh[c]

         tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)

         if diff_mode==0 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c>0) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==1 :
            if (c==2 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=1) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==2 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=2) or not plot_diff:
               ngl.overlay(plot[ip],tplot)
      #-------------------------------------------------------------------------
      # add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))
      #-------------------------------------------------------------------------
      ctr_str = ''
      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      if plot_diff: var_str += ' (diff)'

      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)

      hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str[v], font_height=0.01)

      # #-------------------------------------------------------------------------
      # iop_file = '/global/cfs/cdirs/e3sm/inputdata/atm/cam/scam/iop/DYNAMO_northsounding_iopfile_4scam.nc'
      # ds = xr.open_dataset( iop_file )
      # data = ds['q'].isel(lat=0,lon=0)
      # data = data.isel(time=slice(0,int(5*24*60/20)))
      # data = data.mean(dim='time').values*1e3
      # lev  = ds['lev'].values/1e2

      # # for i in range(len(data)):
      # #    print(f'{lev[i]}   {data.values[i]}')

      # # hc.print_stat(data_list[0])
      # # hc.print_stat(data)

      # tres.xyLineColor   = 'black'
      # tres.xyMarkerColor = 'black'
      # tres.xyDashPattern = 0

      # tplot = ngl.xy(wks, data, lev, tres)
      # ngl.overlay(plot[ip],tplot)
#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.08
   lgres.lgLabelFontHeightF = 0.012
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 8
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   lx,ly = 0.5,0.45
   if num_var==2: lx,ly = 0.3,0.45
   if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

# #-- draw a common title string on top of the panel
# textres               =  ngl.Resources()
# # textres.txFontHeightF =  0.01                  #-- title string size
# # ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
# textres.txFontHeightF =  0.02                  #-- title string size
# if layout[0]==1: y_pos = 0.7
# if layout[0]>=2: y_pos = 0.9
# # ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
