import os, glob, ngl, copy, xarray as xr, numpy as np, numba
import hapy_common as hc, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
lx_list,ne_list = [],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,lx=None,ne=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = '' if n is None else n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   lx_list.append(lx); ne_list.append(ne)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
method_list = []
def add_var(var_name,file_type,n=None,method='mean'):
   var.append(var_name)
   file_type_list.append(file_type)
   if n is None:
      var_str.append(var_name)
   else:
      var_str.append(n)
   method_list.append(method)
#-------------------------------------------------------------------------------
P0    = 101325       # Pa        surface pressure
Rd    = 287.058      # J/kg/K    gas constant for dry air
cp    = 1004.64      # J/kg/K    specific heat for dry air
g     = 9.80665      # m/s       global average of gravity at MSLP
#-------------------------------------------------------------------------------
host = None
if os.path.exists('/global/cfs/cdirs'): host = 'nersc'
if os.path.exists('/lustre/orion'):     host = 'olcf'
#-------------------------------------------------------------------------------
if host=='nersc':
   scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
   scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'


   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',   n='DYNAMO 400km',        p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acn_1',         ne=44,lx=400,c='blue',  n='DYNAMO ne44 400km acn=1',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acc_1',         ne=44,lx=400,c='blue',  n='DYNAMO ne44 400km acc=1', p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L276v1.1',ne=44,lx=400,c='green', n='DYNAMO 400km L276', p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L256v1.1',ne=44,lx=400,c='blue',  n='DYNAMO 400km L256', p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='green', n='DYNAMO 400km L128v2.2',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='blue',  n='DYNAMO 400km L128v2.1',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-00.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',    n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-00.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-00.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-00.ne44.len_400km.DT_60.vgrid_L256v1.1',ne=44,lx=400,c='cyan',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-00.ne44.len_400km.DT_60.vgrid_L276v1.1',ne=44,lx=400,c='magenta',n='GATE-IDEAL 400km',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',    n='RCE 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='RCE 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='RCE 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L256v1.1',ne=44,lx=400,c='cyan',   n='RCE 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L276v1.1',ne=44,lx=400,c='magenta',n='RCE 400km',p=scratch_gpu,s='run')

   # first_file,num_files = 0,10
   # first_file,num_files = 5,5
   # first_file,num_files = 15,15
   # first_file,num_files = 25,5

   # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60',               ne=44,lx=400,c='black',    n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_10',               ne=44,lx=400,c='royalblue',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')

   
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60',         ne=44,lx=400,c='black',       n='GATE-IDEAL 400km',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.nutop_1e3',ne=44,lx=400,d=1,c='black',       n='GATE-IDEAL 400km nutop_1e3',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.nutop_2e4',ne=44,lx=400,d=1,c='black',       n='GATE-IDEAL 400km nutop_2e4',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFR',      ne=44,lx=400,d=0,c='olivedrab', n='GATE-IDEAL 400km FCFR',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFI',      ne=44,lx=400,d=1,c='steelblue', n='GATE-IDEAL 400km FCFI',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFR_FCFI', ne=44,lx=400,d=2,c='orchid',    n='GATE-IDEAL 400km FCFR+FCFI',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_40',  ne=44,lx=400,c='orangered',   n='GATE-IDEAL 400km acc_40',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_20',  ne=44,lx=400,c='plum',        n='GATE-IDEAL 400km acc_20',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_10',  ne=44,lx=400,c='salmon',      n='GATE-IDEAL 400km acc_10',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_1',   ne=44,lx=400,c='rosybrown',   n='GATE-IDEAL 400km acc_1',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_40.FCFR',   ne=44,lx=400,d=1,c='orangered', n='GATE-IDEAL 400km acc_40+FCFR',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_20.FCFR',   ne=44,lx=400,d=1,c='plum',      n='GATE-IDEAL 400km acc_20+FCFR',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_10.FCFR',   ne=44,lx=400,d=1,c='salmon',    n='GATE-IDEAL 400km acc_10+FCFR',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_1.FCFR',    ne=44,lx=400,d=1,c='rosybrown', n='GATE-IDEAL 400km acc_1+FCFR',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne66.len_600km.DT_60',         ne=66, lx=600, c='cyan',      n='GATE-IDEAL 600km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60',         ne=88, lx=800, c='royalblue', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne111.len_1000km.DT_60',       ne=111,lx=1000,c='purple',    n='GATE-IDEAL 1000km',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60',               ne=88,lx=800, c='black',   n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.FCFL',          ne=88,lx=800, c='cyan',    n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.FCFI',          ne=88,lx=800, c='green',   n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.FCFR',          ne=88,lx=800, c='magenta', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.FCFL_FCFR_FCFI',ne=88,lx=800, c='purple',  n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.FCFR',          ne=88,lx=800, d=0,c='black',     n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.eri_0.25.FCFR', ne=88,lx=800, d=1,c='royalblue', n='GATE-IDEAL 800km eri_0.25',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne88.len_800km.DT_60.eci_0.1.FCFR',  ne=88,lx=800, d=1,c='chocolate', n='GATE-IDEAL 800km eci_0.1',p=scratch_gpu,s='run')
      
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.FCFR', ne=88,lx=800, c='black', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.acp_0.0005555555.acr_0.0001.acq_1.0.acn_0.0.acc_0.0.rsc_0.0.FCFR', ne=88,lx=800, c='blue', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acr_0.0001.acq_1.0.rsc_0.0.FCFR', ne=88,lx=800, c='orangered', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acn_0.0.FCFR',                    ne=88,lx=800, c='plum',      n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acq_1.0.FCFR',                    ne=88,lx=800, c='salmon',    n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acr_0.0001.FCFR',                 ne=88,lx=800, c='rosybrown', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acp_0.0005555555.FCFR',           ne=88,lx=800, c='cyan',      n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.rsc_0.0.FCFR',                    ne=88,lx=800, c='green',     n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acc_0.0.FCFR',                    ne=88,lx=800, c='magenta',   n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.acr_0.0001.acq_1.0.FCFR',         ne=88,lx=800, c='purple',    n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.rsc_0.0.FCFR',             ne=88,lx=800, c='pink',      n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.acc_0.0.FCFR',             ne=88,lx=800, c='red',       n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.acr_0.0001.acq_1.0.FCFR',  ne=88,lx=800, c='royalblue', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-03.ne88.len_800km.DT_60.no_ice.FCFR',                     ne=88,lx=800, c='chocolate', n='GATE-IDEAL 800km',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_1.0.eci_0.1',                           ne=88,lx=800, c='black', n='ctrl + rsc_1.0 + eci_0.1',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_2.0.eci_0.2',                           ne=88,lx=800, c='black', n='ctrl + rsc_2.0 + eci_0.2',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_3.0.eci_0.3',                           ne=88,lx=800, c='black', n='ctrl + rsc_3.0 + eci_0.3',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-04.rsc_4.0.eci_0.4',                           ne=88,lx=800, c='black', n='ctrl + rsc_4.0 + eci_0.4',p=scratch_gpu,s='run')

   # add_case('DPSCREAM.2024-GATE-IDEAL-00',                ne=88,lx=800, c='black', d=0, n='',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFL',           ne=88,lx=800, c='red',   d=0, n='',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFI',           ne=88,lx=800, c='green', d=0, n='',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFR',           ne=88,lx=800, c='blue',  d=0, n='',p=scratch_gpu,s='run')
   add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFL_FCFR_FCFI', ne=88,lx=800, c='purple',d=0, n='',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-01a.FCFL_FCFR_FCFI',ne=88,lx=800, c='pink',  d=1, n='',p=scratch_gpu,s='run')
   add_case('DPSCREAM.2024-GATE-IDEAL-02.FCFL_FCFR_FCFI', ne=88,lx=800, c='pink',  d=0, n='',p=scratch_gpu,s='run') # alt ice melt strategy
   

   # first_file,num_files = 15,5
   # first_file,num_files = 5,5
   first_file,num_files = 0,1
   # first_file,num_files = 10,20
   # first_file,num_files = 20,40

   # tscratch = '/pscratch/sd/b/beydoun/dp_screamxx'
   # add_case('scream_dpxx_GATEIDEAL_control_no_rain_frac_larger_domain',       lx=800,c='orange',n='GATE-IDEAL 800km',              p=tscratch,s='run')
   # add_case('scream_dpxx_GATEIDEAL_minimal_recipe_no_rain_frac_larger_domain',lx=800,c='green', n='GATE-IDEAL 800km minimal micro',p=tscratch,s='run')
   # first_file,num_files = 0,1

#-------------------------------------------------------------------------------
if host=='olcf':
   scratch_frontier = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'

   add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',       ne=44,lx=400,c='red', n='DYNAMO ne44 400km',p=scratch_frontier,s='run')
   add_case('DPSCREAM.DYNAMO-TEST-01-P3FRAC.ne44.len_400km.DT_60',ne=44,lx=400,c='blue',n='DYNAMO ne44 400km',p=scratch_frontier,s='run')


   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60',      lx=100,n='RCE ne11 100km',      c='red',   p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60',      lx=200,n='RCE ne22 200km',      c='green', p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60',      lx=400,n='RCE ne44 400km',      c='blue',  p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60.GFLUX',lx=100,n='RCE ne11 100km GFLUX',c='blue',p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60.GFLUX',lx=200,n='RCE ne22 200km GFLUX',c='green', p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60.GFLUX',lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-01.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-02.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')

   # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-04.ne22.len_200km.DT_60.NN_2.GFLUX',ne=22,lx=200,n='DYNAMO ne22 200km',c='blue',p=scratch_frontier,s='run')

   # first_file,num_files = 15,15
   # first_file,num_files = 25,5
   # first_file,num_files = 1*4,1*4


#-------------------------------------------------------------------------------
# tmp_file_type = 'output.scream.3D.3hr.AVERAGE'
# tmp_file_type = 'output.scream.3D.1hr.AVERAGE'
tmp_file_type = 'output.scream.1D.1hr.AVERAGE'
# tmp_file_type = 'output.scream.3D.30min.AVERAGE'
# tmp_file_type = 'output.scream.3D.5min.AVERAGE'

# tmp_file_type = '.scream.15min.inst.INSTANT.nmins_x15' # Hassan's runs w/ minimal micro
# tmp_file_type = 'scream.hourly.avg.AVERAGE.nhours_x1' # Hassan's runs w/ minimal micro

# add_var('dz_mid',             tmp_file_type, n='dz')
# add_var('S',                  tmp_file_type, n=None)
# add_var('dS/dp',              tmp_file_type, n='-dS/dp')
# add_var('T_mid',              tmp_file_type, n='T_mid variance',method='variance')
# add_var('qv',                 tmp_file_type, n='qv variance',method='variance')
# add_var('qc',                 tmp_file_type, n='qc variance',method='variance')
# add_var('qi',                 tmp_file_type, n='qi variance',method='variance')
# add_var('T_mid',              tmp_file_type, n=None)
# add_var('z_mid',              tmp_file_type, n=None)
# add_var('omega',              tmp_file_type, n=None)
# add_var('qv',                 tmp_file_type, n=None)
# add_var('RelativeHumidity',   tmp_file_type, n=None)

# add_var('qcri',                tmp_file_type, n=None)
# add_var('ncri',                tmp_file_type, n=None)
add_var('qc',                 tmp_file_type, n=None)
add_var('nc',                 tmp_file_type, n=None)
add_var('qr',                 tmp_file_type, n=None)
add_var('nr',                 tmp_file_type, n=None)
add_var('qi',                 tmp_file_type, n=None)
add_var('ni',                 tmp_file_type, n=None)
# add_var('qm',                 tmp_file_type, n=None)
# add_var('bm',                 tmp_file_type, n=None)
# add_var('P3_qr2qv_evap',      tmp_file_type, n=None)
# add_var('horiz_winds_u',      tmp_file_type, n=None)
# add_var('rad_heating_pdel',   tmp_file_type, n=None)

# add_var('wind_speed_10m',               tmp_file_type, n='wind_speed_10m')

# tmp_file_type = 'output.scream.3D.3hr.AVERAGE.nhours_x3'
# add_var('T_mid',               tmp_file_type, n='T_mid',lev=None)

num_plot_col = 2#len(var)

#-------------------------------------------------------------------------------

fig_file,fig_type = 'figs_scream/dp-scream.profile.v2','png'

use_height_coord  = False

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# if 'input_file_name' not in locals():
#    files = glob.glob(os.getenv('HOME')+f'/SCREAM/scratch/{case_name}/run/{case_name}.eam.h1.*.nc')
#    input_file_name = sorted(files)[-2]
# print()
# print(f'  input_file_name: {input_file_name}')

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)

res = hs.res_xy()
res.xyMarkLineMode = "Lines"
res.xyLineThicknessF = 8
if use_height_coord:
   res.tiYAxisString = 'Height [km]'
   # res.trYMaxF = 20
   # res.trYMinF = 0
else:
   res.tiYAxisString = 'Pressure [hPa]'
   res.trYReverse = True
   # res.xyYStyle = 'Log'
   # res.trYMaxF = 100
   # res.trYMinF = 100

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

#---------------------------------------------------------------------------------------------------
def get_file_list(case,case_dir,case_sub,file_type):
   global first_file,num_files
   file_path = f'{case_dir}/{case}/{case_sub}/*{file_type}*'
   file_list = sorted(glob.glob(file_path))
   # print(f'  {hc.tclr.RED}WARNING - dropping last file{hc.tclr.END}')
   # file_list.pop() # drop last item
   if 'first_file' in globals(): file_list = file_list[first_file:]
   if 'num_files' in globals(): file_list = file_list[:num_files]
   return file_list
#---------------------------------------------------------------------------------------------------
def get_data(ds,var):
   tvar = var
   #----------------------------------------------------------------------------
   if var=='horiz_winds_u': tvar = 'horiz_winds'
   if var=='horiz_winds_v': tvar = 'horiz_winds'
   if var=='qcri'         : tvar = 'qc'
   if var=='ncri'         : tvar = 'nc'
   if var=='qci'          : tvar = 'qc'
   if var=='nci'          : tvar = 'nc'
   if var=='S'            : tvar = 'T_mid'
   if var=='dS/dp'        : tvar = 'T_mid'
   if var=='dz_mid'       : tvar = 'z_mid'
   #----------------------------------------------------------------------------
   data = ds[tvar]#.load()
   #----------------------------------------------------------------------------
   if var=='horiz_winds_u': data = data.isel(dim2=0)
   if var=='horiz_winds_v': data = data.isel(dim2=1)
   if var=='qcri'         : data = data + ds['qr'] + ds['qi']
   if var=='ncri'         : data = data + ds['nr'] + ds['ni']
   if var=='qci'          : data = data + ds['qi']
   if var=='nci'          : data = data + ds['ni']
   if var=='S'            : data = data + ds['z_mid']*g/cp
   # if var=='dS/dp'        : data = np.gradient( data + ds['z_mid']*g/cp  ,axis=2)
   if var=='dS/dp'        : data = data + ds['z_mid']*g/cp; data = data.differentiate(coord='lev')*-1
   if var=='dz_mid'       : data = data.diff(dim='lev')*-1
   #----------------------------------------------------------------------------
   # unit conversions    
   if 'precip' in var: data = data*86400.*1e3
   if var in ['qv','qc','qr','qi','qcri','qci']: data = data*1e3
   #----------------------------------------------------------------------------
   return data
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
@numba.njit()
def get_coords(ncol,lx=None):
   nx = int(np.sqrt(ncol))
   ne = int(nx/2)
   uxi = np.linspace(0,lx,nx+1)
   uxc = uxi[:-1] + np.diff(uxi)/2
   xc = np.full(ncol,np.nan)
   yc = np.full(ncol,np.nan)
   for ny in range(ne):
      for nx in range(ne):
         for nyj in range(2):
            for nxi in range(2):
               g = ny*4*ne + nx*4 + nyj*2 + nxi
               i = nx*2+nxi
               j = ny*2+nyj
               xc[g] = uxc[i]
               yc[g] = uxc[j]
   return xc,yc
#---------------------------------------------------------------------------------------------------
@numba.njit()
def reorg_data(data,xc,yc,ncol,nlev):
   nx = int(np.sqrt(ncol))
   ne = int(nx/2)
   xx = np.zeros((nx,nx))
   yy = np.zeros((nx,nx))
   data_out = np.zeros((nx,nx,nlev))
   for ny in range(ne):
      for nx in range(ne):
         for nyj in range(2):
            for nxi in range(2):
               g = ny*4*ne + nx*4 + nyj*2 + nxi
               i = nx*2+nxi
               j = ny*2+nyj
               xx[i,j] = xc[g]
               yy[i,j] = yc[g]
               data_out[i,j,:] = data[g,:]
   return data_out,xx,yy
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print(' '*2+f'var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
   data_list = []
   hght_list = []
   for c in range(num_case):
      print(' '*4+f'case: {hc.tclr.CYAN}{case[c]}{hc.tclr.END}')
      #-------------------------------------------------------------------------
      file_list = get_file_list(case[c],case_dir[c],case_sub[c],file_type_list[v])
      #-------------------------------------------------------------------------
      ds = xr.open_mfdataset( file_list )
      #-------------------------------------------------------------------------
      # # discard first few hours
      # print(' '*4+f'{hc.tclr.RED}WARNING - skipping first 12 hours{hc.tclr.END}')
      # # ds = ds.isel(time=slice(48,))
      # ds = ds.isel(time=slice(12,))
      #-------------------------------------------------------------------------
      # area = get_data(ds,'area').isel(time=0,missing_dims='ignore')
      if 'area' in ds.variables:
        area = ds['area'].isel(time=0, missing_dims='ignore')
      elif 'area_PG2' in ds.variables:
        area = ds['area_PG2'].isel(time=0, missing_dims='ignore')
      else:
        raise ValueError('area variable not found in dataset?')
      #-------------------------------------------------------------------------
      data = get_data(ds,var[v])#.mean(dim='time')

      hc.print_stat(data,compact=True)

      data = data.isel(lev=slice(4,-1)) # skip upper levels due to lack of damping there

      #-------------------------------------------------------------------------
      # if var[v]=='z_mid':
      #    ds = ds.isel(time=0)
      #    mlev = ds['hyam'].values*1000 + ds['hybm'].values*1000
      #    ilev = ds['hyai'].values*1000 + ds['hybi'].values*1000
      #    ilevz = np.log(ilev/1e3) * -6740.
      #    mlevz = np.log(mlev/1e3) * -6740.
      #    dlevz = xr.zeros_like(data)
      #    for k in range(len(mlev)):
      #       dlevz[:,k] = ilevz[k] - ilevz[k+1]
      #    data = dlevz
      #-------------------------------------------------------------------------
      # xc,yc = get_coords( len(data), lx=lx_list[c] )
      # xc = xr.DataArray(xc,dims=['ncol'])
      # yc = xr.DataArray(yc,dims=['ncol'])
      #-------------------------------------------------------------------------
      # # focus on center of domain
      # center = lx_list[c]/2
      # distance = np.sqrt( np.square(xc-center) + np.square(yc-center) )
      # condition = xr.full_like( data, False )
      # condition =  distance < 20
      # area = area.where(condition,drop=True)
      # data = data.where(condition,drop=True)
      #-------------------------------------------------------------------------
      # lev_tmp = data['lev']
      # data,xc,yc = reorg_data(data.values,xc.values,yc.values,len(data['ncol']),len(data['lev']))
      # data = xr.DataArray(data,coords={'x':xc[:,0],'y':yc[0,:],'lev':lev_tmp})
      #-------------------------------------------------------------------------
      # ny = len(data['y'])
      # data = data.isel(y=slice(int(ny/2-2),int(ny/2+2)+1)).mean(dim='y').transpose('lev','x')
      #-------------------------------------------------------------------------
      if method_list[v]=='mean':
         # data = data.mean(dim='time')
         data = data.isel(time=-1)
         data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
      if method_list[v]=='variance':
         data = data.var(dim='ncol').mean(dim='time')
      #-------------------------------------------------------------------------
      if use_height_coord:
         hght = get_data(ds,'z_mid')#.mean(dim=('time','ncol'))
         if var[v]=='dz_mid': hght = hght[1:]
         data['lev'] = hght/1e3
         dz = 100e-3 ; data = data.interp(lev=np.arange(dz,18+dz,dz))
      #-------------------------------------------------------------------------
      data_list.append(data.values)
      hght_list.append(data['lev'].values)
   #----------------------------------------------------------------------------
   ip = v
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   tres = copy.deepcopy(res)
   tres.trXMinF = data_min
   tres.trXMaxF = data_max

   if var[v]=='dS/dp': tres.trXMaxF = 0.1
   for c in range(num_case):
      tres.xyLineColor   = clr[c]
      tres.xyDashPattern = dsh[c]
      #-------------------------------------------------------------------------
      tplot = ngl.xy(wks, data_list[c], hght_list[c], tres)
      if c==0: plot[ip] = tplot
      if c>=1: ngl.overlay(plot[ip],tplot)
      #-------------------------------------------------------------------------
      # add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))
      #-------------------------------------------------------------------------
      hs.set_subtitles(wks, plot[ip], 
                       # left_string=case_name[c], 
                       left_string='', 
                       center_string='', 
                       right_string=var_str[v], 
                       font_height=0.012)
      
      
#---------------------------------------------------------------------------------------------------
# Add legend
if num_case>1:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.08
   lgres.lgLabelFontHeightF = 0.012
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 8
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   lx,ly = 0.5,0.45
   if num_var==2: lx,ly = 0.3,0.45
   if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pres = hs.setres_panel()
# pres.nglPanelTop =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
