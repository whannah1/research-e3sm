import os, glob, copy, ngl, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#-------------------------------------------------------------------------------
# case,case_name,case_dir,case_sub = [],[],[],[]
# clr,dsh=[],[]
# obs_flag = []
# def add_case(case_in,n=None,p=None,s=None,c='black',d=0,obs=False,lx=None):
#   global name,case,case_dir,case_sub
#   tmp_name = case_in if n is None else n
#   case.append(case_in); case_name.append(tmp_name)
#   case_dir.append(p); case_sub.append(s)
#   clr.append(c),dsh.append(d)
#   obs_flag.append(obs)

case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
lx_list,ne_list = [],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,lx=None,ne=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = '' if n is None else n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   lx_list.append(lx); ne_list.append(ne)
#-------------------------------------------------------------------------------
var, var_str, file_type_list, method_list = [], [], [], []
def add_var(var_name,file_type,n='',method='avg'):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
  method_list.append(method)
#-------------------------------------------------------------------------------

scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'

# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',      n='DYNAMO ne44 400km',      c='red',  p=scratch_gpu,s='run')
# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acn_1',n='DYNAMO ne44 400km acn=1',c='green',p=scratch_gpu,s='run')
# add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acc_1',n='DYNAMO ne44 400km acc=1',c='blue', p=scratch_gpu,s='run')

scratch_frontier = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'

add_case('DPSCREAM.2024-GATE-IDEAL-00',                ne=88,lx=800, c='black',  n='control',p=scratch_gpu,s='run')
add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFL',           ne=88,lx=800, c='red',    n='FCFL',p=scratch_gpu,s='run')
add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFI',           ne=88,lx=800, c='green',  n='FCFI',p=scratch_gpu,s='run')
add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFR',           ne=88,lx=800, c='blue',   n='FCFR',p=scratch_gpu,s='run')
add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFL_FCFR_FCFI', ne=88,lx=800, c='purple', n='FCFL_FCFR_FCFI',p=scratch_gpu,s='run')

# first_file,num_files = 0,0
# first_file,num_files = 0,60

# add_case('scream_dpxx_RCE_300K.prefactor.001a',ne=50,lx=500,c='red',n='RCE prefactor.001a',p='/pscratch/sd/b/bogensch/dp_screamxx',s='run')
# add_case('scream_dpxx_RCE_300K.rainfrac.001b', ne=50,lx=500,c='blue',n='RCE rainfrac.001b', p='/pscratch/sd/b/bogensch/dp_screamxx',s='run')

# add_case('scream_dpxx_RCE_300K.fixed.001b',     ne=50,lx=500,c='red',n='RCE prefactor.001a',p='/pscratch/sd/b/bogensch/dp_screamxx',s='run')
# add_case('scream_dpxx_RCE_300K.fixed.expr.001b',ne=50,lx=500,c='blue',n='RCE rainfrac.001b', p='/pscratch/sd/b/bogensch/dp_screamxx',s='run')

# first_file,num_files = 0,1

#-------------------------------------------------------------------------------

tmp_file_type = 'output.scream.2D.1hr.AVERAGE.nhours_x1'
# tmp_file_type = 'scream.daily.horiz_avg.AVERAGE.ndays_x1'

add_var('precip_total_surf_mass_flux', tmp_file_type, n='precip')
add_var('VapWaterPath',                tmp_file_type, n='VapWP')
add_var('LiqWaterPath',                tmp_file_type, n='LiqWP')
add_var('IceWaterPath',                tmp_file_type, n='IceWP')
# add_var('RainWaterPath',               tmp_file_type, n='RainWP')
# add_var('surf_sens_flux',              tmp_file_type, n='sfc sens')
# add_var('surf_evap',                   tmp_file_type, n='sfc evap',method='avg')
# add_var('surf_evap',                   tmp_file_type, n='sfc evap',method='std')

# add_var('pme',                   tmp_file_type, n='P-E')


# add_var('RelativeHumidity_at_700hPa', tmp_file_type, n='RH 700hPa')
# add_var('omega_at_500hPa',            tmp_file_type, n='Omega 500hPa')
# add_var('surf_evap',                  tmp_file_type, n='surf_evap')

# add_var('SW_flux_dn_at_model_bot',tmp_file_type)
# add_var('SW_flux_up_at_model_bot',tmp_file_type)
# add_var('LW_flux_dn_at_model_bot',tmp_file_type)
# add_var('LW_flux_up_at_model_bot',tmp_file_type)
# add_var('SW_flux_up_at_model_top',tmp_file_type,n='SW_flux_up_at_model_top')
# add_var('SW_flux_dn_at_model_top',tmp_file_type,n='SW_flux_dn_at_model_top')
# add_var('LW_flux_up_at_model_top',tmp_file_type,n='LW_flux_up_at_model_top')
#-------------------------------------------------------------------------------

fig_type = "png"
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/dp-scream.timeseries.v1'

write_file    = False
print_stats   = True
overlay_cases = True

convert_to_daily_mean  = False

add_trend = False

num_plot_col  = 1

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

# overlay_cases with single case causes segfault
if num_case==1 : overlay_cases = False

if 'clr' not in vars() or clr==[]: clr = ['black']*num_case
if 'dsh' not in vars() or dsh==[]: dsh = np.zeros(num_case)

if 'lev' not in vars(): lev = np.array([0])

#-------------------------------------------------------------------------------
# plot legend in separate file
#-------------------------------------------------------------------------------
if num_case>1:
   create_legend = True
   legend_file = fig_file+'.legend'
   wkres = ngl.Resources() #; npix = 1024 ; wkres.wkWidth,wkres.wkHeight=npix,npix
   lgd_wks = ngl.open_wks('png',legend_file,wkres)
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.03*num_case
   lgres.lgLabelFontHeightF = 0.008
   lgres.lgLabelFont        = "courier"
   lgres.lgMonoDashIndex    = False
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 16
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   labels = case_name
   for i in range(len(labels)): labels[i] = ' '*4+labels[i] 

   pid = ngl.legend_ndc(lgd_wks, len(labels), labels, 0.5, 0.65, lgres)

   ngl.frame(lgd_wks)
   hc.trim_png(legend_file)
   # exit()

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
if overlay_cases:
   plot = [None]*(num_var)
else:
   plot = [None]*(num_case*num_var)

wkres = ngl.Resources()
npix=4096; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
# if create_legend: lgd_wks = ngl.open_wks('png',legend_file,wkres)

res = hs.res_xy()
# res.vpHeightF = 0.5
res.vpHeightF = 0.2
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.xyLineThicknessF = 10

lres = hs.res_xy()
lres.xyDashPattern    = 0
lres.xyLineThicknessF = 1
lres.xyLineColor      = "black"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
  print(' '*2+'var: '+hc.tcolor.GREEN+var[v]+hc.tcolor.ENDC)
  #-----------------------------------------------------------------------------
  time_list,data_list = [],[]
  data_min_list, data_max_list = [],[]
  #-----------------------------------------------------------------------------
  for c in range(num_case):
    print(' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
    
    # tmp_file = get_tmp_file(var[v],case[c])

    if True :
      #-------------------------------------------------------------------------
      file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*{file_type_list[v]}*'
      file_list = sorted(glob.glob(file_path))
      if file_list==[]: raise ValueError(f'No files found for path: {file_path}')
      if 'first_file' in globals(): file_list = file_list[first_file:]
      if 'num_files'  in globals(): file_list = file_list[:num_files]
      #-------------------------------------------------------------------------
      decode_times = True
      if 'horiz_avg' in file_type_list[v] : decode_times = False
      ds = xr.open_mfdataset( file_list, decode_times=decode_times )
      #-------------------------------------------------------------------------
      if 'area' in locals(): del area
      if 'area' in ds.variables: 
        area = ds['area'].isel(time=0, missing_dims='ignore')
      #-------------------------------------------------------------------------
      if var[v]=='pme':
        Lv      = 2.5104e6
        data = ds['precip_total_surf_mass_flux'] - ds['surf_evap']/1e3 # units = m/s
        data = data*86400*1e3 # units = mm/day
      elif var[v]=='precip_total_surf_mass_flux' and 'precip_total_surf_mass_flux' not in ds.variables:
        data = ds['precip_liq_surf_mass_flux'] + ds['precip_ice_surf_mass_flux']
      else:
        data = ds[var[v]]


      # data = data.isel(time=slice(40,100))
      #-------------------------------------------------------------------------
      # unit conversions
      if 'precip' in var[v] : data = data*86400.*1e3
      if var[v]=='surf_evap': data = data/1e3*86400.*1e3
      #-------------------------------------------------------------------------
      if convert_to_daily_mean: data = data.resample(time='D').mean(dim='time')
      
      #reset time index to start at zero and convert to days
      # dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
      # print('      Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')
      #-------------------------------------------------------------------------
      if 'area' in locals():
        if method_list[v]=='avg':
          data = ( (data*area).sum(dim='ncol',skipna=True) / area.sum(dim='ncol') )
        if method_list[v]=='std':
          data = data.std(dim='ncol',skipna=True)
      else:
        if method_list[v]=='std': raise ValueError('Cannot use std method with pre-averaged data!')
        data = data.isel(ncol=0)
      #-------------------------------------------------------------------------
      # print(data.values)
      #-------------------------------------------------------------------------
      time_mean = data.mean(dim='time',skipna=True).values
      print('      Time Mean : '+hc.tcolor.GREEN+f'{time_mean:10.6f}'+hc.tcolor.ENDC)
      #-------------------------------------------------------------------------
      # Make time start at zero
      def fix_time(time): return ( time - time[0] ).astype('float') / 86400e9
      if 'units' in data['time'].attrs:
        if 'days' not in data['time'].units:
          data['time'] = fix_time(data['time'])
      else:
        data['time'] = fix_time(data['time'])
      #-------------------------------------------------------------------------
      data_list.append( data.values )
      time_list.append( data['time'].values )

      # data_min_list.append( data.min(dim='ncol').values )
      # data_max_list.append( data.max(dim='ncol').values )

  #----------------------------------------------------------------------------
  # Create plot
  #----------------------------------------------------------------------------
  tres = copy.deepcopy(res)
  tres.tiXAxisString = 'Time [days]'

  tres.trYMinF = np.min([np.nanmin(d) for d in data_list])
  tres.trYMaxF = np.max([np.nanmax(d) for d in data_list])
  # tres.trYMinF = np.min([np.nanmin(d) for d in data_min_list])
  # tres.trYMaxF = np.max([np.nanmax(d) for d in data_max_list])
  tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
  tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])



  for c in range(num_case):
    ip = c*num_var + v
    if overlay_cases: ip = v

    tres.xyLineColor   = clr[c]
    # tres.xyDashPattern = dsh[c]
    tres.xyDashPattern = 0
    # tplot = ngl.xy(wks, time_list[c], data_list[c], tres)
    tplot = ngl.xy(wks, time_list[c], np.ma.masked_invalid(data_list[c]), tres)

    if overlay_cases: 
      if c==0: 
        plot[ip] = tplot
      else:
        ngl.overlay(plot[ip],tplot)
    else:
      plot[ip] = tplot


    # tres.xyDashPattern = 1
    # ngl.overlay(plot[ip], ngl.xy(wks, time_list[c], data_min_list[c], tres) )
    # ngl.overlay(plot[ip], ngl.xy(wks, time_list[c], data_max_list[c], tres) )

    #------------------------------------------------
    # add linear trend
    #------------------------------------------------
    if add_trend:
      px = time_list[c]
      py = data_list[c]
      # simple and fast method for regression coeff and intercept
      a = np.cov( px.flatten(), py.flatten() )[1,0] / np.var( px )
      b = np.mean(py) - a*np.mean(px)

      if c==0: print()
      print(' '*4+f'linear regression a: {a}    b: {b}')
      if c==(num_case-1): print()

      px_range = np.abs( np.max(px) - np.min(px) )
      lx = np.array([-1e2*px_range,1e2*px_range])

      lres.xyLineColor = clr[c]
      ngl.overlay( plot[ip], ngl.xy(wks, lx, lx*a+b , lres) )
      #------------------------------------------------
      #------------------------------------------------

    #----------------------------------------------------------------------------
    # Set strings at top of plot
    #----------------------------------------------------------------------------
    # var_str = var[v]
    # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"
    # if var[v]=="TMQ"   : var_str = "Column Water Vapor [mm]"

    # if overlay_cases:
    #   rgt_str,lft_str,ctr_str = var_str[v],'',''
    # else:
    #   rgt_str,lft_str,ctr_str = case_name[c],'',''

    # hs.set_subtitles(wks, plot[ip], case_name[c], ctr_str, var_str, font_height=0.015)
    hs.set_subtitles(wks, plot[ip], '', '', var_str[v], font_height=0.015)

  #----------------------------------------------------------------------------
  # Ad legend
  #----------------------------------------------------------------------------
  lgres = ngl.Resources()
  lgres.vpWidthF, lgres.vpHeightF  = 0.08, 0.08  
  lgres.lgLabelFontHeightF = 0.01
  lgres.lgLineThicknessF   = 4
  lgres.lgMonoLineColor    = False
  # lgres.lgMonoDashIndex    = True
  lgres.lgLineColors       = clr
  lgres.lgDashIndexes      = dsh
  lgres.lgLabelJust    = 'CenterLeft'
  # pid = ngl.legend_ndc(wks, len(name), name, 0.5, 0.4, lgres)  # 3x2
  # pid = ngl.legend_ndc(wks, len(name), name, 0.5, 0.1, lgres)  # 3x2
  # pid = ngl.legend_ndc(wks, len(name), name, 0.3, 0.5, lgres)  # 1x2

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
hc.printline()

pres = hs.setres_panel()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

if num_case==1 or num_var==1:
  layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
  layout = [num_var,1]


ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

hc.trim_png(fig_file)
if 'legend_file' in locals(): hc.trim_png(legend_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
