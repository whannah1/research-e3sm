import os, glob, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
var,lev_list,mask_flag = [],[],[]
def add_var(var_name,lev=-1,mask=None): 
   var.append(var_name); lev_list.append(lev),mask_flag.append(mask)
#-------------------------------------------------------------------------------

scratch_path = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch'

# add_case('DPSCREAM.RCE-TEST-00.ne20.SST_200', n='SST_200', c='blue',p=scratch_path,s='run')
# add_case('DPSCREAM.RCE-TEST-00.ne40.SST_270', n='SST_270', c='green',p=scratch_path,s='run')
add_case('DPSCREAM.RCE-TEST-00.ne40.SST_280', n='SST_280', c='red',p=scratch_path,s='run')
# add_case('DPSCREAM.RCE-TEST-00.ne40.SST_300', n='SST_300', c='red',p=scratch_path,s='run')

# htype,first_file,num_files = 'h1',30,1
htype,first_file,num_files = 'h0',80,1

time_slice = 0 # single time index to load (no averaging)

#-------------------------------------------------------------------------------
add_var('TGCLDLWP')
add_var('TGCLDIWP')
# add_var('PRECL')
add_var('LHFLX')
add_var('FLNT')
# add_var('TMQ')
# add_var('TBOT')

num_plot_col = 4

### output figure type and name
fig_type = 'png'
# fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/dp-scream.xy.v1.'+str(time_slice).zfill(3)
# fig_file = 'figs_scream/dp-scream.xy.v1.'+str(time_slice).zfill(3)
fig_file = 'figs_scream/dp-scream.xy.v1'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# if 'input_file_name' not in locals():
#    files = glob.glob(os.getenv('HOME')+f'/SCREAM/scratch/{case_name}/run/{case_name}.eam.h1.*.nc')
#    input_file_name = sorted(files)[-2]
# print()
# print(f'  input_file_name: {input_file_name}')

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

### create the plot workstation
wkres = ngl.Resources() ; npix = 2048 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_case*num_var)

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.lbOrientation                = "Horizontal"
res.lbLabelFontHeightF           = 0.008

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+var[v])

   if 'lev_list' in locals(): lev = lev_list[v]

   for c in range(num_case):

      print('    case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      xcenter = case_obj.load_data('crm_grid_x',htype=htype).values
      ycenter = case_obj.load_data('crm_grid_y',htype=htype).values

      data = case_obj.load_data(var[v],htype=htype,lev=lev,
                                first_file=first_file,num_files=num_files)

      # print(); print(data); exit()
      hc.print_time_length(data.time)

      # if 'time' in data.dims: data = data.isel(time=time_slice)
      if 'time' in data.dims: data = data.mean(dim='time')      

      #-------------------------------------------------------------------------
      # unit conversions
      #-------------------------------------------------------------------------
      if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3

      #-------------------------------------------------------------------------
      # Set colors and contour levels
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      ### change the color map depending on the variable
      tres.cnFillPalette = "MPL_viridis"
      # if var[v] in ['OMEGA']                    : tres.cnFillPalette = 'BlueWhiteOrangeRed'
      # if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
      # if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = 'CBR_wet'

      # ### specify specific contour intervals
      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,60+1,1)
      # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      # # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2) # better for MMF
      # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2,-0.2, num=60).round(decimals=2) # better for non-MMF

      # ### use this for symmetric contour intervals
      # if var[v] in ['U','V'] :
      #    cmin,cmax,cint,clev = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
      #                                               cint=None, max_steps=21,              \
      #                                               returnLevels=True, aboutZero=True )
      #    tres.cnLevels = np.linspace(cmin,cmax,num=91)

      # if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
      
      #-------------------------------------------------------------------------
      # Set up cell fill attributes using scrip grid file
      #-------------------------------------------------------------------------
      
      tres.cnFillMode    = 'RasterFill'
      tres.sfXArray      = xcenter
      tres.sfYArray      = ycenter

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      # tres.trXMinF,tres.trXMaxF = 0,dx*5
      # tres.trYMinF,tres.trYMaxF = 0,dx*5
      
      ip = c*num_var + v

      plot[ip] = ngl.contour(wks,data.values,tres)

      hs.set_subtitles(wks, plot[ip], 
                       left_string=case_name[c], 
                       center_string='', 
                       right_string=var[v], 
                       font_height=0.01)
      
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      area = case_obj.load_data('area',htype=htype)
      xcenter = case_obj.load_data('crm_grid_x',htype=htype)
      ycenter = case_obj.load_data('crm_grid_y',htype=htype)

      condition = area==area.min()

      area = area.where( condition,drop=True)
      xcenter = xcenter.where( condition,drop=True)
      ycenter = ycenter.where( condition,drop=True)
      
      mres = hs.res_xy()
      mres.xyMarkLineMode = 'Markers'
      mres.xyMarker       = 16
      mres.xyMarkerSizeF  = 0.002
      mres.xyMarkerColor  = 'red'
      ngl.overlay( plot[ip], ngl.xy(wks,xcenter.values,ycenter.values,mres) )

#-------------------------------------------------------------------------------
# Finalize plot
#-------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

if num_case==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [num_case,num_var]

ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

### trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
