import os, ngl, glob, subprocess as sp, copy, string, numpy as np, xarray as xr, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
#-------------------------------------------------------------------------------
# ds = xr.open_dataset('/global/homes/w/whannah/E3SM/data_grid/ne30pg2_scrip.nc')
# lat,lon = ds['grid_center_lat'].values, ds['grid_center_lon'].values
# nbeg,npts = 1000,20
# for n in range(nbeg,nbeg+npts): print(f'  {lon[n]:8.2f} , {lat[n]:8.2f}  ')
# exit()
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
lx_list,ne_list = [],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,lx=None,ne=None):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   tmp_name = '' if n is None else n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   lx_list.append(lx); ne_list.append(ne)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
mlev_list = []
def add_var(var_name,file_type,n='',mlev=None):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
  mlev_list.append(mlev)
#-------------------------------------------------------------------------------
host = None
if os.path.exists('/global/cfs/cdirs'): host = 'nersc'
if os.path.exists('/lustre/orion'):     host = 'olcf'
#-------------------------------------------------------------------------------
if host=='nersc':
  scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
  scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'

  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',    n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L256v1.1',ne=44,lx=400,c='cyan',   n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L276v1.1',ne=44,lx=400,c='magenta',n='RCE 400km',p=scratch_gpu,s='run')

  # first_file,num_files = 0,10
  # first_file,num_files = 5,5
  # first_file,num_files = 15,15
  # first_file,num_files = 25,5

  # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',    n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_10',               ne=44,lx=400,c='blue',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
  #add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
  #add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')

  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60',         ne=44,lx=400,c='black',           n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFR',    ne=44,lx=400,c='green',           n='GATE-IDEAL 400km FCFR',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFI',      ne=44,lx=400,d=1,c='steelblue', n='GATE-IDEAL 400km FCFI',p=scratch_gpu,s='run')
   # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.FCFR_FCFI', ne=44,lx=400,d=2,c='orchid',    n='GATE-IDEAL 400km FCFR+FCFI',p=scratch_gpu,s='run')
  
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_40',  ne=44,lx=400,c='orangered',   n='GATE-IDEAL 400km acc_40',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_20',  ne=44,lx=400,c='plum',        n='GATE-IDEAL 400km acc_20',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_10',  ne=44,lx=400,c='salmon',      n='GATE-IDEAL 400km acc_10',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_1',   ne=44,lx=400,c='rosybrown',   n='GATE-IDEAL 400km acc_1',p=scratch_gpu,s='run')

  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_40.FCFR',   ne=44,lx=400,d=1,c='orangered', n='GATE-IDEAL 400km acc_40',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_20.FCFR',   ne=44,lx=400,d=1,c='plum',      n='GATE-IDEAL 400km acc_20+FCFR',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_10.FCFR',   ne=44,lx=400,d=1,c='salmon',    n='GATE-IDEAL 400km acc_10',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-02.ne44.len_400km.DT_60.acc_1.FCFR',    ne=44,lx=400,d=1,c='rosybrown', n='GATE-IDEAL 400km acc_1',p=scratch_gpu,s='run')

  # add_case('scream_dpxx_RCE_300K.fixed.001b',     ne=50,lx=500,c='red',n='RCE prefactor.001a',p='/pscratch/sd/b/bogensch/dp_screamxx',s='run')
  # add_case('scream_dpxx_RCE_300K.fixed.expr.001b',ne=50,lx=500,c='blue',n='RCE rainfrac.001b', p='/pscratch/sd/b/bogensch/dp_screamxx',s='run')

  # add_case('DPSCREAM.2024-GATE-IDEAL-00',                ne=88,lx=800, c='black',  n='control',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFL',           ne=88,lx=800, c='red',    n='FCFL',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFI',           ne=88,lx=800, c='green',  n='FCFI',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFR',           ne=88,lx=800, c='blue',   n='FCFR',p=scratch_gpu,s='run')
  add_case('DPSCREAM.2024-GATE-IDEAL-00.FCFL_FCFR_FCFI', ne=88,lx=800, c='purple', n='FCFL_FCFR_FCFI',p=scratch_gpu,s='run')
  add_case('DPSCREAM.2024-GATE-IDEAL-02.FCFL_FCFR_FCFI', ne=88,lx=800, c='pink',  d=0, n='',p=scratch_gpu,s='run')

  first_file,num_files = 0,1
  # first_file,num_files = 20,40

#-------------------------------------------------------------------------------
if host=='olcf':
  scratch_frontier = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'

  add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',       ne=44,lx=400,c='red', n='DYNAMO ne44 400km',p=scratch_frontier,s='run')
  add_case('DPSCREAM.DYNAMO-TEST-01-P3FRAC.ne44.len_400km.DT_60',ne=44,lx=400,c='blue',n='DYNAMO ne44 400km',p=scratch_frontier,s='run')

  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60',      lx=100,n='RCE ne11 100km',      c='red',   p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60',      lx=200,n='RCE ne22 200km',      c='green', p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60',      lx=400,n='RCE ne44 400km',      c='blue',  p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60.GFLUX',lx=100,n='RCE ne11 100km GFLUX',c='blue',p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60.GFLUX',lx=200,n='RCE ne22 200km GFLUX',c='green', p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60.GFLUX',lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-01.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-02.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')

  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-04.ne22.len_200km.DT_60.NN_2.GFLUX',ne=22,lx=200,n='DYNAMO ne22 200km',c='blue',p=scratch_frontier,s='run')

  # first_file,num_files = 15,15

#-------------------------------------------------------------------------------

# tmp_file_type = 'output.scream.AutoCal.hourly_inst_ne30pg2.INSTANT.nhours_x1.'
# tmp_file_type = 'output.scream.hourly.AVERAGE.nhours_x1.'
# tmp_file_type = 'output.scream.2D.1hr.AVERAGE.nhours_x1'
tmp_file_type = 'output.scream.2D.1hr.AVERAGE.nhours_x1'

# add_var('precip_total_surf_mass_flux', tmp_file_type, n='precip')
# add_var('VapWaterPath',                tmp_file_type, n='VapWP')
add_var('LiqWaterPath',                tmp_file_type, n='LiqWP')
add_var('IceWaterPath',                tmp_file_type, n='IceWP')
add_var('surf_evap',       tmp_file_type, n='surf_evap')

# tmp_file_type = 'output.scream.3D.1hr.AVERAGE'
# add_var('T_mid',           tmp_file_type, n='T_mid @ k=127',  mlev=127)
# add_var('qv',              tmp_file_type, n='qv @ k=127',  mlev=127)



# add_var('T_mid',              tmp_file_type, n='T_mid @ k=2',  mlev=2)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=5',  mlev=5)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=10', mlev=10)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=20', mlev=20)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=40', mlev=40)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=60', mlev=60)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=80', mlev=80)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=100', mlev=100)
# add_var('T_mid',              tmp_file_type, n='T_mid @ k=120', mlev=120)
# add_var('T_mid',              tmp_file_type, n=None, mlev=-1)

# add_var('precip_liq_surf_mass_flux',   tmp_file_type, n='precip_liq')

add_var('wind_speed_10m',                 tmp_file_type, n='wind_speed_10m')
# add_var('surf_sens_flux',                 tmp_file_type, n='surf_sens_flux')
# add_var('surf_evap',                      tmp_file_type, n='surf_evap')

time_slice = -1 # 24*10 # single time index to load (no averaging)

#-------------------------------------------------------------------------------
# tmp_data_path = os.getenv('HOME')+'/Research/E3SM/pub_figs/2023_screamv1_4season/data'
tmp_data_path = 'data_tmp'

fig_file,fig_type = f'figs_scream/dp-scream.xy.v2','png'

# recalculate = True

num_plot_col = len(var)

#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*num_case)

res = hs.res_contour_fill()
res.lbLabelFontHeightF           = 0.012

#---------------------------------------------------------------------------------------------------
def get_coords(ncol):
  lx = 200
  nx = int(np.sqrt(ncol))
  dx = 200/nx
  ne = int(nx/2)

  uxi = np.linspace(0,lx,nx+1)
  uxc = uxi[:-1] + np.diff(uxi)/2

  xc = np.full(ncol,np.nan)
  yc = np.full(ncol,np.nan)

  for ny in range(ne):
    for nx in range(ne):
      for nyj in range(2):
        for nxi in range(2):
          g = ny*4*ne + nx*4 + nyj*2 + nxi
          i = nx*2+nxi
          j = ny*2+nyj
          xc[g] = uxc[i]
          yc[g] = uxc[j]

  return xc,yc
#---------------------------------------------------------------------------------------------------
def arrange_2d_array(data, x_coords, y_coords):
  # Create dictionaries to map the coordinates to their indices
  unique_x = sorted(set(x_coords))
  unique_y = sorted(set(y_coords))
  
  x_index = {value: idx for idx, value in enumerate(unique_x)}
  y_index = {value: idx for idx, value in enumerate(unique_y)}
  
  # Determine the shape of the 2D array
  max_x = len(unique_x)
  max_y = len(unique_y)
  
  # Create an empty 2D array with the determined shape
  array_2d = np.empty((max_x, max_y))
  
  # Fill the 2D array with the given data
  for i, value in enumerate(data):
    x = x_coords[i]
    y = y_coords[i]
    array_2d[x_index[x], y_index[y]] = value
  
  # Create the appropriately arranged x and y coordinate arrays
  arranged_x_coords = np.array(unique_x)
  arranged_y_coords = np.array(unique_y)
  
  return array_2d, arranged_x_coords, arranged_y_coords
#---------------------------------------------------------------------------------------------------
# tmp_data_path = os.getenv('HOME')+'/Research/E3SM/data_tmp'
# tmp_file_prefix = 'scream.cluster-histogram.DP'
# def get_tmp_file(var,case):
#   return f'{tmp_data_path}/{tmp_file_prefix}.tmp.{var}.{case}.nc'
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
  print(' '*2+'var: '+hc.tcolor.GREEN+var[v]+hc.tcolor.ENDC)
  #-----------------------------------------------------------------------------
  data_list = []
  #-----------------------------------------------------------------------------
  for c in range(num_case):
    print(''+' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
    
    # tmp_file = get_tmp_file(var[v],case[c])

    # if recalculate :
    if True:

      file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{file_type_list[v]}*'
      file_list = sorted(glob.glob(file_path))

      if 'first_file' in globals(): file_list = file_list[first_file:]
      if 'num_files' in globals(): file_list = file_list[:num_files]

      # print(); print(file_path)
      # print(); print(file_list)
      # exit()

      ds = xr.open_mfdataset( file_list )
      ds = ds.isel(time=time_slice)
      # ds = ds.mean(dim='time')

      data = ds[var[v]].load()
      #-------------------------------------------------------------------------
      # hc.print_stat(data ,name=var[v],indent=' '*6,compact=True)
      if 'lev' in data.dims:
        if mlev_list[v] is not None: 
          data = data.isel(lev=mlev_list[v])
      # hc.print_stat(data ,name=var[v],indent=' '*6,compact=True)
      #-------------------------------------------------------------------------
      # print(f'  {hc.tclr.RED}WARNING - removing spatial mean{hc.tclr.END}')
      # data = data - data.mean(dim='ncol')
      # hc.print_stat(data ,name=var[v],indent=' '*6,compact=True)
      #-------------------------------------------------------------------------
      # unit conversions    
      if 'precip' in var[v]: data = data*86400.*1e3
      #-------------------------------------------------------------------------
      data_list.append( data.values )

    #    # Write to file
    #    print(' '*6+f'Writing data to file: {tmp_file}')
    #    gbl_mean.name = tvar
    #    tmp_ds = xr.Dataset()
    #    # tmp_ds['time'] = time
    #    tmp_ds[tvar]   = gbl_mean
    #    tmp_ds.to_netcdf(path=tmp_file,mode='w')
    # else:
    #    print(' '*6+f'Reading pre-calculated data from file: {hc.tcolor.MAGENTA}{tmp_file}{hc.tcolor.ENDC}')
    #    tmp_ds = xr.open_dataset( tmp_file )
    #    # time_tmp = tmp_ds['time'].values
    #    gbl_mean = tmp_ds[tvar]
  #-----------------------------------------------------------------------------
  #-----------------------------------------------------------------------------
  for c in range(num_case):
    xc,yc = get_coords( len(data_list[c]) )

    tres = copy.deepcopy(res)
    tres.cnFillPalette = 'MPL_viridis'
    tres.cnFillMode    = 'RasterFill'
    tres.sfXArray      = xc
    tres.sfYArray      = yc
    #---------------------------------------------------------------------------
    if 'precip' in var[v]: tres.cnLevels = np.arange(3,60+3,3)
    # if var[v]=='T_mid': tres.cnLevels = np.arange(-5,5+0.2,0.2)

    if var[v]=='LiqWaterPath': tres.cnLevels = np.arange(0.1,3.1+0.1,0.1)
    if var[v]=='IceWaterPath': tres.cnLevels = np.arange(0.5,15.5+1,1)

    # surface level
    if var[v]=='qv'       : tres.cnLevels = np.arange(10e-3,14e-3+0.2e-3,0.2e-3)
    if var[v]=='T_mid'    : tres.cnLevels = np.arange(293,300,1)
    if var[v]=='surf_evap': tres.cnLevels = np.arange(2e-5,12e-5,1e-5)

    if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
    #---------------------------------------------------------------------------
    ip = c*num_var + v

    plot[ip] = ngl.contour(wks, data_list[c], tres)

    hs.set_subtitles(wks, plot[ip], 
                    left_string=case_name[c], 
                    center_string='', 
                    right_string=var_str[v], 
                    font_height=0.008)
#-------------------------------------------------------------------------------
pres = hs.setres_panel()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

if num_case==1 or num_var==1:
  layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
  # layout = [num_var,num_case]
  layout = [num_case,num_var]

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#-------------------------------------------------------------------------------
