import os, glob, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs

# case_name = 'DPSCREAM.RCE-TEST-00.ne20.SST_200'
# input_file_name = os.getenv('HOME')+f'/SCREAM/scratch/{case_name}/run/{case_name}.eam.h0.2000-01-06-00000.nc'

# case_name = 'DPSCREAM.RCE-TEST-00.ne20.SST_270'
case_name = 'DPSCREAM.DYNAMO-TEST-01.ne16.len_200km.DT_60'

### list of variables to plot
var = ['CLDLIQ']

### single time index of file to load (no averaging)
time_slice = 0

### output figure type and name
fig_file,fig_type = 'figs_scream/dp-scream.xz.v1','png'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
if 'input_file_name' not in locals():
   files = glob.glob(os.getenv('HOME')+f'/SCREAM/scratch/{case_name}/run/{case_name}.eam.h0.*.nc')
   input_file_name = sorted(files)[-1]

print()
print(f'  input_file_name: {input_file_name}')

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_var

res = hs.res_contour_fill()

res.trYMaxF = 10e3

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+var[v])

   tres = copy.deepcopy(res)
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   ds = xr.open_dataset( input_file_name )

   ### identify points at center of domain (not that it matters)
   xcenter = ds.crm_grid_x.values[0,:]
   diff = np.abs( xcenter-xcenter.mean() )
   center_flag = xr.DataArray( diff==np.min(diff), dims=['ncol'] )

   ncol = ds['ncol'].where(center_flag,drop=True)

   X = ds[var[v]].isel(time=time_slice).where(center_flag,drop=True)
   # Z = ds[ 'Z3' ].isel(time=time_slice).where(center_flag,drop=True)

   # print(); print(X)
   # print(); print(X.transpose())
   # xx = ds.crm_grid_y.where(center_flag,drop=True).values[0,:]
   # print(); print(xx)
   # exit()

   tres.cnFillMode    = 'RasterFill'
   tres.sfXArray      = ds.crm_grid_y.where(center_flag,drop=True).values[0,:]
   tres.sfYArray      = ds['Z3'].isel(ncol=0,time=time_slice).values

   #----------------------------------------------------------------------------
   # Set colors and contour levels
   #----------------------------------------------------------------------------
   tres.cnFillPalette = "MPL_viridis"

   # ### specify specific contour intervals
   # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,60+1,1)
   # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
   # # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2) # better for MMF
   # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2,-0.2, num=60).round(decimals=2) # better for non-MMF

   # ### use this for symmetric contour intervals
   # if var[v] in ['U','V'] :
   #    cmin,cmax,cint,clev = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
   #                                               cint=None, max_steps=21,              \
   #                                               returnLevels=True, aboutZero=True )
   #    tres.cnLevels = np.linspace(cmin,cmax,num=91)

   # if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
   
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   ip = v

   plot[ip] = ngl.contour(wks,X.values,tres)

   hs.set_subtitles(wks, plot[ip], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

layout = [1,num_var]

ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
