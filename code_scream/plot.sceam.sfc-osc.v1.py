import os, glob, ngl, subprocess as sp, numpy as np, xarray as xr, dask
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string, cmocean
import numba
# export PYNGL_RANGS=~/.conda/envs/pyn_env/lib/ncarg/database/rangs
host = hc.get_host()
#-------------------------------------------------------------------------------
'''
salloc -A cli115 -p gpu -N 1 -t 4:00:00
micromamba activate pyn_env
'''
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
obs_flag = []
clr,dsh = [],[]
def add_case(case_in,n=None,p=None,s=None,g=None,c='black',d=0,obs=False):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   obs_flag.append(obs)
   clr.append(c)
   dsh.append(d)
#-------------------------------------------------------------------------------
var,var_str,klev_list = [],[],[]
unit_fac_list = []
def add_var(var_name,vstr=None,klev=0,unit_fac=None): 
   var.append(var_name);
   if vstr is None: 
      var_str.append(var_name)
   else:
      var_str.append(vstr)
   klev_list.append(klev)
   unit_fac_list.append(unit_fac)
#-------------------------------------------------------------------------------

# add_case('ERA5',n='ERA5', p=obs_data_root,s='daily',obs=True,d=0,c='black')
# add_case('IMERG',n='IMERG Jan 2020',p=obs_data_root,s='daily_QC_Jan_2020',obs=True,d=1,c='red')

### 2025 popcorn investigation
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne1024pg2_scrip.nc'
# tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
# r1_str = 'cfr_1.acc_10.rsc_0.eci_1e-05.eri_1e-05.mti_7.4e7'
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2',               n='SCREAM ctrl',p=tmp_scratch,s='run')
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1',         n='SCREAM cfrc',p=tmp_scratch,s='run')
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r1_str}',      n='SCREAM rcp1',p=tmp_scratch,s='run')
# # add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1.ne256pg2',                        n='SCREAM',p=tmp_scratch,s='run')
# htype = 'output.scream.2D.1hr.inst.INSTANT.nhours_x1'
# first_file,num_files = 32,1


### 2025 time step tests
scrip_file_path = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne1024pg2.nc'
tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_100.dyn_fac_12.rmp_fac_2.trc_fac_6.trc_ss_1', n='SCREAM ctrl',d=0,c='black',p=tmp_scratch,s='run')
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_100.dyn_fac_12.rmp_fac_2.trc_fac_12.trc_ss_2',n='SCREAM',     d=1,c='black',p=tmp_scratch,s='run')
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_75.dyn_fac_10.rmp_fac_1.trc_fac_5.trc_ss_1',  n='SCREAM',     d=0,c='blue',p=tmp_scratch,s='run')
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_75.dyn_fac_9.rmp_fac_1.trc_fac_9.trc_ss_2',   n='SCREAM',     d=0,c='blue',p=tmp_scratch,s='run')
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_75.dyn_fac_8.rmp_fac_2.trc_fac_4.trc_ss_1',   n='SCREAM',     d=0,c='blue',p=tmp_scratch,s='run')
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_75.dyn_fac_8.rmp_fac_2.trc_fac_8.trc_ss_2',   n='SCREAM',     d=0,c='blue',p=tmp_scratch,s='run')
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_60.dyn_fac_8.rmp_fac_2.trc_fac_4.trc_ss_1',   n='SCREAM',     d=0,c='red',p=tmp_scratch,s='run')
add_case('SCREAM.2025-DT-01.F2010-SCREAMv1.ne1024pg2.dt_phy_60.dyn_fac_8.rmp_fac_2.trc_fac_8.trc_ss_2',   n='SCREAM',     d=0,c='red',p=tmp_scratch,s='run')
htype = 'output.scream.2D'
first_file,num_files = 0,1



#-------------------------------------------------------------------------------

# add_var('precip_total_surf_mass_flux',vstr='precip',unit_fac=86400*1e3)
# add_var('LiqWaterPath')
# add_var('surf_sens_flux')
# add_var('surf_evap')
add_var('surf_mom_flux')
# add_var('U_at_model_bot')
# add_var('V_at_model_bot')

recalculate = True
reduce_ncol = True

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.sfc-osc.v1'

tmp_file_head = 'data_temp/scream.sfc-osc.v1'

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# first_file,num_files = 0,12
# first_file,num_files = 300,1
# first_file,num_files = 2,1

# use_snapshot,ss_t = True,0

print_stats = True

var_x_case = True

# num_plot_col = len(case)

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------

num_var,num_case = len(var),len(case)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
# plot = [None]*(num_var*num_case)
   
# res = hs.res_contour_fill_map()
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
# res.lbLabelFontHeightF           = 0.01
# # res.tmXBOn                       = False
# # res.tmYLOn                       = False
# # res.mpGeophysicalLineColor       = 'white'

# # res.mpCenterLonF                 = 180
# # res.mpProjection                 = 'Robinson'

# res.mpDataBaseVersion = 'MediumRes'
# # res.mpDataBaseVersion = 'HighRes'

# dx = 0.25
# if 'lat1' in locals() : res.mpMinLatF = lat1+dx; res.mpMaxLatF = lat2-dx
# if 'lon1' in locals() : res.mpMinLonF = lon1+dx; res.mpMaxLonF = lon2-dx

#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'scream'
   return comp
#---------------------------------------------------------------------------------------------------
def get_ctr_str(glb_avg=None):
   ctr_str = ''
   if 'lat1' in globals():
      lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
      lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
      ctr_str += f' {lat1_str}:{lat2_str} '
   if 'lon1' in globals():
      lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
      lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
      ctr_str += f' {lon1_str}:{lon2_str} '
   # if glb_avg is not None:
   #    # add logic here t display global average value?
   return ctr_str
#---------------------------------------------------------------------------------------------------

# scrip_ds = xr.open_dataset(scrip_file_path)

use_mask = True

# if use_mask:

#    lat_name,lon_name = 'grid_center_lat','grid_center_lon'
#    tmp_data = np.ones(len(scrip_ds[lat_name]),dtype=bool)
#    mask = xr.DataArray( tmp_data, coords=scrip_ds[lat_name].coords )
#    if 'lat1' in locals(): mask = mask & (scrip_ds[lat_name]>=lat1) & (scrip_ds[lat_name]<=lat2)
#    if 'lon1' in locals(): mask = mask & (scrip_ds[lon_name]>=lon1) & (scrip_ds[lon_name]<=lon2)

#    scrip_ds_center_lon = scrip_ds['grid_center_lon'].where(mask,drop=True)
#    scrip_ds_center_lat = scrip_ds['grid_center_lat'].where(mask,drop=True)
#    scrip_ds_corner_lon = scrip_ds['grid_corner_lon'].where(mask,drop=True)
#    scrip_ds_corner_lat = scrip_ds['grid_corner_lat'].where(mask,drop=True)

#    mask = mask.rename({'grid_size':'ncol'})

# else:

#    scrip_ds_center_lon = scrip_ds['grid_center_lon']
#    scrip_ds_center_lat = scrip_ds['grid_center_lat']
#    scrip_ds_corner_lon = scrip_ds['grid_corner_lon']
#    scrip_ds_corner_lat = scrip_ds['grid_corner_lat']


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   bin_list = []
   glb_avg_list = []
   # std_list,cnt_list = [],[]

   bins = None
   if var[v]=='surf_sens_flux': bins = np.arange(20,800+20,20)
   # if var[v]=='surf_evap'     : bins = np.arange(20,800+20,20)
   if var[v]=='surf_mom_flux' : bins = np.arange(0.2,8+0.2,0.2)
   if bins is None: raise ValueError(f'bins not set for variable {var[v]}')

   for c in range(num_case):
      # print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      tmp_file = f'{tmp_file_head}.{case[c]}.{var[v]}.f0_{first_file}.nf_{num_files}.nc'
      print(' '*4+f'case: {hc.tclr.GREEN}{case[c]}{hc.tclr.END}  =>  {tmp_file}')
      if recalculate:
         #----------------------------------------------------------------------
         # read the data
         file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*{htype}*'
         file_list = sorted(glob.glob(file_path))
         if 'first_file' in locals(): file_list = file_list[first_file:]
         if 'num_files' in locals(): file_list = file_list[:num_files]
         ds = xr.open_mfdataset( file_list )
         #----------------------------------------------------------------------
         # tvar = var[v]
         # if var[v]=='horiz_winds_at_model_bot_u': tvar = 'horiz_winds_at_model_bot'
         # if var[v]=='horiz_winds_at_model_bot_v': tvar = 'horiz_winds_at_model_bot'
         #----------------------------------------------------------------------

         # with dask.config.set(**{'array.slicing.split_large_chunks': True}):
         if True:

            indent = ' '*6
            ####################################################################
            # # Apply spatial mask
            # if v==0 and c==0:
            #    print(' '*6+'Creating mask')
            #    lat1,lat2 = -60,60
            #    mask = xr.DataArray( np.ones(len(ds['lat']),dtype=bool), coords=ds['lat'].coords )
            #    if 'lat1' in locals(): mask = mask & (ds['lat']>=lat1) & (ds['lat']<=lat2)
            #    if 'lon1' in locals(): mask = mask & (ds['lon']>=lon1) & (ds['lon']<=lon2)
            #    mask.load()
            # print(' '*6+'Applying mask')
            # ds = ds.where(mask,drop=True)
            

            ####################################################################
            print(indent+'loading variable data')

            tvar = var[v]
            if var[v]=='horiz_winds_at_model_bot_u': tvar = 'horiz_winds_at_model_bot'
            if var[v]=='horiz_winds_at_model_bot_v': tvar = 'horiz_winds_at_model_bot'
            data = ds[tvar]
            if var[v]=='horiz_winds_at_model_bot_u': data = data.isel(dim2=0)
            if var[v]=='horiz_winds_at_model_bot_v': data = data.isel(dim2=1)

            if var[v]=='surf_mom_flux':
               # data = np.sqrt( np.square(data[:,:,0]) + np.square(data[:,:,1]) )
               data = data[:,:,0]

            ####################################################################
            print(indent+'reducing time dimension')

            # reduce time dimension by time_fac
            time_fac = 4
            time_seg = time_fac-1
            max_time = len(data['time'])
            t1 = int(max_time/time_fac)*time_seg
            t2 = int(max_time/time_fac)*(time_seg+1)
            data = data[t1:t2,:]

            ####################################################################
            # reduce time dimension by time_fac - again
            if reduce_ncol:
               time_fac = 4
               time_seg = 0#time_fac-1
               max_time = len(data['time'])
               t1 = int(max_time/time_fac)*time_seg
               t2 = int(max_time/time_fac)*(time_seg+1)
               data = data[t1:t2,:]
            ####################################################################
            # reduce ncol dimension by ncol_fac - FOR TESTING ONLY
            if reduce_ncol:
               # ncol_fac = (64*2*2*2*2)*2*2
               ncol_fac = (64*2*2*2*2)*2*2*2*2*2*2
               ncol_seg = int(ncol_fac/2)
               ncol_max = len(data['ncol'])
               n1 = int(ncol_max/ncol_fac)*ncol_seg
               n2 = int(ncol_max/ncol_fac)*(ncol_seg+1)
               print(indent+f'reduce_ncol - n1: {n1}  n2: {n2}  ncol: {(n2-n1)}')
               data = data[:,n1:n2]
               print(); print(f'data shape: {data.shape}'); print()
               print(indent+'reduce_ncol - loading reduced data')
               # data.compute()
               # data = data[0:10,0:2]
               # data.compute()
            ####################################################################

            print(indent+'calculating delta')

            delta = np.absolute( data.diff('time'))

            print(indent+'calculating stats')

            if reduce_ncol:
               hc.print_stat(data, name='data', stat='nax',indent=' '*4,compact=True)
               hc.print_stat(delta,name='delta',stat='nax',indent=' '*4,compact=True)
               exit(f'\nEXITING - disable reduce_ncol after setting bins for var: {var[v]}')

            print(indent+'calculating histogram')

            hist, bin_edg = np.histogram( delta.values, bins=bins)
            bin_ctr = bin_edg[:-1] + np.diff(bin_edg)/2

            ####################################################################
            # max_ind = delta.argmax(dim=('time','ncol'))
            # print()
            # # print(max_ind)
            # print()
            # print(max_ind['ncol'].values)
            # print(max_ind['time'].values)
            # ncol_max = max_ind['ncol'].values
            # time_max = max_ind['time'].values
            # print()
            # span = 4
            # for t in range(time_max-span,time_max+span):
            #    val = data.isel(ncol=ncol_max,time=t).values
            #    msg = f'  {t}  {val}'
            #    if t==time_max: msg = msg+'  <<<<<'
            #    print(msg)
            # print()
            ####################################################################
         
         #----------------------------------------------------------------------
         # Write to file
         if os.path.isfile(tmp_file) : os.remove(tmp_file)
         tmp_ds = xr.Dataset()
         tmp_ds[f'{var[v]}_hist'] = hist
         tmp_ds['bin_ctr']        = bin_ctr
         tmp_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         tmp_ds = xr.open_dataset( tmp_file )
         hist    = tmp_ds[f'{var[v]}_hist']
         bin_ctr = tmp_ds['bin_ctr'].values

      #-------------------------------------------------------------------------
      
      data_list.append( hist )
      bin_list.append( bin_ctr )

   #------------------------------------------------------------------------------------------------
   # Create delta distrubtion plot
   #------------------------------------------------------------------------------------------------
   data_min = 0.9 # np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if v==0:
      plot = [None]*num_var
      res = hs.res_xy()
      # res.vpHeightF        = 0.4
      # res.xyLineThicknessF = 2

   # res.xyExplicitLegendLabels = case_name
   # res.pmLegendDisplayMode    = "Always"
   # res.pmLegendOrthogonalPosF = -1.13
   # res.pmLegendParallelPosF   =  0.8+0.04
   # res.pmLegendWidthF         =  0.16
   # res.pmLegendHeightF        =  0.12   
   # res.lgBoxMinorExtentF      =  0.16   

   # res.tiYAxisString    = var_str[v]
   # res.trXMinF          = np.min(bin_ctr)
   # res.trXMaxF          = np.max(bin_ctr)
   res.xyYStyle         = 'Log'
   res.trYMinF          = data_min
   res.trYMaxF          = data_max

   ip = v

   for c in range(num_case):
      res.xyLineColor = clr[c]
      res.xyDashPattern = dsh[c]
      data_tmp = data_list[c]
      # data_tmp = np.where(data_tmp<0.1, 0.1, data_tmp)
      data_tmp = np.where(data_tmp<0.1, np.nan, data_tmp)
      data_tmp = np.ma.masked_invalid(data_tmp)
      tplot = ngl.xy(wks, bin_list[c], data_tmp, res)
      if c==0:
         plot[ip] = tplot 
      else:
         ngl.overlay( plot[ip], tplot )

   hs.set_subtitles(wks, plot[ip], '', '', var_str[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot

# layout = [num_var,num_case] if var_x_case else [num_case_alt,num_var]

# if num_case==1 or num_var==1:
#    layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

num_plot_col = 2
layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### use common label bar
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 10

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
