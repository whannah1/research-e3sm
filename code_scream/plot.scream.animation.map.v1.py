import os, ngl, copy, glob, xarray as xr, numpy as np
import hapy_common as hc

# name = 'SCREAM_ne1024'
# input_file_name = '/global/homes/w/whannah/E3SM/scratch/scream/add-ne1024-grid.FC5AV1C-H01A.ne1024np4_360x720cru_oRRS15to5.cori-knl_intel.2048x8x16.ifs-hindcast.20190716-0819.cam.h1.2016-08-01-00000.nc'
# input_file_name = '/global/cscratch1/sd/bhillma/acme_scratch/cori-knl/master.FC5AV1C-H01A.ne1024np4_360x720cru_oRRS15to5.cori-knl_intel.1024x8x16.thetaNH-L128-microp3-shoc_sgs-rrtmgp-chemnone-deepoff.ifs-hindcast.20191113-1020/run/master.FC5AV1C-H01A.ne1024np4_360x720cru_oRRS15to5.cori-knl_intel.1024x8x16.thetaNH-L128-microp3-shoc_sgs-rrtmgp-chemnone-deepoff.ifs-hindcast.20191113-1020.cam.h1.2016-08-01-00000.nc'

# name = 'SCREAM-HICCUP_ne1024'
# case = 'master.ne1024np4_360x720cru_oRRS15to5.FSCREAM-HR-DYAMOND1.cori-knl_intel.1536x8x16.20200423-1134'
# input_file_path = f'/global/cscratch1/sd/terai/E3SM_simulation/{case}/run/{case}.cam.h2.2016-08-01-*.nc'

# name = 'F2010-SCREAMv1-noAero ne1024pg2'
# case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220908_low_t_mid.bca00ce9f65460369cb679fdd144897aba51a428'
# idir = f'/gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/{case}/run'
# input_file_name_srf = f'{idir}/*scream.surf*'
# input_file_name_avg = f'{idir}/*scream.hi.AVERAGE.*'
# input_file_name_min = f'{idir}/*scream.hi.MIN.*'
# input_file_path = input_file_name_srf



name = 'Decadal Production'
case = 'decadal-production-20240305.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.run1'
idir = f'/lustre/orion/cli115/proj-shared/brhillman/e3sm_scratch/{case}/run'
input_file_path = f'{idir}/output.scream.decadal.1hourlyINST_ne1024pg2.INSTANT.nhours_x1.1994-11-0*'

# name = 'SCREAM PDC'
# case = 'SCREAM.2024-NIMBUS-01.NN_512.ne0np4-saomai-128x8_EC30to60E2r2.F2010-SCREAMv1.PDC'
# idir = f'/p/lustre1/hannah6/scream_scratch/{case}/run'
# input_file_path = f'{idir}/output.saomai.scream.hourly_instant.INSTANT.nhours_x1.2006-08-*-00000.nc'

# name = 'SCREAM PGW'
# case = 'SCREAM.2024-NIMBUS-01.NN_512.ne0np4-saomai-128x8_EC30to60E2r2.F2010-SCREAMv1.PGW'
# idir = f'/p/lustre1/hannah6/scream_scratch/{case}/run'
# input_file_path = f'{idir}/output.saomai.scream.hourly_instant.INSTANT.nhours_x1.2006-08-*-00000.nc'



### scrip file for native grid plot
# scrip_file_name = '/global/homes/w/whannah/E3SM/scratch/scream/ne1024np4_scrip_c20190603.nc'
# scrip_file_name = '/global/cscratch1/sd/terai/mapping/ne1024np4_scrip_c20190603.nc'
# scrip_file_name = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/files_grid/scrip_ne1024pg2.nc'
# scrip_file_name = '/gpfs/alpine/cli115/proj-shared/hannah6/grid_files/ne1024pg2_scrip.nc'
# scrip_file_name = '/p/lustre1/hannah6/2024-saomai-data/Saomai_2006_ne128x8_lon130E_lat25Npg2.scrip.nc'

#-------------------------------------------------------------------------------
var,var_str,lev_list = [],[],[]
def add_var(var_name,vstr=None,lev=0): 
   var.append(var_name);
   if vstr is None: 
      var_str.append(var_name)
   else:
      var_str.append(vstr)
   lev_list.append(lev)
#-------------------------------------------------------------------------------
### build list of variables to plot
add_var('SeaLevelPressure','Psl')
add_var('VapWaterPath','CWV')
# add_var('precip_total_surf_mass_flux','precip')
add_var('U_at_2m_above_surface','U2m')

add_var('precip_total_surf_mass_flux','precip')
# add_var('VapWaterPath','VWP')
# add_var('LiqWaterPath','LWP')
# add_var('IceWaterPath','IWP')

# add_var('T_2m')
# add_var('')
# add_var('surf_evap')
# add_var('surf_sens_flux')


num_plot_col = 3


### single time index to load (no averaging)
# time1,time2 = 400, 405
# time1,time2 = 435, 480
# time1,time2 = 481, 500
# time1,time2 = 400, 500


time1,time2,dtime = 0,24,1
# time1,time2,dtime = 0,48,1
# time1,time2,dtime = 2,2,1
# time1,time2,dtime = 1,24,1
# time1,time2,dtime = 25,48,1
# time1,time2,dtime = 1,48,1

# time1,time2,dtime = 1,99,2


create_png,create_gif = True,True
# create_png,create_gif = True,False
# create_png,create_gif = False,True

### output figure type and name
fig_type = 'png'

anomaly_from_initial = False

# lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = -30,40,360-140,360-45     # East Pac + S. America
# lat1,lat2,lon1,lon2 = -30,0,360-90,360-60     # S. America
# lat1,lat2,lon1,lon2 = -45,0,90,180            # Australia
# lat1,lat2,lon1,lon2 = 79,84,327,332 

lat1,lat2,lon1,lon2 =10,40,100,140  # Taiwan
# lat1,lat2,lon1,lon2 =10,20,130,140  # Taiwan
# lat1,lat2,lon1,lon2 =16,18,134,136  # Taiwan

# Set final animation file (don't include file suffix)
# gif_file = os.getenv('HOME')+f'/Research/E3SM/figs_scream/scream.animation.map.v1.{var[0]}'
gif_file = f'figs_scream/scream.animation.map.v1'#.{var[0]}'
if 'lat1' in vars(): gif_file += f'.lat_{lat1}_{lat2}'
if 'lon1' in vars(): gif_file += f'.lon_{lon1}_{lon2}'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.lbLabelBarOn                 = True
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.lbOrientation                = 'Horizontal'
res.lbLabelFontHeightF           = 0.008
res.mpGridAndLimbOn              = False
res.mpCenterLonF                 = 180
res.mpLimitMode                  = 'LatLon' 
if 'lat1' in vars() : res.mpMinLatF = lat1
if 'lat2' in vars() : res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1
if 'lon2' in vars() : res.mpMaxLonF = lon2

pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

res.mpGeophysicalLineColor = 'white'

#---------------------------------------------------------------------------------------------------
# define function to add subtitles to the top of plot
#---------------------------------------------------------------------------------------------------
def set_subtitles(wks, plot, left_string='', center_string='', right_string='', font_height=0.01):
   ttres          = ngl.Resources()
   ttres.nglDraw  = False

   ### Use plot extent to call ngl.text(), otherwise you will see this error: 
   ### GKS ERROR NUMBER   51 ISSUED FROM SUBROUTINE GSVP  : --RECTANGLE DEFINITION IS INVALID
   strx = ngl.get_float(plot,'trXMinF')
   stry = ngl.get_float(plot,'trYMinF')
   ttres.txFontHeightF = font_height

   ### Set annotation resources to describe how close text is to be attached to plot
   amres = ngl.Resources()
   if not hasattr(ttres,'amOrthogonalPosF'):
      amres.amOrthogonalPosF = -0.52   # Top of plot plus a little extra to stay off the border
   else:
      amres.amOrthogonalPosF = ttres.amOrthogonalPosF

   ### Add left string
   amres.amJust,amres.amParallelPosF = 'BottomLeft', -0.5   # Left-justified
   tx_id_l   = ngl.text(wks, plot, left_string, strx, stry, ttres)
   anno_id_l = ngl.add_annotation(plot, tx_id_l, amres)
   # Add center string
   amres.amJust,amres.amParallelPosF = 'BottomCenter', 0.0   # Centered
   tx_id_c   = ngl.text(wks, plot, center_string, strx, stry, ttres)
   anno_id_c = ngl.add_annotation(plot, tx_id_c, amres)
   # Add right string
   amres.amJust,amres.amParallelPosF = 'BottomRight', 0.5   # Right-justified
   tx_id_r   = ngl.text(wks, plot, right_string, strx, stry, ttres)
   anno_id_r = ngl.add_annotation(plot, tx_id_r, amres)

   return
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# def drop_p3_dim(ds): 
#    return ds if 'P3_input_dim' not in ds.variables else ds.drop(['P3_input_dim'])
# ds = xr.open_mfdataset( input_file_path, combine='by_coords', preprocess=drop_p3_dim )
# ds = xr.open_dataset( input_file_name )

scrip_ds = xr.open_dataset(scrip_file_name)
lat = scrip_ds['grid_center_lat'].rename({'grid_size':'ncol'})
lon = scrip_ds['grid_center_lon'].rename({'grid_size':'ncol'})

# file_list = sorted(glob.glob(input_file_path))
# file_list.remove(file_list[-1]) # last file is empty
# ds = xr.open_mfdataset( file_list, use_cftime=True )

# for f in file_list: print(f)
# exit()

ds = xr.open_mfdataset( file_list, use_cftime=True )


# if 'lat1' in locals() or 'lon1' in locals() : 
#    mask = xr.DataArray( np.ones([len(ds['ncol'])],dtype=bool), coords=[('ncol',ds['ncol'].values)], dims='ncol' )
#    if 'lat1' in locals(): mask = mask & (lat>=lat1) & (lat<=lat2)
#    if 'lon1' in locals(): mask = mask & (lon>=lon1) & (lon<=lon2)

# # Get rid of time dimension if added by combining files
# if 'time' in mask.dims: mask = mask.isel(time=0).squeeze()

tfiles = ''
if 'dtime' not in locals(): dtime = 1
# for t in range(time1,time2+dtime,dtime):
time_list = list(np.arange(time1,time2+dtime,dtime))
baseline_time = 0
if anomaly_from_initial and time1!=baseline_time: time_list.insert(0,baseline_time)
for t in time_list:
   fig_file = gif_file+'.'+str(t).zfill(3)
   if (not anomaly_from_initial) or (anomaly_from_initial and t!=baseline_time): 
      tfiles = tfiles+'  '+f'{fig_file}.{fig_type}'
      tfile_name = f'{fig_file}.{fig_type}'
      tfile_name = tfile_name.replace((os.getenv('HOME')+'/Research/E3SM/'),'')
      print(f'  t: {t:3d}     {tfile_name}')

   if create_png :

      num_case = 2


      ### create the plot workstation
      wkres = ngl.Resources()
      npix = 1024; wkres.wkWidth,wkres.wkHeight=npix,npix
      # npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
      # npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix
      wks = ngl.open_wks(fig_type,fig_file,wkres)
      plot = [None] * (num_case*num_var)

      
      for c in range(num_case):

         if c==0:
            name = 'SCREAM PDC'
            case = 'SCREAM.2024-NIMBUS-01.NN_512.ne0np4-saomai-128x8_EC30to60E2r2.F2010-SCREAMv1.PDC'
            idir = f'/p/lustre1/hannah6/scream_scratch/{case}/run'
            input_file_path = f'{idir}/output.saomai.scream.hourly_instant.INSTANT.nhours_x1.2006-08-*-00000.nc'
         if c==1:
            name = 'SCREAM PGW'
            case = 'SCREAM.2024-NIMBUS-01.NN_512.ne0np4-saomai-128x8_EC30to60E2r2.F2010-SCREAMv1.PGW'
            idir = f'/p/lustre1/hannah6/scream_scratch/{case}/run'
            input_file_path = f'{idir}/output.saomai.scream.hourly_instant.INSTANT.nhours_x1.2006-08-*-00000.nc'

         file_list = sorted(glob.glob(input_file_path))
         file_list.remove(file_list[-1]) # last file is empty
         ds = xr.open_mfdataset( file_list, use_cftime=True )

         for v in range(num_var):
            # print('  var: '+var[v])
            #-------------------------------------------------------------------------
            # read the data
            #-------------------------------------------------------------------------
            data = ds[var[v]]
            data = data.isel(time=t)
            if 'mask' in locals(): data = data.where( mask ,drop=True)

            # Adjust units
            if var[v]=='ps': data = data/1e2
            if var[v]=='precip_total_surf_mass_flux': data = data*86400*1e3 # m/s => mm/day
            # if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3
            # if var[v] in ['Q','Q850'] : data = data*1e3


            # hc.print_stat(data,name=var[v],stat='naxs')

            ### print temporal frequency and length
            # dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
            # print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

            tres.cnFillPalette = 'MPL_viridis'
            
            if anomaly_from_initial:
               if var[v]=='ps'            : tres.cnLevels = np.arange(-50,50+5,5)/1e1
            else:
               if var[v]=='ps'            : tres.cnLevels = np.arange(950,1040+1,1)
               # if var[v]=='ps'            : tres.cnLevels = np.logspace( np.log10(800), np.log10(1008), num=40)
               # if var[v]=='ps'            : tres.cnLevels = np.array([800,820,840,860,880,
               #                                                        900,910,920,930,940,950,
               #                                                        960,965,970,975,980,985,
               #                                                        990,992,994,996,998,
               #                                                        995,996,997,998,999,
               #                                                        1000,1001,1002,1003,1004,1005,1006,1007,1008])
               if var[v]=='surf_sens_flux': tres.cnLevels = np.arange(0,2000+50,50)

               # if var[v]=='precip_total_surf_mass_flux':tres.cnLevels = np.arange(0,200+10,10)
               if var[v]=='precip_total_surf_mass_flux':tres.cnLevels = np.logspace( -1, 2, num=20).round(decimals=2)
               if var[v]=='LW_flux_up_at_model_top'    :tres.cnLevels = np.arange(80,320+10,10)
            

               ### Calculate area-weighted global mean
               # area = ds['area'].where( mask,drop=True)
               # gbl_mean = ( (data*area).sum() / area.sum() ).values 
               # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

               if anomaly_from_initial:
                  if t==baseline_time: baseline = data; continue
                  data = data - baseline

               #-------------------------------------------------------------------------
               # Set colors and contour levels
               #-------------------------------------------------------------------------
               tres = copy.deepcopy(res)

               tres.cnFillPalette = 'MPL_viridis'
               
               if anomaly_from_initial:
                  if var[v]=='ps'            : tres.cnLevels = np.arange(-50,50+5,5)/1e1
               else:
                  if var[v]=='ps'            : tres.cnLevels = np.arange(950,1040+1,1)
                  # if var[v]=='ps'            : tres.cnLevels = np.logspace( np.log10(800), np.log10(1008), num=40)
                  # if var[v]=='ps'            : tres.cnLevels = np.array([800,820,840,860,880,
                  #                                                        900,910,920,930,940,950,
                  #                                                        960,965,970,975,980,985,
                  #                                                        990,992,994,996,998,
                  #                                                        995,996,997,998,999,
                  #                                                        1000,1001,1002,1003,1004,1005,1006,1007,1008])
                  if var[v]=='surf_sens_flux'             : tres.cnLevels = np.arange(0,2000+50,50)
                  if var[v]=='SeaLevelPressure'           : tres.cnLevels = np.arange(920,954+2,2)*1e2
                  if var[v]=='VapWaterPath'               : tres.cnLevels = np.arange(10,90+5,5)
                  if var[v]=='U_at_2m_above_surface'      : tres.cnLevels = np.arange(-20,20+4,4)
                  # if var[v]=='precip_total_surf_mass_flux': tres.cnLevels = np.arange(0,100+5,5)
                  if var[v]=='precip_total_surf_mass_flux': tres.cnLevels = np.logspace( -1, 2.8, num=40).round(decimals=2)
                  # if var[v]=='surf_evap'                  : tres.cnLevels = np.arange(250,300+1,1) #np.logspace( -1, 2.8, num=40).round(decimals=2)

               ### use this for symmetric contour intervals
               if var[v] in ['U','V'] :
                  cmin,cmax,cint,clev = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
                                                             cint=None, max_steps=21,              \
                                                             returnLevels=True, aboutZero=True )
                  tres.cnLevels = np.linspace(cmin,cmax,num=91)

               if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
               
               #-------------------------------------------------------------------------
               # Set up cell fill attributes using scrip grid file
               #-------------------------------------------------------------------------
               scripfile = xr.open_dataset(scrip_file_name)
               tres.cnFillMode    = 'CellFill'
               if 'mask' in locals():
                  tres.sfXArray      = scripfile['grid_center_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
                  tres.sfYArray      = scripfile['grid_center_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
                  tres.sfXCellBounds = scripfile['grid_corner_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values 
                  tres.sfYCellBounds = scripfile['grid_corner_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
               else:
                  tres.sfXArray      = scripfile['grid_center_lon'].rename({'grid_size':'ncol'}).values
                  tres.sfYArray      = scripfile['grid_center_lat'].rename({'grid_size':'ncol'}).values
                  tres.sfXCellBounds = scripfile['grid_corner_lon'].rename({'grid_size':'ncol'}).values
                  tres.sfYCellBounds = scripfile['grid_corner_lat'].rename({'grid_size':'ncol'}).values
               
               #-------------------------------------------------------------------------
               # Create plot
               #-------------------------------------------------------------------------
               ip = c*num_var + v
               # ip = v*num_case + c
               plot[ip] = ngl.contour_map(wks,data.values,tres)
               set_subtitles(wks, plot[ip], name, '', var_str[v], font_height=0.01)
               # plot.append( ngl.contour_map(wks,data.values,tres) )
               # set_subtitles(wks, plot[len(plot)-1], name, '', var_str[v], font_height=0.01)
      #------------------------------------------------------------------------------------------------
      # Finalize plot
      #------------------------------------------------------------------------------------------------
      # if 'num_plot_col' in locals():
      #    layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
      # else:
      #    # layout = [len(plot),1]
      #    layout = [1,len(plot)]

      layout = [num_case,num_var]
      # layout = [num_var,num_case]

      ngl.panel(wks,plot,layout,pres)
      # ngl.panel(wks,plot[0:len(plot)],layout,pres)
      # del wks
      ngl.destroy(wks)

      ### trim white space from image using imagemagik
      fig_file = fig_file+'.'+fig_type
      os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
      # print('\n'+fig_file+'\n')
         

if create_png : ngl.end()
#---------------------------------------------------------------------------------------------------
# Create animated gif
#---------------------------------------------------------------------------------------------------
if create_gif :
   # cmd = "convert -repage 0x0 -delay 10 -loop 0  figs_scream/scream.animation.map.v1.*png   figs_scream/scream.animation.map.v1.gif"
   cmd = 'convert -repage 0x0 -delay 10 -loop 0  '+tfiles+' '+gif_file+'.gif'
   print('\n'+cmd+'\n')
   os.system(cmd)
   print('\n  '+gif_file+'.gif'+'\n')
