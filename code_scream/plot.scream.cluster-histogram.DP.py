import os, glob, ngl, subprocess as sp, copy, string, numpy as np, xarray as xr, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler
#-------------------------------------------------------------------------------
# name,case,case_dir,case_sub = [],[],[],[]
# clr,dsh=[],[]
# obs_flag = []
# def add_case(case_in,n=None,p=None,s=None,c='black',d=0,obs=False):
#   global name,case,case_dir,case_sub
#   tmp_name = case_in if n is None else n
#   case.append(case_in); name.append(tmp_name)
#   case_dir.append(p); case_sub.append(s)
#   clr.append(c),dsh.append(d)
#   obs_flag.append(obs)

case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
lx_list,ne_list = [],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,lx=None,ne=None):
  global name,case,case_dir,case_sub,clr,dsh,mrk
  tmp_name = '' if n is None else n
  case.append(case_in); case_name.append(tmp_name)
  case_dir.append(p); case_sub.append(s); case_grid.append(g)
  dsh.append(d) ; clr.append(c) ; mrk.append(m)
  lx_list.append(lx); ne_list.append(ne)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
lev_list = []
def add_var(var_name,file_type,n='',lev=None):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
  lev_list.append(lev)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
host = None
if os.path.exists('/global/cfs/cdirs'): host = 'nersc'
if os.path.exists('/lustre/orion'):     host = 'olcf'
#-------------------------------------------------------------------------------
if host=='nersc':
  scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
  scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'


  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',      n='DYNAMO ne44 400km',      c='red',  p=scratch_gpu,s='run')
  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acn_1',n='DYNAMO ne44 400km acn=1',c='green',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.acc_1',n='DYNAMO ne44 400km acc=1',c='blue', p=scratch_gpu,s='run')

  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L276v1.1',n='DYNAMO ne44 400km L276v1.1',c='cyan', p=scratch_gpu,s='run')
  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L256v1.1',n='DYNAMO ne44 400km L256v1.1',c='blue', p=scratch_gpu,s='run')
  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.2',n='DYNAMO ne44 400km L128v2.2',c='cyan',    p=scratch_gpu,s='run')
  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60.vgrid_L128v2.1',n='DYNAMO ne44 400km L128v2.1',c='magenta', p=scratch_gpu,s='run')

  # first_file,num_files = 3,1

  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',    n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L256v1.1',ne=44,lx=400,c='cyan',   n='RCE 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-RCE-01.ne44.len_400km.DT_60.vgrid_L276v1.1',ne=44,lx=400,c='magenta',n='RCE 400km',p=scratch_gpu,s='run')

  # first_file,num_files = 0,10


  # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60',               ne=44,lx=400,c='red',    n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
  # add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_10',               ne=44,lx=400,c='blue',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
  #add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60.vgrid_L128v2.1',ne=44,lx=400,c='green',  n='GATE-IDEAL 400km',p=scratch_gpu,s='run')
  #add_case('DPSCREAM.2024-GATE-IDEAL-01.ne44.len_400km.DT_60.vgrid_L128v2.2',ne=44,lx=400,c='blue',   n='GATE-IDEAL 400km',p=scratch_gpu,s='run')

  # first_file,num_files = 6,6
  # first_file,num_files = 20,40

  tscratch = '/pscratch/sd/b/beydoun/dp_screamxx'
  add_case('scream_dpxx_GATEIDEAL_control_no_rain_frac_larger_domain',       lx=800,c='red',  n='GATE-IDEAL 800km',              p=tscratch,s='run')
  add_case('scream_dpxx_GATEIDEAL_minimal_recipe_no_rain_frac_larger_domain',lx=800,c='blue', n='GATE-IDEAL 800km minimal micro',p=tscratch,s='run')

  # first_file,num_files = 0,1

#-------------------------------------------------------------------------------
if host=='olcf':
  scratch_frontier = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'
  # add_case('DPSCREAM.DYNAMO-TEST-01.ne44.len_400km.DT_60',       n='DYNAMO ne44 400km',p=scratch_frontier,s='run')
  # add_case('DPSCREAM.DYNAMO-TEST-01-P3FRAC.ne44.len_400km.DT_60',n='DYNAMO ne44 400km',p=scratch_frontier,s='run')

  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60',      n='RCE ne11 100km',      c='red',   p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60',      n='RCE ne22 200km',      c='green', p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60',      n='RCE ne44 400km',      c='blue',  p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne11.len_100km.DT_60.GFLUX',n='RCE ne11 100km GFLUX',c='blue',p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne22.len_200km.DT_60.GFLUX',n='RCE ne22 200km GFLUX',c='green', p=scratch_frontier,s='run')
  # add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-00.ne44.len_400km.DT_60.GFLUX',n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')
  add_case('DPSCREAM.RCE-GAUSS-FLUX-TEST-01.ne44.len_400km.DT_60.GFLUX',ne=44,lx=400,n='RCE ne44 400km GFLUX',c='blue',  p=scratch_frontier,s='run')

  # first_file,num_files = 3,2
  first_file,num_files = 0,5
#-------------------------------------------------------------------------------

# tmp_file_type = 'output.scream.2D.1hr.AVERAGE.nhours_x1'

tmp_file_type = '.scream.15min.inst.INSTANT.nmins_x15' # Hassan's runs w/ minimal micro

# add_var('precip_total_surf_mass_flux', tmp_file_type, n='precip')
add_var('precip_liq_surf_mass_flux',   tmp_file_type, n='precip')

add_var('LiqWaterPath',                tmp_file_type, n='LiqWP')
# add_var('IceWaterPath',                tmp_file_type, n='IceWP')
# add_var('VapWaterPath',                tmp_file_type, n='VapWP')

# tmp_file_type = 'output.scream.3D.1hr.AVERAGE.nhours_x1'
# add_var('T_mid', tmp_file_type, n='T_mid-bot', lev=-1)

#-------------------------------------------------------------------------------
# tmp_data_path = os.getenv('HOME')+'/Research/E3SM/pub_figs/2023_screamv1_4season/data'
tmp_data_path = 'data_tmp'

fig_file,fig_type = f'figs_scream/scream.cluster-histogram.DP','png'

recalculate = True

eps_width = 0.1

#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var)

res = hs.res_xy()
res.vpHeightF             = 0.4
res.tmYLLabelFontHeightF  = 0.015
res.tmXBLabelFontHeightF  = 0.015
res.tiXAxisFontHeightF    = 0.015
res.tiYAxisFontHeightF    = 0.015
res.xyLineThicknessF      = 5

res.xyXStyle = 'Log'

res.tiXAxisString = 'Log10(Obj Size) [km]'
res.tiYAxisString = 'Count Weighted Histogram'

lres = hs.res_xy()
lres.xyDashPattern    = 1
lres.xyLineThicknessF = 2
lres.xyLineColor      = 'black'

#---------------------------------------------------------------------------------------------------
tmp_data_path = os.getenv('HOME')+'/Research/E3SM/data_tmp'
tmp_file_prefix = 'scream.cluster-histogram.DP'
def get_tmp_file(var,case):
  return f'{tmp_data_path}/{tmp_file_prefix}.tmp.{var}.{case}.nc'
#---------------------------------------------------------------------------------------------------
# def get_size_params(root,case,param=None,platform='DPSCREAM'):
#   data_out = None
#   if param not in ['MOD_date_size_cld','MOD_date_PRECT_cld','MOD_date_y_cld','MOD_date_x_cld']:
#     raise ValueError(f'ERROR - param value {param} not supported')
#   #-----------------------------------------------------------------------------
#   file_path = ''.join([root,platform,'_RainCluster_size_',case,'.txt'])
#   MOD_date_size  = np.loadtxt(file_path,delimiter=',')
#   #-----------------------------------------------------------------------------
#   if param=='MOD_date_size_cld':
#     MOD_date_size_reshape = np.reshape(MOD_date_size,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))
#     MOD_date_size_cldonly = MOD_date_size_reshape[MOD_date_size_reshape>0]
#     data_out = MOD_date_size_cldonly
#   #-----------------------------------------------------------------------------
#   if param=='MOD_date_PRECT_cld':
#     file_path = ''.join([root,platform,'_RainCluster_PRECT_',case,'.txt'],delimiter=',')
#     MOD_date_PRECT = np.loadtxt(file_path,delimiter=',')
#     MOD_date_PRECT_cld = np.reshape(MOD_date_PRECT,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))[MOD_date_size_reshape>0]
#     data_out = MOD_date_PRECT_cld
#   #-----------------------------------------------------------------------------
#   if param=='MOD_date_y_cld':
#     file_path = ''.join([root,platform,'_RainCluster_y_',case,'.txt'],delimiter=',')
#     MOD_date_y = np.loadtxt(file_path,delimiter=',')
#     MOD_date_y_cld = np.reshape(MOD_date_y,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))[MOD_date_size_reshape>0]
#     data_out = MOD_date_y_cld
#   #-----------------------------------------------------------------------------
#   if param=='MOD_date_x_cld':
#     file_path = ''.join([root,platform,'_RainCluster_x_',case,'.txt'],delimiter=',')
#     MOD_date_x = np.loadtxt(file_path,delimiter=',')
#     MOD_date_x_cld = np.reshape(MOD_date_x,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))[MOD_date_size_reshape>0]
#     data_out = MOD_date_x_cld
#   #-----------------------------------------------------------------------------
#   return data_out
#---------------------------------------------------------------------------------------------------
# def retrieve_size_characteristics(dir_loc,platform_str,run_name):
#   MOD_date_size = np.loadtxt(''.join([dir_loc,platform_str,'_RainCluster_size_',run_name,'.txt']),delimiter=',')
#   MOD_date_PRECT = np.loadtxt(''.join([dir_loc,platform_str,'_RainCluster_PRECT_',run_name,'.txt']),delimiter=',')
#   MOD_date_y = np.loadtxt(''.join([dir_loc,platform_str,'_RainCluster_y_',run_name,'.txt']),delimiter=',')
#   MOD_date_x = np.loadtxt(''.join([dir_loc,platform_str,'_RainCluster_x_',run_name,'.txt']),delimiter=',')

#   MOD_date_size_reshape=np.reshape(MOD_date_size,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))
#   MOD_date_size_cldonly=MOD_date_size_reshape[MOD_date_size_reshape>0]

#   MOD_date_PRECT_cldonly=np.reshape(MOD_date_PRECT,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))[MOD_date_size_reshape>0]
#   MOD_date_y_cldonly=np.reshape(MOD_date_y,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))[MOD_date_size_reshape>0]
#   MOD_date_x_cldonly=np.reshape(MOD_date_x,(MOD_date_size.shape[0]*MOD_date_size.shape[1],1))[MOD_date_size_reshape>0]
#   return MOD_date_size_cldonly, MOD_date_PRECT_cldonly, MOD_date_y_cldonly, MOD_date_x_cldonly
#---------------------------------------------------------------------------------------------------
# def chord_histogram( cluster_size, scene_area ):
#   chord_len = np.sqrt( cluster_size / np.pi )
#   radius_length_bin_edges = 2**np.arange(1,10.1,0.4)
#   cnt, bin_edge_sb = np.histogram(chord_len,bins=radius_length_bin_edges)
#   bin_width = radius_length_bin_edges[1:] - radius_length_bin_edges[:-1]
#   cnt_nrm =         cnt /bin_width/scene_area
#   cnt_err = np.sqrt(cnt)/bin_width/scene_area
#   return counts_norm, counts_errors, bin_edges
#---------------------------------------------------------------------------------------------------
def get_coords(ncol,lx):
  nx = int(np.sqrt(ncol)) ; dx = lx/nx ; ne = int(nx/2)
  uxi = np.linspace(0,lx,nx+1)
  uxc = uxi[:-1] + np.diff(uxi)/2
  xc,yc = np.full(ncol,np.nan), np.full(ncol,np.nan)
  for ny in range(ne):
    for nx in range(ne):
      for nyj in range(2):
        for nxi in range(2):
          g = ny*4*ne + nx*4 + nyj*2 + nxi
          i = nx*2+nxi
          j = ny*2+nyj
          xc[g] = uxc[i]
          yc[g] = uxc[j]
  return xc,yc
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
  print(' '*2+'var: '+hc.tcolor.GREEN+var[v]+hc.tcolor.ENDC)
  #-----------------------------------------------------------------------------
  hst_list = []
  bin_list = []
  #-----------------------------------------------------------------------------
  for c in range(num_case):
    print(' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
    
    tmp_file = get_tmp_file(var[v],case[c])

    if recalculate :
      #-------------------------------------------------------------------------
      file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*{file_type_list[v]}*'
      file_list = sorted(glob.glob(file_path))
      if 'first_file' in globals(): file_list = file_list[first_file:]
      if 'num_files'  in globals(): file_list = file_list[:num_files]
      #-------------------------------------------------------------------------
      ds = xr.open_mfdataset( file_list )
      #-------------------------------------------------------------------------
      # discard first few hours
      print(' '*4+f'{hc.tclr.RED}WARNING - skipping first 12 hours{hc.tclr.END}')
      ds = ds.isel(time=slice(48,))
      #-------------------------------------------------------------------------
      data = ds[var[v]]
      #-------------------------------------------------------------------------
      if 'lev' in data.dims and lev_list[v] is not None:
         if lev_list[v]<0: data = data.isel(lev=lev_list[v])
         if lev_list[v]>0: data = data.sel(lev=lev_list[v])
      #-------------------------------------------------------------------------
      # unit conversions
      if 'precip' in var[v]: data = data*86400.*1e3
      #-------------------------------------------------------------------------
      if 'area' in ds.variables:
        area = ds['area'].isel(time=0, missing_dims='ignore')
      elif 'area_PG2' in ds.variables:
        area = ds['area_PG2'].isel(time=0, missing_dims='ignore')
      else:
        raise ValueError('area variable not found in dataset?')
      #-------------------------------------------------------------------------
      area_coeff = 1/1e6
      # area_coeff = 1/1e3
      scene_area = np.sum(area.values) * area_coeff

      num_t = len(data['time'])
      # xc,yc = get_coords( len(data['ncol']), lx=lx_list[c] )
      xc,yc = get_coords( len(data['ncol']), lx=lx_list[c] )
      #-------------------------------------------------------------------------
      # Calculate mean and std deviation of spatial coords to normalize the data
      xc_mean,xc_std = np.mean(xc), np.std(xc)
      yc_mean,yc_std = np.mean(yc), np.std(yc)
      #-------------------------------------------------------------------------
      max_cluster_cnt = 8000
      cluster_sz = np.zeros([num_t,max_cluster_cnt]) # size
      cluster_yc = np.zeros([num_t,max_cluster_cnt]) # latitude
      cluster_xc = np.zeros([num_t,max_cluster_cnt]) # longitude
      cluster_av = np.zeros([num_t,max_cluster_cnt]) # cluster mean data value
      #-------------------------------------------------------------------------
      for t in np.arange(num_t):
        data_tmp = data.isel(time=t)
        data_tmp.load()
        #-----------------------------------------------------------------------
        # hc.print_stat(data,name=var[v],compact=True,indent=' '*6)
        # exit()
        #-----------------------------------------------------------------------
        # unit conversions
        threshold_mode,threshold_val = None,None
        # if 'precip' in var[v]    : threshold_mode='min'; threshold_val = 10
        if 'precip' in var[v]    : threshold_mode='min'; threshold_val = 5
        if var[v]=='LiqWaterPath': threshold_mode='min'; threshold_val = 0.5
        if var[v]=='IceWaterPath': threshold_mode='min'; threshold_val = 0.5
        # if var[v]=='VapWaterPath': threshold_mode='min'; threshold_val = ?
        if var[v]=='T_mid':        threshold_mode='max'; threshold_val = 298
        if threshold_mode is None: raise ValueError(f'threshold_mode not defined for variable: {var[v]}')
        if threshold_val  is None: raise ValueError(f'threshold_val not defined for variable: {var[v]}')
        #-----------------------------------------------------------------------
        # Create mask
        mask = np.zeros(data_tmp.shape)
        if threshold_mode=='min': mask[ data_tmp.values > threshold_val ] = 1
        if threshold_mode=='max': mask[ data_tmp.values < threshold_val ] = 1
        #-----------------------------------------------------------------------
        # skip cases with fewer than 4 grid boxes above threshold
        if np.sum(mask)<4: continue
        #-----------------------------------------------------------------------
        xc_tmp = ( xc - xc_mean ) / xc_std
        yc_tmp = ( yc - yc_mean ) / yc_std
        xc_tmp_masked = xc_tmp[ mask==1 ]
        yc_tmp_masked = yc_tmp[ mask==1 ]
        #-----------------------------------------------------------------------
        # Create array with location info
        coords_masked = np.zeros([len(xc_tmp_masked),2])
        coords_masked[:,0] = xc_tmp_masked
        coords_masked[:,1] = yc_tmp_masked
        #-----------------------------------------------------------------------
        # Clustering step using DBSCAN
        db = DBSCAN(eps=eps_width, min_samples=2).fit(coords_masked)     # eps is a tuning parameter that is sensitive to the spacing
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[ db.core_sample_indices_ ] = True
        labels = db.labels_
        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_    = list(labels).count(-1)
        #-----------------------------------------------------------------------
        # print(' '*4+f't: {t:04d}  # clusters: {n_clusters_}')
        #-----------------------------------------------------------------------
        data_masked = data_tmp[mask==1]
        area_masked = area[mask==1]
        for k in set(labels):
          class_member_mask = (labels == k)
          if k == -1:
            cluster_sz[t,k] = 0.
            cluster_yc[t,k] = 0.
            cluster_xc[t,k] = 0.
            cluster_av[t,k] = 0.
          else:
            area_label = np.array(area_masked[class_member_mask])
            data_label = np.array(data_masked[class_member_mask])
            yc_label   = np.array(yc_tmp_masked[class_member_mask])
            xc_label   = np.array(xc_tmp_masked[class_member_mask])

            # Save values for each cloud object and area weight
            cluster_sz[t,k] = np.sum(area_label)*area_coeff
            cluster_yc[t,k] = np.sum(area_label*yc_label)  /np.sum(area_label)
            cluster_xc[t,k] = np.sum(area_label*xc_label)  /np.sum(area_label)
            cluster_av[t,k] = np.sum(area_label*data_label)/np.sum(area_label)
        #-----------------------------------------------------------------------

      # np.savetxt(''.join([dir_loc,'DPSCREAM_RainCluster_sz_',run_name,'.txt']),Feb16_size,delimiter=',')
      # np.savetxt(''.join([dir_loc,'DPSCREAM_RainCluster_yc_',run_name,'.txt']),Feb16_lat,delimiter=',')
      # np.savetxt(''.join([dir_loc,'DPSCREAM_RainCluster_xc_',run_name,'.txt']),Feb16_lon,delimiter=',')
      # np.savetxt(''.join([dir_loc,'DPSCREAM_RainCluster_av_',run_name,'.txt']),Feb16_PRECT,delimiter=',')
      #-------------------------------------------------------------------------
      # MOD_date_sz = retrieve_size_characteristics(dir_loc,run_name,param='MOD_date_size_cld')
      # MOD_date_av = retrieve_size_characteristics(dir_loc,run_name,param='MOD_date_PRECT_cld')
      # MOD_date_yc = retrieve_size_characteristics(dir_loc,run_name,param='MOD_date_y_cld')
      # MOD_date_xc = retrieve_size_characteristics(dir_loc,run_name,param='MOD_date_x_cld')
      #-------------------------------------------------------------------------
      # cnt_nrm, cnt_err, bin_edges = chord_histogram( cluster_sz, scene_area )
      #-------------------------------------------------------------------------
      chord_len = np.sqrt( cluster_sz / np.pi )
      bin_edges = 2**np.arange(1,10.1,0.4)
      bin_ctr   = (bin_edges[1:]*bin_edges[:-1])**0.5
      cnt, bin_edge_sb = np.histogram(chord_len,bins=bin_edges)
      bin_width = bin_edges[1:] - bin_edges[:-1]

      cnt = cnt / np.sum(cnt)

      cnt_nrm =         cnt /bin_width/scene_area
      cnt_err = np.sqrt(cnt)/bin_width/scene_area
      #-------------------------------------------------------------------------
      hst_list.append(cnt_nrm * 1e5)
      bin_list.append(bin_ctr)
      #-------------------------------------------------------------------------

    #    # Write to file
    #    print(' '*6+f'Writing data to file: {tmp_file}')
    #    gbl_mean.name = tvar
    #    tmp_ds = xr.Dataset()
    #    # tmp_ds['time'] = time
    #    tmp_ds[tvar]   = gbl_mean
    #    tmp_ds.to_netcdf(path=tmp_file,mode='w')
    # else:
    #    print(' '*6+f'Reading pre-calculated data from file: {hc.tcolor.MAGENTA}{tmp_file}{hc.tcolor.ENDC}')
    #    tmp_ds = xr.open_dataset( tmp_file )
    #    # time_tmp = tmp_ds['time'].values
    #    gbl_mean = tmp_ds[tvar]
  #-----------------------------------------------------------------------------
  hst_min = np.min([np.min(x) for x in hst_list])
  hst_max = np.max([np.max(x) for x in hst_list])
  bin_min = np.min([np.min(x) for x in bin_list])
  bin_max = np.max([np.max(x) for x in bin_list])

  res.trYMinF = hst_min
  res.trYMaxF = hst_max
  res.trXMinF = bin_min
  res.trXMaxF = 1e2#bin_max

  #-----------------------------------------------------------------------------
  for c in range(num_case):
    tres = copy.deepcopy(res)
    tres.xyLineColor   = clr[c]
    tres.xyDashPattern = dsh[c]
    
    tplot = ngl.xy(wks, bin_list[c], hst_list[c], tres)

    if c==0: plot[v] = tplot
    if c>0 : ngl.overlay(plot[v],tplot)
  #-----------------------------------------------------------------------------
  hs.set_subtitles(wks, plot[v], '', var_str[v], '', font_height=0.015)
#-------------------------------------------------------------------------------
ngl.panel(wks,plot,[1,num_var],hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#-------------------------------------------------------------------------------
