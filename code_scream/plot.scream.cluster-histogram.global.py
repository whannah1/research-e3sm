import os, glob, ngl, subprocess as sp, copy, string, numpy as np, xarray as xr, cmocean
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.datasets import make_blobs
from sklearn.preprocessing import StandardScaler
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
htype_alt_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False,htype_alt=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
   htype_alt_list.append(htype_alt)
#-------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
lev_list = []
def add_var(var_name,file_type,n='',lev=None):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
  lev_list.append(lev)
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
host = None
if os.path.exists('/global/cfs/cdirs'): host = 'nersc'
if os.path.exists('/lustre/orion'):     host = 'olcf'
#-------------------------------------------------------------------------------
if host=='nersc':
  scratch_cpu = '/pscratch/sd/w/whannah/scream_scratch/pm-cpu'
  scratch_gpu = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'

#-------------------------------------------------------------------------------
if host=='olcf':
  # tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'
  tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
    
  # scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne256pg2_scrip.nc'
  scrip_file_path = '/lustre/orion/cli115/proj-shared/hannah6/files_grid/1000x2000_scrip.nc'

  recipe14 = 'cfr_1.cfi_0.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_1'
  recipe15 = 'cfr_1.cfi_0.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_0.9'
  recipe17 = 'cfr_1.cfi_0.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_0.95'

  tmp_sub = 'remap_1000x2000'

  add_case(f'SCREAM.2025-PC-01.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1',     n='SCREAM cfrc',d=0,c='black',   p=tmp_scratch,s=tmp_sub)
  add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe14}',n='SCREAM R14', d=0,c='red',     p=tmp_scratch,s=tmp_sub,htype_alt=False)
  add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe15}',n='SCREAM R15', d=0,c='cyan',    p=tmp_scratch,s=tmp_sub,htype_alt=False)
  add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe17}',n='SCREAM R17', d=0,c='magenta', p=tmp_scratch,s=tmp_sub,htype_alt=False)

  # first_file,num_files = 3,2
  first_file,num_files = 30,10
#-------------------------------------------------------------------------------

# htype     = 'output.scream.2D.1hr.inst.INSTANT.nhours_x1'
# htype_alt = 'output.scream.2D.1hr.ne256pg2.INSTANT.nhours_x1'
htype = 'output.scream.2D.1hr.ne256pg2.INSTANT.nhours_x1'

add_var('precip_total_surf_mass_flux', htype, n='precip')
# add_var('precip_liq_surf_mass_flux',   htype, n='precip')
# add_var('LiqWaterPath',                htype, n='LiqWP')
# add_var('IceWaterPath',                htype, n='IceWP')
# add_var('VapWaterPath',                htype, n='VapWP')

#-------------------------------------------------------------------------------
# tmp_data_path = os.getenv('HOME')+'/Research/E3SM/pub_figs/2023_screamv1_4season/data'
tmp_data_path = 'data_tmp'

fig_file,fig_type = f'figs_scream/scream.cluster-histogram.global','png'

recalculate = False

eps_width = 0.1

#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var)

res = hs.res_xy()
res.vpHeightF             = 0.4
res.tmYLLabelFontHeightF  = 0.015
res.tmXBLabelFontHeightF  = 0.015
res.tiXAxisFontHeightF    = 0.015
res.tiYAxisFontHeightF    = 0.015
res.xyLineThicknessF      = 5

res.xyXStyle = 'Log'

res.tiXAxisString = 'Log10(Obj Size) [km]'
res.tiYAxisString = 'Count Weighted Histogram'

lres = hs.res_xy()
lres.xyDashPattern    = 1
lres.xyLineThicknessF = 2
lres.xyLineColor      = 'black'

#---------------------------------------------------------------------------------------------------
tmp_data_path = os.getenv('HOME')+'/Research/E3SM/data_tmp'
tmp_file_prefix = 'scream.cluster-histogram.global'
def get_tmp_file(var,case):
  return f'{tmp_data_path}/{tmp_file_prefix}.tmp.{var}.{case}.nc'
#---------------------------------------------------------------------------------------------------

scrip_ds = xr.open_dataset(scrip_file_path)

scrip_ds_center_lon = scrip_ds['grid_center_lon']
scrip_ds_center_lat = scrip_ds['grid_center_lat']
scrip_ds_corner_lon = scrip_ds['grid_corner_lon']
scrip_ds_corner_lat = scrip_ds['grid_corner_lat']
# scrip_ds_area       = scrip_ds['grid_area']

use_mask = True
# lat1,lat2 = -40,40

# if use_mask:

#   lat_name,lon_name = 'grid_center_lat','grid_center_lon'
#   tmp_data = np.ones(len(scrip_ds[lat_name]),dtype=bool)
#   grid_mask = xr.DataArray( tmp_data, coords=scrip_ds[lat_name].coords )
#   if 'lat1' in locals(): grid_mask = grid_mask & (scrip_ds[lat_name]>=lat1) & (scrip_ds[lat_name]<=lat2)
#   if 'lon1' in locals(): grid_mask = grid_mask & (scrip_ds[lon_name]>=lon1) & (scrip_ds[lon_name]<=lon2)

#   scrip_ds_center_lon = scrip_ds_center_lon.where(grid_mask,drop=True)
#   scrip_ds_center_lat = scrip_ds_center_lat.where(grid_mask,drop=True)
#   scrip_ds_corner_lon = scrip_ds_corner_lon.where(grid_mask,drop=True)
#   scrip_ds_corner_lat = scrip_ds_corner_lat.where(grid_mask,drop=True)
#   # scrip_ds_area       = scrip_ds_area      .where(grid_mask,drop=True)

#   grid_mask = grid_mask.rename({'grid_size':'ncol'})


  
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
  print(' '*2+'var: '+hc.tcolor.GREEN+var[v]+hc.tcolor.ENDC)
  #-----------------------------------------------------------------------------
  hst_list = []
  bin_list = []
  #-----------------------------------------------------------------------------
  for c in range(num_case):
    print(' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
    
    tmp_file = get_tmp_file(var[v],case[c])

    if recalculate :
      #-------------------------------------------------------------------------
      htype_tmp = htype
      if htype_alt_list[c]: htype_tmp = htype_alt

      file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*{htype_tmp}*'
      file_list = sorted(glob.glob(file_path))

      if 'first_file' in globals(): file_list = file_list[first_file:]
      if 'num_files'  in globals(): file_list = file_list[:num_files]
      #-------------------------------------------------------------------------
      ds = xr.open_mfdataset( file_list )
      #-------------------------------------------------------------------------
      # # discard first few hours
      # print(' '*4+f'{hc.tclr.RED}WARNING - skipping first 12 hours{hc.tclr.END}')
      # ds = ds.isel(time=slice(48,))
      #-------------------------------------------------------------------------
      data = ds[var[v]]
      area = ds['area']

      if 'time' in area.dims: area = area.isel(time=0)
      #-------------------------------------------------------------------------
      lat1,lat2 = -40,40
      data = data.where((data.lat>=lat1) & (data.lat<=lat2),drop=True)
      area = area.where((area.lat>=lat1) & (area.lat<=lat2),drop=True)
      #-------------------------------------------------------------------------
      # if 'lev' in data.dims and lev_list[v] is not None:
      #    if lev_list[v]<0: data = data.isel(lev=lev_list[v])
      #    if lev_list[v]>0: data = data.sel(lev=lev_list[v])
      #-------------------------------------------------------------------------
      # unit conversions
      if 'precip' in var[v]: data = data*86400.*1e3
      #-------------------------------------------------------------------------
      # area_coeff = 1.
      area_coeff = 6.371e6
      # area_coeff = 1/1e6
      # area_coeff = 1/1e3

      scene_area = np.sum(area.values) * area_coeff

      num_t = len(data['time'])
      # xc,yc = get_coords( len(data['ncol']), lx=lx_list[c] )
      # xc,yc = get_coords( len(data['ncol']), lx=lx_list[c] )
      # xc,yc = scrip_ds_center_lon, scrip_ds_center_lat

      xc = np.transpose( np.repeat( data['lon'].values[...,None],len(data['lat']),axis=1) )
      yc =               np.repeat( data['lat'].values[...,None],len(data['lon']),axis=1)
      #-------------------------------------------------------------------------
      # xc = xc.where((yc>=lat1) & (yc<=lat2),drop=True)
      # yc = yc.where((yc>=lat1) & (yc<=lat2),drop=True)
      #-------------------------------------------------------------------------
      # Calculate mean and std deviation of spatial coords to normalize the data
      xc_mean,xc_std = np.mean(xc), np.std(xc)
      yc_mean,yc_std = np.mean(yc), np.std(yc)
      #-------------------------------------------------------------------------
      max_cluster_cnt = 8000
      cluster_sz = np.zeros([num_t,max_cluster_cnt]) # size
      cluster_yc = np.zeros([num_t,max_cluster_cnt]) # latitude
      cluster_xc = np.zeros([num_t,max_cluster_cnt]) # longitude
      cluster_av = np.zeros([num_t,max_cluster_cnt]) # cluster mean data value
      #-------------------------------------------------------------------------
      # t = 0
      # print(hc.tcolor.RED+'WARNING - only using t=0'+hc.tcolor.ENDC)
      # if True:
      for t in np.arange(num_t):
        data_tmp = data.isel(time=t)
        data_tmp.load()
        #-----------------------------------------------------------------------
        # hc.print_stat(data,name=var[v],compact=True,indent=' '*6)
        # exit()
        #-----------------------------------------------------------------------
        # unit conversions
        threshold_mode,threshold_val = None,None
        # if 'precip' in var[v]    : threshold_mode='min'; threshold_val = 10
        if 'precip' in var[v]    : threshold_mode='min'; threshold_val = 1
        # if 'precip' in var[v]    : threshold_mode='min'; threshold_val = 5
        if var[v]=='LiqWaterPath': threshold_mode='min'; threshold_val = 0.5
        if var[v]=='IceWaterPath': threshold_mode='min'; threshold_val = 0.5
        # if var[v]=='VapWaterPath': threshold_mode='min'; threshold_val = ?
        if var[v]=='T_mid':        threshold_mode='max'; threshold_val = 298
        if threshold_mode is None: raise ValueError(f'threshold_mode not defined for variable: {var[v]}')
        if threshold_val  is None: raise ValueError(f'threshold_val not defined for variable: {var[v]}')
        #-----------------------------------------------------------------------
        # Create mask
        mask = np.zeros(data_tmp.shape)
        if threshold_mode=='min': mask[ data_tmp.values > threshold_val ] = 1
        if threshold_mode=='max': mask[ data_tmp.values < threshold_val ] = 1
        #-----------------------------------------------------------------------
        # skip cases with fewer than 4 grid boxes above threshold
        if np.sum(mask)<4: continue
        #-----------------------------------------------------------------------
        xc_tmp = ( xc - xc_mean ) / xc_std
        yc_tmp = ( yc - yc_mean ) / yc_std
        xc_tmp_masked = xc_tmp[ mask==1 ]
        yc_tmp_masked = yc_tmp[ mask==1 ]
        #-----------------------------------------------------------------------
        # Create array with location info
        coords_masked = np.zeros([len(xc_tmp_masked),2])
        coords_masked[:,0] = xc_tmp_masked
        coords_masked[:,1] = yc_tmp_masked
        #-----------------------------------------------------------------------
        # Clustering step using DBSCAN
        db = DBSCAN(eps=eps_width, min_samples=2).fit(coords_masked)     # eps is a tuning parameter that is sensitive to the spacing
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[ db.core_sample_indices_ ] = True
        labels = db.labels_
        # Number of clusters in labels, ignoring noise if present.
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise_    = list(labels).count(-1)
        #-----------------------------------------------------------------------
        # print()
        print(' '*4+f't: {t:04d}  # clusters: {n_clusters_}')
        # print()
        #-----------------------------------------------------------------------
        data_masked = data_tmp.values[mask==1]
        area_masked = area.values[mask==1]
        for k in set(labels):
          class_member_mask = (labels == k)
          if k == -1:
            cluster_sz[t,k] = 0.
            cluster_yc[t,k] = 0.
            cluster_xc[t,k] = 0.
            cluster_av[t,k] = 0.
          else:
            area_label = np.array(area_masked[class_member_mask])
            data_label = np.array(data_masked[class_member_mask])
            yc_label   = np.array(yc_tmp_masked[class_member_mask])
            xc_label   = np.array(xc_tmp_masked[class_member_mask])

            # Save values for each cloud object and area weight
            cluster_sz[t,k] = np.sum(area_label)*area_coeff
            cluster_yc[t,k] = np.sum(area_label*yc_label)  /np.sum(area_label)
            cluster_xc[t,k] = np.sum(area_label*xc_label)  /np.sum(area_label)
            cluster_av[t,k] = np.sum(area_label*data_label)/np.sum(area_label)
        
        # hc.print_stat(cluster_sz)
      #-------------------------------------------------------------------------
      # cnt_nrm, cnt_err, bin_edges = chord_histogram( cluster_sz, scene_area )
      #-------------------------------------------------------------------------
      chord_len = np.sqrt( cluster_sz / np.pi )
      bin_edges = 2**np.arange(1,10.1,0.4)
      bin_ctr   = (bin_edges[1:]*bin_edges[:-1])**0.5
      cnt, bin_edge_sb = np.histogram(chord_len,bins=bin_edges)
      bin_width = bin_edges[1:] - bin_edges[:-1]

      cnt = cnt / np.sum(cnt)

      cnt_nrm =         cnt /bin_width/scene_area
      cnt_err = np.sqrt(cnt)/bin_width/scene_area
      
      #-------------------------------------------------------------------------
      # Write to file
      print(' '*6+f'Writing data to file: {tmp_file}')
      tmp_ds = xr.Dataset()
      tmp_ds['cnt_nrm'] = cnt_nrm
      tmp_ds['cnt_err'] = cnt_err
      tmp_ds['bin_ctr'] = bin_ctr
      tmp_ds.to_netcdf(path=tmp_file,mode='w')
    else:
      print(' '*6+f'Reading pre-calculated data from file: {hc.tcolor.MAGENTA}{tmp_file}{hc.tcolor.ENDC}')
      tmp_ds = xr.open_dataset( tmp_file )
      cnt_nrm = tmp_ds['cnt_nrm']
      cnt_err = tmp_ds['cnt_err']
      bin_ctr = tmp_ds['bin_ctr']
    #-------------------------------------------------------------------------
    hst_list.append(cnt_nrm * 1e5)
    bin_list.append(bin_ctr)
  #-----------------------------------------------------------------------------
  hst_min = np.min([np.min(x) for x in hst_list])
  hst_max = np.max([np.max(x) for x in hst_list])
  bin_min = np.min([np.min(x) for x in bin_list])
  bin_max = np.max([np.max(x) for x in bin_list])

  res.trYMinF = hst_min
  res.trYMaxF = hst_max
  res.trXMinF = bin_min
  res.trXMaxF = 1e2#bin_max

  #-----------------------------------------------------------------------------
  for c in range(num_case):
    tres = copy.deepcopy(res)
    tres.xyLineColor   = clr[c]
    tres.xyDashPattern = dsh[c]
    
    tplot = ngl.xy(wks, bin_list[c], hst_list[c], tres)

    if c==0: plot[v] = tplot
    if c>0 : ngl.overlay(plot[v],tplot)
  #-----------------------------------------------------------------------------
  hs.set_subtitles(wks, plot[v], '', var_str[v], '', font_height=0.015)
#-------------------------------------------------------------------------------
ngl.panel(wks,plot,[1,num_var],hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#-------------------------------------------------------------------------------
