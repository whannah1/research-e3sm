import os
import glob
import ngl
import copy
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs

### case name for plot title
case_name = 'F2010-SCREAMv1-noAero ne1024pg2'

### full path of input file
# /gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220908_low_t_mid.bca00ce9f65460369cb679fdd144897aba51a428/run/*scream.surf*

# case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220908_low_t_mid.bca00ce9f65460369cb679fdd144897aba51a428'
# idir = f'/gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/{case}/run'

case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220912_low_t_mid.853fd32bc55a3bab87181f9118cb9d26ac6f831e'
idir = f'/gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/{case}/run'


input_file_name_srf = f'{idir}/*scream.surf*'
input_file_name_avg = f'{idir}/*scream.hi.AVERAGE.*'
input_file_name_min = f'{idir}/*scream.hi.MIN.*'

scrip_file = '/gpfs/alpine/cli115/proj-shared/hannah6/grid_files/ne1024pg2_scrip.nc'


### list of variables to plot
var = ['T_mid']


### output figure type and name
fig_type = 'png'
fig_file = 'figs_scream/debug.histogram.v1'

recalculate = False

time1,time2,dtime = 0,8,1

tmp_file_head = 'scream_Tmid_histogram'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]

res = hs.res_contour_fill()
# res.vpHeightF = 0.1
# res.tiXAxisString = 'Time Step'
# res.lbLabelFontHeightF           = 0.012

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# open the dataset

scrip_ds = xr.open_dataset(scrip_file)
lat = scrip_ds['grid_center_lat'].values
lon = scrip_ds['grid_center_lon'].values

file_list_srf = sorted(glob.glob(input_file_name_srf))
file_list_avg = sorted(glob.glob(input_file_name_avg))
file_list_min = sorted(glob.glob(input_file_name_min))

# remove last file (typically empty)
file_list_srf.remove(file_list_srf[-1])
file_list_avg.remove(file_list_avg[-1])
file_list_min.remove(file_list_min[-1])

# ds_srf = xr.open_mfdataset( file_list_srf, combine='by_coords', concat_dim='time', use_cftime=True )
# ds_avg = xr.open_mfdataset( file_list_avg, combine='by_coords', concat_dim='time', use_cftime=True )
ds_min = xr.open_mfdataset( file_list_min, combine='by_coords', concat_dim='time', use_cftime=True )

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

time_list = list(np.arange(time1,time2+dtime,dtime))
ntime = len(time_list)

bin_min,bin_max,bin_spc = 140,200,5
nbin = np.round( ( bin_max - bin_min + bin_spc )/bin_spc ).astype(int)
bins = np.linspace(bin_min,bin_max,nbin)

time_coord = ('time',ds_min['time'].isel(time=time_list))
bin_coord  = ('bin', xr.DataArray(bins))
bin_cnt = xr.DataArray( np.zeros([ntime,nbin]), coords=[time_coord,bin_coord])

print()
print(f'  ntime: {ntime}')
print(f'  nbin : {nbin}')
print()

for t,tt in enumerate(time_list):
   tmp_file = f'{tmp_file_head}.t_{t}.nc'
   print(f'  t: {t}   tmp_file: {tmp_file}')
   if recalculate:
      T_mid = ds_min['T_mid'].isel(lev=-1,time=tt)
      for b in range(nbin):
         print(f'    b: {b}')
         bin_bot = bin_min - bin_spc/2. + bin_spc*(b  )
         bin_top = bin_min - bin_spc/2. + bin_spc*(b+1)
         bin_cnt[t,b] = np.sum( ( T_mid>=bin_bot ) & ( T_mid <bin_top ) )
      print(f'    writing to file: {tmp_file}')
      tmp_ds = xr.Dataset()
      tmp_ds['bin_cnt'] = bin_cnt[t,:]
      tmp_ds.to_netcdf(path=tmp_file,mode='w')
      print()
   else:
      tmp_ds = xr.open_dataset( tmp_file, use_cftime=True  )
      bin_cnt[t,:] = tmp_ds['bin_cnt']

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

time  = bin_cnt['time'].values
bins  = bin_cnt['bin'].values

# time = ( time - time[0] ).astype('float').values / 86400e9
time = np.arange(0,len(time),1)

ntime = len(bin_cnt['time'])
nbin  = len(bin_cnt['bin'])

# for t in range(ntime):
#    print()
#    for b in range(nbin):
#       print(f'  {bins[b]:8.0f}   {bin_cnt[t,b]:8.0f}')

print()
print(bin_cnt)
print()
# print(time)
# print()

res.cnLevelSelectionMode = 'ExplicitLevels'
res.cnLevels = np.arange(0,np.max(bin_cnt.values)+1,1)

res.sfXArray = time
res.sfYArray = bins

res.cnFillMode = 'RasterFill'

# xcenter = time
# ycenter = bins
# dx = dtime
# dy = bin_spc
# xcorner = np.zeros([len(ncol),4])
# ycorner = np.zeros([len(ncol),4])
# for i in range(len(ncol)):
#    xcorner[i,0] = xcenter[i] - dx/2.
#    xcorner[i,1] = xcenter[i] + dx/2.
#    xcorner[i,2] = xcenter[i] + dx/2.
#    xcorner[i,3] = xcenter[i] - dx/2.
#    ycorner[i,0] = ycenter[i] + dy/2.
#    ycorner[i,1] = ycenter[i] + dy/2.
#    ycorner[i,2] = ycenter[i] - dy/2.
#    ycorner[i,3] = ycenter[i] - dy/2.
# res.cnFillMode    = 'CellFill'
# res.sfXArray      = xcenter
# res.sfYArray      = ycenter
# res.sfXCellBounds = xcorner
# res.sfYCellBounds = ycorner

plot[0] = ngl.contour(wks, bin_cnt.transpose(), res)

# exit()


#-------------------------------------------------------------------------------
# Finalize plot
#-------------------------------------------------------------------------------
pres = ngl.Resources()
# pres.nglPanelYWhiteSpacePercent = 30
# pres.nglPanelXWhiteSpacePercent = 5

layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

### trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
