import os, glob, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs

### case name for plot title
# case_name = 'F2010-SCREAMv1-noAero ne1024pg2'

### full path of input file
# /gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220908_low_t_mid.bca00ce9f65460369cb679fdd144897aba51a428/run/*scream.surf*
# case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220908_low_t_mid.bca00ce9f65460369cb679fdd144897aba51a428'
# case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1.20221014_production_run.27604ccf3f1aaa88ea3413b774ef3817cad7343a'; case_name = 'ne1024pg2.F2010-SCREAMv1.20221014'
case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1.20221021_dt50_testrun.feabca3534890046c3b68a0b6d529cf71353a94c'; case_name = 'ne1024.F2010-SCREAMv1.20221021_dt50'
idir = f'/gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/{case}/run'
input_file_name_srf = f'{idir}/output.scream.SurfVars.*'
#input_file_name_srf = f'{idir}/output.scream.Temp_2m.MIN.*'
input_file_name_int = f'{idir}/output.scream.VertIntegrals.*'

# input_file_name_avg = f'{idir}/*scream.hi.AVERAGE.*'
# input_file_name_min = f'{idir}/*scream.hi.MIN.*'

scrip_file = '/gpfs/alpine/cli115/proj-shared/hannah6/grid_files/ne1024pg2_scrip.nc'


### list of variables to plot
# var = ['ps','surf_evap','surf_sens_flux','surf_mom_flux','T_mid']
# var = ['surf_sens_flux','surf_evap']
# var = ['wind_speed_10m','surf_mom_flux']
# var = ['T_2m','wind_speed_10m','surf_sens_flux','surf_evap']
# var = ['precip_liq_surf_mass','precip_ice_surf_mass']
# var = ['T_2m','qv@bot']
# var = ['T_2m','qv@bot','precip_liq_surf_mass','precip_ice_surf_mass']
# var = ['SW_flux_dn@bot','LW_flux_dn@bot']
# var = ['surf_radiative_T','VerticalLayerMidpoint@bot']

# var = ['VapWaterPath','ZonalVapFlux','MeridionalVapFlux']
# var = ['LiqWaterPath','IceWaterPath','RainWaterPath','RimeWaterPath']
# var = ['T_2m','VapWaterPath','ZonalVapFlux','MeridionalVapFlux','LiqWaterPath','RainWaterPath']
var = ['T_2m']


   # float precip_liq_surf_mass(time, ncol) ;
   # float precip_ice_surf_mass(time, ncol) ;
   # float surf_evap(time, ncol) ;
   # float surf_sens_flux(time, ncol) ;
   # float ps(time, ncol) ;
   # float surf_mom_flux(time, ncol, dim2) ;
   # float T_mid@bot(time, ncol) ;
   # float T_mid@lev_126(time, ncol) ;
   # float qv@bot(time, ncol) ;
   # float horiz_winds@bot(time, ncol, dim2) ;
   # float SW_flux_up@bot(time, ncol) ;
   # float SW_flux_dn@bot(time, ncol) ;
   # float LW_flux_up@bot(time, ncol) ;
   # float LW_flux_dn@bot(time, ncol) ;
   # float VerticalLayerMidpoint@bot(time, ncol) ;
   # float surf_radiative_T(time, ncol) ;
   # float T_2m(time, ncol) ;
   # float wind_speed_10m(time, ncol) ;

### single time index to load (no averaging)
# time_slice = 40

### output figure type and name
fig_type = 'png'
fig_file = 'figs_scream/debug.timeseries.v1'#+'.'+str(time_slice).zfill(3)

### regional subset
# lat1,lat2,lon1,lon2 = -70,-60,360-90,360-30
# lat1,lat2 = -90,-60

# day1,day2 = 30,32
day1,day2 = 0,1

X_axis_opt = 'days' # days / steps

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_var

res = hs.res_default()   
res.vpHeightF = 0.1


if X_axis_opt=='days' : res.tiXAxisString = 'Time [days]'
if X_axis_opt=='steps': res.tiXAxisString = 'Time Step'
# res.trXMinF = 400
# res.trXMaxF = 500

# cres = copy.deepcopy(res)

# res.vpWidthF = 0.4
res.xyLineThicknessF             = 6.

# cres.lbLabelFontHeightF           = 0.012
# cres.cnFillOn                     = True
# cres.cnLinesOn                    = False
# cres.cnLineLabelsOn               = False
# cres.cnInfoLabelOn                = False
# cres.lbLabelFontHeightF           = 0.008
# # cres.lbOrientation                = "Horizontal"

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# open the dataset

scrip_ds = xr.open_dataset(scrip_file)
lat = scrip_ds['grid_center_lat'].values
lon = scrip_ds['grid_center_lon'].values

def get_file_list(search_name):
   file_list_tmp = sorted(glob.glob(search_name))
   file_list_tmp.remove(file_list_tmp[-1])
   first_files_tmp = file_list_tmp[:2]
   fpd = 1
   if 'VertIntegrals' in search_name: fpd = 1
   if 'SurfVars'      in search_name: fpd = 24 # surface files are output every 60 min (40 days total as of 10-17-2022)
   if 'day2' in globals(): file_list_tmp = file_list_tmp[          :(day2*fpd)]
   if 'day1' in globals(): file_list_tmp = file_list_tmp[(day1*fpd):          ]
   return file_list_tmp, first_files_tmp

file_list_srf, first_files_srf = get_file_list(input_file_name_srf)
file_list_int, first_files_int = get_file_list(input_file_name_int)

print(); print(file_list_srf)
# exit()
# print('DEBUG 1')
ds_srf = xr.open_mfdataset( file_list_srf, combine='by_coords', concat_dim='time', use_cftime=True )
# print(); print(ds_srf); print()
#exit()
# print('DEBUG 2')
ds_int = xr.open_mfdataset( file_list_int, combine='by_coords', concat_dim='time', use_cftime=True )
# print('DEBUG 3')

# ntime = len(ds_srf['time'].values)

# print('creating mask...')
# ncol = ds['ncol']
# mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
# if 'lat1' in vars(): mask = mask & (ds['lat']>=lat1)
# if 'lat2' in vars(): mask = mask & (ds['lat']<=lat2)
# if 'lon1' in vars(): mask = mask & (ds['lon']>=lon1)
# if 'lon2' in vars(): mask = mask & (ds['lon']<=lon2)

#-------------------------------------------------------------------------------
# First find a column to plot
#-------------------------------------------------------------------------------

# ivar='T_2m'; t1=-20 ; icol = 18958763
# ivar='T_2m'; t1=-200 ; icol = 13061042
# ivar='T_2m'; t1=-600 ; icol = 18299004
# ivar='T_2m'; t1,t2=-1200,-600 ; icol = 19223697
ivar='T_2m'; t1,t2=0,-250 # ; icol =  # 20221021 case
# ivar='T_2m'; t1,t2=-2000,-1200 #; icol = 


if 'icol' not in locals():
   if 't2' in locals():
      #print(); print(ds_srf); print()
      tmp_idx = ds_srf[ivar][t1:t2,:].argmin().values
   else:
      tmp_idx = ds_srf[ivar][t1:,:].argmin().values
   (itime,icol) = np.unravel_index( tmp_idx, ds_srf[ivar][t1:,:].shape )


msg  = f'   icol: {icol}'
msg += f'   lat: {lat[icol]:6.2f}'
msg += f'   lon: {lon[icol]:6.2f}'
if 'itime' in locals():
   msg += f'   itime: {itime}'
   msg += f'   min({ivar}): {ds_srf[ivar][itime,icol].values}'
print(f'\n{msg}\n')

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

for v in range(num_var):
   print('\n  var: '+var[v])

   srf_flag,int_flag = False,False
   if var[v] in ds_srf.variables: srf_flag = True
   if var[v] in ds_int.variables: int_flag = True
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   if srf_flag: data = ds_srf[var[v]].isel(ncol=icol)
   if int_flag: data = ds_int[var[v]].isel(ncol=icol)

   if X_axis_opt=='days' :
      time = data['time'] 
      if srf_flag: ds_first = xr.open_mfdataset( first_files_srf, combine='by_coords', concat_dim='time', use_cftime=True )
      if int_flag: ds_first = xr.open_mfdataset( first_files_int, combine='by_coords', concat_dim='time', use_cftime=True )
      time = ( time - ds_first['time'][0] ).astype('float').values / 86400e9
   if X_axis_opt=='steps':
      time = np.arange(0,len(time)+1,1)

   # if 'lev' in data.dims : 
   #    data = data.isel(lev=slice(100,128))
   #    lev = data['lev']


   #***********************************
   # output file for Oksana
   #***********************************
   # if v==0: ds_out = xr.Dataset()
   # ds_out[var[v]] = data
   # print(); print(ds_out)
   # if v==num_var-1: 
   #    tmp_file_name = f'SCREAM_data_for_Oksana.icol_{icol}.nc'
   #    print(); print(f'writing to file: {tmp_file_name}')
   #    ds_out.to_netcdf(path=tmp_file_name,mode='w')
   #    print('done')
   #***********************************
   #***********************************


   #----------------------------------------------------------------------------
   # unit conversions
   #----------------------------------------------------------------------------
   if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3

   if var[v]=='surf_mom_flux': data = np.sqrt( np.square(data[:,0]) + np.square(data[:,1]) )

   if var[v]=='qv@bot': data = data*1e3 # convert to g/kg

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------

   hc.print_stat(data,name=var[v],compact=True,indent=' '*4)

   data = data.values

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   plot[v] = ngl.xy(wks, time, data, tres)


   hs.set_subtitles(wks, plot[v], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
# pres.nglPanelYWhiteSpacePercent = 30
# pres.nglPanelXWhiteSpacePercent = 5

layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

### trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
