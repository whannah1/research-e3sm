import os, glob, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs

### case name for plot title
# case_name = 'F2010-SCREAMv1-noAero ne1024pg2'

### full path of input file
# /gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220908_low_t_mid.bca00ce9f65460369cb679fdd144897aba51a428/run/*scream.surf*
# case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1-noAero.220908_low_t_mid.bca00ce9f65460369cb679fdd144897aba51a428'
case = 'ne1024pg2_ne1024pg2.F2010-SCREAMv1.20221014_production_run.27604ccf3f1aaa88ea3413b774ef3817cad7343a'; case_name = 'ne1024pg2.F2010-SCREAMv1.20221014'
idir = f'/gpfs/alpine/cli115/proj-shared/donahue/e3sm_scratch/{case}/run'
input_file_name_srf = f'{idir}/output.scream.SurfVars*'
input_file_name_3D1 = f'{idir}/output.scream.QvT.*'
input_file_name_3D2 = f'{idir}/output.scream.QcQi.*'
input_file_name_3D3 = f'{idir}/output.scream.Cldfrac.*'
input_file_name_3D4 = f'{idir}/output.scream.QrPsPsl.*'
input_file_name_3D5 = f'{idir}/output.scream.TkeOmega.*'

scrip_file = '/gpfs/alpine/cli115/proj-shared/hannah6/grid_files/ne1024pg2_scrip.nc'


### list of variables to plot
# var = ['T_mid','qv','qc','qi']
# var = ['T_mid','qv','cldfrac_tot']
# var = ['T_mid','qv','qi','IceCloudMask']
# var = ['T_mid','qv','qc','qr']
var = ['T_mid','qv','omega','tke']


### output figure type and name
fig_type = 'png'
fig_file = 'figs_scream/debug.timeseries.v2'

### regional subset
# lat1,lat2,lon1,lon2 = -70,-60,360-90,360-30
# lat1,lat2 = -90,-60

# srf files are output every 60 min (40 days total as of 10-17-2022)
# 3D files are output once a day
file1,file2 = 30,32
# file1,file2 = 15,25

X_axis_opt = 'days' # days / steps

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_var

# res = hs.res_default()   
# res.vpHeightF = 0.1
# res.xyLineThicknessF             = 6.
# if X_axis_opt=='days' : res.tiXAxisString = 'Time [days]'
# if X_axis_opt=='steps': res.tiXAxisString = 'Time Step'


cres = hs.res_contour_fill()
cres.vpHeightF = 0.2
cres.tiXAxisString = 'Time [days]'
# cres.lbLabelFontHeightF           = 0.012
# cres.cnFillOn                     = True
# cres.cnLinesOn                    = False
# cres.cnLineLabelsOn               = False
# cres.cnInfoLabelOn                = False
# cres.lbLabelFontHeightF           = 0.008
cres.lbOrientation                = "Vertical"

cres.trYMinF = 80
# cres.trYMaxF = 

# cres.trXMinF,cres.trXMaxF = 30.2,31.4

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# open the dataset

scrip_ds = xr.open_dataset(scrip_file)
lat = scrip_ds['grid_center_lat'].values
lon = scrip_ds['grid_center_lon'].values

def get_file_list(search_name):
   file_list_tmp = sorted(glob.glob(search_name))
   file_list_tmp.remove(file_list_tmp[-1])
   first_files_tmp = file_list_tmp[:2]
   if 'file2' in globals(): file_list_tmp = file_list_tmp[:file2]
   if 'file1' in globals(): file_list_tmp = file_list_tmp[file1:]
   return file_list_tmp, first_files_tmp




file_list_srf, first_files_srf = get_file_list(input_file_name_srf)
file_list_3D1, first_files_3D1 = get_file_list(input_file_name_3D1)
# file_list_3D2, first_files_3D2 = get_file_list(input_file_name_3D2)
# file_list_3D3, first_files_3D3 = get_file_list(input_file_name_3D3)
# file_list_3D4, first_files_3D4 = get_file_list(input_file_name_3D4)


# ds_srf = xr.open_mfdataset( file_list_srf, combine='by_coords', concat_dim='time', use_cftime=True )
ds_3D1 = xr.open_mfdataset( file_list_3D1, combine='by_coords', concat_dim='time', use_cftime=True )
# ds_3D2 = xr.open_mfdataset( file_list_3D2, combine='by_coords', concat_dim='time', use_cftime=True )
# ds_3D3 = xr.open_mfdataset( file_list_3D3, combine='by_coords', concat_dim='time', use_cftime=True )
# ds_3D4 = xr.open_mfdataset( file_list_3D4, combine='by_coords', concat_dim='time', use_cftime=True )

ntime = len(ds_3D1['time'].values)

# print('creating mask...')
# ncol = ds['ncol']
# mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
# if 'lat1' in vars(): mask = mask & (ds['lat']>=lat1)
# if 'lat2' in vars(): mask = mask & (ds['lat']<=lat2)
# if 'lon1' in vars(): mask = mask & (ds['lon']>=lon1)
# if 'lon2' in vars(): mask = mask & (ds['lon']<=lon2)

#-------------------------------------------------------------------------------
# First find a column to plot
#-------------------------------------------------------------------------------

ivar='T_2m'; t1,t2=-1200,-600 ; icol = 19223697

# if 'icol' not in locals():
#    if 't2' in locals():
#       tmp_idx = ds_srf[ivar][t1:t2,:].argmin().values
#    else:
#       tmp_idx = ds_srf[ivar][t1:,:].argmin().values
#    (itime,icol) = np.unravel_index( tmp_idx, ds_srf[ivar][t1:,:].shape )


msg  = f'   icol: {icol}'
msg += f'   lat: {lat[icol]:6.2f}'
msg += f'   lon: {lon[icol]:6.2f}'
# if 'itime' in locals():
#    msg += f'   itime: {itime}'
#    msg += f'   min({ivar}): {ds_srf[ivar][itime,icol].values}'
print(f'\n{msg}\n')

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

for v in range(num_var):
   print('\n  var: '+var[v])
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------

   if var[v] in ['T_mid','qv']:
      data = ds_3D1[var[v]].isel(ncol=icol)
   elif var[v] in ['qc','qi']:
      file_list_3D2,dum = get_file_list(input_file_name_3D2)
      ds_3D2 = xr.open_mfdataset( file_list_3D2, combine='by_coords', concat_dim='time', use_cftime=True )
      data = ds_3D2[var[v]].isel(ncol=icol)
   elif var[v] in ['cldfrac_tot','IceCloudMask']:
      file_list_3D3,dum = get_file_list(input_file_name_3D3)
      ds_3D3 = xr.open_mfdataset( file_list_3D3, combine='by_coords', concat_dim='time', use_cftime=True )
      data = ds_3D3[var[v]].isel(ncol=icol)
   elif var[v] in ['qr']:
      file_list_3D4,dum = get_file_list(input_file_name_3D4)
      ds_3D4 = xr.open_mfdataset( file_list_3D4, combine='by_coords', concat_dim='time', use_cftime=True )
      data = ds_3D4[var[v]].isel(ncol=icol)
   elif var[v] in ['tke','omega']:
      file_list_3D5,dum = get_file_list(input_file_name_3D5)
      ds_3D5 = xr.open_mfdataset( file_list_3D5, combine='by_coords', concat_dim='time', use_cftime=True )
      data = ds_3D5[var[v]].isel(ncol=icol)
   # else:
   #    data = ds_srf[var[v]].isel(ncol=icol)

   if X_axis_opt=='days' :
      time = data['time'] 
      # time = ( time - time[0] ).astype('float').values / 86400e9
      ### load separate dataset of initial files for initial time coordinate value
      # ds_srf_first = xr.open_mfdataset( first_files_srf, combine='by_coords', concat_dim='time', use_cftime=True )
      # time = ( time - ds_srf_first['time'][0] ).astype('float').values / 86400e9
      ds_3D1_first = xr.open_mfdataset( first_files_3D1, combine='by_coords', concat_dim='time', use_cftime=True )
      time = ( time - ds_3D1_first['time'][0] ).astype('float').values / 86400e9
   if X_axis_opt=='steps':
      time = np.arange(0,len(time)+1,1)

   # if 'lev' in data.dims : 
   #    data = data.isel(lev=slice(100,128))
   #    lev = data['lev']


   #***********************************
   # output file for Oksana
   #***********************************
   # if v==0: ds_out = xr.Dataset()
   # ds_out[var[v]] = data
   # print(); print(ds_out)
   # if v==num_var-1: 
   #    tmp_file_name = f'SCREAM_data_for_Oksana.icol_{icol}.nc'
   #    print(); print(f'writing to file: {tmp_file_name}')
   #    ds_out.to_netcdf(path=tmp_file_name,mode='w')
   #    print('done')
   #***********************************
   #***********************************


   #----------------------------------------------------------------------------
   # unit conversions
   #----------------------------------------------------------------------------
   # if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3

   # if var[v]=='surf_mom_flux': data = np.sqrt( np.square(data[:,0]) + np.square(data[:,1]) )

   # if var[v]=='qv@bot': data = data*1e3 # convert to g/kg

   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------

   hc.print_stat(data,name=var[v],compact=True,indent=' '*4)

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   
   if 'lev' in data.dims :
      cres.tiYAxisString = 'Level'
      cres.sfXArray      = time
      cres.sfYArray      = data['lev'].values
      cres.trYReverse    = True
      plot[v] = ngl.contour(wks, data.transpose().values, cres)

   # tres = copy.deepcopy(res)
   # plot[v] = ngl.xy(wks, time, data.values, tres)

   hs.set_subtitles(wks, plot[v], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
# pres.nglPanelYWhiteSpacePercent = 30
# pres.nglPanelXWhiteSpacePercent = 5

layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

### trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
