import os, glob, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string
import cmocean
# export PYNGL_RANGS=~/.conda/envs/pyn_env/lib/ncarg/database/rangs
host = hc.get_host()
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
obs_flag = []
htype_alt_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,c=None,obs=False,d=None,htype_alt=False):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   obs_flag.append(obs)
   htype_alt_list.append(htype_alt)
#-------------------------------------------------------------------------------
var,var_str,klev_list = [],[],[]
unit_fac_list = []
def add_var(var_name,vstr=None,klev=0,unit_fac=None): 
   var.append(var_name);
   if vstr is None: 
      var_str.append(var_name)
   else:
      var_str.append(vstr)
   klev_list.append(klev)
   unit_fac_list.append(unit_fac)
#-------------------------------------------------------------------------------
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne256pg2_scrip.nc'
# scrip_file_path = '/lustre/orion/cli115/proj-shared/hannah6/files_grid/ne1024pg2_scrip.nc'
# tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'

### 2025 decadal
# scrip_file_path = '/lustre/orion/cli115/proj-shared/hannah6/files_grid/ne30pg2_scrip.nc'
# tmp_scratch = '/lustre/orion/cli115/proj-shared/brhillman/e3sm_scratch'
# add_case('v2.LR.amip_0101',n='E3SMv2',p='/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch',s='archive/atm/hist')
# add_case('v3.LR.amip_0101',n='E3SMv3',p='/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch',s='archive/atm/hist')
# scrip_file_path = '/lustre/orion/cli115/proj-shared/hannah6/files_grid/ne30pg2_scrip.nc'
# tmp_scratch = '/lustre/orion/cli115/proj-shared/brhillman/e3sm_scratch'
# add_case(f'decadal-production-run6-20240708.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.pnetcdf',n='SCREAM decadal',p=tmp_scratch,s='run')

scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'

add_case('IMERG', n='IMERG')

tmp_scratch = '/global/cfs/cdirs/e3smdata/simulations/scream-decadal'
add_case(f'decadal-production-run6-20240708.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.pnetcdf',n='SCREAM decadal',p=tmp_scratch,s='run')

add_case('v2.LR.amip_0101', n='EAMv2', p='/global/cfs/cdirs/e3smdata/simulations/',s='archive/atm/hist') 
add_case('v3.LR.amip_0101', n='EAMv3', p='/global/cfs/cdirs/m3312/whannah/e3smv3_amip',s='archive/atm/hist')


# first_file,num_files = 3,12#12*5
# first_file,num_files = 3+12*4,12*1 # 1999

#-------------------------------------------------------------------------------

add_var('precip_total_surf_mass_flux',vstr='1999 Mean Precip [mm/day]',unit_fac=86400*1e3)
# add_var('VapWaterPath')
# add_var('LiqWaterPath',vstr='LWP')
# add_var('IceWaterPath',vstr='IWP')
# add_var('LW_flux_up_at_model_top')
# add_var('T_2m')
# add_var('horiz_winds_at_model_bot_u')

# add_var('ps')
# add_var('SeaLevelPressure')
# add_var('wind_speed',klev=127)

# add_var('precip_ice_surf_mass')
# add_var('precip_liq_surf_mass')
# add_var('LiqWaterPath')
# add_var('IceWaterPath')
# add_var('surf_radiative_T')
# add_var('T_2m')
# add_var('ps')
# add_var('wind_speed_10m')
# add_var('surf_evap',unit_fac=1e3)
# add_var('surf_sens_flux')
# add_var('SW_flux_up_at_model_top')
# add_var('SW_flux_dn_at_model_top')
# add_var('LW_flux_up_at_model_top')
# add_var('LW_flux_dn_at_model_top')

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.map.v2'

#-------------------------------------------------------------------------------
# lat1,lat2 = -30,30

# lat1,lat2,lon1,lon2 = 0,30,360-65,360-15 # Atlantic wide view

# lat1,lat2,lon1,lon2 = 0,40,180-60,180+0 # Pacific wide view
# lat1,lat2,lon1,lon2 = -40,40,180-60,180+60 # Pacific wider view

# lat1,lat2,lon1,lon2 = -20,0,220,240
# lat1,lat2,lon1,lon2 = 10,30,360-100,360-55

# lat1,lat2,lon1,lon2 = -10,70,180,340             # N. America
# lat1,lat2,lon1,lon2 = 25, 50, 360-125, 360-75    # CONUS
# lat1,lat2,lon1,lon2 = -40,40,90,240              # MC + West Pac
# lat1,lat2,lon1,lon2 = -20,20,150,200              # MC + West Pac
# lat1,lat2,lon1,lon2 = -5,5,150,160              # MC + West Pac
# lat1,lat2,lon1,lon2 = 0,60,50,120                # India
# lat1,lat2,lon1,lon2 =  20,60,360-60,360-10              # Atlantic
# lat1,lat2,lon1,lon2 = -45,-45+60,360-120,360-40  # S. America
# lat1,lat2,lon1,lon2 = -0,25,360-115,360-75  # Panama

# lat1,lat2,lon1,lon2 = 15,35,360-100,360-75 # Caribbean
# lat1,lat2,lon1,lon2 = 27,30,360-92,360-88 # Louisana coast (Katrina landfall)
# lat1,lat2,lon1,lon2 = 25,30,360-92,360-86 # Louisana coast wider (Katrina landfall)

# lat1,lat2,lon1,lon2 =10,40,100,140  # Taiwan

# lat1,lat2,lon1,lon2 = 0,10,0,10
#-------------------------------------------------------------------------------
# first_file,num_files = 0,12
# first_file,num_files = 300,1
# first_file,num_files = 2,1

use_remap,remap_grid = False,'90x180' # 90x180 / 180x360

plot_diff,add_diff = False,False
# plot_diff,add_diff = False,False

use_snapshot,ss_t = False,0

print_stats = True

var_x_case = False

num_plot_col = 2#len(case)

use_common_label_bar = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
diff_base = 0

if case==[]: raise ValueError('ERROR - case list is empty!')
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.015

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]
# if 'lev' not in vars(): lev = np.array([0])

if 'scrip_file_path' not in locals(): scrip_file_path = None

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
# npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
dx = 0.25
if 'lat1' in locals() : res.mpMinLatF = lat1+dx; res.mpMaxLatF = lat2-dx
if 'lon1' in locals() : res.mpMinLonF = lon1+dx; res.mpMaxLonF = lon2-dx

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.01
# res.tmXBOn                       = False
# res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

res.mpCenterLonF                 = 180
# res.mpProjection                 = 'Robinson'
res.mpProjection                 = 'Mollweide'
res.pmTickMarkDisplayMode        = 'Never'
# res.mpPerimOn                    = False

# res.mpDataBaseVersion = 'MediumRes'
# res.mpDataBaseVersion = 'HighRes'

#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'scream'
   return comp
#---------------------------------------------------------------------------------------------------
def get_ctr_str(glb_avg=None):
   ctr_str = ''
   if 'lat1' in globals():
      lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
      lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
      ctr_str += f' {lat1_str}:{lat2_str} '
   if 'lon1' in globals():
      lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
      lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
      ctr_str += f' {lon1_str}:{lon2_str} '
   # if glb_avg is not None:
   #    # add logic here t display global average value?
   return ctr_str
#---------------------------------------------------------------------------------------------------

scrip_ds = xr.open_dataset(scrip_file_path)

use_mask = False

if use_mask:

   lat_name,lon_name = 'grid_center_lat','grid_center_lon'
   tmp_data = np.ones(len(scrip_ds[lat_name]),dtype=bool)
   mask = xr.DataArray( tmp_data, coords=scrip_ds[lat_name].coords )
   if 'lat1' in locals(): mask = mask & (scrip_ds[lat_name]>=lat1) & (scrip_ds[lat_name]<=lat2)
   if 'lon1' in locals(): mask = mask & (scrip_ds[lon_name]>=lon1) & (scrip_ds[lon_name]<=lon2)

   scrip_ds_center_lon = scrip_ds['grid_center_lon'].where(mask,drop=True)
   scrip_ds_center_lat = scrip_ds['grid_center_lat'].where(mask,drop=True)
   scrip_ds_corner_lon = scrip_ds['grid_corner_lon'].where(mask,drop=True)
   scrip_ds_corner_lat = scrip_ds['grid_corner_lat'].where(mask,drop=True)

   mask = mask.rename({'grid_size':'ncol'})

else:

   scrip_ds_center_lon = scrip_ds['grid_center_lon']
   scrip_ds_center_lat = scrip_ds['grid_center_lat']
   scrip_ds_corner_lon = scrip_ds['grid_corner_lon']
   scrip_ds_corner_lat = scrip_ds['grid_corner_lat']

# print()
# print(scrip_ds_center_lat)
# print()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   glb_avg_list = []
   std_list,cnt_list = [],[]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      # data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      # if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      # if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      # case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
      #                     data_dir=data_dir_tmp, data_sub=data_sub_tmp,
      #                     populate_files=False )
      # case_obj.set_coord_names(var[v])

      # if 'lat1' in locals() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in locals() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------      
      # area = case_obj.load_data('area',component=get_comp(case[c]),htype=htype,file_prefix=None)

      # tvar = var[v]
      # if var[v]=='horiz_winds_at_model_bot_u': tvar = 'horiz_winds_at_model_bot'
      # if var[v]=='horiz_winds_at_model_bot_v': tvar = 'horiz_winds_at_model_bot'

      # data = case_obj.load_data(tvar, component=get_comp(case[c]),
      #                                 htype=htype,ps_htype=htype,lev=lev,file_prefix=None,
      #                                 first_file=first_file,num_files=num_files,
      #                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      #-------------------------------------------------------------------------
      if case[c]=='IMERG':
         file_path = '/pscratch/sd/w/whannah/Obs/IMERG/monthly_remap_ne30pg2/*1999*'
      else:
         if 'SCREAM' in case[c]:
            htype = 'output.scream.decadal.monthlyAVG_ne30pg2.AVERAGE.nmonths_x1'
         else:
            htype = 'eam.h0'
         file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*{htype}*1999*'

      
      file_list_all = sorted(glob.glob(file_path))

      file_list = []
      for f in file_list_all:
         if '~' in f: continue
         file_list.append(f)

      #-------------------------------------------------------------------------
      # print()
      # print(file_path)
      # print()
      # for f in file_list: print(f)
      # print()
      #-------------------------------------------------------------------------
      # file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/*{htype}*'
      # file_list_all = sorted(glob.glob(file_path))
      # # screen out files
      # file_list = []
      # for f in file_list_all:
      #    if '~' in f: continue
      #    file_list.append(f)
      # if 'first_file' in locals(): file_list = file_list[first_file:]
      # if 'num_files' in locals(): file_list = file_list[:num_files]
      # print()
      # for f in file_list: print(f)
      # print()
      #-------------------------------------------------------------------------
      # # load each file separately to determine problematic files
      # for f in file_list: 
      #    print(f)
      #    ds = xr.open_mfdataset(f)
      # exit()
      #-------------------------------------------------------------------------
      ds = xr.open_mfdataset( file_list, use_cftime=True, decode_times=False,decode_cf=False )
      #-------------------------------------------------------------------------
      tvar = var[v]
      var_chk = var[v] in ds.variables
      # print()
      # print(f'var_chk: {var_chk}')
      # print()
      if case[c]=='IMERG':
         tvar = 'precipitation'
      elif 'SCREAM' in case[c]:
         if var[v]=='precip_total_surf_mass_flux' and not var_chk: tvar = 'precip_liq_surf_mass_flux'
         if var[v]=='horiz_winds_at_model_bot_u' : tvar = 'horiz_winds_at_model_bot'
         if var[v]=='horiz_winds_at_model_bot_v' : tvar = 'horiz_winds_at_model_bot'
      else:
         if var[v]=='precip_total_surf_mass_flux': tvar = 'PRECC'

      data = ds[tvar]

      if case[c]=='IMERG':
         data = data * 24. / unit_fac_list[v]
      elif 'SCREAM' in case[c]:
         if var[v]=='precip_total_surf_mass_flux': data = data + ds['precip_ice_surf_mass_flux']
         if var[v]=='horiz_winds_at_model_bot_u' : data = data.isel(dim2=0)
         if var[v]=='horiz_winds_at_model_bot_v' : data = data.isel(dim2=1)
      else:
         if var[v]=='precip_total_surf_mass_flux': data = data + ds['PRECL']

      # Get rid of lev dimension
      if 'lev' in data.dims:
         if klev_list[v] is not None:
            data = data.isel(lev=klev_list[v])
      #-------------------------------------------------------------------------
      if use_mask: data = data.where(mask,drop=True)

      # print('-'*80)
      # print()
      # print(data)
      # print()
      # print('-'*80)
      # exit()

      #-------------------------------------------------------------------------
      # adjust units
      # print(); print(data.isel(ncol=10).values)
      if unit_fac_list[v] is not None : data = data * unit_fac_list[v]
      # if var[v]=='precip_total_surf_mass_flux': data = data*86400*1e3 # m/s => mm/day
      # print(); print(data.isel(ncol=10).values)
      # exit()
      #-------------------------------------------------------------------------
      # # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         # hc.print_time_length(data.time,indent=' '*6)
         if use_snapshot:
            data = data.isel(time=ss_t)
            print(f'{hc.tcolor.RED}WARNING - using snapshot! (ss_t={ss_t}){hc.tcolor.ENDC}')
         else:
            data = data.mean(dim='time')
      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
         glb_avg_list.append(gbl_mean)
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      # print(); print(data)

      if case[c]=='TRMM' and 'lon1' not in locals(): 
         data_list.append( ngl.add_cyclic(data.values) )
      else:
         data_list.append( data.values )

      if 'area' in locals() : area_list.append( area.values )
      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

      if var[v]=='wind_speed_10m': diff_data_min,diff_data_max = -1,1

   for c in range(num_case):

      # data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      # if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      # if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      # case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
      #                     data_dir=data_dir_tmp, data_sub=data_sub_tmp,
      #                     populate_files=False )
      # case_obj.set_coord_names(var[v])

      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      rain_clr_map = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      amp_clr_map  = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      bal_clr_map  = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
      
      tres.cnFillPalette = "MPL_viridis"

      # if var[v]=='U'                          : tres.cnFillPalette = bal_clr_map
      # if 'horiz_winds' in var[v]              : tres.cnFillPalette = bal_clr_map
      # if var[v]=='T_2m'                       : tres.cnFillPalette = amp_clr_map
      # if var[v]=='precip_total_surf_mass_flux': tres.cnFillPalette = rain_clr_map
      # if var[v]=='precip_ice_surf_mass'       : tres.cnFillPalette = rain_clr_map
      # if var[v]=='precip_liq_surf_mass'       : tres.cnFillPalette = rain_clr_map
      # if var[v]=='LiqWaterPath'               : tres.cnFillPalette = rain_clr_map
      # if var[v]=='IceWaterPath'               : tres.cnFillPalette = rain_clr_map
      
      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      if 'precip' in var[v]      : tres.cnLevels = np.arange(1,20+1,1)
      # if 'precip' in var[v]      : tres.cnLevels = np.arange(1,101+5,5)
      # if 'precip' in var[v]      : tres.cnLevels = np.logspace( -2, 2, num=20).round(decimals=2)
      if var[v]=='VapWaterPath'  : tres.cnLevels = np.arange(10,60+10,10)
      if var[v]=='LiqWaterPath'  : tres.cnLevels = np.arange(1,61+3,3)/1e2
      if var[v]=='IceWaterPath'  : tres.cnLevels = np.arange(1,61+3,3)/1e1

      # if 'precip' in var[v]      : tres.cnLevels = np.logspace(  -0.5, 2.6, num=30).round(decimals=2)
      # if 'precip' in var[v]      : tres.cnLevels = np.logspace(  -0.2, 2.6, num=40).round(decimals=2)
      # if var[v]=='LiqWaterPath'  : tres.cnLevels = np.logspace( -2, 1.2, num=20).round(decimals=2)
      # if var[v]=='IceWaterPath'  : tres.cnLevels = np.logspace( -2, 1.2, num=20).round(decimals=2)

      # # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      # if var[v]=='P-E'                 : tres.cnLevels = np.linspace(-10,10,21)
      # # if var[v]=='TS'                  : tres.cnLevels = np.arange(0,40+2,2)
      # if var[v]=='RH'                  : tres.cnLevels = np.arange(10,100+1,1)
      # # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(1,30+1,1)*1e-2
      # # if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2)
      # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(0.005,0.155,0.01)
      # if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.arange(0.01,0.25,0.015)

      # if var[v]=='T_2m'                       : tres.cnLevels = np.arange(290,310+1,1)
      # if var[v]=='U_at_500hPa'                : tres.cnLevels = np.arange(?,?+1,1)
      
      #-------------------------------------------------------------------------
      ### print color levels
      # if hasattr(tres,'cnLevels') : 
      #    print(f'\ntres.cnLevels:')
      #    msg = ''
      #    for cl in range(len(tres.cnLevels)):
      #       msg += f'{tres.cnLevels[cl]}, '
      #    print(f'[ {msg} ]\n')
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 41
         aboutZero = False
         if var[v] in ['SPTLS','SPQTLS','U','V','VOR','DIV',
                       'U850','V850','U200','V200',
                       'MMF_CVT_TEND_T','MMF_CVT_TEND_Q',] : 
            aboutZero = True
         if var[v]=='U':aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      # var_str_tmp = var_str[v]
      # # if var[v]=='PRECT':     var_str = 'Precipitation'

      # lev_str = None
      # if lev>0: lev_str = f'{lev}mb'
      # if lev<0: lev_str = f'k={(lev*-1)}'
      # # if lev_str is not None and var[v] in ['U','V','OMEGA','T','Q','Z3']:
      # if lev_str is not None:
      #    var_str_tmp = f'{lev_str} {var[v]}'
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_common_label_bar: 
         tres.lbLabelBarOn = False
      else:
         tres.lbLabelBarOn = True

      # if use_remap or case_obj.obs :
      #    hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
      # else:
      #    hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_path)

      tres.cnFillMode    = 'CellFill'
      tres.sfXArray      = scrip_ds_center_lon.values # scrip_ds['grid_center_lon'].where(mask,drop=True).values
      tres.sfYArray      = scrip_ds_center_lat.values # scrip_ds['grid_center_lat'].where(mask,drop=True).values
      tres.sfXCellBounds = scrip_ds_corner_lon.values # scrip_ds['grid_corner_lon'].where(mask,drop=True).values
      tres.sfYCellBounds = scrip_ds_corner_lat.values # scrip_ds['grid_corner_lat'].where(mask,drop=True).values
         
         
      if plot_diff and c==diff_base : base_name = name[c]

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         # plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres) 
         
         #----------------------------------------------------------------------
         # set plot subtitles

         # if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'

         # hs.set_subtitles(wks, plot[ip], name[c], get_ctr_str(glb_avg=None), var_str[v], font_height=subtitle_font_height)
         hs.set_subtitles(wks, plot[ip], '', name[c], '', font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels

         if 'precip' in var[v]      : tres.cnLevels = np.arange(-10,10+1,1)
         if var[v]=='VapWaterPath'  : tres.cnLevels = np.arange(-10,10+1,1)/2
         if var[v]=='LiqWaterPath'  : tres.cnLevels = np.arange(-10,10+1,1)*1e-2/4

         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=11,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=11)
         
         ### override the level settings and just use auto
         # tres.cnLevelSelectionMode = "AutomaticLevels"

         # if use_remap:
         #    hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
         # else:
         #    hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_path)

         tres.cnFillMode    = 'CellFill'
         tres.sfXArray      = scrip_ds_center_lon.values
         tres.sfYArray      = scrip_ds_center_lat.values
         tres.sfXCellBounds = scrip_ds_corner_lon.values
         tres.sfYCellBounds = scrip_ds_corner_lat.values
            
         tres.lbLabelBarOn = True

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+(num_case-1)
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         # plot[ipd] = ngl.contour_map(wks,data_list[c],tres)
         plot[ipd] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres)
         #----------------------------------------------------------------------
         # set plot subtitles

         # if glb_avg_list != []: 
         #    glb_diff = glb_avg_list[c] - glb_avg_list[diff_base]
         #    ctr_str += f' ({glb_diff:6.4})'
         
         hs.set_subtitles(wks, plot[ipd], name[c], get_ctr_str(glb_avg=None), var_str[v]+' (Diff)', font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if plot_diff : num_case = num_case+len(diff_case)   # use this to plot both before and after diff

# title_str = None
# title_str = 'ANN'
# if months==[1,2,12]: title_str = 'DJF'
# if months==[6,7,8]: title_str = 'JJA'
# if title_str is not None:
#    textres =  ngl.Resources()
#    textres.txFontHeightF =  0.025
#    ngl.text_ndc(wks,title_str,0.5,.7,textres)

num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]


if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### use common label bar
if use_common_label_bar:
   pnl_res.nglPanelLabelBar = True
   pnl_res.lbTitleString      = f'{var_str[v]}'
   pnl_res.lbTitlePosition    = 'bottom'
   pnl_res.lbTitleFontHeightF = 0.02
   pnl_res.nglPanelLabelBarLabelFontHeightF = 0.01
   

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 10

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
