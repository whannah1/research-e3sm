#-------------------------------------------------------------------------------
import os, ngl, copy, string, xarray as xr, numpy as np, glob, dask, numba, cmocean, subprocess as sp
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
#-------------------------------------------------------------------------------

fig_file,fig_type = f'figs_scream/scream.hov.v1','png'

recalculate = True

time_mean_opt    = '10D' # none / 1D / 5D / 10D

lat1,lat2 = -15,15
dlon = 2

# num_files = 10 # comment to disable and load all files

# num_plot_col = 1

print_stats = True

#---------------------------------------------------------------------------------------------------
tmp_file_root, tmp_file_prefix = os.getenv('HOME')+f'/Research/E3SM/data_temp','scream.hov.v1'
def get_tmp_file(case,var):
   return f'{tmp_file_root}/{tmp_file_prefix}.{case[c]}.{var[v]}.{time_mean_opt}.nc'
#---------------------------------------------------------------------------------------------------
case,case_name,case_dir,case_sub = [],[],[],[]
obs_flag = []
def add_case(case_in,n='',p=None,s='',g=None,d=0,c='black',m=0,r=False,obs=False):
   global case_name,case,case_dir,case_sub,clr,dsh,mrk
   case.append(case_in); case_name.append(n); case_dir.append(p); case_sub.append(s)
   obs_flag.append(obs)
#---------------------------------------------------------------------------------------------------
var = []
var_str = []
var_unit = []
file_type_list = []
obs_var_list = []
obs_file_type_list = []
lev_list = []
def add_var(var_name, file_type, obs_var=None, obs_file_type=None, name='', unit='', lev=None):
   var.append(var_name)
   file_type_list.append(file_type)
   obs_var_list.append(obs_var)
   obs_file_type_list.append(obs_file_type)
   var_str.append(name)
   var_unit.append(unit)
   lev_list.append(lev)
#---------------------------------------------------------------------------------------------------
# scrip_file_sim = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/ne1024pg2_scrip.nc'
# scrip_file_obs = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/IMERG_1800x3600_scrip.nc'
# scrip_file_sim = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne1024pg2.nc'
# scrip_file_sim = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne30pg2.nc'
scrip_file_obs = '~/HICCUP/data_scratch/scrip_ERA5_721x1440.nc'
obs_data_root  = '/lustre/orion/cli115/proj-shared/hannah6/obs_data'

### SCREAMv1 decadal
# tmp_scratch = f'/lustre/orion/cli115/proj-shared/brhillman/e3sm_scratch'
# add_case('decadal-production-20240305.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.run1', p=tmp_scratch,s='run')
tmp_scratch = f'/global/cfs/cdirs/e3smdata/simulations/scream-decadal'
add_case('decadal-production-run6-20240708.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.pnetcdf', n='SCREAMv1 decadal run6', p=tmp_scratch,s='run')

# /global/cfs/cdirs/e3smdata/simulations/scream-decadal/decadal-production-run6-20240708.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.pnetcdf/run/output.scream.decadal.6hourlyAVG_ne30pg2.AVERAGE.nhours_x6.*

# add_case('ERA5',n='ERA5', p=obs_data_root,s='daily',obs=True,d=0,c='black')
# add_case('IMERG',n='IMERG Jan 2020',p=obs_data_root,s='daily_QC_Jan_2020',obs=True,d=1,c='red')


#-------------------------------------------------------------------------------

# scrip_file_sim = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne30pg2.nc'
# file_type = 'output.scream.AutoCal.hourly_inst_ne30pg2.INSTANT.nhours_x1.'
scrip_file_sim = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
file_type = 'output.scream.decadal.6hourlyAVG_ne30pg2.AVERAGE.nhours_x6.'

add_var('precip_total_surf_mass_flux', file_type, name='precip')
add_var('LW_flux_up_at_model_top',     file_type, name='OLR')
# add_var('U_at_model_bot',              file_type, name='OLR')

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
   
plot = [None]*(num_var*num_case)

res = hs.res_contour_fill()
res.vpWidthF = 0.3
res.tmYLLabelFontHeightF   = 0.015
res.tmXBLabelFontHeightF   = 0.015
res.tiXAxisFontHeightF     = 0.015
res.tiYAxisFontHeightF     = 0.015
res.tiYAxisString          = 'Days'
res.tiXAxisString          = 'Longitude'
res.lbOrientation          = 'Vertical'


lres = hs.res_xy()
lres.xyDashPattern    = 1
lres.xyLineThicknessF = 2
lres.xyLineColor      = 'black'

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

sim_grid_ds = xr.open_dataset( scrip_file_sim ).rename({'grid_size':'ncol'})
sim_area = sim_grid_ds['grid_area']
sim_lat  = sim_grid_ds['grid_center_lat']
sim_lon  = sim_grid_ds['grid_center_lon']

tmp_data = np.ones(len(sim_area),dtype=bool)
sim_mask = xr.DataArray( tmp_data, coords=sim_area.coords )
if 'lat1' in locals(): sim_mask = sim_mask & (sim_lat>=lat1) & (sim_lat<=lat2)
# if 'lon1' in locals(): sim_mask = sim_mask & (sim_lon>=lon1) & (sim_lon<=lon2)

sim_area = sim_area.where(sim_mask,drop=True)
sim_lat  = sim_lat .where(sim_mask,drop=True)
sim_lon  = sim_lon .where(sim_mask,drop=True)

# obs_grid_ds = xr.open_dataset( scrip_file_obs ).rename({'grid_size':'ncol'})
# obs_grid_ds['grid_center_lon'] = xr.where(obs_grid_ds['grid_center_lon']<0,obs_grid_ds['grid_center_lon']+360,obs_grid_ds['grid_center_lon'])
# obs_grid_ds['grid_corner_lon'] = xr.where(obs_grid_ds['grid_corner_lon']<0,obs_grid_ds['grid_corner_lon']+360,obs_grid_ds['grid_corner_lon'])
# obs_area = obs_grid_ds['grid_area']
# obs_lat  = obs_grid_ds['grid_center_lat']
# obs_lon  = obs_grid_ds['grid_center_lon']

# era_grid_ds = xr.open_dataset( scrip_file_era ).rename({'grid_size':'ncol'})
# era_grid_ds['grid_center_lon'] = xr.where(era_grid_ds['grid_center_lon']<0,era_grid_ds['grid_center_lon']+360,era_grid_ds['grid_center_lon'])
# era_grid_ds['grid_corner_lon'] = xr.where(era_grid_ds['grid_corner_lon']<0,era_grid_ds['grid_corner_lon']+360,era_grid_ds['grid_corner_lon'])

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_var_data(ds,var,obs_flag):
   tvar = var
   if var=='horiz_winds_at_model_bot_u': tvar = 'horiz_winds_at_model_bot'
   if var=='horiz_winds_at_model_bot_v': tvar = 'horiz_winds_at_model_bot'
   #----------------------------------------------------------------------------
   if var=='precip':
      data = ds['precip_liq_surf_mass'] + ds['precip_ice_surf_mass']
   elif var=='precip_total_surf_mass_flux':
      data = ds['precip_liq_surf_mass_flux'] + ds['precip_ice_surf_mass_flux']
   else:
      data = ds[var]
   #----------------------------------------------------------------------------
   if obs_flag:
      if var=='U_at_850hPa': data = data.sel(level=850)
      if var=='V_at_850hPa': data = data.sel(level=850)
      if var=='U_at_500hPa': data = data.sel(level=500)
      if var=='V_at_500hPa': data = data.sel(level=500)
   else:
      if var=='horiz_winds_at_model_bot_u': data = data.isel(dim2=0)
      if var=='horiz_winds_at_model_bot_v': data = data.isel(dim2=1)
   #----------------------------------------------------------------------------
   # convert units
   if var=='precip_total_surf_mass_flux':
      data = data*86400*1e3 # m/s to mm/day
   if var=='precip':
      if     obs_flag: data = data*24.          # mm/hr to mm/day
      if not obs_flag: data = (data/100)*86400  # kg/m2 to mm/day using dtime=100 (ne1024)
   #----------------------------------------------------------------------------
   if obs_flag:
      if 'latitude'  in data.dims: data = data.rename({'latitude':'lat'})
      if 'longitude' in data.dims: data = data.rename({'longitude':'lon'})
      data = data.stack(ncol=('lat','lon'))
   #----------------------------------------------------------------------------
   return data
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   data_list = []
   time_list = []
   lon_list  = []
   for c in range(num_case):
      if obs_flag[c]:
         tfile_type = obs_file_type_list[v]
         tvar = obs_var_list[v]
      else:
         tfile_type = file_type_list[v]
         tvar = var[v]
      if c==0: print(' '*2+'var: '+hc.tcolor.GREEN+tvar+hc.tcolor.ENDC)
      print('\n'+' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
      #-------------------------------------------------------------------------
      tmp_file = get_tmp_file(case[c],var[v])
      if recalculate :
         print(' '*6+'recalculating...')
         #----------------------------------------------------------------------
         # idenfity the files to load
         tcase = case[c]
         file_path = f'{case_dir[c]}/{tcase}/{case_sub[c]}/{tfile_type}*'
         file_list = sorted(glob.glob(file_path))
         #----------------------------------------------------------------------
         if 'num_files' in locals(): file_list = file_list[:num_files]
         #----------------------------------------------------------------------
         if file_list==[]: print('ERROR: Empty file list:'); print(); print(file_path); exit()
         #----------------------------------------------------------------------
         print(' '*6+f'Loading data ({tvar})...')
         #----------------------------------------------------------------------
         group_name = None # group_name = 'Grid' if case[c]=='IMERG' else None
         ds = xr.open_mfdataset( file_list, group=group_name)
         #----------------------------------------------------------------------
         # Load the data
         data = get_var_data(ds,tvar,obs_flag[c])
         #----------------------------------------------------------------------
         # apply mask
         if not obs_flag[c]: data = data.where(sim_mask,drop=True)
         # if     obs_flag[c]: data = data.where(obs_mask,drop=True)
         #----------------------------------------------------------------------
         if time_mean_opt!='none': 
            data = data.resample(time=time_mean_opt).mean(dim='time')
         #-------------------------------------------------------------------
         if 'lev' in data.dims and lev_list[v] is not None: 
            if lev_list[v]<0: data = data.isel({'lev':np.absolute(lev_list[v])})
         #-------------------------------------------------------------------------
         # Calculate hovmoller
         print(' '*6+f'Calculating hovmoller: {tmp_file}')
         if     obs_flag[c]: area, lon = obs_area, obs_lon
         if not obs_flag[c]: area, lon = sim_area, sim_lon
         bin_ds = hc.bin_YbyX(data, lon, bin_min=lon.min().values, bin_max=lon.max().values,
                              bin_spc=dlon, wgt=area, keep_time=True ).drop_vars(['bin_std'])
         bin_ds['bin_val'] = bin_ds['bin_val'].transpose('time','bin')
         bin_ds['bin_cnt'] = bin_ds['bin_cnt'].transpose('time','bin')
         bin_ds['time'] = data['time']
         del area; del lon
         #----------------------------------------------------------------------
         # write to file
         print(' '*6+f'writing to file: {tmp_file}')
         bin_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( tmp_file )
      #-------------------------------------------------------------------------
      # print some summary stats after spatial averaging
      if print_stats: hc.print_stat(bin_ds['bin_val'],name=tvar,compact=True,indent=' '*6)
      #-------------------------------------------------------------------------
      data_list.append( bin_ds['bin_val'].values )
      time_list.append( bin_ds['time'] )
      lon_list.append( bin_ds['bin'].values )
   #----------------------------------------------------------------------------
   # Create plot
   tres = copy.deepcopy(res)
   data_min = np.min([np.min(d) for d in data_list])
   data_max = np.max([np.max(d) for d in data_list])
   #----------------------------------------------------------------------------
   # Set colors
   tres = copy.deepcopy(res)
   rain_clr_map = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
   amp_clr_map  = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
   bal_clr_map  = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
   tres.cnFillPalette = "MPL_viridis"
   if var[v]=='U'                          : tres.cnFillPalette = bal_clr_map
   if var[v]=='T_2m'                       : tres.cnFillPalette = amp_clr_map
   if var[v]=='precip_total_surf_mass_flux': tres.cnFillPalette = rain_clr_map
   if var[v]=='precip_ice_surf_mass'       : tres.cnFillPalette = rain_clr_map
   if var[v]=='precip_liq_surf_mass'       : tres.cnFillPalette = rain_clr_map
   if var[v]=='LiqWaterPath'               : tres.cnFillPalette = rain_clr_map
   if var[v]=='IceWaterPath'               : tres.cnFillPalette = rain_clr_map
   #----------------------------------------------------------------------------
   # Set explicit contour levels
   if var[v]=='precip_total_surf_mass_flux': tres.cnLevels = np.logspace( -2, 2, num=20).round(decimals=2)
   #----------------------------------------------------------------------------
   for c in range(num_case):
      ip = v*num_case+c
      #-------------------------------------------------------------------------
      time = time_list[c]
      time = ( time - time[0] ).astype('float') / 86400e9
      tres.sfYArray = time.values
      tres.sfXArray = lon_list[c]
      #-------------------------------------------------------------------------
      plot[ip] = ngl.contour(wks, np.ma.masked_invalid(  data_list[c] ), tres) 
   #----------------------------------------------------------------------------
   hs.set_subtitles(wks, plot[v], case_name[c], '', var_str[v], font_height=0.01)
#---------------------------------------------------------------------------------------------------
if 'num_plot_col' in locals():
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [1,num_var]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

