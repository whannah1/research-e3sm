import os, ngl, copy, xarray as xr, numpy as np

### case name for plot title
case_name = 'SCREAM.2024-autocal-spinup.ne1024pg2_ne1024pg2.F2010-SCREAMv1.NN_2048'

### full path of input file
input_file_name = '/global/homes/w/whannah/E3SM/scratch/scream/add-ne1024-grid.FC5AV1C-H01A.ne1024np4_360x720cru_oRRS15to5.cori-knl_intel.2048x8x16.ifs-hindcast.20190716-0819.cam.h1.2016-08-01-00000.nc'

### scrip file for native grid plot
scrip_file_name = '/global/homes/w/whannah/E3SM/scratch/scream/ne1024np4_scrip_c20190603.nc'

### list of variables to plot
var = ['PRECT']

### single time index to load (no averaging)
time_slice = 40

### output figure type and name
fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.map.v1.'+str(time_slice).zfill(3)

### uncomment to plot a regional subset
# lat1,lat2,lon1,lon2 = 0,40,360-160,360-80     # East Pac
# lat1,lat2,lon1,lon2 = -30,0,360-90,360-60     # S. America
# lat1,lat2,lon1,lon2 = -45,0,90,180            # Australia

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = []

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.lbOrientation                = "Horizontal"
res.lbLabelFontHeightF           = 0.008
# res.mpGridAndLimbOn              = False
# res.mpCenterLonF                 = 180
# res.mpLimitMode                  = "LatLon" 
# if 'lat1' in vars() : res.mpMinLatF = lat1
# if 'lat2' in vars() : res.mpMaxLatF = lat2
# if 'lon1' in vars() : res.mpMinLonF = lon1
# if 'lon2' in vars() : res.mpMaxLonF = lon2


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+var[v])
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   ds = xr.open_dataset( input_file_name )

   ncol = ds['ncol']
   mask = xr.DataArray( np.ones([len(ncol)],dtype=bool), coords=[('ncol', ncol)], dims='ncol' )
   if 'lat1' in vars(): mask = mask & (ds['lat']>=lat1)
   if 'lat2' in vars(): mask = mask & (ds['lat']<=lat2)
   if 'lon1' in vars(): mask = mask & (ds['lon']>=lon1)
   if 'lon2' in vars(): mask = mask & (ds['lon']<=lon2)

   data = ds[var[v]].isel(time=time_slice).where(mask,drop=True)

   # adjust units
   if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3

   ### print temporal frequency and length
   # dtime = ( data['time'][-1] - data['time'][0] ).values.astype('timedelta64[D]')
   # print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

   ### Calculate time mean
   if 'time' in data.dims : data = data.mean(dim='time')

   ### Calculate area-weighted global mean
   # area = ds['area'].where( mask,drop=True)
   # gbl_mean = ( (data*area).sum() / area.sum() ).values 
   # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

   #----------------------------------------------------------------------------
   # Set colors and contour levels
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   ### change the color map depending on the variable
   tres.cnFillPalette = "MPL_viridis"
   if var[v] in ['OMEGA']                    : tres.cnFillPalette = 'BlueWhiteOrangeRed'
   if var[v] in ['PRECT','PRECC','PRECL']    : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
   if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = 'CBR_wet'

   ### specify specific contour intervals
   if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,60+1,1)
   if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
   # if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2) # better for MMF
   if var[v]=="TGCLDLWP"            : tres.cnLevels = np.logspace( -2,-0.2, num=60).round(decimals=2) # better for non-MMF

   ### use this for symmetric contour intervals
   if var[v] in ['U','V'] :
      cmin,cmax,cint,clev = ngl.nice_cntr_levels(data.min().values, data.max().values,       \
                                                 cint=None, max_steps=21,              \
                                                 returnLevels=True, aboutZero=True )
      tres.cnLevels = np.linspace(cmin,cmax,num=91)

   if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
   
   #----------------------------------------------------------------------------
   # Set up cell fill attributes using scrip grid file
   #----------------------------------------------------------------------------
   scripfile = xr.open_dataset(scrip_file_name)
   tres.cnFillMode    = 'CellFill'
   tres.sfXArray      = scripfile['grid_center_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
   tres.sfYArray      = scripfile['grid_center_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
   tres.sfXCellBounds = scripfile['grid_corner_lon'].rename({'grid_size':'ncol'}).where( mask,drop=True).values 
   tres.sfYCellBounds = scripfile['grid_corner_lat'].rename({'grid_size':'ncol'}).where( mask,drop=True).values
   
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   plot.append( ngl.contour_map(wks,data.values,tres) )
   set_subtitles(wks, plot[len(plot)-1], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5
layout = [len(plot),1]
ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

### trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
