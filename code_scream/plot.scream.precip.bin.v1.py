# v1 - original
# v2 - calculate dist for each point first as a way to handle area weighting issues
import os, ngl, xarray as xr, numpy as np, pandas as pd
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
home = os.getenv("HOME")
print()

case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)

### 2023 vertical grid tests
add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v10',n='L128v1.0',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v21',n='L128v2.1',c='red',  p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v22',n='L128v2.2',c='blue', p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

var = 'PRECT'

fig_type = "png"
fig_file = home+"/Research/E3SM/figs_clim/clim.precip.bin.v2"

lat1,lat2 = -50,50
# lat1,lat2 = -30,30

### very small domain for debugging
# xlat,xlon,dx,dy = 36, 360-97, 2, 2 # ARM SGP
# if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

# htype,years,months,first_file,num_files = 'h1',[],[],20,1
htype,years,months,first_file,num_files = 'h1',[],[],10,20

recalculate = True

temp_dir = home+"/Research/E3SM/data_temp"

#---------------------------------------------------------------------------------------------------
# use this code for checking bin sizes and range
#---------------------------------------------------------------------------------------------------
# # bin_min, bin_spc, bin_spc_log, nbin_log = 0.04, 0.02, 25., 40
# bin_min, bin_spc, bin_spc_log, nbin_log = 0.1, 0.02, 18., 60
# # bin_min, bin_spc, bin_spc_log, nbin_log = 0.105, 0.01, 10., 100  # Gabe's 2016 paper
# bin_log_wid = np.zeros(nbin_log)
# bin_log_ctr = np.zeros(nbin_log)
# bin_log_ctr[0] = bin_min
# bin_log_wid[0] = bin_spc
# for b in range(1,nbin_log):
#    bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
#    bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
# nbin = nbin_log
# bin_coord = xr.DataArray( bin_log_ctr )
# ratio = bin_log_wid / bin_log_ctr
# for b in range(nbin_log): print('  center: {:12.6f}      width: {:12.6f}     ratio: {:6.4f}'.format(bin_log_ctr[b],bin_log_wid[b],ratio[b]))
# exit()
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = []
res = hs.res_xy()
res.vpHeightF = 0.4
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.xyXStyle = "Log"

if 'clr' not in vars(): 
   clr = ['black']*num_case
   # if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   

if 'dsh' not in vars(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

res.xyLineColors   = clr
res.xyDashPatterns = dsh

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
frq_list = []
amt_list = []
bin_list = []

gpcp_cnt = 0

for c in range(num_case):

   print('    case: '+case[c])

   # if case[c]=='GPCP':
   #    if gpcp_cnt==0: data_dir,data_sub = os.getenv('HOME')+'/Data/Obs','daily'
   #    if gpcp_cnt==1: data_dir,data_sub = '/global/cfs/cdirs/m3312/jlee1046/GPCP','ne30pg2_regrid/daily'
   #    if gpcp_cnt==0: use_remap,remap_str = False,None
   #    if gpcp_cnt==1: use_remap,remap_str = True,'remap_ne30pg2'
   #    gpcp_cnt += 1
   # else:
   #    data_dir,data_sub = None,None
   #    use_remap,remap_str = False,None

   if case_dir[c] is not None: data_dir_tmp = case_dir[c]
   if case_sub[c] is not None: data_sub_tmp = case_sub[c]

   case_obj = he.Case( name=case[c], 
                       atm_comp='scream',
                       data_dir=data_dir_tmp, 
                       data_sub=data_sub_tmp )

   bin_tmp_file = f'{temp_dir}/scream.precip.bin.v1.{case[c]}.{var}'
   if 'lat1' in locals(): bin_tmp_file += f'.lat1_{lat1}.lat2_{lat2}.nc'
   print('    bin_tmp_file: '+bin_tmp_file )

   if recalculate :
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in vars() : case_obj.lat1, case_obj.lat1 = lat1, lat1
      if 'lat2' in vars() : case_obj.lat2, case_obj.lat2 = lat2, lat2
      
      area = case_obj.load_data("area",htype=htype,num_files=1)

      if var=='PRECT':
         tvar1 = 'precip_ice_surf_mass'
         tvar2 = 'precip_liq_surf_mass'
         data1 = case_obj.load_data(tvar1,htype=htype,years=years,months=months,num_files=num_files)
         data2 = case_obj.load_data(tvar2,htype=htype,years=years,months=months,num_files=num_files)
         data = (data1+data2)/600*86400 # convert kg/m2 to mm/day using dtime=600
      else:
         data = case_obj.load_data(var,htype=htype,years=years,months=months,num_files=num_files)

      # data['time'] = pd.to_datetime(data['time'].values, unit='D')

      # convert to daily mean
      data = data.resample(time='D').mean(dim='time')

      hc.print_stat(data,name=f'\n{var} (daily mean)',indent=' '*4,compact=True)

      wgt, *__ = xr.broadcast(area, data) 
      gbl_mean = ( (data*wgt).sum() / wgt.sum() ).values 
      #-------------------------------------------------------------------------
      # Calculate the distribution
      #-------------------------------------------------------------------------
      # if var=='PRECT':
      #    bin_min_ctr = 0.04
      #    bin_min_wid = 0.02
      #    bin_spc_pct = 25.
      #    nbin = 40

      if var=='PRECT':
         bin_min_ctr = 0.1
         bin_min_wid = 0.02
         bin_spc_pct = 18.
         nbin = 60

      #-------------------------------------------------------------------------
      # Recreate bin_YbyX here for log bin case
      #-------------------------------------------------------------------------
      bin_min=bin_min_ctr
      bin_spc=bin_min_wid
      bin_spc_log=bin_spc_pct
      #----------------------------------------------------
      # set up bins
      bin_log_wid = np.zeros(nbin)
      bin_log_ctr = np.zeros(nbin)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      bin_coord = xr.DataArray( bin_log_ctr )

      #----------------------------------------------------
      # create output data arrays
      ntime = len(data['time']) if 'time' in data.dims else 1   
      
      shape,dims,coord = (nbin,),'bins',[('bins', bin_coord)]

      bin_cnt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
      bin_amt = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )
      bin_frq = xr.DataArray( np.zeros(shape,dtype=data.dtype), coords=coord, dims=dims )

      condition = xr.DataArray( np.full(data.shape,False,dtype=bool), coords=data.coords )

      wgt, *__ = xr.broadcast(area,data)
      wgt = wgt.transpose()

      data_area_wgt = (data*wgt) / wgt.sum()

      ones_area_wgt = xr.DataArray( np.ones(data.shape), coords=data.coords )
      ones_area_wgt = ( ones_area_wgt*wgt ) / wgt.sum()

      #----------------------------------------------------
      # Loop through bins
      for b in range(nbin):
         bin_bot = bin_log_ctr[b] - bin_log_wid[b]/2.
         bin_top = bin_log_ctr[b] + bin_log_wid[b]/2.
         condition.values = ( data.values >=bin_bot )  &  ( data.values < bin_top )
         bin_cnt[b] = condition.sum()
         if bin_cnt[b]>0 :
            bin_frq[b] = ones_area_wgt.where(condition,drop=True).sum() * 1e2 / (bin_spc_pct/1e2)
            bin_amt[b] = data_area_wgt.where(condition,drop=True).sum() / (bin_spc_pct/1e2)

      #----------------------------------------------------
      # use a dataset to hold all the output
      dims = ('bins',)
      bin_ds = xr.Dataset()
      bin_ds['bin_amt'] = (dims, bin_amt )
      bin_ds['bin_frq'] = (dims, bin_frq )
      bin_ds['bin_cnt'] = (dims, bin_cnt )
      bin_ds['bin_pct'] = (dims, bin_cnt/bin_cnt.sum()*1e2 )
      # bin_ds['bin_cnt'] = (dims, bin_cnt.sum(dim='ncol') )
      # bin_ds['bin_pct'] = (dims, bin_cnt.sum(dim='ncol')/bin_cnt.sum()*1e2 )
      bin_ds.coords['bins'] = ('bins',bin_coord)

      bin_ds['gbl_mean'] = gbl_mean

      #-------------------------------------------------------------------------
      # Write to file 
      #-------------------------------------------------------------------------
      if os.path.isfile(bin_tmp_file) : os.remove(bin_tmp_file)
      bin_ds.to_netcdf(path=bin_tmp_file,mode='w')
   else:
      bin_ds = xr.open_dataset( bin_tmp_file )
      bin_cnt = bin_ds['bin_cnt']
      bin_amt = bin_ds['bin_amt']
      bin_frq = bin_ds['bin_frq']
      bin_coord = bin_ds['bins']
   #-------------------------------------------------------------------------
   #-------------------------------------------------------------------------
   frequency = np.ma.masked_invalid( bin_frq.values )
   amount    = np.ma.masked_invalid( bin_amt.values )
   
   if 'gbl_mean' in locals():
      print('\n'+f'    Area weighted global mean:  {gbl_mean} ')
      print(     f'    sum of rain amount dist:    {np.sum(amount*(bin_spc_pct/1e2))} \n')

   frq_list.append( frequency    )
   amt_list.append( amount )
   bin_list.append( bin_coord )
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
unit_str = ''
# if var in ['PRECT','PRECC','PRECL']   : unit_str = '[mm/day]'
# res.tiXAxisString = unit_str
res.tiXAxisString = 'Rain Rate [mm/day]'

var_str = var
if var=="PRECT"      : var_str = "Precipitation"

#-------------------------------------------------------------------------------
# Create plot
#-------------------------------------------------------------------------------

res.xyExplicitLegendLabels = case_name
res.pmLegendDisplayMode    = "Always"
res.pmLegendOrthogonalPosF = -1.13
res.pmLegendParallelPosF   =  0.8+0.04
res.pmLegendWidthF         =  0.16
res.pmLegendHeightF        =  0.12   
res.lgBoxMinorExtentF      =  0.16   

res.trXMinF = np.min(bin_list)
res.trXMaxF = np.max(bin_list)

### plot frequency
res.tiYAxisString = 'Frequency [%]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(frq_list) ,res)  )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)


### plot rain amount
res.pmLegendDisplayMode = "Never"
res.tiYAxisString = 'Rain Amount [mm/day]'
plot.append( ngl.xy(wks, np.stack(bin_list) , np.stack(amt_list) ,res) )
# hs.set_subtitles(wks, plot[len(plot)-1], '', '', var_str, font_height=0.015)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# hs.set_plot_labels(wks, plot, font_height=0.01, justify='left')

layout = [1,len(plot)]
ngl.panel(wks,plot[0:len(plot)],layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
