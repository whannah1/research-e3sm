import os, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#---------------------------------------------------------------------------------------------------
# var,vclr,vdsh = [],[],[]
# def add_var(var_name,clr='black',dsh=0): 
#    var.append(var_name); vclr.append(clr); vdsh.append(dsh)
#---------------------------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
def add_var(var_name,n='',file_type=None):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
#---------------------------------------------------------------------------------------------------

# lev = np.array([30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([50,100,150,200,300,400,500,600,700,750,800,850,875,900,925,975])
# lev = np.array([1,2,3,4,5,6,7,8,9,10,15,20,25,30,40,50,100,150,200])

### 2025 popcorn investigation
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne256pg2_scrip.nc'
# tmp_scratch = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'
tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
r1_str = 'cfr_1.acc_10.rsc_0.eci_1e-05.eri_1e-05.mti_7.4e7'
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2',               n='SCREAM ctrl',p=tmp_scratch,s='run')
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1',         n='SCREAM cfrc',p=tmp_scratch,s='run')
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r1_str}',      n='SCREAM rcp1',p=tmp_scratch,s='run')

#---------------------------------------------------------------------------------------------------

# add_var('T_mid')
# add_var('omega')
# add_var('horiz_winds')
# add_var('qv')
add_var('qc')
add_var('qi')
# add_var('cldfrac_tot')
# add_var('cldfrac_ice')
# add_var('cldfrac_liq')
# add_var('RelativeHumidity')




num_plot_col = len(var)
# num_plot_col = 2


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.profile.v1'


# lat1,lat2 = -30,30
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50

# xlat,xlon,dy,dx = 60,120,10,10;
# xlat,xlon,dy,dx = 0,0,10,10;
# xlat,xlon,dy,dx = 10,180,10,10;
# xlat,xlon,dx,dy = 36, 360-97, 2, 2 # ARM SGP

if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

# lat1,lat2 = -90,-70
# lat1,lat2,lon1,lon2 = -60,90,0,360


# htype,years,months,first_file,num_files = 'h2',[],[],0,30
# htype,years,months,first_file,num_files = 'h0',[],[3,4,5],0,5*12
# htype,years,months,first_file,num_files = 'h1',[],[],1,10
# htype,years,months,first_file,num_files = 'h2',[],[],1,10


plot_diff = False

overlay_vars = False

use_snapshot,ss_t  = False,1
use_height_coord   = False

omit_bot,bot_k     = False,-2
omit_top,top_k     = False,30

print_stats        = True
print_profile      = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'diff_mode' not in locals(): diff_mode = 0

if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
# plot = [None]*(num_var*num_case)
if overlay_vars: 
   plot = [None]*(num_case)
else:
   plot = [None]*(num_var)
res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

if use_height_coord: 
   res.tiYAxisString = 'Height [km]'
else:
   res.tiYAxisString = 'Pressure [hPa]'
   res.trYReverse = True
   # res.xyYStyle = 'Log'



# res.trYMinF = 500

# if use_height_coord:
   # res.trYMaxF = 30e3
   # res.trYMinF = 10e3
# else:
#    res.trYMaxF = 100

# if not use_height_coord: res.trYMinF = 800

if use_snapshot:
   print(hc.tcolor.RED+'WARNING - plotting snapshot!!!'+hc.tcolor.ENDC)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(hc.tcolor.GREEN+'  var: '+var[v]+hc.tcolor.ENDC)
   data_list,lev_list = [],[]
   for c in range(num_case):
      print(hc.tcolor.CYAN+'    case: '+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]

      case_obj = he.Case( name=case[c], 
                          atm_comp='scream',
                          data_dir=data_dir_tmp, 
                          data_sub=data_sub_tmp )

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tvar = var[v]
      # if tvar=='OMEGA' and 'pg2' in case[c] : tvar = 'DYN_OMEGA'

      area_name = 'area'
      
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      tnum_files = num_files

      if use_height_coord: 
         Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c])

      area = case_obj.load_data(area_name,htype=htype,years=years,months=months,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c]).astype(np.double)
      X    = case_obj.load_data(tvar,     htype=htype,years=years,months=months,first_file=first_file,num_files=tnum_files,use_remap=remap_flag[c],lev=lev)

      # print(); print(X); exit()

      hc.print_time_length(X.time,indent=' '*6)
      # if print_stats: hc.print_stat(X,name=f'{var[v]} before averaging',indent=' '*4,compact=True)


      if len(X.time)>1:
        if htype=='h0':
           dtime = ( X['time'][-1] - X['time'][0] + (X['time'][1]-X['time'][0]) ).values.astype('timedelta64[M]') + 1
           print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[D]'))+')')
        else:
           dtime = ( X['time'][-1] - X['time'][0] ).values.astype('timedelta64[D]')+1
           print('    Time length: '+str(dtime)+'  ('+str(dtime.astype('timedelta64[M]'))+')')

      if use_snapshot:
         X = X.isel(time=ss_t)
         if use_height_coord: Z = Z.isel(time=ss_t)
      else:
         X = X.mean(dim='time')
         if use_height_coord: Z = Z.mean(dim='time')

      if case[c]=='ERA5' or 'ncol' not in X.dims:
         X = ( (X*area).sum(dim=['lat','lon']) / area.sum(dim=['lat','lon']) )
      else:
         if use_height_coord: Z = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') )
         X = ( (X*area).sum(dim='ncol') / area.sum(dim='ncol') )

      # top_lev = 60
      # X = X[top_lev:]

      # if use_height_coord: Z = Z[top_lev:]
      # Z = Z[top_lev:]

      if omit_bot:
         X = X[:bot_k]
         if use_height_coord: Z = Z[:bot_k]

      if omit_top:
         X = X[top_k:]
         if use_height_coord: Z = Z[top_k:]

      if print_stats: hc.print_stat(X,name=f'{var[v]} after averaging',indent=' '*4,compact=True)

      if print_profile:
         print()
         for xx in X.values: print(f'    {xx}')
         print()

      # gbl_mean = ( (X*area).sum() / area.sum() ).values 
      # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')

      data_list.append( X.values )
      if use_height_coord:
         lev_list.append( Z.values )
      else:
         lev_list.append( X['lev'].values )

   data_list_list.append(data_list)
   lev_list_list.append(lev_list)

#-------------------------------------------------------------------------------
# Create plot 1 - overlay all vars for each case
#-------------------------------------------------------------------------------
if overlay_vars:
   for c in range(num_case):
      ip = c
      tres = copy.deepcopy(res)
      data_min = np.min( data_list_list[0][c] )
      data_max = np.max( data_list_list[0][c] )
      for v in range(num_var):
         data_min = np.min([ data_min, np.nanmin(data_list_list[v][c]) ])
         data_max = np.max([ data_max, np.nanmax(data_list_list[v][c]) ])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      
      for v in range(num_var):
         tres.xyLineColor   = vclr[v]
         tres.xyMarkerColor = vclr[v]
         tres.xyDashPattern = vdsh[v]
         tplot = ngl.xy(wks, data_list_list[v][c], lev_list_list[v][c], tres)

         if v==0 :
            plot[ip] = tplot
         else:
            ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''

      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      hs.set_subtitles(wks, plot[ip], case_name[c], ctr_str, '', font_height=0.01)
     
#-------------------------------------------------------------------------------
# Create plot 2 - overlay all cases for each var
#-------------------------------------------------------------------------------
if not overlay_vars:
   for v in range(num_var):
      data_list = data_list_list[v]
      lev_list = lev_list_list[v]
      
      tres = copy.deepcopy(res)

      # ip = v*num_case+c
      ip = c*num_var+v
      
      baseline = data_list[0]
      if diff_mode==1 :
         baseline1 = data_list[0]
         baseline2 = data_list[1]
      if diff_mode==2 :
         baseline1 = data_list[0]
         baseline2 = data_list[2]
      if plot_diff:
         for c in range(num_case): 
            if diff_mode==1 :
               if c==0 or c==2 : baseline = baseline1
               if c==1 or c==3 : baseline = baseline2
            if diff_mode==2 :
               if c==0 or c==1 : baseline = baseline1
               if c==2 or c==3 : baseline = baseline2
            data_list[c] = data_list[c] - baseline

      
      data_min = np.min([np.nanmin(d) for d in data_list])
      data_max = np.max([np.nanmax(d) for d in data_list])
      tres.trXMinF = data_min
      tres.trXMaxF = data_max
      ip = v

      for c in range(num_case):
         tres.xyLineColor   = clr[c]
         tres.xyMarkerColor = clr[c]
         tres.xyDashPattern = dsh[c]

         tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)

         if diff_mode==0 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c>0) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==1 :
            if (c==2 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=1) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

         if diff_mode==2 :
            if (c==1 and plot_diff) or (c==0 and not plot_diff) :
               plot[ip] = tplot
            elif (plot_diff and c!=0 and c!=2) or not plot_diff:
               ngl.overlay(plot[ip],tplot)

      ### add vertical line
      lres = hs.res_xy()
      lres.xyLineThicknessF = 1
      lres.xyDashPattern = 0
      lres.xyLineColor = 'black'
      ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))

      ctr_str = ''
      var_str = var[v]
      if var[v]=='CLOUD': var_str = 'Cloud Fraction'


      if 'lat1' in locals(): 
         lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
         lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
         ctr_str += f' {lat1_str}:{lat2_str} '
      if 'lon1' in locals(): 
         lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
         lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
         ctr_str += f' {lon1_str}:{lon2_str} '

      if plot_diff: var_str += ' (diff)'

      # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)

      hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str, font_height=0.01)


#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
if num_case>1:
   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.08
   lgres.lgLabelFontHeightF = 0.012
   lgres.lgMonoDashIndex    = True
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 8
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   lx,ly = 0.5,0.45
   if num_var==2: lx,ly = 0.3,0.45
   if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# layout = [1,len(plot)]
# layout = [int(np.ceil(num_var/4.)),4]
# layout = [num_var,num_case]
# layout = [num_case,num_var]
# layout = [1,num_var]

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]


# if num_var==1  : layout = [int(np.ceil(num_case/2.)),2]
# if num_var==1  : layout = [num_case,num_var]
# if num_case==1 : layout = [num_var,num_case]

# if num_var==1 and num_case==4 : layout = [2,2]
# if num_var==1 and num_case==6 : layout = [3,2]

# if num_case==1 and num_var==4 : layout = [2,2]
# if num_case==1 and num_var==6 : layout = [3,2]

#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
