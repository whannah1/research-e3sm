import os, ngl, glob, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#---------------------------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
htype_alt_list = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False,htype_alt=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
   htype_alt_list.append(htype_alt)
#---------------------------------------------------------------------------------------------------
# var,vclr,vdsh = [],[],[]
# def add_var(var_name,clr='black',dsh=0): 
#    var.append(var_name); vclr.append(clr); vdsh.append(dsh)
#---------------------------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
def add_var(var_name,n='',file_type=None):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
#---------------------------------------------------------------------------------------------------

### 2025 popcorn investigation
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne256pg2_scrip.nc'
# tmp_scratch = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'
tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
r01_str = 'cfr_1.acc_10.rsc_0.eci_1e-05.eri_1e-05.mti_7.4e7'
r04_str = 'cfr_1.acc_10.rsc_0.01.eci_0.01.eri_0.01.mti_7.4e7.acp_1.acq_3.acn_3.acr_1e-05'
r05_str = 'cfr_1.acc_10.rsc_0.01.eci_0.01.eri_0.01.mti_7.4e7.acp_0.5.acq_2.acn_2.acr_1e-05'

r06_str = 'cfr_1.acc_10.rsc_1e-05.eci_1e-05.eri_1e-05.mti_7.4e7.acp_0.5.acq_2.acn_2.acr_1e-05'
r07_str = 'cfr_1.acc_10.rsc_1e-05.eci_1e-05.eri_1e-05.mti_7.4e7.acp_0.5.acq_2.acn_2.acr_1e-05.isf_0.95'

r08_str = 'cfr_1.acc_10.rsc_1e-05.eci_1e-05.eri_1e-05.mti_7.4e7.acp_10.acq_3.acn_2.acr_5e-06'
r09_str = 'cfr_1.acc_10.rsc_1e-05.eci_1e-05.eri_1e-05.mti_7.4e7.acp_10.acq_3.acn_2.acr_5e-06.isf_0.99'
r10_str = 'cfr_1.acc_10.rsc_1e-05.eci_1e-05.eri_1e-05.mti_7.4e7.acp_10.acq_3.acn_2.acr_5e-06.isf_1.01'

r11_str = 'cfr_1.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_5e-06.isf_0.95'
r12_str = 'cfr_1.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_5e-06.isf_1'
r13_str = 'cfr_1.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_5e-06.isf_1.05'


# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r01_str}',      n='SCREAM rcp1',d=0,c='blue', p=tmp_scratch,s='run')

# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2',               n='SCREAM ctrl',d=1,c='black',p=tmp_scratch,s='run')
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1',         n='SCREAM cfrc',d=0,c='black',  p=tmp_scratch,s='run')

# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r04_str}',n='SCREAM R04',d=0,c='blue',   p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r05_str}',n='SCREAM R05',d=0,c='cyan', p=tmp_scratch,s='run',htype_alt=True)

# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r06_str}',n='SCREAM R06',d=0,c='darkgreen', p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r07_str}',n='SCREAM R07',d=0,c='palegreen', p=tmp_scratch,s='run',htype_alt=True)

# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r08_str}',n='SCREAM R08',d=0,c='magenta', p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r09_str}',n='SCREAM R09',d=0,c='pink', p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r10_str}',n='SCREAM R10',d=0,c='purple', p=tmp_scratch,s='run',htype_alt=True)

# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r11_str}',n='SCREAM R11',d=0,c='magenta',  p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r12_str}',n='SCREAM R12',d=0,c='pink',      p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r13_str}',n='SCREAM R13',d=0,c='purple',  p=tmp_scratch,s='run',htype_alt=True)

recipe14 = 'cfr_1.cfi_0.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_1'
recipe15 = 'cfr_1.cfi_0.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_0.9'
# recipe16 = 'cfr_1.cfi_1.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_0.9'
recipe17 = 'cfr_1.cfi_0.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_0.95'
# recipe18 = 'cfr_1.cfi_1.acc_60.rsc_5.eci_0.1.eri_0.1.mti_7.4e6.acp_100.acq_2.5.acn_1.5.acr_2e-05.isf_0.95'

add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe14}',n='SCREAM R14',d=0,c='red',  p=tmp_scratch,s='run',htype_alt=True)
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe15}',n='SCREAM R15',d=0,c='cyan',  p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe16}',n='SCREAM R16',d=0,c='cyan',  p=tmp_scratch,s='run',htype_alt=True)
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe17}',n='SCREAM R17',d=0,c='magenta',  p=tmp_scratch,s='run',htype_alt=True)
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{recipe18}',n='SCREAM R18',d=0,c='magenta',  p=tmp_scratch,s='run',htype_alt=True)



#---------------------------------------------------------------------------------------------------
# import glob, os
# case = 'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2'
# # case = 'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1'
# case_sub = 'run'
# case_dir = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
# file_path = f'{case_dir}/{case}/{case_sub}/output*'
# file_list = sorted(glob.glob(file_path))
# # print(); print(file_path)
# # print(); print(file_list)
# for f in file_list:
#    # print(f)
#    print(f)
#    print(f.replace('.mean.','.ne256pg2.'))
#    exit()
#---------------------------------------------------------------------------------------------------

# htype = 'output.scream.Cess.monthly_ne1024.AVERAGE.nmonths_x1'

# add_var('T_mid')
# add_var('omega')
# add_var('horiz_winds')
# add_var('qv')

# add_var('qc',                       n='qc',     file_type=htype)
# add_var('qi',                       n='qi',     file_type=htype)
# add_var('RelativeHumidity',         n='RH',     file_type=htype)
# add_var('cldfrac_tot_for_analysis', n='cld_tot',file_type=htype)
# add_var('cldfrac_ice_for_analysis', n='cld_ice',file_type=htype)
# add_var('cldfrac_liq',              n='cld_liq',file_type=htype)


# add_var('qci',                      n='Global Mean Qc+Qi',         file_type=htype)
# add_var('cldfrac_tot_for_analysis', n='Global Mean Cloud Fraction',file_type=htype)

tmp_file_type1 = 'output.scream.3D.1dy.mean.AVERAGE.ndays_x1'
tmp_file_type2 = 'output.scream.3D.1dy.ne256pg2.AVERAGE.ndays_x1'
# add_var('T_mid',           n='T', file_type=tmp_file_type)
# add_var('RelativeHumidity',n='RH',file_type=tmp_file_type)
add_var('qv',              n='qv',file_type=tmp_file_type1)
add_var('qc',              n='qc',file_type=tmp_file_type1)
add_var('qr',              n='qr',file_type=tmp_file_type1)
add_var('qi',              n='qi',file_type=tmp_file_type1)

# add_var('cldfrac_ice')
# add_var('cldfrac_liq')



first_file,num_files = 35,5
# first_file,num_files = 20,20

num_plot_col = len(var)
# num_plot_col = 2


#---------------------------------------------------------------------------------------------------
# lat1,lat2 = -30,30
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50

# xlat,xlon,dy,dx = 60,120,10,10;
# xlat,xlon,dy,dx = 0,0,10,10;
# xlat,xlon,dy,dx = 10,180,10,10;
# xlat,xlon,dx,dy = 36, 360-97, 2, 2 # ARM SGP
# if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

# lat1,lat2 = -90,-70
# lat1,lat2,lon1,lon2 = -60,90,0,360
#---------------------------------------------------------------------------------------------------


fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.profile.v2','png'

tmp_file_head = 'data_temp/scream.profile.v2'

recalculate = False

plot_diff   = False
use_height  = False
print_stats = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)
res = hs.res_xy()
# res.vpWidthF = 0.4
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
res.tmYLLabelFontHeightF = 0.015
res.tmXBLabelFontHeightF = 0.015
res.tiYAxisFontHeightF = 0.02
res.tiXAxisFontHeightF = 0.02

res.tmXBAutoPrecision = False
res.tmXBPrecision = 2

if use_height: 
   res.tiYAxisString = 'Height [km]'
else:
   res.tiYAxisString = 'Pressure [hPa]'
   res.trYReverse = True
   # res.xyYStyle = 'Log'


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_file_list(case,case_dir,case_sub,file_type):
   file_path = f'{case_dir}/{case}/{case_sub}/*{file_type}*'
   file_list = sorted(glob.glob(file_path))
   if 'first_file' in globals(): file_list = file_list[first_file:]
   if 'num_files' in globals(): file_list = file_list[:num_files]
   # print(); print(file_list); print(); exit()
   return file_list
#---------------------------------------------------------------------------------------------------
def get_data(ds,var):
   tvar = var
   if var=='qci'          : tvar = 'qc'
   if var=='horiz_winds_u': tvar = 'horiz_winds'
   if var=='horiz_winds_v': tvar = 'horiz_winds'
   data = ds[tvar]#.load()
   if var=='horiz_winds_u': data = data.isel(dim2=0)
   if var=='horiz_winds_v': data = data.isel(dim2=1)
   if var=='qci'          : data += ds['qi'] 
   if 'lev' in data.dims : data = interpolate_to_pressure(ds,data)
   return data
#---------------------------------------------------------------------------------------------------
lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,
               550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([1,2,4,6,10,30,50,100,150,200,300,400,500,600,700,800,850,925,950])
# lev = np.array([5,10,30,50,100,150,200,300,400,500,600,700,800,850,925,975,1000])
ncol_chunk_size = int(100e3)
def interpolate_to_pressure(ds,data_mlev,interp_type=2,extrap_flag=False):
   global lev,chunk_size
   hya = ds['hyam'].isel(time=0,missing_dims='ignore').values
   hyb = ds['hybm'].isel(time=0,missing_dims='ignore').values
   # Create empty array with new lev dim
   data_plev = xr.full_like( data_mlev.isel(lev=0,drop=True), np.nan )
   data_plev = data_plev.expand_dims(dim={'lev':lev}, axis=data_mlev.get_axis_num('lev'))
   # Add dummy dimension
   data_mlev = data_mlev.expand_dims(dim='dummy',axis=len(data_mlev.dims))

   PS_dum = ds['ps'].expand_dims(dim='dummy',axis=len(ds['ps'].dims))
   P0 = xr.DataArray(1e3)
   data_mlev = data_mlev.transpose('time','lev','ncol','dummy')
   data_plev = data_plev.transpose('time','lev','ncol')
   # Do the interpolation in chunks
   c1 = 0
   num_chunk = len(data_plev.chunksizes['ncol'])
   for c,sz in enumerate(data_plev.chunksizes['ncol']):
      c2 = c1+sz
      data_plev[:,:,c1:c2] = ngl.vinth2p( data_mlev[:,:,c1:c2,:].values, \
                                          hya, hyb, lev, PS_dum[:,c1:c2,:], \
                                          interp_type, P0, 1, extrap_flag)[:,:,:,0]
      c1 = c2
   data_plev = xr.DataArray( np.ma.masked_values( data_plev ,1e30), coords=data_plev.coords )
   return data_plev
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
data_list_list,lev_list_list = [],[]
for v in range(num_var):
   hc.printline()
   print(' '*2+f'var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
   #----------------------------------------------------------------------------
   data_list,lev_list = [],[]
   #----------------------------------------------------------------------------
   for c in range(num_case):
      tmp_file = f'{tmp_file_head}.{case[c]}.{var[v]}.f0_{first_file}.nf_{num_files}.nc'
      # print(' '*4+f'case: {hc.tclr.CYAN}{case[c]}{hc.tclr.END}')
      print(' '*4+f'case: {hc.tclr.CYAN}{case[c]}{hc.tclr.END}  =>  {tmp_file}')
      if recalculate:
         #----------------------------------------------------------------------
         tmp_file_type = file_type_list[v]
         if htype_alt_list[c]: tmp_file_type = tmp_file_type2
         file_list = get_file_list(case[c],case_dir[c],case_sub[c],tmp_file_type)
         ds = xr.open_mfdataset( file_list, chunks={'ncol':ncol_chunk_size} )
         lat  = get_data(ds,'lat').isel(time=0,missing_dims='ignore')
         area = get_data(ds,'area').isel(time=0,missing_dims='ignore')
         data = get_data(ds,var[v])
         #----------------------------------------------------------------------
         data = data.mean(dim='time', skipna=True)
         data = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
         #----------------------------------------------------------------------
         # if use_height: 
         #    hght = ???
         #    hght = ( (hght*area).sum(dim='ncol') / area.sum(dim='ncol') )
         #----------------------------------------------------------------------
         # Write to file 
         if os.path.isfile(tmp_file) : os.remove(tmp_file)
         tmp_ds = xr.Dataset()
         tmp_ds[var[v]] = data
         tmp_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         tmp_ds = xr.open_dataset( tmp_file )
         data = tmp_ds[var[v]]
      #-------------------------------------------------------------------------
      # adjust units
      if var[v]=='qci': data = data*1e3 # g/kg => kg/kg
      #-------------------------------------------------------------------------
      if print_stats: hc.print_stat(data,compact=True)
      #-------------------------------------------------------------------------
      data_list.append( data.values )
      if not use_height: lev_list.append( data['lev'].values )
      if     use_height: lev_list.append( hght.values )
   #----------------------------------------------------------------------------
   # Create plot

   ip = v
   
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   tres = copy.deepcopy(res)
   tres.trXMinF = data_min
   tres.trXMaxF = data_max + (data_max-data_min)*0.02

   if var[v]=='qci'      : tres.tiXAxisString = 'Mixing Ratio [g/kg]'
   if 'cldfrac' in var[v]: tres.tiXAxisString = 'Fraction'
   #----------------------------------------------------------------------------
   if plot_diff:
      baseline = copy.deepcopy(data_list[0])
      for c in range(num_case): 
         data_list[c] = data_list[c] - baseline
   #----------------------------------------------------------------------------
   for c in range(num_case):
      tres.xyLineColor   = clr[c]
      tres.xyMarkerColor = clr[c]
      tres.xyDashPattern = dsh[c]

      tplot = ngl.xy(wks, data_list[c], lev_list[c], tres)
   
      if (c==0 and not plot_diff) or (c>0 and plot_diff):
         plot[ip] = tplot
      else:
         ngl.overlay(plot[ip],tplot)
   #----------------------------------------------------------------------------
   # add vertical line
   lres = hs.res_xy()
   lres.xyLineThicknessF = 1
   lres.xyDashPattern = 0
   lres.xyLineColor = 'black'
   ngl.overlay(plot[ip],ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e8]), lres))
   #----------------------------------------------------------------------------
   # ctr_str = ''
   # var_str = var[v]
   # if var[v]=='CLOUD': var_str = 'Cloud Fraction'

   # if 'lat1' in locals(): 
   #    lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
   #    lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
   #    ctr_str += f' {lat1_str}:{lat2_str} '
   # if 'lon1' in locals(): 
   #    lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
   #    lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
   #    ctr_str += f' {lon1_str}:{lon2_str} '

   # if plot_diff: var_str += ' (diff)'

   # hs.set_subtitles(wks, plot[len(plot)-1], case_name, ctr_str, var_str, font_height=0.01)

   hs.set_subtitles(wks, plot[ip], '', var_str[v], '', font_height=0.01)


#-------------------------------------------------------------------------------
# Add legend

lgres = ngl.Resources()
lgres.vpWidthF           = 0.06
lgres.vpHeightF          = 0.10
lgres.lgLabelFontHeightF = 0.012
lgres.lgLabelFont        = "courier"
lgres.lgMonoDashIndex    = False
lgres.lgLineLabelsOn     = False
lgres.lgLineThicknessF   = 20
lgres.lgLabelJust        = 'CenterLeft'
lgres.lgLineColors       = clr
lgres.lgDashIndexes      = dsh

indent = ' '*2
labels = case_name
for i in range(len(labels)): 
   labels[i] = indent+labels[i] 

# pid = ngl.legend_ndc(wks, len(labels), labels, 0.82, 0.65, lgres)
# pid = ngl.legend_ndc(wks, len(labels), labels, 0.3, 0.47, lgres)

# # use 2 separate legends to separate groups
# lgres.vpHeightF     = 0.05
# lgres.lgLineColors  = clr[0:2]
# lgres.lgDashIndexes = dsh[0:2]
# pid = ngl.legend_ndc(wks, 2, labels[0:2], 0.3, 0.48, lgres)
# lgres.lgLineColors  = clr[2:4]
# lgres.lgDashIndexes = dsh[2:4]
# pid = ngl.legend_ndc(wks, 2, labels[2:4], 0.3, 0.42, lgres)



#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
#-- draw a common title string on top of the panel
textres               =  ngl.Resources()
# textres.txFontHeightF =  0.01                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot
textres.txFontHeightF =  0.02                  #-- title string size
if layout[0]==1: y_pos = 0.7
if layout[0]>=2: y_pos = 0.9
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,y_pos,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
