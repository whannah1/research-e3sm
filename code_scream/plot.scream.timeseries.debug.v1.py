import os, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#-------------------------------------------------------------------------------
var,vclr,vdsh = [],[],[]
def add_var(var_name,clr='black',dsh=0): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh)
#-------------------------------------------------------------------------------

### 2023 vertical grid tests
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v10',n='L128v1.0',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v21',n='L128v2.1',c='red',  p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v22',n='L128v2.2',c='blue', p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

add_case('SCREAM.2023-COLDT-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1',n='SCREAM ne256pg2',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-COLDT-00.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.RF_6',n='SCREAM ne256pg2',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

ivar = 'T_2m'

add_var('T_2m')
# add_var('surf_radiative_T')
# add_var('VapWaterPath')
# add_var('LiqWaterPath')
# add_var('IceWaterPath')
# add_var('precip_ice_surf_mass')
# add_var('precip_liq_surf_mass')
# add_var('cldtot')
# add_var('cldlow')
# add_var('cldmed')
# add_var('cldhgh')
# add_var('wind_speed_10m')
# add_var('surf_evap')
# add_var('surf_sens_flux')
# add_var('SW_flux_up@tom')
# add_var('SW_flux_dn@tom')
# add_var('LW_flux_up@tom')
# add_var('LW_flux_dn@tom')
# add_var('LW_flux_up@bot')
# add_var('LW_flux_dn@bot')




# num_plot_col = len(var)
num_plot_col = 1


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.timeseries.debug.v1'


# lat1,lat2 = -30,30
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50
# xlat,xlon,dy,dx = 60,120,10,10;
# if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx


# htype,years,months,first_file,num_files = 'h1',[],[],10,2
# htype,years,months,first_file,num_files = 'h1',[],[],28,2
htype,years,months,first_file,num_files = 'h1',[],[],10,2

plot_diff   = False

print_stats = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)

res = hs.res_xy()
res.vpHeightF = 0.1
# res.tmXBAutoPrecision = False
# res.tmXBPrecision = 2
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
icol_list = []
icol_msg_list = []
for v in range(num_var):
   hc.printline()
   print(' '*2+f'{hc.tcolor.GREEN}var: {var[v]}{hc.tcolor.ENDC}')
   time_list = []
   data_list = []
   for c in range(num_case):
      print(' '*4+f'{hc.tcolor.CYAN}case: {case[c]}{hc.tcolor.ENDC}')

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      case_obj = he.Case( name=case[c], atm_comp='scream',
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if v==0:
         data = case_obj.load_data(ivar,htype=htype,first_file=first_file,num_files=num_files,lev=lev)
         lat = case_obj.load_data('lat',htype=htype).values
         lon = case_obj.load_data('lon',htype=htype).values
         tmp_idx = data.argmin().values
         (itime,icol) = np.unravel_index( tmp_idx, data.shape )
         msg = ''
         msg += f'   icol: {icol}'
         msg += f'   lat: {lat[icol]:6.2f}'
         msg += f'   lon: {lon[icol]:6.2f}'
         if 'itime' in locals():
            msg += f'   itime: {itime}'
            msg += f'   min({var[v]}): {data[itime,icol].values}'
         # print(f'\n{msg}\n')
         icol_msg_list.append(msg)
         icol_list.append(icol)
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   

      data = case_obj.load_data(var[v],htype=htype,first_file=first_file,num_files=num_files,lev=lev)
      data = data.isel(ncol=icol_list[c])

      if print_stats: hc.print_stat(data,name=' '*6+f'\n{var[v]}',indent=' '*6,compact=True)

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      # time = data.time ; time = ( time - time[0] ).astype('float').values / 86400e9
      time = data.time.dt.day \
            +data.time.dt.hour   /(24) \
            +data.time.dt.minute /(24*60) \
            +data.time.dt.second /(24*60*60)

      time_list.append( time.values )
      data_list.append( data.values )

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.xyLineColors  = clr
   tres.tiXAxisString = 'Time [days]'

   tres.trYMinF = np.min([np.nanmin(d) for d in data_list])
   tres.trYMaxF = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
   tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])

   plot[v] = ngl.xy(wks, np.stack(time_list), np.stack(data_list), tres)

   hs.set_subtitles(wks, plot[v], '', '', var[v], font_height=0.01)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
for c in range(num_case):
   print(' '*4+f'{hc.tcolor.CYAN}case: {case[c]}{hc.tcolor.ENDC}')
   print(f'\n{icol_msg_list[c]}\n')
#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
# if num_case>1:
#    lgres = ngl.Resources()
#    lgres.vpWidthF           = 0.05
#    lgres.vpHeightF          = 0.08
#    lgres.lgLabelFontHeightF = 0.012
#    lgres.lgMonoDashIndex    = True
#    lgres.lgLineLabelsOn     = False
#    lgres.lgLineThicknessF   = 8
#    lgres.lgLabelJust        = 'CenterLeft'
#    lgres.lgLineColors       = clr
#    lgres.lgDashIndexes      = dsh

#    lx,ly = 0.5,0.45
#    if num_var==2: lx,ly = 0.3,0.45
#    if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
