import os, glob, ngl, copy, xarray as xr, numpy as np, cmocean, dask
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#-------------------------------------------------------------------------------
var,vclr,vdsh,htype_list = [],[],[],[]
cvar_list = []
def add_var(var_name,clr='black',dsh=0,htype='h1',cvar=None): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh); htype_list.append(htype)
   cvar_list.append(cvar)
#-------------------------------------------------------------------------------

### 2023 vertical grid tests
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v10',n='L128v1.0',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v21',n='L128v2.1',c='red',  p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v22',n='L128v2.2',c='blue', p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

# add_case('SCREAM.2023-COLDT-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1',n='SCREAM ne256pg2',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
add_case('SCREAM.2023-COLDT-00.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.RF_6',n='SCREAM ne256pg2',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

ivar,ivar_htype = 'T_2m','h1'

# add_var('T_2m',htype='h1')
# add_var('surf_radiative_T')
# add_var('VapWaterPath')
# add_var('LiqWaterPath')
# add_var('IceWaterPath')
# add_var('precip_ice_surf_mass')
# add_var('precip_liq_surf_mass')
# add_var('cldtot')
# add_var('cldlow')
# add_var('cldmed')
# add_var('cldhgh')
# add_var('wind_speed_10m')
# add_var('surf_evap')
# add_var('surf_sens_flux')
# add_var('SW_flux_up@tom')
# add_var('SW_flux_dn@tom')
# add_var('LW_flux_up@tom')
# add_var('LW_flux_dn@tom')
# add_var('LW_flux_up@bot')
# add_var('LW_flux_dn@bot')


# add_var('T_mid',                 htype='h2')
add_var('rad_heating_pdel',      htype='h2',cvar='qc')
# add_var('omega',                 htype='h2')
# add_var('qv',                    htype='h2')
# add_var('qc',                    htype='h2')
# add_var('qi',                    htype='h2')

# add_var('horiz_winds',           htype='h2')
# add_var('VerticalLayerMidpoint', htype='h2')



# num_plot_col = len(var)
num_plot_col = 1


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.timeseries.profile.v2'


# lat1,lat2 = -30,30
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50
# xlat,xlon,dy,dx = 60,120,10,10;
# if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx

# h1_first_file,h2_first_file,num_files = 11,1,1
h1_first_file,h2_first_file,num_files = 10,0,2

plot_diff   = False

print_stats = True

extract_col_data = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)

xyres = hs.res_xy()
xyres.vpHeightF = 0.3
xyres.xyMarkerSizeF = 0.008
xyres.xyMarker = 16
xyres.xyLineThicknessF = 8


cnres = hs.res_contour_fill()
cnres.vpHeightF = 0.3
cnres.lbLabelFontHeightF = 0.01
cnres.cnFillPalette = "MPL_viridis"
cnres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
cnres.nglYAxisType = "LinearAxis"
# cnres.trYReverse = True


cnres.trYMaxF = 0.25


cnres.trXMinF,cnres.trXMaxF = 12.1,12.45
xyres.trXMinF,xyres.trXMaxF = 12.1,12.45

ctres = hs.res_contour()
ctres.trYMaxF,ctres.trXMinF,ctres.trXMaxF = cnres.trYMaxF,cnres.trXMinF,cnres.trXMaxF

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

icol = 1507625

tmp_file_list = []
for c in range(num_case):
   tmp_dir  = os.getenv('HOME')+f'/Research/E3SM/data_temp/'
   tmp_file = f'{tmp_dir}/scream.timeseries.profile.v2.{case[c]}.icol_{icol}'
   tmp_file_list.append(tmp_file)


if extract_col_data:
   for c in range(num_case):
      tmp_file = tmp_file_list[c]
      h1_file_path = f'/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/{case[c]}/run/{case[c]}.scream.h1.*'
      h2_file_path = f'/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/{case[c]}/run/{case[c]}.scream.h2.*'
      #-------------------------------------------------------------------------
      # get h1 files
      file_list = sorted(glob.glob(h1_file_path))
      print()
      print(f'file_path:{h1_file_path}')
      print(f'file_list:')
      print(file_list)
      print()
      if num_files==0 and h1_first_file>0: file_list = file_list[h1_first_file:]
      file_list = file_list[h1_first_file:h1_first_file+num_files]     # use initial files
      ds_h1 = xr.open_mfdataset( file_list )
      #-------------------------------------------------------------------------
      # get h2 files
      file_list = sorted(glob.glob(h2_file_path))
      print()
      print(f'file_path:{h2_file_path}')
      print(f'file_list:')
      print(file_list)
      print()
      if num_files==0 and h2_first_file>0: file_list = file_list[h2_first_file:]
      if num_files>0 : file_list = file_list[h2_first_file:h2_first_file+num_files]     # use initial files
      ds_h2 = xr.open_mfdataset( file_list )
      #-------------------------------------------------------------------------
      # select column of interest
      ds_h1 = ds_h1.isel(ncol=icol)
      ds_h2 = ds_h2.isel(ncol=icol)
      #-------------------------------------------------------------------------
      # combine data and write to new file
      icol_ds = xr.merge([ ds_h1, ds_h2 ])
      icol_ds.to_netcdf(path=tmp_file,mode='w')
      ds_h1.close(); ds_h2.close(); icol_ds.close()
      #-------------------------------------------------------------------------

print(f'\n  tmp_file: {tmp_file}\n')
# exit()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
icol_list = []
icol_msg_list = []
for v in range(num_var):
   hc.printline()
   print(' '*2+f'{hc.tcolor.GREEN}var: {var[v]}{hc.tcolor.ENDC}')
   time_list = []
   data_list = []
   lev_list  = []
   cdata_list = []
   for c in range(num_case):
      print(' '*4+f'{hc.tcolor.CYAN}case: {case[c]}{hc.tcolor.ENDC}')

      # load data variable - avoid creating large chunks
      with dask.config.set(**{'array.slicing.split_large_chunks': True}):
         ds = xr.open_mfdataset( tmp_file_list[c] )
         print(ds['lat'].values)
         print(ds['lon'].values)
         exit()
         data = ds[var[v]]
         data.load()

         #----------------------------------------------------------------------
         if var[v]=='rad_heating_pdel':
            hyai,hybi,ps,p0 = ds['hyai'],ds['hybi'],ds['ps'],1e5
            pint = hyai*p0 + hybi*ps
            pint.compute()
            pdel = pint.isel(ilev=slice(1,129)).values \
                  -pint.isel(ilev=slice(0,128)).values
            pdel = xr.DataArray( pdel ,coords=data.coords)
            data = data/pdel.values * 86400.
            
            hc.print_stat(ps  ,name='ps',indent=' '*6,compact=True)
            hc.print_stat(pdel,name='pdel',indent=' '*6,compact=True)
            exit()
         #----------------------------------------------------------------------

         if print_stats: hc.print_stat(data,name=' '*6+f'\n{var[v]}',indent=' '*6,compact=True)
         
         time = data.time.dt.day \
               +data.time.dt.hour   /(24) \
               +data.time.dt.minute /(24*60) \
               +data.time.dt.second /(24*60*60)
         time_list.append( time.values )

         # if 'lev' in data.dims: data = data.isel(lev=0)
         # print(); print(time.values)
         # print(); print(data.values)
         # print(); print(data)

         if 'lev' in data.dims:
            k1,k2 = 80,127
            data_list.append( data.isel(lev=slice(k1,k2)).transpose().values )
            Z = ds['VerticalLayerMidpoint'].mean(dim='time')/1e3
            lev_list.append( Z.isel(lev=slice(k1,k2)).values )

            if cvar_list[v] is not None:
               cdata = ds[cvar_list[v]]
               cdata.load()
               cdata_list.append(cdata.isel(lev=slice(k1,k2)).transpose().values)
            else:
               cdata_list.append(None)
         else:
            data_list.append( data.values )
            lev_list.append( None )

         data.close()

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   if htype_list[v]=='h1':
      tres = copy.deepcopy(xyres)
      tres.xyLineColors  = clr
      tres.tiXAxisString = 'Time [days]'
      # tres.trYMinF = np.min([np.nanmin(d) for d in data_list])
      # tres.trYMaxF = np.max([np.nanmax(d) for d in data_list])
      # tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
      # tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])
      plot[v] = ngl.xy(wks, np.stack(time_list), np.stack(data_list), tres)

   if htype_list[v]=='h2':
      tres = copy.deepcopy(cnres)
      tres.lbLabelBarOn = True
      tres.sfYArray = lev_list[c]
      tres.sfXArray = time_list[c]
      tres.tiXAxisString = 'Time [days]'
      tres.tiYAxisString = 'Height [km]'
      plot[v] = ngl.contour(wks, data_list[c], tres)

      if cvar_list[v] is not None:
         # if cdata_list[c] is not None:
         tres = copy.deepcopy(ctres)
         tres.sfYArray = lev_list[c]
         tres.sfXArray = time_list[c]
         ngl.overlay(plot[v], ngl.contour(wks, cdata_list[c], tres))

   hs.set_subtitles(wks, plot[v], '', '', var[v], font_height=0.01)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
for c in range(num_case):
   print(' '*4+f'{hc.tcolor.CYAN}case: {case[c]}{hc.tcolor.ENDC}')
   # print(f'\n{icol_msg_list[c]}\n')
#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
# if num_case>1:
#    lgres = ngl.Resources()
#    lgres.vpWidthF           = 0.05
#    lgres.vpHeightF          = 0.08
#    lgres.lgLabelFontHeightF = 0.012
#    lgres.lgMonoDashIndex    = True
#    lgres.lgLineLabelsOn     = False
#    lgres.lgLineThicknessF   = 8
#    lgres.lgLabelJust        = 'CenterLeft'
#    lgres.lgLineColors       = clr
#    lgres.lgDashIndexes      = dsh

#    lx,ly = 0.5,0.45
#    if num_var==2: lx,ly = 0.3,0.45
#    if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
