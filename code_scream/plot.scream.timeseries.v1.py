import os, ngl, copy, xarray as xr, numpy as np
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
remap_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,c='black',m=0,r=False):
   global name,case,case_dir,case_sub,clr,dsh,mrk
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); case_name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   remap_flag.append(r)
#-------------------------------------------------------------------------------
var,vclr,vdsh,dt_flag = [],[],[],[]
def add_var(var_name,clr='black',dsh=0,dt=False): 
   var.append(var_name); vclr.append(clr); vdsh.append(dsh); dt_flag.append(dt)
#-------------------------------------------------------------------------------

### 2023 vertical grid tests
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v10',n='L128v1.0',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v21',n='L128v2.1',c='red',  p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-VGRID-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.L128v22',n='L128v2.2',c='blue', p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

# add_case('SCREAM.2023-COLDT-test.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1',n='SCREAM ne256pg2',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-COLDT-00.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.RF_6',n='SCREAM ne256pg2',c='black',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-COLDT-01.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.RF_6',n='SCREAM ne256pg2',c='orange',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

# add_case('SCREAM.2023-COLDT-00.GNUGPU.ne120pg2_ne120pg2.F2010-SCREAMv1.RF_1',n='SCREAM ne120pg2 RF 1',c='blue',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-COLDT-00.GNUGPU.ne120pg2_ne120pg2.F2010-SCREAMv1.RF_6',n='SCREAM ne120pg2 RF 6',c='red', p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

# add_case('SCREAM.2023-FRICTION-00.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.TFAC_1', n='SCREAM ne256 TFAC_1', c='red', p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')
# add_case('SCREAM.2023-FRICTION-00.GNUGPU.ne256pg2_ne256pg2.F2010-SCREAMv1.TFAC_10',n='SCREAM ne256 TFAC_10',c='blue',p='/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch',s='run')

# tmp_data_root = '/lustre/orion/cli115/proj-shared/noel/e3sm_scratch/s03-jan17'
# add_case('tcess-control.ne1024pg2_ne1024pg2.F2010-SCREAMv1.s03-jan17.n0320t08x1.nr.nohist.allyaml.S0.cfix',n='',c='red' ,d=0,p=tmp_data_root,s='run') # /lustre/orion/cli115/proj-shared/noel/e3sm_scratch/s03-jan17/tcess-control.ne1024pg2_ne1024pg2.F2010-SCREAMv1.s03-jan17.n0320t08x1.nr.nohist.allyaml.S0.cfix
# add_case('tcess-control.ne1024pg2_ne1024pg2.F2010-SCREAMv1.s03-jan17.n0320t16x1.nr.nohist.allyaml.S0.cfix',n='',c='blue',d=1,p=tmp_data_root,s='run') # /lustre/orion/cli115/proj-shared/noel/e3sm_scratch/s03-jan17/tcess-control.ne1024pg2_ne1024pg2.F2010-SCREAMv1.s03-jan17.n0320t16x1.nr.nohist.allyaml.S0.cfix

scrip_file_path = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne1024pg2.nc'
tmp_scratch='/lustre/orion/cli115/proj-shared/hannah6/scream_scratch'
# add_case('SCREAM.2024-autocal-spinup.ne1024pg2_ne1024pg2.F2010-SCREAMv1.NN_2048.NDG_OFF',            n='NDG OFF',     c='gray',  p=tmp_scratch,s='run')
# add_case('SCREAM.2024-autocal-spinup.ne1024pg2_ne1024pg2.F2010-SCREAMv1.NN_2048.NDG_ON.NDGTAU_3600', n='NDGTAU_3600', c='red',   p=tmp_scratch,s='run')
# add_case('SCREAM.2024-autocal-spinup.ne1024pg2_ne1024pg2.F2010-SCREAMv1.NN_2048.NDG_ON.NDGTAU_21600',n='NDGTAU_21600',c='green', p=tmp_scratch,s='run')
add_case('SCREAM.2024-autocal-spinup.ne1024pg2_ne1024pg2.F2010-SCREAMv1.NN_2048.NDG_ON.NDGTAU_43200',n='NDGTAU_43200',c='blue',  p=tmp_scratch,s='run')


# add_var('ps')
# add_var('surf_evap')
# add_var('SW_flux_dn_at_model_top')
# add_var('LW_flux_up_at_model_top')
add_var('T_2m')
# add_var('T_2m',dt=True)
# add_var('surf_radiative_T')
# add_var('LiqWaterPath')
# add_var('IceWaterPath')
# add_var('precip_total_surf_mass_flux')
# add_var('precip_ice_surf_mass_flux')
# add_var('precip_liq_surf_mass_flux')
# add_var('cldtot')
# add_var('cldlow')
# add_var('cldmed')
# add_var('cldhgh')
# add_var('wind_speed_10m')
# add_var('surf_evap')
# add_var('surf_sens_flux')
# add_var('SW_flux_up@tom')
# add_var('SW_flux_dn@tom')
# add_var('LW_flux_up@tom')
# add_var('LW_flux_dn@tom')
# add_var('SW_flux_up@bot')
# add_var('SW_flux_dn@bot')
# add_var('LW_flux_up@bot')
# add_var('LW_flux_dn@bot')






# num_plot_col = len(var)
num_plot_col = 2


fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.timeseries.v1'
# fig_file = os.getenv('HOME')+f'/Research/E3SM/figs_scream/scream.timeseries.v1.{case[0]}'


# lat1,lat2 = -30,30
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50
# xlat,xlon,dy,dx = 60,120,10,10;
# if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy,xlat+dy,xlon-dx,xlon+dx


htype,first_file,num_files = 'h1.AVERAGE',0,0
# htype,first_file,num_files = 'h1',3*3,3
# htype,first_file,num_files = 'h1',60,40


plot_diff   = False

# plot_min = True
plot_avg = True
# plot_max = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------

if 'plot_min' not in locals(): plot_min = False
if 'plot_avg' not in locals(): plot_avg = False
if 'plot_max' not in locals(): plot_max = False

num_var = len(var)
num_case = len(case)

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var)

res = hs.res_xy()
res.vpHeightF = 0.1
# res.tmXBAutoPrecision = False
# res.tmXBPrecision = 2
# res.xyMarkLineMode = "MarkLines"
res.xyMarkerSizeF = 0.008
res.xyMarker = 16
res.xyLineThicknessF = 8
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008


#---------------------------------------------------------------------------------------------------
def get_ctr_str(glb_avg=None):
   ctr_str = ''
   if 'lat1' in globals():
      lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
      lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
      ctr_str += f' {lat1_str}:{lat2_str} '
   if 'lon1' in globals():
      lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
      lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
      ctr_str += f' {lon1_str}:{lon2_str} '
   # if glb_avg is not None:
   #    # add logic here t display global average value?
   return ctr_str
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

for v in range(num_var):
   hc.printline()
   var_str = var[v]
   if dt_flag[v]: var_str = f'ddt {var[v]}'
   print(' '*2+f'{hc.tcolor.GREEN}var: {var[v]}{hc.tcolor.ENDC}')

   time_list = []
   data_min_list = []
   data_avg_list = []
   data_max_list = []
   for c in range(num_case):
      print(' '*4+f'{hc.tcolor.CYAN}case: {case[c]}{hc.tcolor.ENDC}')

      data_dir_tmp,data_sub_tmp = None, None
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      # case_obj = he.Case( name=case[c], atm_comp='scream',
      #                     data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      # if 'lat1' in vars() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in vars() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      # area = case_obj.load_data('area',htype=htype,first_file=first_file,num_files=num_files,use_remap=remap_flag[c]).astype(np.double)
      # data = case_obj.load_data(var[v],htype=htype,
      #                           first_file=first_file,
      #                           num_files=num_files,
      #                           use_remap=remap_flag[c],lev=lev)

      input_file_name = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/output.scream.Cess.3hourlyAVG_ne120.AVERAGE.nhours_x3.2019-08-01-00000.nc'
      print() ; print(input_file_name) ; print()
      # exit()
      ds = xr.open_dataset( input_file_name )
      data = ds[var[v]]

      # init_time = case_obj.load_data('time',htype=htype,first_file=0,num_files=1)[0]
      # day0 = init_time.time.dt.dayofyear
      time = data.time.dt.dayofyear \
            +data.time.dt.hour   /(24) \
            +data.time.dt.minute /(24*60) \
            +data.time.dt.second /(24*60*60)
      # time = time -(day0-1) # reset time to be zero at the start

      time_list.append( time )

      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      if dt_flag[v]:
         data = xr.DataArray( np.gradient(data.values,axis=0) ,coords=data.coords)
      
      if plot_min: data_min_list.append( data.min (dim='ncol').values )
      # if plot_avg: data_avg_list.append( data.mean(dim='ncol').values )
      if plot_avg: data_avg_list.append( data.isel(ncol=60000).values )
      if plot_max: data_max_list.append( data.max (dim='ncol').values )

      hc.print_stat(data,name=var[v],indent='\n'+' '*6,compact=True)
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tmp_idx = data.argmin().values
      (itime,icol) = np.unravel_index( tmp_idx, data.shape )

      # lat = case_obj.load_data('lat',htype=htype).values
      # lon = case_obj.load_data('lon',htype=htype).values

      msg = ' '*6
      msg += f'icol: {icol}'
      # msg += f'   lat: {lat[icol]:6.2f}'
      # msg += f'   lon: {lon[icol]:6.2f}'
      if 'itime' in locals():
         msg += f'   itime: {itime}'
         msg += f'   min({var[v]}): {data[itime,icol].values}'
      print(f'\n{msg}\n')

   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------
   tres = copy.deepcopy(res)
   tres.xyLineColors   = clr
   tres.xyDashPatterns = dsh
   tres.tiXAxisString  = 'Time [days]'

   tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
   tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])
   tres.trYMinF = np.min([np.nanmin(d) for d in data_avg_list])
   tres.trYMaxF = np.max([np.nanmax(d) for d in data_avg_list])

   if plot_min: tres.trYMinF = np.min([np.nanmin(d) for d in data_min_list])
   if plot_max: tres.trYMaxF = np.max([np.nanmax(d) for d in data_max_list])
   

   if plot_avg:
      tres.xyDashPattern = 0
      plot[v] = ngl.xy(wks, np.stack(time_list), np.stack(data_avg_list), tres)
      tres.xyDashPattern = 1
      if plot_min: ngl.overlay(plot[v], ngl.xy(wks, np.stack(time_list), np.stack(data_min_list), tres) )
      if plot_max: ngl.overlay(plot[v], ngl.xy(wks, np.stack(time_list), np.stack(data_max_list), tres) )
   # else: <= how to handle this case?





   # tres.trYMinF = np.min([np.nanmin(d) for d in data_min_list])
   # tres.trYMaxF = np.max([np.nanmax(d) for d in data_min_list])
   # tres.trXMinF = np.min([np.nanmin(d) for d in time_list])
   # tres.trXMaxF = np.max([np.nanmax(d) for d in time_list])
   # plot[v] = ngl.xy(wks, np.stack(time_list), np.stack(data_min_list), tres)

   if dt_flag[v]:
      rstr = f'ddt {var[v]}'
   else:
      rstr = f'{var[v]}'
   hs.set_subtitles(wks, plot[v], '', get_ctr_str(), rstr, font_height=0.01)

#-------------------------------------------------------------------------------
# Add legend
#-------------------------------------------------------------------------------
# if num_case>1:
#    lgres = ngl.Resources()
#    lgres.vpWidthF           = 0.05
#    lgres.vpHeightF          = 0.08
#    lgres.lgLabelFontHeightF = 0.012
#    lgres.lgMonoDashIndex    = True
#    lgres.lgLineLabelsOn     = False
#    lgres.lgLineThicknessF   = 8
#    lgres.lgLabelJust        = 'CenterLeft'
#    lgres.lgLineColors       = clr
#    lgres.lgDashIndexes      = dsh

#    lx,ly = 0.5,0.45
#    if num_var==2: lx,ly = 0.3,0.45
#    if num_var==4: lx,ly = 0.05,0.5

   # pid = ngl.legend_ndc(wks, len(case_name), case_name, lx, ly, lgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
