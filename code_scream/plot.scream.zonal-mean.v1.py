#-------------------------------------------------------------------------------
import os, ngl, copy, string, xarray as xr, numpy as np, glob, dask, numba, cmocean, subprocess as sp
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
data_dir,data_sub = None,None
#-------------------------------------------------------------------------------
case_name,case,case_dir,case_sub = [],[],[],[]
clr,dsh,mrk = [],[],[]
obs_flag = []
def add_case(case_in,n='',p=None,s='',g=None,d=0,c='black',m=0,r=False,obs=False):
   global case_name,case,case_dir,case_sub,clr,dsh,mrk
   case.append(case_in); case_name.append(n)
   case_dir.append(p); case_sub.append(s)
   dsh.append(d) ; clr.append(c) ; mrk.append(m)
   obs_flag.append(obs)
#-------------------------------------------------------------------------------
var = []
var_str = []
var_unit = []
file_type_list = []
obs_var_list = []
obs_file_type_list = []
lev_list = []
def add_var(var_name,file_type,obs_var=None,obs_file_type=None,name='',unit='',lev=None):
   var.append(var_name)
   file_type_list.append(file_type)
   obs_var_list.append(obs_var)
   obs_file_type_list.append(obs_file_type)
   var_str.append(name)
   var_unit.append(unit)
   lev_list.append(lev)
#-------------------------------------------------------------------------------
# scrip_file_sim = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/ne1024pg2_scrip.nc'
# scrip_file_obs = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/IMERG_1800x3600_scrip.nc'
# scrip_file_era = '~/HICCUP/files_grid/scrip_ERA5_721x1440.nc'
# sim_data_root  = '/global/cfs/cdirs/e3sm/gsharing/EAMxx'
# obs_data_root  = '/pscratch/sd/w/whannah/Obs'

# scrip_file_sim = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne1024pg2.nc'
# scrip_file_sim = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne30pg2.nc'
# scrip_file_obs = '~/HICCUP/data_scratch/scrip_ERA5_721x1440.nc'
# obs_data_root  = '/lustre/orion/cli115/proj-shared/hannah6/obs_data'

### SCREAMv1 decadal
# tmp_scratch = f'/lustre/orion/cli115/proj-shared/brhillman/e3sm_scratch'
# add_case('decadal-production-20240305.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.run1', p=tmp_scratch,s='run')


### 2025 popcorn investigation
scrip_file_sim = os.getenv('HOME')+'/E3SM/data_grid/ne256pg2_scrip.nc'
# tmp_scratch = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'
tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
r1_str = 'cfr_1.acc_10.rsc_0.eci_1e-05.eri_1e-05.mti_7.4e7'
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2',               n='SCREAM ctrl',d=1,c='black',p=tmp_scratch,s='run')
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1',         n='SCREAM cfrc',d=0,c='red',  p=tmp_scratch,s='run')
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r1_str}',      n='SCREAM rcp1',d=0,c='blue', p=tmp_scratch,s='run')


# add_case('ERA5',n='ERA5', p=obs_data_root,s='daily',obs=True,d=0,c='black')
# add_case('IMERG',n='IMERG Jan 2020',p=obs_data_root,s='daily_QC_Jan_2020',obs=True,d=1,c='red')

#-------------------------------------------------------------------------------
'''
NE=256
GRID_FILE_PATH=/pscratch/sd/w/whannah/e3sm_scratch/files_grid
GenerateCSMesh --alt --res ${NE} --file ${GRID_FILE_PATH}/ne${NE}.g
GenerateVolumetricMesh --in ${GRID_FILE_PATH}/ne${NE}.g --out ${GRID_FILE_PATH}/ne${NE}pg2.g --np 2 --uniform
ConvertMeshToSCRIP --in ${GRID_FILE_PATH}/ne${NE}pg2.g --out ${GRID_FILE_PATH}/ne${NE}pg2_scrip.nc
'''
#-------------------------------------------------------------------------------

# scrip_file_sim = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne30pg2.nc'
# scrip_file_sim = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/ne1024pg2_scrip.nc'
# scrip_file_sim = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/ne256pg2_scrip.nc'

# tmp_file_type = 'output.scream.AutoCal.hourly_inst_ne30pg2.INSTANT.nhours_x1.'
# tmp_file_type = 'output.scream.Cess.monthly_ne1024.AVERAGE.nmonths_x1'
tmp_file_type = 'output.scream.2D.1hr.inst'

first_file,num_files = 10,30 # last 3 months

add_var('precip_total_surf_mass_flux', tmp_file_type, name='precip')
add_var('LW_flux_up_at_model_top',     tmp_file_type, name='OLR')
add_var('SW_flux_up_at_model_top',     tmp_file_type, name='SW_flux_up_at_model_top')

add_var('VapWaterPath',                tmp_file_type, name='VapWP')
add_var('LiqWaterPath',                tmp_file_type, name='LiqWP')
add_var('IceWaterPath',                tmp_file_type, name='IceWP')

# add_var('T_mid',                       tmp_file_type, name='T500',lev=-21)
# add_var('T_mid',                       tmp_file_type, name='T850',lev=-30)
# add_var('T_2m',                        tmp_file_type,obs_var='t2m', obs_file_type='daily.sfc', name='2m Temperature')

# add_var('U',                           tmp_file_type,name='U100',lev=-10)
# add_var('U',                           tmp_file_type,name='U500',lev=-21)
# add_var('U',                           tmp_file_type,name='U850',lev=-30)

#-------------------------------------------------------------------------------
# set lat bounds for regional subsets

# lat1,lat2 = -30,30 # tropical belt

# xlat,xlon,dy,dx =  0,180,10,10;
# xlat,xlon,dy,dx = 40,180,5,5;
# xlat,xlon,dy,dx = 80,180,5,5;
# xlat,xlon,dy,dx = 15,360-40,10,10;
# xlat,xlon,dy,dx = 60,60,1,1;
# xlat,xlon,dy,dx = 60,60,5,5;
# xlat,xlon,dy,dx = 60,60,10,10;

# xlat,xlon,dy,dx = 0,75,1,1;
# xlat,xlon,dy,dx = 0,75,20,20;

if 'xlat' in locals(): lat1,lat2,lon1,lon2 = xlat-dy/2,xlat+dy/2,xlon-dx/2,xlon+dx/2

#-------------------------------------------------------------------------------
# tmp_data_path = os.getenv('HOME')+'/Research/E3SM/pub_figs/2023_screamv1_4season/data'
tmp_data_path = 'data_tmp'

fig_file,fig_type = f'figs_scream/scream.zonal-mean.v1','png'

recalculate = True

plot_as_anomaly = False

bin_dlat = 1

# var_x_case = False
# num_plot_col = len(var)
num_plot_col = 3

subtitle_font_height = 0.015
# subtitle_font_height = 0.005

print_stats = False
#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var)

res = hs.res_xy()
res.vpHeightF = 0.5
res.vpHeightF = 0.4
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.xyLineThicknessF = 5

lres = hs.res_xy()
lres.xyDashPattern    = 1
lres.xyLineThicknessF = 2
lres.xyLineColor      = 'black'

# print();print(clr)

# # use random colors for all but first case
# ngl.define_colormap(wks,'ncl_default')
# clr[1:] = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case-1,dtype=int)

# print();print(clr)
# exit()

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

# sim_grid_ds = xr.open_dataset( scrip_file_sim ).rename({'grid_size':'ncol'})
# # sim_grid_ds['grid_center_lon'] = xr.where(sim_grid_ds['grid_center_lon']<0,sim_grid_ds['grid_center_lon']+360,sim_grid_ds['grid_center_lon'])
# # sim_grid_ds['grid_corner_lon'] = xr.where(sim_grid_ds['grid_corner_lon']<0,sim_grid_ds['grid_corner_lon']+360,sim_grid_ds['grid_corner_lon'])
# sim_area = sim_grid_ds['grid_area']
# sim_lat  = sim_grid_ds['grid_center_lat']

# obs_grid_ds = xr.open_dataset( scrip_file_obs ).rename({'grid_size':'ncol'})
# obs_grid_ds['grid_center_lon'] = xr.where(obs_grid_ds['grid_center_lon']<0,obs_grid_ds['grid_center_lon']+360,obs_grid_ds['grid_center_lon'])
# obs_grid_ds['grid_corner_lon'] = xr.where(obs_grid_ds['grid_corner_lon']<0,obs_grid_ds['grid_corner_lon']+360,obs_grid_ds['grid_corner_lon'])
# obs_area = obs_grid_ds['grid_area']
# obs_lat  = obs_grid_ds['grid_center_lat']

# era_grid_ds = xr.open_dataset( scrip_file_era ).rename({'grid_size':'ncol'})
# era_grid_ds['grid_center_lon'] = xr.where(era_grid_ds['grid_center_lon']<0,era_grid_ds['grid_center_lon']+360,era_grid_ds['grid_center_lon'])
# era_grid_ds['grid_corner_lon'] = xr.where(era_grid_ds['grid_corner_lon']<0,era_grid_ds['grid_corner_lon']+360,era_grid_ds['grid_corner_lon'])

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def calculate_obs_area(lon,lat,lon_bnds,lat_bnds):
   re = 6.37122e06  # radius of earth
   nlat,nlon = len(lat),len(lon)
   area = np.empty((nlat,nlon),np.float64)
   for j in range(nlat):
      for i in range(nlon):
         dlon = np.absolute( lon_bnds[j,1] - lon_bnds[j,0] )
         dlat = np.absolute( lat_bnds[j,1] - lat_bnds[j,0] )
         dx = re*dlon*np.pi/180.
         dy = re*dlat*np.pi/180.
         area[j,i] = dx*dy
   return area
#---------------------------------------------------------------------------------------------------
def get_ctr_str(glb_avg=None):
   ctr_str = ''
   # if 'lat1' in globals():
   #    lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
   #    lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
   #    ctr_str += f' {lat1_str}:{lat2_str} '
   # if 'lon1' in globals():
   #    lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
   #    lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
   #    ctr_str += f' {lon1_str}:{lon2_str} '
   # # if glb_avg is not None:
   # #    # add logic here t display global average value?
   return ctr_str
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   # time_list = []
   data_list = []
   bin_list = []
   for c in range(num_case):
      if obs_flag[c]:
         tfile_type = obs_file_type_list[v]
         tvar = obs_var_list[v]
      else:
         tfile_type = file_type_list[v]
         tvar = var[v]
         if var[v]=='horiz_winds_at_model_bot_u': tvar = 'horiz_winds_at_model_bot'
         if var[v]=='horiz_winds_at_model_bot_v': tvar = 'horiz_winds_at_model_bot'
      if c==0: print(' '*2+'var: '+hc.tcolor.GREEN+tvar+hc.tcolor.ENDC)
      print('\n'+' '*4+'case: '+hc.tcolor.CYAN+case[c]+hc.tcolor.ENDC)
      #-------------------------------------------------------------------------
      # if 'ne1024pg2' in case[c]: scrip_file_sim = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/ne1024pg2_scrip.nc'
      # if  'ne256pg2' in case[c]: scrip_file_sim = '/pscratch/sd/w/whannah/e3sm_scratch/files_grid/ne256pg2_scrip.nc'
      sim_grid_ds = xr.open_dataset( scrip_file_sim ).rename({'grid_size':'ncol'})
      sim_area = sim_grid_ds['grid_area']
      sim_lat  = sim_grid_ds['grid_center_lat']
      #-------------------------------------------------------------------------
      # tmp_file = f'{tmp_data_path}/global-mean-timeseries.v1.tmp.nday_{nday_data}.{tvar}.{case[c]}.nc'

      if recalculate :
         print(' '*6+'recalculating...')
         #----------------------------------------------------------------------
         # idenfity the files to load
         tcase = case[c]
         # if 'IMERG' in tcase: tcase = 'IMERG'
         # if 'ERA5'  in tcase: tcase = 'ERA5'
         # file_path = f'{case_dir[c]}/{tcase}/{case_sub[c]}/{tcase}.{tfile_type}*'
         file_path = f'{case_dir[c]}/{tcase}/{case_sub[c]}/{tfile_type}*'
         file_list = sorted(glob.glob(file_path))

         if 'first_file' in globals(): file_list = file_list[first_file:]
         if 'num_files' in globals(): file_list = file_list[:num_files]

         # trim down file list
         # nf = nday_data
         # if obs_flag[c]: nf = nday_data*48 # IMERG frequency is 30min
         # file_list = file_list[:nf] # use initial files
         # if len(file_list)>2:
         #    file_list = [file_list[len(file_list)-2]] # skip last (incomplete) file
         # else:
         #    file_list = [file_list[len(file_list)-1]] # skip last (incomplete) file
         #----------------------------------------------------------------------
         if file_list==[]:
            print('ERROR: Empty file list:')
            print(); print(file_path)
            print(); print(file_list)
            exit()
         #----------------------------------------------------------------------
         print(' '*6+f'Loading data ({tvar})...')
         # for f in range(len(file_list)):
         f = 0
         if True:
            # print(f' '*8+f'f: {f:03d}  file: {hc.tcolor.YELLOW}{file_list[f]}{hc.tcolor.ENDC}')
            #-------------------------------------------------------------------
            # group_name = None # group_name = 'Grid' if case[c]=='IMERG' else None
            # ds = xr.open_dataset( file_list[f], group=group_name)

            ds = xr.open_mfdataset( file_list )
            #-------------------------------------------------------------------
            # Load the data
            if tvar=='precip':
               data = ds['precip_liq_surf_mass'] + ds['precip_ice_surf_mass']
            else:
               data = ds[tvar]
            if obs_flag[c]: 
               if 'latitude'  in data.dims: data = data.rename({'latitude':'lat'})
               if 'longitude' in data.dims: data = data.rename({'longitude':'lon'})
               data = data.stack(ncol=('lat','lon'))
            #-------------------------------------------------------------------
            if obs_flag[c]:
               if var[v]=='U_at_850hPa': data = data.sel(level=850)
               if var[v]=='V_at_850hPa': data = data.sel(level=850)
               if var[v]=='U_at_500hPa': data = data.sel(level=500)
               if var[v]=='V_at_500hPa': data = data.sel(level=500)
            else:
               if var[v]=='horiz_winds_at_model_bot_u': data = data.isel(dim2=0)
               if var[v]=='horiz_winds_at_model_bot_v': data = data.isel(dim2=1)
            #-------------------------------------------------------------------
            if 'lev' in data.dims and lev_list[v] is not None: 
               if lev_list[v]<0: data = data.isel({'lev':np.absolute(lev_list[v])})
            #-------------------------------------------------------------------
            # convert units
            if var[v]=='precip_total_surf_mass_flux':
               data = data*86400*1e3 # m/s to mm/day
            if var[v]=='precip':
               if obs_flag[c]:
                  data = data*24. # mm/hr to mm/day
               else:
                  data = (data/100)*86400 # kg/m2 to mm/day using dtime=100 (ne1024)
            #-------------------------------------------------------------------------
            # Calculate time and zonal mean
            if obs_flag[c]:
               area = obs_area
               lat  = obs_lat
            else:
               area = sim_area
               lat  = sim_lat
            #-------------------------------------------------------------------------
            data = data.mean(dim='time')
            # glb_mean = ( (data*area).sum(dim='ncol') / area.sum(dim='ncol') )
            # print(f'  Area weighted global mean: {hc.tclr.GREEN}{glb_mean.values}{hc.tclr.END}')
            # continue
            #-------------------------------------------------------------------------
            bin_ds = hc.bin_YbyX( data, lat, 
                                 bin_min=-90, bin_max=90, 
                                 bin_spc=bin_dlat, wgt=area )
            data_list.append( np.ma.masked_invalid(  bin_ds['bin_val'].values ) )
            lat_bins = bin_ds['bins'].values
            sin_lat_bins = np.sin(lat_bins*np.pi/180.)
            bin_list.append(sin_lat_bins)

            if 'area' in locals(): del area
         #----------------------------------------------------------------------
         # print some summary stats after zonal average
         if print_stats: hc.print_stat(gbl_mean,name=tvar,compact=True,indent=' '*6)
   #----------------------------------------------------------------------------
   # Create plot
   data_min = np.min([np.min(d) for d in data_list])
   data_max = np.max([np.max(d) for d in data_list])
   # time_min = np.min([np.min(d) for d in time_list])
   # time_max = np.max([np.max(d) for d in time_list])

   if var[v] in ['T_mid','T_2m']:
      if ( (data_max-data_min)!=4):
         delta = 4 - (data_max-data_min)
         data_max = data_max + delta/2
         data_min = data_min - delta/2

   res.trYMinF = data_min
   res.trYMaxF = data_max

   lat_tick = np.array([-90,-60,-30,0,30,60,90])
   res.tmXBMode = 'Explicit'
   res.tmXBValues = np.sin( lat_tick*3.14159/180. )
   res.tmXBLabels = lat_tick
   
   # res.trXMinF = time_min
   # res.trXMaxF = time_max

   # res.tiXAxisString = 'Time [days]'
   # res.tiYAxisString = f'{var_str[v]}'
   # res.tiYAxisString = f'[{var_unit[v]}]'
   #----------------------------------------------------------------------------
   # Calculate anomalies from a reference value
   if plot_as_anomaly:
      for c in range(num_case):
         # calculate ref value as mean of last 2 days
         sample_per_day = int(24*60/15) # 15 minute sampling fro SCREAM
         nsample = 2*sample_per_day
         ref_value = np.mean(data_list[c][-1*nsample:])
         # subtract reference value to collapse curves
         data_list[c] = data_list[c] - ref_value
   #----------------------------------------------------------------------------
   data_min = np.min([np.min(d) for d in data_list])
   data_max = np.max([np.max(d) for d in data_list])
   res.trYMinF = data_min
   res.trYMaxF = data_max
   for c in range(num_case):
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.xyLineColor   = clr[c]
      tres.xyDashPattern = dsh[c]
      # tplot = ngl.xy(wks, time_list[c], data_list[c], tres)
      tplot = ngl.xy(wks, bin_list[c], np.ma.masked_invalid( data_list[c] ), tres)
      if c==0:
         plot[v] = tplot
      else:
         ngl.overlay(plot[v],tplot)
      #-------------------------------------------------------------------------
      # add line to indicate reference value
      if plot_as_anomaly:
         ref_value = np.mean(data_list[c][-1*nsample:])
         ref_array = np.ones(time_list[c].shape) * ref_value
         lres.xyLineColor   = clr[c]
         lres.xyDashPattern = 1
         lplot = ngl.xy(wks, time_list[c], ref_array, lres)
         ngl.overlay(plot[v],lplot)
   #----------------------------------------------------------------------------
   hs.set_subtitles(wks, plot[v], get_ctr_str(), '', var_str[v], font_height=subtitle_font_height)
#---------------------------------------------------------------------------------------------------
if 'num_plot_col' in locals():
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [1,num_var]


ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------