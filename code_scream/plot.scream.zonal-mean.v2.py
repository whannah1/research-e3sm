#---------------------------------------------------------------------------------------------------
import os, ngl, xarray as xr, numpy as np, cmocean, copy, cftime, warnings, glob
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
#---------------------------------------------------------------------------------------------------
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=None,c=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = ''
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name); case_dir.append(p); case_sub.append(s); case_grid.append(g)
#---------------------------------------------------------------------------------------------------
var, var_str, file_type_list = [], [], []
def add_var(var_name,n='',file_type=None):
  var.append(var_name)
  file_type_list.append(file_type)
  var_str.append(n)
#---------------------------------------------------------------------------------------------------

# ### 2024 NCT test
# scrip_file_path = '/global/homes/w/whannah/E3SM/data_grid/ne256pg2_scrip.nc'
# tmp_scratch='/pscratch/sd/w/whannah/scream_scratch'
# tmp_sub = 'run'
# add_case('SCREAM.NCT-test.ne256pg2_ne256pg2.F2010-SCREAMv1.NCT-off', n='SCREAMv1 ne256',          p=tmp_scratch, s=tmp_sub )
# add_case('SCREAM.NCT-test.ne256pg2_ne256pg2.F2010-SCREAMv1.NCT-on',  n='SCREAMv1 ne256 w/NCT on', p=tmp_scratch, s=tmp_sub )

### 2025 popcorn investigation
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne256pg2_scrip.nc'
# tmp_scratch = '/pscratch/sd/w/whannah/scream_scratch/pm-gpu'
tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
r1_str = 'cfr_1.acc_10.rsc_0.eci_1e-05.eri_1e-05.mti_7.4e7'
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2',               n='SCREAM ctrl',p=tmp_scratch,s='run')
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1',         n='SCREAM cfrc',p=tmp_scratch,s='run')
add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.{r1_str}',      n='SCREAM rcp1',p=tmp_scratch,s='run')
# add_case(f'SCREAM.2025-PC-00.F2010-SCREAMv1.ne256pg2',n='E3SM',p=tmp_scratch,s='run')

#---------------------------------------------------------------------------------------------------

# htype = 'monthly.AVERAGE.nmonths_x1' 

# add_var('horiz_winds_u',n='U',file_type='monthly.AVERAGE.nmonths_x1')

tmp_file_type = 'output.scream.3D.1dy.mean.AVERAGE.ndays_x1'
# add_var('T_mid',           n='T', file_type=tmp_file_type)
# add_var('RelativeHumidity',n='RH',file_type=tmp_file_type)
add_var('qv',              n='qv',file_type=tmp_file_type)
add_var('qc',              n='qc',file_type=tmp_file_type)
add_var('qr',              n='qr',file_type=tmp_file_type)
# add_var('qi',              n='qi',file_type=tmp_file_type)


first_file,num_files = 30,10

#---------------------------------------------------------------------------------------------------

fig_file = "figs_scream/scream.zonal-mean.v2"

plot_diff = True

var_x_case = True
num_plot_col = 2
use_common_label_bar = False

recalculate = True

lat1, lat2, dlat = -88., 88., 1

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

wks = ngl.open_wks('png',fig_file)
plot = [None]*(num_var*num_case)
res = hs.res_contour_fill()
res.vpHeightF = 0.3
res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.lbLabelFontHeightF     = 0.015
res.trYReverse = True
res.trYMinF = 200
res.tiXAxisString = 'Latitude'
res.tiYAxisString = 'Pressure [hPa]'

#---------------------------------------------------------------------------------------------------
def get_file_list(case,case_dir,case_sub,file_type):
   file_path = f'{case_dir}/{case}/{case_sub}/*{file_type}*'
   file_list = sorted(glob.glob(file_path))
   if 'first_file' in locals(): file_list = file_list[first_file:]
   if 'num_files' in locals(): file_list = file_list[:num_files]
   return file_list
#---------------------------------------------------------------------------------------------------
def get_data(ds,var):
   tvar = var
   if var=='horiz_winds_u': tvar = 'horiz_winds'
   if var=='horiz_winds_v': tvar = 'horiz_winds'
   data = ds[tvar]#.load()
   if var=='horiz_winds_u': data = data.isel(dim2=0)
   if var=='horiz_winds_v': data = data.isel(dim2=1)
   if 'lev' in data.dims : data = interpolate_to_pressure(ds,data)
   return data
#---------------------------------------------------------------------------------------------------
# lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,
#                550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
lev = np.array([1,2,4,6,10,30,50,100,150,200,300,400,500,600,700,800,850,925,950])
# lev = np.array([5,10,30,50,100,150,200,300,400,500,600,700,800,850,925,975,1000])
ncol_chunk_size = int(100e3)
def interpolate_to_pressure(ds,data_mlev,interp_type=2,extrap_flag=False):
   global lev,chunk_size
   hya = ds['hyam'].isel(time=0,missing_dims='ignore').values
   hyb = ds['hybm'].isel(time=0,missing_dims='ignore').values
   # Create empty array with new lev dim
   data_plev = xr.full_like( data_mlev.isel(lev=0,drop=True), np.nan )
   data_plev = data_plev.expand_dims(dim={'lev':lev}, axis=data_mlev.get_axis_num('lev'))
   # Add dummy dimension
   data_mlev = data_mlev.expand_dims(dim='dummy',axis=len(data_mlev.dims))

   PS_dum = ds['ps'].expand_dims(dim='dummy',axis=len(ds['ps'].dims))
   P0 = xr.DataArray(1e3)
   data_mlev = data_mlev.transpose('time','lev','ncol','dummy')
   data_plev = data_plev.transpose('time','lev','ncol')
   # Do the interpolation in chunks
   c1 = 0
   num_chunk = len(data_plev.chunksizes['ncol'])
   for c,sz in enumerate(data_plev.chunksizes['ncol']):
      c2 = c1+sz
      data_plev[:,:,c1:c2] = ngl.vinth2p( data_mlev[:,:,c1:c2,:].values, \
                                          hya, hyb, lev, PS_dum[:,c1:c2,:], \
                                          interp_type, P0, 1, extrap_flag)[:,:,:,0]
      c1 = c2
   data_plev = xr.DataArray( np.ma.masked_values( data_plev ,1e30), coords=data_plev.coords )
   return data_plev
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print(' '*2+f'var: {hc.tclr.GREEN}{var[v]}{hc.tclr.END}')
   #----------------------------------------------------------------------------
   data_list,lev_list = [],[]
   #----------------------------------------------------------------------------
   for c in range(num_case):
      print(' '*4+f'case: {hc.tclr.CYAN}{case[c]}{hc.tclr.END}')

      temp_dir = os.getenv('HOME')+'/Research/E3SM/data_temp'
      tmp_file = f'{temp_dir}/scream.zonal-mean.v2.{case[c]}.{var[v]}'

      print(' '*6+f'tmp_file: {tmp_file}\n')
      #-------------------------------------------------------------------------
      if recalculate :
         file_list = get_file_list(case[c],case_dir[c],case_sub[c],file_type_list[v])
         ds = xr.open_mfdataset( file_list, chunks={'ncol':ncol_chunk_size} )
         lat  = get_data(ds,'lat').isel(time=0)
         area = get_data(ds,'area').isel(time=0)
         data = get_data(ds,var[v])

         data = data.mean(dim='time', skipna=True)
         #----------------------------------------------------------------------
         hc.print_stat(data,compact=True,indent=' '*6)
         #----------------------------------------------------------------------
         with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=RuntimeWarning)
            bin_ds = hc.bin_YbyX( data, lat, bin_min=lat1, bin_max=lat2, \
                                  bin_spc=dlat, wgt=area, keep_lev=True )
         #----------------------------------------------------------------------
         print(' '*6+f'writing to file: {tmp_file}')
         bin_ds.to_netcdf(path=tmp_file,mode='w')
      else:
         bin_ds = xr.open_dataset( tmp_file )
      #-------------------------------------------------------------------------
      lat_bins = bin_ds['bins'].values
      data_binned = np.ma.masked_invalid( bin_ds['bin_val'].transpose().values )
      data_list.append( data_binned )

   #------------------------------------------------------------------------------------------------
   # Plot zonally averaged data
   #------------------------------------------------------------------------------------------------
   if plot_diff:
      # Find min and max difference values for setting color bar
      tmp_data = data_list - data_list[0]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[0]
      diff_data_min = np.min([np.min(d) for d in tmp_data])
      diff_data_max = np.max([np.max(d) for d in tmp_data])
      # diff_data_min = -2. * np.std(tmp_data)
      # diff_data_max =  2. * np.std(tmp_data)

      # print(f'diff_data_min: {diff_data_min}')
      # print(f'diff_data_max: {diff_data_max}')

   for c in range(num_case):
      ip = v*num_case+c if var_x_case else c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors, contour levels, and plot strings
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      
      tres.lbLabelBarOn = False if use_common_label_bar else True

      # if var[v] in ['OMEGA']  : tres.cnFillPalette = "BlueWhiteOrangeRed"
      # if var[v]=="OMEGA"      : tres.cnLevels = np.linspace(-1,1,21)*0.1
      # if var[v] in ['Q','DYN_Q']: tres.cnLevels = np.linspace(1,16,16)
      # if var[v] in ['MMF_DU','MMF_DV']: tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
      # if var[v] in ['MMF_DU','MMF_DV']: tres.cnLevels = np.linspace(-2,2,11)

      if plot_diff and c>0 : 
         # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
         tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

      # if plot_diff and c>0 : 
      #    if var[v]=="U" : tres.cnLevels = np.linspace(-10,10,11)

      if hasattr(tres,'cnLevels') and not (plot_diff and c>0) : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         if (var[v] in ['U','V']) \
         or (plot_diff and c>0) : 
            aboutZero = True
         else:
            aboutZero = False
         
         if plot_diff and c>0 : 
            aboutZero = True
            data_min = diff_data_min
            data_max = diff_data_max
         else:
            data_min = np.min([np.min(d) for d in data_list])
            data_max = np.max([np.max(d) for d in data_list])
         
         print()
         print(' '*4+f'data_min: {data_min}')
         print(' '*4+f'data_max: {data_max}')
         # print()
         (cmin,cmax,cint) = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=21, \
                                                 returnLevels=False,aboutZero=aboutZero )
         tres.cnLevels = np.linspace(cmin,cmax,num=21)
         tres.cnLevelSelectionMode = "ExplicitLevels"

      # if plot_diff and c>0 : 
         # if var[v]=="U" : tres.cnLevels = np.linspace(-10,10,11)

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------

      tres.sfXArray = lat_bins

      lat_tick = np.array([-90,-60,-30,0,30,60,90])
      res.tmXBMode = "Explicit"
      res.tmXBValues = lat_tick # np.sin( lat_tick*3.14159/180. )
      res.tmXBLabels = lat_tick

      # if 'lev' in vars() : tres.sfYCStartV = min( bin_ds['lev'].values )
      # if 'lev' in vars() : tres.sfYCEndV   = max( bin_ds['lev'].values )
      # tres.sfYCStartV = min( bin_ds['lev'].values )
      # tres.sfYCEndV   = max( bin_ds['lev'].values )
      tres.sfYArray = bin_ds['lev'].values

      if plot_diff and c >0 :
         # tres.cnFillPalette = np.array( cmocean.cm.diff(np.linspace(0,1,256)) )
         # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
         tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
         plot[ip] = ngl.contour(wks, data_list[c] - data_list[0], tres)
      else:
         plot[ip] = ngl.contour(wks, data_list[c], tres)

      cstr = ''
      if plot_diff and c>0 : cstr = 'diff'

      hs.set_subtitles(wks, plot[ip], name[c], cstr, var_str[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

if num_case==1 or num_var==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
else:
   layout = [num_var,num_case] if var_x_case else [num_case,num_var]

pnl_res = hs.setres_panel()
pnl_res.nglPanelYWhiteSpacePercent = 5
pnl_res.nglPanelXWhiteSpacePercent = 0

if use_common_label_bar: pnl_res.nglPanelLabelBar = True

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
