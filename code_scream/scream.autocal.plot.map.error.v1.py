import os, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string
import cmocean
host = hc.get_host()
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
obs_flag = []
def add_case(case_in,n=None,p=None,s=None,g=None,c=None,obs=False):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   obs_flag.append(obs)
#-------------------------------------------------------------------------------
var = []
var_str = []
var_unit = []
file_type_list = []
obs_var_list = []
obs_file_list = []
lev_list = []
def add_var(var_name,obs_var=None,obs_file=None,name='',unit='',lev=None):
   var.append(var_name)
   obs_var_list.append(obs_var)
   obs_file_list.append(obs_file)
   var_str.append(name)
   var_unit.append(unit)
   lev_list.append(lev)
#-------------------------------------------------------------------------------
# var,var_str,lev_list = [],[],[]
# def add_var(var_name,vstr=None,lev=0): 
#    var.append(var_name);
#    if vstr is None: 
#       var_str.append(var_name)
#    else:
#       var_str.append(vstr)
#    lev_list.append(lev)
#-------------------------------------------------------------------------------

obs_root = '/lustre/orion/cli115/proj-shared/hannah6/SCREAM.2024-autocal-00.ne1024pg2/obs/'

tmp_scratch='/lustre/orion/cli115/proj-shared/noel/e3sm_scratch/s10-feb7/dd1024'
tmp_sub = 'SCREAM.2024-autocal-00.ne1024pg2/run'

# add_case('seed', n='seed',  p=tmp_scratch, s=tmp_sub )

# for n in range( 1,12+1): add_case(f'm{n:04d}', p=tmp_scratch,s=tmp_sub)
# for n in range(13,18+1): add_case(f'm{n:04d}', p=tmp_scratch,s=tmp_sub)
# for n in range(20,24+1): add_case(f'm{n:04d}', p=tmp_scratch,s=tmp_sub)

add_case('m0001',c='green', p=tmp_scratch,s=tmp_sub)
# add_case('m0002',c='green', p=tmp_scratch,s=tmp_sub)
# add_case('m0003',c='green', p=tmp_scratch,s=tmp_sub)
# add_case('m0004',c='green', p=tmp_scratch,s=tmp_sub)

scrip_file_path = '/lustre/orion/cli115/proj-shared/hannah6/HICCUP/data/scrip_ne30pg2.nc'
htype = 'output.scream.AutoCal.daily_avg_ne30pg2.AVERAGE.nhours_x24' # also used for file_prefix arg

# add_case('ERA5',n='ERA5', p=obs_data_root,s='daily',obs=True,d=0,c='black')
# add_case('IMERG',n='IMERG Jan 2020',p=obs_data_root,s='daily_QC_Jan_2020',obs=True,d=1,c='red')

#-------------------------------------------------------------------------------

# add_var('precip_total_surf_mass_flux')
# add_var('VapWaterPath')
# add_var('LiqWaterPath')
# add_var('T_2m')
# add_var('horiz_winds_at_model_bot_u')

# add_var('precip_ice_surf_mass')
# add_var('precip_liq_surf_mass')
# add_var('LiqWaterPath')
# add_var('IceWaterPath')
# add_var('T_2m')
# add_var('SW_flux_up@tom')
add_var('LW_flux_up@tom',obs_var='LW_flux_up_at_model_top',obs_file='CERES.LW_flux_up_at_model_top.AVERAGE.ne30pg2.20200126.nc',name='TOA LW up',unit='')

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.autocal.map.error.v1'

#-------------------------------------------------------------------------------
# lat1,lat2 = -50,50
# lat1,lat2,lon1,lon2 = -20,0,220,240
# lat1,lat2,lon1,lon2 = 10,30,360-100,360-55

# lat1,lat2,lon1,lon2 = -10,70,180,340             # N. America
# lat1,lat2,lon1,lon2 = 25, 50, 360-125, 360-75    # CONUS
# lat1,lat2,lon1,lon2 = -40,40,90,240              # MC + West Pac
# lat1,lat2,lon1,lon2 = -20,20,150,200              # MC + West Pac
# lat1,lat2,lon1,lon2 = -5,5,150,160              # MC + West Pac
# lat1,lat2,lon1,lon2 = 0,60,50,120                # India
# lat1,lat2,lon1,lon2 =  20,60,360-60,360-10              # Atlantic
# lat1,lat2,lon1,lon2 = -45,-45+60,360-120,360-40  # S. America

# lat1,lat2,lon1,lon2 = 0,10,0,10
#-------------------------------------------------------------------------------
first_file,num_files = 1,1

# htype,first_file,num_files = 'h1.INSTANT',2,1

# htype,first_file,num_files = 'h1',0,31
# htype,first_file,num_files = 'h1',30,1

use_remap,remap_grid = False,'90x180' # 90x180 / 180x360

plot_diff,add_diff = False,False

use_snapshot,ss_t = False,0

print_stats = True

var_x_case = True

num_plot_col = len(case)

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
diff_base = 0

if case==[]: raise ValueError('ERROR - case list is empty!')
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.01

if 'diff_case' not in vars(): diff_case = [(i+1) for i in range(num_case-1)]

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
# npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
dx = 0.25
if 'lat1' in locals() : res.mpMinLatF = lat1+dx; res.mpMaxLatF = lat2-dx
if 'lon1' in locals() : res.mpMinLonF = lon1+dx; res.mpMaxLonF = lon2-dx

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.01
# res.tmXBOn                       = False
# res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'scream'
   return comp
#---------------------------------------------------------------------------------------------------
def get_ctr_str(glb_avg=None):
   ctr_str = ''
   if 'lat1' in globals():
      lat1_str = f'{lat1}N' if lat1>=0 else f'{(lat1*-1)}S'
      lat2_str = f'{lat2}N' if lat2>=0 else f'{(lat2*-1)}S'
      ctr_str += f' {lat1_str}:{lat2_str} '
   if 'lon1' in globals():
      lon1_str = f'{lon1}E' #if lon1>=0 and lon1<=360 else f'{(lon1*-1)}S'
      lon2_str = f'{lon2}E' #if lon2>=0 and lon2<=360 else f'{(lon2*-1)}S'
      ctr_str += f' {lon1_str}:{lon2_str} '
   # if glb_avg is not None:
   #    # add logic here t display global average value?
   return ctr_str
#---------------------------------------------------------------------------------------------------

scrip_ds = xr.open_dataset(scrip_file_path)

use_mask = False

if use_mask:

   lat_name,lon_name = 'grid_center_lat','grid_center_lon'
   tmp_data = np.ones(len(scrip_ds[lat_name]),dtype=bool)
   mask = xr.DataArray( tmp_data, coords=scrip_ds[lat_name].coords )
   if 'lat1' in locals(): mask = mask & (scrip_ds[lat_name]>=lat1) & (scrip_ds[lat_name]<=lat2)
   if 'lon1' in locals(): mask = mask & (scrip_ds[lon_name]>=lon1) & (scrip_ds[lon_name]<=lon2)

   scrip_ds_center_lon = scrip_ds['grid_center_lon'].where(mask,drop=True)
   scrip_ds_center_lat = scrip_ds['grid_center_lat'].where(mask,drop=True)
   scrip_ds_corner_lon = scrip_ds['grid_corner_lon'].where(mask,drop=True)
   scrip_ds_corner_lat = scrip_ds['grid_corner_lat'].where(mask,drop=True)

   mask = mask.rename({'grid_size':'ncol'})

else:

   scrip_ds_center_lon = scrip_ds['grid_center_lon']
   scrip_ds_center_lat = scrip_ds['grid_center_lat']
   scrip_ds_corner_lon = scrip_ds['grid_corner_lon']
   scrip_ds_corner_lat = scrip_ds['grid_corner_lat']

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   glb_avg_list = []
   std_list,cnt_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp,
                          populate_files=False )
      case_obj.set_coord_names(var[v])

      # if 'lat1' in locals() : case_obj.lat1 = lat1; case_obj.lat2 = lat2
      # if 'lon1' in locals() : case_obj.lon1 = lon1; case_obj.lon2 = lon2

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------      
      # area = case_obj.load_data('area',component=get_comp(case[c]),htype=htype,file_prefix=htype)

      # tvar = var[v]
      # if var[v]=='horiz_winds_at_model_bot_u': tvar = 'horiz_winds_at_model_bot'
      # if var[v]=='horiz_winds_at_model_bot_v': tvar = 'horiz_winds_at_model_bot'

      # data = case_obj.load_data(tvar, component=get_comp(case[c]),
      #                                 htype=htype,ps_htype=htype,lev=lev,file_prefix=htype,
      #                                 first_file=first_file,num_files=num_files,
      #                                 use_remap=use_remap,remap_str=f'remap_{remap_grid}')
      #-------------------------------------------------------------------------
      file_name = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/output.scream.Cess.3hourlyAVG_ne120.AVERAGE.nhours_x3.2019-08-01-00000.nc'
      ds = xr.open_dataset( file_name )
      #-------------------------------------------------------------------------
      if var[v]=='horiz_winds_at_model_bot_u': data = data.isel(dim2=0)
      if var[v]=='horiz_winds_at_model_bot_v': data = data.isel(dim2=1)

      if use_mask: data = data.where(mask,drop=True)

      # print('-'*80)
      # print()
      # print(data)
      # print()
      # print('-'*80)
      # exit()

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      #-------------------------------------------------------------------------
      # adjust units
      if var[v]=='precip_total_surf_mass_flux': data = data*86400*1e3 # m/s => mm/day
      #-------------------------------------------------------------------------
      # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         if use_snapshot:
            data = data.isel(time=ss_t)
            print(hc.tcolor.RED+'WARNING - plotting snapshot!!!'+hc.tcolor.ENDC)
         else:
            data = data.mean(dim='time')
      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
         glb_avg_list.append(gbl_mean)
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      # print(); print(data)

      if case[c]=='TRMM' and 'lon1' not in locals(): 
         data_list.append( ngl.add_cyclic(data.values) )
      else:
         data_list.append( data.values )

      if 'area' in locals() : area_list.append( area.values )
      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff :
         if c==diff_base:
            data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

      if var[v]=='wind_speed_10m': diff_data_min,diff_data_max = -1,1

   for c in range(num_case):

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]), 
                          data_dir=data_dir_tmp, data_sub=data_sub_tmp,
                          populate_files=False )
      case_obj.set_coord_names(var[v])
      if 'AQUA-RRM-TEST' in case[c]: case_obj.grid = 'RRM_cubeface_grad_ne4x5_pg2'

      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      rain_clr_map = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      amp_clr_map  = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      bal_clr_map  = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
      
      tres.cnFillPalette = "MPL_viridis"

      if var[v]=='U'                          : tres.cnFillPalette = bal_clr_map
      if var[v]=='T_2m'                       : tres.cnFillPalette = amp_clr_map
      if var[v]=='precip_total_surf_mass_flux': tres.cnFillPalette = rain_clr_map
      if var[v]=='precip_ice_surf_mass'       : tres.cnFillPalette = rain_clr_map
      if var[v]=='precip_liq_surf_mass'       : tres.cnFillPalette = rain_clr_map
      if var[v]=='LiqWaterPath'               : tres.cnFillPalette = rain_clr_map
      if var[v]=='IceWaterPath'               : tres.cnFillPalette = rain_clr_map
      
      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(2,20+2,2)
      # # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      # if var[v]=='P-E'                 : tres.cnLevels = np.linspace(-10,10,21)
      # # if var[v]=='TS'                  : tres.cnLevels = np.arange(0,40+2,2)
      # if var[v]=='RH'                  : tres.cnLevels = np.arange(10,100+1,1)
      # # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(1,30+1,1)*1e-2
      # # if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.logspace( -2, 0.25, num=60).round(decimals=2)
      # if var[v] in ['TGCLDIWP','TGPRCIWP']: tres.cnLevels = np.arange(0.005,0.155,0.01)
      # if var[v] in ['TGCLDLWP','TGPRCLWP']: tres.cnLevels = np.arange(0.01,0.25,0.015)

      # if var[v]=='precip_total_surf_mass_flux': tres.cnLevels = np.logspace( -2, 2, num=20).round(decimals=2)
      # if var[v]=='T_2m'                       : tres.cnLevels = np.arange(290,310+1,1)
      # if var[v]=='U_at_500hPa'                : tres.cnLevels = np.arange(?,?+1,1)
      

      ### print color levels
      # if hasattr(tres,'cnLevels') : 
      #    print(f'\ntres.cnLevels:')
      #    msg = ''
      #    for cl in range(len(tres.cnLevels)):
      #       msg += f'{tres.cnLevels[cl]}, '
      #    print(f'[ {msg} ]\n')
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 41
         aboutZero = False
         if var[v] in ['SPTLS','SPQTLS','U','V','VOR','DIV',
                       'U850','V850','U200','V200',
                       'MMF_CVT_TEND_T','MMF_CVT_TEND_Q',] : 
            aboutZero = True
         if var[v]=='U':aboutZero = True
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      # var_str_tmp = var_str[v]
      # # if var[v]=='PRECT':     var_str = 'Precipitation'

      # lev_str = None
      # if lev>0: lev_str = f'{lev}mb'
      # if lev<0: lev_str = f'k={(lev*-1)}'
      # # if lev_str is not None and var[v] in ['U','V','OMEGA','T','Q','Z3']:
      # if lev_str is not None:
      #    var_str_tmp = f'{lev_str} {var[v]}'
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      if use_common_label_bar: 
         tres.lbLabelBarOn = False
      else:
         tres.lbLabelBarOn = True

      # if use_remap or case_obj.obs :
      #    hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
      # else:
      #    hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_path)

      tres.cnFillMode    = 'CellFill'
      tres.sfXArray      = scrip_ds_center_lon.values # scrip_ds['grid_center_lon'].where(mask,drop=True).values
      tres.sfYArray      = scrip_ds_center_lat.values # scrip_ds['grid_center_lat'].where(mask,drop=True).values
      tres.sfXCellBounds = scrip_ds_corner_lon.values # scrip_ds['grid_corner_lon'].where(mask,drop=True).values
      tres.sfYCellBounds = scrip_ds_corner_lat.values # scrip_ds['grid_corner_lat'].where(mask,drop=True).values
         
         
      if plot_diff and c==diff_base : base_name = name[c]

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v

      if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==diff_base) : 

         # plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres) 
         
         #----------------------------------------------------------------------
         # set plot subtitles

         # if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'

         hs.set_subtitles(wks, plot[ip], name[c], get_ctr_str(glb_avg=None), var_str[v], font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c in diff_case :
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         # if var[v] in ['PRECT','PRECC','PRECL'] : tres.cnLevels = np.arange(-5,5+1,1)
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(hc.tcolor.RED+'WARNING: Difference is zero!'+hc.tcolor.ENDC)
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=11,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=11)
         
         ### override the level settings and just use auto
         # tres.cnLevelSelectionMode = "AutomaticLevels"

         # if use_remap:
         #    hs.set_cell_fill(tres,case_obj=case_obj,lat=lat,lon=lon)
         # else:
         #    hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_path)

         tres.cnFillMode    = 'CellFill'
         tres.sfXArray      = scrip_ds_center_lon.values
         tres.sfYArray      = scrip_ds_center_lat.values
         tres.sfXCellBounds = scrip_ds_corner_lon.values
         tres.sfYCellBounds = scrip_ds_corner_lat.values
            
         tres.lbLabelBarOn = True

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+(num_case-1)
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         # plot[ipd] = ngl.contour_map(wks,data_list[c],tres)
         plot[ipd] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres)
         #----------------------------------------------------------------------
         # set plot subtitles

         # if glb_avg_list != []: 
         #    glb_diff = glb_avg_list[c] - glb_avg_list[diff_base]
         #    ctr_str += f' ({glb_diff:6.4})'
         
         hs.set_subtitles(wks, plot[ipd], name[c], get_ctr_str(glb_avg=None), var_str[v]+' (Diff)', font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# if plot_diff : num_case = num_case+len(diff_case)   # use this to plot both before and after diff

# title_str = None
# title_str = 'ANN'
# if months==[1,2,12]: title_str = 'DJF'
# if months==[6,7,8]: title_str = 'JJA'
# if title_str is not None:
#    textres =  ngl.Resources()
#    textres.txFontHeightF =  0.025
#    ngl.text_ndc(wks,title_str,0.5,.7,textres)

num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]


if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### use common label bar
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
