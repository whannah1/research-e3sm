import os, glob, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, cmocean
#-------------------------------------------------------------------------------
'''
# for res.mpDataBaseVersion = 'HighRes' use this:
export PYNGL_RANGS=~/.conda/envs/pyn_env/lib/ncarg/database/rangs

# for blue marble images:
https://earthobservatory.nasa.gov/features/BlueMarble

wget https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74117/world.200408.3x21600x10800.png

# NCL example using GDAL translated image:
https://www.ncl.ucar.edu/Applications/Scripts/topo_9.ncl

# Use GDAL to create geo-referenced blue marble RGB data:

FILE_PREFIX=/pscratch/sd/w/whannah/Blue_Marble/world.200408.3x5400x2700
gdal_translate -ot Int16 -of netCDF ${FILE_PREFIX}.jpg ${FILE_PREFIX}.jpg.nc

FILE_PREFIX=/pscratch/sd/w/whannah/Blue_Marble/world.200408.3x21600x10800.august
gdal_translate -ot Int16 -of netCDF ${FILE_PREFIX}.png ${FILE_PREFIX}.nc

# Create grid files for plotting blue marble data
ncremap -G ttl='Equi-Angular grid 21600x10800'#latlon=10800,21600#lat_typ=uni#lon_typ=180_wst -g /pscratch/sd/w/whannah/files_grid/scrip_21600x10800.nc
ncremap -G ttl='Equi-Angular grid 5400x2700'#latlon=2700,5400#lat_typ=uni#lon_typ=180_wst -g /pscratch/sd/w/whannah/files_grid/scrip_5400x2700.nc
ncremap -G ttl='Equi-Angular grid 294x196'#latlon=196,294#lat_typ=uni#lon_typ=180_wst -g /pscratch/sd/w/whannah/files_grid/scrip_294x196.nc
'''
#-------------------------------------------------------------------------------

# data_file_path = '/pscratch/sd/e/ebercosh/e3sm_scratch/pm-gpu/oct11_katrina1.ne1024pg2_ne1024pg2.F2010-SCREAMv1.pm-gpu_gnugpu/run/scream_output.katrina.15minINST.h.INSTANT.nmins_x15.2005-08-27-00000.nc'
# data_file_path = '/pscratch/sd/e/ebercosh/e3sm_scratch/pm-gpu/oct11_katrina1.ne1024pg2_ne1024pg2.F2010-SCREAMv1.pm-gpu_gnugpu/run/scream_output.katrina.15minINST.h.INSTANT.nmins_x15.2005-08-27-21600.nc'
# data_file_path = '/pscratch/sd/e/ebercosh/e3sm_scratch/pm-gpu/oct11_katrina1.ne1024pg2_ne1024pg2.F2010-SCREAMv1.pm-gpu_gnugpu/run/scream_output.katrina.15minINST.h.INSTANT.nmins_x15.2005-08-28-21600.nc'

data_file_path = '/pscratch/sd/e/ebercosh/e3sm_scratch/pm-gpu/oct11_katrina1.ne1024pg2_ne1024pg2.F2010-SCREAMv1.pm-gpu_gnugpu/run/scream_output.katrina.15minINST.h.INSTANT.nmins_x15.2005-08-29-21600.nc' # dt=50s
# data_file_path = '/pscratch/sd/e/ebercosh/e3sm_scratch/pm-gpu/oct7_katrina1.ne1024pg2_ne1024pg2.F2010-SCREAMv1.pm-gpu_gnugpu/run/scream_output.katrina.15minINST.h.INSTANT.nmins_x15.2005-08-29-21600.nc' # dt=100s

# data_file_path = '/global/cfs/cdirs/e3smdata/simulations/scream-decadal/decadal-production-run6-20240708.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.pnetcdf/run/output.scream.decadal.1hourlyINST_ne1024pg2.INSTANT.nhours_x1.1998-08-15-03600.nc'

# data_file_path = '/global/cfs/cdirs/e3smdata/simulations/eamxxerfaer/pd.eam3.29bdb81.free.1024/3hi.INSTANT.nhours_x3.2020-01-01-10800.nc'
# data_file_path = '/global/cfs/cdirs/e3smdata/simulations/eamxxerfaer/pd.eam3.29bdb81.nudg.1024/3hi.INSTANT.nhours_x3.2020-01-01-10800.nc'

fig_file,fig_type = os.getenv('HOME')+'/Research/E3SM/figs_scream/scream.eye-candy.map.v1','png'

use_blue_marble = True
overlay_rain    = True
#-------------------------------------------------------------------------------
# lat1,lat2,lon1,lon2 = 0,60,360-120,360-30 # Carribean+Atlantic - wide
# lat1,lat2,lon1,lon2 = 0,60,360-105,360-15 # Carribean+Atlantic - better for Robinson projection
lat1,lat2,lon1,lon2 = 10,35,360-105,360-45 # Carribean 
# lat1,lat2,lon1,lon2 = 15,30,360-90,360-70 # zoom in on Katrina

# lat1,lat2,lon1,lon2 = 0,45,360-75,360-15 # general Atlantic view
# lat1,lat2,lon1,lon2 = -10,45,-180,360-90 # Pacific view - near equator
# lat1,lat2,lon1,lon2 = 15,60,-180,360-105 # Pacific view - high lat
# lat1,lat2,lon1,lon2 = -30,30,60,180 # Pacific view - near equator

#---------------------------------------------------------------------------------------------------
npix = 8192*2 # 1024 / 2048 / 4096 / 8192
wkres = ngl.Resources()
wkres.wkWidth,wkres.wkHeight=npix,npix  
wks = ngl.open_wks(fig_type,fig_file,wkres)

res = ngl.Resources()
res.nglDraw                   = False
res.nglFrame                  = False
res.cnFillOn                  = True
res.cnLinesOn                 = False
res.cnLineLabelsOn            = False
res.cnInfoLabelOn             = False
res.lbLabelBarOn              = False
res.pmTickMarkDisplayMode     = 'Never'

#---------------------------------------------------------------------------------------------------
def add_map_res(res,geo_lines=False):
   tmp_res = copy.deepcopy(res)
   # tmp_res.mpProjection              = 'Robinson'
   # tmp_res.mpGridAndLimbOn           = False
   # tmp_res.mpLimitMode               = 'LatLon'
   # tmp_res.mpMinLatF                 = lat1
   # tmp_res.mpMaxLatF                 = lat2
   # tmp_res.mpMinLonF                 = lon1
   # tmp_res.mpMaxLonF                 = lon2
   if geo_lines:
      tmp_res.mpOutlineBoundarySets = 'Geophysical'
      tmp_res.mpDataBaseVersion = 'MediumRes' # LowRes / MediumRes / HighRes
   else:
      tmp_res.mpOutlineBoundarySets     = 'NoBoundaries'
   return tmp_res
#---------------------------------------------------------------------------------------------------
def trim_png(fig_file,verbose=True):
   """ crop white space from png file """
   fig_file = fig_file+".png"
   fig_file = fig_file.replace(os.getenv('HOME')+'/Research/E3SM/','')
   if os.path.isfile(fig_file) :
      cmd = "convert -trim +repage "+fig_file+"   "+fig_file
      os.system(cmd)
      if verbose: print("\n"+fig_file+"\n")
   else:
      raise FileNotFoundError(f'\ntrim_png(): {fig_file} does not exist?!\n')
#---------------------------------------------------------------------------------------------------
# RGB color maps
clr_map_R = np.zeros([256,4])
clr_map_G = np.zeros([256,4])
clr_map_B = np.zeros([256,4])

# ramp = np.arange(0,256,1)/255
ramp = np.linspace(0,1,256)

clr_map_R[:,0] = ramp ; clr_map_R[:,3] = 1.
clr_map_G[:,1] = ramp ; clr_map_G[:,3] = 0.
clr_map_B[:,2] = ramp ; clr_map_B[:,3] = 0.

#---------------------------------------------------------------------------------------------------
if use_blue_marble:
   # fig_file = fig_file_prefix+'.background'
   # wks = ngl.open_wks(fig_type,fig_file,wkres)
   #----------------------------------------------------------------------------
   # # low-res blue marble
   # data_ds = xr.open_dataset( '/pscratch/sd/w/whannah/Blue_Marble/world.200408x294x196.nc' )
   # grid_ds = xr.open_dataset( '/pscratch/sd/w/whannah/files_grid/scrip_294x196.nc' )

   # medium-res blue marble
   data_ds = xr.open_dataset( '/pscratch/sd/w/whannah/Blue_Marble/world.200408.3x5400x2700.nc' )
   grid_ds = xr.open_dataset( '/pscratch/sd/w/whannah/files_grid/scrip_5400x2700.nc' )

   # # high-res blue marble
   # data_ds = xr.open_dataset( '/pscratch/sd/w/whannah/Blue_Marble/world.200408.3x21600x10800.august.nc' )
   # grid_ds = xr.open_dataset( '/pscratch/sd/w/whannah/files_grid/scrip_21600x10800.nc' )

   #----------------------------------------------------------------------------
   bm_res = copy.deepcopy(res)
   bm_res.cnFillMode    = 'RasterFill'
   bm_res.sfXArray      = grid_ds['grid_center_lon'].values
   bm_res.sfYArray      = grid_ds['grid_center_lat'].values
   bm_res.cnLevelSelectionMode  = 'EqualSpacedLevels'
   bm_res.cnMaxLevelCount       = 254
   bm_res.cnFillBackgroundColor = [ 1., 1., 1., 1. ]
   
   print(); print('  Creating blue marble background')

   bm_res.cnFillColors=clr_map_R ; plot_R=ngl.contour_map(wks, data_ds['Band1'].values.flatten(),add_map_res(bm_res))
   bm_res.cnFillColors=clr_map_G ; plot_G=ngl.contour    (wks, data_ds['Band2'].values.flatten(),bm_res)
   bm_res.cnFillColors=clr_map_B ; plot_B=ngl.contour    (wks, data_ds['Band3'].values.flatten(),bm_res)

   plot = plot_R
   ngl.overlay(plot, plot_G)
   ngl.overlay(plot, plot_B)

   print('  done.')
   #----------------------------------------------------------------------------
   # # uncomment these lines to only plot blue marble data
   # ngl.draw(plot)
   # ngl.frame(wks)
   # trim_png(fig_file)
   # exit()

#---------------------------------------------------------------------------------------------------
# load SCREAM data for plot of clouds
print(); print('  Loading SCREAM data...')
data_ds = xr.open_dataset( data_file_path )
data_ds = data_ds.isel(time=0)
#-------------------------------------------------------------------------------
scrip_ds = xr.open_dataset('/pscratch/sd/w/whannah/e3sm_scratch/files_grid/ne1024pg2_scrip.nc')
#-------------------------------------------------------------------------------
tres = copy.deepcopy(res)

tres.cnFillMode    = 'CellFill'
tres.sfXArray      = scrip_ds['grid_center_lon'].values
tres.sfYArray      = scrip_ds['grid_center_lat'].values
tres.sfXCellBounds = scrip_ds['grid_corner_lon'].values
tres.sfYCellBounds = scrip_ds['grid_corner_lat'].values

# create black=>white color map for clouds
clr_map_cld = np.zeros([256,4])
clr_map_cld[:,0] = ramp
clr_map_cld[:,1] = ramp
clr_map_cld[:,2] = ramp
clr_map_cld[:,3] = ramp
tres.cnFillPalette = clr_map_cld

print(); print('  Creating cloud data plot')
tres.cnLevelSelectionMode = "ExplicitLevels"
tres.cnLevels = np.logspace(-3,0.8,num=40)
CWP = data_ds['LiqWaterPath']
if 'IceWaterPath' in data_ds.variables: CWP = CWP + data_ds['IceWaterPath']
if use_blue_marble:
   ngl.overlay(plot,ngl.contour(wks,CWP.values,tres))
else:
   plot = ngl.contour_map(wks,CWP.values,add_map_res(tres,geo_lines=True))
print('  done.')

if overlay_rain:
   print(); print('  Creating rain data plot')
   cmap = ngl.read_colormap_file('ncl_default')
   cmap[:,3] = np.linspace(0,1,len(cmap[:,3]))
   tres.cnFillPalette = cmap
   tres.cnLevels = np.logspace(-1.9,1.4,num=40)
   ngl.overlay(plot, ngl.contour(wks,data_ds['RainWaterPath'].values,tres))
   print('  done.')

# finalize the plot
ngl.draw(plot)
ngl.frame(wks)
trim_png(fig_file)
ngl.end()

#---------------------------------------------------------------------------------------------------
