#---------------------------------------------------------------------------------------------------
# Plot the zonal mean of the specified variables
#---------------------------------------------------------------------------------------------------
import os,ngl,xarray as xr,numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy,cftime,warnings
data_dir,data_sub = None,None
home = os.getenv("HOME")
print()

### Early Science
# case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ,'earlyscience.FC5AV1C-L.ne30.sp1_64x1_1000m.20190415' ]
# case = ['earlyscience.FC5AV1C-L.ne30.E3SM.20190519' ]

### INCITE2019
# case = ['INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026']
# name = ['INCITE2019']

### physgrid validation
name,case,git_hash = [],[],'cbe53b'
case.append(f'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
case.append(f'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
case.append(f'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
case.append(f'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# case.append(f'E3SM.PGVAL.conusx4v1_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
# case.append(f'E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-{git_hash}')
for c in case:
   if 'E3SM.PGVAL.ne30_r05_oECv3'         in c: name.append('ne30np4')
   if 'E3SM.PGVAL.ne30pg2_r05_oECv3'      in c: name.append('ne30pg2')
   if 'E3SM.PGVAL.ne30pg3_r05_oECv3'      in c: name.append('ne30pg3')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4')
   if 'E3SM.PGVAL.ne30pg4_r05_oECv3'      in c: name.append('ne30pg4')
   if 'E3SM.PGVAL.conusx4v1_r05_oECv3'    in c: name.append('RRM np4')
   if 'E3SM.PGVAL.conusx4v1pg2_r05_oECv3' in c: name.append('RRM pg2')


### INCITE test runs
name,case = [],[]
case_head = 'E3SM.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX'
# case.append(f'{case_head}.CRMNX_64.CRMDX_1600.CRMDT_10.NLEV_50.CRMNZ_46.RADNX_4.2008-10-01.00');name.append('MMF 64x1.6 L50/46')  
# case.append(f'{case_head}.CRMNX_64.CRMDX_1600.CRMDT_5.NLEV_72.CRMNZ_58.RADNX_4.2008-10-01.00'); name.append('MMF 64x1.6 L72/58')  
# case.append(f'{case_head}.CRMNX_256.CRMDX_200.CRMDT_5.NLEV_100.CRMNZ_95.RADNX_4.2008-10-01.00');name.append('MMF 256x200 L100/95')  
# case.append(f'{case_head}.CRMNX_256.CRMDX_200.CRMDT_2.NLEV_120.CRMNZ_115.RADNX_4.2008-10-01.00');name.append('MMF 256x200 L120/115')
# case.append(f'{case_head}.CRMNX_64.CRMNY_64.CRMDX_1600.CRMDT_10.NLEV_50.CRMNZ_46.RADNX_4.2008-10-01.00');name.append('MMF 64x64x1.6 L50/46')
case.append(f'{case_head}.CRMNX_16.CRMNY_64.CRMDX_1600.CRMDT_10.NLEV_50.CRMNZ_46.RADNX_4.2008-10-01.00');name.append('MMF 16x64x1.6 L50/46')
case.append(f'{case_head}.CRMNX_32.CRMNY_32.CRMDX_3200.CRMDT_10.NLEV_50.CRMNZ_46.RADNX_4.2008-10-01.00');name.append('MMF 32x32x3.2 L50/46')



# lev = np.array([10,30,50,75,100,125,150,200,250,300,350,400,450,500,550,600,650,700,750,800,825,850,875,900,925,950,975,1000])
# lev = np.array([5,10,30,50,100,150,200,300,400,500,600,700,800,850,925,975,1000])
# lev = np.array([550,600,650,700,750,800,825,850,875,900,925,950,975,1000])

# var = ['CLOUD','CLDLIQ','CLDICE','QRS','QRL']
# var = ['OMEGA','CLOUD','CLDLIQ','CLDICE']
# var = ['QRS','QRL']
# var = ['SPDT','SPDQ','SPTLS','SPQTLS']
# var = ['U','V','T','Q']
var = ['CLDLIQ','MMF_TKE','MMF_TKEW','MMF_TKES']
# var = ['CLDLIQ']

use_remap,remap_grid = True,'180x360'

plot_diff = False

htype,years,months,num_files,first_file = 'h2',[],[],2,0


fig_file = home+"/Research/E3SM/figs_clim/transect.simple.v1"

# lat1,lat2 = -20,-15
xlat,lon1,lon2 = -20,360-110,360-75


#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_case = len(case)

wks = ngl.open_wks('png',fig_file)
plot = [None]*(num_var*num_case)
res = hs.res_contour_fill()
res.vpHeightF = 0.4
# res.trYReverse = True
# res.tiYAxisString = 'Pressure [hPa]'

res.tiYAxisString = 'Height [m]'
res.trYMinF = 0

res.tmYLLabelFontHeightF   = 0.01
res.tmXBLabelFontHeightF   = 0.01
res.lbLabelFontHeightF     = 0.015



if 'name' not in vars() : name = case
if 'lev' not in vars() : lev = np.array([0])

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+var[v])
   data_list,lev_list,lon_list = [],[],[]
   for c in range(num_case):
      print('    case: '+case[c])
      if use_remap:
         case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub = f'data_remap_{remap_grid}/' )
      else:
         case_obj = he.Case( name=case[c], data_dir=data_dir, data_sub=data_sub )

      # ip = v*num_case+c
      ip = c*num_var+v
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if 'lat1' in vars(): case_obj.lat1 = lat1
      if 'lat2' in vars(): case_obj.lat2 = lat2
      if 'lon1' in vars(): case_obj.lon1 = lon1
      if 'lon2' in vars(): case_obj.lon2 = lon2

      if use_remap:

         tvar = var[v]
         if var[v]=='MMF_TKEW' and '256' in case[c]: tvar = 'MMF_WTKE'
         
         # data = case_obj.load_data(var[v],htype=htype,ps_htype=htype,first_file=first_file,num_files=num_files
         data = case_obj.load_data(tvar,htype=htype,first_file=first_file,num_files=num_files,use_remap=True,remap_str=f'remap_{remap_grid}')
         zlev = case_obj.load_data('Z3'  ,htype=htype,first_file=first_file,num_files=num_files,use_remap=True,remap_str=f'remap_{remap_grid}')
         area = case_obj.load_data('area',htype=htype,first_file=first_file,num_files=num_files,use_remap=True,remap_str=f'remap_{remap_grid}')
         # lat = case_obj.load_data('lat',htype=htype,num_files=1,use_remap=True,remap_str=f'remap_{remap_grid}')
         # lon = case_obj.load_data('lon',htype=htype,num_files=1,use_remap=True,remap_str=f'remap_{remap_grid}')
      # else:
         # not supported at the moment...

   
      data.load()
      zlev.load()
      area.load()

      # select region
      data = data.sel(lat=xlat,method="nearest").sel(lon=slice(lon1,lon2))
      zlev = zlev.sel(lat=xlat,method="nearest").sel(lon=slice(lon1,lon2))
      area = area.sel(lat=xlat,method="nearest").sel(lon=slice(lon1,lon2))

      
      # hc.print_stat(data)

      # average in time
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         data = data.mean(dim='time')
         zlev = zlev.mean(dim='time')

      # rename ilev if present
      # if 'ilev' in data.dims : data = data.rename({'ilev':'lev'})

      # hc.print_stat(data)

      # exit(data)

      # # add data to list
      # data_list.append( data )
      # lev_list.append(data['lev'].values)
      # lon_list.append(data['lon'].values-360)
      
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      with warnings.catch_warnings():
         warnings.simplefilter("ignore", category=RuntimeWarning)

         for x in range(len(data['lon'])):
            bin_ds = hc.bin_YbyX( data.isel(lon=x), zlev.isel(lon=x), bin_min=0, bin_max=4000, bin_spc=400, wgt=area )
            
            if x==0:
               zlev_bins = bin_ds['bins'].values
               nlev = len(zlev_bins)
               nlon = len(data['lon'])
               data_binned = xr.DataArray( np.zeros([nlev,nlon]), coords=[('zlev', zlev_bins),('lon', data['lon'])] )

            data_binned[:,x] = bin_ds['bin_val'].values


      # data_binned = np.ma.masked_invalid( data_binned.transpose().values )
      data_binned = np.ma.masked_invalid( data_binned.values )

      # add data to list
      data_list.append( data_binned )
      lev_list.append(zlev_bins)
      lon_list.append(data['lon'].values-360)
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      
      

   #------------------------------------------------------------------------------------------------
   # Plot zonally averaged data
   #------------------------------------------------------------------------------------------------
   if plot_diff:

      # Find min and max difference values for setting color bar
      tmp_data = data_list - data_list[0]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[0]
      diff_data_min = np.min([np.min(d) for d in tmp_data])
      diff_data_max = np.max([np.max(d) for d in tmp_data])

   for c in range(num_case):
      case_obj = he.Case( name=case[c] )

      # ip = v*num_case+c
      ip = c*num_var+v

      #-------------------------------------------------------------------------
      # Set colors, contour levels, and plot strings
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      
      # tres.cnFillPalette = "MPL_viridis"
      
      # if var[v] in ['OMEGA']  : tres.cnFillPalette = "BlueWhiteOrangeRed"
      # if var[v]=="OMEGA"      : tres.cnLevels = np.linspace(-1,1,21)*0.1
      # if var[v] in ['Q','DYN_Q']: tres.cnLevels = np.linspace(1,16,16)

      # if var[v] in ['MMF_WTKE','MMF_TKE','MMF_TKES']: 
      #    # tres.cnFillPalette = "BlAqGrWh2YeOrReVi22"
      #    tres.cnFillPalette = "WhiteBlueGreenYellowRed"
      #    # tres.cnLevels = np.logspace( np.log(0.05) , np.log(5), num=30)
      #    tres.cnLevels = np.arange(0.02,1+0.02,0.02)
      #    # tres.cnLevels = np.linspace(0.1,2,21)*0.1

      # if plot_diff and c>0 : 
      #    if var[v]=="U" : tres.cnLevels = np.linspace(-10,10,11)

      if hasattr(tres,'cnLevels') and not (plot_diff and c>0) : 
         tres.cnLevelSelectionMode = "ExplicitLevels"
      else:
         if (var[v] in ['SPTLS','SPQTLS','U','V']) or (plot_diff and c>0) : 
            aboutZero = True
         else:
            aboutZero = False
         
         if plot_diff and c>0 : 
            aboutZero = True
            data_min = diff_data_min
            data_max = diff_data_max
         else:
            data_min = np.min([np.min(d) for d in data_list])
            data_max = np.max([np.max(d) for d in data_list])
         
         cmin,cmax,cint,clev = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=21, \
                                                    returnLevels=True,aboutZero=aboutZero )
         tres.cnLevels = np.linspace(cmin,cmax,num=21)
         tres.cnLevelSelectionMode = "ExplicitLevels"

      if plot_diff and c>0 : 
         if var[v]=="U" : tres.cnLevels = np.linspace(-10,10,11)

      var_str = var[v]
      # if var[v]=="PRECT" : var_str = "Precipitation [mm/day]"

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      # tres.sfXCStartV = min( lat_bins )
      # tres.sfXCEndV   = max( lat_bins )
      # tres.sfXArray = sin_lat_bins
      # tres.sfXCStartV = -1. #np.min( sin_lat_bins )
      # tres.sfXCEndV   =  1. #np.max( sin_lat_bins )

      # lat_tick = np.array([-90,-60,-30,0,30,60,90])
      # res.tmXBMode = "Explicit"
      # res.tmXBValues = np.sin( lat_tick*3.14159/180. )
      # res.tmXBLabels = lat_tick

      # if 'lev' in vars() : tres.sfYCStartV = min( bin_ds['lev'].values )
      # if 'lev' in vars() : tres.sfYCEndV   = max( bin_ds['lev'].values )
      # tres.sfYCStartV = min( bin_ds['lev'].values )
      # tres.sfYCEndV   = max( bin_ds['lev'].values )

      tres.sfXArray = lon_list[c]
      tres.sfYArray = lev_list[c]

      if plot_diff and c >0 :
         plot[ip] = ngl.contour(wks, data_list[c] - data_list[0], tres)
      else:
         plot[ip] = ngl.contour(wks, data_list[c], tres)

      var_str = var[v]
      if plot_diff and c>0 : var_str = var_str+' (diff)'

      # name_str = case_obj.short_name
      name_str = name[c]
      if plot_diff and c==0 : name0 = name_str
      if plot_diff and c >0 : name_str = name_str+' - '+name0

      hs.set_subtitles(wks, plot[ip], name_str, f'{xlat}N', var_str, font_height=0.008)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [num_var,num_case]
layout = [num_case,num_var]
# layout = [num_var,num_case]

if num_var==4 and num_case==1 : layout = [2,2]

ngl.panel(wks,plot,layout,hs.setres_panel())
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
