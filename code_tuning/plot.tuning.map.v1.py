#---------------------------------------------------------------------------------------------------
# commands for regridding CERES-EBAF to model grid:
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne30pg2_scrip.nc 
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne45pg2_scrip.nc 
# cd /gpfs/alpine/scratch/hannah6/cli115/Obs/CERES-EBAF
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne30pg2/${FILE}
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne45pg2/${FILE}
#---------------------------------------------------------------------------------------------------
# map file command for ERA5 - use HICCUP grid file:
# ncremap --map_file=${HOME}/maps/map_721x1440_to_ne30pg2.nc --grd_src=${HOME}/HICCUP/files_grid/scrip_721x1440_n2s.nc --grd_dst=${HOME}/E3SM/data_grid/ne30pg2_scrip.nc 
# Use this script for actual regridding: ~/Research/Obs/ERA/regrid.ERA5.ne30pg2.py
#---------------------------------------------------------------------------------------------------
import os, glob, copy, string, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import cmocean
host = hc.get_host()

# ref_flag,ctl_flag = [],[]
# case,name,clr,dsh,mrk = [],[],[],[],[]
# def add_case(case_in,n=None,d=0,m=1,c='black',ctl=False,ref=False):
#    global name,case,clr,dsh,mrk
#    if n is None: n = '' 
#    case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
#    ref_flag.append(ref); ctl_flag.append(ctl)

ref_flag,ctl_flag = [],[]
case,name,clr,dsh,mrk = [],[],[],[],[]
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,m=1,c='black',ctl=False,ref=False):
   global name,case,clr,dsh,mrk
   if n is None: n = case_in
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   ref_flag.append(ref); ctl_flag.append(ctl)

#-------------------------------------------------------------------------------

obs_data_path = '/gpfs/alpine/scratch/hannah6/cli115/Obs/CERES-EBAF'
use_obs_remap = True; obs_remap_sub = 'clim_ne45pg2'
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne45pg2_scrip.nc'

### INCITE 2022 coupled runs
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO',                      n='E3SMv2',      c='black')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',                       n='E3SM-MMF',    c='red')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QI_3E-05',n='E3SM-MMF alt',c='blue')

### coupled spinup for non-MMF after tuning ensemble
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# add_case('ERA5',p='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5',s='monthly_ne30pg2',c='gray',d=1,ref=True); era5_yr1,era5_yr2 = 1960,1965
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950',   n='E3SMv2',    c='red')
# add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',  n='MMF',       c='blue',p='/ccs/home/hannah6/E3SM/scratch2',s='run')

### INCITE 2022 low-cld exp
add_case('CERES-EBAF',n='CERES-EBAF',c='gray',d=1,ref=True); obs_remap_sub='clim_ne30pg2'
he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1',                         n='MMF',                           c='red')
add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED',                  n='MMF+HV+SED',                    c='green')
add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_5E-04',         n='MMF+HV+SED -qw=>5e-4',          c='blue')
add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_1E-04',         n='MMF+HV+SED -qw=>1e-4',          c='cyan')
add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_5E-04_QI_5E-05',n='MMF+HV+SED -qw=>5e-4 -qi=>5e-5',c='purple')
add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_5E-04_QI_8E-05',n='MMF+HV+SED -qw=>5e-4 -qi=>8e-5',c='pink')

### INCITE 2022 low-cld Cess
# add_case('CERES-EBAF',n='CERES-EBAF',c='gray',d=1,ref=True); obs_remap_sub='clim_ne30pg2'
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_0K', n='MMF +0K', c='blue')
# add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_4K', n='MMF +4K', c='red')

### INCITE 2022 coupled runs
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# # add_case('CERES-EBAF',n='CERES-EBAF',c='gray',d=1,ref=True); obs_remap_sub='clim_ne30pg2'
# # add_case('ERA5',n='ERA5',c='gray',d=1,ref=True); obs_remap_sub='monthly_ne30pg2'; obs_data_path = '/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5'
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950',                                     n='E3SMv2',       c='gray')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO',                               n='E3SMv2-PAERO', c='black')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',                                n='E3SM-MMF',     c='red')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QI_3E-05',         n='E3SM-MMF alt1',c='blue')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QW_5E-04_QI_3E-05',n='E3SM-MMF alt2',c='purple')

# head = 'E3SM.INCITE2022-CESS-T00.ne45pg2.SSTP_0K.F2010-MMF1.NXY_32x32'

# add_case('CERES-EBAF', n='CERES-EBAF'); use_obs_remap = True
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-03_QI_1E-04_VT_0.4',n='control                       ')
# add_case(f'{head}.TN_230_TX_250_QW_1E-03_QI_1E-05_VT_0.3',      n='-Tmin/max=>230/250, -qci=>1e-5, -vt=>0.3')
# add_case(f'{head}.TN_240_TX_273.16_QW_1E-03_QI_1E-05_VT_0.4',   n='-Tmin/max=>240, -qci=>1e-5')

# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_1E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>1e-5',            c='orange')
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_2E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>2e-5',            c='red')
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_3E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>3e-5',            c='cyan')
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_5E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>5e-5',            c='green')
# add_case(f'{head}.TN_240_TX_260_QW_1E-04_QI_3E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>3e-5, -qcw=>1e-4',c='purple')

# add_case(f'{head}.SED.TN_240_TX_260_QW_1E-03_QI_3E-05_VT_0.4',   n='+SED, -Tmin/max=>240/260, -qci=>3e-5',            c='firebrick')
# add_case(f'{head}.SED.TN_240_TX_260_QW_1E-04_QI_3E-05_VT_0.4',   n='+SED, -Tmin/max=>240/260, -qci=>3e-5, -qcw=>1e-4',c='deepskyblue')

#-------------------------------------------------------------------------------
var,plot_var_name,lev_list = [],[],[]
def add_var(var_name,pname=None,lev=0): var.append(var_name); plot_var_name.append(pname); lev_list.append(lev)

# add_var('PRECT')
# add_var('LHFLX')
# add_var('SHFLX')
# add_var('TS')
# add_var('TMQ')
# add_var('CLDLOW')
# add_var('CLDMED')
# add_var('CLDHGH')
# add_var('CLDTOT')
# add_var('TGCLDLWP',pname='Liq Water Path')
# add_var('TGCLDIWP',pname='Ice Water Path')

# add_var('NET_TOA_RAD')
add_var('FSNTOA')
# add_var('FLNT')
# add_var('SWCF')
# add_var('LWCF')

# add_var('NET_CF')
# add_var('FSNTOAC'); add_var('FLUTC')
# add_var('FSNT'); add_var('FLNT')

fig_type = 'png'
fig_file = 'figs_tuning/tuning.map.v1'

lat1,lat2 = -60,60
# lat1,lat2,lon1,lon2 = -45,-45+60,360-120,360-40  # S. America

# htype,first_file,num_files = 'h0',0,12
htype,first_file,num_files = 'h0',0,12*6
# htype,first_file,num_files = 'h0',12*5,12*5

plot_diff,add_diff = True,False

print_stats = True

var_x_case = True

num_plot_col = 2

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.008
# subtitle_font_height = 0.010

wkres = ngl.Resources()
# npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
if plot_diff and add_diff: 
   plot = [None]*(num_var*(num_case*2-1))
elif plot_diff and not add_diff: 
   plot = [None]*(num_var*(num_case-1))
else:
   plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.01
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

scrip_ds = xr.open_dataset(scrip_file_path)
res.cnFillMode    = "CellFill"
res.sfXArray      = scrip_ds['grid_center_lon'].values
res.sfYArray      = scrip_ds['grid_center_lat'].values
res.sfXCellBounds = scrip_ds['grid_corner_lon'].values
res.sfYCellBounds = scrip_ds['grid_corner_lat'].values

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list = []
   glb_avg_list,glb_rms_list = [],[]
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      if case[c]=='CERES-EBAF':
         if use_obs_remap:
            file_list = sorted(glob.glob(f'{obs_data_path}/{obs_remap_sub}/*'))
         else:
            file_list = sorted(glob.glob(f'{obs_data_path}/clim_180x360/*'))
         file_list = file_list[:num_files]
         ds = xr.open_mfdataset(file_list,combine='by_coords',concat_dim='time').isel(nv=0)

         tvar = var[v]
         if var[v]=='NET_TOA_RAD': tvar = 'RESTOA'
         if var[v]=='NET_CF': tvar = 'SWCF'
         data = ds[tvar]
         if var[v]=='NET_CF': data = data + ds['LWCF']

         if use_obs_remap:
            lat,lon = ds['lat'],ds['lon']
            # scrip_ds = xr.open_dataset(os.getenv('HOME')+f'/E3SM/data_grid/{CERES_remap_scrip}.nc')
            area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})
         else:
            data = data.stack(ncol=('lat','lon'))
            lon2D = np.transpose( np.repeat( ds['lon'].values[...,None],len(ds['lat']),axis=1) )
            lat2D =               np.repeat( ds['lat'].values[...,None],len(ds['lon']),axis=1)
            lon = xr.DataArray(lon2D,dims=('lat','lon')).stack(ncol=('lat','lon'))
            lat = xr.DataArray(lat2D,dims=('lat','lon')).stack(ncol=('lat','lon'))
            fv_scrip_ds = xr.open_dataset(os.getenv('HOME')+'/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc')
            area = fv_scrip_ds['grid_area'].rename({'grid_size':'ncol'})
            # convert ncol to DataArray - multiindex causes problems
            data['ncol'] = np.arange(data.shape[1])
            area['ncol'] = np.arange(data.shape[1])
            lat['ncol']  = np.arange(data.shape[1])
            lon['ncol']  = np.arange(data.shape[1])
      
      elif case[c]=='ERA5':
         if not use_obs_remap: exit('ummm... what are you doing?')
         # file_list = sorted(glob.glob(f'{obs_data_path}/{obs_remap_sub}/*'))
         file_list = sorted(glob.glob(f'{case_dir[c]}/{case_sub[c]}/*'))
         file_list = file_list[:num_files]
         ds = xr.open_mfdataset(file_list,combine='by_coords',concat_dim='time').isel(nv=0)

         tvar = None
         if var[v]=='PS':          tvar = 'sp'     # surface_pressure
         if var[v]=='TS':          tvar = 'skt'    # skin_temperature
         if var[v]=='TGCLDLWP':    tvar = 'tclw'   # total_column_cloud_liquid_water
         if var[v]=='TGCLDIWP':    tvar = 'tciw'   # total_column_cloud_ice_water
         if var[v]=='TMQ':         tvar = 'tcw'    # total_column_water
         if var[v]=='FSNTOA':      tvar = 'tsr'    # top_net_solar_radiation
         if var[v]=='FLNT':        tvar = 'ttr'    # top_net_thermal_radiation
         if var[v]=='SHFLX':       tvar = 'msshf'  # mean_surface_sensible_heat_flux
         if var[v]=='LHFLX':       tvar = 'mslhf'  # mean_surface_latent_heat_flux

         if tvar is None:
            if var[v]=='NET_TOA_RAD': tvar = var[v]; data = ds['tsr'] + ds['ttr']
            if tvar is None: raise ValueError(f'variable {var[v]} not found in ERA5 data?')
         else:
            data = ds[tvar]
         
         # unit conversions
         if var[v]=='TS':           data = data - 273.15  # convert to Celsius
         if var[v]=='NET_TOA_RAD':  data = data/86400.    # convert J/m2 to W/m2 (only for monthly)
         if var[v]=='FSNTOA':       data = data/86400.    # convert J/m2 to W/m2 (only for monthly)
         if var[v]=='FLNT':         data = data/86400.*-1 # convert J/m2 to W/m2 (only for monthly)

         lat,lon = ds['lat'],ds['lon']
         area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})

      else:
         data_dir_tmp,data_sub_tmp = None, None
         if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         if case_sub[c] is not None: data_sub_tmp = case_sub[c]
         case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
         # case_obj = he.Case( name=case[c] )
         # case_obj.set_coord_names(var[v])
         if 'lev'  in locals() : case_obj.lev  = lev
         lat  = case_obj.load_data('lat',  htype=htype,num_files=1)
         lon  = case_obj.load_data('lon',  htype=htype,num_files=1)
         area = case_obj.load_data('area', htype=htype,num_files=1).astype(np.double)

         tvar = var[v]
         if var[v]=='NET_CF': tvar = 'SWCF'
         data = case_obj.load_data(tvar,htype=htype,first_file=first_file,num_files=num_files)


         if var[v]=='NET_CF': data = data + case_obj.load_data('LWCF',htype=htype,first_file=first_file,num_files=num_files)

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent=' '*6,compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         data = data.mean(dim='time')
      #-------------------------------------------------------------------------
      # Calculate area-weighted global mean      
      #-------------------------------------------------------------------------
      gbl_mean = ( (data*area).sum() / area.sum() ).values 
      glb_avg_list.append(gbl_mean)
      #-------------------------------------------------------------------------
      # Calculate RMSE - data must be on same grid!
      #-------------------------------------------------------------------------
      if c==0:
         baseline = data
         glb_rms_list.append(0.)
      if c>0:
         rmse = np.sqrt( np.mean( np.square( data.to_masked_array() - baseline.to_masked_array() )))
         glb_rms_list.append(rmse)

      print(        (' '*6)+f'Area Weighted Global Mean : {hc.tcolor.CYAN}{gbl_mean:6.4}{hc.tcolor.ENDC}')
      if c>0: print((' '*6)+f'Root Mean Square Error    : {hc.tcolor.CYAN}{    rmse:6.4}{hc.tcolor.ENDC}')
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      data_list.append( data.values )
      #-------------------------------------------------------------------------
      # save baseline for diff map
      #-------------------------------------------------------------------------
      if plot_diff and c==0: data_baseline = data.copy()

   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------

   # # flip sign of FLUT for visual clarity when comparing LW and SW fluxes
   # if var[v]=='FLUT' and not plot_diff : 
   #    for c in range(num_case): data_list[c] = -1*data_list[c]

   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   
   if plot_diff:
      tmp_data = data_list - data_list[0]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[0]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])

   for c in range(num_case):
      #-------------------------------------------------------------------------
      # Set color map
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      # tres.cnFillPalette = "MPL_viridis"
      tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )

      tmp_clr = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
      if var[v]=='NET_TOA_RAD': tres.cnFillPalette = tmp_clr
      if var[v]=='FSNTOA'     : tres.cnFillPalette = tmp_clr
      if var[v]=='FLUT'       : tres.cnFillPalette = tmp_clr

      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(2,20+2,2)
      
      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         nlev = 21
         aboutZero = False
         clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      if plot_var_name[v] is not None:
         var_str = plot_var_name[v]
      else:
         var_str = var[v]

      lev_str = None
      if lev>0: lev_str = f'{lev}mb'
      if lev<0: lev_str = f'k={(lev*-1)}'
      if lev_str is not None: var_str = f'{lev_str} {var[v]}'
      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      tres.lbLabelBarOn = True
      if use_common_label_bar: tres.lbLabelBarOn = False

      num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
      ip = v*num_case_alt+c if var_x_case else c*num_var+v
      
      # if not plot_diff  or (plot_diff and add_diff) or (plot_diff and c==0) : 
      if not plot_diff  or (plot_diff and add_diff) : 

         # plot[ip]  = ngl.blank_plot(wks,tres)
         plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
         hs.set_subtitles(wks, plot[ip], name[c], '', var_str, font_height=subtitle_font_height)

      #-------------------------------------------------------------------------
      # create difference plot
      #-------------------------------------------------------------------------
      if plot_diff and c>0 :

         # skip the first case when just plot_diff==True
         if (plot_diff and not add_diff):
            ip = v*(num_case-1)+(c-1) if var_x_case else (c-1)*num_var+v
         
         data_list[c] = data_list[c] - data_baseline.values

         tres.cnFillPalette = 'BlueWhiteOrangeRed'
         tres.cnLevelSelectionMode = "ExplicitLevels"
         tres.lbLabelBarOn = True
         
         if hasattr(tres,'cnLevels') : del tres.cnLevels
         if not hasattr(tres,'cnLevels') : 
            if np.min(data_list[c])==np.max(data_list[c]) : 
               print(f'{hc.tcolor.RED}WARNING: Difference is zero!{hc.tcolor.ENDC}')
            else:
               cmin,cmax,cint,clev = ngl.nice_cntr_levels(diff_data_min, diff_data_max,    \
                                                          cint=None, max_steps=21,      \
                                                          returnLevels=True, aboutZero=True )
               tres.cnLevels = np.linspace(cmin,cmax,num=21)

         ipd = ip
         if add_diff and     var_x_case: ipd = ip+(num_case-1)
         if add_diff and not var_x_case: ipd = ip+num_var*(num_case-1)

         plot[ipd] = ngl.contour_map(wks,data_list[c],tres)
         hs.set_subtitles(wks, plot[ipd], name[c], '', var_str, font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

num_case_alt = num_case*2-1 if (plot_diff and add_diff) else num_case
layout = [num_var,num_case_alt] if var_x_case else [num_case_alt,num_var]

if not (plot_diff and add_diff):
   if num_case==1 or num_var==1:
      layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pnl_res = hs.setres_panel()

### use common label bar
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5


ngl.panel(wks,plot,layout,pnl_res)

ngl.end()

hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
