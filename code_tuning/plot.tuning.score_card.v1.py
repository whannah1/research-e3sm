#---------------------------------------------------------------------------------------------------
# create a tuning "score card" full of helpful plots
# commands for regridding CERES-EBAF to model grid:
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne30pg2_scrip.nc 
# ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --grd_src=${HOME}/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc --grd_dst=${HOME}/E3SM/data_grid/ne45pg2_scrip.nc 
# cd /gpfs/alpine/scratch/hannah6/cli115/Obs/CERES-EBAF
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne30pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne30pg2/${FILE}
# FILE=CERES-EBAF_01_climo.nc; ncremap --map_file=${HOME}/maps/map_180x360_to_ne45pg2.nc --in_file=clim_180x360/${FILE} --out_file=clim_ne45pg2/${FILE}
#---------------------------------------------------------------------------------------------------
import os, ngl, xarray as xr, numpy as np, glob, copy
import hapy_common as hc, hapy_E3SM as he, hapy_setres as hs
np.seterr(divide='ignore', invalid='ignore')
np.errstate(divide='ignore', invalid="ignore")
print()

ref_flag,ctl_flag = [],[]
case,name,clr,dsh,mrk = [],[],[],[],[]
case_name,case,case_dir,case_sub,case_grid,clr,dsh,mrk = [],[],[],[],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None,d=0,m=1,c='black',ctl=False,ref=False):
   global name,case,clr,dsh,mrk
   if n is None: n = case_in
   case.append(case_in); name.append(n); dsh.append(d) ; clr.append(c) ; mrk.append(m)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
   ref_flag.append(ref); ctl_flag.append(ctl)


obs_data_path = '/gpfs/alpine/scratch/hannah6/cli115/Obs/CERES-EBAF'
use_obs_remap = True; obs_remap_sub = 'clim_ne45pg2'
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne45pg2_scrip.nc'

### coupled tuning tests for non-MMF
scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# add_case('CERES-EBAF',n='CERES-EBAF',c='gray',d=1,ref=True); obs_remap_sub='clim_ne30pg2'
add_case('ERA5',d='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5',s='monthly_ne30pg2'); era5_yr1,era5_yr2 = 1960,1965
he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950',                                    n='E3SMv2',               c='gray')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO',                              n='E3SMv2 (no-chem)',     c='red')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.001D6.nccons_100.0D6',n='E3SMv2 (no-chem) alt1',c='pink')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.01D6.nccons_200.0D6', n='E3SMv2 (no-chem) alt2',c='purple')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.01D6.nccons_40.0D6',  n='E3SMv2 (no-chem) alt3',c='magenta')
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-PAERO.nicons_0.01D6.nccons_70.0D6',  n='E3SMv2 (no-chem) alt4',c='orange')
add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',                                n='E3SM-MMF',            c='blue')
add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QI_3E-05',         n='E3SM-MMF alt1',       c='aquamarine')
add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1.TN_240_TX_260_QW_5E-04_QI_3E-05',n='E3SM-MMF alt2',       c='cyan')

### coupled spinup for non-MMF after tuning ensemble
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# add_case('ERA5',p='/gpfs/alpine/scratch/hannah6/cli115/Obs/ERA5',s='monthly_ne30pg2',c='gray',d=1,ref=True); era5_yr1,era5_yr2 = 1960,1965
# add_case('E3SM.INCITE2022-CPL-T00.ne30pg2_EC30to60E2r2.WCYCL1950',   n='E3SMv2',    c='red')
# add_case('E3SM.INCITE2022-CPL.ne30pg2_EC30to60E2r2.WCYCL1950-MMF1',  n='E3SM-MMF',  c='blue',p='/ccs/home/hannah6/E3SM/scratch2',s='run')

### UP tuning runs
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# add_case('CERES-EBAF',n='CERES-EBAF',c='gray',d=1,ref=True); obs_remap_sub='clim_ne30pg2'
# add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1',                         n='MMF L125',                            c='red')
# add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED',                  n='MMF L125 +HV+SED',                    c='green')
# add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_5E-04',         n='MMF L125 +HV+SED -qw=>5e-4',          c='blue')
# add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_1E-04',         n='MMF L125 +HV+SED -qw=>1e-4',          c='cyan')
# add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_5E-04_QI_5E-05',n='MMF L125 +HV+SED -qw=>5e-4 -qi=>5e-5',c='purple')
# add_case('E3SM.INCITE2022-LOW-CLD-00.ne30pg2.F2010-MMF1.L125_115.NXY_256x1.HV.SED.QW_5E-04_QI_8E-05',n='MMF L125 +HV+SED -qw=>5e-4 -qi=>8e-5',c='pink')

# head = 'E3SM.INCITE2022-CESS-T00.ne45pg2.SSTP_0K.F2010-MMF1.NXY_32x1'
# add_case('CERES-EBAF', n='CERES-EBAF', c='gray', d=1,ref=True)
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-03_QI_1E-04_VT_0.4',n='control             ',c='black',d=1)
# add_case(f'{head}.TN_240_TX_273.16_QW_1E-03_QI_1E-04_VT_0.4',   n='-Tmin=>240          ',c='red')
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_1E-04_VT_0.4',      n='-Tmin/max=>240/260  ',c='orange')
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-03_QI_1E-04_VT_0.6',n='+vtice=>0.6         ',c='magenta')
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-03_QI_1E-05_VT_0.4',n='-qci=>1e-5          ',c='green')
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-03_QI_1E-06_VT_0.4',n='-qci=>1e-6          ',c='cyan')
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-04_QI_1E-04_VT_0.4',n='-qcw=>1e-4          ',c='blue')
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-04_QI_1E-05_VT_0.4',n='-qcw/-qci=>1e-4/1e-5',c='purple')


# head = 'E3SM.INCITE2022-CESS-T00.ne45pg2.SSTP_0K.F2010-MMF1.NXY_32x32'
# add_case('CERES-EBAF', n='CERES-EBAF', c='gray', d=1,ref=True)
# add_case(f'{head}.TN_253.16_TX_273.16_QW_1E-03_QI_1E-04_VT_0.4',n='control                       ',            c='black',d=1,ctl=True)
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_1E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>1e-5',            c='orange')
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_2E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>2e-5',            c='red')
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_3E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>3e-5',            c='cyan')
# add_case(f'{head}.TN_240_TX_260_QW_1E-03_QI_5E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>5e-5',            c='green')
# add_case(f'{head}.TN_240_TX_260_QW_1E-04_QI_3E-05_VT_0.4',      n='-Tmin/max=>240/260, -qci=>3e-5, -qcw=>1e-4',c='purple')

# add_case(f'{head}.TN_230_TX_250_QW_1E-03_QI_1E-05_VT_0.3',      n='-Tmin/max=>230/250, -qci=>1e-5, -vt=>0.3',c='red')
# add_case(f'{head}.TN_240_TX_273.16_QW_1E-03_QI_1E-05_VT_0.4',   n='-Tmin=>240, -qci=>1e-5',              c='blue')

# add_case(f'{head}.SED.TN_240_TX_260_QW_1E-03_QI_3E-05_VT_0.4',   n='+SED, -Tmin/max=>240/260, -qci=>3e-5',            c='firebrick')
# add_case(f'{head}.SED.TN_240_TX_260_QW_1E-04_QI_3E-05_VT_0.4',   n='+SED, -Tmin/max=>240/260, -qci=>3e-5, -qcw=>1e-4',c='deepskyblue')

### use this for showing sensitivity to time length averaging
# add_case('CERES-EBAF', n='CERES-EBAF', c='gray', d=1,ref=True)
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='E3SM-MMF - 120 months',c='red')
# add_case('CERES-EBAF', n='CERES-EBAF', c='gray', d=1,ref=True)
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='E3SM-MMF -  12 months',c='orange')
# add_case('CERES-EBAF', n='CERES-EBAF', c='gray', d=1,ref=True)
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='E3SM-MMF -   6 months',c='green')
# add_case('CERES-EBAF', n='CERES-EBAF', c='gray', d=1,ref=True)
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='E3SM-MMF -   3 months',c='blue')
# add_case('CERES-EBAF', n='CERES-EBAF', c='gray', d=1,ref=True)
# add_case(f'E3SM.INCITE2021-CMT.GNUGPU.ne45pg2_r05_oECv3.F-MMFXX.NXY_32x32.BVT.MOMFB.02',n='E3SM-MMF -   1 month' ,c='purple')

### INCITE 2022 low-cld Cess
# add_case('CERES-EBAF',n='CERES-EBAF',c='gray',d=1,ref=True); obs_remap_sub='clim_ne30pg2'
# scrip_file_path = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'
# he.default_data_dir = '/ccs/home/hannah6/E3SM/scratch/'
# add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_0K', n='MMF +0K', c='blue')
# add_case('E3SM.INCITE2022-LOW-CLD-CESS.ne30pg2.F2010-MMF1.SSTP_4K', n='MMF +4K', c='red')




pvar,plot_var_name,lev_list = [],[],[]
def add_var(var_name,pname=None,lev=0): pvar.append(var_name); plot_var_name.append(pname); lev_list.append(lev)

# add_var('TMQ')
# add_var('CLDLOW')
# add_var('CLDMED')
# add_var('CLDHGH')
# add_var('CLDTOT')

# add_var('NET_TOA_RAD')
add_var('FSNTOA',pname='TOA Net SW')
# add_var('FLNT'  ,pname='TOA Net LW')
add_var('FLUT'  ,pname='TOA Net LW')
# add_var('SWCF')
# add_var('LWCF')

# add_var('TGCLDLWP',pname='Liq Water Path')
# add_var('TGCLDIWP',pname='Ice Water Path')

# add_var('NET_CF')
# add_var('FSNTOAC'); add_var('FLUTC')
# add_var('FSNT'); add_var('FLNT')


htype,first_file,num_files = 'h0',0,0
# htype,first_file,num_files = 'h0',0,12*6
# htype,first_file,num_files = 'h0',12*5,12*5
# htype,first_file,num_files = 'h1',0,5

fig_file = 'figs_tuning/tuning.score_card.v1'

bin_dlat = 5
bin_dlon = 5

# bounds for meridional mean
lat1,lat2 = -30,30

plot_diff = True

print_stats = False

add_legend = True

subtitle_font_height = 0.005

num_pvar,num_case = len(pvar),len(case)


check_time_length = False

#-------------------------------------------------------------------------------
# plot legend in separate file
#-------------------------------------------------------------------------------
if num_case>1 and add_legend:

   legend_file = fig_file+'.legend'

   wkres = ngl.Resources()
   # npix = 1024 ; wkres.wkWidth,wkres.wkHeight=npix,npix
   lgd_wks = ngl.open_wks('png',legend_file,wkres)

   lgres = ngl.Resources()
   lgres.vpWidthF           = 0.05
   lgres.vpHeightF          = 0.03*num_case
   lgres.lgLabelFontHeightF = 0.008
   lgres.lgLabelFont        = "courier"
   lgres.lgMonoDashIndex    = False
   lgres.lgLineLabelsOn     = False
   lgres.lgLineThicknessF   = 16
   lgres.lgLabelJust        = 'CenterLeft'
   lgres.lgLineColors       = clr
   lgres.lgDashIndexes      = dsh

   labels = name

   if 'INCITE2021-CMT' in case[1]:
      lgres.vpHeightF          = 0.01*num_case
      labels,lclr,ldsh = [],[],[]
      for i in range(len(name)):
         if name[i]!='CERES-EBAF':
            labels.append(name[i]); lclr.append(clr[i]); ldsh.append(dsh[i])
      lgres.lgLineColors       = lclr
      lgres.lgDashIndexes      = ldsh

   for i in range(len(labels)):
      labels[i] = ' '*4+labels[i] 
         
   
   ndcx,ndcy = 0.5,0.65

   pid = ngl.legend_ndc(lgd_wks, len(labels), labels, ndcx, ndcy, lgres)

   ngl.frame(lgd_wks)
   # ngl.end()

   hc.trim_png(legend_file)
   
   # exit()

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------

wkres = ngl.Resources()
npix = 4096 ; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks('png',fig_file,wkres)
zmean_plot = [None]*num_pvar
mmean_plot = [None]*num_pvar
gmean_plot = [None]*num_pvar

res = hs.res_xy()
res.vpHeightF = 0.3
res.xyLineThicknessF = 6

mres = hs.res_xy()
mres.vpHeightF = res.vpHeightF

lres = hs.res_xy()
lres.xyLineThicknessF = 1
lres.xyLineColor = 'gray'

if 'clr' not in locals(): 
   if num_case>1 : clr = np.linspace(2,len( ngl.retrieve_colormap(wks) )-1,num_case,dtype=int)
   else : clr = ['black']
if 'dsh' not in locals(): 
   if num_case>1 : dsh = np.zeros(num_case)
   else : dsh = [0]


scrip_ds = xr.open_dataset(scrip_file_path)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

for v in range(num_pvar):
   print('  var: '+hc.tcolor.MAGENTA+pvar[v]+hc.tcolor.ENDC)
   time_length_list = []
   lat_data_list,lat_bin_list = [],[]
   lon_data_list,lon_bin_list = [],[]
   glb_avg_list = []
   glb_rms_list = []
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)
      
      if 'INCITE2021-CMT' in case[1]:
         if c==0: cnt = 0
         if case[c]=='CERES-EBAF':
            cnt+=1

            if cnt==1: num_files = 12*10
            if cnt==2: num_files = 12
            if cnt==3: num_files = 6
            if cnt==4: num_files = 3
            if cnt==5: num_files = 1

            # if cnt==1: num_files = 12
            # if cnt==2: num_files = 6
            # if cnt==3: num_files = 3
            # if cnt==4: num_files = 1

      # print(); print(f'num_files: {num_files}')

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------
      if case[c]=='CERES-EBAF':
         if use_obs_remap:
            file_list = sorted(glob.glob(f'{obs_data_path}/{obs_remap_sub}/*'))
         else:
            file_list = sorted(glob.glob(f'{obs_data_path}/clim_180x360/*'))
         file_list = file_list[:num_files]
         ds = xr.open_mfdataset(file_list,combine='by_coords',concat_dim='time').isel(nv=0)

         tvar = pvar[v]
         if pvar[v]=='NET_TOA_RAD': tvar = 'RESTOA'
         if pvar[v]=='NET_CF': tvar = 'SWCF'
         data = ds[tvar]
         if pvar[v]=='NET_CF': data = data + ds['LWCF']

         if use_obs_remap:
            lat = ds['lat']
            lon = ds['lon']
            # scrip_ds = xr.open_dataset(os.getenv('HOME')+f'/E3SM/data_grid/{scrip_file_path}.nc')
            area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})
         else:
            data = data.stack(ncol=('lat','lon'))
            lon2D = np.transpose( np.repeat( ds['lon'].values[...,None],len(ds['lat']),axis=1) )
            lat2D =               np.repeat( ds['lat'].values[...,None],len(ds['lon']),axis=1)
            lon = xr.DataArray(lon2D,dims=('lat','lon')).stack(ncol=('lat','lon'))
            lat = xr.DataArray(lat2D,dims=('lat','lon')).stack(ncol=('lat','lon'))
            scrip_ds = xr.open_dataset(os.getenv('HOME')+'/E3SM/data_grid/cmip6_180x360_scrip.20181001.nc')
            area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})
            # convert ncol to DataArray - multiindex causes problems
            data['ncol'] = np.arange(data.shape[1])
            area['ncol'] = np.arange(data.shape[1])
            lat['ncol']  = np.arange(data.shape[1])
            lon['ncol']  = np.arange(data.shape[1])
      
      elif case[c]=='ERA5':
         if not use_obs_remap: exit('ummm... what are you doing?')
         # file_list = sorted(glob.glob(f'{obs_data_path}/{obs_remap_sub}/*'))
         file_list = sorted(glob.glob(f'{case_dir[c]}/{case_sub[c]}/*'))
         file_list = file_list[:num_files]
         ds = xr.open_mfdataset(file_list,combine='by_coords',concat_dim='time').isel(nv=0)

         # print(ds)
         # print(); print(file_list)
         # print()
         # exit()

         tvar = None
         if pvar[v]=='PS':          tvar = 'sp'     # surface_pressure
         if pvar[v]=='TS':          tvar = 'skt'    # skin_temperature
         if pvar[v]=='TGCLDLWP':    tvar = 'tclw'   # total_column_cloud_liquid_water
         if pvar[v]=='TGCLDIWP':    tvar = 'tciw'   # total_column_cloud_ice_water
         if pvar[v]=='TMQ':         tvar = 'tcw'    # total_column_water
         if pvar[v]=='FSNTOA':      tvar = 'tsr'    # top_net_solar_radiation
         if pvar[v]=='FLNT':        tvar = 'ttr'    # top_net_thermal_radiation
         if pvar[v]=='FLUT':        tvar = 'ttr'    # top_net_thermal_radiation
         if pvar[v]=='SHFLX':       tvar = 'msshf'  # mean_surface_sensible_heat_flux
         if pvar[v]=='LHFLX':       tvar = 'mslhf'  # mean_surface_latent_heat_flux

         if tvar is None:
            if pvar[v]=='NET_TOA_RAD': tvar = pvar[v]; data = ds['tsr'] + ds['ttr']
            if tvar is None: raise ValueError(f'variable {pvar[v]} not found in ERA5 data?')
         else:
            data = ds[tvar]
         
         # unit conversions
         if pvar[v]=='TS':           data = data - 273.15  # convert to Celsius
         if pvar[v]=='NET_TOA_RAD':  data = data/86400.    # convert J/m2 to W/m2 (only for monthly)
         if pvar[v]=='FSNTOA':       data = data/86400.    # convert J/m2 to W/m2 (only for monthly)
         if pvar[v]=='FLNT':         data = data/86400.*-1 # convert J/m2 to W/m2 (only for monthly)
         if pvar[v]=='FLUT':         data = data/86400.*-1 # convert J/m2 to W/m2 (only for monthly)

         lat,lon = ds['lat'],ds['lon']
         area = scrip_ds['grid_area'].rename({'grid_size':'ncol'})

      else:
         data_dir_tmp,data_sub_tmp = None, None
         if case_dir[c] is not None: data_dir_tmp = case_dir[c]
         if case_sub[c] is not None: data_sub_tmp = case_sub[c]
         case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp  )
         # case_obj = he.Case( name=case[c] )
         # case_obj.set_coord_names(pvar[v])
         if 'lev'  in locals() : case_obj.lev  = lev
         lat  = case_obj.load_data('lat',  htype=htype,num_files=1)
         lon  = case_obj.load_data('lon',  htype=htype,num_files=1)
         area = case_obj.load_data('area', htype=htype,num_files=1).astype(np.double)

         tvar = pvar[v]
         if pvar[v]=='NET_CF': tvar = 'SWCF'
         data = case_obj.load_data(tvar,htype=htype,first_file=first_file,num_files=num_files)

         if pvar[v]=='NET_CF': data = data + case_obj.load_data('LWCF',htype=htype,first_file=first_file,num_files=num_files)

      hc.print_time_length(data.time,indent=(' '*6),print_span=False)

      # make sure time dimension lengths are identical
      if check_time_length:
         time_length_list.append(len(data.time))
         if len(time_length_list)>=2:
            if time_length_list.count(time_length_list[0]) != len(time_length_list): 
               print(f'time_length_list: {time_length_list}')
               exit(f'{hc.tcolor.RED}ERROR: time dimension lengths are not equal!{hc.tcolor.ENDC}')

      # print helpful statistics as a santiy check
      if print_stats: hc.print_stat(data,name=pvar[v],stat='naxsh',indent=(' '*6),compact=True)

      # Average in time
      data = data.mean(dim='time')

      # load to avoid dask array
      data.load(); lat.load(); lon.load(); area.load()

      #-------------------------------------------------------------------------
      # Calculate area-weighted global mean      
      #-------------------------------------------------------------------------
      gbl_mean = ( (data*area).sum() / area.sum() ).values 
      glb_avg_list.append(gbl_mean)
      
      #-------------------------------------------------------------------------
      # Calculate RMSE - data must be on same grid!
      #-------------------------------------------------------------------------
      if 'INCITE2021-CMT' in case[1]:
         if case[c]=='CERES-EBAF' :
            baseline = data
            glb_rms_list.append(0.)
         else:
            rmse = np.sqrt( np.mean( np.square( data.to_masked_array() - baseline.to_masked_array() )))
            glb_rms_list.append(rmse)

      else:

         if c==0:
            baseline = data
            glb_rms_list.append(0.)
         if c>0:
            rmse = np.sqrt( np.mean( np.square( data.to_masked_array() - baseline.to_masked_array() )))
            glb_rms_list.append(rmse)

      


      print(        (' '*6)+f'Area Weighted Global Mean : {hc.tcolor.CYAN}{gbl_mean:6.4}{hc.tcolor.ENDC}')
      if c>0: print((' '*6)+f'Root Mean Square Error    : {hc.tcolor.RED }{    rmse:6.4}{hc.tcolor.ENDC}')
      #-------------------------------------------------------------------------
      # Calculate zonal mean
      #-------------------------------------------------------------------------
      lat_bin_ds = hc.bin_YbyX( data, lat, bin_min=-90, bin_max=90, bin_spc=bin_dlat, wgt=area )
      lat_bin_list.append(np.sin(lat_bin_ds['bins'].values*np.pi/180.))
      lat_data_list.append( lat_bin_ds['bin_val'].values )

      #-------------------------------------------------------------------------
      # Calculate zonal mean
      #-------------------------------------------------------------------------
      # reduce to equatorial subset
      mask = xr.DataArray( np.ones([len(lat)],dtype=bool), dims=('ncol') )
      mask = mask & (lat>=lat1) & (lat<=lat2)

      data = data.where( mask, drop=True)
      area = area.where( mask, drop=True)
      lon  =  lon.where( mask, drop=True)

      lon_bin_ds = hc.bin_YbyX( data, lon, bin_min=bin_dlon, bin_max=360, bin_spc=bin_dlon, wgt=area )

      lon_bin_list.append(lon_bin_ds['bins'].values)
      lon_data_list.append( lon_bin_ds['bin_val'].values )

      # make a copy to double length of longitude
      nlon = len(lon_bin_list[c])
      tmp_bins = np.empty(nlon*2)
      tmp_bins[0:nlon] = lon_bin_list[c]-np.max(lon_bin_list[c])
      tmp_bins[nlon:]  = lon_bin_list[c]+bin_dlon
      lon_bin_list[c] = tmp_bins
      tmp_data = np.empty(nlon*2)
      tmp_data[0:nlon] = lon_data_list[c]
      tmp_data[nlon:]  = lon_data_list[c]
      lon_data_list[c] = tmp_data

      #-------------------------------------------------------------------------
      # Clean up
      #-------------------------------------------------------------------------
      if 'area' in locals(): del area

   #----------------------------------------------------------------------------
   # Take difference from first case
   #----------------------------------------------------------------------------
   if plot_diff: 
      lat_data_baseline = lat_data_list[0]
      lon_data_baseline = lon_data_list[0]
      for c in range(num_case): 
         if 'INCITE2021-CMT' in case[1] and 'INCITE2021-CMT' not in case[c] :
               lat_data_baseline = lat_data_list[c]
               lon_data_baseline = lon_data_list[c]
         lat_data_list[c] = lat_data_list[c] - lat_data_baseline
         lon_data_list[c] = lon_data_list[c] - lon_data_baseline
   
   #----------------------------------------------------------------------------
   # Create zonal mean plot
   #----------------------------------------------------------------------------
   res.tiYAxisString = ''
   res.tiXAxisString = 'sin( Latitude )'
   res.trXMinF,res.trXMaxF = -1.,1.
   lat_tick = np.array([-90,-60,-30,0,30,60,90])
   res.tmXBMode = 'Explicit'
   res.tmXBValues = np.sin( lat_tick*3.14159/180. )
   res.tmXBLabels = lat_tick
   res.trYMinF = np.min([np.nanmin(d) for d in lat_data_list])
   res.trYMaxF = np.max([np.nanmax(d) for d in lat_data_list])
   for c in range(num_case):
      res.xyLineColor,res.xyDashPattern = clr[c],dsh[c]
      tplot = ngl.xy(wks, lat_bin_list[c], np.ma.masked_invalid( lat_data_list[c] ), res)
      if c==0:
         zmean_plot[v] = tplot
      else:
         ngl.overlay( zmean_plot[v], tplot )
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   if plot_var_name[v] is not None:
      var_str = plot_var_name[v]
   else:
      var_str = pvar[v]

   hs.set_subtitles(wks, zmean_plot[v], '', '', var_str, font_height=subtitle_font_height)

   #----------------------------------------------------------------------------
   # Create meridional mean plot
   #----------------------------------------------------------------------------
   # res.tiYAxisString = ''
   # res.tiXAxisString = 'Longitude'
   # # res.trXMinF,res.trXMaxF,lon_tick_vals = -180,180,[-180,-270,-90,0,90,270,180]
   # res.trXMinF,res.trXMaxF,lon_tick_vals = 0,360,[0,90,270,180,360]
   # res.tmXBMode = 'Explicit'
   # res.tmXBValues = np.array(lon_tick_vals)
   # res.tmXBLabels = lon_tick_vals
   # res.trYMinF = np.min([np.nanmin(d) for d in lon_data_list])
   # res.trYMaxF = np.max([np.nanmax(d) for d in lon_data_list])
   # for c in range(num_case):
   #    res.xyLineColor,res.xyDashPattern = clr[c],dsh[c]
   #    tplot = ngl.xy(wks, lon_bin_list[c], np.ma.masked_invalid( lon_data_list[c] ), res)
   #    if c==0:
   #       mmean_plot[v] = tplot
   #    else:
   #       ngl.overlay( mmean_plot[v], tplot )
   # hs.set_subtitles(wks, mmean_plot[v], '', '', pvar[v], font_height=subtitle_font_height)

   #----------------------------------------------------------------------------
   # Create global mean plot
   #----------------------------------------------------------------------------
   mres.tiYAxisString = 'RMSE'
   mres.tiXAxisString = 'Area-Weighted Global Mean'
   mres.tmXBMode = 'Automatic'
   mres.xyMarkLineMode = 'Markers'
   mres.xyMonoMarkerColor = False
   mres.xyMarkerSizeF = 0.015
   mres.xyMarker= 16

   mxmin = np.min([np.nanmin(d) for d in glb_avg_list[:] ])
   mxmax = np.max([np.nanmax(d) for d in glb_avg_list[:] ])
   mymin = np.min([np.nanmin(d) for d in glb_rms_list[1:] ])
   mymax = np.max([np.nanmax(d) for d in glb_rms_list[1:] ])

   buffer_frac = 0.3
   dx,dy = np.absolute(mxmax-mxmin),np.absolute(mxmax-mxmin)
   mres.trXMinF = mxmin-dx*buffer_frac
   mres.trXMaxF = mxmax+dx*buffer_frac
   mres.trYMinF = mymin-dy*buffer_frac
   mres.trYMaxF = mymax+dy*buffer_frac

   # Create plot with all markers to set bounds
   gmean_plot[v] = ngl.xy(wks, glb_avg_list, glb_rms_list, mres)


   # overlay to get colors
   for c in range(num_case):
      mres.xyMarkerColor = clr[c]
      xx = glb_avg_list[c]
      yy = glb_rms_list[c]
      ngl.overlay( gmean_plot[v], ngl.xy(wks, [xx,xx], [yy,yy], mres) )

   hs.set_subtitles(wks,gmean_plot[v], '', '', pvar[v], font_height=subtitle_font_height)

   # add reference lines
   xx,yy = [0,0],[-1e5,1e5]
   ngl.overlay( gmean_plot[v], ngl.xy(wks, xx, yy, lres) )
   ngl.overlay( gmean_plot[v], ngl.xy(wks, yy, xx, lres) )

   # add line for obs global mean
   tres = copy.deepcopy(lres)
   lres.xyLineThicknessF = 4
   lres.xyLineColor = clr[0]
   lres.xyDashPattern = dsh[0]
   xx = [1,1]*glb_avg_list[0]
   ngl.overlay( gmean_plot[v], ngl.xy(wks, xx, yy, lres) )

   #----------------------------------------------------------------------------
   # add arrows to scatter plot to visually organize how experiments differ from control
   #----------------------------------------------------------------------------
   # ares = hs.res_xy()
   # ares.xyLineThicknessF = 1
   # ares.xyLineColor = 'black'
   # c0 = np.where(ctl_flag)[0][0]
   # i1,i2 = 2,18
   # for c in range(num_case):
   #    if not ref_flag[c] and not ctl_flag[c]:
   #       xx = np.linspace( glb_avg_list[c0], glb_avg_list[c], num=20)
   #       yy = np.linspace( glb_rms_list[c0], glb_rms_list[c], num=20)
   #       ngl.overlay( gmean_plot[v], ngl.xy(wks, xx[i1:i2], yy[i1:i2], ares) )

   #       # ax0, ay0 = ngl.datatondc(gmean_plot[v], xx[i2-1], yy[i2-1])

   #       slope_angle = np.arctan2( yy[1]-yy[0] ,  xx[1]-xx[0] )
   #       for i in range(2):
   #          if i==0: da = -np.pi+np.pi/4.
   #          if i==1: da = -np.pi-np.pi/4.
   #          ax = np.array([ xx[i2-1], xx[i2-1] + 0.3*np.cos(slope_angle+da) ])
   #          ay = np.array([ yy[i2-1], yy[i2-1] + 0.3*np.sin(slope_angle+da) ])
   #          ngl.overlay( gmean_plot[v], ngl.xy(wks, ax, ay, ares) )
   
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# first combine plots
num_plot = 2
plot = [None]*num_pvar*num_plot
for v in range(num_pvar):
   pcnt = 0
   pcnt+=1;plot[num_pvar*(pcnt-1)+v] = zmean_plot[v]
   # pcnt+=1;plot[num_pvar*(pcnt-1)+v] = mmean_plot[v]
   pcnt+=1;plot[num_pvar*(pcnt-1)+v] = gmean_plot[v]

# Specify layout
layout = [num_plot,num_pvar]

pres = hs.setres_panel()
pres.nglFrame = False
if add_legend: pres.nglPanelRight = 0.5
ngl.panel(wks,plot,layout,pres)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
ngl.frame(wks)
ngl.end()

hc.trim_png(fig_file)

hc.trim_png(legend_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------