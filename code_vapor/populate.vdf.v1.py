#!/usr/bin/env python
import os, subprocess as sp
vapor_path = '/Applications/vapor.app/Contents/MacOS'

vapor_version = 3

case = 'E3SM.HR-OUTPUT.ne30pg2_ne30pg2.F-MMF1.CRMNX_256.CRMDX_500.RADNX_8.BVT.00'

var_names = ['PS' ,'TS','LHFLX','CLDLIQ','CLDICE']

date_str = '0001-01-01'

reg_name = 'WPAC'

nt = 1

idir = os.getenv('HOME')+f'/E3SM/scratch/{case}/vapor'

ifile = f'{idir}/{case}.{date_str}.reduced.{reg_name}.nc'

#===================================================================================================
#===================================================================================================
class tcolor:
   ENDC,RED,GREEN,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[35m','\033[36m'
def run_cmd(cmd,suppress_output=False,execute=True):
   if suppress_output : cmd = cmd + ' > /dev/null'
   msg = tcolor.GREEN + cmd + tcolor.ENDC
   print(f'\n{msg}')
   if execute: os.system(cmd)
   return
#===================================================================================================
#===================================================================================================

varstr  = " "+":".join(var_list)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
cmd = f'{vapor_path}/cfvdccreate'
cmd += f' -force'
cmd += f' -vars {varstr} '
cmd += f' {ifile} {ofile}'
print("\n"+cmd)
sp.call(cmd,shell=True)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
cmd = f'{vapor_path}/cf2vdc'
cmd += f' -numts {nt}'
cmd += f' -vars {varstr} '
cmd += f' {ifile} {ofile}'
print(cmd)
sp.call(cmd,shell=True)


#-------------------------------------------------------------------------------
print("\noutput file: "+ofile+"\n")

#===================================================================================================
#===================================================================================================

