#-------------------------------------------------------------------------------
# create a regionally reduced dataset from E3SM output
# 
# use variations on this command for regridding:
#   CASE=??? ; var=CLDICE; ncremap -v $var -m /project/projectdirs/acme/mapping/maps/map_ne120np4_to_fv800x1600_bilin.20160301.nc 
#   -i ${CASE}.cam.h2.0002-09-12-00000.nc 
#   -o ${CASE}.cam.h2.0002-09-12-00000.$var.remap.nc
#-------------------------------------------------------------------------------
import os, xarray as xr, numpy as np

var_names = ['PS' ,'TS','LHFLX','CLDLIQ','CLDICE']
case = 'E3SM.HR-OUTPUT.ne30pg2_ne30pg2.F-MMF1.CRMNX_256.CRMDX_500.RADNX_8.BVT.00'

date_str = '0001-01-01'

#-------------------------------------------------------------------------------
### Select the region for reducing the data
# reg_name,clat,clon,dx = 'WPAC',  15, 140, 15
reg_name,clat,clon,dx = 'FLORIDA', 27, 360-80, 15
#-------------------------------------------------------------------------------

lat1,lat2,lon1,lon2 = clat-dx,clat+dx,clon-dx,clon+dx

idir = os.getenv('HOME')+f'/E3SM/scratch/{case}/data_regrid'
odir = os.getenv('HOME')+f'/E3SM/scratch/{case}/vapor'

ifile = f'{idir}/{case}.cam.h1.{date_str}-00000.remap.nc'
ofile = f'{odir}/{case}.{date_str}.reduced.{reg_name}.nc'

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
print()
print(f'  ifile: {ifile}')
print(f'  ofile: {ofile}')
print()

ds = xr.open_dataset(ifile)

print(ds)
exit()

ds_out = xr.Dataset()

#-------------------------------------------------------------------------------
# load and reduce data
#-------------------------------------------------------------------------------

# regional mask
reg_mask = (ds_tmp.coords['lat']>=lat1)&(ds_tmp.coords['lat']<=lat2) \
          &(ds_tmp.coords['lon']>=lon1)&(ds_tmp.coords['lon']<=lon2)

# Load height for converting to height coordinate
Z = ds_tmp['Z3'].where(reg_mask,drop=True)

for var in var_names:

   if 'CRM_' in tvar :

      derived = False
      if var=='CRM_QCI': derived=True; X = ds_tmp[CRM_QC][:,:,0,:,0] + ds_tmp[CRM_QI][:,:,0,:,0]
      if not derived : X = ds_tmp[tvar][:,:,0,:,0]

      # rename dimensions
      X = X.rename({'crm_nz':'z','crm_nx':'x'})

      # build coordinates for CRM data
      x_coord = np.arange(0, X.shape[2], 1) 
      z_coord = ds_tmp['Z3'][:,:,0].mean(dim='time') /1e3
      z1 = len(z_coord)-X.shape[1]
      X = X.assign_coords( z=z_coord.values[z1:].astype('float') )
      X = X.assign_coords( x=x_coord.astype('float') )
      X.coords['z'].attrs['units']     = 'km'
      X.coords['z'].attrs['lon_name']  = 'height'
      X.coords['z'].attrs['axis']      = 'Y'
      X.coords['x'].attrs['units']     = 'km'
      X.coords['x'].attrs['lon_name']  = 'x'
      X.coords['x'].attrs['axis']      = 'X'

   else:
      # Convert to height coords

      X = ds_tmp[tvar].where(reg_mask,drop=True)

ds_out[var] = X



print(f'\n{ofile}\n')

# write new dataset to file
ds_out.to_netcdf(path=ofile, mode='w')


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
