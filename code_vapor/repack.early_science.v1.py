#-------------------------------------------------------------------------------
# This script creates a regionally reduced dataset from E3SM output
# v1 - use raw, unstructured grid data 
# v2 - use list of NCO commands to regrid and subset then combine the resulting files
#-------------------------------------------------------------------------------
import os
import ngl
import xarray as xr
import numpy as np
import custom_generic as cg
# import custom_ESMF as ce
home = os.getenv("HOME")
print()


var_names = ["PS","TS","LHFLX","TMQ"   \
            ,"CLDLIQ","CLDICE"   \
            ,"CRM_W","CRM_QCI" \
            ]


case = "earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m.20190329"
date_str = "0002-09-12"
ifile_2d = home+"/Data/E3SM_local/"+case+"/atm/"+case+".cam.h1."+date_str+"-00000.nc"
ifile_3d = home+"/Data/E3SM_local/"+case+"/atm/"+case+".cam.h2."+date_str+"-00000.nc"
ifile_sc = home+"/Data/E3SM_local/"+case+"/atm/"+case+".cam.h3."+date_str+"-00000.nc"

ofile = home+"/Data/E3SM_local/"+case+"/vapor/"+case+"."+date_str+".reduced.nc"

lat1 = 0
lat2 = 60
lon1 = 360-120
lon2 = 360-80

# ARM site = 97.8w - 97.2w  36.27n - 36.75n

crm_reg_str = "_97.8w_to_97.2w_36.27n_to_36.75n"

#-------------------------------------------------------------------------------
# Set up regridding stuff
#-------------------------------------------------------------------------------
# grid = ce.create_grid_rectilinear(lat1,lat2,lon1,lon2,0.25)
# print(grid)
# exit()

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
print("  "+ifile_3d)
print("  "+ifile_2d)
print("  "+ifile_sc)
print()

ds_2d = xr.open_dataset(ifile_2d)
ds_3d = xr.open_dataset(ifile_3d)
ds_sc = xr.open_dataset(ifile_sc)

ds_out = xr.Dataset()

# Setup condition for masking regional subset
lat_cond = np.logical_and( ds_2d.lat>=lat1, ds_2d.lat<=lat2 )
lon_cond = np.logical_and( ds_2d.lon>=lon1, ds_2d.lon<=lon2 )
reg_cond = np.logical_and(lat_cond,lon_cond)

# load and reduce data, combine into new dataset
for var in var_names :
   tvar = var
   if "CRM_" in tvar : tvar = var+crm_reg_str

   if tvar in ds_3d.variables : ds_tmp = ds_3d
   if tvar in ds_2d.variables : ds_tmp = ds_2d
   if tvar in ds_sc.variables : ds_tmp = ds_sc

   if "CRM_" in tvar :
      derived = False
      if var=="CRM_QCI" : 
         ds_tmp = ds_sc
         tvar1 = "CRM_QC"+crm_reg_str
         tvar2 = "CRM_QI"+crm_reg_str
         X = ds_tmp[tvar1][:,:,0,:,:] + \
             ds_tmp[tvar2][:,:,0,:,:]
         derived = True
      if not derived : X = ds_tmp[tvar][:,:,0,:,:]
   else:
      X = ds_tmp[tvar].where(reg_cond,drop=True)

   print("\nvar: "+variables)
   # print(X)
   print(X.shape)

   ds_out[var] = X

# exit()

# write new dataset to file
ds_out.to_netcdf(path=ofile,mode='w')

print("\n"+ofile+"\n")

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
