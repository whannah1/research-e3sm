#-------------------------------------------------------------------------------
# This script creates a regionally reduced dataset from E3SM output
# v1 - use raw, unstructured grid data 
# v2 - use regridded data
# 
# use variations on this command for regridding:
#   var=CLDICE; ncremap -v $var -m /project/projectdirs/acme/mapping/maps/map_ne120np4_to_fv800x1600_bilin.20160301.nc 
#   -i earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m.20190329.cam.h2.0002-09-12-00000.nc 
#   -o earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m.20190329.cam.h2.0002-09-12-00000.$var.remap.nc
#-------------------------------------------------------------------------------
import os
import xarray as xr
import numpy as np
# import custom_generic as cg
home = os.getenv("HOME")
print()

var_names = ["PS" ,"TS","LHFLX","TMQ"   \
            ,"CLDLIQ","CLDICE"   \
            # ,"CRM_W","CRM_QCI" \
            ]


case = "earlyscience.FC5AV1C-H01A.ne120.sp1_64x1_1000m.20190329"
date_str = "0002-09-12"
idir = home+"/Data/E3SM_local/"+case+"/atm/"
# idir = "/project/projectdirs/m3312/whannah/early_science/"
ifile_2d = idir+case+".cam.h1."+date_str+"-00000.remap.nc"
# ifile_3d = idir+case+".cam.h2."+date_str+"-00000.nc"
ifile_sc = idir+case+".cam.h3."+date_str+"-00000.nc"

odir = home+"/Data/E3SM_local/"+case+"/vapor/"
# odir = idir
ofile = odir+case+"."+date_str+".reduced.WPAC.nc"

# lat1 = 0
# lat2 = 60
# lon1 = 360-120
# lon2 = 360-80

# lat1 = 5
# lat2 = 30
# lon1 = 205
# lon2 = 235

lat1 = 10
lat2 = 30
lon1 = 130
lon2 = 155

# ARM site = 97.8w - 97.2w  36.27n - 36.75n
crm_reg_str = "_97.8w_to_97.2w_36.27n_to_36.75n"

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# print("  "+ifile_3d)
print("  "+ifile_2d)
print("  "+ifile_sc)
print()

ds_2d = xr.open_dataset(ifile_2d)
# ds_3d = xr.open_dataset(ifile_3d)
ds_sc = xr.open_dataset(ifile_sc)

ds_out = xr.Dataset()

# lat_cond = np.logical_and( ds_2d.lat>=lat1, ds_2d.lat<=lat2 )
# lon_cond = np.logical_and( ds_2d.lon>=lon1, ds_2d.lon<=lon2 )
# reg_mask = np.logical_and(lat_cond,lon_cond)

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

# load and reduce data, combine into new dataset
for var in var_names :
   print()
   print("var: "+var)

   tvar = var
   if var=="CRM_QCI" : tvar = "CRM_QC"
   if "CRM_" in tvar : tvar = tvar+crm_reg_str

   found = False
   ifile_3d = idir+case+".cam.h2."+date_str+"-00000."+var+".remap.nc"
   if os.path.isfile(ifile_3d) :
      ds_3d = xr.open_dataset(ifile_3d)
      if tvar in ds_3d.variables : ds_tmp = ds_3d; found=True
   if tvar in ds_2d.variables : ds_tmp = ds_2d; found=True
   if tvar in ds_sc.variables : ds_tmp = ds_sc; found=True

   if not found : 
      print(tvar+" not found in available files!")
      exit()

   if "CRM_" in tvar :
      derived = False
      if var=="CRM_QCI" : 
         ds_tmp = ds_sc
         tvar1 = "CRM_QC"+crm_reg_str
         tvar2 = "CRM_QI"+crm_reg_str
         X = ds_tmp[tvar1][:,:,0,:,0] + \
             ds_tmp[tvar2][:,:,0,:,0]
         derived = True
      if not derived : X = ds_tmp[tvar][:,:,0,:,0]
      X = X.rename({'crm_nz':'z','crm_nx':'x'})
      # build coordinates for CRM data
      x_coord = np.arange(0, X.shape[2], 1) 
      z_coord = ds_tmp['Z3'+crm_reg_str][:,:,0].mean(dim='time') /1e3
      z1 = len(z_coord)-X.shape[1]
      X = X.assign_coords( z=z_coord.values[z1:].astype('float') )
      X = X.assign_coords( x=x_coord.astype('float') )
      X.coords['z'].attrs['units']     = "km"
      X.coords['z'].attrs['lon_name']  = "height"
      X.coords['z'].attrs['axis']      = "Y"
      X.coords['x'].attrs['units']     = "km"
      X.coords['x'].attrs['lon_name']  = "x"
      X.coords['x'].attrs['axis']      = "X"
   else:
      reg_mask = (ds_tmp.coords['lat']>=lat1) \
                &(ds_tmp.coords['lat']<=lat2) \
                &(ds_tmp.coords['lon']>=lon1) \
                &(ds_tmp.coords['lon']<=lon2)
      X = ds_tmp[tvar].where(reg_mask,drop=True)

   
   print("dim: "+str(X.shape))
   # print(X)

   ds_out[var] = X

# print("\nExiting...")
# exit()

print("\n"+ofile+"\n")

# write new dataset to file
ds_out.to_netcdf(path=ofile,mode='w')



#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
