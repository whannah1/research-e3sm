import os, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import copy, string
import cmocean
host = hc.get_host()

name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,d=None,s=None,g=None,c=None):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); case_dir.append(d); case_sub.append(s); case_grid.append(g)

#-------------------------------------------------------------------------------

### INCITE-AQUA-RRM
# add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.CRMDX_2000.00',  n='MMF dx=2km')
# add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.CRMDX_200.00',   n='MMF dx=200m')
add_case('E3SM.AQUA-RRM-TEST.F-MMFXX-AQP1.ne30x3pg2.NXY_32x1.DXSCL_12e-3.00', n='MMF dxscl=12e-3',d='/global/homes/w/whannah/E3SM/scratch')
scrip_file_path = '/global/homes/w/whannah/E3SM/data_grid/RRM_cubeface_grad_ne30x3pg2_scrip.nc'

#-------------------------------------------------------------------------------

var,lev_list = [],[]
def add_var(var_name,lev=0): var.append(var_name); lev_list.append(lev)


# add_var('PRECT')
# add_var('TMQ')
add_var('TGCLDLWP')
# add_var('TGCLDIWP')
# add_var('FSNT');add_var('FLNT')
# add_var('FSNS');add_var('FLNS')
# add_var('FSNTOA')
# add_var('UBOT')
# add_var('U',lev=-59)

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_vis/ECRP.aqua-rrm'

#-------------------------------------------------------------------------------

# htype,years,months,first_file,num_files = 'h1',[],[],120,1
htype,years,months,first_file,num_files = 'h0',[],[],0,0


use_remap,remap_grid = False,'90x180' # 90x180 / 180x360

print_stats = True

var_x_case = True

num_plot_col = len(var)#1

# use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var,num_case = len(var),len(case)

subtitle_font_height = 0.01

if 'scrip_file_path' not in locals(): scrip_file_path = None

wkres = ngl.Resources()
# npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
npix = 4096; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*(num_var*num_case)
   
res = hs.res_contour_fill_map()
if 'lat1' in vars() : res.mpMinLatF = lat1; res.mpMaxLatF = lat2
if 'lon1' in vars() : res.mpMinLonF = lon1; res.mpMaxLonF = lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.01
res.tmXBOn                       = False
res.tmYLOn                       = False
# res.mpGeophysicalLineColor       = 'white'

res.lbLabelBarOn = False

res.mpGridAndLimbOn     = False
res.mpPerimOn           = False
res.mpProjection        = "Orthographic"
res.cnCellFillEdgeColor = "black"

res.mpOutlineBoundarySets = 'NoBoundaries'

res.mpCenterLonF = 0

# res.mpCenterLonF = 15
# res.mpCenterLatF = -5

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+hc.tcolor.MAGENTA+var[v]+hc.tcolor.ENDC)
   data_list,area_list,lat_list,lon_list = [],[],[],[]
   glb_avg_list = []
   if 'lev_list' in locals(): lev = lev_list[v]
   for c in range(num_case):
      print('    case: '+hc.tcolor.GREEN+case[c]+hc.tcolor.ENDC)

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      case_obj.set_coord_names(var[v])

      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      aname = case_obj.area_name
      area = case_obj.load_data(aname,htype=htype)

      data = case_obj.load_data(var[v],htype=htype,ps_htype=htype,
                                      years=years,months=months,lev=lev,
                                      first_file=first_file,num_files=num_files,
                                      use_remap=use_remap,remap_str=f'remap_{remap_grid}')

      # Get rid of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      # print stats before time averaging
      if print_stats: hc.print_stat(data,name=var[v],stat='naxsh',indent='    ',compact=True)

      # average over time dimension
      if 'time' in data.dims : 
         hc.print_time_length(data.time,indent=' '*6)
         data = data.mean(dim='time')
         # data = data.isel(time=0)

      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      #-------------------------------------------------------------------------
      if 'area' in locals() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(hc.tcolor.CYAN+f'      Area Weighted Global Mean : {gbl_mean:6.4}'+hc.tcolor.ENDC)
         glb_avg_list.append(gbl_mean)
      #-------------------------------------------------------------------------
      # append to data lists
      #-------------------------------------------------------------------------
      data_list.append( data.values )

      if 'area' in locals() : area_list.append( area.values )


   #------------------------------------------------------------------------------------------------
   # Plot averaged data
   #------------------------------------------------------------------------------------------------
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   for c in range(num_case):

      data_dir_tmp,data_sub_tmp = None, None
      if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'
      if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      
      case_obj = he.Case( name=case[c], data_dir=data_dir_tmp, data_sub=data_sub_tmp )
      case_obj.set_coord_names(var[v])
      if 'AQUA-RRM-TEST' in case[c]: case_obj.grid = 'RRM_cubeface_grad_ne4x5_pg2'

      #-------------------------------------------------------------------------
      # Set colors
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)
      tres.cnFillPalette = "MPL_viridis"
      # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      if var[v] in ['P-E']                      : tres.cnFillPalette = "BlueWhiteOrangeRed"
      if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = "CBR_wet"
      if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['DYN_QLIQ']                 : tres.cnFillPalette = "MPL_viridis"
      if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'BlueWhiteOrangeRed'
      # if var[v] in ['TS','PS']                  : tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
      # if var[v] in ['TS','PS'] : tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      if var[v] in ['U','V','UBOT','VBOT','U850','V850','U200','V200',
                    'MMF_CVT_TEND_T','MMF_CVT_TEND_Q']: 
         tres.cnFillPalette = "BlueWhiteOrangeRed"
         # tres.cnFillPalette = 'MPL_RdYlBu'

      #-------------------------------------------------------------------------
      # Set explicit contour levels
      #-------------------------------------------------------------------------
      if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(2,20+2,2)
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.arange(5,100+5,5) # for std dev
      # if var[v] in ['PRECT','PRECC','PRECL']   : tres.cnLevels = np.logspace( -2, 1.4, num=60).round(decimals=2)
      # if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
      if var[v]=='P-E'                 : tres.cnLevels = np.linspace(-10,10,21)
      if var[v]=='RH'                  : tres.cnLevels = np.arange(10,100+1,1)
      if var[v]=='TS'                  : tres.cnLevels = np.arange(-55,35+2,2)

      # if var[v] in ['TGCLDIWP']: tres.cnLevels = np.arange(0.005,0.155,0.01)
      # if var[v] in ['TGCLDLWP']: tres.cnLevels = np.arange(0.01,0.25,0.015)

      if var[v]=='TGCLDLWP': tres.cnLevels = np.logspace( -2 , 0.25, num=60)
      if var[v]=='TGCLDIWP': tres.cnLevels = np.logspace( -10 , 2, num=60)
      # if var[v]=='TGCLDLWP': tres.cnLevels = np.arange(0.01,1.,0.015)

      if var[v] in ['U','V']: 
         if lev==850: tres.cnLevels = np.arange(-20,20+2,2)
         if lev==200: tres.cnLevels = np.arange(-60,60+6,6)

      #-------------------------------------------------------------------------
      # set non-explicit contour levels
      #-------------------------------------------------------------------------
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         tres.cnLevelSelectionMode = 'AutomaticLevels'
      # else:
      #    nlev = 41
      #    aboutZero = False
      #    if var[v] in ['U','V','VOR','DIV',
      #                  'U850','V850','U200','V200'] : 
      #       aboutZero = True
      #    clev_tup = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=nlev, \
      #                                    returnLevels=False, aboutZero=aboutZero )
      #    if clev_tup==None: 
      #       tres.cnLevelSelectionMode = 'AutomaticLevels'   
      #    else:
      #       cmin,cmax,cint = clev_tup
      #       tres.cnLevels = np.linspace(cmin,cmax,num=nlev)
      #       tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # set alternate variable names
      #-------------------------------------------------------------------------
      var_str = var[v]
      # if var[v]=='PRECT':     var_str = 'Precipitation'
      # if var[v]=='TMQ':       var_str = 'CWV'
      if var[v]=='TGCLDLWP':  var_str = 'Liq. Water Path'
      # if var[v]=='TGCLDIWP':  var_str = 'IWP'

      lev_str = None
      if lev>0: lev_str = f'{lev}mb'
      if lev<0: lev_str = f'k={(lev*-1)}'
      # if lev_str is not None and var[v] in ['U','V','OMEGA','T','Q','Z3']:
      if lev_str is not None:
         var_str = f'{lev_str} {var[v]}'

      #-------------------------------------------------------------------------
      # Create plot
      #-------------------------------------------------------------------------
      # if use_common_label_bar: 
      #    tres.lbLabelBarOn = False
      # else:
      #    tres.lbLabelBarOn = True

      hs.set_cell_fill(tres,case_obj=case_obj,htype=htype,scrip_file_path=scrip_file_path)
         
      ip = v*num_case+c if var_x_case else c*num_var+v

      # plot[ip] = ngl.contour_map(wks,data_list[c],tres) 
      plot[ip] = ngl.contour_map(wks,np.ma.masked_invalid(data_list[c]),tres) 
      
      #----------------------------------------------------------------------
      # set plot subtitles
      #----------------------------------------------------------------------
      ctr_str = ''
      if 'name' not in vars(): case_name = case_obj.short_name
      if 'name'     in vars(): case_name = name[c]

      if glb_avg_list != []: ctr_str = f'{glb_avg_list[c]:6.4}'

      # hs.set_subtitles(wks, plot[ip], case_name, ctr_str, var_str, font_height=subtitle_font_height)

      
#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------

# title_str = None
# title_str = 'ANN'
# if months==[1,2,12]: title_str = 'DJF'
# if months==[6,7,8]: title_str = 'JJA'
# if title_str is not None:
#    textres =  ngl.Resources()
#    textres.txFontHeightF =  0.025
#    ngl.text_ndc(wks,title_str,0.5,.7,textres)

layout = [num_var,num_case] if var_x_case else [num_case,num_var]

if num_case==1 or num_var==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]
   
pnl_res = hs.setres_panel()

### use common label bar
# if use_common_label_bar: pnl_res.nglPanelLabelBar = True

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01
# if layout==[3,2] : pnl_res.nglPanelFigureStringsFontHeightF = 0.015

pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
