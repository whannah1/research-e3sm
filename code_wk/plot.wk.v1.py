import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string, glob
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs
import wavenumber_frequency_functions as wf
import cmocean
name,case,case_dir,case_sub,case_grid = [],[],[],[],[]
def add_case(case_in,n=None,p=None,s=None,g=None):
   global name,case,case_dir,case_sub
   if n is None:
      tmp_name = tmp_name.replace('.',' ')
      tmp_name = tmp_name.replace('_','=')
   else:
      tmp_name = n
   case.append(case_in); name.append(tmp_name)
   case_dir.append(p); case_sub.append(s); case_grid.append(g)
var,lev_list = [],[]
def add_var(var_name,lev=-1): 
   var.append(var_name); lev_list.append(lev)
#-------------------------------------------------------------------------------
### Rotating RCE for Da Yang
# pscratch,psub = '/pscratch/sd/w/whannah/e3sm_scratch/perlmutter', 'run'
# pscratch,psub = '/global/cfs/cdirs/m1517/dyang/E3SM_MMF',         'data_native'
# pscratch,psub = '/global/cfs/cdirs/m1517/dyang/E3SM_MMF',         'data_remap_90x180/data_2d'
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.RADNX_1.03', n='MMF ROT-RCE CTRL',    d=pscratch,s=psub)
# add_case('E3SM.GNUGPU.ne30pg2.F-MMFXX-RCEROT.BVT.GBL_QRT.03', n='MMF ROT-RCE FIX-QRAD',d=pscratch,s=psub)

### 4xCO2 tests on Summit
# pscratch,psub = '/gpfs/alpine/cli115/proj-shared/hannah6/e3sm_scratch/','data_remap_90x180'
# add_case('NOAA',n='NOAA')
# add_case('E3SM.2023-CO2-TEST-00.GNUGPU.ne30pg2_oECv3.WCYCL1850-MMF1.1xCO2',n='MMF 1xCO2',d=pscratch,s=psub)
# add_case('E3SM.2023-CO2-TEST-00.GNUGPU.ne30pg2_oECv3.WCYCL1850-MMF1.4xCO2',n='MMF 4xCO2',d=pscratch,s=psub)


### v3 L80 test
# add_case('20230629.v3alpha02.amip.chrysalis.L72',n='L72',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')
# add_case('20230629.v3alpha02.amip.chrysalis.L80',n='L80',p='/lcrc/group/e3sm/ac.whannah/E3SMv3_dev',s='archive/atm/hist')

### SciDAC MMF tests w/ new vertical grids
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','run'
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L60',n='MMF L60',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L64',n='MMF L64',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-SCIDAC-MMF.ne30pg2_EC30to60E2r2.F2010-MMF1.L72',n='MMF L72',p=tmp_path,s=tmp_sub)


### coupled historical
# tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/e3smv2_historical','data_remap_90x180'
# add_case('v2.LR.historical_0101',n='E3SMv2',p=tmp_path,s=tmp_sub)

# tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/2023-CPL/','data_remap_90x180'
# add_case('E3SM.INCITE2023-CPL.ne30pg2_EC30to60E2r2.WCYCL20TR-MMF1',n='E3SM-MMF',p=tmp_path,s=tmp_sub)

### rad column sensitivity
# tmp_path,tmp_sub = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu','data_remap_90x180'
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_1.00',  n='RNX_1',p=tmp_path,s=tmp_sub)
# add_case('E3SM.RAD-SENS.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_128x1.RNX_128.00',n='RNX_128',p=tmp_path,s=tmp_sub)

### MJO test (transferred from Summit)
# tmp_path,tmp_sub = '/global/cfs/cdirs/m3312/whannah/2023-MJO','data_remap_90x180'
# add_case('E3SM.2023-MJO-test.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.L60.NX_64.RX_4.DX_2000',  n='MMF L60 64x2km',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-MJO-test.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.L60.NX_32.RX_4.DX_4000',  n='MMF L60 32x4km',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-MJO-test.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.L72.NX_64.RX_4.DX_2000',  n='MMF L72 64x2km',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2023-MJO-test.GNUGPU.ne30pg2_EC30to60E2r2.F2010-MMF1.L72.NX_32.RX_4.DX_4000',  n='MMF L72 32x4km',p=tmp_path,s=tmp_sub)


### 2024 PAM test for E3SM tutorial
# tmp_path,tmp_sub = '/gpfs/alpine2/atm146/proj-shared/hannah6/e3sm_scratch/','data_remap_180x360'
# add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF1', n='MMF1',p=tmp_path,s=tmp_sub)
# add_case('E3SM.2024-PAM-CHK-04.ne30pg2_oECv3.F2010-MMF2', n='MMF2',p=tmp_path,s=tmp_sub)


### 2024 nontraditional coriolis
# scrip_file_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
# tmp_path_cpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
# tmp_path_gpu = '/pscratch/sd/w/whannah/e3sm_scratch/pm-gpu'
# # tmp_sub = 'run'
# tmp_sub = 'archive/atm/hist'
# # add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-off.NCT-off'               ,n='EAM ',       p=tmp_path_cpu,s=tmp_sub)
# # add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-off'                ,n='EAM NHS',    p=tmp_path_cpu,s=tmp_sub)
# # add_case('E3SM.2024-coriolis-00.GNUCPU.ne30pg2_oECv3.F2010.NHS-on.NCT-on'                 ,n='EAM NHS+NCT',p=tmp_path_cpu,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-off.NCT-off' ,n='MMF ',       p=tmp_path_gpu,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-off'  ,n='MMF NHS',    p=tmp_path_gpu,s=tmp_sub)
# add_case('E3SM.2024-coriolis-00.GNUGPU.ne30pg2_oECv3.F2010-MMF1.NXY_64_1.NHS-on.NCT-on'   ,n='MMF NHS+NCT',p=tmp_path_gpu,s=tmp_sub)


# add_case('v2.LR.amip_0101',n='E3SMv2',p='/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch',s='archive/atm/hist')
# add_case('v3.LR.amip_0101',n='E3SMv3',p='/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch',s='archive/atm/hist')

add_case('NOAA',n='NOAA 1974-2021')
# tmp_scratch = '/lustre/orion/cli115/proj-shared/brhillman/e3sm_scratch'
tmp_scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
add_case(f'decadal-production-run6-20240708.ne1024pg2_ne1024pg2.F20TR-SCREAMv1.pnetcdf',n='SCREAM decadal 1995-1999',p=tmp_scratch,s='remap_180x360')

#-------------------------------------------------------------------------------

# add_var('U',lev=850)
# add_var('PRECT')
# add_var('FLNT')
# add_var('FLUT')
# add_var('U850')
# add_var('U200')

# add_var('precip_liq_surf_mass_flux')
add_var('LW_flux_up_at_model_top')

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/figs_wk/wk.v1'


lat1,lat2 = -15,15

# htype,years,months,first_file,num_files = 'h1',[],[],0,int(365*30/30)


# htype,years,months,first_file,num_files = 'h1',[],[],0,int(365*10)
# htype,years,months,first_file,num_files = 'h1',[],[],0,int(365*10/5)
# htype,years,months,first_file,num_files = 'h1',[],[],365*40,365*20  # use for MMF historical starting in 1950
# htype,years,months,first_file,num_files = 'h1',[],[],0,122  # use for v2 historical starting in 2000-2009 (30 daily means per file)
# htype,years,months,first_file,num_files = 'h1',[],[],0,int(365*30/30)
# htype,years,months,first_file,num_files = 'h1',[],[],0,365*5


# use_remap,remap_grid = True,'90x180'
# use_remap,remap_grid = True,'180x360'

recalculate_spectra = False

use_daily   = True
var_x_case  = True


# normalize,add_diff = False,True
normalize,add_diff = False,False


num_plot_col = len(case)

use_common_label_bar = False

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_case,num_var = len(case),len(var)

wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix
wks = ngl.open_wks(fig_type,fig_file,wkres)

# if add_diff:
#    plot = [None]*(num_var*(num_case*2-1))
# else:
#    plot = [None]*(num_var*num_case)
plot = [None]*(num_var*num_case)

res = hs.res_contour_fill()
res.vpHeightF = 0.5

if use_common_label_bar:  res.lbLabelBarOn = False

res.lbLabelFontHeightF = 0.02

# res.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )

lres = hs.res_xy()
lres.xyLineThicknessF = 1
lres.xyLineColor      = 'black'
lres.xyDashPattern    = 0



#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def get_comp(case):
   comp = 'eam'
   return comp
#---------------------------------------------------------------------------------------------------
# get data for dispersion curves
#---------------------------------------------------------------------------------------------------
swfq, swwn = wf.genDispersionCurves() # swfq.shape # -->(6, 3, 50)
swf = np.ma.masked_invalid( np.where(swfq==1e20, np.nan, swfq) )
swk = np.ma.masked_invalid( np.where(swwn==1e20, np.nan, swwn) )
#---------------------------------------------------------------------------------------------------
# routine for adding theoretical dispersion curves and MJO frequency bounds
#---------------------------------------------------------------------------------------------------
def add_dispersion_curves(plot):
   # add dispersion curves
   for ii in range(3,6):
      lres.xyDashPattern = 0
      ngl.overlay( plot, ngl.xy(wks, np.array([0,0]), np.array([-1e3,1e3]), lres) )
      ngl.overlay( plot, ngl.xy(wks, swk[ii, 0,:], swf[ii,0,:], lres) )
      ngl.overlay( plot, ngl.xy(wks, swk[ii, 1,:], swf[ii,1,:], lres) )
      ngl.overlay( plot, ngl.xy(wks, swk[ii, 2,:], swf[ii,2,:], lres) )
   # overlay lines for MJO frequency range
   lres.xyDashPattern = 1
   tfrq=1./30.; ngl.overlay( plot, ngl.xy(wks, np.array([-1e3,1e3]), np.array([tfrq,tfrq]), lres) )
   tfrq=1./90.; ngl.overlay( plot, ngl.xy(wks, np.array([-1e3,1e3]), np.array([tfrq,tfrq]), lres) )
   return
#---------------------------------------------------------------------------------------------------
# put white rectangle over satellite aliasing signal
#---------------------------------------------------------------------------------------------------
pgres = ngl.Resources()
pgres.nglDraw,pgres.nglFrame = False,False
pgres.gsLineThicknessF = 1
pgres.gsFillColor = 'white'
pgres.gsLineColor = 'white'

awn1,awn2 = 13,16
afq1,afq2 = 0.095,0.125

def hide_aliasing_artifact(plot):
   bx = [awn1,awn1,awn2,awn2,awn1]
   by = [afq1,afq2,afq2,afq1,afq1]
   pdum = ngl.add_polygon(wks,plot,bx,by,pgres)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print(f'\n  var: '+hc.tcolor.GREEN+f'{var[v]}'+hc.tcolor.ENDC)
   spec_list, freq_list, wvnm_list = [],[],[]
   for c in range(num_case):
      print(f'    case: '+hc.tcolor.CYAN+f'{case[c]}'+hc.tcolor.ENDC)

      # data_dir_tmp,data_sub_tmp = None, None
      # if case_dir[c] is not None: data_dir_tmp = case_dir[c]
      # if case_sub[c] is not None: data_sub_tmp = case_sub[c]
      # if use_remap: data_sub_tmp = f'data_remap_{remap_grid}/'

      

      if case[c]=='NOAA':
         # obs_year_start = 1974+20
         # obs_year_end   = 1974+20+num_files/12+1
         # wk_tmp_file = os.getenv('HOME')+f'/Data/Obs/OLR/SpaceTime.NOAA.olr.daily.wk.v1.{obs_year_end}-{obs_year_end}.{lat1}:{lat2}.nc'
         wk_tmp_file = os.getenv('HOME')+f'/Data/Obs/OLR/SpaceTime.NOAA.olr.daily.wk.v1.{lat1}:{lat2}.nc'
      else:
         # case_obj = he.Case( name=case[c], atm_comp=get_comp(case[c]),
         #                     data_dir=data_dir_tmp, data_sub=data_sub_tmp,
         #                     populate_files=False )
         # case_obj.set_coord_names(var[v])
         # wk_tmp_file = case_obj.data_dir+'/'+case_obj.name+f'/wk.v1.{var[v]}.{lat1}:{lat2}.nc'
         wk_tmp_file = f'{case_dir[c]}/{case[c]}/wk.v1.{var[v]}.{lat1}:{lat2}.nc'


      if use_daily: wk_tmp_file = wk_tmp_file.replace('.nc','.daily.nc')
      
      if recalculate_spectra :
         #-------------------------------------------------------------------------
         # read the data
         #-------------------------------------------------------------------------
         print(f'      loading data...')

         if case[c]=='NOAA':

            obs_file = os.getenv('HOME')+'/Data/Obs/OLR/olr.daily.1974-2021.nc'
            ds = xr.open_dataset(obs_file)
            data = ds['olr']
            
            # num_year = int(num_files/365)
            # yr1 = 2005 ; yr2 = yr1+num_year-1
            num_year = 20
            yr1 = 2000 ; yr2 = yr1+num_year-1
            date_beg = f'{yr1}-01-01'
            date_end = f'{yr2}-12-31'
            data = data.sel(time=slice(date_beg,date_end))
         else:
            if 'SCREAM' in case[c]:
               htype = 'output.scream.decadal.6hourlyAVG_ne30pg2.AVERAGE.nhours_x6'

               file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{htype}*'
               file_list = sorted(glob.glob(file_path))

               print()
               print(' '*6+f'num files: {len(file_list)}')
               print()
               # print(); print(file_path)
               # print(); print(file_list)
               # print('WARNING - reducing to 360 day subset')
               # file_list = file_list[:360]

               # ds = xr.open_mfdataset( file_list, use_cftime=True, decode_times=False,decode_cf=False )
               ds = xr.open_mfdataset( file_list )

               data = ds[var[v]]

            # data = case_obj.load_data(var[v],htype='h1',years=years,months=months
            #                          ,first_file=first_file,num_files=num_files
            #                          ,use_remap=use_remap,remap_str=f'remap_{remap_grid}')
            # if var[v]=='PRECT': data = data*86400.*1e3
         # print(); print(data); exit()

         ### reduce to equatorial subset
         mask = xr.DataArray( np.ones([len(data['lat']),len(data['lon'])],dtype=bool), dims=('lat','lon') )
         mask = mask & (data['lat']>=lat1) & (data['lat']<=lat2)
         data = data.where( mask, drop=True)


         ### Convert to daily mean
         if use_daily: data = data.resample(time='D').mean(dim='time')

         ### calculate samples per day for FFT
         dtime = ( data['time'][1]-data['time'][0] ).values / np.timedelta64(1, 's')
         spd = int( 86400./dtime )

         hc.print_stat(data,name=var[v],compact=True,indent=' '*6)

         ### check for invalid values
         # num_nan = np.sum(np.isnan(data.values))
         # num_inf = np.sum(np.isinf(data.values))
         # if num_nan>0 or num_inf>0:
         #    print(f'  num_nan: {num_nan}')
         #    print(f'  num_inf: {num_inf}')
         #    exit()

         data.load()

         num_days = len(data['time'].values)

         # check for NaN/Inf values
         # print(data.isnull().count())
         # print(data.count())
         # exit()

         #-------------------------------------------------------------------------
         # Calculate the WK spectra
         #-------------------------------------------------------------------------
         print(f'      calculating spectra...')

         if use_daily: 
            # segsize,noverlap = 180*spd,60*spd
            segsize,noverlap = 96*spd,32*spd
         else:
            if spd==8: segsize,noverlap = 60*spd,30*spd

         spec = wf.spacetime_power( data, 
                                    segsize=segsize, 
                                    noverlap=noverlap, 
                                    spd=spd, 
                                    dosymmetries=True )

         # spec.loc[{'frequency':0}] = np.nan # get rid of spurious power at \nu = 0

         ### calculate background from average of symmetric & antisymmetric components
         spec_all = spec.mean(dim='component')
         bkgd_all = wf.smooth_wavefreq(spec_all,    kern=wf.simple_smooth_kernel(), nsmooth=50, freq_name='frequency')
         bkgd_sym = wf.smooth_wavefreq(spec[0,:,:], kern=wf.simple_smooth_kernel(), nsmooth=50, freq_name='frequency')
         bkgd_asm = wf.smooth_wavefreq(spec[1,:,:], kern=wf.simple_smooth_kernel(), nsmooth=50, freq_name='frequency')
         

         ### separate components and throw away negative frequencies
         nfreq = len(spec['frequency'])
         rspec_sym  = spec[0,:,int(nfreq/2):]
         rspec_asy  = spec[1,:,int(nfreq/2):]
         bkgd_all = bkgd_all[:,int(nfreq/2):] 
         bkgd_sym = bkgd_sym[:,int(nfreq/2):] 
         bkgd_asm = bkgd_asm[:,int(nfreq/2):] 

         ### collect everything into a dataset
         wk_ds = xr.Dataset()
         wk_ds['num_days']  = num_days
         wk_ds['segsize']   = segsize
         wk_ds['noverlap']  = noverlap
         wk_ds['rspec_sym'] = rspec_sym
         wk_ds['rspec_asy'] = rspec_asy
         wk_ds['bkgd_all']  = bkgd_all
         wk_ds['bkgd_sym']  = bkgd_sym
         wk_ds['bkgd_asm']  = bkgd_asm
         #-------------------------------------------------------------------------
         # Write to file 
         #-------------------------------------------------------------------------
         print(f'      writing to file - {wk_tmp_file}')
         wk_ds.to_netcdf(path=wk_tmp_file,mode='w')
      else:
         print(f'      loading pre-calculated spectra... {wk_tmp_file}')
         wk_ds = xr.open_mfdataset( wk_tmp_file )
         num_days   = wk_ds['num_days'].values
         rspec_sym  = wk_ds['rspec_sym']
         rspec_asy  = wk_ds['rspec_asy']
         # nspec_sym  = wk_ds['nspec_sym'] 
         # nspec_asy  = wk_ds['nspec_asy'] 
         bkgd_all   = wk_ds['bkgd_all']
         bkgd_sym   = wk_ds['bkgd_sym']
         bkgd_asm   = wk_ds['bkgd_asm']

      print(f'      num_days: {num_days}')
      #----------------------------------------------------------------------------
      #----------------------------------------------------------------------------

      ### normalize by the background
      if normalize:
         nspec_sym = rspec_sym / bkgd_sym
         nspec_asy = rspec_asy / bkgd_asm

         spec_list.append(nspec_sym.transpose().values)
         # spec_list.append(nspec_asy.transpose().values)
         # spec_list.append(bkgd_sym.transpose().values)
      else:
         spec_list.append(rspec_sym.transpose().values)
         # spec_list.append(rspec_asy.transpose().values)

      freq_list.append(wk_ds['frequency'].values)
      wvnm_list.append(wk_ds['wavenumber'].values)

      #-------------------------------------------------------------------------
      # zero out values over region with satellite aliasing - helps to set difference colormap
      #-------------------------------------------------------------------------
      if case[c]=='NOAA':
         for i in range(len(wvnm_list[c])):
            twn = wvnm_list[c][i]
            if twn>=awn1 and  twn<=awn2:
               for j in range(len(freq_list[c])):
                  tfq = freq_list[c][j]
                  if tfq>=afq1 and  tfq<=afq2:
                     spec_list[c][j,i] = 0
   #------------------------------------------------------------------------------------------------
   # calculate limits for common color bar
   #------------------------------------------------------------------------------------------------

   data_min = np.min([np.nanmin(d) for d in spec_list])
   data_max = np.max([np.nanmax(d) for d in spec_list])

   print()
   print(f'      data_min: {data_min:.20f}')
   print(f'      data_max: {data_max:.20f}')
   print()
   
   if add_diff:
      # spec_diff_list = np.zeros(len(spec_list))
      # for c in range(num_case):
      #    spec_diff_list[c] = spec_list[c] - spec_list[0]
      diff_min = np.min([np.nanmin(d-spec_list[0]) for d in spec_list])
      diff_max = np.max([np.nanmax(d-spec_list[0]) for d in spec_list])

   #------------------------------------------------------------------------------------------------
   # Create plot
   #------------------------------------------------------------------------------------------------
   for c in range(num_case):
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      tres = copy.deepcopy(res)

      if normalize: 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
         # tres.cnLevels = np.arange(4,44+4,4)/1e1
         # tres.cnLevels = np.arange(2,30+2,2)/1e1
         # tres.cnLevels = np.arange(4,30,2)/1e1
         # tres.cnLevels = np.arange(10,30,2)/1e1
         # tres.cnLevels = np.arange(1.0,3.0,0.2)
         tres.cnLevels = np.arange(0.6,3.6,0.2)
         # if any(var[v] in ['U850','U200']): 
         #    tres.cnLevels = np.arange(4,44+4,4)/1e1
      else:
         tres.cnLevelSelectionMode = 'ExplicitLevels'
         # num_clev,aboutZero = 21,False
         # (cmin,cmax,cint) = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=num_clev, returnLevels=False, aboutZero=aboutZero )
         # (cmin,cmax,cint) = ngl.nice_cntr_levels( np.min(spec_list[c]), np.max(spec_list[c])*0.1, cint=None, max_steps=num_clev, returnLevels=False, aboutZero=aboutZero )
         # tres.cnLevels = np.linspace(cmin,cmax,num=num_clev)

         if var[v]=='LW_flux_up_at_model_top' : tres.cnLevels = np.arange(2,34,2)

         # ### log scales for RCEROT
         # if var[v]=='FLUT' : tres.cnLevels = np.logspace(  0.0, 2.0, num=30).round(decimals=4)
         # if var[v]=='FLNT' : tres.cnLevels = np.logspace(  0.0, 2.0, num=30).round(decimals=4)
         # # if var[v]=='PRECT': tres.cnLevels = np.logspace(  0.0, 2.0, num=30).round(decimals=4)
         # if var[v]=='PRECT': tres.cnLevels = np.logspace( -1.5, 0.0, num=30).round(decimals=4)
         # if var[v]=='U850' : tres.cnLevels = np.logspace( -1.5, 1.0, num=30).round(decimals=4)
         # if var[v]=='U200' : tres.cnLevels = np.logspace( -1.0, 2.0, num=30).round(decimals=4)
      
      tres.sfXArray = wvnm_list[c]
      tres.sfYArray = freq_list[c]

      tm_period_values  = np.array([1,2,5,10,30,90])
      tres.tmYLMode     = 'Explicit'
      tres.tmYLValues   = 1./tm_period_values
      tres.tmYLLabels   = tm_period_values

      # tres.trYMaxF,tres.trXMinF,tres.trXMaxF = 0.3,-15,15
      # tres.trYMaxF,tres.trXMinF,tres.trXMaxF = 0.3,-20,20
      tres.trYMaxF,tres.trXMinF,tres.trXMaxF = 0.2,-10,10
      # tres.trYMaxF,tres.trXMinF,tres.trXMaxF = 0.1,-8,8
      # tres.trYMaxF,tres.trXMinF,tres.trXMaxF = 0.04,0,5

      tres.cnFillPalette = 'WhiteBlueGreenYellowRed'
      # tres.cnFillPalette = np.array( cmocean.cm.matter(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.amp(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.phase(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.rain(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.delta(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )
      # tres.cnFillPalette = np.array( cmocean.cm.thermal(np.linspace(0,1,256)) )

      # plot.append( ngl.contour(wks, rspec_sym.transpose().values, tres) )
      # plot.append( ngl.contour(wks, rspec_asy.transpose().values, tres) )
      # plot.append( ngl.contour(wks, nspec_sym.transpose().values, tres) )
      # plot.append( ngl.contour(wks, nspec_asy.transpose().values, tres) )
      # plot.append( ngl.contour(wks, background.transpose().values, tres) )

      # if add_diff:
      #    ip = v*(num_case*2-1)+c if var_x_case else c*num_var+v
      # else:
      #    ip = v*num_case+c if var_x_case else c*num_var+v
      ip = v*num_case+c if var_x_case else c*num_var+v

      plot[ip] = ngl.contour(wks, spec_list[c], tres)

      add_dispersion_curves(plot[ip])
      if case[c]=='NOAA': hide_aliasing_artifact(plot[ip])

      var_str = var[v]
      if var[v]=='PRECT' : var_str = 'Precipitation'
      if var[v]=='FLNT'  : var_str = 'OLR'
      if var[v]=='FLUT'  : var_str = 'OLR'
      hs.set_subtitles(wks, plot[ip], name[c], '', var_str, font_height=0.01)

      #-------------------------------------------------------------------------
      # Add difference plot
      #-------------------------------------------------------------------------
      if add_diff:
         if c==0:
            nspec_base = spec_list[c]
         else:
            nspec_diff = spec_list[c] - nspec_base


            num_clev,aboutZero = 11,True
            # std = np.std(nspec_diff)
            # (cmin,cmax,cint) = ngl.nice_cntr_levels(np.min(nspec_diff), np.max(nspec_diff), cint=None, max_steps=num_clev, returnLevels=False, aboutZero=aboutZero )
            # (cmin,cmax,cint) = ngl.nice_cntr_levels(-4*std, 4*std, cint=None, max_steps=num_clev, returnLevels=False, aboutZero=aboutZero )
            (cmin,cmax,cint) = ngl.nice_cntr_levels(diff_min, diff_max, cint=None, max_steps=num_clev, returnLevels=False, aboutZero=aboutZero )
            tres.cnLevels = np.linspace(cmin,cmax,num=num_clev)

            tres.cnFillPalette = np.array( cmocean.cm.balance(np.linspace(0,1,256)) )

            # ip = v*(num_case*2-1)+num_case+c-1 if var_x_case else (num_case+c-1)*num_var+v
            ip = v*num_case+c if var_x_case else c*num_var+v

            plot[ip] = ngl.contour(wks, nspec_diff, tres)

            add_dispersion_curves(plot[ip])
            if case[0]=='NOAA': hide_aliasing_artifact(plot[ip])

            hs.set_subtitles(wks, plot[ip], name[c], '', var_str+' diff', font_height=0.01)
      #-------------------------------------------------------------------------
      #-------------------------------------------------------------------------
      del tres

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [1,len(plot)]
# if add_diff:
#    layout = [num_var,num_case*2-1] if var_x_case else [num_case*2-1,num_var]
# else:
#    layout = [num_var,num_case] if var_x_case else [num_case,num_var]
layout = [num_var,num_case] if var_x_case else [num_case,num_var]

if num_case==1 or num_var==1:
   layout = [int(np.ceil(len(plot)/float(num_plot_col))),num_plot_col]

pnl_res = hs.setres_panel()
if use_common_label_bar: pnl_res.nglPanelLabelBar = True

ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

# fig_file = fig_file.replace(os.getenv('HOME')+'/Research/E3SM/','../../')+'.png'
# print();print(fig_file);print()

# print()
hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
