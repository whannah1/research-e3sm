import numpy as np 
import xarray as xr 
# import pylab as plt 
import numba
import os 
import sys 
from sklearn.neighbors import NearestNeighbors

# import parameters 
EARTHR = 6.371e6
PI = np.pi

@numba.njit 
def great_circle(pt1, pt2):
    
    phi1 = PI/180.0 * pt1[0]
    lambda1 = PI/180.0 * pt1[1]

    phi2 =  PI/180.0 *pt2[0]
    lambda2 = PI/180.0 * pt2[1]

    if phi1 == phi2 and lambda1 == lambda2: 
        return 0.0 
    
    deltalambda = np.abs(lambda2 - lambda1)
    deltasigma = np.arccos(np.sin(phi1) * np.sin(phi2) + np.cos(phi1) * np.cos(phi2) * np.cos(deltalambda))

    return  EARTHR * deltasigma

@numba.njit 
def great_circle_bearing(pt1, pt2):
    deg_to_rad = PI/180.
    lat1 = pt1[0] * deg_to_rad
    lat2 = pt2[0] * deg_to_rad
    lon1 = pt1[1] * deg_to_rad
    lon2 = pt2[1] * deg_to_rad

    dlon = lon1 - lon2

    atan_tmp1 = np.sin(lon2-lon1)*np.cos(lat2)
    atan_tmp2 = np.cos(lat1)*np.sin(lat2)-np.sin(lat1)*np.cos(lat2)*np.cos(lon2-lon1)
    bearing = np.arctan2( atan_tmp1, atan_tmp2 )

    return bearing * 180./PI

@numba.njit
def _neighbors_distances(i, lat, lon, pts, distances): 
    count = 0 
    n = len(pts)
    for p in pts: 
        mypt = (lat[i], lon[i])
        neigh = (lat[p], lon[p])
        distances[count] = great_circle(mypt, neigh)
        count += 1 

    return 

@numba.njit
def _neighbors_bearings(i, lat, lon, pts, bearings): 
    count = 0 
    n = len(pts)
    for p in pts: 
        mypt = (lat[i], lon[i])
        neigh = (lat[p], lon[p])
        bearings[count] = great_circle_bearing(mypt, neigh)
        count += 1 

    return 

@numba.njit
def _max_distance_lat(i, lat, lon, pts): 
    n = len(pts)
    max_lat = -1e6
    max_lon = -1e-6
    for p in pts: 
        mypt = (lat[i], lon[i])
        neigh = (lat[p], lon[p])
        
        max_lat = np.max([max_lat, np.abs(mypt[0]-neigh[0]) ])
        max_lon = np.max([max_lon, np.abs(mypt[1]-neigh[1]) ])


    return max_lat, max_lon 

@numba.njit
def _merge_neighbors(d0, d1, d1shift, neighbors_0, neighbors_1, ndiff): 
    
    tmp = np.empty((neighbors_0.shape[1] + neighbors_1.shape[1]), dtype=numba.types.int64)
    dist_tmp = np.empty((tmp.shape[0]), dtype=np.double)

    count = 0 
    #Loop over each model column 
    for i in range(neighbors_0.shape[0]):
        identical = True 
        ndiff_ = 0 

        #Now determine if each the two neighborhoods are identical 
        for n in range(neighbors_0.shape[1]): 
            if neighbors_0[i,n] != neighbors_1[i,n]: 
                identical = False
                ndiff_ += 1 

        #If they are not identical we have to combine them and remove overlapping columns 
        if not identical:
            ntmp = 0          #This is a counter 
            tmp[:] = -1       #This needs to be an invalid index. Making it negative is safe 
            dist_tmp[:] = 1e9 #This needs to be set to a large number > 360

            #First we copy all of one neighborhood into a temporary array and keep track of how many 
            #points are valid in the temporary array by incrementing ntmp
            for n0 in range(neighbors_0.shape[1]):
                tmp[n0] = neighbors_0[i,n0]
                ntmp += 1 

            #Now search the second neighborhood and add no duplicate points to tmp and continue to increment
            #ntmp
            for n1 in range(neighbors_1.shape[1]):
                in_neighbors0 = False 
                for n0 in range(neighbors_0.shape[1]):
                    if neighbors_1[i,n1] == neighbors_0[i,n0]:
                        in_neighbors0 = True 
                if not in_neighbors0: 
                    tmp[ntmp] = neighbors_1[i,n1]
                    ntmp += 1 

            #Now loop over the datasets and compute the distance between non overlapping columns and column i 
            for n_tmp in range(tmp.shape[0]):
                if tmp[n_tmp] != -1: 
                    delta_d1 = np.fmin(np.abs(d1[i] - d1[tmp[n_tmp]]), np.abs(d1shift[i] - d1shift[tmp[n_tmp]]))
                    dist_tmp[n_tmp] = np.sqrt((d0[i] - d0[tmp[n_tmp]])**2.0 + (delta_d1)**2.0 )
            
            #Arg sort to get the indicies of the closest columns to coulmn i 
            dist_sort = np.argsort(dist_tmp)
            for nl in range(neighbors_0.shape[1]): 
                neighbors_0[i,nl] = tmp[dist_sort[nl]]  #The mearged neighborhoods will be written to neighbors_0
            
            
    return 

class Neighbors: 
    def __init__(self, d0, d1, n, shift_merge = True):
        
        self._d0 = np.copy(d0) 
        self._d1 = np.copy(d1) 
        self._n = n
        self._pts = None 
        self._neighbors = None 
        # self._distances = None 
        self._distances = np.empty([len(d0),n])
        self._bearings = np.empty([len(d0),n])
        self._max_distance = None
        self._shift_merge = shift_merge 



        self._combine_ds()
        self._compute_neighbors()
        self._compute_distance_range()

        return 

    def _combine_ds(self): 

        self._pts = np.zeros((self._d0.shape[0],2), dtype=np.double)
        for i in range(self._d0.shape[0]): 
            self._pts[i, 0] = self._d0[i]
            self._pts[i, 1] = self._d1[i]

        return 

    def _compute_neighbors(self): 
        # Here use SKL nearest neighbors class, for now set the algorithm option to auto which 
        # must use some heuristic model to determine the best algorithm to compute the nearest 
        # neighbors. Setting algorithm='brute' uses the brute force method which could be usefule 
        # in future testin to insure that the more efficnet NN algorithms are exact. Although 
        # they sshould be. 
        
        #Fit the skl NN algorithm to the data 
        skl_nbrs = NearestNeighbors(n_neighbors=self._n, algorithm='auto').fit(self._pts)

        #Now compute the nearest neighbors (Note that the closest neighbor to a given point is itself)
        distance_matrix_0,  neighbors_0 = skl_nbrs.kneighbors(self._pts)
        
        if self._shift_merge == True: 
            shift_pts = np.copy(self._pts)

            
            shift_pts[:,1] = (shift_pts[:,1] - 180.0)%360.0

            skl_nbrs1 = NearestNeighbors(n_neighbors=self._n, algorithm='auto').fit(shift_pts)
            distance_matrix_1, neighbors_1 = skl_nbrs1.kneighbors(shift_pts)
            

            ndiff = np.empty(shift_pts.shape[0], dtype=np.double)

            _merge_neighbors(self._d0, self._d1,  shift_pts[:,1], neighbors_0, neighbors_1, ndiff)
            
            self._neighbors = neighbors_0
        else: 
            self._neighbors = neighbors_0

                   
        return 


    def _compute_distance_range(self):
        
        distances = np.empty(self._n, dtype=np.double)
        bearings  = np.empty(self._n, dtype=np.double)
        self._max_distance = np.empty(self._d0.shape[0], dtype=np.double)
        for i in range(self._d0.shape[0]): 
            _neighbors_distances(i, self._d0, self._d1, self._neighbors[i,:], distances)
            self._distances[i,:] = distances

            max_distance = np.amax(distances)
            self._max_distance[i] = max_distance/1000.0

            _neighbors_bearings(i, self._d0, self._d1, self._neighbors[i,:], bearings)
            self._bearings[i,:] = bearings

        return 

    @property
    def d(self): 
        return self._d0, self._d1

    @property
    def n(self): 
        return self._n 

    @property
    def pts(self): 
        return self._pts 

    @property
    def neighbors(self):
        return self._neighbors


class FilterBase: 
    def __init__(self, NN, area, opts_dict = {}): 
        self._NN = NN
        self._area = np.copy(area)
        self._opts_dict = {} 
        return

    def filter(self, field_array): 
        
        return

    @property
    def area(self): 
        return self._area  


@numba.njit
def _boxcar_filter_field( lat, lon, all_pts, area, field, field_filt): 
    #all_pts_f = np.asfortranarray(all_pts)
    for i in range(all_pts.shape[0]): 
        #print(i, all_pts[i,:])
        #neighbors_distances(i, lat, lon, all_pts[i,:], distances)

       # print(np.max(distances))

        count = 0 
        field_filt[i] = 0 
        area_sum = 0 
        for pt in all_pts[i,:]:

            field_filt[i] += field[pt] * area[pt] 
            area_sum += area[pt]
            count += 1 
            #print(count)
        field_filt[i] /= area_sum


class FilterBoxcar(FilterBase): 

    def __init__(self, NN, area, opts_dict):
        FilterBase.__init__(self, NN, area, opts_dict)
 
        return 

    def filter(self, field): 
        field_filt_out = np.empty(field.shape[0], dtype=np.double)
        # field_filt_out = np.empty(field.shape[0], dtype=field.dtype)

        d0, d1 = self._NN.d
        _boxcar_filter_field(d0, d1, self._NN.neighbors, self._area, field, field_filt_out)
    
        return field_filt_out

@numba.njit
def _gauss_kernel(sigma, dist): 
    return 1.0/(np.sqrt(2.0 * PI)*sigma) * np.exp(-dist * dist / (2 * sigma * sigma))

@numba.njit
def _gauss_filter_field(lat, lon, all_pts, area, distances, sigma, field, field_filt): 
    for i in range(all_pts.shape[0]): 

        _neighbors_distances(i, lat, lon, all_pts[i,:], distances)
        
        count = 0 
        field_filt[i] = 0 
        area_sum = 0 
        for pt in all_pts[i,:]:
            kernel_weight = _gauss_kernel(sigma, distances[count]/1000.0) 
            field_filt[i] += field[pt] * area[pt] * kernel_weight
            area_sum += area[pt] * kernel_weight
            count += 1 

        field_filt[i] /= area_sum
    return 
            

class FilterGauss(FilterBase): 

    def __init__(self, NN, area, opts_dict):
        FilterBase.__init__(self, NN, area, opts_dict)
        self._sigma = opts_dict['sigma']
        return 

    def filter(self, field): 
        field_filt_out = np.empty(field.shape[0], dtype=np.double)
        distances = np.empty(self._NN.n)
        d0, d1 = self._NN.d
        _gauss_filter_field(d0, d1, self._NN.neighbors, self._area, distances, self._sigma, field, field_filt_out)
    
        return field_filt_out




def filter_factory(NN, area, filter_type, opts_dict):
    if filter_type == 'boxcar':
        return FilterBoxcar(NN, area, opts_dict)
    elif filter_type == 'gauss': 
        return FilterGauss(NN, area, opts_dict)
    else:
        print('No filter type: ', filter_type)
        sys.exit() 


@numba.njit
def _node_type_mean(node_sizes, area ,data, means): 
    areasum0 = 0.0
    areasum1 = 0.0
    areasum2 = 0.0  

    total_area = 0.0 
    for i in range(len(data)): 

        if area[i] < node_sizes[0]: 
            means[0] += data[i]*area[i]
            areasum0 += area[i]

        if area[i] > node_sizes[0] and area[i] < node_sizes[1]:
            means[1] += data[i]*area[i]
            areasum1 += area[i]

        if area[i] > node_sizes[1]: 
            means[2] += data[i]*area[i]
            areasum2 += area[i]

        total_area += area[i]


    means[0] = means[0]/areasum0
    means[1] = means[1]/areasum1
    means[2] = means[2]/areasum2


class NodeTypeMean: 
    def __init__(self, node_sizes, area): 
        self._node_sizes = node_sizes 
        self._area = np.copy(area)
        return 

    def compute(self, data): 
        means = np.zeros(len(self._node_sizes) + 1, dtype =np.double)
        _node_type_mean(self._node_sizes, self._area, data, means)
        
        return means 

    @property
    def node_sizes(self): 
        return self._node_sizes


class FilteredField: 
    
    def __init__(self, Filter, NTM, data):
        
        #Do not copy NN and Filter here 
        self._Filter = Filter
        self._NTM = NTM
        self._filtered_data  = None 
        self._small = None
        self._medium = None
        self._large = None 

        self._compute_filtered_field(data)


        return

    def _compute_filtered_field(self, data):

        if len(data.shape) == 1: 
            self._filtered_data = self._Filter.filter(data)
            self._small, self._medium, self._large = self._NTM.compute(self._filtered_data)
        elif len(data.shape) == 2:  
            self._filtered_data = np.empty(data.shape, dtype=np.double)
            self._small = np.empty(data.shape[0], dtype=np.double)
            self._medium = np.empty(data.shape[0], dtype=np.double)
            self._large = np.empty(data.shape[0], dtype=np.double)
            for i in range(data.shape[0]):
                self._filtered_data[i, :] = self._Filter.filter(data[i,:])     
                self._small[i], self._medium[i], self._large[i] = self._NTM.compute(self._filtered_data[i,:])
                
        return


    @property
    def large(self): 
        return self._large

    @property 
    def medium(self): 
        return self._medium 
    
    @property 
    def small(self): 
        return self._small 

    @property 
    def values(self): 
        return self._filtered_data

    @property
    def shape(self): 
        return np.shape(self._filtered_data)



# def main(path , variable): 

#     h1_xr = xr.open_mfdataset(os.path.join(path,'*nc'), chunks={'time':1, 'lev':1})



#     lat = h1_xr.lat.values[0,:]
#     lon = h1_xr.lon.values[0,:]
#     area = h1_xr.area.values[0,:]
   
#     n_neighbors = 64*4
#     NN = Neighbors( lat, lon, n_neighbors)
#     NNNoShift = Neighbors(lat, lon, n_neighbors, shift_merge=False)
   
   
#     filter_opts_dict = {} 
#     filter_type ='gauss'
#     filter_opts_dict['sigma'] = 100.0
#     Filter = filter_factory(NN, area, filter_opts_dict, filter_type)
#     FilterNoShift = filter_factory(NNNoShift, area, filter_opts_dict, filter_type)


#     #field = h1_xr.OMEGA.values[-1, -15, :]
#     field = h1_xr.PRECT.values[-1, :]
#     field_filt = Filter.filter(field)


#     field_filt_no_shift = FilterNoShift.filter(field)


#     node_sizes = np.array([0.0001, 0.00025]) 
#     NTM = NodeTypeMean(node_sizes, area)
#     print('Node type conditional mean pre-filter: ', NTM.compute(field))
#     print('Node type conditional mean post-filter: ', NTM.compute(field_filt))
#     print('Node type conditional mean post-filter: ', NTM.compute(field_filt_no_shift))

#     field_max = np.amax(field)
#     field_min = np.amin(field)

#     prime = field - field_filt
#     prime_max = np.amax(prime)
#     prime_min = np.amin(prime)
#     prime_range = np.max([np.abs(prime_max), np.abs(prime_min)])
    


#     plt.figure(1) 
#     plt.subplot(311)
#     plt.tricontourf(np.array(lon), np.array(lat), field_filt_no_shift, 100, cmap=plt.cm.nipy_spectral)
#     plt.colorbar(format='%.0e')
#     plt.subplot(312)
#     plt.tricontourf(np.array(lon), np.array(lat), field_filt, 100, cmap=plt.cm.nipy_spectral)
#     plt.colorbar(format='%.0e')
#     plt.subplot(313)
#     plt.tricontourf(np.array(lon), np.array(lat), field_filt - field_filt_no_shift, 100, cmap=plt.cm.nipy_spectral)
#     plt.colorbar(format='%.0e') 
#     plt.show() 




#     plt.figure(2) 
#     plt.subplot(311)
#     plt.tricontourf(np.array(lon), np.array(lat), np.array(field),100, cmap=plt.cm.nipy_spectral)
#     plt.colorbar(format='%.0e', label=r'$\phi$')
#     plt.subplot(312)
#     plt.tricontourf(np.array(lon), np.array(lat), np.array(field_filt),100,cmap=plt.cm.nipy_spectral, vmin=field_min, vmax=field_max)
#     plt.clim(vmin=field_min, vmax=field_max)
#     plt.colorbar(format='%.0e', label=r'$\hat{\phi}$')
#     plt.subplot(313)
#     plt.tricontourf(np.array(lon), np.array(lat), np.array(field - field_filt),100, cmap=plt.cm.seismic,vmin=-prime_range, vmax=prime_range)
#     plt.colorbar(format='%.0e', label=r'$\phi^\prime$')
#     plt.clim(vmin=-prime_range, vmax=prime_range)
#     plt.show() 
#     plt.savefig('filtering.png', dpi=600)
#     plt.close()
    

#     return 



# if __name__ == '__main__': 
#     path = '/Users/pres026/RCEMIP_DATA/'
#     variable = 'PRECT'
#     main(path, variable)
