import os, ngl, subprocess as sp, numpy as np, xarray as xr, copy, string
import hapy_common as hc, hapy_E3SM   as he, hapy_setres as hs


case = 'E3SM.HR-OUTPUT.ne30pg2_ne30pg2.F-MMF1.CRMNX_256.CRMDX_500.RADNX_8.BVT.01'

# ds = xr.open_mfdataset(os.getenv('HOME')+f'/E3SM/scratch/{case}/run/{case}.eam.h1.0001-01-*-00000.nc')
ds = xr.open_mfdataset(os.getenv('HOME')+f'/E3SM/scratch/{case}/run/{case}.eam.h1.*.nc')

# reg_name,clat,clon = 'FLORIDA', 27, 360-77
# reg_name,clat,clon = 'FLORIDA', 27, 360-80.5
reg_name,clat,clon = 'GULF', 30, 360-90

# reg_name,clat,clon = 'WESTPAC', -1, 147

num_col = 8

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

scrip_file_name = os.getenv('HOME')+'/Research/E3SM/data_grid/ne30pg2_scrip.nc'

scrip_ds = xr.open_dataset(scrip_file_name)
center_lat = scrip_ds['grid_center_lat'][:].values
center_lon = scrip_ds['grid_center_lon'][:].values
# corner_lat = scrip_ds['grid_corner_lat'][:].values
# corner_lon = scrip_ds['grid_corner_lon'][:].values

cos_dist = np.sin(clat*hc.deg_to_rad)*np.sin(center_lat*hc.deg_to_rad) + \
           np.cos(clat*hc.deg_to_rad)*np.cos(center_lat*hc.deg_to_rad)*np.cos((center_lon-clon)*hc.deg_to_rad)
cos_dist = np.where( cos_dist> 1.0,  1.0, cos_dist )
cos_dist = np.where( cos_dist<-1.0, -1.0, cos_dist )
dist = np.arccos( cos_dist )

ncol_idx = ds['ncol'].sortby( xr.DataArray( dist, coords=[ds['ncol']], dims=['ncol'] ) )[:num_col]

# center_lat = xr.DataArray( center_lat, coords=[ds['ncol']], dims=['ncol'] )
# center_lon = xr.DataArray( center_lon, coords=[ds['ncol']], dims=['ncol'] )
# print(); print(f'ncol_idx: {ncol_idx.values}')
# print(); print(f'lats    : {center_lat.isel(ncol=ncol_idx).values}')
# print(); print(f'lons    : {center_lon.isel(ncol=ncol_idx).values}')
# exit()

# print(); hc.print_stat(dist)
# print(); print( xr.DataArray( dist, coords=[ds['ncol']] ).isel(ncol=ncol_idx).values )

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------

da = ds['PRECT'].isel(ncol=ncol_idx).mean(dim='ncol')

print(); print(da)

print('time index:')
min_ind = da.argmin(dim='time').values
max_ind = da.argmax(dim='time').values
day = max_ind/4+1
min_val = da.isel(time=min_ind).values * 1e3*3600*24  
max_val = da.isel(time=max_ind).values * 1e3*3600*24  
print()
print(f'  max_ind: {max_ind}')
print(f'  day    : {day}')
print(f'  min_val: {min_val}')
print(f'  max_val: {max_val}')
print()



#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------