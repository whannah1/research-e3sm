# Script to perfrom comparison with CMIP5 models
import os, json, glob, subprocess as sp
import xarray as xr,numpy as np
import hapy_common as hc, hapy_E3SM as he
import datetime
import ngl, hapy_setres as hs

case = [
      'E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      'E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      'E3SM.PGVAL.ne30pg3_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      'E3SM.PGVAL.ne30pg4_r05_oECv3.F2010SC5-CMIP6.master-cbe53b',
      ]

# seasons = ['son']
seasons = ['ann','djf','mam','jja','son']

# var_dict_list = [
#    {'name':'pr',     'region':'global','title':'prec (mm day$^{-1}$)','seasons':seasons},
#    {'name':'ua-850', 'region':'global','title':'u850 (m s$^{-1}$)','seasons':seasons},
# ]

var_dict_list = [
   {'name':'rt',     'region':'global','title':'Net TOA (W m$^{-2}$)','seasons':seasons},
   {'name':'rstcre', 'region':'global','title':'SW CRE (W m$^{-2}$)','seasons':seasons},
   {'name':'rltcre', 'region':'global','title':'LW CRE (W m$^{-2}$)','seasons':seasons},
   {'name':'pr',     'region':'global','title':'prec (mm day$^{-1}$)','seasons':seasons},
   {'name':'tas',    'region':'land',  'title':'tas (land, K)','seasons':seasons},
   {'name':'tauu',   'region':'ocean', 'title':u'\N{GREEK SMALL LETTER TAU}$_x$ (ocean, Pa)','seasons':seasons},
   {'name':'ua-200', 'region':'global','title':'u200 (m s$^{-1}$)','seasons':seasons},
   {'name':'ua-850', 'region':'global','title':'u850 (m s$^{-1}$)','seasons':seasons},
   {'name':'zg-500', 'region':'global','title':'Zg-500 (m)','seasons':seasons},
   ### {'name':'ta-850', 'region':'global','title':'ta-850','seasons':seasons},
]

obs_path = os.getenv('HOME')+'/Data/PMP/obs/atm/mo'
scratch  = os.getenv('SCRATCH')

verbose  = False

create_plot = False  # create zonal mean plots for each season of first variable then exit

indent = ' '*4

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
class tcolor:
   ENDC,RED,GREEN,YELLOW,MAGENTA,CYAN = '\033[0m','\033[31m','\033[32m','\033[33m','\033[35m','\033[36m'
out_file_list = []
for v,var in enumerate(var_dict_list):
   
   print()
   if verbose: print('var: '+var['name'])

   #----------------------------------------------------------------------------
   # define var name and plev
   obs_var_name,plev = var['name'],None
   if '-' in var['name']: 
      obs_var_name = var['name'].split('-')[0]
      plev = float(var['name'].split('-')[1]) 

   #----------------------------------------------------------------------------
   # Figure out the preferred data source
   src_name = None
   obs_src_list  = sorted(glob.glob(f'{obs_path}/{obs_var_name}/*'))
   if len(obs_src_list)>1:
      for src_path in obs_src_list: 
         tmp_src_name = src_path.replace(f'{obs_path}/{obs_var_name}/','')
         if tmp_src_name=='ERAINT': src_name = tmp_src_name
      if src_name is None: 
         print(obs_src_list)
         raise ValueError('No valid data source found!')
   else:
      src_name = obs_src_list[0].replace(f'{obs_path}/{obs_var_name}/','')

   #----------------------------------------------------------------------------
   # get data file(s)

   obs_file_list = sorted(glob.glob(f'{obs_path}/{obs_var_name}/{src_name}/ac/*'))
   
   # for file_name in obs_file_list: 
      # print('  '+file_name)
      # os.system(f'ncdump -h {file_name} | grep "lat =" ')
      # os.system(f'ncdump -h {file_name} | grep "lon =" ')

   #----------------------------------------------------------------------------
   # open obs dataset
   obs_file = obs_file_list[0]
   obs_ds = xr.open_dataset(obs_file,decode_times=False).rename({'time':'month'})

   lat,lon = obs_ds['lat'],obs_ds['lon']
   num_lat,num_lon = len(lat),len(lon)

   if verbose: print(f'{indent}obs_file: {obs_file}\n')

   # print(f'    num lat/lon: {num_lat} / {num_lon}')
   # hc.print_stat(lat,name='  lat',stat='nx',compact=True)
   # hc.print_stat(lon,name='  lon',stat='nx',compact=True)

   obs_data = obs_ds[obs_var_name]

   #----------------------------------------------------------------------------
   # Select pressure level (convert to Pa to match file coordinate)
   if plev is not None : obs_data = obs_data.sel(plev=plev*1e2)

   #----------------------------------------------------------------------------
   # Adjust units
   if obs_var_name=='pr': obs_data['units'],obs_data = 'mm/day',obs_data*86400

   #============================================================================
   #============================================================================
   # Loop through cases
   #============================================================================
   #============================================================================
   for c in range(len(case)):
      #-------------------------------------------------------------------------
      # Define output path
      out_dir  = f'{scratch}/e3sm_scratch/cori-knl/{case[c]}/pmp_rmse'
      # out_file = f'{out_dir}/pmp_rmse.{var.get("name")}.nc'
      out_file = f'{out_dir}/pmp_rmse_alt.{var.get("name")}.nc'
      out_file_list.append(out_file)
      #-------------------------------------------------------------------------
      # open the model dataset
      mod_path = f'{scratch}/e3sm_scratch/cori-knl/{case[c]}/data_remap_pmp_{num_lat}x{num_lon}'
      mod_path = f'{mod_path}/{case[c]}.cam.h0.*.remap_{num_lat}x{num_lon}.nc' 

      if verbose: 
         print(f'{indent}case    : {case[c]}')
         print(f'{indent}mod_path: {mod_path}')
         print(f'{indent}out_file: {out_file}')

      mod_ds = xr.open_mfdataset( mod_path, combine='by_coords' )

      # convert time coordinate to represent the middle of the month instead of the end
      # mod_ds['time'] = mod_ds['time'].get_index('time') - datetime.timedelta(days=31)/2
      mod_ds['time'] = xr.DataArray(mod_ds['time_bnds']).mean(dim='nbnd')


      # broadcast dimensions so that mod_time matches data
      # including time is important for ocean variables since ocean frac is not constant
      mod_time,mod_lat,mod_lon = xr.broadcast(mod_ds['time'],mod_ds['lat'],mod_ds['lon'])

      # print(mod_lat)
      # exit()

      # print(mod_ds['time'][:5])
      # exit()


      #-------------------------------------------------------------------------
      # Find the corresponding variable
      if obs_var_name=='rt':     mod_var_name = 'FSNT'
      if obs_var_name=='rstcre': mod_var_name = 'SWCF'
      if obs_var_name=='rltcre': mod_var_name = 'LWCF'
      if obs_var_name=='pr':     mod_var_name = 'PRECC'
      if obs_var_name=='tas':    mod_var_name = 'TS'
      if obs_var_name=='tauu':   mod_var_name = 'TAUX'
      if obs_var_name=='ua':     mod_var_name = 'U'
      if obs_var_name=='zg':     mod_var_name = 'Z3'

      groupby_str = 'time.month'

      #-------------------------------------------------------------------------
      # Load the data
      mod_data = mod_ds[mod_var_name]

      if obs_var_name=='rt': mod_data = mod_data - mod_ds['FLNT' ]
      if obs_var_name=='pr': mod_data = mod_data + mod_ds['PRECL']

      #-------------------------------------------------------------------------
      # calculate monthly average across all years

      # print()
      # hc.print_stat(mod_data.groupby(groupby_str).mean(dim='time').isel(month=0),compact=True)
      # hc.print_stat(mod_data.isel(time=[12-1,24-1,36-1,48-1,60-1]).mean(dim='time'),compact=True)
      # hc.print_stat(mod_data.isel(time=[0,12,24,36,48,60]).mean(dim='time'),compact=True)
      # hc.print_stat(mod_data.isel(time=[0+1,12+1,24+1,36+1,48+1,60+1]).mean(dim='time'),compact=True)
      # exit()

      mod_data = mod_data.groupby(groupby_str).mean(dim='time')
      mod_lat = mod_lat.groupby(groupby_str).mean(dim='time')

      # make time/month coordinate match the obs data
      mod_data.coords['month'] = obs_data.coords['month']
      mod_lat.coords['month'] = obs_data.coords['month']

      #-------------------------------------------------------------------------
      # interpolate to pressure surface if needed
      if plev is not None :
         mod_data = he.interpolate_to_pressure( mod_ds, data_mlev=mod_data, lev=np.array([plev]), extrap_flag=False)
         mod_data = mod_data.isel(lev=0)

      #-------------------------------------------------------------------------
      # Adjust units
      if obs_var_name=='pr':  mod_data['units'],mod_data = 'mm/day',mod_data*86400*1e3
      if obs_var_name=='tauu': mod_data = mod_data*-1

      #-------------------------------------------------------------------------
      # mask out land/ocean points
      if var['region']!='global':
         
         # Load ocean fraction from model data - only load once
         if c==0: 
            ocnfrac = mod_ds['OCNFRAC'].groupby(groupby_str).mean(dim='time')
            landfrac = mod_ds['LANDFRAC'].groupby(groupby_str).mean(dim='time')
            ocnfrac.coords['month'] = obs_data.coords['month']
            landfrac.coords['month'] = obs_data.coords['month']

         # turn masked values into nan
         if var['region']=='ocean':
            obs_data = obs_data.where( ocnfrac>=0.5, drop=True)
            mod_data = mod_data.where( ocnfrac>=0.5, drop=True)
            # mod_lat  = mod_lat.where( ocnfrac.isel(month=0)>=0.5, drop=True)
            mod_lat  = mod_lat.where( ocnfrac>=0.5, drop=True)
         if var['region']=='land': 
            obs_data = obs_data.where( landfrac>=0.5, drop=True)
            mod_data = mod_data.where( landfrac>=0.5, drop=True)
            # mod_lat  = mod_lat.where( landfrac.isel(month=0)>=0.5, drop=True)
            mod_lat  = mod_lat.where( landfrac>=0.5, drop=True)

      # print(mod_lat.shape)
      # exit()
      #-------------------------------------------------------------------------
      # print some diagnostic stuff
      if verbose:
         print()
         # print(obs_data); print(); print(mod_data); print()
         print(indent+'monthly data:')
         hc.print_stat(obs_data,name=f'{indent}obs_var: {obs_var_name:8}',stat='naxh',compact=True)
         hc.print_stat(mod_data,name=f'{indent}mod_var: {mod_var_name:8}',stat='naxh',compact=True)
         print()

      #=========================================================================
      #=========================================================================
      # Calculate RMSE for each season
      #=========================================================================
      #=========================================================================

      # set up plot
      if create_plot:
         fig_file = 'cmip5_rmse_zonal_mean'
         wks = ngl.open_wks('png',fig_file)
         plot,res = [],hs.res_xy()
         res.vpHeightF = 0.3
         res.xyLineThicknessF = 8
         res.xyLineColors   = ['black','red']

      for season in seasons:
         if season=='ann': m_list = [0,1,2,3,4,5,6,7,8,9,10,11]
         if season=='djf': m_list = [11,0,1]
         if season=='mam': m_list = [2,3,4]
         if season=='jja': m_list = [5,6,7]
         if season=='son': m_list = [8,9,10]


         # print()
         # hc.print_stat(obs_data.isel(month=m_list),compact=True)
         # hc.print_stat(mod_data.isel(month=m_list),compact=True)
         # print()
         # hc.print_stat(obs_data.isel(month=m_list).to_masked_array(),compact=True)
         # hc.print_stat(mod_data.isel(month=m_list).to_masked_array(),compact=True)
         # print()
         # hc.print_stat(np.mean( obs_data.isel(month=m_list).to_masked_array(), axis=0),compact=True)
         # hc.print_stat(np.mean( mod_data.isel(month=m_list).to_masked_array(), axis=0),compact=True)
         # print()
         # exit()

         # i,j = 0,170
         # for m in range(12): print(f'  {(obs_data[m,j,i].values):10.4}  {(mod_data[m,j,i].values):10.4}')
         # exit()

         # Calculate seasonal average
         tmp_obs_data = np.mean( obs_data.isel(month=m_list).to_masked_array(), axis=0)
         tmp_mod_data = np.mean( mod_data.isel(month=m_list).to_masked_array(), axis=0)
         tmp_mod_lat  = np.mean( mod_lat.isel(month=m_list).to_masked_array(), axis=0)

         # hc.print_stat(np.mean(tmp_obs_data,axis=1),compact=True)
         # hc.print_stat(np.mean(tmp_mod_data,axis=1),compact=True)

         # plot zonal mean comparison
         if create_plot:
            data_list = []
            data_list.append( np.mean(tmp_obs_data,axis=1) )
            data_list.append( np.mean(tmp_mod_data,axis=1) )
            lat_list = [lat,lat]
            tplot = ngl.xy(wks, np.array(np.stack(lat_list)), np.ma.masked_invalid( np.stack(data_list) ), res)
            hs.set_subtitles(wks, tplot, obs_var_name, season, '', font_height=0.015)
            plot.append( tplot )
         

         if verbose:
            print(f'{indent}{season} average data:')
            hc.print_stat(tmp_obs_data,name=f'{indent}obs_var: {obs_var_name:8}',stat='naxh',compact=True)
            hc.print_stat(tmp_mod_data,name=f'{indent}mod_var: {mod_var_name:8}',stat='naxh',compact=True)
         
         # print()
         # print(var['name'])
         # print(mod_lat.shape)
         # print(tmp_mod_data.shape)
         # print(tmp_obs_data.shape)
         rmse = np.sqrt(np.mean(np.cos(tmp_mod_lat*3.14159/180.)*np.square( tmp_mod_data - tmp_obs_data )))
         # rmse = np.sqrt(np.mean(np.square( tmp_mod_data - tmp_obs_data )))

         # rmse = np.sqrt(np.mean(np.square( tmp_mod_data - tmp_obs_data ))).values
         # rmse = np.sqrt(np.mean(np.square( tmp_mod_data.values - tmp_obs_data.values )))
         # rmse = np.sqrt(np.mean(np.square( tmp_mod_data.to_masked_array() - tmp_obs_data.to_masked_array() )))
         
         rmse_str = f'{tcolor.GREEN}{rmse}{tcolor.ENDC}'

         if verbose:
            print(f'\n{indent}  {season} RMSE: {rmse_str}\n')
         else:
            case_str = f'{tcolor.CYAN}{case[c]:60}{tcolor.ENDC}'
            print(f'{indent}{var.get("name"):8}  {case_str}  {season} RMSE: {rmse_str}')
         #----------------------------------------------------------------------
         # Aggregate RMSE values to list
         if season==seasons[0]: rmse_list = []
         rmse_list.append(rmse)

      # Finalize plot
      if create_plot:
         ngl.panel(wks,plot,[len(plot),1],hs.setres_panel())
         ngl.end()
         hc.trim_png(fig_file)
         exit()

      #-------------------------------------------------------------------------
      # write RMSE dataset to file
      rmse_ds = xr.Dataset()
      rmse_ds.coords['season'] = ('season',seasons)
      rmse_ds[var['name']] = ( ('season',), rmse_list )

      # Create output directory if it does not exist
      if not os.path.exists(out_dir) : os.makedirs(out_dir)

      # delete the file if it already exists
      if os.path.isfile(out_file) : os.remove(out_file)

      rmse_ds.to_netcdf(path=out_file,mode='w')

#-------------------------------------------------------------------------------
print('\nOutput file list:')
for f in out_file_list: print('  '+f)
#-------------------------------------------------------------------------------