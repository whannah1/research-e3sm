

```shell
# gdal_translate -ot Int16 -of netCDF EarthMap_2500x1250.jpg EarthMap_2500x1250.nc

FILE_PREFIX=world.200408.3x5400x2700
gdal_translate -ot Int16 -of netCDF ${FILE_PREFIX}.png ${FILE_PREFIX}.nc


FILE_PREFIX=world.200408.3x5400x2700
gdal_translate -ot Int16 -of netCDF ${FILE_PREFIX}.jpg ${FILE_PREFIX}.jpg.nc

FILE_PREFIX=world.200408x294x196
gdal_translate -ot Int16 -of netCDF ${FILE_PREFIX}.jpg ${FILE_PREFIX}.nc

FILE_PREFIX=/pscratch/sd/w/whannah/Blue_Marble/world.200408.3x21600x10800.august
gdal_translate -ot Int16 -of netCDF ${FILE_PREFIX}.png ${FILE_PREFIX}.nc

FILE_PREFIX=/pscratch/sd/w/whannah/Blue_Marble/world.200408.3x21600x10800.august
gdal_translate -ot Float32 -of netCDF ${FILE_PREFIX}.png ${FILE_PREFIX}.nc

```


OpenCV overlay from forum post
https://stackoverflow.com/questions/40895785/using-opencv-to-overlay-transparent-image-onto-another-image
```python
import cv2

background = cv2.imread("background.png", cv2.IMREAD_UNCHANGED)
foreground = cv2.imread("overlay.png", cv2.IMREAD_UNCHANGED)

# normalize alpha channels from 0-255 to 0-1
alpha_background = background[:,:,3] / 255.0
alpha_foreground = foreground[:,:,3] / 255.0

# set adjusted colors
for color in range(0, 3):
    background[:,:,color] = alpha_foreground * foreground[:,:,color] + \
        alpha_background * background[:,:,color] * (1 - alpha_foreground)

# set adjusted alpha and denormalize back to 0-255
background[:,:,3] = (1 - (1 - alpha_foreground) * (1 - alpha_background)) * 255

# display the image
cv2.imshow("Composited image", background)
cv2.waitKey(0)
```