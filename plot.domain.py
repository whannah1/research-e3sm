import os, ngl, copy, xarray as xr, numpy as np

# input_file_path = '/global/homes/w/whannah/HICCUP/data_scratch/files_domain/domain.lnd.ne30pg2_oEC60to30v3.20240423.nc'
# input_file_path = '/global/homes/w/whannah/HICCUP/data_scratch/files_domain/domain.ocn.ne30pg2_oEC60to30v3.20240423.nc'

# input_file_path = '/global/homes/w/whannah/E3SM/E3SM_SRC0/tools/generate_domain_files/domain.lnd.ne30pg2_oEC60to30v3.20240424.nc'
# input_file_path = '/global/homes/w/whannah/E3SM/E3SM_SRC0/tools/generate_domain_files/domain.ocn.ne30pg2_oEC60to30v3.20240424.nc'

dom_list = []
dom_list.append('/global/homes/w/whannah/E3SM/E3SM_SRC0/tools/generate_domain_files/domain.lnd.ne30pg2_oEC60to30v3.20240424.nc')
dom_list.append('/global/cfs/cdirs/e3sm/inputdata/share/domains/domain.lnd.ne30pg2_oEC60to30v3.200220.nc')

# dom_list.append('/global/homes/w/whannah/E3SM/E3SM_SRC0/tools/generate_domain_files/domain.lnd.ne70pg2_oEC60to30v3.20240424.nc')
# dom_list.append('/global/cfs/cdirs/e3sm/inputdata/share/domains/domain.lnd.ne70pg2_oEC60to30v3.240313.nc')

plot_diff = True

# scrip_file_name = '/global/homes/w/whannah/E3SM/data_grid/ne30pg2_scrip.nc'

var = ['frac','mask']
# var = ['frac']

### output figure type and name
fig_file,fig_type = 'domain','png'

#---------------------------------------------------------------------------------------------------
# Set up plot resources
num_dom = len(dom_list)
num_var = len(var)

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
if plot_diff:
   plot = [None]*(num_dom*2-1)*num_var
else:
   plot = [None]*num_dom*num_var

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.lbOrientation                = 'Horizontal'
res.lbLabelFontHeightF           = 0.008
res.mpGridAndLimbOn              = False
res.mpCenterLonF                 = 180
res.mpLimitMode                  = 'LatLon'
res.cnFillPalette                = 'MPL_viridis'

#---------------------------------------------------------------------------------------------------
# define function to add subtitles to the top of plot
#---------------------------------------------------------------------------------------------------
def set_subtitles(wks, plot, left_string='', center_string='', right_string='', font_height=0.01):
   ttres         = ngl.Resources()
   ttres.nglDraw = False

   ### Use plot extent to call ngl.text(), otherwise you will see this error: 
   ### GKS ERROR NUMBER   51 ISSUED FROM SUBROUTINE GSVP  : --RECTANGLE DEFINITION IS INVALID
   strx = ngl.get_float(plot,'trXMinF')
   stry = ngl.get_float(plot,'trYMinF')
   ttres.txFontHeightF = font_height

   ### Set annotation resources to describe how close text is to be attached to plot
   amres = ngl.Resources()
   if not hasattr(ttres,'amOrthogonalPosF'):
      amres.amOrthogonalPosF = -0.52   # Top of plot plus a little extra to stay off the border
   else:
      amres.amOrthogonalPosF = ttres.amOrthogonalPosF

   ### Add left string
   amres.amJust,amres.amParallelPosF = 'BottomLeft', -0.5   # Left-justified
   tx_id_l   = ngl.text(wks, plot, left_string, strx, stry, ttres)
   anno_id_l = ngl.add_annotation(plot, tx_id_l, amres)
   # Add center string
   amres.amJust,amres.amParallelPosF = 'BottomCenter', 0.0   # Centered
   tx_id_c   = ngl.text(wks, plot, center_string, strx, stry, ttres)
   anno_id_c = ngl.add_annotation(plot, tx_id_c, amres)
   # Add right string
   amres.amJust,amres.amParallelPosF = 'BottomRight', 0.5   # Right-justified
   tx_id_r   = ngl.text(wks, plot, right_string, strx, stry, ttres)
   anno_id_r = ngl.add_annotation(plot, tx_id_r, amres)

   return
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('\n  var: '+var[v])
   for d in range(num_dom):
      print('    dom: '+dom_list[d])
      #----------------------------------------------------------------------------
      # read the data
      ds = xr.open_dataset( dom_list[d] )
      
      data = ds[var[v]].isel(nj=0).rename({'ni':'ncol'})

      #----------------------------------------------------------------------------
      # Set colors and contour levels
      tres = copy.deepcopy(res)
      # Set up cell fill attributes with grid info from domain file
      tres.cnFillMode    = 'CellFill'
      tres.sfXArray      = ds['xc'].isel(nj=0).rename({'ni':'ncol'}).values
      tres.sfYArray      = ds['yc'].isel(nj=0).rename({'ni':'ncol'}).values
      tres.sfXCellBounds = ds['xv'].isel(nj=0).rename({'ni':'ncol'}).values
      tres.sfYCellBounds = ds['yv'].isel(nj=0).rename({'ni':'ncol'}).values
      #----------------------------------------------------------------------------
      # Create plot
      ip = v*(num_dom*2-1) + d if plot_diff else v*num_dom + d
      plot[ip] = ngl.contour_map(wks,data.values,tres)
      set_subtitles(wks, plot[ip], var[v], '', '', font_height=0.01)
      #----------------------------------------------------------------------------
      if plot_diff:
         tres.mpOutlineBoundarySets = 'NoBoundaries'
         if d==0:
            baseline = data.values
         else:
            ip = v*(num_dom*2-1) + d*2
            diff = data.values - baseline
            print(' '*6+f'diff min: {np.min(diff)}')
            print(' '*6+f'diff avg: {np.mean(diff)}')
            print(' '*6+f'diff max: {np.max(diff)}')
            print(' '*6+f'diff num: {np.sum(diff!=0)}')
            plot[ip] = ngl.contour_map(wks,diff,tres)
            set_subtitles(wks, plot[ip], var[v], '', 'difference', font_height=0.01)
#---------------------------------------------------------------------------------------------------
# Finalize plot
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

if plot_diff:
   layout = [num_var,num_dom*2-1]
else:
   layout = [num_var,num_dom]

ngl.panel(wks,plot,layout,pres)
ngl.end()

### trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------