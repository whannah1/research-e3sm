import os, copy, ngl, glob, xarray as xr, numpy as np
#-------------------------------------------------------------------------------
name,case,case_dir,case_sub = [],[],[],[]
def add_case(case_in,n=None,p=None,s=None):
   global name,case,case_dir,case_sub
   tmp_name = case_in if n is None else n
   case.append(case_in); name.append(tmp_name); 
   case_dir.append(p); case_sub.append(s); 
#-------------------------------------------------------------------------------

# specify case name and name for plot title as list
case, name, input_file_path = [],[],[]

# case.append('E3SM.GNU.ne4pg2_ne4pg2.F-MMFXX.RADNX_4.01'); name.append('MMFXX')
# case.append('v2.snow.snowRedisOnly'); name.append('v2.snow.snowRedisOnly')

# case.append('SCREAMv1 no-TMS'); name.append('SCREAMv1 no-TMS')
# case.append('SCREAMv1 w/TMS');  name.append('SCREAMv1 w/TMS')



# path to input files
# for c in case: input_file_path.append(f'/ccs/home/hannah6/E3SM/scratch/{c}/run/{c}.cam.h1.*.nc')
# for c in case: input_file_path.append(f'/lcrc/group/e3sm/ac.jeffery/E3SMv2/{c}/run/{c}.eam.h0.*.nc')

# input_file_path.append('/lcrc/group/e3sm/ac.onguba/scratch/chrys/sky-scream-v1-opt0-notms/run/climoout-year1/output.aaa.monthly.AVERAGE.nmonths_x1_ANN_000101_000112_climo.nc')
# input_file_path.append('/lcrc/group/e3sm/ac.onguba/scratch/chrys/sky-scream-v1-opt0/run/climoout-year1/output.aaa.monthly.AVERAGE.nmonths_x1_ANN_000101_000112_climo.nc')

# input_file_path.append('/lcrc/group/e3sm/ac.onguba/scratch/chrys/sky-scream-v1-opt0-notms/run/output.aaa.monthly.AVERAGE.nmonths_x1.eam.h0.0001-01.nc')
# input_file_path.append('/lcrc/group/e3sm/ac.onguba/scratch/chrys/sky-scream-v1-opt0/run/output.aaa.monthly.AVERAGE.nmonths_x1.eam.h0.0001-01.nc')

# case.append('ne30');name.append('ne30');input_file_path.append('/lcrc/group/e3sm/ac.whannah/scratch/chrys/E3SM.2023-L80-SPINUP-00.ne30pg2_EC30to60E2r2.F2010/run/E3SM.2023-L80-SPINUP-00.ne30pg2_EC30to60E2r2.F2010.eam.i.0002-01-01-00000.nc')
# case.append('ne30');name.append('ne30');input_file_path.append('/lcrc/group/e3sm/ac.whannah/HICCUP/data/eami_mam4_Linoz_ne30np4_L80_c20231010.nc')
# case.append('ne30');name.append('ne30');input_file_path.append('/lcrc/group/e3sm/data/inputdata/atm/cam/inic/homme/NGD_v3atm.ne30pg2.eam.i.0001-01-01-00000.c20230106.nc')

# tmp_path,tmp_sub = '/global/cfs/cdirs/m4310/whannah/E3SM','archive/atm/hist'
# add_case('20230629.v3alpha02.amip.chrysalis.L72',n='E3SM L72',p=tmp_path,s=tmp_sub)
# add_case('20230629.v3alpha02.amip.chrysalis.L80',n='E3SM L80',p=tmp_path,s=tmp_sub)


htype = 'h0'
num_files = 12*1


### scrip file for native grid plot
scrip_file_name = os.getenv('HOME')+'/E3SM/data_grid/ne30pg2_scrip.nc'

# list of variables to plot
var = ['PRECT','TMQ','TGCLDLWP','TGCLDIWP']

plot_diff = False

# output figure file name and type
fig_file, fig_type = 'e3sm.map', 'png'

# flag to print some stats for sanity checking
print_stats = True

#---------------------------------------------------------------------------------------------------
# Set up plot resources
num_var, num_case = len(var), len(case)

# create plot workstation and plot object
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*(num_var*num_case)

### set oup the plot resources
res = ngl.Resources()
res.nglDraw,res.nglFrame         = False, False # turn off the automatic drawing/framing
res.tmXBOn,res.tmYLOn            = False, False # Turn off tick marks
### modify the color bar (i.e. "labelbar")
res.lbLabelBarOn                 = True
res.lbLabelFontHeightF           = 0.014
res.lbOrientation                = 'Horizontal'
res.lbLabelFontHeightF           = 0.008
### switch from contrours to filled cells
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.cnFillOn                     = True
# turn off map grid and center on the Pacific
res.mpGridAndLimbOn              = False

# res.mpCenterLonF                 = 180

# res.mpLimitMode = 'LatLon' 
# res.mpMaxLatF             =  90.
# res.mpMinLatF             =  60.
# res.mpCenterLatF          =  90. 


# Sterographic Polar View
# res.mpProjection         = 'Stereographic'
# res.mpEllipticalBoundary = True
# res.mpCenterLatF         = -90.
# res.mpLimitMode          = 'Angles'
# res.mpBottomAngleF       = 50
# res.mpLeftAngleF         = 50
# res.mpRightAngleF        = 50
# res.mpTopAngleF          = 50

### use this to zoom in on a region
# res.mpLimitMode = 'LatLon' 
# res.mpMinLatF   = 45 -15
# res.mpMaxLatF   = 45 +15
# res.mpMinLonF   = 180-15
# res.mpMaxLonF   = 180+15

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# create regional mask - or comment this whole block to disable the regional subset

# scripfile = xr.open_dataset(scrip_file_name).rename({'grid_size':'ncol'})
# ncol = scripfile['ncol'].values
# tmp_data = np.ones([len(ncol)],dtype=bool)
# tmp_coords = [('ncol', ncol)]
# mask = xr.DataArray( tmp_data, coords=tmp_coords, dims='ncol' )
# mask = mask & (scripfile['grid_center_lat'].values>=40) 
# # mask = mask & (scripfile['grid_center_lat'].values<=) 
# # mask = mask & (scripfile['grid_corner_lon'].values>=) 
# # mask = mask & (scripfile['grid_corner_lon'].values<=) 
# mask = mask.compute()

#---------------------------------------------------------------------------------------------------
# routine for printing various helpful statistics
def print_stat(x,fmt='f',stat='naxh',indent='',compact=False):
   """ Print min, avg, max, and std deviation of input """
   if fmt=='f' : fmt = '%.4f'
   if fmt=='e' : fmt = '%e'
   msg = ''
   line = f'{indent}'
   if not compact: msg += line+'\n'
   for c in list(stat):
      if not compact: line = indent
      if c=='h': line += 'shp: '+str(x.shape)
      if c=='a': line += 'avg: '+fmt%x.mean()
      if c=='n': line += 'min: '+fmt%x.min()
      if c=='x': line += 'max: '+fmt%x.max()
      if c=='s': line += 'std: '+fmt%x.std()
      if not compact: msg += line+'\n'
      if compact: line += ' '*2
   if compact: msg += line#+'\n'
   print(msg)
   return msg
#---------------------------------------------------------------------------------------------------
# routine to add subtitles to the top of plot
def set_subtitles(wks, plot, left_string='', right_string='', font_height=0.01):
   ttres          = ngl.Resources()
   ttres.nglDraw  = False

   # Use plot extent to call ngl.text(), otherwise you will see this error: 
   # GKS ERROR NUMBER   51 ISSUED FROM SUBROUTINE GSVP  : --RECTANGLE DEFINITION IS INVALID
   strx = ngl.get_float(plot,'trXMinF')
   stry = ngl.get_float(plot,'trYMinF')
   ttres.txFontHeightF = font_height

   # Set annotation resources to describe how close text is to be attached to plot
   amres = ngl.Resources()
   if not hasattr(ttres,'amOrthogonalPosF'):
      amres.amOrthogonalPosF = -0.52   # Top of plot plus a little extra to stay off the border
   else:
      amres.amOrthogonalPosF = ttres.amOrthogonalPosF

   # Add left string
   amres.amJust,amres.amParallelPosF = 'BottomLeft', -0.5   # Left-justified
   tx_id_l   = ngl.text(wks, plot, left_string, strx, stry, ttres)
   anno_id_l = ngl.add_annotation(plot, tx_id_l, amres)
   # Add right string
   amres.amJust,amres.amParallelPosF = 'BottomRight', 0.5   # Right-justified
   tx_id_r   = ngl.text(wks, plot, right_string, strx, stry, ttres)
   anno_id_r = ngl.add_annotation(plot, tx_id_r, amres)

   return
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('-'*80)
   print(f'  var: {var[v]}')
   data_list = []
   for c in range(num_case):
      print(f'    case: {case[c]}')
      #-------------------------------------------------------------------------
      # specify the path to the input files
      input_file_path = f'{case_dir[c]}/{case[c]}/{case_sub[c]}/{case[c]}.eam.{htype}.*.nc'

      # open the files as an xarray dataset
      file_list_tmp = sorted(glob.glob(input_file_path))

      # if 'num_files' in globals(): file_list_tmp = file_list_tmp[-num_files:] # backwards from last file
      if 'num_files' in globals(): file_list_tmp = file_list_tmp[:num_files] # forwards from first file

      # ds = xr.open_mfdataset( file_list_tmp, combine='by_coords', concat_dim='time', use_cftime=True )
      ds = xr.open_mfdataset( file_list_tmp, combine='nested', concat_dim='time', use_cftime=True )

      #-------------------------------------------------------------------------
      # load the data
      if var[v]=='PRECT' and 'PRECT' not in ds:
         data = ds['PRECC'] + ds['PRECL']
      elif var[v]=='wind_speed':
         data = ( ds['U']**2 + ds['V']**2 )**0.5
         data = data.isel(lev=71)
      else:
         data = ds[var[v]]

      # load the area for calculating global mean - comment this out to disable
      area = ds['area']

      # handle of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=0)

      #-------------------------------------------------------------------------
      # Adjust units
      if var[v]=='PRECT':   data['units'],data.values = 'mm/day',data*86400.*1e3
      if var[v]=='PRECC':   data['units'],data.values = 'mm/day',data*86400.*1e3
      if var[v]=='PRECL':   data['units'],data.values = 'mm/day',data*86400.*1e3
      if var[v]=='Q':       data['units'],data.values = 'g/kg',  data*1e3
      if var[v]=='Q850':    data['units'],data.values = 'g/kg',  data*1e3
      if var[v]=='CLDLIQ':  data['units'],data.values = 'g/kg',  data*1e3
      if var[v]=='CLDICE':  data['units'],data.values = 'g/kg',  data*1e3
      if var[v]=='TS':      data['units'],data.values = 'C',     data - 273
      if var[v]=='QRS':     data['units'],data.values = 'K/day', data*86400.
      if var[v]=='QRL':     data['units'],data.values = 'K/day', data*86400.

      # handle of lev dimension
      if 'lev' in data.dims : data = data.isel(lev=-1)
      # if 'lev' in data.dims : data = data.isel(lev=0)
      #-------------------------------------------------------------------------

      # print some statistics as a sanity check
      if print_stats: 
         print_stat(data,stat='naxsh',compact=True,indent=' '*4)

      # average over time dimension
      if 'time' in data.dims : 
         if len(data['time'])==1:
            data = data.isel(time=0)
         else:
            data = data.mean(dim='time',skipna=True)

      #-------------------------------------------------------------------------
      # Calculate area weighted global mean
      if 'area' in vars() :
         gbl_mean = ( (data*area).sum() / area.sum() ).values 
         print(' '*4+f'Area Weighted Global Mean : {gbl_mean:6.4}')

      if 'mask' in locals():
         data = data.where( mask, drop=True)

      # add it to the list with other cases
      data_list.append( data.values )

   #------------------------------------------------------------------------------------------------
   # Plot map of time averaged data

   # get min and max so that colorbar levels are the same for all cases
   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])

   if plot_diff:
      diff_base = 0
      tmp_data = data_list - data_list[diff_base]
      for c in range(num_case): tmp_data[c] = data_list[c] - data_list[diff_base]
      diff_data_min = np.min([np.nanmin(d) for d in tmp_data])
      diff_data_max = np.max([np.nanmax(d) for d in tmp_data])
      for c in range(1,num_case): data_list[c] = data_list[c] - data_list[diff_base]

   for c in range(num_case):
      # set plot index
      plot_id = c*num_var+v
      #-------------------------------------------------------------------------
      # Set colors and contour levels

      # make a copy of the plot resources
      tres = copy.deepcopy(res)
      # specify non-default color table
      tres.cnFillPalette = 'MPL_viridis'
      # if var[v] in ['CLDLOW','CLDMED','CLDHGH'] : tres.cnFillPalette = 'CBR_wet'
      # if var[v] in ['TGCLDLWP','TGCLDIWP']      : tres.cnFillPalette = 'MPL_viridis'
      # if var[v] in ['DYN_QLIQ']                 : tres.cnFillPalette = 'MPL_viridis'
      # if var[v] in ['TS']                       : tres.cnFillPalette = 'BlueRed'
      # if var[v] in ['U','V','UBOT','VBOT','U850','V850']: 
      #    tres.cnFillPalette = 'BlueWhiteOrangeRed'

      if plot_diff and c>0: tres.cnFillPalette = 'BlueWhiteOrangeRed'

      #-------------------------------------------------------------------------
      # specify custom value ranges
      # if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,20+1,1)/1e1
      # if var[v]=='TGCLDLWP'            : tres.cnLevels = np.logspace( -2 , 0.25, num=40)

      if plot_diff and c>0:
         tmp_data_min, tmp_data_max = diff_data_min, diff_data_max
      else:
         tmp_data_min, tmp_data_max = data_min, data_max

      # if custom levels are specified above then use them, otherwise find "nice" level set
      if hasattr(tres,'cnLevels') : 
         tres.cnLevelSelectionMode = 'ExplicitLevels'
      else:
         aboutZero = False
         if var[v] in ['U','V','U850','V850'] : aboutZero = True
         if plot_diff and c>0: aboutZero = True
         clev_tup = ngl.nice_cntr_levels(tmp_data_min, tmp_data_max,cint=None, max_steps=21,
                                         returnLevels=False, aboutZero=aboutZero )
         if clev_tup==None: 
            tres.cnLevelSelectionMode = 'AutomaticLevels'   
         else:
            cmin,cmax,cint = clev_tup
            tres.cnLevels = np.linspace(cmin,cmax,num=21)
            tres.cnLevelSelectionMode = 'ExplicitLevels'

      #-------------------------------------------------------------------------
      # Set up cell fill attributes using scrip grid file
      scripfile = xr.open_dataset(scrip_file_name).rename({'grid_size':'ncol'})
      tres.cnFillMode    = 'CellFill'
      if 'mask' in locals():
         tres.sfXArray      = scripfile['grid_center_lon'].where( mask, drop=True).values
         tres.sfYArray      = scripfile['grid_center_lat'].where( mask, drop=True).values
         tres.sfXCellBounds = scripfile['grid_corner_lon'].where( mask, drop=True).values
         tres.sfYCellBounds = scripfile['grid_corner_lat'].where( mask, drop=True).values
      else:
         tres.sfXArray      = scripfile['grid_center_lon'].values
         tres.sfYArray      = scripfile['grid_center_lat'].values
         tres.sfXCellBounds = scripfile['grid_corner_lon'].values
         tres.sfYCellBounds = scripfile['grid_corner_lat'].values
      #-------------------------------------------------------------------------
      # Create plot

      plot[plot_id] = ngl.contour_map(wks,data_list[c],tres) 

      case_name = ''
      if 'name' in vars(): case_name = name[c]

      # scale subtitle font size so it decreases with more panels
      subtitle_font_height = 0.02 - 0.0015*num_var - 0.0014*(num_case)
      set_subtitles(wks, plot[plot_id], case_name, var[v], font_height=subtitle_font_height)

#---------------------------------------------------------------------------------------------------
# Finalize plot

# print another line
print('-'*80)

# specify the multi-panel layout
if num_case==1 or num_var==1 : 
   layout = [np.ceil(len(plot)/2),2]
else:
   # layout = [num_var,num_case]
   layout = [num_case,num_var]

# draw and frame the plots
pnl_res = ngl.Resources()
pnl_res.nglPanelYWhiteSpacePercent = 5
pnl_res.nglPanelXWhiteSpacePercent = 5
ngl.panel(wks,plot,layout,pnl_res)
ngl.end()

# trim the white space using imagemagik
fig_file = fig_file+'.'+fig_type
os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )

# print the final file
print(f'\n{fig_file}\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------