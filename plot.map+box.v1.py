import os, ngl, subprocess as sp, numpy as np, xarray as xr
import hapy_common as hc

fig_type = 'png'
fig_file = os.getenv('HOME')+'/Research/E3SM/map+box.v1'

#---------------------------------------------------------------------------------------------------
# map  and box bounds
#---------------------------------------------------------------------------------------------------
# lat1,lat2,lon1,lon2 = 15,25,360-60,360-50
# blat1,blat2,blon1,blon2 = 15,25,360-60,360-50

# xlat,xlon = 40,-40
# xlat,xlon = 30,180
# xlat,xlon = -10,75
# xlat,xlon = -10,-60
# xlat,xlon = 15,5
xlat,xlon = 5,260
mdx,mdy,bdx = 60,60,10
bdy = bdx
if 'xlat' in locals(): 
  lat1,lat2 = xlat-mdy,xlat+mdy
  # lon1,lon2 = xlon-mdx,xlon+mdx
  blat1,blat2,blon1,blon2 = xlat-bdy,xlat+bdy,xlon-bdx,xlon+bdx

lat1,lat2 = -80,80

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
wkres = ngl.Resources()
npix = 2048; wkres.wkWidth,wkres.wkHeight=npix,npix

wks = ngl.open_wks(fig_type,fig_file,wkres)
plot = [None]*1
   
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.mpGridAndLimbOn              = False
res.mpCenterLonF                 = 180
res.mpLimitMode                  = "LatLon"

if 'lat1' in locals(): res.mpMinLatF,res.mpMaxLatF = lat1,lat2
if 'lon1' in locals(): res.mpMinLonF,res.mpMaxLonF = lon1,lon2

res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
# res.lbLabelFontHeightF           = 0.018
res.tmXBOn                       = False
res.tmYLOn                       = False
res.mpGeophysicalLineThicknessF  = 2.0
# res.mpGeophysicalLineColor       = 'white'

#---------------------------------------------------------------------------------------------------
# Plot map
#---------------------------------------------------------------------------------------------------

plot[0] = ngl.map(wks,res) 

#---------------------------------------------------------------------------------------------------
# Add box
#---------------------------------------------------------------------------------------------------

# ngl.overlay(plot,ngl.xy)

pgx = [blon1,blon1,blon2,blon2,blon1]
pgy = [blat1,blat2,blat2,blat1,blat1]

pgres = ngl.Resources()
pgres.gsEdgesOn         = True
pgres.gsEdgeThicknessF  = 4.
pgres.gsEdgeColor       = 'red'
pgres.gsFillColor       = 'red'
pgres.gsFillIndex       = 14#6
# pgres.gsFillOpacityF    = 0.1

gdum = ngl.add_polygon(wks,plot[0],pgx,pgy,pgres)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# title_str = '???'
# textres =  ngl.Resources()
# textres.txFontHeightF =  0.025
# ngl.text_ndc(wks,title_str,0.5,.7,textres)

pnl_res = ngl.Resources()

### add panel labels
# pnl_res.nglPanelFigureStrings            = list(string.ascii_lowercase)
# pnl_res.nglPanelFigureStringsJust        = "TopLeft"
# pnl_res.nglPanelFigureStringsFontHeightF = 0.01

# pnl_res.nglPanelYWhiteSpacePercent = 5

ngl.panel(wks,plot,[len(plot),1],pnl_res)
ngl.end()

hc.trim_png(fig_file)

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------