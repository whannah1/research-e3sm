import os
import ngl
import xarray as xr
import numpy as np
import hapy_common as hc
import hapy_E3SM   as he
import hapy_setres as hs
import copy
home = os.getenv("HOME")
print()

scratch_main = '/gpfs/alpine/cli115/scratch/hannah6/inputdata/atm/cam/inic/homme'
scratch_hiccup = '/gpfs/alpine/scratch/hannah6/cli115/HICCUP/data'
file,clr,dsh = [],[],[]
# file.append(f'{scratch_hiccup}/HICCUP.cami_mam3_Linoz_ne4np4.L50.nc'); dsh.append(0); clr.append('red')
file.append(f'{scratch_main}/cami_aquaplanet_ne4np4_L72_c190218.nc'); dsh.append(0); clr.append('blue')
file.append(f'{scratch_hiccup}/HICCUP.AQUA.ne4np4.L50.nc'); dsh.append(0); clr.append('red')


var = ['T','Q']


fig_type = "png"
fig_file = home+"/Research/E3SM/ncdata.profile.v1"


# lat1,lat2 = -30,30

use_height_coord = False

omit_bottom = True


#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
num_file = len(file)

if 'name' not in locals() or name==[]: 
   name = [None]*(num_file)

if 'dsh' not in locals(): 
   if num_file>1 : dsh = np.zeros(num_file)
   else : dsh = [0]

if 'lev' not in vars(): lev = np.array([0])

wks = ngl.open_wks(fig_type,fig_file)
# plot = [None]*(num_var*num_file)
plot = [None]*(num_var)
res = hs.res_xy()
res.vpWidthF = 0.4
# res.tmYLLabelFontHeightF         = 0.008
# res.tmXBLabelFontHeightF         = 0.008
res.xyMarkLineMode         = "MarkLines"
res.xyMarker = 16
res.xyMarkerSizeF = 0.005

if not use_height_coord: 
   res.trYReverse = True
   res.xyYStyle = 'Log'


# res.trXMinF = 12

# if use_height_coord:
#    res.trYMaxF = 30e3
#    # res.trYMinF = 10e3
# else:
#    res.trYMaxF = 100

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   hc.printline()
   print('  var: '+var[v])
   data_list,lev_list = [],[]
   for f in range(num_file):
      print('    file: '+file[f])
      
      #-------------------------------------------------------------------------
      # read the data
      #-------------------------------------------------------------------------   
      
      ds = xr.open_dataset(file[f])

      # print(ds)
      # exit()

      if use_height_coord: 
         Z = case_obj.load_data('Z3',htype=htype,first_file=first_file,num_files=num_files)

      area = ds['area']
      X    = ds[var[v]]
    

      # X = X.isel(time=ss_t)
      # if use_height_coord: Z = Z.isel(time=ss_t)
      
      
      if use_height_coord: Z = ( (Z*area).sum(dim='ncol') / area.sum(dim='ncol') )#.values 
      X = ( (X*area).sum(dim='ncol') / area.sum(dim='ncol') )#.values 



      if omit_bottom and use_height_coord:
         X = X[:-1]
         Z = Z[:-1]


      # hc.print_stat(X,name=f'{var[v]} after averaging',indent='    ')
      # hc.print_stat(Z,name=f'Z after averaging',indent='    ')

      # gbl_mean = ( (X*area).sum() / area.sum() ).values 
      # print('\n      Area Weighted Global Mean : '+'%f'%gbl_mean+'\n')
      
      data_list.append( X.values )
      if use_height_coord:
         lev_list.append( Z.values )
      else:
         lev_list.append( X['lev'].values )

   
   #-------------------------------------------------------------------------
   # Set colors and contour levels
   #-------------------------------------------------------------------------
   tres = copy.deepcopy(res)

   #-------------------------------------------------------------------------
   # Create plot
   #-------------------------------------------------------------------------

   ip = f*num_var+v

   # clr = np.linspace(2,len(ngl.retrieve_colormap(wks))-1,len(X.ncol),dtype=int)
   # tres.xyLineColor = -1

   # tres.xyDashPattern = 1
   # plot[ip] = ngl.xy(wks,X.transpose().values,X.lev.values,tres) 
   # plot[ip] = ngl.xy(wks,X.transpose().values,Z.transpose().values,tres) 

   # for n in range(len(X.ncol)) :
   #    if LWP.isel(ncol=n)<0.02 :
   #       # tres.xyLineColor = clr[n]
   #       tres.xyLineColor = 'red'
   #       # tres.xyDashPattern = 1
   #    else:
   #       tres.xyLineColor = 'blue'
   #       # tres.xyDashPattern = 0
   #    # ngl.overlay(plot[ip], ngl.xy(wks,X.isel(ncol=n).values,X.lev.values,tres) )
   #    ngl.overlay(plot[ip], ngl.xy(wks,X.isel(ncol=n).values,Z.isel(ncol=n).values,tres) )
    

   ### use for global average profile
   # tres.xyLineColors = clr
   # tres.xyDashPatterns = dsh
   # ip = v
   # plot[ip] = ngl.xy(wks, np.stack(data_list), np.stack(lev_list), tres) 

   data_min = np.min([np.nanmin(d) for d in data_list])
   data_max = np.max([np.nanmax(d) for d in data_list])
   tres.trXMinF = data_min
   tres.trXMaxF = data_max
   ip = v

   if var[v]=='Q' : tres.xyXStyle = 'Log'

   for f in range(num_file):
      tres.xyLineColor = clr[f]
      tres.xyMarkerColor = clr[f]
      tres.xyDashPattern = dsh[f]
      tplot = ngl.xy(wks, data_list[f], lev_list[f], tres)
      if f==0:
         plot[ip] = tplot
      else:
         ngl.overlay(plot[ip],tplot)


   ctr_str = ''
   var_str = var[v]

   hs.set_subtitles(wks, plot[ip], '', ctr_str, var_str, font_height=0.015)


#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
# layout = [len(plot),1]
layout = [num_file,num_var]

#-- draw a common title string on top of the panel
# textres               =  ngl.Resources()
# textres.txFontHeightF =  0.02                  #-- title string size
# ngl.text_ndc(wks,f'time step = {ss_t}',0.5,.97,textres)  #-- add title to plot

pres = hs.setres_panel()
pres.nglPanelTop      =  0.93

ngl.panel(wks,plot,layout,pres)
ngl.end()

hc.trim_png(fig_file)
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------