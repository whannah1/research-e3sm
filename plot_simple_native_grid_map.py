import os, glob, ngl, copy, xarray as xr, numpy as np

# full path of input file
case,case_name = 'v2.LR.historical_0101','E3SMv2'
data_root = '/global/cfs/cdirs/m3312/whannah/e3smv2_historical'
file_path = f'{data_root}/{case}/archive/atm/hist/{case}.eam.h0.1900*.nc'

# scrip grid file
scrip_file_name = '/global/homes/w/whannah/E3SM/data_grid/ne30pg2_scrip.nc'

# list of variables to plot
var = ['PRECC','TMQ']

# output figure name and type
fig_file,fig_type = 'simple_native_grid_map','png'



#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)
# create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = [None]*num_var

# set up the plot resources
res = ngl.Resources()
res.nglDraw                 = False
res.nglFrame                = False
res.tmXTOn                  = False
res.tmXBMajorOutwardLengthF = 0.
res.tmXBMinorOutwardLengthF = 0.
res.tmYLMajorOutwardLengthF = 0.
res.tmYLMinorOutwardLengthF = 0.
res.tmYLLabelFontHeightF    = 0.015
res.tmXBLabelFontHeightF    = 0.015
res.tiXAxisFontHeightF      = 0.015
res.tiYAxisFontHeightF      = 0.015
res.tmXBMinorOn             = False
res.tmYLMinorOn             = False
res.tmYLLabelFontHeightF    = 0.008
res.tmXBLabelFontHeightF    = 0.008
res.lbLabelFontHeightF      = 0.012
res.cnFillOn                = True
res.cnLinesOn               = False
res.cnLineLabelsOn          = False
res.cnInfoLabelOn           = False
res.lbOrientation           = "Horizontal"
res.lbLabelFontHeightF      = 0.008
res.mpGridAndLimbOn         = False
res.mpCenterLonF            = 180
res.mpLimitMode             = "LatLon" 
res.cnFillPalette           = "MPL_viridis" # perceptually uniform colormap


# use these lines to zoom in on a region

# lat1,lat2 = -40,40
# lat1,lat2,lon1,lon2 = 0,40,360-160,360-80     # East Pac
# lat1,lat2,lon1,lon2 = 0,40,360-90,360-10      # Atlantic/carribean

# if 'lat1' in vars() : res.mpMinLatF = lat1
# if 'lat2' in vars() : res.mpMaxLatF = lat2
# if 'lon1' in vars() : res.mpMinLonF = lon1
# if 'lon2' in vars() : res.mpMaxLonF = lon2

#---------------------------------------------------------------------------------------------------
# define function to add subtitles to the top of plot
#---------------------------------------------------------------------------------------------------
def set_subtitles(wks, plot, left_string='', center_string='', right_string='', font_height=0.01):
   ttres         = ngl.Resources()
   ttres.nglDraw = False

   ### Use plot extent to call ngl.text(), otherwise you will see this error: 
   ### GKS ERROR NUMBER   51 ISSUED FROM SUBROUTINE GSVP  : --RECTANGLE DEFINITION IS INVALID
   strx = ngl.get_float(plot,'trXMinF')
   stry = ngl.get_float(plot,'trYMinF')
   ttres.txFontHeightF = font_height

   ### Set annotation resources to describe how close text is to be attached to plot
   amres = ngl.Resources()
   if not hasattr(ttres,'amOrthogonalPosF'):
      amres.amOrthogonalPosF = -0.52   # Top of plot plus a little extra to stay off the border
   else:
      amres.amOrthogonalPosF = ttres.amOrthogonalPosF

   ### Add left string
   amres.amJust,amres.amParallelPosF = 'BottomLeft', -0.5   # Left-justified
   tx_id_l   = ngl.text(wks, plot, left_string, strx, stry, ttres)
   anno_id_l = ngl.add_annotation(plot, tx_id_l, amres)
   # Add center string
   amres.amJust,amres.amParallelPosF = 'BottomCenter', 0.0   # Centered
   tx_id_c   = ngl.text(wks, plot, center_string, strx, stry, ttres)
   anno_id_c = ngl.add_annotation(plot, tx_id_c, amres)
   # Add right string
   amres.amJust,amres.amParallelPosF = 'BottomRight', 0.5   # Right-justified
   tx_id_r   = ngl.text(wks, plot, right_string, strx, stry, ttres)
   anno_id_r = ngl.add_annotation(plot, tx_id_r, amres)

   return
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+var[v])
   #----------------------------------------------------------------------------
   # read the data
   file_list = sorted(glob.glob(file_path))
   ds = xr.open_mfdataset( file_list )
   data = ds[var[v]]
   #----------------------------------------------------------------------------
   # Calculate time mean
   if 'time' in data.dims: data = data.mean(dim='time')
   #----------------------------------------------------------------------------
   # adjust units
   if var[v] in ['PRECT','PRECC','PRECL'] : data = data*86400.*1e3
   if var[v]=='PS': data = data/1e2
   #----------------------------------------------------------------------------
   # Set colors and contour levels
   tres = copy.deepcopy(res)
   #----------------------------------------------------------------------------
   # specify specific contour intervals
   if var[v] in ['PRECT','PRECC']   : tres.cnLevels = np.arange(1,10+1,1)
   if var[v]=='LHFLX'               : tres.cnLevels = np.arange(5,205+5,5)
   if var[v]=='TGCLDLWP'            : tres.cnLevels = np.logspace( -2,-0.2, num=60).round(decimals=2) # better for non-MMF
   if var[v]=='PS'                  : tres.cnLevels = np.arange(940,1040+1,1)
   if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'
   #----------------------------------------------------------------------------
   # attributes needed for plotting an unstructured grid
   scripfile = xr.open_dataset(scrip_file_name)
   tres.cnFillMode    = 'CellFill'
   tres.sfXArray      = scripfile['grid_center_lon'].rename({'grid_size':'ncol'}).values
   tres.sfYArray      = scripfile['grid_center_lat'].rename({'grid_size':'ncol'}).values
   tres.sfXCellBounds = scripfile['grid_corner_lon'].rename({'grid_size':'ncol'}).values
   tres.sfYCellBounds = scripfile['grid_corner_lat'].rename({'grid_size':'ncol'}).values
   #----------------------------------------------------------------------------
   # Create plot
   plot[v] = ngl.contour_map(wks,data.values,tres)
   set_subtitles(wks, plot[v], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

layout = [len(plot),1]

ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

# trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------