import os
import xarray as xr
import numpy as np
import hapy_E3SM   as he
import hapy_common as hc

# Only use 2 cases!

# case = ['E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.F-MMF1.base',
#         'E3SM.ENS_5.PREQX.ne4pg2_ne4pg2.F-MMF1.test']

case = ['E3SM.ne30pg2_r05_oECv3.F-MMF1.00',
        'E3SM.ne30pg2_r05_oECv3.F-MMF1.SFC.00']

var,lev = [],[]
var.append('PRECT');    lev.append(None)
var.append('LHFLX');    lev.append(None)
var.append('SHFLX');    lev.append(None)
var.append('TMQ');      lev.append(None)
var.append('TGCLDLWP'); lev.append(None)
var.append('TGCLDIWP'); lev.append(None)
var.append('FLNT');     lev.append(None)
var.append('FLNS');     lev.append(None)
var.append('FSNT');     lev.append(None)
var.append('FSNS');     lev.append(None)
# var.append('SWCF');     lev.append(None)
# var.append('LWCF');     lev.append(None)
# var.append('OMEGA'); lev.append(850)
# var.append('U'); lev.append(850)

# lat1,lat2,lon1,lon2 = 0,90,180,360 # N. America
htype,years,first_file,num_files = 'h0',[],0,0

str_len = 15
indent_len = 8

class tformat: END,BOLD,ULINE = '\033[0m','\033[1m','\033[4m'

# Print table header
header = ' '*indent_len +'Avg 1'.rjust(str_len) +'Avg 2'.rjust(str_len) +'RMSE'.rjust(str_len)
header = tformat.BOLD+tformat.ULINE+header+tformat.END
print('\n'+header)

for v in range(len(var)):
   tlev = lev[v]
   if tlev is None: tlev = np.array([0])
   for c in range(len(case)):
      # read the data
      case_obj = he.Case( name=case[c] )
      
      area = case_obj.load_data('area',htype=htype,years=years,first_file=first_file,num_files=num_files).astype(np.double)
      data = case_obj.load_data(var[v],htype=htype,years=years,first_file=first_file,num_files=num_files,lev=tlev)

      # ninst = 5
      # for i in range(ninst):
      #    if i==0:
      #       area = case_obj.load_data('area',component=f'cam_{(i+1):04}',htype=htype,years=years,num_files=num_files).astype(np.double)
      #    if i>0: data_prev = data
      #    data = case_obj.load_data(var[v],component=f'cam_{(i+1):04}',htype=htype,years=years,num_files=num_files,lev=tlev)
      #    if i>0: data = (data+data_prev)/2
      
      # average over time dimension
      if 'time' in data.dims : 
         # hc.print_time_length(data.time)
         data = data.mean(dim='time')
      
      # Calculate area weighted global mean
      data_mean = ( (data*area).sum() / area.sum() ).values 

      # Calculate RMSE
      if c==0:
         base_data = data
         base_mean = data_mean
      if c==1:
         data_rmse = (np.sqrt( np.square( data - base_data ).mean(dim=['ncol']) )).values

         msg = tformat.BOLD+ var[v].rjust(indent_len) +tformat.END
         msg += f'{base_mean:8.4}'.rjust(str_len) 
         msg += f'{data_mean:8.4}'.rjust(str_len) 
         msg += f'{data_rmse:8.4}'.rjust(str_len)
         print(msg)
         
print()
