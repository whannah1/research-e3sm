import hy
import struct, numpy as npy

def is_none(a): return a is None
def first(a): return a[0]
def is_zero(a): return a == 0

def vis_read_arrays(fname):
    def vis_read_array(f):
        b = f.read(4)
        if is_zero(len(b)): return None
        ndim = first(struct.unpack('@i', b))
        b = f.read(4 * ndim)
        dims = struct.unpack('@' + 'i' * ndim, b)
        return npy.fromfile(f, count=npy.prod(dims)).reshape(dims)
    beg = 0
    end = -1
    stride = 1
    d = []
    with open(fname, 'rb') as f:
        i = 0
        i_next = beg
        while True:
            a = vis_read_array(f)
            if is_none(a): break
            if i == i_next:
                d.append(a)
                i_next = i_next + stride
            i += 1
    return d
