import os, ngl, copy, xarray as xr, numpy as np, warnings

### case name for plot title
case_name = 'E3SM'

### full path of input file
scratch_dir = '/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl'
case = 'INCITE2019.GPU.ne120pg2.FC5AV1C-H01A.SP1_64x1_1000m.20191026'
input_file_path = f'{scratch_dir}/{case}/run/{case}.cam.h0.0005-01.nc'

### list of variables to plot 
### (this script is set up for a contour plot with level information)
var = ['U','V']

### output figure type and name
fig_file,fig_type = 'zonal_mean_plot.v1','png'

### parameters for defining the zonal averaging "bins"
lat1, lat2, dlat = -88., 88., 2

#---------------------------------------------------------------------------------------------------
# Set up plot resources
#---------------------------------------------------------------------------------------------------
num_var = len(var)

### create the plot workstation
wks = ngl.open_wks(fig_type,fig_file)
plot = []

### set oup the plot resources
res = ngl.Resources()
res.nglDraw                      = False
res.nglFrame                     = False
res.tmXTOn                       = False
res.tmXBMajorOutwardLengthF      = 0.
res.tmXBMinorOutwardLengthF      = 0.
res.tmYLMajorOutwardLengthF      = 0.
res.tmYLMinorOutwardLengthF      = 0.
res.tmYLLabelFontHeightF         = 0.015
res.tmXBLabelFontHeightF         = 0.015
res.tiXAxisFontHeightF           = 0.015
res.tiYAxisFontHeightF           = 0.015
res.tmXBMinorOn                  = False
res.tmYLMinorOn                  = False
res.tmYLLabelFontHeightF         = 0.008
res.tmXBLabelFontHeightF         = 0.008
res.lbLabelFontHeightF           = 0.012
res.cnFillOn                     = True
res.cnLinesOn                    = False
res.cnLineLabelsOn               = False
res.cnInfoLabelOn                = False
res.lbOrientation                = "Horizontal"
res.lbLabelFontHeightF           = 0.008

res.vpHeightF = 0.3
res.trYReverse = True
# res.tiXAxisString = 'Latitude'
res.tiXAxisString = 'sin( Latitude )'
res.tiYAxisString = 'Pressure [hPa]'

#---------------------------------------------------------------------------------------------------
# define function to add subtitles to the top of plot
#---------------------------------------------------------------------------------------------------
def set_subtitles(wks, plot, left_string='', center_string='', right_string='', font_height=0.01):
   ttres         = ngl.Resources()
   ttres.nglDraw = False

   ### Use plot extent to call ngl.text(), otherwise you will see this error: 
   ### GKS ERROR NUMBER   51 ISSUED FROM SUBROUTINE GSVP  : --RECTANGLE DEFINITION IS INVALID
   strx = ngl.get_float(plot,'trXMinF')
   stry = ngl.get_float(plot,'trYMinF')
   ttres.txFontHeightF = font_height

   ### Set annotation resources to describe how close text is to be attached to plot
   amres = ngl.Resources()
   if not hasattr(ttres,'amOrthogonalPosF'):
      amres.amOrthogonalPosF = -0.52   # Top of plot plus a little extra to stay off the border
   else:
      amres.amOrthogonalPosF = ttres.amOrthogonalPosF

   ### Add left string
   amres.amJust,amres.amParallelPosF = 'BottomLeft', -0.5   # Left-justified
   tx_id_l   = ngl.text(wks, plot, left_string, strx, stry, ttres)
   anno_id_l = ngl.add_annotation(plot, tx_id_l, amres)
   ### Add center string
   amres.amJust,amres.amParallelPosF = 'BottomCenter', 0.0   # Centered
   tx_id_c   = ngl.text(wks, plot, center_string, strx, stry, ttres)
   anno_id_c = ngl.add_annotation(plot, tx_id_c, amres)
   ### Add right string
   amres.amJust,amres.amParallelPosF = 'BottomRight', 0.5   # Right-justified
   tx_id_r   = ngl.text(wks, plot, right_string, strx, stry, ttres)
   anno_id_r = ngl.add_annotation(plot, tx_id_r, amres)

   return
#---------------------------------------------------------------------------------------------------
# Overly Complicated Binning Routine
#---------------------------------------------------------------------------------------------------
def bin_YbyX (Vy,Vx,bins=[],bin_min=0,bin_max=1,bin_spc=1,bin_spc_log=20,nbin_log=2,
              bin_mode="manual",verbose=False,wgt=[],
              keep_time=False,keep_lev=False):
   """ Average Vy into bins of Vx values according to bins and bin_mode. 
   Manual mode takes an array bin center values and determines the bin spacings. 
   Explicit mode takes a list of bin edge values, which is useful for an 
   irregularly spacing, such as logarithmic.  """
   #----------------------------------------------------------------------------
   # Manual mode - use min, max, and spc (i.e. stride) to define bins
   if bin_mode == "manual":
      nbin    = np.round( ( bin_max - bin_min + bin_spc )/bin_spc ).astype(np.int)
      bins    = np.linspace(bin_min,bin_max,nbin)
      bin_coord = xr.DataArray( bins )
   #----------------------------------------------------------------------------
   # Explicit mode - requires explicitly defined bin edges
   if bin_mode == "explicit":
      bins = xr.DataArray( bins )   # convert the input to a DataArray
      nbin = len(bins)-1
      bin_coord = ( bins[0:nbin-1+1] + bins[1:nbin+1] ) /2.
   #----------------------------------------------------------------------------
   # Log mode - logarithmically spaced bins that increase in width by bin_spc_log [%]
   if bin_mode == "log":
      bin_log_wid = np.zeros(nbin_log)
      bin_log_ctr = np.zeros(nbin_log)
      bin_log_ctr[0] = bin_min
      bin_log_wid[0] = bin_spc
      for b in range(1,nbin_log):
         bin_log_wid[b] = bin_log_wid[b-1] * (1.+bin_spc_log/1e2)  # note - bin_spc_log is in %
         bin_log_ctr[b] = bin_log_ctr[b-1] + bin_log_wid[b-1]/2. + bin_log_wid[b]/2.
      nbin = nbin_log
      bin_coord = xr.DataArray( bin_log_ctr )
   #----------------------------------------------------------------------------
   # create output data arrays
   #----------------------------------------------------------------------------
   nlev  = len(Vy['lev'])  if 'lev'  in Vy.dims else 1
   ntime = len(Vy['time']) if 'time' in Vy.dims else 1

   if ntime==1 and keep_time==True : keep_time = False

   shape,dims,coord = (nbin,),'bin',[('bin', bin_coord)]
   
   if nlev>1 and keep_lev and not keep_time :
      coord = [ ('bin', bin_coord), ('lev', Vy['lev']) ]
      shape = (nbin,nlev)
      dims = ['bin','lev']   
   if nlev==1 and not keep_lev and not keep_time :
      shape,dims,coord = (nbin,),'bin',[('bin', bin_coord)]
   if nlev==1 and keep_time==True :
      coord = [('bin', bin_coord), ('time', Vy['time'])]
      shape = (nbin,ntime)
      dims = ['bin','time']
   
   mval = np.nan
   bin_val = xr.DataArray( np.full(shape,mval,dtype=Vy.dtype), coords=coord, dims=dims )
   bin_std = xr.DataArray( np.full(shape,mval,dtype=Vy.dtype), coords=coord, dims=dims )
   bin_cnt = xr.DataArray( np.zeros(shape,dtype=Vy.dtype), coords=coord, dims=dims )
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   lev_chk=False
   if 'lev' in Vy.dims and len(Vy.lev)>1 and keep_lev : lev_chk = True
   if lev_chk :
      avg_dims = ['ncol']
      if 'time' in Vy.dims : avg_dims = ['time','ncol']
      avg_dims_wgt = ['ncol']

   val_chk = np.isfinite(Vx.values)

   if keep_time and 'time' in Vy.dims:
      if wgt.dims != Vy.dims : 
         wgt, *__ = xr.broadcast(wgt, Vy) 
         wgt = wgt.transpose('time','ncol')
   #----------------------------------------------------------------------------
   # Loop through bins
   #----------------------------------------------------------------------------
   for b in range(nbin):
      if bin_mode == "manual":
         bin_bot = bin_min - bin_spc/2. + bin_spc*(b  )
         bin_top = bin_min - bin_spc/2. + bin_spc*(b+1)
      if bin_mode == "explicit":
         bin_bot = bins[b]  .values
         bin_top = bins[b+1].values
      if bin_mode == "log":
         bin_bot = bin_log_ctr[b] - bin_log_wid[b]/2.
         bin_top = bin_log_ctr[b] + bin_log_wid[b]/2.
      condition = xr.DataArray( np.full(Vx.shape,False,dtype=bool), coords=Vx.coords )
      condition.values = ( np.where(val_chk,Vx.values,bin_bot-1e3) >=bin_bot ) \
                        &( np.where(val_chk,Vx.values,bin_bot-1e3)  <bin_top )
      if np.sum(condition.values)>0 :
         if lev_chk :
            ### xarray method
            if len(wgt)==0 : 
               bin_val[b,:] = Vy.where(condition,drop=True).mean( dim=avg_dims, skipna=True )
            else:
               if wgt.dims != Vy.dims : 
                  wgt, *__ = xr.broadcast(wgt, Vy) 
                  if 'time' in Vy.dims :
                     wgt = wgt.transpose('time','lev','ncol')
                  else :
                     wgt = wgt.transpose('lev','ncol')
               if 'time' in Vy.dims : 
                  bin_val[b,:] = ( (Vy*wgt).where(condition,drop=True).sum( dim='ncol', skipna=True ) \
                                      / wgt.where(condition,drop=True).sum( dim='ncol', skipna=True ) ).mean(dim='time', skipna=True )
               else:
                  bin_val[b,:] = ( (Vy*wgt).where(condition,drop=True).sum( dim='ncol', skipna=True ) \
                                      / wgt.where(condition,drop=True).sum( dim='ncol', skipna=True ) )
            bin_std[b,:] = Vy.where(condition,drop=True).std(  dim=avg_dims, skipna=True )
            bin_cnt[b,:] = Vy.where(condition,drop=True).count(dim=avg_dims)

         elif keep_time and 'time' in Vy.dims:
            bin_val[b,:] = ( (Vy*wgt).where(condition,drop=True).sum( dim='ncol', skipna=True ) \
                                / wgt.where(condition,drop=True).sum( dim='ncol', skipna=True ) )
            bin_cnt[b] = np.sum( np.where(condition, 1, 0 ) )
         else:
            ### NOTE - we can't avoid xarray divide warnings with dask arrays
            bin_val[b] = Vy.where(condition).mean(skipna=True)
            bin_std[b] = Vy.where(condition).std(skipna=True)
            bin_cnt[b] = np.sum( condition )
   #----------------------------------------------------------------------------
   #----------------------------------------------------------------------------
   # use a dataset to hold all the output
   dims = ('bins',)
   if lev_chk and not keep_time : dims = ('bins','lev')
   if not lev_chk and keep_time : dims = ('bins','time')
   if lev_chk and keep_time     : dims = ('bins','time','lev')
   
   bin_ds = xr.Dataset()
   bin_ds['bin_val'] = (dims, bin_val )
   bin_ds['bin_std'] = (dims, bin_std )
   bin_ds['bin_cnt'] = (dims, bin_cnt )
   bin_ds['bin_pct'] = (dims, bin_cnt/bin_cnt.sum()*1e2 )
   bin_ds.coords['bins'] = ('bins',bin_coord)
   if lev_chk : bin_ds.coords['lev'] = ( 'lev', xr.DataArray(Vy['lev']) )

   #----------------------------------------------------------------------------
   return bin_ds
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
for v in range(num_var):
   print('  var: '+var[v])
   #----------------------------------------------------------------------------
   # read the data
   #----------------------------------------------------------------------------
   ds = xr.open_dataset( input_file_path )

   lat  = ds['lat']
   area = ds['area_p']
   data = ds[var[v]]

   #----------------------------------------------------------------------------
   # Calculate time and zonal mean
   #----------------------------------------------------------------------------
   with warnings.catch_warnings():
      warnings.simplefilter("ignore", category=RuntimeWarning)

      bin_ds = bin_YbyX( data.mean(dim='time', skipna=True), lat, \
                            bin_min=lat1, bin_max=lat2, \
                            bin_spc=dlat, wgt=area, keep_lev=True )

   lat_bins = bin_ds['bins'].values
   sin_lat_bins = np.sin(lat_bins*np.pi/180.)

   data_binned = np.ma.masked_invalid( bin_ds['bin_val'].transpose().values )

   #----------------------------------------------------------------------------
   # Set colors and contour levels
   #----------------------------------------------------------------------------
   res = copy.deepcopy(res)
   res.cnFillPalette = "MPL_viridis" ### use a perceptually uniform colormap

   ### specify specific contour intervals
   # if var[v]=="OMEGA"      : tres.cnLevels = np.linspace(-1,1,21)*0.1
   # if hasattr(tres,'cnLevels') : tres.cnLevelSelectionMode = 'ExplicitLevels'

   aboutZero = False
   if (var[v] in ['U','V']) : aboutZero = True
   
   data_min = np.min(data_binned)
   data_max = np.max(data_binned)

   cmin,cmax,cint,clev = ngl.nice_cntr_levels(data_min, data_max, cint=None, max_steps=21, \
                                              returnLevels=True,aboutZero=aboutZero )
   res.cnLevels = np.linspace(cmin,cmax,num=21)
   res.cnLevelSelectionMode = "ExplicitLevels"
   
   #----------------------------------------------------------------------------
   # Create plot
   #----------------------------------------------------------------------------

   res.sfXArray = sin_lat_bins
   lat_tick = np.array([-90,-60,-30,0,30,60,90])
   res.tmXBMode = "Explicit"
   res.tmXBValues = np.sin( lat_tick*3.14159/180. )
   res.tmXBLabels = lat_tick

   res.sfYCStartV = min( bin_ds['lev'].values )
   res.sfYCEndV   = max( bin_ds['lev'].values )

   plot.append( ngl.contour(wks, data_binned, res) )

   set_subtitles(wks, plot[len(plot)-1], case_name, '', var[v], font_height=0.01)

#---------------------------------------------------------------------------------------------------
# Finalize plot
#---------------------------------------------------------------------------------------------------
pres = ngl.Resources()
pres.nglPanelYWhiteSpacePercent = 5
pres.nglPanelXWhiteSpacePercent = 5

layout = [len(plot),1]

ngl.panel(wks,plot[0:len(plot)],layout,pres)
ngl.end()

### trim white space from image using imagemagik
if fig_type == 'png' :
   fig_file = fig_file+'.png'
   os.system( 'convert -trim +repage '+fig_file+'   '+fig_file )
   print('\n'+fig_file+'\n')

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------