import xarray as xr, numpy as np

ds = xr.open_dataset('/global/homes/w/whannah/Data/Obs/NOAA/daily/HICCUP.sst_noaa.2008-10-01.nc')

ncol_coord = np.arange(len(ds['lat'])*len(ds['lon']))
dims   = ['time','ncol']
coords = [ds['time'],ncol_coord]

ds_flat = xr.Dataset()

for v in ds.variables:
  if all(coord in ds[v].coords for coord in ['lat','lon']):
    ds_flat[v] = xr.DataArray(ds[v].stack(ncol=('lat','lon')).values,dims=dims,coords=coords)
  else:
    ds_flat[v] = ds[v]

print();print(ds)
print();print(ds_flat)

exit()

ncol_idx = np.arange(len(ds['ncol']))

print();print(ncol_idx)

da = da.set_index(ncol=ncol_idx)

print();print(da)

# da.to_netcdf('test_flatten.nc')