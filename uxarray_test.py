import os, uxarray as ux, holoviews as hv
# import matplotlib.pyplot as plt
import cartopy.crs as ccrs
# import cartopy.feature as cfeature

grid_path = os.getenv('HOME')+f'/E3SM/data_grid/ne30pg2_scrip.nc'
data_path = '/global/homes/w/whannah/E3SM/scratch_pm-cpu/E3SM.2024-SCIDAC-heating-test-01.F2010/run/E3SM.2024-SCIDAC-heating-test-01.F2010.eam.h1.0001-01-01-00000.nc'

uxds = ux.open_mfdataset(grid_path, data_path)
uxgrid = uxds.uxgrid

uxda = uxds['PRECT'].isel(time=0)

plot = uxda.plot.polygons(rasterize=False,
                          projection=ccrs.Robinson(),
                          cmap=ux.cmaps.diverging,
                          line_color='white',
                         )
# plot = uxda.plot.polygons(backend='matplotlib')


fig_file = 'uxarray_test.png'
hv.save(plot,fig_file)

print(f'\n{fig_file}\n')

