import uxarray as ux
# import matplotlib
import matplotlib.pyplot as plt
import hvplot.pandas
import holoviews as hv
import cartopy.crs as ccrs
#--------------------------------------------
grid_file = '/lcrc/group/e3sm/data/inputdata/share/meshes/homme/ne30pg2_scrip_c20191218.nc'

fpath_scratch = '/lcrc/group/e3sm/ac.eva.sinha'
caseid = '20240703_SSP245_ZATM_BGC_ne30pg2_f09_oEC60to30v3'

data_file = f'{fpath_scratch}/{caseid}/run/{caseid}.eam.h0.2075-06.nc'

var = 'PRECC'

fig_file = f'uxarray_test.v1.{var}.png'

#--------------------------------------------
print()
print(f'grid_file: {grid_file}')
print(f'data_file: {data_file}')
print()
#--------------------------------------------
uxds = ux.open_dataset(grid_file, data_file)
uxda = uxds[var].isel(time=0)
gdf_data = uxda.to_geodataframe()
#--------------------------------------------
hv.extension("matplotlib")

plot_kwargs = {
    "cmap": "inferno",
    "width": 400,
    "height": 200,
}

gdf_data.hvplot.polygons(**plot_kwargs)
# gdf_data.hvplot.polygons(**plot_kwargs, rasterize=True)
# gdf_data.hvplot.polygons(**plot_kwargs, rasterize=True, projection=ccrs.Robinson())

plt.savefig(fig_file)
#--------------------------------------------
print(f'fig_file:  {fig_file}')
print()
#--------------------------------------------
