import uxarray as ux, holoviews as hv, cartopy.crs as ccrs
hv.extension('matplotlib')
#-------------------------------------------------------------------------------

caseid = '20240703_SSP245_ZATM_BGC_ne30pg2_f09_oEC60to30v3'

scratch = '/lcrc/group/e3sm/ac.eva.sinha'
data_file = f'{scratch}/{caseid}/run/{caseid}.eam.h0.2075-06.nc'
grid_file = '/lcrc/group/e3sm/data/inputdata/share/meshes/homme/ne30pg2_scrip_c20191218.nc'

var = 'PRECC'

fig_file = f'uxarray_test.v2.{var}.png'

#-------------------------------------------------------------------------------
print()
print(f'grid_file: {grid_file}')
print(f'data_file: {data_file}\n')
#-------------------------------------------------------------------------------
# Read the data
uxds = ux.open_dataset(grid_file, data_file)
uxda = uxds[var].isel(time=0)
gdf_data = uxda.to_geodataframe()
print(); print(uxda)
print(); print(gdf_data)
exit()
#-------------------------------------------------------------------------------
# Create the plot
out = uxda.plot(backend='matplotlib', 
                fig_size=350,
                cmap='viridis',
                # projection=ccrs.Mollweide(central_longitude=180),
                )
hv.save(out, fig_file, fmt='png')
#-------------------------------------------------------------------------------
print(f'fig_file:  {fig_file}\n')
#-------------------------------------------------------------------------------
