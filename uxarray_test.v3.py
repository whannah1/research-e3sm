import uxarray as ux
# import holoviews as hv
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
# import hvplot.pandas
# hv.extension('matplotlib')
#-------------------------------------------------------------------------------
caseid = '20240703_SSP245_ZATM_BGC_ne30pg2_f09_oEC60to30v3'
scratch = '/lcrc/group/e3sm/ac.eva.sinha'
data_file = f'{scratch}/{caseid}/run/{caseid}.eam.h0.2075-06.nc'
grid_file = '/lcrc/group/e3sm/data/inputdata/share/meshes/homme/ne30pg2_scrip_c20191218.nc'
var = 'PRECC'
fig_file = f'uxarray_test.v3.{var}.png'
#-------------------------------------------------------------------------------
print()
print(f'grid_file: {grid_file}')
print(f'data_file: {data_file}\n')
#----------------------------------------------------------
uxds = ux.open_dataset(grid_file, data_file)
# print()
# print(uxds['lon'].min().values)
# print(uxds['lon'].max().values)
# print(uxds['lat'].min().values)
# print(uxds['lat'].max().values)
# print()
# exit()
uxda = uxds[var].isel(time=0)
if var=='PRECC': uxda = uxda*86400*1e3 # convert to mm/day
pc, _ = uxda.to_polycollection()
#----------------------------------------------------------
map_proj = ccrs.Mollweide(central_longitude=180)
pc.set_transform(ccrs.PlateCarree())
pc.set_cmap("inferno")
plt.figure(figsize=(20, 10))
ax = plt.axes(projection=map_proj)
ax.add_collection(pc)
ax.set_global()
ax.set_extent((-180, 180, -90, 90), crs=map_proj)
plt.colorbar(pc)
plt.savefig(fig_file)
#----------------------------------------------------------
# pc.set_transform(ccrs.PlateCarree())
# plt.figure(figsize=(20, 10))
# ax = plt.axes(projection=ccrs.Robinson())
# pc.set_cmap("inferno")
# ax.add_collection(pc)
# ax.set_global()
# ax.set_extent((-180, 180, -90, 90), crs=ccrs.PlateCarree())
# # ax.set_extent((0, 360, -90, 90), crs=map_proj)
# plt.colorbar(pc)
# plt.savefig(fig_file)
#----------------------------------------------------------
print(f'fig_file:  {fig_file}\n')
#----------------------------------------------------------


