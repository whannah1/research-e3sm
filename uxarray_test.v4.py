import xarray as xr
import uxarray as ux
import holoviews as hv
import cartopy.crs as ccrs
import geoviews as gv
import geoviews.feature as gf
# import hvplot.pandas
hv.extension('matplotlib')
#-------------------------------------------------------------------------------
caseid = '20240703_SSP245_ZATM_BGC_ne30pg2_f09_oEC60to30v3'
scratch = '/lcrc/group/e3sm/ac.eva.sinha'
data_file = f'{scratch}/{caseid}/run/{caseid}.eam.h0.2075-06.nc'
grid_file = '/lcrc/group/e3sm/data/inputdata/share/meshes/homme/ne30pg2_scrip_c20191218.nc'
var = 'PRECC'
fig_file = f'uxarray_test.v4.{var}.png'
#-------------------------------------------------------------------------------
print()
print(f'grid_file: {grid_file}')
print(f'data_file: {data_file}\n')
#-------------------------------------------------------------------------------
# Read the data
# grid_ds = xa.open_dataset(grid_file)
# uxds = ux.open_dataset(grid_file, data_file)
# uxda = uxds[var].isel(time=0)
# gdf_data = uxda.to_geodataframe()
#-------------------------------------------------------------------------------
ds = gv.Dataset(xr.open_dataset(data_file))
ensemble = ds.to(gv.Image, ['lon', 'lat'], var)

gv.output(ensemble.opts(cmap='viridis', 
                        colorbar=True, 
                        fig_size=120, 
                        backend='matplotlib') * gf.coastline(),
          backend='matplotlib')
gv.save(out, fig_file, fmt='png')
#-------------------------------------------------------------------------------
# out = gdf_data.hvplot.polygons(cmap='viridis',
#                                rasterize=True,
#                                projection=ccrs.Mollweide(central_longitude=180),
#                               )
# hv.save(out, fig_file, fmt='png')
#-------------------------------------------------------------------------------
print(f'fig_file:  {fig_file}\n')
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# print(); print(uxda)
# print(); print(grid_ds)
# print(); print(gdf_data)
# exit()