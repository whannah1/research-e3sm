import xarray as xr
import uxarray as ux
import holoviews as hv
import cartopy.crs as ccrs
import geoviews as gv
import geoviews.feature as gf
# import hvplot.pandas
hv.extension('matplotlib')
#-------------------------------------------------------------------------------
'''
source /global/common/software/e3sm/anaconda_envs/load_latest_e3sm_unified_pm-cpu.sh
'''
#-------------------------------------------------------------------------------
# caseid = '20240703_SSP245_ZATM_BGC_ne30pg2_f09_oEC60to30v3'
# scratch = '/lcrc/group/e3sm/ac.eva.sinha'
# data_file = f'{scratch}/{caseid}/run/{caseid}.eam.h0.2075-06.nc'
#-------------------------------------------------------------------------------
# # LCRC
# caseid = 'SCREAM.2025-PC-00.F2010-SCREAMv1-DYAMOND1.ne256pg2.cfr_1.acc_10.rsc_0.eci_1e-05.eri_1e-05.mti_7.4e7'
# scratch = '/lustre/orion/cli115/proj-shared/hannah6/e3sm_scratch'
# data_file = f'{scratch}/{caseid}/run/output.scream.2D.1hr.inst.INSTANT.nhours_x1.2016-09-01-00000.nc'
# grid_file = '/ccs/home/hannah6/E3SM/data_grid/ne256pg2_scrip.nc'
# var = 'precip_total_surf_mass_flux'
#-------------------------------------------------------------------------------
# NERSC
caseid = 'E3SM_2024-SCIDAC-00_F20TR_ne30pg2_EF_0.35_CF_10_HD_0.50_HM_02.5_SS_10.0_PS_700'
scratch = '/pscratch/sd/w/whannah/e3sm_scratch/pm-cpu'
data_file = f'{scratch}/{caseid}/run/{caseid}.eam.h0.1999-12.nc'
grid_file = '/global/homes/w/whannah/E3SM/data_grid/ne30pg2_scrip.nc'
var = 'PRECC'
#-------------------------------------------------------------------------------
fig_file = f'uxarray_test.v5.{var}.png'
#-------------------------------------------------------------------------------
print()
print(f'grid_file: {grid_file}')
print(f'data_file: {data_file}\n')
#-------------------------------------------------------------------------------
# Read the data
# grid_ds = xa.open_dataset(grid_file)
# uxds = ux.open_dataset(grid_file, data_file)
# uxda = uxds[var].isel(time=0)
# gdf_data = uxda.to_geodataframe()
#-------------------------------------------------------------------------------
ds = gv.Dataset(xr.open_dataset(data_file))
ensemble = ds.to(gv.Image, ['lon', 'lat'], var)

gv.output(ensemble.opts(cmap='viridis', 
                        colorbar=True, 
                        fig_size=120, 
                        backend='matplotlib') * gf.coastline(projection=ccrs.Robinson()),
          backend='matplotlib')
gv.save(out, fig_file, fmt='png')
#-------------------------------------------------------------------------------
# out = gdf_data.hvplot.polygons(cmap='viridis',
#                                rasterize=True,
#                                projection=ccrs.Mollweide(central_longitude=180),
#                               )
# hv.save(out, fig_file, fmt='png')
#-------------------------------------------------------------------------------
print(f'fig_file:  {fig_file}\n')
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# print(); print(uxda)
# print(); print(grid_ds)
# print(); print(gdf_data)
# exit()