import os, subprocess as sp, numpy as np, xarray as xr, copy, string, dask
import scipy, pywt, pycwt
from statsmodels.tsa.arima.model import ARIMA
#---------------------------------------------------------------------------------------------------
'''
source /global/common/software/e3sm/anaconda_envs/load_latest_e3sm_unified_pm-cpu.sh
source activate pyn_env
'''
#---------------------------------------------------------------------------------------------------

fig_file,fig_type = 'wavelet_spectrum_SST_reproducer','png'

region,lat1,lat2,lon1,lon2 = 'Nino3'  ,-5,5,360-150,360-90

year_start = 1871
year_end   = 1996

plot_mode = 'ngl' # ngl / mpl

#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def deseason(xraw):
    # Calculates the deseasonalized data
    months_per_year = 12
    # Create array to hold climatological values and deseasonalized data
    # Create months_per_year x 1 array of zeros
    xclim = np.zeros((months_per_year, 1))
    # Create array with same shape as xraw
    x_deseasoned = np.zeros(xraw.shape)
    # Iterate through all 12 months.
    for month in np.arange(months_per_year):
        # `xraw[month::12]` will return the data for this month every year (12 months)
        # (i.e., from month until the end of xraw, get every 12th month)
        # Get the mean of this month, using data from every year, ignoring NaNs
        xclim[month] = np.nanmean(xraw[month::months_per_year])
    num_years = int(np.floor(len(x_deseasoned) / months_per_year))
    # Iterate through all years in x_deseasoned (same number as in xraw)
    for year in np.arange(num_years):
        year_index = year * months_per_year
        # Iterate through all months of the year
        for month in np.arange(months_per_year):
            month_index = year_index + month
            # Subtract the month's mean over num_years from xraw's data for this month in this year
            # i.e., get the difference between this month's value and it's "usual" value
            x_deseasoned[month_index] = xraw[month_index] - xclim[month][0]
    return x_deseasoned
#---------------------------------------------------------------------------------------------------
wavelet_degree    = 6
wavelet_period_mn = np.arange(6,12*64+1)
wavelet_period_yr = wavelet_period_mn/12.
wavelet_freq      = 1/wavelet_period_mn
#---------------------------------------------------------------------------------------------------
import warnings
def get_psd_from_wavelet_scipy(data):
   widths = wavelet_degree / (2*np.pi*wavelet_freq)
   warnings.simplefilter(action='ignore', category=DeprecationWarning) # supress DeprecationWarning from scipy
   cwtmatr = scipy.signal.cwt( deseason(data), scipy.signal.morlet2, widths=widths, w=wavelet_degree )
   warnings.simplefilter("always")
   psd = np.mean(np.square(np.abs(cwtmatr)),axis=1)
   return psd
#---------------------------------------------------------------------------------------------------
def get_psd_from_wavelet_pywt(data,wavelet):
   # scales = pywt.frequency2scale(wavelet,wavelet_freq)
   # cwtmatr, freq = pywt.cwt( deseason(data), scales=scales, wavelet=wavelet )
   # psd = np.mean(np.square(np.abs(cwtmatr)),axis=1)
   scales = pycwt.frequency2scale(wavelet,wavelet_freq)
   cwtmatr, freq = pycwt.cwt( deseason(data), scales=scales, wavelet=wavelet )
   psd = np.mean(np.square(np.abs(cwtmatr)),axis=1)
   return psd
#---------------------------------------------------------------------------------------------------
# calculate radius of Earth assuming oblate spheroid
def earth_radius(lat):
   # lat: vector or latitudes in degrees  
   # r: vector of radius in meters
   # WGS84: https://earth-info.nga.mil/GandG/publications/tr8350.2/tr8350.2-a/Chapter%203.pdf
   from numpy import deg2rad, sin, cos

   # define oblate spheroid from WGS84
   a = 6378137
   b = 6356752.3142
   e2 = 1 - (b**2/a**2)

   # convert from geodecic to geocentric
   # see equation 3-110 in WGS84
   lat = deg2rad(lat)
   lat_gc = np.arctan( (1-e2)*np.tan(lat) )

   # radius equation
   # see equation 3-107 in WGS84
   r = ( (a * (1 - e2)**0.5) / (1 - (e2 * np.cos(lat_gc)**2))**0.5 )

   return r
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
# read the data

lat_name,lon_name = 'latitude','longitude',

ds = xr.open_dataset( '/global/cfs/cdirs/m4310/whannah/HadISST_sst.nc' )

data = ds['sst']
data = data.where( data['time.year']>=year_start, drop=True)
data = data.where( data['time.year']<=year_end,   drop=True)
#-------------------------------------------------------------------------------
# calculate area
xy_dims = (lon_name,lat_name)
xlon, ylat = np.meshgrid(ds[lat_name],ds[lon_name])
R = earth_radius(ylat)
dlat = np.deg2rad(np.gradient(ylat, axis=0))
dlon = np.deg2rad(np.gradient(xlon, axis=1))
dy,dx = dlat * R , dlon * R * np.cos(np.deg2rad(ylat))
area = np.absolute(dy*dx) / np.square(R) # calculate area and convert to steridians
area = xr.DataArray(area,dims=xy_dims).transpose()
tlon1,tlon2 = lon1,lon2
if tlon1>180: tlon1 = tlon1 - 360
if tlon2>180: tlon2 = tlon2 - 360
#-------------------------------------------------------------------------------
# mask the data
tmp_data = np.ones([len(ds[lat_name]),len(ds[lon_name])],dtype=bool)
tmp_coords = {lat_name:ds[lat_name],lon_name:ds[lon_name]}
mask = xr.DataArray( tmp_data, coords=tmp_coords, dims=(lat_name,lon_name) )
mask = mask & (ds[lat_name]>= lat1) & (ds[lat_name]<= lat2)
mask = mask & (ds[lon_name]>=tlon1) & (ds[lon_name]<=tlon2)
data = data.where( mask, drop=True)
area = area.where( mask, drop=True)
#-------------------------------------------------------------------------------
# calculate area weighted spatial mean
data_avg = ( (data*area).sum(dim=xy_dims) / area.sum(dim=xy_dims) ).compute()
            
# convert to anomalies
data_avg = data_avg - data_avg.mean()

# detrend in time
fit = xr.polyval(data_avg['time'], data_avg.polyfit(dim='time', deg=1).polyfit_coefficients)
data_avg = data_avg - fit

#-------------------------------------------------------------------------------
# calculate wavelet spectrum

wavelet_psd = get_psd_from_wavelet_scipy(data_avg.values)

#-------------------------------------------------------------------------------
## Fit AR1 model to estimate the autocorrelation (alpha) and variance (sigma2)
mod = ARIMA( data_avg.values, order=(1,0,0) )
res = mod.fit()
(alpha,sigma2) = (res.params[1],res.params[2])

#---------------------------------------------------------------------------------------------------
# calculate red noise and 95% confidence level
alpha_assumed = 0.72
# chi_lb = 5.99 
# chi_ub = 0.103 # 5.99 
# chi_lb = 7.3778
chi_ub = 0.0506


red_freq = 1/(wavelet_period_mn)

# mean red noise spectrum
red_noise_mean_psd = ( 1 - np.square(alpha_assumed) ) / ( 1 + np.square(alpha_assumed) - 2*alpha_assumed*np.cos(2*np.pi*red_freq) )

# 95% confidence level
# red_noise_conf_95 = 0.5 * red_noise_mean_psd * chi_val * sigma2
red_noise_conf_95 = 0.5 * red_noise_mean_psd * chi_ub 

# #-------------------------------------------------------------------------------
# calculate red noise spectrum
def get_psd_from_ar1(data,freq):
    ## Fit AR1 model (red noise)
    mod = ARIMA( deseason(data), order=(1,0,0) )
    res = mod.fit()
    (alpha,sigma2) = (res.params[1],res.params[2])
    ## Compute power spectral density
    cos    = alpha * np.cos(2 * np.pi * freq)
    sin    = alpha * np.sin(2 * np.pi * freq)
    psd    = sigma2/((1 - cos)**2 + sin**2)
    ## Compute 95-th confindence interval
    ##   using alpha/2 and 1 - alpha/2 quantiles of
    ##   a chi-square r.v. with 2 degrees of freedom
    ## https://stats.stackexchange.com/a/499241/405221
    chi_lb = 7.3778 ## qchisq(1 - 0.05/2, 2)
    chi_ub = 0.0506 ## qchisq(    0.05/2, 2)
    # chi_lb,chi_ub = 5.991, 0.103
    psd_lb = 2 * psd / chi_lb
    psd_ub = 2 * psd / chi_ub
    return ( psd, psd_lb, psd_ub)

# ( red_noise_mean_psd, red_lower, red_noise_conf_95 ) = get_psd_from_ar1( data_avg.values, red_freq )

#---------------------------------------------------------------------------------------------------
# Create plot with pyngl
if plot_mode=='ngl':
    import ngl
    wkres = ngl.Resources()
    wks = ngl.open_wks(fig_type,fig_file,wkres)
    res = ngl.Resources()
    res.nglDraw                      = False
    res.nglFrame                     = False
    res.tmXTOn                       = False
    res.tmXBMajorOutwardLengthF      = 0.
    res.tmXBMinorOutwardLengthF      = 0.
    res.tmYLMajorOutwardLengthF      = 0.
    res.tmYLMinorOutwardLengthF      = 0.
    res.tmYLLabelFontHeightF         = 0.015
    res.tmXBLabelFontHeightF         = 0.015
    res.tiXAxisFontHeightF           = 0.015
    res.tiYAxisFontHeightF           = 0.015
    res.tmXBMinorOn                  = False
    res.tmYLMinorOn                  = False
    res.vpHeightF                    = 0.5
    res.xyMarkerSizeF                = 0.008
    res.xyMarker                     = 16
    res.xyLineThicknessF             = 12
    res.tiXAxisString = 'Period [years]'
    res.tiYAxisString = 'PSD'
    tm_log = np.array([1,2,4,8,16,32,64])
    res.tmXBMode      = 'Explicit'
    res.tmXBValues    = tm_log
    res.tmXBLabels    = tm_log
    res.xyXStyle ='Log'
    res.trXMinF = np.min(wavelet_period_yr)
    res.trXMaxF = np.max(wavelet_period_yr)
    res.trYMinF = 0
    res.trYMaxF = 15
    res.trXReverse = True
    lres = ngl.Resources()
    lres.nglDraw          = False
    lres.nglFrame         = False
    lres.xyDashPattern    = 1
    lres.xyLineThicknessF = 4

    res.xyLineColor = 'black'
    plot = ngl.xy(wks, wavelet_period_yr, wavelet_psd, res)

    # lres.xyLineColor = 'red'
    # ngl.overlay(plot, ngl.xy(wks, wavelet_period_yr, wavelet_psd_2, lres) )

    # lres.xyLineColor = 'blue'
    # ngl.overlay(plot, ngl.xy(wks, wavelet_period_yr, wavelet_psd_1, lres) )

    # lres.xyLineColor = 'red'
    # ngl.overlay(plot, ngl.xy(wks, wavelet_period_yr, red_noise_mean_psd, lres) )

    # lres.xyLineColor = 'blue'
    # ngl.overlay(plot, ngl.xy(wks, wavelet_period_yr, red_noise_conf_95, lres) )

    ngl.draw(plot); ngl.frame(wks)
#---------------------------------------------------------------------------------------------------
# Create plot with mpl
if plot_mode=='mpl':
    import matplotlib.pyplot as plt
    wavelet_psd_0 = get_psd_from_wavelet_scipy(data_avg.values)
    wavelet_1,wavelet_2,wavelet_3,wavelet_4 = 'morl','cmor1.0-1.0','cmor1.5-1.0','cmor1.5-1.5'
    # wavelet_1,wavelet_2 = 'cmor1.0-1.0','cmor1.5-1.5'
    # wavelet_1,wavelet_2 = 'cmor3.0-1.0','cmor4.0-1.0'
    wavelet_psd_1 = get_psd_from_wavelet_pywt( data_avg.values, wavelet=wavelet_1)
    wavelet_psd_2 = get_psd_from_wavelet_pywt( data_avg.values, wavelet=wavelet_2)
    wavelet_psd_3 = get_psd_from_wavelet_pywt( data_avg.values, wavelet=wavelet_3)
    wavelet_psd_4 = get_psd_from_wavelet_pywt( data_avg.values, wavelet=wavelet_4)
    print()
    print(); print(np.mean(wavelet_psd_0 / wavelet_psd_1))
    print(); print(np.mean(wavelet_psd_0 / wavelet_psd_2))
    print(); print(np.mean(wavelet_psd_0 / wavelet_psd_3))
    print(); print(np.mean(wavelet_psd_0 / wavelet_psd_4))
    print()
    plt.plot(wavelet_period_yr, wavelet_psd_0, color='black', label=f'CWT PSD scipy')
    plt.plot(wavelet_period_yr, wavelet_psd_1, color='red',   label=f'CWT PSD pywt {wavelet_1}')
    plt.plot(wavelet_period_yr, wavelet_psd_2, color='blue',  label=f'CWT PSD pywt {wavelet_2}')
    plt.plot(wavelet_period_yr, wavelet_psd_3, color='green', label=f'CWT PSD pywt {wavelet_3}')
    plt.plot(wavelet_period_yr, wavelet_psd_4, color='purple',label=f'CWT PSD pywt {wavelet_4}')
    plt.xscale('log')
    plt.xticks([64., 32., 16., 8., 4., 2., 1, 0.5])
    plt.xlim( np.min(wavelet_period_yr), np.max(wavelet_period_yr) )
    plt.ylim(0, 15)
    plt.title('')
    plt.xlabel('Period [years]')
    plt.ylabel('PSD')
    plt.legend()
    plt.savefig(f'{fig_file}.{fig_type}')
# #---------------------------------------------------------------------------------------------------
# # Create plot with mpl
# if plot_mode=='mpl':
#     import matplotlib.pyplot as plt
#     wavelet_psd_0 = get_psd_from_wavelet_scipy(data_avg.values)
#     wavelet_psd_1 = get_psd_from_wavelet_pywt( data_avg.values, wavelet='morl')
#     wavelet_psd_2 = get_psd_from_wavelet_pywt( data_avg.values, wavelet='cmor1.5-1.0')
#     plt.plot(wavelet_period_yr, wavelet_psd_0, color='black', label='CWT PSD scipy')
#     plt.plot(wavelet_period_yr, wavelet_psd_1, color='red',   label='CWT PSD pywt morl')
#     plt.plot(wavelet_period_yr, wavelet_psd_2, color='blue',  label='CWT PSD pywt cmor1.5-1.0')
#     # plt.plot(wavelet_period_yr, wavelet_psd, color='black', label='Wavelets')
#     # plt.plot(wavelet_period_yr, red_noise_mean_psd, color='red', linestyle='--', label='Red noise (mean)')  # Blue line
#     # plt.plot(wavelet_period_yr, red_noise_mean_psd * 5.99 / 2, color='blue', linestyle='--', label='Red noise (95%)') # Green line
#     plt.xscale('log')
#     # plt.gca().invert_xaxis() # Reversing the x-axis
#     plt.xticks([64., 32., 16., 8., 4., 2., 1, 0.5])
#     plt.xlim( np.min(wavelet_period_yr), np.max(wavelet_period_yr) )
#     plt.ylim(0, 15)
#     plt.title('')
#     plt.xlabel('Period [years]')
#     plt.ylabel('PSD')
#     plt.legend()
#     # plt.show()
#     plt.savefig(f'{fig_file}.{fig_type}')
#---------------------------------------------------------------------------------------------------
# trim white space
os.system(f'convert -trim +repage {fig_file}.{fig_type} {fig_file}.{fig_type}')
print(f'\n{fig_file}.{fig_type}\n')
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
